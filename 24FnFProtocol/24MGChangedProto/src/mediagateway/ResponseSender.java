/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediagateway;

import java.io.*;
import java.net.*;

public class ResponseSender {

    public static void send(String msg, String ip, int port) {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte sendData[] = new byte[msg.length()];
            sendData = msg.getBytes();
            DatagramPacket packet = new DatagramPacket(sendData, sendData.length, InetAddress.getByName(ip), port);
            socket.send(packet);
            socket.close();
        } catch (IOException ex) {
            System.out.println("Error in sending msg to proxy-->" + ex.getMessage());
        }
    }
}
