/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mediagateway;

import header.HeaderLoader;
import header.LoadHeader;
import java.net.*;
import java.io.*;
import java.util.Date;
import local.sbc.MediaGatewayProfile;
import org.zoolu.sip.message.Message;
import org.zoolu.tools.Log;
import local.sbc.MediaGw;
import org.zoolu.tools.RotatingLog;
import org.zoolu.tools.ShutdownDetection;
import org.apache.log4j.Logger;
import org.zoolu.sip.utils.AppConstants;

public class MediaGateway extends Thread {

    static Logger logger = Logger.getLogger(MediaGateway.class.getName());
    private static ShutdownDetection shutdownDetection = null;
    MediaGw media_gw = null;
    protected Log log = null;
    MediaGatewayProfile mg_profile = null;

    public MediaGateway(String config_file) {
        mg_profile = new MediaGatewayProfile(config_file);
        log = new RotatingLog(mg_profile.log_path + "//" + mg_profile.log_file + ".log", mg_profile.debug_level, mg_profile.max_logsize * 1024, mg_profile.log_rotations, mg_profile.rotation_scale, mg_profile.rotation_time);
        media_gw = new MediaGw(mg_profile, log);
    }

    public void run() {
        try {
            DatagramSocket serverSocket = new DatagramSocket(mg_profile.mg_host_port);
            byte[] receiveData = new byte[1536];
            while (true) {
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                serverSocket.receive(receivePacket);
                String line = new String(receivePacket.getData());
                logger.debug("Packet received from proxy-->" + receivePacket.getSocketAddress().toString() + " at " + new Date());
                Message msg = new Message(line);
                msg = media_gw.processSessionDescriptor(msg);
                ResponseSender.send(msg.toString(), receivePacket.getAddress().getHostAddress(), mg_profile.proxy_server_port);
                logger.debug("Packet sent to proxy-->" + receivePacket.getSocketAddress().toString() + " at " + new Date());
            }
        } catch (IOException ex) {
            logger.debug("Error receiving packet from proxy-->" + ex.getMessage());
        }
    }

    public static void main(String[] args) {
        String file = null;

        for (int i = 0; i < args.length; i++) {
            if (args[i].equals("-f") && args.length > (i + 1)) {
                file = args[++i];
                continue;
            }
            if (args[i].equals("-h")) {
                System.out.println("usage:\n   java Proxy [options] \n");
                System.out.println("   options:");
                System.out.println("   -h               this help");
                System.out.println("   -f <config_file> specifies a configuration file");
                System.exit(0);
            }
        }

        MediaGateway mg = new MediaGateway(file);
        mg.start();
        // TIMER THREAD TO LOAD HEADER DATA
        HeaderLoader.getInstance().load_headers_db();
        HeaderLoader.getInstance().load_dialpeers_db();
        LoadHeader.setExecution_time(System.currentTimeMillis());
        LoadHeader header_load = new LoadHeader(AppConstants.LOADING_INTERVAL);
        header_load.start();

        shutdownDetection = new ShutdownDetection();
        shutdownDetection.start();

    }
}
