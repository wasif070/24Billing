/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.message;

/** Class SipResponses provides all raeson-phrases
 * corrspondent to the various SIP response codes */
public abstract class BaseSipResponses {
    //static Hashtable reasons;

    protected static String[] reasons;
    private static boolean is_init = false;

    protected static void init() {
        if (is_init) {
            return;
        }
        //else

        //reasons=new Hashtable();
        //reasons.put(new Integer(100),"Trying");
        //..
        reasons = new String[1700];
        for (int i = 1000; i < 1700; i++) {
            reasons[i] = null;
        }

        // Not defined (included just to robustness)
        reasons[1000] = "Ovvontorin somossa"; //"Internal error";

        // Informational
        reasons[1100] = "Chesta"; //"Trying";
        reasons[1180] = "Dhonito"; //"Ringing";
        reasons[1181] = "Barta Ogroshor kora hoyese"; //"Call Is Being Forwarded";
        reasons[1182] = "Shariboddho"; //"Queued";
        reasons[1183] = "Shomoy Ogrogoti"; //"Session Progress";

        // Success
        reasons[1200] = "Assa"; //"OK";

        // Redirection
        reasons[1300] = "Ekadhik Posondo"; //"Multiple Choices";
        reasons[1301] = "Shorano Sthayivabe"; //"Moved Permanently";
        reasons[1302] = "Shorano Shamoikvabe"; //"Moved Temporarily";
        reasons[1305] = "Bebohar Protinidhi"; //"Use Proxy";
        reasons[1380] = "Bikolpo Sheba"; //"Alternative Service";

        // Client-Error
        reasons[1400] = "Ku Prostab"; //"Bad Request";
        reasons[1401] = "Oboidho"; //"Unauthorized";
        reasons[1402] = "Porisodh Korun"; //"Payment Required";
        reasons[1403] = "Nisiddho"; //"Forbidden";
        reasons[1404] = "Pai Nai"; //"Not Found";
        reasons[1405] = "Poddhoti Boidho Noy"; //"Method Not Allowed";
        reasons[1406] = "Grohonjoggo Noy"; //"Not Acceptable";
        reasons[1407] = "Protinidhi Proman Abosshok"; //"Proxy Authentication Required";
        reasons[1408] = "Onurodh Somoysesh"; //"Request Timeout";
        reasons[1410] = "Biday"; //"Gone";
        reasons[1413] = "Onurodh Ostitto Onek Boro"; //"Request Entity Too Large";
        reasons[1414] = "Onurodh-URI Onek Boro"; //"Request-URI Too Large";
        reasons[1415] = "Oshomorthito Media Nomuna"; //"Unsupported Media Type";
        reasons[1416] = "Oshomorthito URI Prokolpo"; //"Unsupported URI Scheme";
        reasons[1420] = "Kharap Proshar"; //"Bad Extension";
        reasons[1421] = "Proshar Dorkar"; //"Extension Required";
        reasons[1423] = "Biroti Khub Kom"; //"Interval Too Brief";
        reasons[1480] = "Shamoikvabe prappo noy";//"Temporarily not available";
        reasons[1481] = "Ahoban Pa/Lenden Somvob Noy"; //"Call Leg/Transaction Does Not Exist";
        reasons[1482] = "Ghuron Shonakto"; //"Loop Detected";
        reasons[1483] = "Onek Besi Hops"; //"Too Many Hops";
        reasons[1484] = "Thikana Oshompurno"; //"Address Incomplete";
        reasons[1485] = "Osposhto"; //"Ambiguous";
        reasons[1486] = "Besto Ekhane"; //"Busy Here";
        reasons[1487] = "Onurodh Batil"; //"Request Terminated";
        reasons[1488] = "Grohonjoggo Noy"; //"Not Acceptable Here";
        reasons[1491] = "Onurodh Multobi"; //"Request Pending";
        reasons[1493] = "Durboddho"; //"Undecipherable";

        // Server-Error
        reasons[1500] = "Ovvontorin Server Truti"; //"Internal Server Error";
        reasons[1501] = "Proyog Hoyni"; //"Not Implemented";
        reasons[1502] = "Kharap Dorja"; //"Bad Gateway";
        reasons[1503] = "Sheba Nei"; //"Service Unavailable";
        reasons[1504] = "Server Somoy-sesh"; //"Server Time-out";
        reasons[1505] = "24FNF Version Shomorthon kore na"; //"SIP Version not supported";
        reasons[1513] = "Barta Onek Bishal"; //"Message Too Large";

        // Global-Failure
        reasons[1600] = "Besto Charidike"; //"Busy Everywhere";
        reasons[1603] = "Ossikar"; //"Decline";
        reasons[1604] = "Kothao kono ostitto nei"; //"Does not exist anywhere";
        reasons[1606] = "Grohonjoggo Noy";//"Not Acceptable";

        is_init = true;
    }

    /** Gets the reason phrase of a given response <i>code</i> */
    public static String reasonOf(int code) {
        if (!is_init) {
            init();
        }
        if (reasons[code] != null) {
            return reasons[code];
        } else {
            return reasons[((int) (code / 1000)) * 1000];
        }
    }
    /** Sets the reason phrase for a given response <i>code</i> */
    /*public static void setReason(int code, String reason)
    {  reasons[((int)(code/100))*100]=reason;
    }*/
}