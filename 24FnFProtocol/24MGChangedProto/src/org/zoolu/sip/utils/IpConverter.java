/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import java.util.StringTokenizer;
import org.zoolu.sip.address.SipURL;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.provider.SipParser;

/**
 *
 * @author reefat
 */
public class IpConverter {

    public static long ipToLong(String ipStr) {
        String[] ip = ipStr.split("\\.");
        long ipVal = 0;
        for (int i = 0; i < 4; i++) {
            ipVal <<= 8;
            ipVal |= Integer.parseInt(ip[i]);
        }
        ipVal -= 2147483647;
        return ipVal;
    }

    public static String longToIp(long l) {
        l += 2147483647;
        return ((l >> 24) & 0xFF) + "."
                + ((l >> 16) & 0xFF) + "."
                + ((l >> 8) & 0xFF) + "."
                + (l & 0xFF);
    }


    public static void main(String[] args) {
        System.out.println("getIPVal ==>  " + ipToLong("10.0.2.15"));
        System.out.println("hudai ==>  " + longToIp(ipToLong("192.168.1.95")));

        String delims = BaseMessage.SIP_VERSION;

        StringTokenizer st = new StringTokenizer("1654|sadad|asdsd", delims);
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           System.out.println("No of Token = " + st.countTokens());

        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

    }
}
