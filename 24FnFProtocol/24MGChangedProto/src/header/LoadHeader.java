/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package header;

import org.zoolu.sip.utils.AppConstants;

/**
 *
 * @author Wasif Islam
 */
public class LoadHeader extends Thread {

    long RELOAD_TIME = AppConstants.LOADING_INTERVAL;
    static long execution_time = 0;

    public LoadHeader(long reload_time) {
        RELOAD_TIME = reload_time;
    }

    public static void setExecution_time(long execution_time) {
        LoadHeader.execution_time = execution_time;
    }

    @Override
    public void run() {
        while (true) {
            try {
                if (System.currentTimeMillis() - execution_time >= RELOAD_TIME) {
                    HeaderLoader.getInstance().load_headers_db();
                    HeaderLoader.getInstance().load_dialpeers_db();
                    execution_time = System.currentTimeMillis();
                }
                this.sleep(RELOAD_TIME);
            } catch (Exception ex) {
            }
        }
    }
}