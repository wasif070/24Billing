/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package header;

import remotedbconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.header.EndPointHeader;
import org.zoolu.sip.header.SipHeaders;
import org.zoolu.sip.header.SubscriptionStateHeader;
import org.zoolu.sip.header.ViaHeader;
import org.zoolu.sip.message.BaseMessage;
import org.zoolu.sip.message.BaseSipMethods;
import org.zoolu.sip.message.SipMethods;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.provider.SipProvider;
import org.zoolu.sip.provider.UdpTransport;
import org.zoolu.sip.utils.AppConstants;

/**
 *
 * @author user
 */
public class HeaderLoader {

    private HashMap<String, String> headersMap = new HashMap<String, String>();
    public static HeaderLoader loader = null;
    private HashMap<Integer, String> dialpeersMap = new HashMap<Integer, String>();

    public HeaderLoader() {
        load_headers_db();
        load_dialpeers_db();
    }

    public static HeaderLoader getInstance() {
        if (loader == null) {
            createLoader();
        }
        return loader;
    }

    private synchronized static void createLoader() {
        if (loader == null) {
            loader = new HeaderLoader();
        }
    }

    public void load_headers_db() {
        //headersMap = new HashMap<String, String>();
        remotedbconnector.DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            con = remotedbconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            String sql = "select * from headers";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                headersMap.put(rs.getString("sip_header"), rs.getString("24fnf_header"));
            }

            //BaseSipMethods
            BaseSipMethods.REGISTER = getHeader("REGISTER");
            BaseSipMethods.INVITE = getHeader("INVITE");
            BaseSipMethods.ACK = getHeader("ACK");
            BaseSipMethods.CANCEL = getHeader("CANCEL");
            BaseSipMethods.BYE = getHeader("BYE");
            BaseSipMethods.INFO = getHeader("INFO");
            BaseSipMethods.OPTIONS = getHeader("OPTIONS");
            BaseSipMethods.UPDATE = getHeader("UPDATE");
            //SipMethods
            SipMethods.SUBSCRIBE = getHeader("SUBSCRIBE");
            SipMethods.NOTIFY = getHeader("NOTIFY");
            SipMethods.MESSAGE = getHeader("MESSAGE");
            SipMethods.REFER = getHeader("REFER");
            SipMethods.PUBLISH = getHeader("PUBLISH");
            //BaseMessage
            BaseMessage.PROTO_UDP = getHeader("udp");
            BaseMessage.PROTO_TCP = getHeader("tcp");
            BaseMessage.PROTO_TLS = getHeader("tls");
            BaseMessage.PROTO_SCTP = getHeader("sctp");
            BaseMessage.SIP_VERSION = getHeader("SIP/");
            //UdpTransport
            UdpTransport.PROTO_UDP = getHeader("udp");
            //SipProvider
            SipProvider.PROTO_UDP = getHeader("udp");
            SipProvider.PROTO_TCP = getHeader("tcp");
            SipProvider.PROTO_TLS = getHeader("tls");
            SipProvider.PROTO_SCTP = getHeader("sctp");
            //SipHeaders
            SipHeaders.Refer_To = getHeader("Refer-To");
            SipHeaders.Referred_By = getHeader("Referred-By");
            SipHeaders.Replaces = getHeader("Replaces");
            SipHeaders.Event = getHeader("Event");
            SipHeaders.Event_short = getHeader("o");
            SipHeaders.Allow_Events = getHeader("Allow-Events");
            SipHeaders.Subscription_State = getHeader("Subscription-State");
            //BaseSipHeaders
            BaseSipHeaders.Accept = getHeader("Accept");
            BaseSipHeaders.Alert_Info = getHeader("Alert-Info");
            BaseSipHeaders.Allow = getHeader("Allow");
            BaseSipHeaders.Authentication_Info = getHeader("Authentication-Info");
            BaseSipHeaders.Authorization = getHeader("Authorization");
            BaseSipHeaders.Call_ID = getHeader("Call-ID");
            BaseSipHeaders.Call_ID_short = getHeader("i");
            BaseSipHeaders.Contact = getHeader("Contact");
            BaseSipHeaders.Contact_short = getHeader("m");
            BaseSipHeaders.Content_Length = getHeader("Content-Length");
            BaseSipHeaders.Content_Length_short = getHeader("l");
            BaseSipHeaders.Content_Type = getHeader("Content-Type");
            BaseSipHeaders.Content_Type_short = getHeader("c");
            BaseSipHeaders.CSeq = getHeader("CSeq");
            BaseSipHeaders.Date = getHeader("Date");
            BaseSipHeaders.Expires = getHeader("Expires");
            BaseSipHeaders.From = getHeader("From");
            BaseSipHeaders.From_short = getHeader("f");
            BaseSipHeaders.User_Agent = getHeader("User-Agent");
            BaseSipHeaders.Max_Forwards = getHeader("Max-Forwards");
            BaseSipHeaders.Proxy_Authenticate = getHeader("Proxy-Authenticate");
            BaseSipHeaders.Proxy_Authorization = getHeader("Proxy-Authorization");
            BaseSipHeaders.Proxy_Require = getHeader("Proxy-Require");
            BaseSipHeaders.Record_Route = getHeader("Record-Route");
            BaseSipHeaders.Require = getHeader("Require");
            BaseSipHeaders.Route = getHeader("Route");
            BaseSipHeaders.Server = getHeader("Server");
            BaseSipHeaders.Subject = getHeader("Subject");
            BaseSipHeaders.Subject_short = getHeader("s");
            BaseSipHeaders.Supported = getHeader("Supported");
            BaseSipHeaders.Supported_short = getHeader("k");
            BaseSipHeaders.To = getHeader("To");
            BaseSipHeaders.To_short = getHeader("t");
            BaseSipHeaders.Unsupported = getHeader("Unsupported");
            BaseSipHeaders.Via = getHeader("Via");
            BaseSipHeaders.Via_short = getHeader("v");
            BaseSipHeaders.WWW_Authenticate = getHeader("WWW-Authenticate");
            //ViaHeader
            ViaHeader.received_param = getHeader("received");
            ViaHeader.rport_param = getHeader("rport");
            ViaHeader.branch_param = getHeader("branch");
            ViaHeader.maddr_param = getHeader("maddr");
            ViaHeader.ttl_param = getHeader("ttl");
            //EndPointHeader
            EndPointHeader.tag_param = getHeader("tag");
            EndPointHeader.expires_param = getHeader("expires");
            //ServerEngine
//            ServerEngine.Loop_Tag = getHeader("Loop-Tag");
            //SubscriptionStateHeader
            SubscriptionStateHeader.ACTIVE = getHeader("active");
            SubscriptionStateHeader.PENDING = getHeader("pending");
            SubscriptionStateHeader.TERMINATED = getHeader("terminated");
            //SipParser                
            SipParser.header_param = getHeader("sip");
            SipParser.headers_params = getHeader("sips");
            SipParser.header_colon = getHeader(":");
            SipParser.bracket_header_start = getHeader("<");
            SipParser.bracket_header_end = getHeader(">");

        } catch (Exception ex) {
            System.err.println("Headers ex-->" + ex.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(con);
                }
            } catch (Exception ex) {
            }
        }
    }

    public String getHeader(String sip_header) {
        String fnf_header = sip_header;
        if (headersMap != null && headersMap.get(sip_header) != null) {
            fnf_header = headersMap.get(sip_header);
        }
        return fnf_header;
    }

    public void load_dialpeers_db() {
        remotedbconnector.DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            int i = 0;
            con = remotedbconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            String sql = "select dialpeer_term_ip from dialpeers";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                i++;
                dialpeersMap.put(i, rs.getString("dialpeer_term_ip"));
            }
        } catch (Exception ex) {
            System.err.println("Dialpeers ex-->" + ex.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(con);
                }
            } catch (Exception ex) {
            }
        }
    }

    public boolean isRouteIP(String ip) {
        for (int i = 0; i < dialpeersMap.size(); i++) {
            System.out.println(i + "-"+dialpeersMap.get(i));
        }
//        dialpeersMap.size()
        if (dialpeersMap != null && dialpeersMap.containsValue(ip)) {
            return true;
        } else {
            return false;
        }
    }
}
