package local.sbc;

import org.zoolu.net.SocketAddress;
import org.zoolu.tools.Configure;
import org.zoolu.tools.Parser;

import java.util.Vector;
import org.zoolu.sip.utils.IpConverter;
import org.zoolu.tools.RotatingLog;

/**
 * MediaGatewayProfile maintains the SessionBorderController configuration.
 */
public class MediaGatewayProfile extends Configure {

    // *********************** MG configurations *********************
    /** Maximum time that the UDP relay remains active without receiving UDP datagrams (in milliseconds). */
    public long relay_timeout = 60000; // 1min
    /** Refresh time of address-binding cache (in milliseconds) */
    public long binding_timeout = 3600000;
    /** Minimum time between two changes of peer address (in milliseconds) */
    public int handover_time = 0; //5000;
    /** Rate of keep-alive datagrams sent toward all registered UAs (in milliseconds).
     * Set keepalive_time=0 to disable the sending of keep-alive datagrams */
    public long keepalive_time = 0;
    /** Whether sending keepalive datagram only to UAs that explicitely request it through 'keep-alive' parameter. */
    //public boolean keepalive_selective=false;
    /** Whether sending keepalive datagram to all contacted UAs (also toward non-registered UAs) */
    public boolean keepalive_aggressive = false;
    /** Whether implementing symmetric RTP for NAT traversal. */
    //boolean symmetric_rtp=false;
    /** Minimum inter-packet departure time */
    public long interpacket_time = 0;
    /** Whether intercepting media traffics. */
    public boolean do_interception = false;
    /** Whether injecting new media flows. */
    public boolean do_active_interception = false;
    /** Sink address for media traffic interception. */
    public String sink_addr = "127.0.0.1";
    /** Sink port for media traffic interception. */
    public int sink_port = 0;
    /** Media address. */
    public String media_addr = "0.0.0.0";
    /** media gateway listening port. */
    public int mg_host_port = 10001;
    /** media gateway listening port. */
    public int proxy_server_port = 10001;
    /** Available media ports (default interval=[41000:41499]). */
    public Vector media_ports = null;
    /** First Available media port. */
    private int first_port = 41000;
    /** Last Available media port. */
    private int last_port = 41199;
    /** Backend proxy where all requests not coming from it are passed to. 
     * It can be specified as FQDN or host_addr[:host_port].
     * Use 'NONE' for not using a backend proxy (or let it undefined). */
    public SocketAddress backend_proxy = null;
    
    
    // ************************ debug and logs ************************
    /** log file name */
    public static String log_file = "24mg";
    
    /** Log level. Only logs with a level less or equal to this are written. */
    public static int debug_level = 1;
    /** Path for the log folder where log files are written.
     * By default, it is used the "./log" folder.
     * Use ".", to store logs in the current root folder. */
    public static String log_path = "log";
    /** The size limit of the log file [kB] */
    public static int max_logsize = 2048; // 2MB
    /** The number of rotations of log files. Use '0' for NO rotation, '1' for rotating a single file */
    public static int log_rotations = 0; // no rotation
    /** The rotation period, in MONTHs or DAYs or HOURs or MINUTEs
     * examples: log_rotation_time=3 MONTHS, log_rotations=90 DAYS 
     * Default value: log_rotation_time=2 MONTHS */
    private static String log_rotation_time = null;
    /** The rotation time scale */
    public static int rotation_scale = RotatingLog.MONTH;
    /** The rotation time value */
    public static int rotation_time = 2;

    // ************************** costructors *************************
    /** Costructs a new MediaGatewayProfile */
    public MediaGatewayProfile() {
        init(null);
    }

    /** Costructs a new MediaGatewayProfile */
    public MediaGatewayProfile(String file) {
        init(file);
    }

    /** Inits the MediaGatewayProfile */
    private void init(String file) {
        loadFile(file);
        media_ports = new Vector();
        for (int i = first_port; i <= last_port; i += 2) {
            media_ports.addElement(new Integer(i));
        }
    }

    // **************************** methods ***************************
    /** Parses a single line (loaded from the config file) */
    protected void parseLine(String line) {
        String attribute;
        Parser par;
        int index = line.indexOf("=");
        if (index > 0) {
            attribute = line.substring(0, index).trim();
            par = new Parser(line, index + 1);
        } else {
            attribute = line;
            par = new Parser("");
        }

        if (attribute.equals("relay_timeout")) {
            relay_timeout = par.getInt();
            return;
        }
        if (attribute.equals("binding_timeout")) {
            binding_timeout = par.getInt();
            return;
        }
        if (attribute.equals("handover_time")) {
            handover_time = par.getInt();
            return;
        }
        if (attribute.equals("keepalive_time")) {
            keepalive_time = par.getInt();
            return;
        }
        //if (attribute.equals("keepalive_selective")) { keepalive_selective=(par.getString().toLowerCase().startsWith("y")); return; }
        if (attribute.equals("keepalive_aggressive")) {
            keepalive_aggressive = (par.getString().toLowerCase().startsWith("y"));
            return;
        }
        //if (attribute.equals("symmetric_rtp")) { symmetric_rtp=(par.getString().toLowerCase().startsWith("y")); return; }
        if (attribute.equals("interpacket_time")) {
            interpacket_time = par.getInt();
            return;
        }
        if (attribute.equals("do_interception")) {
            do_interception = (par.getString().toLowerCase().startsWith("y"));
            return;
        }
        if (attribute.equals("do_active_interception")) {
            do_active_interception = (par.getString().toLowerCase().startsWith("y"));
            return;
        }
        if (attribute.equals("sink_addr")) {
            sink_addr = par.getString();
            return;
        }
        if (attribute.equals("sink_port")) {
            sink_port = par.getInt();
            return;
        }
        if (attribute.equals("media_addr")) {
            media_addr = String.valueOf(IpConverter.ipToLong(par.getString()));
            return;
        }
        if (attribute.equals("mg_host_port")) {
            mg_host_port = par.getInt();
            return;
        }
        if (attribute.equals("proxy_server_port")) {
            proxy_server_port = par.getInt();
            return;
        }
        if (attribute.equals("media_ports")) {
            char[] delim = {' ', '-', ':'};
            first_port = Integer.parseInt(par.getWord(delim));
            last_port = Integer.parseInt(par.getWord(delim));
            return;
        }
        if (attribute.equals("backend_proxy")) {
            String soaddr = par.getString();
            if (soaddr == null || soaddr.length() == 0 || soaddr.equalsIgnoreCase(Configure.NONE)) {
                backend_proxy = null;
            } else {
                backend_proxy = new SocketAddress(soaddr);
            }
            return;
        }

        // debug and logs
        if (attribute.equals("debug_level")) {
            debug_level = par.getInt();
            return;
        }
        if (attribute.equals("log_path")) {
            log_path = par.getString();
            return;
        }
        if (attribute.equals("max_logsize")) {
            max_logsize = par.getInt();
            return;
        }
        if (attribute.equals("log_rotations")) {
            log_rotations = par.getInt();
            return;
        }
        if (attribute.equals("log_rotation_time")) {
            log_rotation_time = par.getRemainingString();
            return;
        }
    }

    /** Converts the entire object into lines (to be saved into the config file) */
    protected String toLines() {  // currently not implemented..
        return toString();
    }
}
