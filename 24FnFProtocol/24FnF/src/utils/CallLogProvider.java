/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import helpers.JSONResponseListener;
import helpers.JSONResponseProvider;
import helpers.MyFnFSettings;
import myfnfui.RefreshFriendList;
import myfnfui.ResetUserProfileFriendList;
import myfnfui.usedThreads.SetDynamicFriend;
import myfnfui.UpdateCallLog;

/**
 *
 * @author Ashraful
 */
public class CallLogProvider implements JSONResponseListener {

    private UpdateCallLog updateCallLog = null;

    public CallLogProvider() {
    }

    public void setCallLogList(String p_url, UpdateCallLog obj) {
        this.updateCallLog = obj;
        JSONResponseProvider provider = new JSONResponseProvider(this, p_url, 10000);
    }

    @Override
    public void onReceivedResponse(JSONResponseProvider provider, String response, String p_url) {
        try {
            if (response.length() > 0) {
                //updateCallLog.call_log_repaint(response);
            }
        } catch (Exception ex) {
        }
    }
}
