/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import helpers.JSONResponseListener;
import helpers.JSONResponseProvider;
import helpers.MyFnFSettings;
import myfnfui.RefreshFriendList;
import myfnfui.ResetUserProfileFriendList;
import myfnfui.usedThreads.SetDynamicFriend;

/**
 *
 * @author Ashraful
 */
public class ContactListProvider implements JSONResponseListener {

    private RefreshFriendList refreshFriendList = null;

    public ContactListProvider() {
    }

    public void setContactList(String p_url, RefreshFriendList obj) {
        this.refreshFriendList = obj;
        JSONResponseProvider provider = new JSONResponseProvider(this, p_url, 15000);
    }

    @Override
    public void onReceivedResponse(JSONResponseProvider provider, String response, String p_url) {
        try {
            if (response.length() > 0) {
                ResetUserProfileFriendList.set_refesh_friend_clss(response);
                if (SetDynamicFriend.repaint_friendlist) {
                    try {
                        refreshFriendList.getfriend_list();
                    } catch (Exception e) {
                        MyFnFSettings.isRegisterred = false;
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
