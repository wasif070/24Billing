/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import helpers.JSONResponseListener;
import helpers.JSONResponseProvider;
import myfnfui.CreateNewAccount;

public class UserNameProvider implements JSONResponseListener {

    private String search_str = "";
    private CreateNewAccount create_new_acc = null;

    public UserNameProvider() {
    }

    public void setUserAvailability(String p_url, String search_str, CreateNewAccount create_new_acc) {
        this.search_str = search_str;
        this.create_new_acc = create_new_acc;
        JSONResponseProvider provider = new JSONResponseProvider(this, p_url);
    }

    @Override
    public void onReceivedResponse(JSONResponseProvider provider, String response, String p_url) {
        try {
            create_new_acc.set_availability(response, search_str);
        } catch (Exception ex) {
        }
    }
}
