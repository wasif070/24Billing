/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.ua;

import helpers.APIParametersFiles;
import helpers.CallLogHelper;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import static helpers.HelperMethods.getImageName_from_url;
import helpers.MyFnFSettings;
import helpers.StaticImages;
import imageDownload.DownLoaderHelps;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import myfnfui.GUI24FnF;
import myfnfui.Messages.NotificationMessages;
import org.zoolu.sip.address.*;
import org.zoolu.sip.provider.*;
import org.zoolu.tools.*;

/**
 * Simple GUI-based SIP user agent (UA).
 */
public class IncomingGraphicalUA extends JFrame implements UserAgentListener, ActionListener {

    int posX = 0;
    int posY = 0;
    public JPanel main_container;
    public JPanel topBar;
    public JPanel log_container;
    public JLabel log_Label;
    public JButton button_window_close;
    JLabel label_image;
    JLabel display;
    JLabel duration_label;
    JButton call_b;
    JButton cancel_b;
    JLabel user_Status;
    JLabel friend_name;
    public static IncomingGraphicalUA staticObject;
    int counterVariable = 1;
    String printDot = "";
    public JButton button_window_minimize;
    private UserAgentProfile profile;
    public JButton call_p2p;
    JButton call_mobile_button;
    JLabel fnf_lable;
    JLabel mobile_no_lable;
    HelperMethods helperMethods = new HelperMethods();
    DownLoaderHelps dhelp = new DownLoaderHelps();
//-------------------------------- newly changed -----------------------------------
    JLabel friend_image_panel;

    public static IncomingGraphicalUA getStaticObject() {
        return staticObject;
    }

    public static void setStaticObject(IncomingGraphicalUA staticObject) {
        IncomingGraphicalUA.staticObject = staticObject;
    }
    private static SipProvider sip_provider;

    public static void setSipProvider(SipProvider paramSip_provider) {
        sip_provider = paramSip_provider;
    }

    public static SipProvider getSipProvider() {
        return sip_provider;
    }
    private boolean stopDurationCounting = false;

    public void reset() {
        sec = 0;
        min = 0;
        hour = 0;
    }

    public void setStopDurationCounting(boolean stopDurationCounting) {
        this.stopDurationCounting = stopDurationCounting;
    }
    private int sec = 0;
    private int min = 0;
    private int hour = 0;
    java.text.DecimalFormat nft = new DecimalFormat("#00.###");

    protected String showTime() {
        sec++;
        if (sec == 60) {
            sec = 0;
            min++;
            if (min == 60) {
                hour++;
            }
        }
        return (nft.format(hour) + ":" + nft.format(min) + ":" + nft.format(sec));
    }

    public void CallDurationChecker(final JLabel paramDisplay) {

        final ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();

        Runnable periodicTask = new Runnable() {
            public void run() {
                // Invoke method(s) to do the work                
                paramDisplay.setText(doPeriodicWork());
                if (stopDurationCounting) {
                    executor.shutdown();
                }
            }

            private String doPeriodicWork() {
                return showTime();
            }
        };

        executor.scheduleAtFixedRate(periodicTask, 2, 1, TimeUnit.SECONDS);

    }
    /**
     * This application
     */
    final String app_name = "24FnF";
    /**
     * Log
     */
    protected Log log;
    /**
     * User Agent
     */
    protected UserAgent ua;
    /**
     * UserAgentProfile
     */
    protected UserAgentProfile ua_profile;
    // ************************* UA internal state *************************
    /**
     * UA_IDLE=0
     */
    protected static final String UA_IDLE = "IDLE";
    /**
     * UA_INCOMING_CALL=1
     */
    protected static final String UA_INCOMING_CALL = "INCOMING_CALL";
    /**
     * UA_OUTGOING_CALL=2
     */
    protected static final String UA_OUTGOING_CALL = "OUTGOING_CALL";
    /**
     * UA_ONCALL=3
     */
    protected static final String UA_ONCALL = "ONCALL";
    /**
     * Call state: <P>UA_IDLE=0, <BR>UA_INCOMING_CALL=1, <BR>UA_OUTGOING_CALL=2,
     * <BR>UA_ONCALL=3
     */
    String call_state = UA_IDLE;

    /**
     * Changes the call state
     */
    protected void changeStatus(String state) {
        call_state = state;
        printLog("state: " + call_state, Log.LEVEL_MEDIUM);
    }

    /**
     * Checks the call state
     */
    protected boolean statusIs(String state) {
        return call_state.equals(state);
    }

    /**
     * Gets the call state
     */
    protected String getStatus() {
        return call_state;
    }

    // *************************** Public methods **************************
    /**
     * Creates a new GraphicalUA
     */
    public IncomingGraphicalUA(SipProvider sip_provider, UserAgentProfile ua_profile) {

        super();
        setStaticObject(this);

        profile = ua_profile;
        if (profile != null) {
            //  System.out.println(profile.getCountry());
        } else {
            //  System.out.println("dddddddddddddddddddd");
        }
        this.setTitle(ua_profile.getFirstName() + " " + ua_profile.getLastName());
        this.sip_provider = sip_provider;
        this.ua_profile = ua_profile;
        log = sip_provider.getLog();
        this.ua = GUI24FnF.getUserAgent();
        ua.setListener(this);
        changeStatus(UA_INCOMING_CALL);

        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void jbInit() throws Exception {
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + 100, DefaultSettings.DEFAULT_LOCATION_TOP + 45);
        this.setSize(DefaultSettings.DEFAULT_WIDTH, 400);
        int start_point = 27;
        int text_width = 180;
        int start_text_point = start_point + 80;
        display = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("Incoming Call Window", start_text_point, start_point + 35, text_width, 11, 1, Color.WHITE);
        final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                if (!statusIs(UA_INCOMING_CALL)) {
                    try {
                        executor.shutdown();
                        counterVariable = 1;
                    } catch (Exception e) {
                    }
                }
                switch (counterVariable) {
                    case 1:
                        printDot = ".  ";
                        break;
                    case 2:
                        printDot = ".. ";
                        break;
                    case 3:
                        printDot = "...";
                        break;

                }
                if (statusIs(UA_INCOMING_CALL)) {
                    display.setText("Incoming Call" + printDot);
                }
                counterVariable++;
                if (counterVariable > 3) {
                    counterVariable = 1;
                }

            }
        };
        executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);

        ImageIcon imageIcon = DesignClasses.return_image(StaticImages.ICON_24MYFNF);
        Image image = imageIcon.getImage();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setIconImage(image);
        this.setTitle("24FnF " + ua_profile.getFirstName() + " " + ua_profile.getLastName());
        this.setSize(DefaultSettings.DEFAULT_WIDTH, DefaultSettings.DEFAULT_HEIGHT / 2 - 150);
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + 380, DefaultSettings.DEFAULT_LOCATION_TOP + 150);
        this.setResizable(false);
        this.setUndecorated(true);
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                //sets frame position when mouse dragged			
                setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);

            }
        });

        addWindowListener(
                new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                HelperMethods.create_confrim_panel(NotificationMessages.CANCEL_NOTIFICAITON, 20);
                if (HelperMethods.confirm == 1) {
                    jButton2_actionPerformed();
                    dispose();
                    ua.setListener(GUI24FnF.getObjMain());
                } else {
                    setVisible(true);
                }
            }
        });
        main_container = (JPanel) getContentPane();
        main_container.setBorder(DefaultSettings.calling_window_border_color);
        main_container.setBackground(DefaultSettings.calling_window_background_color);
        main_container.setLayout(null);
        main_container.add(DesignClasses.create_colored_bottom_border(2, 26, 300, Color.WHITE));
        label_image = new JLabel("", DesignClasses.return_image(StaticImages.IMAGE_callingwindow_bar), JLabel.CENTER);
        label_image.setBackground(null);
        topBar = new JPanel(new BorderLayout());
        topBar.add(label_image, BorderLayout.CENTER);
        topBar.setBounds(2, 2, DefaultSettings.DEFAULT_WIDTH - 4, DefaultSettings.DEFAULT_TOP_BAR / 2);
        topBar.setBorder(null);
        String bar_title = "24FnF Call";

        JLabel bar_text = DesignClasses.makeJLabel_with_text_color(bar_title, 10, 0, DefaultSettings.DEFAULT_WIDTH - 50, 11, 1, Color.WHITE);
        main_container.add(bar_text);
        int minimize_button_left_possion = 256;

        //    JLabel friend_image_panel = DesignClasses.create_image_label_with_border(10, start_point + 30, 70, 70, frined_image);
        friend_image_panel = helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), 10, start_point + 30, 70, 70);


//        friend_name = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("Calling " + profile.getUserIdentity(), start_text_point, start_point + 15, text_width, 14, 1, Color.WHITE);

        friend_name = DesignClasses.makeJLabelCenterAling_with_font_size_width_color(profile.getUserIdentity(), start_text_point, start_point + 15, text_width, 14, 1, Color.WHITE);
        main_container.add(friend_image_panel);
        main_container.add(friend_name);
        main_container.add(display);
        duration_label = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("", start_text_point, start_point + 60, text_width, 12, 1, Color.WHITE);
        button_window_close = DesignClasses.create_button_with_image_fixed_location(StaticImages.ICON_CLOSE, StaticImages.ICON_CLOSE_HOVER, "Close Window", minimize_button_left_possion + 22, 2, 20, 20);
        button_window_close.addActionListener(this);
        button_window_minimize = DesignClasses.create_button_with_image_fixed_location(StaticImages.IMAGE_MINIMIZE, StaticImages.IMAGE_MINIMIZE_HOVER, "Minimize", minimize_button_left_possion, 2, 20, 20);
        button_window_minimize.addActionListener(this);
        main_container.add(button_window_minimize);
        call_b = new JButton();
        call_b.setBounds(95, 120, 95, 24);
        call_b.setBorder(null);
        call_b.addActionListener(this);
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER_H));
        call_b.addActionListener(this);
        main_container.add(call_b);
        cancel_b = new JButton();
        cancel_b.setBounds(198, 120, 95, 24);
        cancel_b.setBorder(null);
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE_H));
        cancel_b.addActionListener(this);
        main_container.add(cancel_b);
        main_container.add(button_window_close);
        main_container.add(topBar);
        main_container.add(duration_label);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            jButton2_actionPerformed();
            this.dispose();
            ua.setListener(GUI24FnF.getObjMain());
        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        } else if (event.getSource() == cancel_b) {
//            if (statusIs(UA_IDLE)) {
////                display.setText("");
////                duration_label.setText("");
//            }

//----------------------------------------------------------------------------------------
            if (profile.getPresence() != null) {
                //System.out.println(sec);
                if ((profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred)) {
                    call_b.setEnabled(true);
                    if (statusIs(UA_OUTGOING_CALL)) {
                        if (CallLogHelper.callId != null) {
                            CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
//                            sec = 0;
                        }
                    }

                } else {
                    call_b.setEnabled(false);
                }
//                if (statusIs(UA_IDLE)) {
////                display.setText("");
////                duration_label.setText("");
//                }
            }
//----------------------------------------------------------------------------------------                        
            jButton2_actionPerformed();
        } else if (event.getSource() == call_b) {
            duration_label.setText("");
            call_b.setEnabled(false);
            jButton1_actionPerformed();
        }

    }

    /**
     * When the call/accept button is pressed.
     */
    void jButton1_actionPerformed() {
        if (statusIs(UA_IDLE)) {
            String url = profile.getUserIdentity();
            if (url != null && url.length() > 0) {
                ua.hangup();
                changeStatus(UA_OUTGOING_CALL);
                display.setText("Calling ");
                ua.call(url);

                CallLogHelper.sent_call_log_to_server_in_call_start(ua_profile.getUserIdentity(), ua_profile.getUserIdentity(), (long) 0000);

//                if (ua_profile.hangup_time > 0) {
//                    automaticHangup(ua_profile.hangup_time);
//                }
            }
        } else if (statusIs(UA_INCOMING_CALL)) {
            this.setStopDurationCounting(false);
            ua.accept();
            changeStatus(UA_ONCALL);
            this.reset();

            this.CallDurationChecker(duration_label);
            display.setText("Call Established");
            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_END));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_END_H));
            call_b.setEnabled(false);
        }
        IncomingGraphicalUA.getStaticObject().setAlwaysOnTop(false);
    }

    /**
     * changeStatus(UA_INCOMING_CALL); When the refuse/hangup button is pressed.
     */
    void jButton2_actionPerformed() {
        this.setStopDurationCounting(false);
        if (!statusIs(UA_IDLE)) {
            ua.hangup();
            changeStatus(UA_IDLE);
            this.setStopDurationCounting(true);
            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));
            display.setText("Call Cancelled");
        }
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        if (profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) {
            call_b.setEnabled(true);
        } else {
            call_b.setEnabled(false);
        }
        if (IncomingGraphicalUA.getStaticObject() != null) {

            IncomingGraphicalUA.getStaticObject().setAlwaysOnTop(false);
        }
    }

    /**
     * Gets the UserAgent
     */
    // ********************** UA callback functions **********************
    /**
     * When a new call is incoming
     */
    @Override
    public void onUaIncomingCall(UserAgent ua, NameAddress callee, NameAddress caller, Vector media_descs) {

        String[] temp = caller.getAddress().toString().split("@");
        String temp_var = temp[0].substring(temp[0].indexOf("|") + 1, temp[0].length());
        profile = HelperMethods.get_all_for_a_profile(temp_var);

        friend_name.setText(profile.getUserIdentity());
        BufferedImage img = null;
        String location = DesignClasses.get_male_or_female_image(profile.getGender());
        // helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), 10, start_point + 30, 70, 70);
        if (profile.getProfileImage() != null && profile.getProfileImage().length() > 3) {
            String image_name = getImageName_from_url(profile.getProfileImage());
            //   System.out.println("******************************" + image_name);
            File f = new File(dhelp.getDestinationFolder() + "\\" + image_name);
            if (f.exists()) {
                location = dhelp.getUser_images_location() + image_name;
            } else {
                location = DesignClasses.get_scaled_male_or_female_image(profile.getGender());
            }
        } else {
            location = DesignClasses.get_scaled_male_or_female_image(profile.getGender());
        }

        img = HelperMethods.scalled_image(location, 70, 70);
        friend_image_panel.setIcon(new ImageIcon(img));
        changeStatus(UA_INCOMING_CALL);

        IncomingGraphicalUA.getStaticObject().toFront();
        IncomingGraphicalUA.getStaticObject().setAlwaysOnTop(true);
        IncomingGraphicalUA.getStaticObject().setState(Frame.NORMAL);
        display.setText("");
        duration_label.setText("");
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER_H));
        call_b.setEnabled(true);


        if (ua_profile.redirect_to != null) // redirect the call
        {
            display.setText("CALL redirected to " + ua_profile.redirect_to);
            ua.redirect(ua_profile.redirect_to);
        } else if (ua_profile.accept_time >= 0) // automatically accept the call
        {
            display.setText("Call Established");
            automaticAccept(ua_profile.accept_time);
        } else {
//            friend_name.setText("Calling " + profile.getUserIdentity());
//            friend_name.setText(profile.getFirstName() + " " + profile.getLastName());
//            friend_name.setText(profile.getUserIdentity());

            final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            Runnable periodicTask = new Runnable() {
                public void run() {
                    if (!statusIs(UA_INCOMING_CALL)) {
                        try {
                            executor.shutdown();
                            counterVariable = 1;
                        } catch (Exception e) {
                        }
                    }
                    switch (counterVariable) {
                        case 1:
                            printDot = ".  ";
                            break;
                        case 2:
                            printDot = ".. ";
                            break;
                        case 3:
                            printDot = "...";
                            break;

                    }
                    if (statusIs(UA_INCOMING_CALL)) {
                        display.setText("Incoming Call" + printDot);
                    }
                    counterVariable++;
                    if (counterVariable > 3) {
                        counterVariable = 1;
                    }

                }
            };

            executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);

            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE_H));
            set_reset_call_button(false);
        }
    }

    /**
     * When an ougoing call is stated to be in progress
     */
    public void onUaCallProgress(UserAgent ua) {
        display.setText("PROGRESS");
    }

    /**
     * When an ougoing call is remotly ringing
     */
    public void onUaCallRinging(UserAgent ua) {
        //display.setText("RINGING");
        final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                if (!statusIs(UA_OUTGOING_CALL)) {
                    try {
                        executor.shutdown();
                        counterVariable = 1;
                    } catch (Exception e) {
                    }
                }

                switch (counterVariable) {
                    case 1:
                        printDot = ".  ";
                        break;
                    case 2:
                        printDot = ".. ";
                        break;
                    case 3:
                        printDot = "...";
                        break;
                }

                counterVariable++;
                if (counterVariable > 3) {
                    counterVariable = 1;
                }

                if (statusIs(UA_OUTGOING_CALL)) {
                    display.setText("Ringing" + printDot);
                }
            }
        };

        executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);
    }

    /**
     * When an ougoing call has been accepted
     */
    public void onUaCallAccepted(UserAgent ua) {
        this.reset();
        this.setStopDurationCounting(false);
        this.CallDurationChecker(duration_label);
        changeStatus(UA_ONCALL);
        display.setText("Call Established");
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_END));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_END_H));
        if (ua_profile.hangup_time > 0) {
            automaticHangup(ua_profile.hangup_time);
        }
    }

    /**
     * When an incoming call has been cancelled
     */
    public void onUaCallCancelled(UserAgent ua) {
        changeStatus(UA_IDLE);
        display.setText("Call Cancelled");
//        CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) sec * 1000);

        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_H));
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));
    }

    /**
     * When a call has been transferred
     */
    public void onUaCallTransferred(UserAgent ua) {
        changeStatus(UA_IDLE);
        display.setText("TRASFERRED");
    }

    /**
     * When an ougoing call has been refused or timeout
     */
    public void onUaCallFailed(UserAgent ua, String reason) {
//        display.setText("FAILED" + ((reason != null) ? " (" + reason + ")" : ""));
        String show_message = reason;
//        if (reason.equalsIgnoreCase("Forbidden")) {
//            show_message = "User Busy";
//        }
        if (statusIs(UA_OUTGOING_CALL)) {
            if (CallLogHelper.callId != null) {
                CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
//                sec = 0;
            }
        }
        changeStatus(UA_IDLE);
        display.setText(show_message);
    }

    /**
     * When a call has been locally or remotely closed
     */
    public void onUaCallClosed(UserAgent ua) {
        this.setStopDurationCounting(true);
        changeStatus(UA_IDLE);
        display.setText("Call Ended");

        CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));

        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_H));
        if (profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) {
            call_b.setEnabled(true);
        } else {
            call_b.setEnabled(false);
        }
    }

    /**
     * When a new media session is started.
     */
    public void onUaMediaSessionStarted(UserAgent ua, String type, String codec) {  //printLog(type+" started "+codec);
    }

    /**
     * When a media session is stopped.
     */
    public void onUaMediaSessionStopped(UserAgent ua, String type) {  //printLog(type+" stopped");
    }

    /**
     * When registration succeeded.
     */
    public void onUaRegistrationSucceeded(UserAgent ua, String result) {
//        this.setTitle(ua_profile.getUserURI().toString());
        printLog("REGISTRATION SUCCESS: " + result);
    }

    /**
     * When registration failed.
     */
    public void onUaRegistrationFailed(UserAgent ua, String result) {
//        this.setTitle(sip_provider.getContactAddress(ua_profile.user).toString());
        printLog("REGISTRATION FAILURE: " + result);
    }

    // ************************ scheduled events ************************
    /**
     * Schedules a re-inviting after <i>delay_time</i> secs. It simply changes
     * the contact address.
     */
    void reInvite(final int delay_time) {
        printLog("AUTOMATIC RE-INVITING/MODIFING: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.modify(null);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.modify(null);
                }
            };
        }
    }

    /**
     * Schedules a call-transfer after <i>delay_time</i> secs.
     */
    void callTransfer(final NameAddress transfer_to, final int delay_time) {
        printLog("AUTOMATIC REFER/TRANSFER: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.transfer(transfer_to);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.transfer(transfer_to);
                }
            };
        }
    }

    /**
     * Schedules an automatic answer after <i>delay_time</i> secs.
     */
    void automaticAccept(final int delay_time) {
        printLog("AUTOMATIC ANSWER: " + delay_time + " secs");
        if (delay_time == 0) {
            jButton1_actionPerformed();
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    jButton1_actionPerformed();
                }
            };
        }
    }

    /**
     * Schedules an automatic hangup after <i>delay_time</i> secs.
     */
    void automaticHangup(final int delay_time) {
        printLog("AUTOMATIC HANGUP: " + delay_time + " secs");
        if (delay_time == 0) {
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    jButton2_actionPerformed();
                }
            };
        }
    }

    // ******************************* Logs ******************************
    /**
     * Adds a new string to the default Log
     */
    private void printLog(String str) {
        printLog(str, Log.LEVEL_HIGH);
    }

    /**
     * Adds a new string to the default Log
     */
    private void printLog(String str, int level) {
        if (log != null) {
            log.println("GraphicalUA: " + str, UserAgent.LOG_OFFSET + level);
        }
    }

    public void set_reset_call_button(Boolean dd) {
        if (profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) {
            call_p2p.setVisible(dd);
        } else {
            call_p2p.setEnabled(false);
        }
        call_mobile_button.setVisible(dd);
        fnf_lable.setVisible(dd);
        mobile_no_lable.setVisible(dd);
    }

    public void forceHangUp() {
        try {
            jButton2_actionPerformed();
            this.dispose();
        } catch (Exception e) {
        }
    }
}
