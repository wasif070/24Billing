/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.ua;

import helpers.APIParametersFiles;
import helpers.CallLogHelper;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import static helpers.HelperMethods.getImageName_from_url;
import helpers.MyFnFSettings;
import helpers.StaticImages;
import imageDownload.DownLoaderHelps;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.*;
import local.media.RtpConstants;
import myfnfui.DialPad;
import myfnfui.GUI24FnF;
import myfnfui.Messages.NotificationMessages;
import myfnfui.SingleUserProfile;
import org.zoolu.sip.address.*;
import org.zoolu.sip.provider.*;
import org.zoolu.tools.*;

/**
 * Simple GUI-based SIP user agent (UA).
 */
public class GraphicalUA extends JFrame implements UserAgentListener, ActionListener {

    ScheduledWork scheduledWork;
    int posX = 0;
    int posY = 0;
    public JPanel main_container;
    public JPanel topBar;
    public JButton button_window_minimize;
    public JButton button_window_close;
    JLabel label_image;
    JLabel display;
    JLabel duration_label;
    JButton call_b;
    JButton cancel_b;
    JLabel user_Status;
    JLabel friend_name;
    int counterVariable = 1;
    String printDot = "";
    public static GraphicalUA staticObject;
    private static SipProvider sip_provider;
    private String call_type;
    private boolean stopDurationCounting = false;
    private UserAgentProfile profile;
    JPanel dialing_panel;
    public JButton call_p2p;
    JButton call_mobile_button;
    JLabel fnf_lable;
    JLabel mobile_no_lable;
    protected Log log;
    protected UserAgent ua;
    protected UserAgentProfile ua_profile;
    HelperMethods helperMethods = new HelperMethods();
    JLabel friend_image_panel;
    DownLoaderHelps dhelp = new DownLoaderHelps();

    public static GraphicalUA getStaticObject() {
        return staticObject;
    }

    public static void setStaticObject(GraphicalUA staticObject) {
        GraphicalUA.staticObject = staticObject;
    }

    public static void setSipProvider(SipProvider paramSip_provider) {
        sip_provider = paramSip_provider;
    }

    public static SipProvider getSipProvider() {
        return sip_provider;
    }

    private GraphicalUA() {
    }

    public void setStopDurationCounting(boolean stopDurationCounting) {
        this.stopDurationCounting = stopDurationCounting;
    }

    public void reset() {
        sec = 0;
        min = 0;
        hour = 0;
    }
    private int sec = 0;
    private int min = 0;
    private int hour = 0;
    java.text.DecimalFormat nft = new DecimalFormat("#00.###");

    protected String showTime() {
        sec++;
        if (sec == 60) {
            sec = 0;
            min++;
            if (min == 60) {
                hour++;
            }
        }
        return (nft.format(hour) + ":" + nft.format(min) + ":" + nft.format(sec));
    }

    public void CallDurationChecker(final JLabel paramDisplay) {

        final ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                // Invoke method(s) to do the work                
                paramDisplay.setText(doPeriodicWork());
                if (stopDurationCounting) {
                    try {
                        executor.shutdown();
                        reset();
                    } catch (Exception e) {
                    }
                }
            }

            private String doPeriodicWork() {
                return showTime();
            }
        };

        executor.scheduleAtFixedRate(periodicTask,
                2, 1, TimeUnit.SECONDS);

    }
    // ************************* UA internal state *************************
    /**
     * UA_IDLE=0
     */
    public static final String UA_IDLE = "IDLE";
    /**
     * UA_INCOMING_CALL=1
     */
    public static final String UA_INCOMING_CALL = "INCOMING_CALL";
    /**
     * UA_OUTGOING_CALL=2
     */
    public static final String UA_OUTGOING_CALL = "OUTGOING_CALL";
    /**
     * UA_ONCALL=3
     */
    public static final String UA_ONCALL = "ONCALL";
    /**
     * Call state: <P>UA_IDLE=0, <BR>UA_INCOMING_CALL=1, <BR>UA_OUTGOING_CALL=2,
     * <BR>UA_ONCALL=3
     */
    static String call_state = UA_IDLE;

    /**
     * Changes the call state
     */
    protected void changeStatus(String state) {
        call_state = state;
        printLog("state: " + call_state, Log.LEVEL_MEDIUM);
    }

    /**
     * Checks the call state
     */
    protected boolean statusIs(String state) {
        return call_state.equals(state);
    }

    /**
     * Gets the call state
     */
    public static String getStatus() {
        return call_state;
    }

    /**
     * Creates a new GraphicalUA
     */
    public GraphicalUA(SipProvider sip_provider, UserAgentProfile ua_profile, String call_mob_or_p2p) {
        super();
        setStaticObject(this);
        call_type = call_mob_or_p2p;
        profile = ua_profile;
        if (profile != null) {
            this.setTitle(ua_profile.getFirstName() + " " + ua_profile.getLastName());
        } else {
            this.setTitle(ua_profile.getFirstName() + " " + ua_profile.getLastName());
        }
        this.sip_provider = sip_provider;
        this.ua_profile = ua_profile;
        log = sip_provider.getLog();
        this.ua = GUI24FnF.getUserAgent();
        ua.setListener(this);
        changeStatus(UA_IDLE);
        try {
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void jbInit() throws Exception {

        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + 100, DefaultSettings.DEFAULT_LOCATION_TOP + 45);
        this.setSize(DefaultSettings.DEFAULT_WIDTH, 400);
        int start_point = 27;
        int text_width = 180;
        int start_text_point = start_point + 80;
        display = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("Outgoing Call Window", start_text_point, start_point + 35, text_width, 11, 1, Color.WHITE);
        ImageIcon imageIcon = DesignClasses.return_image(StaticImages.ICON_24MYFNF);
        Image image = imageIcon.getImage();
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setIconImage(image);
        this.setTitle("24FnF " + ua_profile.getFirstName() + " " + ua_profile.getLastName());
        this.setSize(DefaultSettings.DEFAULT_WIDTH, DefaultSettings.DEFAULT_HEIGHT / 2 - 150);
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + 380, DefaultSettings.DEFAULT_LOCATION_TOP + 150);
        this.setResizable(false);
        this.setUndecorated(true);
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                //sets frame position when mouse dragged			
                setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);

            }
        });

        addWindowListener(
                new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                HelperMethods.create_confrim_panel(NotificationMessages.CANCEL_NOTIFICAITON, 20);
                if (HelperMethods.confirm == 1) {
                    if (DialPad.buttons[DialPad.call_index] != null) {
                        DialPad.buttons[DialPad.call_index].setEnabled(true);
                    }
                    setStaticObject(null);
                    ua.setListener(GUI24FnF.getObjMain());
                    jButton2_actionPerformed();
                    dispose();
                } else {
                    setVisible(true);
                }
            }
        });
        main_container = (JPanel) getContentPane();
        main_container.setBorder(DefaultSettings.calling_window_border_color);
        main_container.setBackground(DefaultSettings.calling_window_background_color);
        main_container.setLayout(null);
        main_container.add(DesignClasses.create_colored_bottom_border(2, 26, 300, Color.WHITE));
        //
        //  label_image =DesignClasses.create_(sec, posX, min, sec, call_state)
        label_image = new JLabel("", DesignClasses.return_image(StaticImages.IMAGE_callingwindow_bar), JLabel.CENTER);
        label_image.setBackground(null);
        //   label_image.setText("Call Phone");
        topBar = new JPanel(new BorderLayout());

        topBar.add(label_image, BorderLayout.CENTER);
        topBar.setBounds(2, 2, DefaultSettings.DEFAULT_WIDTH - 4, DefaultSettings.DEFAULT_TOP_BAR / 2);
        topBar.setBorder(null);
        String friend_name_or_mobile = "";
        if (call_type.equals(SingleUserProfile.call_to_mobile) && MyFnFSettings.LOGIN_USER_ID.equals(profile.getUserIdentity().toString())) {
            friend_name_or_mobile = profile.getMobilePhone();
            RtpConstants.is_ROUTE_CALL = true;
        } else if (call_type.equals(SingleUserProfile.call_to_mobile) && !MyFnFSettings.LOGIN_USER_ID.equals(profile.getUserIdentity().toString())) {
            friend_name_or_mobile = profile.getUserIdentity() + "(" + profile.getMobilePhone() + ")";
            RtpConstants.is_ROUTE_CALL = false;
        } else {
            RtpConstants.is_ROUTE_CALL = false;
            friend_name_or_mobile = profile.getUserIdentity();
        }
        String bar_title = "Call Phone";
        if (call_type.equals(SingleUserProfile.call_to_mobile)) {
            bar_title = "Call Phone";
        } else {
            friend_name_or_mobile = profile.getUserIdentity();
            bar_title = "24FnF Call";
        }

        JLabel bar_text = DesignClasses.makeJLabel_with_text_color(bar_title, 10, 0, DefaultSettings.DEFAULT_WIDTH - 50, 11, 1, Color.WHITE);

        main_container.add(bar_text);
        int minimize_button_left_possion = 256;
        //  JLabel friend_image_panel = DesignClasses.create_image_label_with_border(10, start_point + 30, 70, 70, frined_image);
        friend_image_panel = helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), 10, start_point + 30, 70, 70);
//helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), 10, start_point + 30, 70, 70);
//        friend_name = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("Calling " + friend_name_or_mobile, start_text_point - 20, start_point + 15, text_width + 40, 14, 1, Color.WHITE);

        friend_name = DesignClasses.makeJLabelCenterAling_with_font_size_width_color(friend_name_or_mobile, start_text_point - 20, start_point + 15, text_width + 40, 14, 1, Color.WHITE);
        main_container.add(friend_image_panel);
        main_container.add(friend_name);
        main_container.add(display);
        duration_label = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("", start_text_point, start_point + 60, text_width, 12, 1, Color.WHITE);
        button_window_close = DesignClasses.create_button_with_image_fixed_location(StaticImages.ICON_CLOSE, StaticImages.ICON_CLOSE_HOVER, "Close Window", minimize_button_left_possion + 22, 2, 20, 20);
        button_window_close.addActionListener(this);
        button_window_minimize = DesignClasses.create_button_with_image_fixed_location(StaticImages.IMAGE_MINIMIZE, StaticImages.IMAGE_MINIMIZE_HOVER, "Minimize", minimize_button_left_possion, 2, 20, 20);
        button_window_minimize.addActionListener(this);
        main_container.add(button_window_minimize);
        call_b = new JButton();
        call_b.setBounds(95, 120, 95, 24);
        call_b.setBorder(null);
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_H));
        call_b.addActionListener(this);
        call_b.setEnabled(false);
        //      call_b.setPressedIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));

        main_container.add(call_b);
        cancel_b = new JButton();
        cancel_b.setBorder(null);
        cancel_b.setBounds(198, 120, 95, 24);
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));
        cancel_b.addActionListener(this);
        main_container.add(cancel_b);
        main_container.add(button_window_close);
        main_container.add(topBar);
        main_container.add(duration_label);

        jButton1_actionPerformed();
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            if (DialPad.buttons[DialPad.call_index] != null) {
                DialPad.buttons[DialPad.call_index].setEnabled(true);
            }
            setStaticObject(null);
            ua.setListener(GUI24FnF.getObjMain());
            jButton2_actionPerformed();
            this.dispose();
        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        } else if (event.getSource() == cancel_b) {
            this.setStopDurationCounting(true);
//            System.out.println("CallLogHelper.callFriend_id  " + CallLogHelper.called_in);
            if (!statusIs(UA_IDLE) && CallLogHelper.called_in.startsWith("+")) {
                if (CallLogHelper.callId != null) {
                    if (scheduledWork != null) {
                        scheduledWork.halt_timer();
                        scheduledWork = null;
                    }
                    MyFnFSettings.userProfile.setBalance((MyFnFSettings.userProfile.getBalance() - (long) ((hour * 3600 + min * 60 + sec) * 1000)));
                    CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
                }
            } else {
                if (profile.getPresence() != null) {
                    if ((profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) || (SingleUserProfile.call_to_mobile.equals(call_type))) {
                        call_b.setEnabled(true);
                        if (statusIs(UA_OUTGOING_CALL)) {
                            if (CallLogHelper.callId != null) {
                                CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
                            }
                        }

                    } else {
                        call_b.setEnabled(false);
                    }
                }
            }
            jButton2_actionPerformed();
        } else if (event.getSource() == call_b) {
            duration_label.setText("");
            call_b.setEnabled(false);
            jButton1_actionPerformed();
        }

    }

    protected void exit() {  // close possible active call before exiting
        jButton2_actionPerformed();
        // exit now
    }

    /**
     * When the call/accept button is pressed.
     */
    void jButton1_actionPerformed() {
        if (statusIs(UA_IDLE)) {
            String url = null;

            if (SingleUserProfile.call_to_pin.equals(call_type)) {
                url = profile.getUserIdentity();
            } else if (SingleUserProfile.call_to_mobile.equals(call_type) && (MyFnFSettings.userProfile.getBalance() / 1000) > 2) {
                url = profile.getMobilePhone();
            }
            if (url != null && url.length() > 0) {
                ua.hangup();
                changeStatus(UA_OUTGOING_CALL);
                // display.setText("Calling " + url);
                display.setText("Dialing ");
                ua.call(url);
                //Integer y = 0;
                long duration = (long) 0;
                if (SingleUserProfile.call_to_pin.equals(call_type)) {
                    CallLogHelper.sent_call_log_to_server_in_call_start(ua_profile.getUserIdentity(), ua_profile.getUserIdentity(), duration * 1000);
                } else if (SingleUserProfile.call_to_mobile.equals(call_type)) {
                    CallLogHelper.sent_call_log_to_server_in_call_start(ua_profile.getUserIdentity(), ua_profile.getMobilePhone(), duration * 1000);
                }
                /* if (ua_profile.hangup_time > 0) {
                 automaticHangup(ua_profile.hangup_time);
                 }*/
            } else {
                HelperMethods.create_confrim_panel_no_cancel("You have reached your call limit for today", 15);
                changeStatus(UA_IDLE);
                display.setText("Daily call limit exceeded");
                call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
                call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
                call_b.setEnabled(true);
                cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
                cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));

            }
        } else if (statusIs(UA_INCOMING_CALL)) {
            this.setStopDurationCounting(false);
            ua.accept();
            changeStatus(UA_ONCALL);
            this.reset();

            this.CallDurationChecker(duration_label);
            display.setText("Call Established");
            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_END));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_END_H));
            call_b.setEnabled(false);
        }
    }

    /**
     * When the refuse/hangup button is pressed.
     */
    void jButton2_actionPerformed() {
//        this.setStopDurationCounting(false);
        if (!statusIs(UA_IDLE)) {
            ua.hangup();
            if (statusIs(UA_ONCALL)) {
                display.setText("Call Ended");
//                reset();
//                if (sec > 0 && CallLogHelper.callId != null) {
//                    CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) sec);
//                    sec = 0;
//
//                }
            } else {
                display.setText("Call Cancelled");
//                if (sec > 0 && CallLogHelper.callId != null) {
//                    CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) sec);
//                    sec = 0;
//
//                }
            }
            changeStatus(UA_IDLE);
            this.setStopDurationCounting(true);
            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));

        }
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_H));
        if (profile.getPresence() != null) {
            if ((profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) || (SingleUserProfile.call_to_mobile.equals(call_type))) {
                call_b.setEnabled(true);
            } else {
                call_b.setEnabled(false);
            }
        } else {
            call_b.setEnabled(true);
        }
        if (GraphicalUA.getStaticObject() != null) {
            GraphicalUA.getStaticObject().setAlwaysOnTop(false);
        }
    }

    /**
     * Gets the UserAgent
     */
    // ********************** UA callback functions **********************
    /**
     * When a new call is incoming
     */
    public void onUaIncomingCall(UserAgent ua, NameAddress callee, NameAddress caller, Vector media_descs) {

//--------------------------------------------------------------------------------------

        String[] temp = caller.getAddress().toString().split("@");
        String temp_var = temp[0].substring(temp[0].indexOf("|") + 1, temp[0].length());
        profile = HelperMethods.get_all_for_a_profile(temp_var);

        friend_name.setText(profile.getUserIdentity());
        BufferedImage img = null;
        String location = DesignClasses.get_male_or_female_image(profile.getGender());
        // helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), 10, start_point + 30, 70, 70);
        if (profile.getProfileImage() != null && profile.getProfileImage().length() > 3) {
            String image_name = getImageName_from_url(profile.getProfileImage());
            //   System.out.println("******************************" + image_name);
            File f = new File(dhelp.getDestinationFolder() + "\\" + image_name);
            if (f.exists()) {
                location = dhelp.getUser_images_location() + image_name;
            } else {
                location = DesignClasses.get_scaled_male_or_female_image(profile.getGender());
            }
        } else {
            location = DesignClasses.get_scaled_male_or_female_image(profile.getGender());
        }

        img = HelperMethods.scalled_image(location, 70, 70);
        friend_image_panel.setIcon(new ImageIcon(img));
//---------------------------------------------------------------------------------------        




        changeStatus(UA_INCOMING_CALL);

        GraphicalUA.getStaticObject().toFront();
        GraphicalUA.getStaticObject().setAlwaysOnTop(true);
        GraphicalUA.getStaticObject().setState(Frame.NORMAL);
        display.setText("");
        duration_label.setText("");
        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_ANSWER_H));
        call_b.setEnabled(true);

//        String[] temp = caller.getAddress().toString().split("@");
//        String temp_var = temp[0].substring(temp[0].indexOf("|") + 1, temp[0].length());

//        profile = HelperMethods.get_all_for_a_profile(var[1]);
//        profile = HelperMethods.get_all_for_a_profile(temp_var);
        if (ua_profile.redirect_to != null) // redirect the call
        {
            display.setText("Call redirected to " + ua_profile.redirect_to);
            ua.redirect(ua_profile.redirect_to);
        } else if (ua_profile.accept_time >= 0) // automatically accept the call
        {
            display.setText("Call Established");
            automaticAccept(ua_profile.accept_time);
        } else {
//            friend_name.setText(profile.getFirstName() + " " + profile.getLastName());
            final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
            Runnable periodicTask = new Runnable() {
                public void run() {
                    if (!statusIs(UA_INCOMING_CALL)) {
                        try {
                            executor.shutdown();
                            counterVariable = 1;
                        } catch (Exception e) {
                        }
                    }
                    switch (counterVariable) {
                        case 1:
                            printDot = ".  ";
                            break;
                        case 2:
                            printDot = ".. ";
                            break;
                        case 3:
                            printDot = "...";
                            break;

                    }
                    if (statusIs(UA_INCOMING_CALL)) {
                        display.setText("Incoming Call" + printDot);
                    }
                    counterVariable++;
                    if (counterVariable > 3) {
                        counterVariable = 1;
                    }

                }
            };

            executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);
            cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE));
            cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL_DECLINE_H));
            set_reset_call_button(false);
        }
    }

    /**
     * When an ougoing call is stated to be in progress
     */
    public void onUaCallProgress(UserAgent ua) {
        display.setText("Progress");
    }

    /**
     * When an ougoing call is remotly ringing
     */
    public void onUaCallRinging(UserAgent ua) {
        final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask = new Runnable() {
            public void run() {
                if (!statusIs(UA_OUTGOING_CALL)) {
                    try {
                        executor.shutdown();
                        counterVariable = 1;
                    } catch (Exception e) {
                    }
                }

                switch (counterVariable) {
                    case 1:
                        printDot = ".  ";
                        break;
                    case 2:
                        printDot = ".. ";
                        break;
                    case 3:
                        printDot = "...";
                        break;
                }

                counterVariable++;
                if (counterVariable > 3) {
                    counterVariable = 1;
                }

                if (statusIs(UA_OUTGOING_CALL)) {
                    display.setText("Ringing" + printDot);
                }
            }
        };

        executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);
    }

    /**
     * When an ougoing call has been accepted
     */
    public void onUaCallAccepted(UserAgent ua) {
        this.setStopDurationCounting(false);
        this.reset();
        this.CallDurationChecker(duration_label);
        changeStatus(UA_ONCALL);
        display.setText("Call Established");
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_END));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_END_H));
        if (SingleUserProfile.call_to_mobile.equals(call_type) && (MyFnFSettings.userProfile.getBalance() / 1000) > 2) {
            automaticHangup((int) (MyFnFSettings.userProfile.getBalance() / 1000));
        }
    }

    /**
     * When an incoming call has been cancelled
     */
    public void onUaCallCancelled(UserAgent ua) {
        changeStatus(UA_IDLE);
        display.setText("Cancelled");

        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));

    }

    /**
     * When a call has been transferred
     */
    public void onUaCallTransferred(UserAgent ua) {
        changeStatus(UA_IDLE);
        display.setText("Transferred");
    }

    /**
     * When an ougoing call has been refused or timeout
     */
    public void onUaCallFailed(UserAgent ua, String reason) {
        //display.setText("FAILED" + ((reason != null) ? " (" + reason + ")" : ""));
        String show_message = reason;

        if (statusIs(UA_OUTGOING_CALL)) {
            if (CallLogHelper.callId != null) {
                CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
            }
        }
        changeStatus(UA_IDLE);
        display.setText(show_message);
    }

    /**
     * When a call has been locally or remotely closed
     */
    public void onUaCallClosed(UserAgent ua) {
        this.setStopDurationCounting(true);
        changeStatus(UA_IDLE);
        display.setText("Call Ended");
        MyFnFSettings.userProfile.setBalance((MyFnFSettings.userProfile.getBalance() - (long) ((hour * 3600 + min * 60 + sec) * 1000)));

        if (scheduledWork != null) {
            scheduledWork.halt_timer();
            scheduledWork = null;
        }

        CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
//        sec = 0;


        call_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        call_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CALL));
        if ((profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) || (SingleUserProfile.call_to_mobile.equals(call_type))) {
            call_b.setEnabled(true);
        } else {
            call_b.setEnabled(false);
        }
        cancel_b.setIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL));
        cancel_b.setRolloverIcon(DesignClasses.return_image(StaticImages.IMAGE_CANCEL_H));

    }

    /**
     * When a new media session is started.
     */
    public void onUaMediaSessionStarted(UserAgent ua, String type, String codec) {  //printLog(type+" started "+codec);
    }

    /**
     * When a media session is stopped.
     */
    public void onUaMediaSessionStopped(UserAgent ua, String type) {  //printLog(type+" stopped");
    }

    /**
     * When registration succeeded.
     */
    public void onUaRegistrationSucceeded(UserAgent ua, String result) {
//        this.setTitle(ua_profile.getUserURI().toString());
        printLog("REGISTRATION SUCCESS: " + result);
    }

    /**
     * When registration failed.
     */
    public void onUaRegistrationFailed(UserAgent ua, String result) {
//        this.setTitle(sip_provider.getContactAddress(ua_profile.user).toString());
        printLog("REGISTRATION FAILURE: " + result);
    }

    // ************************ scheduled events ************************
    /**
     * Schedules a re-inviting after <i>delay_time</i> secs. It simply changes
     * the contact address.
     */
    void reInvite(final int delay_time) {
        printLog("AUTOMATIC RE-INVITING/MODIFING: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.modify(null);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.modify(null);
                }
            };
        }
    }

    /**
     * Schedules a call-transfer after <i>delay_time</i> secs.
     */
    void callTransfer(final NameAddress transfer_to, final int delay_time) {
        printLog("AUTOMATIC REFER/TRANSFER: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.transfer(transfer_to);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.transfer(transfer_to);
                }
            };
        }
    }

    /**
     * Schedules an automatic answer after <i>delay_time</i> secs.
     */
    void automaticAccept(final int delay_time) {
        printLog("AUTOMATIC ANSWER: " + delay_time + " secs");
        if (delay_time == 0) {
            jButton1_actionPerformed();
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    jButton1_actionPerformed();
                }
            };
        }
    }

    /**
     * Schedules an automatic hangup after <i>delay_time</i> secs.
     */
    void automaticHangup(final int delay_time) {
        printLog("AUTOMATIC HANGUP: " + delay_time + " secs");
        if (delay_time == 0) {
            jButton2_actionPerformed();
        } else {
            scheduledWork = new ScheduledWork(delay_time * 1000) {
                public void doWork() {
//                    halt_timer();
                    MyFnFSettings.userProfile.setBalance((MyFnFSettings.userProfile.getBalance() - (long) ((hour * 3600 + min * 60 + sec) * 1000)));
                    CallLogHelper.sent_call_log_to_server_in_call_end(CallLogHelper.callFriend_id, CallLogHelper.called_in, (long) ((hour * 3600 + min * 60 + sec) * 1000));
                    jButton2_actionPerformed();
                }
            };
        }
    }

    // ******************************* Logs ******************************
    /**
     * Adds a new string to the default Log
     */
    private void printLog(String str) {
        printLog(str, Log.LEVEL_HIGH);
    }

    /**
     * Adds a new string to the default Log
     */
    private void printLog(String str, int level) {
        if (log != null) {
            log.println("GraphicalUA: " + str, UserAgent.LOG_OFFSET + level);
        }
    }

    public void set_reset_call_button(Boolean dd) {
        call_p2p.setVisible(dd);
        call_mobile_button.setVisible(dd);
        fnf_lable.setVisible(dd);
        mobile_no_lable.setVisible(dd);
    }

    public void forceHangUp() {
        jButton2_actionPerformed();
        this.dispose();
    }
}
