/*
 * Copyright (C) 2010 Luca Veltri - University of Parma - Italy
 * 
 * This source code is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This source code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this source code; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package local.media;

import local.net.RtpPacket;
import local.net.RtpSocket;
//import java.net.*;
import org.zoolu.net.*;

import java.io.InputStream;
import org.zoolu.sound.G729A;

/**
 * RtpStreamSender is a generic RTP sender. It takes media from a given
 * InputStream and sends it through RTP packets to a remote destination.
 */
public class RtpStreamSender extends Thread {

    /**
     * Whether working in debug mode.
     */
    public static boolean DEBUG = false;
    /**
     * The InputStream
     */
    InputStream input_stream = null;
    /**
     * The RtpSocket
     */
    static RtpSocket rtp_socket = null;
    /**
     * Whether the socket has been created here
     */
    boolean socket_is_local_attribute = false;
    /**
     * Payload type
     */
    int p_type;
    /**
     * Number of frame per second
     */
    long frame_rate;
    /**
     * Number of bytes per frame
     */
    int frame_size_in_byte;
    /**
     * Whether it works synchronously with a local clock, or it it acts as slave
     * of the InputStream
     */
    boolean do_sync = true;
    /**
     * Synchronization correction value, in milliseconds. It accellarates the
     * sending rate respect to the nominal value, in order to compensate program
     * latencies.
     */
    int sync_adj = 0;
    /**
     * Whether it is running
     */
    boolean running = false;
    private int FRAME_SIZE = 320;

    /**
     * Constructs a RtpStreamSender.
     *
     * @param input_stream the stream source
     * @param do_sync whether time synchronization must be performed by the
     * RtpStreamSender, or it is performed by the InputStream (e.g. by the
     * system audio input)
     * @param payload_type the payload type
     * @param frame_rate the frame rate, i.e. the number of frames that should
     * be sent per second; it is used to calculate the nominal packet time and,
     * in case of do_sync==true, the next departure time
     * @param frame_size_in_byte the size of the payload
     * @param dest_addr the destination address
     * @param dest_port the destination port
     */
    public RtpStreamSender(InputStream input_stream, boolean do_sync, int payload_type, long frame_rate, int frame_size, String dest_addr, int dest_port) {
        init(input_stream, do_sync, payload_type, frame_rate, frame_size, null, dest_addr, dest_port);
    }

    /**
     * Constructs a RtpStreamSender.
     *
     * @param input_stream the stream source
     * @param do_sync whether time synchronization must be performed by the
     * RtpStreamSender, or it is performed by the InputStream (e.g. by the
     * system audio input)
     * @param payload_type the payload type
     * @param frame_rate the frame rate, i.e. the number of frames that should
     * be sent per second; it is used to calculate the nominal packet time and,
     * in case of do_sync==true, the next departure time
     * @param frame_size_in_byte the size of the payload
     * @param src_port the source port
     * @param dest_addr the destination address
     * @param dest_port the destination port
     */
    //public RtpStreamSender(InputStream input_stream, boolean do_sync, int payload_type, long frame_rate, int frame_size_in_byte, int src_port, String dest_addr, int dest_port)
    //{  init(input_stream,do_sync,payload_type,frame_rate,frame_size_in_byte,null,src_port,dest_addr,dest_port);
    //}                
    /**
     * Constructs a RtpStreamSender.
     *
     * @param input_stream the stream to be sent
     * @param do_sync whether time synchronization must be performed by the
     * RtpStreamSender, or it is performed by the InputStream (e.g. by the
     * system audio input)
     * @param payload_type the payload type
     * @param frame_rate the frame rate, i.e. the number of frames that should
     * be sent per second; it is used to calculate the nominal packet time and,
     * in case of do_sync==true, the next departure time
     * @param frame_size_in_byte the size of the payload
     * @param src_socket the socket used to send the RTP packet
     * @param dest_addr the destination address
     * @param dest_port the destination port
     */
    public RtpStreamSender(InputStream input_stream, boolean do_sync, int payload_type, long frame_rate, int frame_size, UdpSocket src_socket, String dest_addr, int dest_port) {
        init(input_stream, do_sync, payload_type, frame_rate, frame_size, src_socket, dest_addr, dest_port);
    }

    /**
     * Inits the RtpStreamSender
     */
    private void init(InputStream input_stream, boolean do_sync, int payload_type, long frame_rate, int frame_size, UdpSocket src_socket, /*int src_port,*/ String dest_addr, int dest_port) {
        this.input_stream = input_stream;
        this.p_type = payload_type;
        this.frame_rate = frame_rate;
        this.frame_size_in_byte = frame_size;
        this.do_sync = do_sync;
        try {
            if (src_socket == null) {  //if (src_port>0) src_socket=new UdpSocket(src_port); else
                src_socket = new UdpSocket(0);
                socket_is_local_attribute = true;
            }
            rtp_socket = new RtpSocket(src_socket, IpAddress.getByName(dest_addr), dest_port);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the local port.
     */
    public int getLocalPort() {
        if (rtp_socket != null) {
            return rtp_socket.getUdpSocket().getLocalPort();
        } else {
            return 0;
        }
    }

    /**
     * Changes the remote socket address.
     */
    public static void setRemoteSoAddress(SocketAddress remote_soaddr) {
        if (remote_soaddr != null && rtp_socket != null) {
            //System.out.println("Setting Address-->" + remote_soaddr.getAddress() + ":" + remote_soaddr.getPort());
            try {
                rtp_socket = new RtpSocket(rtp_socket.getUdpSocket(), IpAddress.getByName(remote_soaddr.getAddress().toString()), remote_soaddr.getPort());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Gets the remote socket address.
     */
    public SocketAddress getRemoteSoAddress() {
        if (rtp_socket != null) {
            return new SocketAddress(rtp_socket.getRemoteAddress().toString(), rtp_socket.getRemotePort());
        } else {
            return null;
        }
    }

    /**
     * Sets the synchronization adjustment time (in milliseconds).
     */
    public void setSyncAdj(int millisecs) {
        sync_adj = millisecs;
    }

    /**
     * Whether is running
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * Stops running
     */
    public void halt() {
        running = false;
    }

    /**
     * Runs it in a new Thread.
     */
    public void run() {
        //System.out.println("RTP Sender has been started");
        if (rtp_socket == null || input_stream == null) {
            return;
        }
        //else
        frame_size_in_byte = FRAME_SIZE;
        byte[] packet_buffer = new byte[RtpConstants.RTP_HEADER_LENGTH + (frame_size_in_byte / 160) * 10];
        RtpPacket rtp_packet = new RtpPacket(packet_buffer, packet_buffer.length);
//        rtp_packet.setHeader(p_type);
        int seqn = 0;
        long time = 0;

        running = true;
        if (DEBUG) {
            println("RTP: localhost:" + rtp_socket.getUdpSocket().getLocalPort() + " --> " + rtp_socket.getRemoteAddress().toString() + ":" + rtp_socket.getRemotePort());
        }
        if (DEBUG) {
            println("RTP: sending pkts of " + (packet_buffer.length - RtpConstants.RTP_HEADER_LENGTH) + " bytes of RTP payload");
        }

        try {
            G729A g729 = new G729A();
            while (running) {
                byte[] read_bytes = new byte[frame_size_in_byte];
                int num = read(input_stream, read_bytes, 0, frame_size_in_byte);
                if (num > 0) {
                    try {
                        num = g729.encode(byteArrayToShortArray(read_bytes), 0, packet_buffer, frame_size_in_byte / 2);
                    } catch (Exception ex) {
                    }

//                    rtp_packet.setSequenceNumber(seqn++);
//                    rtp_packet.setTimestamp(time);
//                    rtp_packet.setPayloadLength(num);
//                    if (RtpConstants.is_ROUTE_CALL) {
//                        rtp_packet.setRouteCallFlag(1);
//                    }

//                    System.out.println("Sender rtp_packet.getHeaderLength() ==> " + rtp_packet.getHeaderLength());
                    rtp_socket.send(rtp_packet);
                    // update rtp timestamp (in milliseconds)
                    time += frame_size_in_byte / 2;

                } else if (num < 0) {
                    if (DEBUG) {
                        println("Error reading from InputStream");
                    }
                    running = false;
                }
            }
        } catch (Exception e) {
            running = false;
            e.printStackTrace();
        }

        //if (DEBUG) println("rtp time:  "+time);
        //if (DEBUG) println("real time: "+(System.currentTimeMillis()-start_time));

        // close RtpSocket and local UdpSocket
        UdpSocket socket = rtp_socket.getUdpSocket();
        rtp_socket.close();
        if (socket_is_local_attribute && socket != null) {
            socket.close();
        }

        // free all
        input_stream = null;
        rtp_socket = null;

        if (DEBUG) {
            println("rtp sender terminated");
        }
    }

    /**
     * Reads a block of bytes from an InputStream and put it into a given
     * buffer. This method is used by the RtpStreamSender to compose RTP
     * packets, and can be re-defined by a class that extends RtpStreamSender in
     * order to implement new RTP encoding mechanisms.
     *
     * @return It returns the number of bytes read.
     */
    protected int read(InputStream input_stream, byte[] buff, int off, int len) throws Exception {
        return input_stream.read(buff, off, len);
    }

    /**
     * Debug output
     */
    private static void println(String str) {
        System.err.println("RtpStreamSender: " + str);
    }

    public static short[] byteArrayToShortArray(byte[] byteArray) {
        short[] short_array = new short[byteArray.length / 2];
        for (int i = 0; i < short_array.length; i++) {
            short_array[i] = (short) (((int) byteArray[2 * i + 1] << 8) + (int) byteArray[2 * i]);
        }
        return short_array;
    }
}
