package org.zoolu.sound;

import dialer.codecG729.Encoder;
import dialer.codecG729.Decoder;
import local.media.RtpConstants;
import local.media.RtpStreamSender;

/**
 * Encodes/decodes g729a packets.
 */
public class G729A implements Codec {

    private Encoder encoder = new Encoder();
    private Decoder decoder = new Decoder();

    public G729A() {
    }

    //len should be # bytes not samples
    public byte[] encode(short samples[], int off, int len) {
        byte[] encoded = new byte[len / 80 * 10 + 12];
        encoder.encode(encoded, 12, samples, off, len / 80);
        return encoded;
    }

    public int encode(short audio_stream[], int offset, byte buffer[], int frame_size) {
        encoder.encode(buffer, RtpConstants.RTP_HEADER_LENGTH, audio_stream, offset, frame_size / 80);
        return (frame_size / 80) * 10;
    }

    private int getuint32(byte[] data, int offset) {
        int ret;
        ret = (int) data[offset + 3] & 0xff;
        ret += ((int) data[offset + 2] & 0xff) << 8;
        ret += ((int) data[offset + 1] & 0xff) << 16;
        ret += ((int) data[offset + 0] & 0xff) << 24;
        return ret;
    }
    private int decode_timestamp;

    public short[] decode(byte encoded[], int off, int len) {
        if (len < 12) {
            return null;
        }
        int decode_timestamp = getuint32(encoded, 4);
        if (this.decode_timestamp == 0) {
            this.decode_timestamp = decode_timestamp;
        } else {
            this.decode_timestamp = decode_timestamp;
        }
        short[] samples = new short[(len - 12) / 10 * 80];
        decoder.decode(samples, 0, encoded, 12, (len - 12) / 10);
        return samples;
    }

    public int decode(byte encoded_data[], short audio_stream[], int frame_size) {
        short temp[] = new short[(frame_size / 10) * 80];
        decoder.decode(temp, 0, encoded_data, RtpConstants.RTP_HEADER_LENGTH, frame_size / 10);
        for (int i = 0; i < temp.length; i++) {
            audio_stream[i] = temp[i];
        }
        return (frame_size / 10) * 80;
    }

    public static void main(String[] args) {
        G729A codec = new G729A();
        byte[] byteArray = new byte[172];
        for (int i = 0; i < 172; i++) {
            byteArray[i] = (byte) i;
        }

        byte[] encoded = codec.encode(RtpStreamSender.byteArrayToShortArray(byteArray), 12, 160);
        //System.out.println("encoded byte array-->" + Arrays.toString(encoded));


        //System.out.println("byte array-->" + Arrays.toString(byteArray));
        //System.out.println("byte to short array-->" + Arrays.toString(RtpStreamSender.byteArrayToShortArray(byteArray)));
        //System.out.println("short to byte array-->" + Arrays.toString(RtpStreamReceiver.shortArrayToByteArray(RtpStreamSender.byteArrayToShortArray(byteArray))));
    }
}
