/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.message;

import myfnfui.headers.HeaderConstants;
import myfnfui.headers.HeaderContainer;

/**
 * Class BaseSipMethods collects all SIP method names.
 */
public abstract class BaseSipMethods {

    /**
     * String "INVITE"
     */
    static {
        INVITE = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.INVITE);// HeaderLoader.getInstance().getHeader("INVITE");
        ACK = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.ACK);// HeaderLoader.getInstance().getHeader("ACK");
        CANCEL = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.CANCEL);
        BYE = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.BYE);
        INFO = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.INFO);
        OPTIONS = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.OPTIONS);
        REGISTER = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.REGISTER);
        UPDATE = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.UPDATE);
    }
    public static String INVITE;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.INVITE);// HeaderLoader.getInstance().getHeader("INVITE");

    /**
     * Whether <i>str</i> is INVITE
     */
    public static boolean isInvite(String str) {
        return same(str, INVITE);
    }
    /**
     * String "ACK"
     */
    public static String ACK;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.ACK);// HeaderLoader.getInstance().getHeader("ACK");

    /**
     * Whether <i>str</i> is ACK
     */
    public static boolean isAck(String str) {
        return same(str, ACK);
    }
    /**
     * String "CANCEL"
     */
    public static String CANCEL;//  HeaderLoader.getInstance().getHeader("CANCEL");

    /**
     * Whether <i>str</i> is CANCEL
     */
    public static boolean isCancel(String str) {
        return same(str, CANCEL);
    }
    /**
     * String "BYE"
     */
    public static String BYE;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.BYE);//  HeaderLoader.getInstance().getHeader("BYE");

    /**
     * Whether <i>str</i> is BYE
     */
    public static boolean isBye(String str) {
        return same(str, BYE);
    }
    /**
     * String "INFO"
     */
    public static String INFO;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.INFO);//  HeaderLoader.getInstance().getHeader("INFO");

    /**
     * Whether <i>str</i> is INFO
     */
    public static boolean isInfo(String str) {
        return same(str, INFO);
    }
    /**
     * String "OPTIONS"
     */
    public static String OPTIONS;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.OPTIONS);// HeaderLoader.getInstance().getHeader("OPTIONS");

    /**
     * Whether <i>str</i> is OPTIONS
     */
    public static boolean isOptions(String str) {
        return same(str, OPTIONS);
    }
    /**
     * String "REGISTER"
     */
    public static String REGISTER;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.REGISTER);//  HeaderLoader.getInstance().getHeader("REGISTER");

    /**
     * Whether <i>str</i> is REGISTER
     */
    public static boolean isRegister(String str) {
        return same(str, REGISTER);
    }
    /**
     * String "UPDATE"
     */
    public static String UPDATE;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.UPDATE);//  HeaderLoader.getInstance().getHeader("UPDATE");

    /**
     * Whether <i>str</i> is UPDATE
     */
    public static boolean isUpdate(String str) {
        return same(str, UPDATE);
    }

    /**
     * Whether <i>s1</i> and <i>s2</i> are case-unsensitive-equal.
     */
    protected static boolean same(String s1, String s2) {  //return s1.compareToIgnoreCase(s2)==0;
        return s1.equalsIgnoreCase(s2);
    }
    /**
     * Array of standard methods
     */
    public static final String[] methods = {INVITE, ACK, CANCEL, BYE, INFO, OPTIONS, REGISTER, UPDATE};
    /**
     * Array of standards methods that creates a dialog
     */
    public static final String[] dialog_methods = {INVITE};
}
