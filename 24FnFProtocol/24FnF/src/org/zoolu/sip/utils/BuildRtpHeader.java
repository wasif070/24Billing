/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import org.zoolu.tools.Random;

/**
 *
 * @author reefat
 */
public class BuildRtpHeader {
    private int seqnum = 0;
    private int timestamp = 0;
    private int ssrc = -1;
    private static Random r = new Random();
    private static Object rlock = new Object();
    
    public int getseqnum() {
        return seqnum++;
    }

    public int gettimestamp() {
        int ret = timestamp;
        timestamp += 160;
        return ret;
    }

    public int getssrc() {
        if (ssrc != -1) {
            return ssrc;
        }
        synchronized (rlock) {
            ssrc = r.nextInt();
        }
        return ssrc;
    }

    public byte[] buildHeader(int type, int seqnum, int timestamp, int ssrc) {
        //build RTP header
            byte[] data = new byte[12];
            data[0] = (byte) 0x80;  //version
            data[1] = (byte) type;  //0=g711 18=g729a 101=RFC2833 etc.
            data[2] = (byte) ((seqnum & 0xff00) >> 8);
            data[3] = (byte) (seqnum & 0xff);
            data[4] = (byte) ((timestamp & 0xff000000) >>> 24);
            data[5] = (byte) ((timestamp & 0xff0000) >> 16);
            data[6] = (byte) ((timestamp & 0xff00) >> 8);
            data[7] = (byte) (timestamp & 0xff);
            data[8] = (byte) ((ssrc & 0xff000000) >>> 24);
            data[9] = (byte) ((ssrc & 0xff0000) >> 16);
            data[10] = (byte) ((ssrc & 0xff00) >> 8);
            data[11] = (byte) (ssrc & 0xff);
            
            return data;
    }

    public static void main(String[] args) throws Exception {
//        int len = 160;
        byte[] encoded = new byte[12]; //for g729a
        System.out.println("///"+new String(encoded));
        BuildRtpHeader buildRtpHeader = new BuildRtpHeader();
        encoded = buildRtpHeader.buildHeader(18, buildRtpHeader.getseqnum(), buildRtpHeader.gettimestamp(), buildRtpHeader.getssrc());
        System.out.println("///"+encoded[11]);
        System.out.println(""+((encoded[0] & 0xFF) >> 6));
//        System.out.println(""+new String(encoded));
    }
}
