/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.zoolu.sip.utils;

import java.util.StringTokenizer;
import org.zoolu.sip.address.SipURL;
import org.zoolu.sip.provider.SipParser;

/**
 *
 * @author reefat
 */
public class IpConverter {

    public static long getIPVal(String ipStr) {
        String[] ip = ipStr.split("\\.");
        long ipVal = 0;
        for (int i = 0; i < 4; i++) {
            ipVal <<= 8;
            ipVal |= Integer.parseInt(ip[i]);
        }
        ipVal -= 2147483647;
        return ipVal;
    }

    public static String longToIp(long l) {
        l += 2147483647;
        return ((l >> 24) & 0xFF) + "."
                + ((l >> 16) & 0xFF) + "."
                + ((l >> 8) & 0xFF) + "."
                + (l & 0xFF);
    }

    public static String convertUrlIpToLong(String url) {
        int counter = 0;
        String hdr = "";
        String user_ip = "";
        String port = "";
        StringTokenizer st = new StringTokenizer(url.toString(), SipParser.header_colon);
        while (st.hasMoreTokens()) {
            counter++;
            if (counter == 1) {
                hdr = st.nextToken();
            } else if (counter == 2) {
                user_ip = st.nextToken();
                if(user_ip.contains("@")){
                    String temp [] = user_ip.split("@");
                    user_ip = temp[0] + "@" + String.valueOf(getIPVal(temp[1]));
                }else{
                user_ip = String.valueOf(getIPVal(st.nextToken()));
                }
            } else {
                port = st.nextToken();
            }
        }
        url = hdr + SipParser.header_colon + user_ip + SipParser.header_colon + port;
        return url;
    }

    public static void main(String[] args) {
        System.out.println("getIPVal ==>  " + getIPVal("192.168.1.95"));
        System.out.println("hudai ==>  " + longToIp(Long.valueOf("-1502847858")));

        String delims = SipParser.header_colon;

        StringTokenizer st = new StringTokenizer("1654|sadad|asdsd", delims);
        System.out.println("No of Token = " + st.countTokens());

        while (st.hasMoreTokens()) {
            System.out.println(st.nextToken());
        }

    }
}
