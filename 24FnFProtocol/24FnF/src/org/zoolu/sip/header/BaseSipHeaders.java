/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.header;

import myfnfui.headers.HeaderConstants;
import myfnfui.headers.HeaderContainer;

/**
 * SipHeaders simply collects all standard SIP header names.
 */
public abstract class BaseSipHeaders {

    static {
        Accept = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Accept);// HeaderLoader.getInstance().getHeader("Accept");
        /**
         * String "Alert-Info"
         */
        Alert_Info = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Alert_Info);//  HeaderLoader.getInstance().getHeader("Alert-Info");
        /**
         * String "Allow"
         */
        Allow = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Allow);// HeaderLoader.getInstance().getHeader("Allow");
        /**
         * String "Authentication-Info"
         */
        Authentication_Info = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Authentication_Info);// HeaderLoader.getInstance().getHeader("Authentication-Info");
        /**
         * String "Authorization"
         */
        Authorization = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Authorization);//  HeaderLoader.getInstance().getHeader("Authorization");
        /**
         * String "Call-ID"
         */
        Call_ID = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Call_ID);// HeaderLoader.getInstance().getHeader("Call-ID");
        /**
         * String "i"
         */
        Call_ID_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.i);// HeaderLoader.getInstance().getHeader("i");
        /**
         * String "Contact"
         */
        Contact = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Contact);// HeaderLoader.getInstance().getHeader("Contact");
        /**
         * String "m"
         */
        Contact_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.m);//  HeaderLoader.getInstance().getHeader("m");
        /**
         * String "Content-Length"
         */
        Content_Length = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Content_Length);// HeaderLoader.getInstance().getHeader("Content-Length");
        /**
         * String "l"
         */
        Content_Length_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.l);// HeaderLoader.getInstance().getHeader("l");
        /**
         * String "Content-Type"
         */
        Content_Type = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Content_Type);//  HeaderLoader.getInstance().getHeader("Content-Type");
        /**
         * String "c"
         */
        Content_Type_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.c);// HeaderLoader.getInstance().getHeader("c");
        /**
         * String "CSeq"
         */
        CSeq = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.CSeq);// HeaderLoader.getInstance().getHeader("CSeq");
        /**
         * String "Date"
         */
        Date = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Date);// HeaderLoader.getInstance().getHeader("Date");
        /**
         * String "Expires"
         */
        Expires = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Expires);// HeaderLoader.getInstance().getHeader("Expires");
        /**
         * String "From"
         */
        From = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.From);// HeaderLoader.getInstance().getHeader("From");
        /**
         * String "f"
         */
        From_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.f);//  HeaderLoader.getInstance().getHeader("f");
        /**
         * String "User-Agent"
         */
        User_Agent = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.User_Agent);//  HeaderLoader.getInstance().getHeader("User-Agent");
        /**
         * String "Max-Forwards"
         */
        Max_Forwards = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Max_Forwards);//  HeaderLoader.getInstance().getHeader("Max-Forwards");
        /**
         * String "Proxy-Authenticate"
         */
        Proxy_Authenticate = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Authenticate);//  HeaderLoader.getInstance().getHeader("Proxy-Authenticate");
        /**
         * String "Proxy-Authorization"
         */
        Proxy_Authorization = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Authorization);// HeaderLoader.getInstance().getHeader("Proxy-Authorization");
        /**
         * String "Proxy-Require"
         */
        Proxy_Require = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Require);//  HeaderLoader.getInstance().getHeader("Proxy-Require");
        /**
         * String "Record-Route"
         */
        Record_Route = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Record_Route);// HeaderLoader.getInstance().getHeader("Record-Route");
        /**
         * String "Require"
         */
        Require = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Require);//  HeaderLoader.getInstance().getHeader("Require");
        /**
         * String "Route"
         */
        Route = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Route);//  HeaderLoader.getInstance().getHeader("Route");
        /**
         * String "Server"
         */
        Server = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Server);//  HeaderLoader.getInstance().getHeader("Server");
        /**
         * String "Subject"
         */
        Subject = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Subject);// HeaderLoader.getInstance().getHeader("Subject");
        /**
         * String "s"
         */
        Subject_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.s);//  HeaderLoader.getInstance().getHeader("s");
        /**
         * String "Supported"
         */
        Supported = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Supported);//  HeaderLoader.getInstance().getHeader("Supported");
        /**
         * String "k"
         */
        Supported_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.k);// HeaderLoader.getInstance().getHeader("k");
        /**
         * String "To"
         */
        To = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.To);// HeaderLoader.getInstance().getHeader("To");
        /**
         * String "t"
         */
        To_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.t);// HeaderLoader.getInstance().getHeader("t");
        /**
         * String "Unsupported"
         */
        Unsupported = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Unsupported);// HeaderLoader.getInstance().getHeader("Unsupported");
        /**
         * String "Via"
         */
        Via = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Via);// HeaderLoader.getInstance().getHeader("Via");
        /**
         * String "v"
         */
        Via_short = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.v);// HeaderLoader.getInstance().getHeader("v");
        /**
         * String "WWW-Authenticate"
         */
        WWW_Authenticate = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.WWW_Authenticate);//  HeaderLoader.getInstance().getHeader("WWW-Authenticate");

    }
    /**
     * String "Accept"
     */
    public static String Accept;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Accept);// HeaderLoader.getInstance().getHeader("Accept");
    /**
     * String "Alert-Info"
     */
    public static String Alert_Info;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Alert_Info);//  HeaderLoader.getInstance().getHeader("Alert-Info");
    /**
     * String "Allow"
     */
    public static String Allow;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Allow);// HeaderLoader.getInstance().getHeader("Allow");
    /**
     * String "Authentication-Info"
     */
    public static String Authentication_Info;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Authentication_Info);// HeaderLoader.getInstance().getHeader("Authentication-Info");
    /**
     * String "Authorization"
     */
    public static String Authorization;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Authorization);//  HeaderLoader.getInstance().getHeader("Authorization");
    /**
     * String "Call-ID"
     */
    public static String Call_ID;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Call_ID);// HeaderLoader.getInstance().getHeader("Call-ID");
    /**
     * String "i"
     */
    public static String Call_ID_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.i);// HeaderLoader.getInstance().getHeader("i");
    /**
     * String "Contact"
     */
    public static String Contact;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Contact);// HeaderLoader.getInstance().getHeader("Contact");
    /**
     * String "m"
     */
    public static String Contact_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.m);//  HeaderLoader.getInstance().getHeader("m");
    /**
     * String "Content-Length"
     */
    public static String Content_Length;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Content_Length);// HeaderLoader.getInstance().getHeader("Content-Length");
    /**
     * String "l"
     */
    public static String Content_Length_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.l);// HeaderLoader.getInstance().getHeader("l");
    /**
     * String "Content-Type"
     */
    public static String Content_Type;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Content_Type);//  HeaderLoader.getInstance().getHeader("Content-Type");
    /**
     * String "c"
     */
    public static String Content_Type_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.c);// HeaderLoader.getInstance().getHeader("c");
    /**
     * String "CSeq"
     */
    public static String CSeq;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.CSeq);// HeaderLoader.getInstance().getHeader("CSeq");
    /**
     * String "Date"
     */
    public static String Date;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Date);// HeaderLoader.getInstance().getHeader("Date");
    /**
     * String "Expires"
     */
    public static String Expires;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Expires);// HeaderLoader.getInstance().getHeader("Expires");
    /**
     * String "From"
     */
    public static String From;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.From);// HeaderLoader.getInstance().getHeader("From");
    /**
     * String "f"
     */
    public static String From_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.f);//  HeaderLoader.getInstance().getHeader("f");
    /**
     * String "User-Agent"
     */
    public static String User_Agent;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.User_Agent);//  HeaderLoader.getInstance().getHeader("User-Agent");
    /**
     * String "Max-Forwards"
     */
    public static String Max_Forwards;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Max_Forwards);//  HeaderLoader.getInstance().getHeader("Max-Forwards");
    /**
     * String "Proxy-Authenticate"
     */
    public static String Proxy_Authenticate;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Authenticate);//  HeaderLoader.getInstance().getHeader("Proxy-Authenticate");
    /**
     * String "Proxy-Authorization"
     */
    public static String Proxy_Authorization;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Authorization);// HeaderLoader.getInstance().getHeader("Proxy-Authorization");
    /**
     * String "Proxy-Require"
     */
    public static String Proxy_Require;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Proxy_Require);//  HeaderLoader.getInstance().getHeader("Proxy-Require");
    /**
     * String "Record-Route"
     */
    public static String Record_Route;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Record_Route);// HeaderLoader.getInstance().getHeader("Record-Route");
    /**
     * String "Require"
     */
    public static String Require;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Require);//  HeaderLoader.getInstance().getHeader("Require");
    /**
     * String "Route"
     */
    public static String Route;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Route);//  HeaderLoader.getInstance().getHeader("Route");
    /**
     * String "Server"
     */
    public static String Server;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Server);//  HeaderLoader.getInstance().getHeader("Server");
    /**
     * String "Subject"
     */
    public static String Subject;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Subject);// HeaderLoader.getInstance().getHeader("Subject");
    /**
     * String "s"
     */
    public static String Subject_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.s);//  HeaderLoader.getInstance().getHeader("s");
    /**
     * String "Supported"
     */
    public static String Supported;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Supported);//  HeaderLoader.getInstance().getHeader("Supported");
    /**
     * String "k"
     */
    public static String Supported_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.k);// HeaderLoader.getInstance().getHeader("k");
    /**
     * String "To"
     */
    public static String To;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.To);// HeaderLoader.getInstance().getHeader("To");
    /**
     * String "t"
     */
    public static String To_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.t);// HeaderLoader.getInstance().getHeader("t");
    /**
     * String "Unsupported"
     */
    public static String Unsupported;// = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Unsupported);// HeaderLoader.getInstance().getHeader("Unsupported");
    /**
     * String "Via"
     */
    public static String Via;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.Via);// HeaderLoader.getInstance().getHeader("Via");
    /**
     * String "v"
     */
    public static String Via_short;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.v);// HeaderLoader.getInstance().getHeader("v");
    /**
     * String "WWW-Authenticate"
     */
    public static String WWW_Authenticate;//  = HeaderContainer.getInstance().getHeaders().get(HeaderConstants.WWW_Authenticate);//  HeaderLoader.getInstance().getHeader("WWW-Authenticate");

    /**
     * Whether <i>s1</i> and <i>s2</i> are case-unsensitive-equal.
     */
    protected static boolean same(String s1, String s2) { //return s1.compareToIgnoreCase(s2)==0;
        return s1.equalsIgnoreCase(s2);
    }

    /**
     * Whether <i>str</i> is a Accept field
     */
    public static boolean isAccept(String str) {
        return same(str, Accept);
    }

    /**
     * Whether <i>str</i> is a Alert_Info field
     */
    public static boolean isAlert_Info(String str) {
        return same(str, Alert_Info);
    }

    /**
     * Whether <i>str</i> is a Allow field
     */
    public static boolean isAllow(String str) {
        return same(str, Allow);
    }

    /**
     * Whether <i>str</i> is a Authentication_Info field
     */
    public static boolean isAuthentication_Info(String str) {
        return same(str, Authentication_Info);
    }

    /**
     * Whether <i>str</i> is a Authorization field
     */
    public static boolean isAuthorization(String str) {
        return same(str, Authorization);
    }

    /**
     * Whether <i>str</i> is a Call-ID field
     */
    public static boolean isCallId(String str) {
        return same(str, Call_ID) || same(str, Call_ID_short);
    }

    /**
     * Whether <i>str</i> is a Contact field
     */
    public static boolean isContact(String str) {
        return same(str, Contact) || same(str, Contact_short);
    }

    /**
     * Whether <i>str</i> is a Content_Length field
     */
    public static boolean isContent_Length(String str) {
        return same(str, Content_Length) || same(str, Content_Length_short);
    }

    /**
     * Whether <i>str</i> is a Content_Type field
     */
    public static boolean isContent_Type(String str) {
        return same(str, Content_Type) || same(str, Content_Type_short);
    }

    /**
     * Whether <i>str</i> is a CSeq field
     */
    public static boolean isCSeq(String str) {
        return same(str, CSeq);
    }

    /**
     * Whether <i>str</i> is a Date field
     */
    public static boolean isDate(String str) {
        return same(str, Date);
    }

    /**
     * Whether <i>str</i> is a Expires field
     */
    public static boolean isExpires(String str) {
        return same(str, Expires);
    }

    /**
     * Whether <i>str</i> is a From field
     */
    public static boolean isFrom(String str) {
        return same(str, From) || same(str, From_short);
    }

    /**
     * Whether <i>str</i> is a User_Agent field
     */
    public static boolean isUser_Agent(String str) {
        return same(str, User_Agent);
    }

    /**
     * Whether <i>str</i> is a Max_Forwards field
     */
    public static boolean isMax_Forwards(String str) {
        return same(str, Max_Forwards);
    }

    /**
     * Whether <i>str</i> is a Proxy_Authenticate field
     */
    public static boolean isProxy_Authenticate(String str) {
        return same(str, Proxy_Authenticate);
    }

    /**
     * Whether <i>str</i> is a Proxy_Authorization field
     */
    public static boolean isProxy_Authorization(String str) {
        return same(str, Proxy_Authorization);
    }

    /**
     * Whether <i>str</i> is a Proxy_Require field
     */
    public static boolean isProxy_Require(String str) {
        return same(str, Proxy_Require);
    }

    /**
     * Whether <i>str</i> is a Record_Route field
     */
    public static boolean isRecord_Route(String str) {
        return same(str, Record_Route);
    }

    /**
     * Whether <i>str</i> is a Require field
     */
    public static boolean isRequire(String str) {
        return same(str, Require);
    }

    /**
     * Whether <i>str</i> is a Route field
     */
    public static boolean isRoute(String str) {
        return same(str, Route);
    }

    /**
     * Whether <i>str</i> is a Server field
     */
    public static boolean isServer(String str) {
        return same(str, Server);
    }

    /**
     * Whether <i>str</i> is a Subject field
     */
    public static boolean isSubject(String str) {
        return same(str, Subject) || same(str, Subject_short);
    }

    /**
     * Whether <i>str</i> is a Supported field
     */
    public static boolean isSupported(String str) {
        return same(str, Supported) || same(str, Supported_short);
    }

    /**
     * Whether <i>str</i> is a To field
     */
    public static boolean isTo(String str) {
        return same(str, To) || same(str, To_short);
    }

    /**
     * Whether <i>str</i> is a Unsupported field
     */
    public static boolean isUnsupported(String str) {
        return same(str, Unsupported);
    }

    /**
     * Whether <i>str</i> is a Via field
     */
    public static boolean isVia(String str) {
        return same(str, Via) || same(str, Via_short);
    }

    /**
     * Whether <i>str</i> is a WWW_Authenticate field
     */
    public static boolean isWWW_Authenticate(String str) {
        return same(str, WWW_Authenticate);
    }
}
