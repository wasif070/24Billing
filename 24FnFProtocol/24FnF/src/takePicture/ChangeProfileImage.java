/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package takePicture;

import com.lti.civil.CaptureDeviceInfo;
import com.lti.civil.CaptureException;
import com.lti.civil.CaptureObserver;
import com.lti.civil.CaptureStream;
import com.lti.civil.CaptureSystem;
import com.lti.civil.CaptureSystemFactory;
import com.lti.civil.DefaultCaptureSystemFactorySingleton;
import com.lti.civil.awt.AWTImageConverter;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGImageEncoder;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.StaticImages;
import imageDownload.DownLoaderHelps;
import imageUpload.ImageUploader;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author user
 */
public class ChangeProfileImage implements CaptureObserver {

    //  public static String whatsInMind;
    //   HelperMethods.ImageFilter1 fJavaFilter = new HelperMethods.ImageFilter1();
    ImageFilter1 fJavaFilter = new ImageFilter1();
    JLabel profile_picture;
    static File fFile;
    BufferedImage img = null;
    static File oldfile;
    int image_panle_size = 300;
    ImageUploader img_up = new ImageUploader();
    DownLoaderHelps dHelp = new DownLoaderHelps();
    /*camere*/
    public static CaptureStream captureStream = null;
    public static boolean takeShot = false;
    boolean take_in_panel = true;
    JPanel panel_profile_pic;
    JButton take_photo;
    JButton upload_botton;
    JButton use_pic_botton;
    List camare_list;

    public void change_profile_pic(String current_image) {
        CaptureSystemFactory factory = DefaultCaptureSystemFactorySingleton.instance();
        CaptureSystem system;
        try {
            system = factory.createCaptureSystem();
            system.init();
            camare_list = system.getCaptureDeviceInfoList();
            int i = 0;
            if (i < camare_list.size()) {
                CaptureDeviceInfo info = (CaptureDeviceInfo) camare_list.get(i);
                //  System.out.println((new StringBuilder()).append("Device ID ").append(i).append(": ").append(info.getDeviceID()).toString());
                // System.out.println((new StringBuilder()).append("Description ").append(i).append(": ").append(info.getDescription()).toString());
                captureStream = system.openCaptureDeviceStream(info.getDeviceID());
                captureStream.setObserver(ChangeProfileImage.this);
            }
        } catch (CaptureException ex) {
            ex.printStackTrace();
        }
        int size = image_panle_size + 100;
        final JDialog dialog_pp = HelperMethods.create_joption_panel(size, size, "Change profile picture");
        int margin_left = 45;
        int margin_top = 5;
        int label_width = 100;

        panel_profile_pic = new JPanel();
        panel_profile_pic.setLayout(null);
        final JLabel error_text_label = DesignClasses.makeJLabel_with_text_color("", margin_left, margin_top, 100, 11, 0, Color.RED);

        String location = current_image;
        img = HelperMethods.scalled_image(location, image_panle_size + 30, image_panle_size);
        profile_picture = new JLabel(new ImageIcon((Image) img));
        profile_picture.setBounds(margin_left - 5, margin_top + 30, image_panle_size + 30, image_panle_size);
        profile_picture.setBorder(DefaultSettings.thinBorder);
        panel_profile_pic.add(profile_picture);
        upload_botton = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Upload", 12, 1, (size / 2) - 50, margin_top = margin_top + image_panle_size + 40, 74, 22);
        take_photo = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Take Photo", 12, 1, (size / 2) + 30, margin_top, 74, 22);
        use_pic_botton = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Use Photo", 12, 1, (size / 2) + 30, margin_top, 74, 22);
        //take_photo.setEnabled(false);
        use_pic_botton.setVisible(false);
        if (camare_list.size() > 0) {
            try {
                captureStream.start();
                //System.out.println("Camara On");
            } catch (CaptureException ex) {
            }
            takeShot = true;
        } else {
            take_photo.setEnabled(false);
        }
        panel_profile_pic.add(use_pic_botton);
        panel_profile_pic.add(upload_botton);
        panel_profile_pic.add(take_photo);
        // panel.add(button);
        panel_profile_pic.add(error_text_label);
        //JButton[] buttons = {button};
        take_photo.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                take_photo.setVisible(false);
                use_pic_botton.setVisible(true);
                takeShot = false;
            }
        });
        use_pic_botton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                upload_botton.setEnabled(false);
                try {
                    captureStream.stop();
                    // profile_repaint();
                } catch (CaptureException ex) {
                    Logger.getLogger(HelperMethods.class.getName()).log(Level.SEVERE, null, ex);
                }
                //  takeShot = false;
                img_up.uploader_method(MyFnFSettings.temp_image_folder + File.separator + "img_test.jpg");
                dialog_pp.dispose();

            }
        });
        //img_up.uploader_method(fFile.getAbsolutePath());
        upload_botton.addActionListener(new ActionListener() {

            private String last_str;

            @Override
            public void actionPerformed(ActionEvent e) {
                boolean status = false;
                if (camare_list.size() > 0) {
                    takeShot = false;
                    try {
                        captureStream.stop();
                        // profile_repaint();
                    } catch (CaptureException ex) {
                        Logger.getLogger(HelperMethods.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                try {
                    status = openFile();
                    dialog_pp.dispose();
                } catch (IOException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }

            }
        });
        dialog_pp.getContentPane().add(panel_profile_pic);

        dialog_pp.setVisible(true);
    }

    public boolean openFile() throws IOException {
        JFileChooser fc = new JFileChooser();
        JFrame dd = new JFrame();
        fc.setDialogTitle("Open File");
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.setCurrentDirectory(new File("."));
        fc.setFileFilter(fJavaFilter);
        int result = fc.showOpenDialog(dd);
        if (result == JFileChooser.CANCEL_OPTION) {
            return true;
        } else if (result == JFileChooser.APPROVE_OPTION) {

            fFile = fc.getSelectedFile();
            oldfile = fFile;
            //    System.out.println(fFile);
            //    System.out.println(fileInputStream);
            //    System.out.println(fFile.getAbsolutePath());
            // img = ImageIO.read(fileInputStream);
            String location = MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.LOADING_IMAGE_SCALED;
            img_up.uploader_method(fFile.getAbsolutePath());
            //img = scalled_image(fFile.getAbsolutePath(), image_panle_size, image_panle_size);
            img = HelperMethods.scalled_image(location, image_panle_size, image_panle_size);
            profile_picture.setIcon(new ImageIcon(img));
//            label = new JLabel(new ImageIcon(icon));
//            label.setBounds(0, 0, 500, 500);
//            label.setVisible(true);
//            getContentPane().add(label);
//            // Set the position of its text, relative to its icon:
//            label.setVerticalTextPosition(JLabel.BOTTOM);
//            label.setHorizontalTextPosition(JLabel.CENTER);
        } else {
            return false;
        }
        return true;
    }

    @Override
    public void onNewImage(CaptureStream stream, com.lti.civil.Image image) {
        if (!takeShot) {
            return;
        }
        //    takeShot = false;
        //    System.out.println("New Image Captured");
        byte bytes[] = null;
        try {
            if (image == null) {
                bytes = null;
                return;
            }
            try {
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                JPEGImageEncoder jpeg = JPEGCodec.createJPEGEncoder(os);
                jpeg.encode(AWTImageConverter.toBufferedImage(image));
                os.close();
                bytes = os.toByteArray();
            } catch (IOException e) {
                e.printStackTrace();
                bytes = null;
            } catch (Throwable t) {
                t.printStackTrace();
                bytes = null;
            }
            if (bytes == null) {
                return;
            }
            ByteArrayInputStream is = new ByteArrayInputStream(bytes);
            File file = new File(MyFnFSettings.temp_image_folder + File.separator + "img_test.jpg");
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bytes);
            fos.close();
            //   System.out.println("Destination:" + file);
            BufferedImage myImage = ImageIO.read(file);
            //   profile_picture = new JLabel(new ImageIcon((Image) myImage));
            profile_picture.setIcon(new ImageIcon(myImage));
            panel_profile_pic.add(profile_picture);
            panel_profile_pic.revalidate();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onError(CaptureStream stream, CaptureException ce) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    class ImageFilter1 extends javax.swing.filechooser.FileFilter {

        public boolean accept(File f) {
            return f.getName().toLowerCase().endsWith(".png")
                    || f.getName().toLowerCase().endsWith(".jpg")
                    || f.getName().toLowerCase().endsWith(".jif")
                    || f.isDirectory();
        }

        public String getDescription() {
            return "Image files (*.png)";
        }
    }
}
