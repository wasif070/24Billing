/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

/**
 *
 * @author faizahmed
 */
public class JsonFields extends FeedBackFields {

    private String sessionId;
    private String userIdentity;
    //  private String packetId;
    // private Long packetIdFromServer;
    private String oldPass;
    private String newPass;
    private String whatisInYourMind;
    private String noOfHeaders;
    private String gender;
    private String firstName;
    private String lastName;
    private String mobilePhoneDialingCode;
    private String country;
    private String email;
    private String mobilePhone;
    private String friendId;
    private String phoneNo;
    private Long duration;
    private Long callingTime;
    private String callId;
    private String userName;
    private String password;
    private String version;
    private String searchParam;
//    private Boolean success;
//    private String message;
    private String switchIp;
    private Integer switchPort;
    private Integer presence;
    private Integer friendShipStatus;
    private String profileImage;
    private int[] indexOfHeaders;
    private Long groupId;
    private String versionMessage;
    private Boolean downloadMandatory;
    private Long balance;
    private Integer device;

    public Integer getDevice() {
        return device;
    }

    public void setDevice(Integer device) {
        this.device = device;
    }

    public Long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public Boolean getDownloadMandatory() {
        return downloadMandatory;
    }

    public void setDownloadMandatory(Boolean downloadMandatory) {
        this.downloadMandatory = downloadMandatory;
    }

    public String getVersionMessage() {
        return versionMessage;
    }

    public void setVersionMessage(String versionMessage) {
        this.versionMessage = versionMessage;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

//    public long getGroupId() {
//        return groupId;
//    }
//
//    public void setGroupId(long groupId) {
//        this.groupId = groupId;
//    }
    public int[] getIndexOfHeaders() {
        return indexOfHeaders;
    }

    public void setIndexOfHeaders(int[] indexOfHeaders) {
        this.indexOfHeaders = indexOfHeaders;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getFriendShipStatus() {
        return friendShipStatus;
    }

    public void setFriendShipStatus(Integer friendShipStatus) {
        this.friendShipStatus = friendShipStatus;
    }

    public Integer getPresence() {
        return presence;
    }

    public void setPresence(Integer presence) {
        this.presence = presence;
    }

    public String getSwitchIp() {
        return switchIp;
    }

    public void setSwitchIp(String switchIp) {
        this.switchIp = switchIp;
    }

    public Integer getSwitchPort() {
        return switchPort;
    }

    public void setSwitchPort(Integer switchPort) {
        this.switchPort = switchPort;
    }

//    public Boolean getSuccess() {
//        return success;
//    }
//
//    public void setSuccess(Boolean success) {
//        this.success = success;
//    }
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
//
////    public long getPacketIdFromServer() {
////        return packetIdFromServer;
////    }
////
////    public void setPacketIdFromServer(long packetIdFromServer) {
////        this.packetIdFromServer = packetIdFromServer;
////    }
//
//    public String getAction() {
//        return action;
//    }
//
//    public void setAction(String action) {
//        this.action = action;
//    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

//    public String getPacketId() {
//        return packetId;
//    }
//
//    public void setPacketId(String packetId) {
//        this.packetId = packetId;
//    }
    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getWhatisInYourMind() {
        return whatisInYourMind;
    }

    public void setWhatisInYourMind(String whatisInYourMind) {
        this.whatisInYourMind = whatisInYourMind;
    }

    public String getNoOfHeaders() {
        return noOfHeaders;
    }

    public void setNoOfHeaders(String noOfHeaders) {
        this.noOfHeaders = noOfHeaders;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobilePhoneDialingCode() {
        return mobilePhoneDialingCode;
    }

    public void setMobilePhoneDialingCode(String mobilePhoneDialingCode) {
        this.mobilePhoneDialingCode = mobilePhoneDialingCode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getCallingTime() {
        return callingTime;
    }

    public void setCallingTime(Long callingTime) {
        this.callingTime = callingTime;
    }

//    public String getDuration() {
//        return duration;
//    }
//
//    public void setDuration(String duration) {
//        this.duration = duration;
//    }
//
//    public String getCallingTime() {
//        return callingTime;
//    }
//
//    public void setCallingTime(String callingTime) {
//        this.callingTime = callingTime;
//    }
    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getSearchParam() {
        return searchParam;
    }

    public void setSearchParam(String searchParam) {
        this.searchParam = searchParam;
    }
}
