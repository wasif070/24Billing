/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.tampClass;

/**
 *
 * @author reefat
 */
public class PresenceDto {

    private String action;
    private int[] indexOfHeaders;
    private String userIdentity;
    private String firstName;
    private String lastName;
    private String gender;
    private String country;
    private String mobilePhone;
    private String email;
    private String presence;
    private String whatisInYourMind;
    private String friendShipStatus;
    private String message;
    private Long packetIdFromServer;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int[] getIndexOfHeaders() {
        return indexOfHeaders;
    }

    public void setIndexOfHeaders(int[] indexOfHeaders) {
        this.indexOfHeaders = indexOfHeaders;
    }

    public String getUserIdentity() {
        return userIdentity;
    }

    public void setUserIdentity(String userIdentity) {
        this.userIdentity = userIdentity;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPresence() {
        return presence;
    }

    public void setPresence(String presence) {
        this.presence = presence;
    }

    public String getWhatisInYourMind() {
        return whatisInYourMind;
    }

    public void setWhatisInYourMind(String whatisInYourMind) {
        this.whatisInYourMind = whatisInYourMind;
    }

    public String getFriendShipStatus() {
        return friendShipStatus;
    }

    public void setFriendShipStatus(String friendShipStatus) {
        this.friendShipStatus = friendShipStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getPacketIdFromServer() {
        return packetIdFromServer;
    }

    public void setPacketIdFromServer(Long packetIdFromServer) {
        this.packetIdFromServer = packetIdFromServer;
    }
}
