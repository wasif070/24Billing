/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.tampClass;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author user
 */
public class TempStorageOfContactList {

    public static TempStorageOfContactList responses_in_map;

    public static TempStorageOfContactList getInstance() {
        if (responses_in_map == null) {
            responses_in_map = new TempStorageOfContactList();
        }
        return responses_in_map;
    }
    private Map<String, String> response_msg_container = new ConcurrentHashMap<String, String>();

    public Map<String, String> getResponse_msg_container() {
        return response_msg_container;
    }

    public void setResponse_msg_container(Map<String, String> response_msg_container) {
        this.response_msg_container = response_msg_container;
    }
    
}
