/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.tampClass;

import myfnfui.FeedBackFields;

/**
 *
 * @author faizahmed
 */
public class CommunicationPortMapping extends FeedBackFields {

    private Integer communicationPort;

    public Integer getCommunicationPort() {
        return communicationPort;
    }

    public void setCommunicationPort(Integer communicationPort) {
        this.communicationPort = communicationPort;
    }
}
