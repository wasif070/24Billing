package myfnfui;

import java.util.ArrayList;
import local.ua.UserAgentProfile;

public class UserProfile extends UserAgentProfile {

    private UserAgentProfile uniqueProfile = null;

    public UserAgentProfile getUniqueProfile() {
        return uniqueProfile;
    }

    public void setUniqueProfile(UserAgentProfile uniqueProfile) {
        this.uniqueProfile = uniqueProfile;
    }
    private ArrayList<UserAgentProfile> contactList = null;

    public ArrayList<UserAgentProfile> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<UserAgentProfile> contactList) {
        this.contactList = contactList;
    }
}
