
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import myfnfui.usedThreads.SetDynamicFriend;
import com.google.gson.Gson;
import helpers.GetResponseJsons;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Properties;
import javax.swing.*;
import helpers.EmailValidator;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.InternetConnection;
import helpers.SearchItem;
import helpers.SocketConstants;
import helpers.StaticFields;
import helpers.StaticImages;
import helpers.MyFnFSettings;
import myfnfui.usedThreads.SearchThread;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import local.ua.UserAgent;
import local.ua.UserAgentProfile;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.packetsInfo.PackedDistributor;
import org.zoolu.sip.provider.SipProvider;
import utils.UserNameAvailabilityQueue;

/**
 *
 * @author FaizAhmed
 */
public class CreateNewAccount extends MainFrame implements PackedDistributor, ActionListener, KeyListener {

    JLabel errorLabel_new;
    JLabel firstNameLable = null;
    JLabel lastNameLable = null;
    JLabel userIDLable = null;
    JLabel passwordLable = null;
    JLabel conPasswordLable = null;
    JLabel emailLable = null;
    JLabel mobileNoLable = null;
    JLabel countryLable = null;
    JLabel genderLable = null;
    JLabel country_prefixLable = null;
    /*
     * dsfsdf
     */
    JTextField firstNameTextField = null;
    JTextField lastNameTextField = null;
    JTextField userIDTextField = null;
    JPasswordField passwordTextField = null;
    JPasswordField conPasswordTextField = null;
    JTextField emailTextField = null;
    JTextField country_prefixTextField = null;
    JTextField mobileNoTextField = null;
    JComboBox countryComboBox = null;
    JComboBox genderComboBox = null;
    JComboBox country_prefix_ComboBox = null;
    JButton save_button;
    JButton reset_button;
    String countryDefault = "Bangladesh";
    //  String singup_error_type = "";
    EmailValidator email;
    String save_fistname = "s_fname";
    String save_lastname = "s_lname";
    String save_userid = "s_uid";
    String save_email = "s_email";
    String save_mobileno = "s_mobile";
    String save_country = "s_country";
    String save_gender = "s_gender";
    GetResponseJsons jsonObj;
    public static CreateNewAccount newUserClass;
    public static GUI24FnF mainuserUi;
    public static Main loginPanel;
    // NewUserRegister registerCheck;
    String msg = "";
    public boolean isNetworkAvailable = false;
    public boolean isAuthenticated = false;
    java.util.Timer newUertimer;
    SendNewUserRequst newuserObj;
    String c_prefix = "";
    public static int prefix_created = 0;
// String c_prefix = "";
//    public static int prefix_created = 0;
    protected UserAgent ua;
    protected UserAgentProfile ua_profile;
    protected SipProvider sip_provider;
//error labels
    JLabel userID_error_label;
    JLabel first_name_error_label;
    JLabel last_name_error_label;
    JLabel passwd_error_label;
    JLabel conf_passwd_error_label;
    JLabel email_error_label;
    JLabel country_error_label;
    JLabel mobile_number_error_label;
    JLabel gender_error_label;
    int align = 4;
    int counter = 0;
    private boolean stopTimer = false;
    private boolean success = false;
    private boolean internet_check_flag = false;
    private boolean com_port_check = false;
    private boolean packed_send = false;
    int com_port = 0;
    String bd_mobile;
    PackageActions pak_action = new PackageActions();

    public void setStopTimer(boolean stopTimer) {
        this.stopTimer = stopTimer;
    }

    public static CreateNewAccount getNewUserClass() {
        return newUserClass;
    }

    public static void setNewUserClass(CreateNewAccount newUserClass) {
        CreateNewAccount.newUserClass = newUserClass;
    }

    public CreateNewAccount(SipProvider sip_provider, UserAgentProfile ua_profile, UserAgent ua) {
        super();
        SearchThread th = new SearchThread();
        SearchThread.startSearchThread();
        th.start();
        /*new */
        loginPanel = new Main(sip_provider, ua_profile);
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT, DefaultSettings.DEFAULT_LOCATION_TOP);
        this.ua = ua;
        this.sip_provider = sip_provider;
        this.ua_profile = ua_profile;
        /*new */
        setNewUserClass(this);
        int loginpanelstart = 70;
        int labelLeft = 15;
        int labelText = 120;
        int error_label_height = 12;
        int top_increment = 28 + error_label_height;
        int label_width = 95;
        Color label_color = Color.WHITE;
        int label_font_width = 1;
        int label_font_size = 11;
        int label_font_aling = 2;
        Color label_text_color = DefaultSettings.text_color1;

        /*only test purpose*/
        boolean main_server = false;

        String default_user_id_test = "f_test";
        String pass_to_test = "123456";
        String first_name = "Test";
        String last_name = "24FnF_PC";
        String email_test = "test24fnf@ipvision.ca";
        String test_mobile = "1728119927";
        if (main_server) {
            default_user_id_test = "";
            pass_to_test = "";
            first_name = "";
            last_name = "";
            email_test = "";
            test_mobile = "";
        }

        /**/
        JLabel dd = DesignClasses.makeJLabel_with_background_font_size_width_color("Sign Up ", labelLeft, loginpanelstart - 5, 125, 20, 12, 1, Color.BLACK, label_color, label_font_aling);

        main_container.add(dd);
        main_container.add(DesignClasses.create_bottom_border(labelLeft, loginpanelstart + 15, 250));
        email = new EmailValidator();
        errorLabel_new = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelText, loginpanelstart + 18, 150, error_label_height + 5, 11, 1, Color.BLUE, label_color, label_font_aling);
        main_container.add(errorLabel_new);

        userIDLable = DesignClasses.make_label_for_field_text(StaticFields.MY_FNF_NAME_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);

        main_container.add(userIDLable);
        userIDTextField = DesignClasses.makeTextField(default_user_id_test, labelText, loginpanelstart, DefaultSettings.textBoxWidth);
        userIDTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void removeUpdate(DocumentEvent e) {
                if (userIDTextField.getText().length() >= 4) {
                    errorLabel_new.setText("");
                    get_availability(userIDTextField.getText().toString());
                } else {
                    set_user_id();
                }
                //errorLabel_new.setText("");
            }

            public void insertUpdate(DocumentEvent e) {
                if (userIDTextField.getText().length() >= 4) {
                    errorLabel_new.setText("");
                    get_availability(userIDTextField.getText().toString());
                } else {
                    set_user_id();
                }

            }

            public void changedUpdate(DocumentEvent e) {
                if (userIDTextField.getText().length() >= 4) {
                    errorLabel_new.setText("");
                    get_availability(userIDTextField.getText().toString());
                } else {
                    set_user_id();
                }
            }
        });


        main_container.add(userIDTextField);
        userID_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelLeft, loginpanelstart + 26, 250, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(userID_error_label);


        firstNameLable = DesignClasses.make_label_for_field_text(StaticFields.FIRST_NAME_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(firstNameLable);
        firstNameTextField = DesignClasses.makeTextField(first_name, labelText, loginpanelstart, DefaultSettings.textBoxWidth);

        main_container.add(firstNameTextField);
        first_name_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelText, loginpanelstart + 26, DefaultSettings.textBoxWidth, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(first_name_error_label);


        lastNameLable = DesignClasses.make_label_for_field_text(StaticFields.LAST_NAME_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(lastNameLable);
        lastNameTextField = DesignClasses.makeTextField(last_name, labelText, loginpanelstart, DefaultSettings.textBoxWidth);
        main_container.add(lastNameTextField);
        last_name_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelText, loginpanelstart + 26, DefaultSettings.textBoxWidth, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(last_name_error_label);

        passwordLable = DesignClasses.make_label_for_field_text(StaticFields.PASSWORD_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(passwordLable);
        passwordTextField = DesignClasses.makeJPasswordField(pass_to_test, labelText, loginpanelstart);
        main_container.add(passwordTextField);
        passwd_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelLeft + 55, loginpanelstart + 26, 200, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(passwd_error_label);

        conPasswordLable = DesignClasses.make_label_for_field_text(StaticFields.CONF_PASSWORD_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width + 10);
        main_container.add(conPasswordLable);
        conPasswordTextField = DesignClasses.makeJPasswordField(pass_to_test, labelText, loginpanelstart);
        main_container.add(conPasswordTextField);
        conf_passwd_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelLeft + 55, loginpanelstart + 26, 200, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(conf_passwd_error_label);

        emailLable = DesignClasses.make_label_for_field_text(StaticFields.EMAIL_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(emailLable);
        emailTextField = DesignClasses.makeTextField(email_test, labelText, loginpanelstart, DefaultSettings.textBoxWidth);
        main_container.add(emailTextField);
        email_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelLeft + 55, loginpanelstart + 26, 200, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(email_error_label);


        countryLable = DesignClasses.make_label_for_field_text(StaticFields.COUNTRY_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(countryLable);
        countryComboBox = DesignClasses.create_ComboBox_from_array(HelperMethods.get_only_country_list(), labelText, loginpanelstart, DefaultSettings.country_combox_width);
        countryComboBox.setSelectedItem("Bangladesh");
        countryComboBox.setEditable(true);
        countryComboBox.addActionListener(this);
        main_container.add(countryComboBox);
        country_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelText, loginpanelstart + 26, DefaultSettings.textBoxWidth, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(country_error_label);

        country_prefixLable = DesignClasses.make_label_for_field_text(StaticFields.PHONE_NO_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(country_prefixLable);
        country_prefixTextField = DesignClasses.makeTextField("+880", labelText, loginpanelstart, DefaultSettings.DEFAULT_PREFIX_TEXT_BOX_WIDTH);
        main_container.add(country_prefixTextField);
        mobileNoTextField = DesignClasses.makeTextField(test_mobile, labelText + DefaultSettings.DEFAULT_PREFIX_TEXT_BOX_WIDTH + 5, loginpanelstart, DefaultSettings.textBoxWidth - (DefaultSettings.DEFAULT_PREFIX_TEXT_BOX_WIDTH + 5));
        main_container.add(mobileNoTextField);
        mobile_number_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelLeft + 55, loginpanelstart + 26, 200, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(mobile_number_error_label);

        genderLable = DesignClasses.make_label_for_field_text(StaticFields.GENDER_TEXT + "*", labelLeft, loginpanelstart = loginpanelstart + top_increment, label_width);
        main_container.add(genderLable);
        genderComboBox = DesignClasses.create_ComboBox_from_array(MyFnFSettings.GENDER_ARRAY, labelText, loginpanelstart, 80);
        main_container.add(genderComboBox);
        gender_error_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", labelText, loginpanelstart + 26, DefaultSettings.textBoxWidth, error_label_height, 10, 2, Color.RED, label_color, align);
        main_container.add(gender_error_label);

        save_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Sign Up", 11, 1, labelText - 8, loginpanelstart + top_increment, 74, 22);
        save_button.addActionListener(this);
        save_button.addKeyListener(this);
        main_container.add(save_button);

        reset_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_5, StaticImages.IMAGE_Scale_5_h, "Cancel", 12, 1, 2, DefaultSettings.DEFAULT_HEIGHT - 47, DefaultSettings.DEFAULT_WIDTH - 4, 45);
        reset_button.addActionListener(this);
        main_container.add(reset_button);


        addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                // read_signup_info();
                userIDTextField.requestFocus();
            }
        });

    }

    public void get_availability(String search_string) {
        SearchItem item = new SearchItem();
        item.setSearch_string(search_string);
        item.setSearch_time(System.currentTimeMillis());
        item.setCreate_new_account(this);
        UserNameAvailabilityQueue.getInstance().push(item);
    }

    public void set_availability(String response, String search_string) {
        if (search_string.length() > 3) {
            if (HelperMethods.check_string_contains_substring(response, "true")) {
                errorLabel_new.setForeground(Color.blue);
                errorLabel_new.setText(StaticFields.MY_FNF_NAME_TEXT + " is available ");
            } else {
                errorLabel_new.setForeground(Color.red);
                errorLabel_new.setText(StaticFields.MY_FNF_NAME_TEXT + " is not available ");
            }
        } else {
            errorLabel_new.setText("");
        }
    }

    public void reset_fields() {
        firstNameTextField.setText("");
        lastNameTextField.setText("");
        userIDTextField.setText("");
        passwordTextField.setText("");
        conPasswordTextField.setText("");
        emailTextField.setText("");
        mobileNoTextField.setText("");
        countryComboBox.setSelectedItem(countryDefault);
        genderComboBox.setSelectedItem("Male");
        save_button.setEnabled(true);
        firstNameTextField.requestFocus();
        save_button.setEnabled(true);
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        actionSource = event.getSource();
        if (actionSource == button_window_close) {
            loginPanel.showLogin_panel();
            this.dispose();
        } else if (actionSource == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        } else if (actionSource == reset_button) {
//            reset_fields();
            SearchThread.stopSearchThread();
            loginPanel.first_time_login = false;
            loginPanel.showLogin_panel();
            this.dispose();

        } else if (actionSource == save_button) {

            MyFnFSettings.registrationStop = false;
            if (!save_button.isEnabled()) {
                return;
            }
            int error = 0;
            String user_msg = "";
            String f_msg = "";
            String l_msg = "";
            String p_msg = "";
            String c_p_msg = "";
            String e_msg = "";
            String m_msg = "";
            String c_msg = "";
            String g_msg = "";
            if (userIDTextField.getText().trim().length() < 4) {
                error = 1;
                user_msg = StaticFields.MY_FNF_NAME_TEXT + " atleast 4 character long";

            } else {
                if (!userIDTextField.getText().substring(0, 1).matches("[a-zA-Z]")) {
                    error = 1;
                    user_msg = StaticFields.MY_FNF_NAME_TEXT + " must start with a letter";
                } else if (!userIDTextField.getText().toString().matches("^[a-zA-Z][a-zA-Z0-9_]*")) {
                    error = 1;
                    user_msg = StaticFields.MY_FNF_NAME_TEXT + " can contain only letters numbers and uderscore";
                }

            }
//            f_msg = (firstNameTextField.getText().trim().length() < 1) ? "Please enter First Name" : "";
//            l_msg = (lastNameTextField.getText().trim().length() < 1) ? "Please enter Last Name" : "";

            if (firstNameTextField.getText().trim().length() < 1) {
                f_msg = "Please enter First Name";
                error = 1;

            }

            if (lastNameTextField.getText().trim().length() < 1) {
                error = 1;
                l_msg = "Please enter Last Name";

            }

            if (passwordTextField.getText().trim().length() < 6) {
                error = 1;
                p_msg = StaticFields.PASSWORD_TEXT + " must be atleast 6 characters";
            } else if (HelperMethods.check_string_contains_substring(passwordTextField.getText(), userIDTextField.getText().toString())) {
                p_msg = StaticFields.PASSWORD_TEXT + " can't contain  " + StaticFields.MY_FNF_NAME_TEXT;
                error = 1;
            }

            if (HelperMethods.check_string_contains_substring(passwordTextField.getText(), ",")) {
                p_msg = "Comma(,) not allowed in " + StaticFields.PASSWORD_TEXT;
                error = 1;
            }

            if (conPasswordTextField.getText().trim().length() < 6) {
                c_p_msg = StaticFields.PASSWORD_TEXT + " must be atleast 6 characters";
                error = 1;
            } else if (!passwordTextField.getText().trim().equals(conPasswordTextField.getText().trim())) {
                c_p_msg = "Password not matched";
                error = 1;
            }

            if (HelperMethods.check_string_contains_substring(conPasswordTextField.getText(), ",")) {
                c_p_msg = "Comma(,) not allowed in " + StaticFields.PASSWORD_TEXT;
                error = 1;
            }

            if (emailTextField.getText().trim().length() < 5 || email.validate(emailTextField.getText()) == false) {
                e_msg = "Please enter a valid email address";
                error = 1;
            }
            if (mobileNoTextField.getText().trim().length() < 3) {
                m_msg = "Please enter phone number";
                error = 1;
            } else if (!"".equals(HelperMethods.check_only_digit(mobileNoTextField.getText()))) {
                m_msg = HelperMethods.check_only_digit(mobileNoTextField.getText());
                error = 1;
            }


            if (country_prefixTextField.getText().trim().length() < 1) {
                m_msg = "Please specify calling code";
                error = 1;
            } else if (!"".equals(HelperMethods.check_country_code(country_prefixTextField.getText()))) {
                m_msg = HelperMethods.check_country_code(country_prefixTextField.getText());
                error = 1;
            }

            if (countryComboBox.getSelectedItem().toString().length() < 2) {
                error = 1;

                c_msg = "Please choose a country";

            }

            if (genderComboBox.getSelectedItem().toString().length() < 2) {
                error = 1;
                g_msg = "Please choose Gender";
            }
            userID_error_label.setText(user_msg);
            first_name_error_label.setText(f_msg);
            last_name_error_label.setText(l_msg);
            passwd_error_label.setText(p_msg);
            conf_passwd_error_label.setText(c_p_msg);
            email_error_label.setText(e_msg);
            country_error_label.setText(c_msg);
            mobile_number_error_label.setText(m_msg);
            gender_error_label.setText(g_msg);
            if (error == 0) {
                MyFnFSettings.userProfile = null;
                MyFnFSettings.LOGIN_SESSIONID = null;

                counter = 0;
                stopTimer = false;
                success = false;
                internet_check_flag = false;
                com_port_check = false;
                packed_send = false;
                //   singup_error_type = "no";
                com_port = 0;
                save_button.setEnabled(false);
                msg = "Checking Internet";
                errorLabel_new.setText(msg);
                MyFnFSettings.LOGIN_USER_ID = userIDTextField.getText().trim();
                MyFnFSettings.LOGIN_USER_PASSWORD = passwordTextField.getText();

                final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
                Runnable periodicTask = new Runnable() {
                    public void run() {

                        if (stopTimer) {
                            save_button.setEnabled(true);
                            executor.shutdown();
                        }
                        if (InternetConnection.isInternetReachable()) {
                            internet_check_flag = true;
                            msg = "Checking Port";
                            if (com_port == 0) {
                                if (SocketConstants.SOCKET_PORT <= 0) {
                                    String port_string = GetResponseJsons.getApiHost(MyFnFSettings.LOGIN_USER_ID, MyFnFSettings.LOGIN_USER_PASSWORD);

                                    if (HelperMethods.check_string_contains_substring(port_string, "true")) {
                                        com_port = GetResponseJsons.getComPort(port_string);
                                    }
                                } else {
                                    com_port = SocketConstants.SOCKET_PORT;
                                }
                            }

                            if (com_port != 0) {
                                com_port_check = true;
                                SocketConstants.SOCKET_PORT = com_port;
                                msg = "Signin up";
                                if (!packed_send) {
                                    bd_mobile = HelperMethods.bd_mobile_number(country_prefixTextField.getText().toString(), mobileNoTextField.getText().toString());
                                    mobileNoTextField.setText(bd_mobile);
                                    JsonFields fields = new JsonFields();
                                    fields.setAction(SocketConstants.AUTHENTICATION);
                                    fields.setType(SocketConstants.SIGN_UP);
                                    String packet_id = SocketConstants.SIGN_UP + System.currentTimeMillis();
                                    fields.setPacketId(packet_id);
                                    fields.setMobilePhoneDialingCode(country_prefixTextField.getText().toString());
                                    fields.setGender(genderComboBox.getSelectedItem().toString());
                                    fields.setFirstName(firstNameTextField.getText().trim());
                                    fields.setLastName(lastNameTextField.getText().trim());
                                    fields.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
                                    fields.setCountry(countryComboBox.getSelectedItem().toString());
                                    fields.setEmail(emailTextField.getText().trim());
                                    fields.setMobilePhone(bd_mobile);
                                    fields.setPassword(MyFnFSettings.LOGIN_USER_PASSWORD);
                                    pak_action.send_And_Store_Packed_As_String(fields, packet_id);
                                    //     pak_action.sendPkdAndStore(fields, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT, packet_id);
                                    packed_send = true;
                                }

                                if (success) {
                                    stopTimer = true;
                                    try {
                                        executor.shutdown();
                                    } catch (Exception e) {
                                    }

                                    if (MyFnFSettings.userProfile.getSessionId().length() > 0) {
                                        MyFnFSettings.isAuthenticated = true;
                                        SearchThread.stopSearchThread();

                                        //  this.dispose();
                                        if (!SetDynamicFriend.runningFlag) {
                                            SetDynamicFriend.runningFlag = true;
                                        }
                                        setMainUserInterface();
                                    }

                                }

                            }
                        }
                        if (!stopTimer) {
                            counterValueCheck(msg);
                        }
                    }
                };
                executor.scheduleAtFixedRate(periodicTask, 0, 1, TimeUnit.SECONDS);



            }

        }
        if (actionSource == countryComboBox) {
            //System.out.println(countryComboBox.getSelectedItem() + "");
            String c_code = HelperMethods.get_country_code_from_contry_name(countryComboBox.getSelectedItem().toString());
            country_prefixTextField.setText(c_code);
        }
    }

    public void counterValueCheck(String msg) {
        errorLabel_new.setForeground(Color.BLUE);
        //   System.out.println("counter--> " + counter);
        //   String msg = "Registering";

        switch (counter) {
            case 1:
                errorLabel_new.setText(msg + "..     ");
                break;
            case 2:
                errorLabel_new.setText(msg + "...     ");
                break;
            case 3:
                errorLabel_new.setText(msg + "....    ");
                break;
            case 4:
                errorLabel_new.setText(msg + "....    ");
                break;
            case 5:
                errorLabel_new.setText(msg + ".....     ");
                break;
            case 6:
                errorLabel_new.setText(msg + "......    ");
                break;
            case 7:
                errorLabel_new.setText(msg + ".......    ");
                break;
        }

        if (counter >= 7) {
            setStopTimer(true);
            save_button.setEnabled(true);
            errorLabel_new.setForeground(Color.red);
            if (!internet_check_flag) {
                errorLabel_new.setText("Network Error");
            } else {
                if (!com_port_check) {
                    errorLabel_new.setText("COM Port Problem");
                }
                if (!success) {
                    errorLabel_new.setText("Server Not Responding");
                }


            }

        }
        counter++;
//        System.err.println("counter ==> " + counter);
    }

    @Override
    public void keyTyped(KeyEvent event) {
        actionSource = event.getSource();
        if (actionSource == userIDTextField) {
            errorLabel_new.setText("");
        } else if (actionSource == passwordTextField) {
            errorLabel_new.setText("");
        }
    }

    @Override
    public void keyPressed(KeyEvent event) {
        actionSource = event.getSource();
        int key = (int) event.getKeyChar();
        if (key == KeyEvent.VK_ENTER) {
            if (!save_button.isEnabled()) {
                errorLabel_new.setText(" ");
                save_button.setEnabled(true);
            } else {
                save_button.doClick();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent event) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void save_signup_info() {
        try {
            BufferedWriter outFile = new BufferedWriter(new FileWriter(MyFnFSettings.SIGNUP_INFO_FILE));
            outFile.write("\n" + save_fistname + "=" + firstNameTextField.getText().trim());
            outFile.write("\n" + save_lastname + "=" + lastNameTextField.getText().trim());
            outFile.write("\n" + save_userid + "=" + userIDTextField.getText().trim());
            outFile.write("\n" + save_email + "=" + emailTextField.getText().trim());
            outFile.write("\n" + save_mobileno + "=" + HelperMethods.bd_mobile_number(country_prefixTextField.getText().toString(), mobileNoTextField.getText().toString()));
            outFile.write("\n" + save_country + "=" + countryComboBox.getSelectedItem());
            outFile.write("\n" + save_gender + "=" + genderComboBox.getSelectedItem());
            outFile.close();
        } catch (Exception ex) {
            //System.out.println(ex);
        }
    }

    public void read_signup_info() {
        try {
            InputStream input = null;
            File file = new File(MyFnFSettings.SIGNUP_INFO_FILE);
            if (file.exists()) {
                input = new FileInputStream(file);
                Properties fileProp = new Properties();
                fileProp.load(input);
                input.close();

                if (fileProp.containsKey(save_fistname)) {
                    String accStr = fileProp.getProperty(save_fistname);
                    if (accStr != null && accStr.trim().length() > 0) {
                        firstNameTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(save_lastname)) {
                    String accStr = fileProp.getProperty(save_lastname);
                    if (accStr != null && accStr.trim().length() > 0) {
                        lastNameTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(save_userid)) {
                    String accStr = fileProp.getProperty(save_userid);
                    if (accStr != null && accStr.trim().length() > 0) {
                        userIDTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(save_email)) {
                    String accStr = fileProp.getProperty(save_email);
                    if (accStr != null && accStr.trim().length() > 0) {
                        emailTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(save_mobileno)) {
                    String accStr = fileProp.getProperty(save_mobileno);
                    if (accStr != null && accStr.trim().length() > 0) {
                        mobileNoTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(save_country)) {
                    String accStr = fileProp.getProperty(save_country);
                    if (accStr != null && accStr.trim().length() > 0) {
                        countryComboBox.setSelectedItem(accStr);
                    }
                }
                if (fileProp.containsKey(save_gender)) {
                    String accStr = fileProp.getProperty(save_gender);
                    if (accStr != null && accStr.trim().length() > 0) {
                        genderComboBox.setSelectedItem(accStr);
                    }
                }
                firstNameTextField.requestFocus();
            }
        } catch (Exception e) {
        }
    }

    public void deleteSingUpUserInfoFile() {
        try {
            File userInfoFile = new File(MyFnFSettings.SIGNUP_INFO_FILE);
            if (userInfoFile.exists()) {
                userInfoFile.delete();
            }
        } catch (Exception ex) {
        }
    }

    public void setMainUserInterface() {
        if (MyFnFSettings.isAuthenticated) {
            mainuserUi = new GUI24FnF(sip_provider, ua_profile, ua);
            mainuserUi.showLogin_User_panel(true);

            this.dispose();
        }
    }

//    @Override
//    public void onReceivedPacket(SendReceive udp, UdpPacket packet) {
//        String data = SocketConstants.getDataFromUdpPak(packet);
//        // System.out.println("Receved Signup req:" + data);
//        JsonFields js = HelperMethods.response_json(data);
//        if (js.getType().equals(SocketConstants.USER_ID_AVAILABLE)) {
//            if (js.getSuccess()) {
//                errorLabel_new.setForeground(Color.blue);
//                errorLabel_new.setText("Available");
//            } else {
//                errorLabel_new.setForeground(Color.red);
//                errorLabel_new.setText(js.getMessage());
//            }
//        } else {
//            if (!js.getSuccess()) {
//                setStopTimer(true);
//                errorLabel_new.setForeground(Color.red);
//                errorLabel_new.setText(js.getMessage());
//            } else {
//                if (js.getSuccess()) {
//
//                    //  System.out.println(js.getSessionId());
//                    // System.out.println(js.getSwitchIp());
//                    HelperMethods.user_profile(data);
//                    MyFnFSettings.LOGIN_SESSIONID = js.getSessionId();
//                    MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
//                    MyFnFSettings.userProfile.setMobilePhoneDialingCode(country_prefixTextField.getText().toString());
//                    MyFnFSettings.userProfile.setGender(genderComboBox.getSelectedItem().toString());
//                    MyFnFSettings.userProfile.setFirstName(firstNameTextField.getText().trim());
//                    MyFnFSettings.userProfile.setLastName(lastNameTextField.getText().trim());
//                    //     MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
//                    MyFnFSettings.userProfile.setCountry(countryComboBox.getSelectedItem().toString());
//                    MyFnFSettings.userProfile.setEmail(emailTextField.getText().trim());
//                    MyFnFSettings.userProfile.setMobilePhone(bd_mobile);
//                    MyFnFSettings.userProfile.setWhatisInYourMind(null);
//                    MyFnFSettings.isAuthenticated = true;
//                    Registration registration = new Registration(ua_profile, ua);
//                    setStopTimer(true);
//                    success = true;
//
//                }
//            }
//
//
////            MyFnFSettings.LOGIN_SESSIONID = js.getSessionId();
////            MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
////            MyFnFSettings.userProfile.setMobilePhoneDialingCode(country_prefixTextField.getText().toString());
////            MyFnFSettings.userProfile.setGender(genderComboBox.getSelectedItem().toString());
////            MyFnFSettings.userProfile.setFirstName(firstNameTextField.getText().trim());
////            MyFnFSettings.userProfile.setLastName(lastNameTextField.getText().trim());
////            MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
////            MyFnFSettings.userProfile.setCountry(countryComboBox.getSelectedItem().toString());
////            MyFnFSettings.userProfile.setEmail(emailTextField.getText().trim());
////            MyFnFSettings.userProfile.setMobilePhone(bd_mobile);
////            MyFnFSettings.userProfile.setSessionId(js.getSessionId());
////            MyFnFSettings.userProfile.setSwitchIp(js.getSwitchIp());
////            MyFnFSettings.userProfile.setSwitchPort(js.getSwitchPort());
//
//        }
//
//    }
    private void set_user_id() {
        if (userIDTextField.getText().length() == 0) {
            errorLabel_new.setText("");
        } else if (userIDTextField.getText().length() < 4) {
            errorLabel_new.setForeground(Color.RED);
            errorLabel_new.setText(StaticFields.MY_FNF_NAME_TEXT + " atleast 4 character long");
        }
    }

    @Override
    public void onReceivedData(JsonFields getJsonClass) {
        //  String data = SocketConstants.getDataFromUdpPak(packet);
        // System.out.println("Receved Signup req:" + data);
        JsonFields js = getJsonClass;
        if (js.getType().equals(SocketConstants.USER_ID_AVAILABLE)) {
            if (js.getSuccess()) {
                errorLabel_new.setForeground(Color.blue);
                errorLabel_new.setText("Available");
            } else {
                errorLabel_new.setForeground(Color.red);
                errorLabel_new.setText(js.getMessage());
            }
        } else {
            if (!js.getSuccess()) {
                setStopTimer(true);
                errorLabel_new.setForeground(Color.red);
                errorLabel_new.setText(js.getMessage());
            } else {
                if (js.getSuccess()) {

                    String create_string = new Gson().toJson(js);
                    HelperMethods.user_profile(create_string);
                    MyFnFSettings.LOGIN_SESSIONID = js.getSessionId();
                    MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
                    MyFnFSettings.userProfile.setMobilePhoneDialingCode(country_prefixTextField.getText().toString());
                    MyFnFSettings.userProfile.setGender(genderComboBox.getSelectedItem().toString());
                    MyFnFSettings.userProfile.setFirstName(firstNameTextField.getText().trim());
                    MyFnFSettings.userProfile.setLastName(lastNameTextField.getText().trim());
                    //     MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
                    MyFnFSettings.userProfile.setCountry(countryComboBox.getSelectedItem().toString());
                    MyFnFSettings.userProfile.setEmail(emailTextField.getText().trim());
                    MyFnFSettings.userProfile.setMobilePhone(bd_mobile);
                    MyFnFSettings.userProfile.setWhatisInYourMind(null);
                    MyFnFSettings.isAuthenticated = true;
                    Registration registration = new Registration(ua_profile, ua);
                    setStopTimer(true);
                    success = true;

                }
            }


//            MyFnFSettings.LOGIN_SESSIONID = js.getSessionId();
//            MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
//            MyFnFSettings.userProfile.setMobilePhoneDialingCode(country_prefixTextField.getText().toString());
//            MyFnFSettings.userProfile.setGender(genderComboBox.getSelectedItem().toString());
//            MyFnFSettings.userProfile.setFirstName(firstNameTextField.getText().trim());
//            MyFnFSettings.userProfile.setLastName(lastNameTextField.getText().trim());
//            MyFnFSettings.userProfile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
//            MyFnFSettings.userProfile.setCountry(countryComboBox.getSelectedItem().toString());
//            MyFnFSettings.userProfile.setEmail(emailTextField.getText().trim());
//            MyFnFSettings.userProfile.setMobilePhone(bd_mobile);
//            MyFnFSettings.userProfile.setSessionId(js.getSessionId());
//            MyFnFSettings.userProfile.setSwitchIp(js.getSwitchIp());
//            MyFnFSettings.userProfile.setSwitchPort(js.getSwitchPort());

        }
    }
}
