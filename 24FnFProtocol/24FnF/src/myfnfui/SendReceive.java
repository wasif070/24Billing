/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import java.io.IOException;

/**
 *
 * @author reefat
 */
public class SendReceive extends Thread {

    public static boolean isThredActive = false;
    protected static UdpSocket socket;
    protected SendReceiveListener listener;
    private boolean stop = false;
    public static final int BUFFER_SIZE = 20535;
//    public static SendReceive send_receive;

    public SendReceive(UdpSocket param_socket, SendReceiveListener param_listener) {
//        send_receive = this;
        socket = param_socket;
        listener = param_listener;
        start();
    }

    public UdpSocket getUdpSocket() {
        return socket;
    }

    public void send(UdpPacket packet) throws IOException {
        if (!stop) {
            socket.send(packet);
        }
    }

    public void halt() {
        stop = true;
    }

    @Override
    public void run() {
        isThredActive = true;
        byte[] buf = new byte[BUFFER_SIZE];
        UdpPacket packet = new UdpPacket(buf, buf.length);
        while (!stop) {
//
            //System.out.println("receiver running");

            try {
                //     System.out.println("length --> " + packet.getLength());
                buf = new byte[BUFFER_SIZE];
                packet = new UdpPacket(buf, buf.length);


                //   System.out.println("Receiving start !!!");
                socket.receive(packet);

                if (packet.getLength() >= 0) {
                    if (listener != null) {
                        listener.onReceivedPacket(this, packet);
                    }
                }
            } catch (Exception e) {
            }

        }
    }
}
