/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import java.util.ArrayList;
import local.ua.UserAgentProfile;

/**
 *
 * @author FaizAhmed
 */
public class MapFriendList extends UserAgentProfile {

    private ArrayList<UserAgentProfile> contactList = null;

    public ArrayList<UserAgentProfile> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<UserAgentProfile> contactList) {
        this.contactList = contactList;
    }
}
