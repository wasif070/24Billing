/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.StaticImages;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

/**
 *
 * @author FaizAhmed
 */
public class MainFrame extends JFrame implements ActionListener {

    int posX = 0;
    int posY = 0;
    public JPanel main_container;
    public JPanel topBar;
    public JPanel log_container;
    public JLabel log_Label;
    public JButton button_window_minimize;
    public JButton button_window_close;
    JLabel label_image;
    Object actionSource;
    Object focusedSource;

    public MainFrame() {


        ImageIcon imageIcon = DesignClasses.return_image(StaticImages.ICON_24MYFNF);
        Image image = imageIcon.getImage();
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        //   this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setIconImage(image);
//        this.setIconImage(image);
        this.setTitle("24FnF");
        this.setSize(DefaultSettings.DEFAULT_WIDTH, DefaultSettings.DEFAULT_HEIGHT);
        //  
        // this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setUndecorated(true);
        this.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                posX = e.getX();
                posY = e.getY();
            }
        });
        this.addMouseMotionListener(new MouseAdapter() {
            public void mouseDragged(MouseEvent evt) {
                //sets frame position when mouse dragged			
                setLocation(evt.getXOnScreen() - posX, evt.getYOnScreen() - posY);

            }
        });


        main_container = (JPanel) getContentPane();
        main_container.setBorder(DefaultSettings.thickBorder);
        main_container.setBackground(Color.WHITE);
        main_container.setLayout(null);
        label_image = new JLabel("", DesignClasses.return_image(StaticImages.IMAGE_joption_header), JLabel.CENTER);
        label_image.setBackground(null);
        topBar = new JPanel(new BorderLayout());
        topBar.add(label_image, BorderLayout.CENTER);
        topBar.setBounds(2, 2, DefaultSettings.DEFAULT_WIDTH - 4, DefaultSettings.DEFAULT_TOP_BAR);

        topBar.setBorder(null);
        int minimize_button_left_possion = 250;
        button_window_minimize = DesignClasses.create_button_with_image_fixed_location(StaticImages.IMAGE_MINIMIZE, StaticImages.IMAGE_MINIMIZE_HOVER, "Minimize", minimize_button_left_possion, 15, 20, 20);
        button_window_minimize.addActionListener(this);
        main_container.add(button_window_minimize);

        button_window_close = DesignClasses.create_button_with_image_fixed_location(StaticImages.ICON_CLOSE, StaticImages.ICON_CLOSE_HOVER, "Close Window", minimize_button_left_possion + 20, 15, 20, 20);
        button_window_close.addActionListener(this);
        main_container.add(button_window_close);
        main_container.add(topBar);
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            this.dispose();
        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        }

    }
}
