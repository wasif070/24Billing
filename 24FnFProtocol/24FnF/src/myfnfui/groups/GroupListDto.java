/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nazmul
 */
public class GroupListDto implements Serializable {

    private String action;
    private String type;
    private String seq;
    private String packetId;
    private Long packetIdFromServer;
    private List<GroupDto> groupList = new ArrayList<GroupDto>();
    private String message;
    private boolean success = true;

    public List<GroupDto> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<GroupDto> groupList) {
        this.groupList = groupList;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public Long getPacketIdFromServer() {
        return packetIdFromServer;
    }

    public void setPacketIdFromServer(Long packetIdFromServer) {
        this.packetIdFromServer = packetIdFromServer;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
