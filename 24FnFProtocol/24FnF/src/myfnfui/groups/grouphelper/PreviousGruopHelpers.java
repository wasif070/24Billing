/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups.grouphelper;

import helpers.APIParametersFiles;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticImages;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import local.ua.UserAgentProfile;
import myfnfui.JsonFields;
import myfnfui.Messages.NotificationMessages;
import myfnfui.groups.CreateGroup;
import myfnfui.groups.GroupConstants;
import myfnfui.groups.GroupDto;
import myfnfui.groups.GroupMemberDto;
import myfnfui.groups.GroupsWindow;
import static myfnfui.groups.GroupsWindow.add_group_member_save_cancle_button_panel;
import static myfnfui.groups.GroupsWindow.gruop_actions_panel;
//import myfnfui.groups.GroupsWindow;
//import static myfnfui.groups.GroupsWindow.draw_group_details;
import static myfnfui.groups.GroupsWindow.gruop_members_panel;
import static myfnfui.groups.GroupsWindow.gruop_panel;
import static myfnfui.groups.GroupsWindow.gruop_save_button_panel;
import static myfnfui.groups.GroupsWindow.scrollPane_for_gruop;
import myfnfui.groups.NewGruopFields;
import myfnfui.groups.TempGruoupContainer;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author user
 */
public class PreviousGruopHelpers {

    private int button_width = 70;
    PackageActions pak_action = new PackageActions();
    public static Long processing_gruop_id = null;
    JButton delete_gruop;
    JButton leave_gruop;
    JButton add_member;
    HelperMethods helperMethods = new HelperMethods();

    public void draw_grouplist() {
        //   System.out.println("ddd");
        gruop_panel.removeAll();
        //  freind_detials_panel.add(createNewGroup());
//createSingleGruopDetails
        int row = 0;
        Color clr = Color.WHITE;
        if (!TempGruoupContainer.getInstance().getGruopFriendContainer().isEmpty()) {

            for (Long gruopId : TempGruoupContainer.getInstance().getGruopFriendContainer().keySet()) {
                GroupDto getValues = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gruopId);
//                //    GroupMemberDto members= getValues.getGroupMembers();
//                for (GroupMemberDto members : getValues.getGroupMembers()) {
//                    if (row % 2 != 0) {
//
//                        //   System.out.println("ddd");
//                        clr = DefaultSettings.table_odd_background;
//                    } else {
//                        //  System.out.println("eee");
//                        // freind_detials_panel.setBackground(null);
//                        gruop_panel.setBackground(DefaultSettings.friend_list_border_color);
//                        clr = DefaultSettings.table_odd_background;
//
//                    }
//                    row++;
//                    gruop_panel.add(createSingleGruopDetails(getValues.getGroupMembers().size(), gruopId, getValues.getSuperAdmin(), getValues.getGroupName(), clr));
//                }
                clr = DefaultSettings.table_odd_background;
                gruop_panel.add(createSingleGruopDetails(getValues.getGroupMembers().size(), gruopId, getValues.getSuperAdmin(), getValues.getGroupName(), clr));
                //  System.out.println("Group Name : " + getValues.getGroupName());
            }
        } else {
            //  System.out.println("ddd");
            gruop_panel.add(add_not_goup_window());
        }
        gruop_panel.revalidate();
        gruop_panel.repaint();
    }

    public void delete_single_group_details() {
        //  gruop_actions_panel.removeAll();
        gruop_actions_panel.removeAll();
        //  gruop_actions_panel.add(create_action_bar_in_friend_details(gropId, getValues.getGroupName(), super_admin, admin, 200, 25, clr));
        gruop_actions_panel.revalidate();
        gruop_actions_panel.repaint();


        gruop_members_panel.removeAll();
        gruop_members_panel.revalidate();
        gruop_members_panel.repaint();
    }

    public void draw_group_details(Long gropId) {
        //   System.out.println("ddd");

        //  freind_detials_panel.add(createNewGroup());
//createSingleGruopDetails
        //   GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gropId);
        // GroupMemberDto members= dto.getGroupMembers();
        int row = 0;
        Color clr = DefaultSettings.bar_bg_color;
//        if (TempGruoupContainer.getInstance().getGruopFriendContainer() != null) {
//            for (Long gruopId : TempGruoupContainer.getInstance().getGruopFriendContainer().keySet()) {
        GroupDto getValues = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gropId);
        int super_admin = 0;
        int admin = 0;

        if (getValues.getSuperAdmin().equals(MyFnFSettings.LOGIN_USER_ID)) {
            super_admin = 1;
            admin = 1;
        } else {
            for (GroupMemberDto members : getValues.getGroupMembers()) {
                if (members.getUserIdentity().equals(MyFnFSettings.LOGIN_USER_ID) && members.getAdmin()) {
                    admin = 1;
                    break;
                }
            }
        }
        gruop_actions_panel.removeAll();
        gruop_actions_panel.add(create_action_bar_in_friend_details(gropId, getValues.getGroupName(), super_admin, admin, 200, 25, clr));
        gruop_actions_panel.revalidate();
        gruop_actions_panel.repaint();
        //    GroupMemberDto members= getValues.getGroupMembers();
        //   gruop_members_panel.add(create_action_bar_in_friend_details(getValues.getGroupName(), super_admin, admin, 200, 25, clr));
        gruop_members_panel.removeAll();
        for (GroupMemberDto members : getValues.getGroupMembers()) {
            if (row % 2 != 0) {

                //   System.out.println("ddd");
                clr = DefaultSettings.table_even_background;
            } else {
                //  System.out.println("eee");
                // freind_detials_panel.setBackground(null);

                gruop_members_panel.setBackground(DefaultSettings.friend_list_border_color);
                clr = DefaultSettings.table_even_background;

            }
            row++;
            gruop_members_panel.add(createSingle_frined_Gruop_Details(gropId, members, super_admin, admin, clr));
        }
        //  System.out.println("Group Name : " + getValues.getGroupName());
//            }
//        }
        gruop_members_panel.revalidate();
        gruop_members_panel.repaint();
    }

    public boolean check_already_in_group(GroupDto mm, String user_id) {
        boolean has = true;
        for (GroupMemberDto members : mm.getGroupMembers()) {
            if (members.getUserIdentity().equals(user_id)) {
                has = false;
                break;
            }
        }
        return has;
    }

    public void draw_friendlist_for_add_in_gruoup(GroupDto mm, int superadmin) {
        if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        }
        int row = 0;
        Color clr = DefaultSettings.bar_bg_color;
        gruop_members_panel.removeAll();
        for (String key : FriendList.getInstance().getFriend_hash_map().keySet()) {
            if (TempGruoupContainer.getInstance().getGroupMemberContainer().get(key) == null) {
                UserAgentProfile single_profile = FriendList.getInstance().getFriend_hash_map().get(key);
                if (single_profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED) && check_already_in_group(mm, key)) {

                    //   System.out.println("key:" + key);

                    if (single_profile != null) {
                        if (row % 2 != 0) {

                            //   System.out.println("ddd");
                            clr = DefaultSettings.table_odd_background;
                        } else {
                            gruop_members_panel.setBackground(DefaultSettings.friend_list_border_color);
                            clr = DefaultSettings.table_odd_background;

                        }
                        gruop_members_panel.add(createSingleFriends_for_add_member(mm.getGroupId(), single_profile, clr, 35, superadmin));

                    }
                    row++;
                }
            }
        }
        //  System.out.println("Group Name : " + getValues.getGroupName());
//            }
//        }
        gruop_members_panel.revalidate();
        gruop_members_panel.repaint();

    }

    private JPanel createSingleFriends_for_add_member(final Long gruopid, final UserAgentProfile single_p, final Color clr, int height, int superadmin) {
        //   System.out.println("in line 338:" + single_p.getFirstName());
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, height));
        int container_width = 140;
        JPanel container = new JPanel(new GridLayout(2, 1, 0, 0));
        container.setBounds(40, 0, container_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);
        //container.setBackground(Color.CYAN);
        //  container.addMouseListener(this);
        container.setFocusable(true);
        JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(single_p.getFirstName() + " " + single_p.getLastName(), 10, 0, 100, 12, 0, DefaultSettings.text_color1);

        JLabel whatLable = new JLabel(single_p.getUserIdentity());
        //  whatLable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container.add(user_name_button);
        container.add(whatLable);
        //  rowPanel.setBackground(Color.white);
        rowPanel.add(container);
        int friend_conaiter = container_width;
        int gap = 10;
        int check_box_width = 25;
        final JCheckBox saveBox = new JCheckBox("");
        saveBox.setBounds(30 + friend_conaiter + gap, 5, check_box_width, 20);
        saveBox.setBackground(null);
        saveBox.setEnabled(false);
        //  Boolean admin_true = TempGruoupContainer.getInstance().getGroupMemberContainer().get(single_p.getUserIdentity());
        //System.out.println("Booolean:" + admin_true);
        //  saveBox.setSelected(admin_true);

        //   saveBox.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
        saveBox.addItemListener(
                new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    //       System.out.println("selected");
                    TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), true);

                }
                if (e.getStateChange() != ItemEvent.SELECTED) {
                    TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), false);
                }

            }
        });
        rowPanel.add(saveBox);
        int image_width = 35;
        final JButton add_inGrp = DesignClasses.create_image_button_with_text_scale1("Add", container_width + image_width + gap + check_box_width + 5, 5);
        rowPanel.add(add_inGrp);

        add_inGrp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                add_inGrp.setEnabled(false);
                saveBox.setEnabled(true);
//                System.out.println("TempGruoupContainer. size ==> \n" + TempGruoupContainer.getInstance().getGroupMemberContainer().size());
                TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), false);
                create_save_canecl_panel_in_buttom(gruopid, 230, 35, clr);

            }
        });
        rowPanel.setBackground(clr);
        JLabel user_image = helperMethods.create_image_from_url(single_p.getGender(), single_p.getProfileImage(), 2, 0, image_width, image_width);
        rowPanel.add(user_image);


        return rowPanel;
    }

    public JPanel createSingleGruopDetails(int numberOfFriend, final Long gropId, String superAmdin, final String gruopName, Color clr) {
        if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        }
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, 35));
        rowPanel.setBackground(clr);
        int container_width = 170;
        JPanel container = new JPanel(new GridLayout(1, 1, 0, 0));
        container.setBounds(40, 1, container_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);
        //container.setBackground(Color.CYAN);
        //  container.addMouseListener(this);
        container.setFocusable(true);
        // JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(gruopName, 10, 0, 100, 11, 1, DefaultSettings.text_color1);
        JButton user_name_button = DesignClasses.create_button_no_image_with_font_size_width(gruopName + " ( " + numberOfFriend + " )", 0, 1, 150, 13, 0);
        JLabel whatLable = new JLabel("Creator: " + superAmdin);
        user_name_button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container.add(user_name_button);
        container.add(whatLable);
        rowPanel.setBackground(Color.white);
        rowPanel.add(container);
        int image_width = 35;
        user_name_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                GroupsWindow.currentSelectedGruop = gropId;

                //   gruop_name_label.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, DefaultSettings.blue_bar_background));
                //     gruop_name_label.setText(gruopName);
                draw_group_details(gropId);
            }
        });
//        rowPanel.setBackground(clr);

        JLabel user_image = HelperMethods.get_scale_image(0, 2, image_width, image_width, MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.GROUP_IMAGE_SCALED);
        rowPanel.add(user_image);

        return rowPanel;
    }

    public JPanel createSingle_frined_Gruop_Details(final Long gropId, final GroupMemberDto members, int superAdmin, int admin, Color clr) {

        hide_save_canecl_panel_in_buttom();

        JLabel statusImage_label;
        int image_width = 35;
        int status_image_width = 22;
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, 35));
        rowPanel.setBackground(clr);
        int container_width = 140;
        JPanel container = new JPanel(new GridLayout(1, 1, 0, 0));
        container.setBounds(status_image_width + 2 + image_width + 10, 3, container_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);
        //container.setBackground(Color.CYAN);
        //  container.addMouseListener(this);
        container.setFocusable(true);
        String f_name = members.getFirstName();
        String l_name = members.getLastName();
        String gender = members.getGender();
        String presence = members.getPresence();
        String status_image = StaticImages.IMAGE_OFFLINE;
        UserAgentProfile userProfile = FriendList.getInstance().getFriend_hash_map().get(members.getUserIdentity());

        if (userProfile != null) {
            f_name = userProfile.getFirstName();
            l_name = userProfile.getLastName();
            gender = userProfile.getGender();
            presence = userProfile.getPresence().toString();
        }

//        //  System.out.println("f_name-***************************** "+f_name);
//        if (f_name == null) {
//            // System.out.println("f_name-***************************** "+f_name);
//        }

        if (gender == null) {
            gender = "Male";
        }


        if (presence != null && MyFnFSettings.isRegisterred && presence.equals(APIParametersFiles.PRESENCE_ONLINE.toString())) {
            status_image = StaticImages.IMAGE_ONLINE;
        }



//----------------------------------------- #?#?#? (2) - start ----------------------------------------------------               
        statusImage_label = DesignClasses.create_image_label(5, 0, status_image_width, 20, status_image);
//----------------------------------------- #?#?#? (2) - end ----------------------------------------------------                


        statusImage_label = DesignClasses.create_image_label(2, 8, status_image_width, 20, status_image);
        rowPanel.add(statusImage_label);
        // JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(gruopName, 10, 0, 100, 11, 1, DefaultSettings.text_color1);
        JButton user_name_button = DesignClasses.create_button_no_image_with_font_size_width(f_name + " " + l_name, 0, 24, 150, 11, 0);
        String admin_id = members.getUserIdentity();
        final JButton delete_member = DesignClasses.create_image_button_with_text_scale1("Delete", 215, 8);

        delete_member.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.create_confrim_panel(NotificationMessages.DELETE_NOTIFICAITON, 80);
                if (HelperMethods.confirm == 1) {
                    send_remove_gruop_member_msg(gropId, members.getUserIdentity());
                    delete_member.setEnabled(false);

                    //     draw_group_details(gropId);
                }
            }
        });
        int check_box_width = 25;
        final JCheckBox saveBox = new JCheckBox("");
        saveBox.setBounds(190, 8, check_box_width, 20);
        saveBox.setBackground(null);
        if (members.getAdmin()) {
            saveBox.setSelected(members.getAdmin());
        }
        saveBox.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
        saveBox.addItemListener(
                new ItemListener() {
            public void itemStateChanged(ItemEvent e) {

                if (members.getAdmin()) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        TempGruoupContainer.getInstance().getGroupMemberContainer().remove(members.getUserIdentity());
                        // System.out.println("Selected " + TempGruoupContainer.getInstance().getGroupMemberContainer().get(members.getUserIdentity()));
                    }
                    if (e.getStateChange() != ItemEvent.SELECTED) {
                        TempGruoupContainer.getInstance().getGroupMemberContainer().put(members.getUserIdentity(), false);
                        //  System.out.println("Selected " + TempGruoupContainer.getInstance().getGroupMemberContainer().get(members.getUserIdentity()));
                    }
                }
                if (!members.getAdmin()) {
                    if (e.getStateChange() == ItemEvent.SELECTED) {
                        TempGruoupContainer.getInstance().getGroupMemberContainer().put(members.getUserIdentity(), true);
                        //  System.out.println("Selected " + TempGruoupContainer.getInstance().getGroupMemberContainer().get(members.getUserIdentity()));
                    }
                    if (e.getStateChange() != ItemEvent.SELECTED) {
                        TempGruoupContainer.getInstance().getGroupMemberContainer().remove(members.getUserIdentity());

                        // System.out.println("Selected " + TempGruoupContainer.getInstance().getGroupMemberContainer().get(members.getUserIdentity()));
                    }

                }
                if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {

                    create_save_button_in_buttom(gropId, 200, 35, null);
                } else {
                    delete_save_button_in_buttom();
                }
            }
        });

        if (superAdmin == 1 && members.getUserIdentity().equals(MyFnFSettings.LOGIN_USER_ID)) {
            admin_id = admin_id + ", " + "Super Admin";
        } else {
            if (members.getAdmin()) {
                admin_id = admin_id + ", " + "Admin";
            }
            if (superAdmin == 1) {
                rowPanel.add(delete_member);
                rowPanel.add(saveBox);
            } else if (superAdmin != 1 && admin == 1) {
                if (!members.getAdmin()) {
                    rowPanel.add(delete_member);
                }
            }
        }
        JLabel whatLable = new JLabel(admin_id);
        user_name_button.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container.add(user_name_button);
        container.add(whatLable);
        rowPanel.add(container);
        String pp = DesignClasses.get_scaled_male_or_female_image(gender);
        JLabel user_image;// = HelperMethods.get_scale_image(status_image_width + 2, 2, image_width, image_width, pp);

//----------------------------------------- #?#?#? (1) - start ----------------------------------------------------        
        try {
            user_image = helperMethods.create_image_from_url(userProfile.getGender(), userProfile.getProfileImage(), 30, 2, 35, 35);
        } catch (Exception e) {
            user_image = HelperMethods.get_scale_image(status_image_width + 2, 2, image_width, image_width, pp);
        }
//----------------------------------------- #?#?#? (1) - end ----------------------------------------------------        

        rowPanel.add(user_image);

        return rowPanel;
    }

    public void hide_save_canecl_panel_in_buttom() {
        TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        add_group_member_save_cancle_button_panel.removeAll();
        add_group_member_save_cancle_button_panel.repaint();
        add_group_member_save_cancle_button_panel.revalidate();
    }

    public void create_save_canecl_panel_in_buttom(final Long gruopid, int width, int height, Color clr) {

        add_group_member_save_cancle_button_panel.removeAll();
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(width, height));
        rowPanel.setBackground(clr);
        int left = 120;
        JLabel number_of_change = DesignClasses.makeJLabel_with_text_color("Seleced: " + TempGruoupContainer.getInstance().getGroupMemberContainer().size(), 0, 2, 100, 11, 0, Color.BLUE);
        rowPanel.add(number_of_change);
        final JButton save_button = DesignClasses.create_button_with_text_color_bgColor("Save", 10, 1, left, 2, button_width, 20, Color.WHITE, new Color(0x666633));
        save_button.setEnabled(true);
        save_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                save_button.setEnabled(false);
                send_add_group_member_request(gruopid);

            }
        });
        final JButton calcel_button = DesignClasses.create_button_with_text_color_bgColor("Cancel", 10, 1, left + button_width + 2, 2, button_width, 20, Color.WHITE, new Color(0x666633));

        calcel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
                draw_group_details(gruopid);
                hide_save_canecl_panel_in_buttom();
            }
        });
        if (TempGruoupContainer.getInstance().getGroupMemberContainer().size() > 0) {
            rowPanel.add(calcel_button);
            rowPanel.add(save_button);
        }
        add_group_member_save_cancle_button_panel.add(rowPanel);
        add_group_member_save_cancle_button_panel.repaint();
        add_group_member_save_cancle_button_panel.revalidate();

    }

    public void create_save_button_in_buttom(final Long gruopid, int width, int height, Color clr) {
        gruop_save_button_panel.removeAll();
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(width, height));
        rowPanel.setBackground(clr);

        JLabel number_of_change = DesignClasses.makeJLabel_with_text_color("Number of Change: " + TempGruoupContainer.getInstance().getGroupMemberContainer().size(), 70, 2, 130, 11, 0, Color.BLUE);
        rowPanel.add(number_of_change);
        final JButton save_button = DesignClasses.create_button_with_text_color_bgColor("Save Change", 10, 1, 200, 2, button_width, 20, Color.WHITE, new Color(0x666633));

        save_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //    if (processing_gruop_id == null) {
                //   processing_gruop_id = gruopid;
                send_update_msg_in_group(gruopid);
                //save_button.setEnabled(false);
//                } else {
//                    HelperMethods.create_confrim_panel_no_cancel("One Group Operation running,please wait", 50);
//                }
            }
        });
        rowPanel.add(save_button);
        rowPanel.add(save_button);
        gruop_save_button_panel.add(rowPanel);
        gruop_save_button_panel.repaint();
        gruop_save_button_panel.revalidate();

    }

    public static void delete_save_button_in_buttom() {
        gruop_save_button_panel.removeAll();
        gruop_save_button_panel.repaint();
        gruop_save_button_panel.revalidate();

    }

    public static JPanel add_not_goup_window() {
        scrollPane_for_gruop.setBorder(null);
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, 50));
        Color clr = DefaultSettings.bar_bg_color;
        rowPanel.setBackground(Color.WHITE);
        JLabel jlble = DesignClasses.makeJLabel_with_background_font_size_width_color("No Group", 50, 0, 150, 25, 12, 1, Color.BLUE, Color.WHITE, 0);
        rowPanel.add(jlble);
        return rowPanel;

    }

    public JPanel create_action_bar_in_friend_details(final Long gropId, final String gruopName, final int superAdmin, int admin, int width, int height, Color clr) {
        //   System.out.println("in line 338:" + single_p.getFirstName());
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(width, height));
        rowPanel.setBackground(clr);
        int p_width = width;
        int margin_left = 10;
        int label_width = 80;
        final GroupDto getValues = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gropId);
        if (admin == 1) {
            add_member = DesignClasses.create_button_with_text_color_bgColor("Add Member", 10, 1, p_width - (button_width + 5), 8, button_width, 20, Color.WHITE, new Color(0x666633));

            add_member.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    hide_save_canecl_panel_in_buttom();
                    draw_friendlist_for_add_in_gruoup(getValues, superAdmin);
                    //  new NotificationWindow().createNotificationWindow("test");
//                    SwingUtilities.invokeLater(new Runnable() {
//                        public void run() {
//                            new NotificationWindow().createNotificationWindow("test");
//
//                        }
//                    });
                }
            });
            rowPanel.add(add_member);
        }
        if (superAdmin == 1) {
            delete_gruop = DesignClasses.create_button_with_text_color_bgColor("Delete Group", 10, 1, p_width - 2, 8, button_width, 20, Color.WHITE, new Color(0xA00000));
            rowPanel.add(delete_gruop);
            delete_gruop.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    HelperMethods.create_confrim_panel("Delete " + gruopName + " ? ", 80);
                    if (HelperMethods.confirm == 1) {
                        delete_gruop.setEnabled(false);
                        send_group_delete_request(gropId);

                        //     draw_group_details(gropId);
                    }
                }
            });
        } else {
            leave_gruop = DesignClasses.create_button_with_text_color_bgColor("Leave Group", 10, 1, p_width - 2, 8, button_width, 20, Color.WHITE, new Color(0xA00000));
            leave_gruop.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    HelperMethods.create_confrim_panel("Leave from " + gruopName + " ? ", 80);
                    if (HelperMethods.confirm == 1) {
                        leave_gruop.setEnabled(false);
                        send_leave_group_request(gropId);

                        //     draw_group_details(gropId);
                    }
                }
            });
            rowPanel.add(leave_gruop);
        }

        JLabel gruopName_label = DesignClasses.makeJLabel_with_background_font_size_width_color(gruopName, margin_left, 5, label_width, 25, 12, 1, Color.WHITE, Color.WHITE, 2);
        gruopName_label.setBackground(null);
        //   gruopName_label.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, DefaultSettings.blue_bar_background));
        rowPanel.add(gruopName_label);
        return rowPanel;
    }

    private void send_update_msg_in_group(Long gruop_id) {
        //   CreateGroup.getInstance().getGroupMembers().clear();
        CreateGroup grpClass = new CreateGroup();
        for (String key : TempGruoupContainer.getInstance().getGroupMemberContainer().keySet()) {
            NewGruopFields grpMapp = new NewGruopFields();
            grpMapp.setUserIdentity(key);
            grpMapp.setAdmin(TempGruoupContainer.getInstance().getGroupMemberContainer().get(key));
            //   System.out.println("key: in line 517" + grpMapp.getUserIdentity());
            grpClass.getGroupMembers().add(grpMapp);
        }
        /*
         "action":"UPDATE",
         "type":"edit_group_member",
         "packetId":"packetId",    //generated by source 
         "sessionId":"sessionid",
         "groupId":"45",
         "groupMembers":
         */

        //   if (CreateGroup.getInstance().getGroupMembers() != null) {
        String pakId = PackageActions.create_packed_id_for(GroupConstants.TYPE_edit_group_member);

        grpClass.setAction(SocketConstants.UPDATE);
        grpClass.setType(GroupConstants.TYPE_edit_group_member);
        grpClass.setPacketId(pakId);
        grpClass.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        grpClass.setGroupId(gruop_id);
        //   grpClass.setGroupMembers(CreateGroup.getInstance().getGroupMembers());
        // String response = new Gson().toJson(grpClass);
        TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        pak_action.send_And_Store_Packed_As_String(grpClass, pakId);
        delete_save_button_in_buttom();

        // }

    }

    private void send_group_delete_request(Long gruop_id) {
        String pakId = PackageActions.create_packed_id_for(GroupConstants.TYPE_delete_group);
        JsonFields grpClass = new JsonFields();
        grpClass.setAction(SocketConstants.UPDATE);
        grpClass.setType(GroupConstants.TYPE_delete_group);
        grpClass.setPacketId(pakId);
        grpClass.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        grpClass.setGroupId(gruop_id);
        //  String response = new Gson().toJson(grpClass);
        //System.out.println("respnose " + response);
        pak_action.send_And_Store_Packed_As_String(grpClass, pakId);
        delete_save_button_in_buttom();

    }

    private void send_remove_gruop_member_msg(Long gruop_id, String userID) {
        CreateGroup grpClass = new CreateGroup();
        NewGruopFields grpMapp = new NewGruopFields();
        grpMapp.setUserIdentity(userID);
//        System.out.println("user id for remove " + userID);
        grpClass.getGroupMembers().add(grpMapp);
        String pakId = PackageActions.create_packed_id_for(GroupConstants.TYPE_remove_group_member);

        grpClass.setAction(SocketConstants.UPDATE);
        grpClass.setType(GroupConstants.TYPE_remove_group_member);
        grpClass.setPacketId(pakId);
        grpClass.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        grpClass.setGroupId(gruop_id);
        //String response = new Gson().toJson(grpClass);
        pak_action.send_And_Store_Packed_As_String(grpClass, pakId);
        delete_save_button_in_buttom();
    }

    private void send_add_group_member_request(Long groupId) {
        //  CreateGroup.getInstance().getGroupMembers().clear();
        String pakId = PackageActions.create_packed_id_for(GroupConstants.TYPE_add_group_member);
        CreateGroup grpClass = new CreateGroup();
        // System.out.println("hash size------>" + TempGruoupContainer.getInstance().getGroupMemberContainer().size());
        for (String key : TempGruoupContainer.getInstance().getGroupMemberContainer().keySet()) {
            NewGruopFields grpMapp = new NewGruopFields();
            grpMapp.setUserIdentity(key);
            grpMapp.setAdmin(TempGruoupContainer.getInstance().getGroupMemberContainer().get(key));
            //   System.out.println("key: in line 517" + grpMapp.getUserIdentity());
            grpClass.getGroupMembers().add(grpMapp);
        }


        //CreateGroup grpClass = new CreateGroup();
        grpClass.setAction(SocketConstants.UPDATE);
        grpClass.setType(GroupConstants.TYPE_add_group_member);
        grpClass.setPacketId(pakId);
        grpClass.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        grpClass.setGroupId(groupId);
        //  String response = new Gson().toJson(grpClass);
        pak_action.send_And_Store_Packed_As_String(grpClass, pakId);
        //   grpClass.setGroupMembers(CreateGroup.getInstance().getGroupMembers());

        //   System.out.println(response);

        //  TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        //  System.out.println(TempGruoupContainer.getInstance().getGroupMemberContainer().size());


    }

    private void send_leave_group_request(Long gruop_id) {
        JsonFields fields_fetch_group = new JsonFields();
        fields_fetch_group.setAction(SocketConstants.UPDATE);
        fields_fetch_group.setType(GroupConstants.TYPE_leave_group);
        String pakage_id_group = PackageActions.create_packed_id_for(GroupConstants.TYPE_leave_group);
        fields_fetch_group.setPacketId(pakage_id_group);
        fields_fetch_group.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        fields_fetch_group.setGroupId(gruop_id);
        //   System.out.println("secoosdfsd fnsdlkfsdljflsd fjkjsdflj sldfjlsd***************");
        //  pak_action.sendPkdAndStore(fields2, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT, pakage_id);
        //    String response3 = new Gson().toJson(fields_fetch_group);
        pak_action.send_And_Store_Packed_As_String(fields_fetch_group, pakage_id_group);
    }
}
