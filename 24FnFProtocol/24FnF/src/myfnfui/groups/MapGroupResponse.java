/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import java.util.ArrayList;
import java.util.List;
import myfnfui.FeedBackFields;

/**
 *
 * @author user
 */
public class MapGroupResponse extends FeedBackFields {

    private List<GroupDto> groupList = new ArrayList<GroupDto>();

    public List<GroupDto> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<GroupDto> groupList) {
        this.groupList = groupList;
    }
}
