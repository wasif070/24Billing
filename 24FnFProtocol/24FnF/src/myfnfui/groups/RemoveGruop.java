/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class RemoveGruop extends GroupMapper {

    private ArrayList<NewGruopFields> removedMembers = new ArrayList<NewGruopFields>();

    public ArrayList<NewGruopFields> getGroupMembers() {
        return removedMembers;
    }

    public void setGroupMembers(ArrayList<NewGruopFields> removedMembers) {
        this.removedMembers = removedMembers;
    }
}
