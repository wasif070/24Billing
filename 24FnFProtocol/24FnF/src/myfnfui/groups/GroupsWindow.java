
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import helpers.APIParametersFiles;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticImages;
import helpers.UiMethods;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import local.ua.UserAgentProfile;
import myfnfui.LightScrollPane;
import myfnfui.MainFrame;
import myfnfui.groups.grouphelper.PreviousGruopHelpers;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author user
 */
public class GroupsWindow extends MainFrame {

    public static Long currentSelectedGruop = null;
//    JPanel create_new_group;
//    public static GroupsWindow groupWindow;
//
//    public static GroupsWindow getGroupWindow() {
//        return groupWindow;
//    }
//
//    public static void setGroupWindow(GroupsWindow groupWindow) {
//        GroupsWindow.groupWindow = groupWindow;
//    }
    public static ArrayList<GroupsWindow> listOfGroupsWindowObjects = new ArrayList<GroupsWindow>();
    private JPanel tabedPanel;
    JButton newGroupButton;
    JButton previousTab;
    int action_index = 1;
    int profiel_index = 2;
    JPanel tabConentContainer;
    LightScrollPane scrollPane;
    LightScrollPane scrollPane_for_selected_friend;
    HelperMethods helperMethods = new HelperMethods();
    int second_menu_bar = 34;
    int container_window_height = 0;
    JTextField group_Name_text_field;
    private JLabel search_image;
    private JTextField searchTextField_group;
    String default_mobile_text = "Search";
    public static JPanel freind_detials_panel;
    public static JPanel selected_friend_list;
    public static JPanel crate_gruop_action_panel;
    public static JPanel admin_delete_text_panel_for_new_group;
    public static JPanel save_button_panel_for_new_group;
    /*gruop*/
//    public static JPanel gruop_list_container;
    public static JPanel gruop_panel;
    public static JPanel gruop_members_panel;
    public static JLabel gruop_name_label;
    public static JPanel gruop_actions_panel;
    public static JPanel gruop_save_button_panel;
    public static JPanel add_group_member_save_cancle_button_panel;
    public static JPanel no_group_window;
    public static LightScrollPane scrollPane_for_gruop;
    UiMethods extra_methods;
    public static GroupsWindow groupWindow;
    PackageActions pak_action = new PackageActions();
    JButton send_group_request;
    JButton add_new_member;
    /**/
    int top_height = 20;
    int margin_left = 20;
    int value_font_size = 12;
    int label_height = 25;
    int lable_WIDTH = 80;
    int text_box_width = 150;
    int frinedlist_height = 180;
    int containers_width = 270;
    PreviousGruopHelpers dd = new PreviousGruopHelpers();
    int button_width = 70;
    int text_label_height = 22;

    public static GroupsWindow getGroupWindow() {
        return groupWindow;
    }

    public static void setGroupWindow(GroupsWindow groupWindow) {
        GroupsWindow.groupWindow = groupWindow;
    }

    public GroupsWindow() {
        extra_methods = new UiMethods();
        //   setGroupWindow(this);
        listOfGroupsWindowObjects.add(this);
        setGroupWindow(this);
        int relative_locations_of_groups_window = listOfGroupsWindowObjects.size() * 5;
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + DefaultSettings.DEFAULT_WIDTH + 3 + relative_locations_of_groups_window, DefaultSettings.DEFAULT_LOCATION_TOP + relative_locations_of_groups_window);
        this.setSize(DefaultSettings.DEFAULT_WIDTH, DefaultSettings.DEFAULT_HEIGHT);
        tabedPanel = new JPanel(new GridLayout(1, 2, 1, 1));
        tabedPanel.setBounds(3, DefaultSettings.DEFAULT_TOP_BAR + 3, DefaultSettings.DEFAULT_WIDTH - 6, second_menu_bar);
        tabedPanel.setBackground(Color.WHITE);
        main_container.add(tabedPanel);
        //     tabConentContainer = new JPanel();


        addWindowListener(
                new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {

                listOfGroupsWindowObjects.remove(this);
                dispose();
            }
        });
        JPanel borderlaoutpanel = new JPanel();
        scrollPane = new LightScrollPane(borderlaoutpanel);
        container_window_height = DefaultSettings.DEFAULT_HEIGHT - (DefaultSettings.DEFAULT_TOP_BAR + second_menu_bar + 10);
        scrollPane.setBackground(Color.WHITE);
        scrollPane.setBounds(2, DefaultSettings.DEFAULT_TOP_BAR + second_menu_bar + 3, 298, container_window_height);
        borderlaoutpanel.setLayout(new BorderLayout(0, 0));
        borderlaoutpanel.setBackground(Color.WHITE);
        main_container.add(scrollPane);
        tabConentContainer = new JPanel();
        borderlaoutpanel.add(tabConentContainer, BorderLayout.NORTH);
        tabConentContainer.setLayout(new GridLayout(0, 1, 0, 1));
        tabConentContainer.setBackground(Color.WHITE);
        tabConentContainer.add(previos_history());

        //newGroupButton = DesignClasses.create_button_with_text_color_bgColor("Create New Group", 11, 1, 22, 22, 150, 22, Color.BLACK, DefaultSettings.topTab_background);
        newGroupButton = DesignClasses.create_image_button_with_text_scale6("Create New Group", 11, 1, Color.BLACK);
//        actionButton.setBackground(DefaultSettings.blue_bar_background);
//        actionButton.setForeground(Color.WHITE);
        newGroupButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button_click_color_change(action_index);
                drawNewWindow();

            }
        });

        previousTab = DesignClasses.create_image_button_with_text_scale6("Groups", 11, 1, Color.BLACK);
        DesignClasses.change_image(StaticImages.Scale_6_h, previousTab);
        tabedPanel.add(previousTab);
        tabedPanel.add(newGroupButton);
        previousTab.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button_click_color_change(profiel_index);
                drawPreviouswindow();

            }
        });
    }

    public void drawNewWindow() {
        currentSelectedGruop = null;
        tabConentContainer.removeAll();
        tabConentContainer.add(createNewGroup());
        tabConentContainer.revalidate();
        tabConentContainer.repaint();
    }

    public void drawPreviouswindow() {
        currentSelectedGruop = null;
        tabConentContainer.removeAll();
        tabConentContainer.add(previos_history());
        tabConentContainer.revalidate();
        tabConentContainer.repaint();
    }

    public JPanel createNewGroup() {
        if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        }

        top_height = 20;
        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(280, container_window_height));
        rowPanel.setLayout(null);
        rowPanel.setBackground(null);

        rowPanel.add(DesignClasses.makeJLabel_with_background_font_size_width_color("Group Name ", margin_left, top_height, lable_WIDTH, label_height, value_font_size, 1, DefaultSettings.text_color1, Color.WHITE, 2));
        group_Name_text_field = DesignClasses.makeTextField("", margin_left + lable_WIDTH + 5, top_height, text_box_width);
        rowPanel.add(DesignClasses.makeJLabel_with_background_font_size_width_color("Choose Friends ", margin_left, top_height + label_height + 10, lable_WIDTH + 100, label_height, 13, 1, DefaultSettings.text_color2, Color.WHITE, 2));
        rowPanel.add(group_Name_text_field);
        int p_width = 278;
        int search_box_left = 0;
        int search_image_width = 25;
        JPanel p = new JPanel(new FlowLayout(2, 0, 3));
        p.setBackground(null);
        p.setBounds(12, top_height + label_height + 40, p_width, 28);



        search_image = DesignClasses.create_image_label(search_box_left, 0, search_image_width, DefaultSettings.texBoxHeight, StaticImages.SEARCH_IMG);
        search_image.setOpaque(true);
        search_image.setBorder(BorderFactory.createMatteBorder(1, 1, 0, 0, DefaultSettings.blue_border_color));
        search_image.setPreferredSize(new Dimension(search_image_width, 25));
        p.add(search_image);


        //     main_container.add(search_image);
        searchTextField_group = DesignClasses.makeTextField(default_mobile_text, search_box_left, 0, DefaultSettings.searchtextBoxWidth);
        searchTextField_group.setForeground(DefaultSettings.disable_font_color);
        searchTextField_group.setBorder(BorderFactory.createMatteBorder(1, 0, 0, 1, DefaultSettings.blue_border_color));
        //    main_container.add(searchTextField);

        searchTextField_group.setPreferredSize(new Dimension((p_width - search_image_width) - 8, 25));
        //headerMenuContainer.add(searchTextField);
        p.add(searchTextField_group);
        rowPanel.add(p);
        searchTextField_group.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                extra_methods.set_reset_defalut_text(searchTextField_group, default_mobile_text, true);
            }

            public void focusGained(FocusEvent e) {
                if (searchTextField_group.getText().length() <= 0 || searchTextField_group.getText().toString().equals(default_mobile_text)) {
                    extra_methods.set_reset_defalut_text(searchTextField_group, default_mobile_text, false);
                } else if (searchTextField_group.getText().toString().length() < 1) {
                    extra_methods.set_reset_defalut_text(searchTextField_group, default_mobile_text, true);
                }
            }
        });
        searchTextField_group.requestFocusInWindow();
        searchTextField_group.grabFocus();
        searchTextField_group.requestFocus();
        searchTextField_group.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                draw_frinedlist();
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                draw_frinedlist();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                draw_frinedlist();
            }
        });

        JPanel borderlaoutpanel = new JPanel();
        scrollPane = new LightScrollPane(borderlaoutpanel);
        scrollPane.setBounds(margin_left, top_height = top_height + label_height + 68, containers_width, frinedlist_height);
        scrollPane.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, DefaultSettings.blue_border_color));

        borderlaoutpanel.setLayout(new BorderLayout(0, 0));
        borderlaoutpanel.setBackground(Color.WHITE);
        rowPanel.add(scrollPane);

        freind_detials_panel = new JPanel();
        borderlaoutpanel.add(freind_detials_panel, BorderLayout.NORTH);
        freind_detials_panel.setLayout(new GridLayout(0, 1, 0, 1));
        draw_frinedlist();


        admin_delete_text_panel_for_new_group = new JPanel();
        admin_delete_text_panel_for_new_group.setBounds(margin_left, top_height = top_height + frinedlist_height + 12, containers_width, text_label_height);
        admin_delete_text_panel_for_new_group.setLayout(new BorderLayout(0, 0));
        admin_delete_text_panel_for_new_group.setBackground(Color.WHITE);
        rowPanel.add(admin_delete_text_panel_for_new_group);


        JPanel selected_frinedPanel = new JPanel();
        scrollPane_for_selected_friend = new LightScrollPane(selected_frinedPanel);
        int select_container_height = frinedlist_height - 30;
        scrollPane_for_selected_friend.setBounds(margin_left, top_height = top_height + text_label_height + 5, containers_width, select_container_height);
        scrollPane_for_selected_friend.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, DefaultSettings.blue_bar_background));
        //    scrollPane_for_selected_friend.setBackground(Color.white);
        selected_frinedPanel.setLayout(new BorderLayout(0, 0));
        selected_frinedPanel.setBackground(Color.WHITE);
        rowPanel.add(scrollPane_for_selected_friend);

        crate_gruop_action_panel = new JPanel();

        selected_friend_list = new JPanel();
        selected_frinedPanel.add(selected_friend_list, BorderLayout.NORTH);
        selected_friend_list.setLayout(new GridLayout(0, 1, 0, 1));
        //   selected_friend_list.setBackground(Color.WHITE);
        draw_selected_frinedlist();

        save_button_panel_for_new_group = new JPanel();
        save_button_panel_for_new_group.setBounds(margin_left, top_height + select_container_height, containers_width, 35);
        save_button_panel_for_new_group.setLayout(new BorderLayout(0, 0));
        save_button_panel_for_new_group.setBackground(Color.WHITE);
        rowPanel.add(save_button_panel_for_new_group);

//        send_group_request = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Save Gruop", 11, 1, margin_left + (containers_width / 2) - 30, top_height + select_container_height + 5, 74, 22);
//        rowPanel.add(send_group_request);
//        send_group_request.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//                if (group_Name_text_field.getText().toString().length() >= 3) {
//                    if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
//                        send_group_request.setEnabled(false);
//                        create_new_gruop_request();
////                        for (String friend_id : CreateGroup.getInstance().getGroupMemberContainer().keySet()) {
////                        }
//                    } else {
//                        HelperMethods.create_confrim_panel_no_cancel("Friend Not Added", 80);
//                    }
//                } else {
//                    HelperMethods.create_confrim_panel_no_cancel("Invalid Group Name", 80);
//                }
//            }
//        });

        return rowPanel;
    }

    private void draw_frinedlist() {

        //   System.out.println("ddd");
        freind_detials_panel.removeAll();
        //  freind_detials_panel.add(createNewGroup());

        int row = 0;
        Color clr = Color.WHITE;
        for (String key : FriendList.getInstance().getFriend_hash_map().keySet()) {
            if (TempGruoupContainer.getInstance().getGroupMemberContainer().get(key) == null) {
                UserAgentProfile single_profile = FriendList.getInstance().getFriend_hash_map().get(key);
                if (single_profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
                    if (searchTextField_group.getText().toString().length() <= 0 || searchTextField_group.getText().toString().equals(default_mobile_text)) {
                        //   System.out.println("key:" + key);

                        if (single_profile != null) {
                            if (row % 2 != 0) {

                                //   System.out.println("ddd");
                                clr = DefaultSettings.table_odd_background;
                            } else {
                                //  System.out.println("eee");
                                // freind_detials_panel.setBackground(null);
                                freind_detials_panel.setBackground(DefaultSettings.friend_list_border_color);
                                clr = DefaultSettings.table_odd_background;

                            }
                            freind_detials_panel.add(createSingleFriends(single_profile, clr, 35));

                        }

                    } else if (HelperMethods.check_string_contains_substring(single_profile.getFirstName(), searchTextField_group.getText()) || HelperMethods.check_string_contains_substring(single_profile.getUserIdentity(), searchTextField_group.getText().toString())) {
                        freind_detials_panel.add(createSingleFriends(single_profile, clr, 35));
                    }

                    row++;
                }
            }
        }
        freind_detials_panel.revalidate();
        freind_detials_panel.repaint();
    }

    private void draw_selected_frinedlist() {

        //   if (TempGruoupContainer.getInstance().getGroupMemberContainer().size() > 0) {
        show_admin_delete_text_panel();
        //   }
        selected_friend_list.removeAll();
        //  freind_detials_panel.add(createNewGroup());

        int row = 0;
        Color clr = Color.WHITE;
        if (TempGruoupContainer.getInstance().getGroupMemberContainer() != null) {
            for (String key : TempGruoupContainer.getInstance().getGroupMemberContainer().keySet()) {
                //     System.out.println(key);
                UserAgentProfile single_profile = FriendList.getInstance().getFriend_hash_map().get(key);
                if (single_profile != null) {
                    selected_friend_list.add(create_selected_SingleFriends(single_profile, clr, 36));
                }
                row++;
            }
        }
        selected_friend_list.revalidate();
        selected_friend_list.repaint();
    }

    private JPanel createSingleFriends(final UserAgentProfile single_p, Color clr, int height) {
        //   System.out.println("in line 338:" + single_p.getFirstName());
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, height));
        int container_width = 170;
        JPanel container = new JPanel(new GridLayout(2, 1, 0, 0));
        container.setBounds(40, 0, container_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);
        //container.setBackground(Color.CYAN);
        //  container.addMouseListener(this);
        container.setFocusable(true);
        JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(single_p.getFirstName() + " " + single_p.getLastName(), 10, 0, 100, 12, 0, DefaultSettings.text_color1);

        JLabel whatLable = new JLabel(single_p.getUserIdentity());
        //  whatLable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container.add(user_name_button);
        container.add(whatLable);
        //  rowPanel.setBackground(Color.white);
        rowPanel.add(container);
        int image_width = 35;
        final JButton add_inGrp = DesignClasses.create_image_button_with_text_scale1("Add", container_width + image_width + 5, 5);
        rowPanel.add(add_inGrp);

        add_inGrp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                add_inGrp.setEnabled(false);
                TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), false);
                //      System.out.println("in 403:" + TempGruoupContainer.getInstance().getGroupMemberContainer().get(single_p.getUserIdentity()));
                draw_selected_frinedlist();
                draw_frinedlist();
                create_new_group_save_panel();


            }
        });
        rowPanel.setBackground(clr);
        JLabel user_image = helperMethods.create_image_from_url(single_p.getGender(), single_p.getProfileImage(), 2, 0, image_width, image_width);
        rowPanel.add(user_image);


        return rowPanel;
    }

    private JPanel create_selected_SingleFriends(final UserAgentProfile single_p, Color clr, int height) {
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        int friend_conaiter = (containers_width / 2) + 10;
        int gap = 10;
        rowPanel.setPreferredSize(new Dimension(friend_conaiter, height));
        int container_width = 130;
        JPanel container = new JPanel(new GridLayout(2, 1, 0, 0));
        container.setBounds(40, 0, container_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);
        //container.setBackground(Color.CYAN);
        //  container.addMouseListener(this);

        String f_name = single_p.getFirstName();
        String l_name = single_p.getLastName();
        String gender = single_p.getGender();
        //  System.out.println("f_name-***************************** "+f_name);
        if (f_name == null) {
            // System.out.println("f_name-***************************** "+f_name);
            f_name = FriendList.getInstance().getFriend_hash_map().get(single_p.getUserIdentity()).getFirstName();
            l_name = FriendList.getInstance().getFriend_hash_map().get(single_p.getUserIdentity()).getLastName();
            gender = FriendList.getInstance().getFriend_hash_map().get(single_p.getUserIdentity()).getGender();
        }

        container.setFocusable(true);
        JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(f_name + " " + l_name, 10, 0, 100, 10, 1, DefaultSettings.blue_bar_background);

        JLabel whatLable = new JLabel(single_p.getUserIdentity());
        //  whatLable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container.add(user_name_button);
        container.add(whatLable);
        //  rowPanel.setBackground(Color.white);
        rowPanel.add(container);
        int image_width = 35;
        int check_box_width = 25;

        final JCheckBox saveBox = new JCheckBox("");
        saveBox.setBounds(margin_left + friend_conaiter + gap, 5, check_box_width, 20);
        saveBox.setBackground(null);
        Boolean admin_true = TempGruoupContainer.getInstance().getGroupMemberContainer().get(single_p.getUserIdentity());
        //System.out.println("Booolean:" + admin_true);
        saveBox.setSelected(admin_true);

        saveBox.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
        saveBox.addItemListener(
                new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    //       System.out.println("selected");
                    TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), true);

                }
                if (e.getStateChange() != ItemEvent.SELECTED) {
                    TempGruoupContainer.getInstance().getGroupMemberContainer().put(single_p.getUserIdentity(), false);
                }
                // Set "ignore" whenever box is checked or unchecked.
                //ignore = (e.getStateChange() == ItemEvent.SELECTED);
            }
        });

        rowPanel.add(saveBox);

        final JButton delete_button = DesignClasses.create_image_button_with_text_scale1("Delete", margin_left + friend_conaiter + (gap * 2) + check_box_width, 5);
        rowPanel.add(delete_button);
        delete_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TempGruoupContainer.getInstance().getGroupMemberContainer().remove(single_p.getUserIdentity());
                draw_selected_frinedlist();
                draw_frinedlist();
                create_new_group_save_panel();
                show_admin_delete_text_panel();
            }
        });


        JLabel user_image = helperMethods.create_image_from_url(gender, single_p.getProfileImage(), 2, 0, image_width, image_width);
        rowPanel.add(user_image);
        rowPanel.setBackground(clr);
        return rowPanel;
    }

    public JPanel previos_history() {
        if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
        }
        top_height = 20;
        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(280, container_window_height));
        rowPanel.setLayout(null);
        rowPanel.setBackground(Color.WHITE);
        //  int top_height = 50;
//        int border_width = 200;
//        int margin_left = 20;
//        int label_height_increment_factor = 50;
//        int border_height_increment_factor = 40;
//        int lable_WIDTH = 110;
//        int margin_left_in_text = margin_left + 20;

//        add_new_member = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Add new Member", 11, 1, margin_left + (containers_width / 2) - 30, top_height, 74, 22);
//        rowPanel.add(add_new_member);

        JPanel gruop_list_container = new JPanel();
        scrollPane_for_gruop = new LightScrollPane(gruop_list_container);
        scrollPane_for_gruop.setBounds(margin_left, top_height = top_height + 25, containers_width, frinedlist_height);
        scrollPane_for_gruop.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, DefaultSettings.blue_border_color));

        gruop_list_container.setLayout(new BorderLayout(0, 0));
        gruop_list_container.setBackground(Color.WHITE);
        rowPanel.add(scrollPane_for_gruop);

        gruop_panel = new JPanel();
        // gruop_panel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, DefaultSettings.blue_border_color));
        gruop_list_container.add(gruop_panel, BorderLayout.NORTH);
        gruop_panel.setLayout(new GridLayout(0, 1, 0, 1));

        dd.draw_grouplist();


        no_group_window = new JPanel();
        no_group_window.setBounds(margin_left, top_height + 7, containers_width, 60);
        no_group_window.setLayout(new BorderLayout(0, 0));
        no_group_window.setBackground(null);
        rowPanel.add(no_group_window);
        //  gruop_name_label = DesignClasses.makeJLabel_text_color1("", margin_left, top_height = top_height + frinedlist_height + 20, containers_width, 13);
        // gruop_name_label.setForeground(DefaultSettings.blue_bar_background);
        // rowPanel.add(gruop_name_label);

        int select_container_height = frinedlist_height;
        int action_panel_height = 35;
        gruop_actions_panel = new JPanel();
        gruop_actions_panel.setBounds(margin_left, top_height = top_height + frinedlist_height + 25, containers_width, action_panel_height);
        gruop_actions_panel.setLayout(new BorderLayout(0, 0));
        gruop_actions_panel.setBackground(null);
        rowPanel.add(gruop_actions_panel);

        JPanel gruop_member_con = new JPanel();
        LightScrollPane gruop_details = new LightScrollPane(gruop_member_con);
        //    int select_container_height = frinedlist_height;
        gruop_details.setBounds(margin_left, top_height = top_height + action_panel_height, containers_width, select_container_height);
        //   gruop_details.setBorder(BorderFactory.createMatteBorder(0, 1, 1, 1, DefaultSettings.disable_font_color));
        //    scrollPane_for_selected_friend.setBackground(Color.white);
        gruop_member_con.setLayout(new BorderLayout(0, 0));
        gruop_member_con.setBackground(Color.WHITE);
        rowPanel.add(gruop_details);



        gruop_members_panel = new JPanel();
        gruop_member_con.add(gruop_members_panel, BorderLayout.NORTH);
        gruop_members_panel.setLayout(new GridLayout(0, 1, 0, 1));

        gruop_save_button_panel = new JPanel();
        gruop_save_button_panel.setBounds(margin_left, top_height = top_height + select_container_height + 7, containers_width, action_panel_height);
        gruop_save_button_panel.setLayout(new BorderLayout(0, 0));
        gruop_save_button_panel.setBackground(null);
        rowPanel.add(gruop_save_button_panel);



        add_group_member_save_cancle_button_panel = new JPanel();
        add_group_member_save_cancle_button_panel.setBounds(margin_left, top_height, containers_width, action_panel_height);
        add_group_member_save_cancle_button_panel.setLayout(new BorderLayout(0, 0));
        add_group_member_save_cancle_button_panel.setBackground(null);
        // JButton save_button = DesignClasses.create_image_button_with_text_scale1("Save", 270 - 40, 8);
        // gruop_save_button_panel.add(save_button);
        rowPanel.add(add_group_member_save_cancle_button_panel);
        return rowPanel;
    }

    private void button_click_color_change(int index) {
        if (index == action_index) {
//            newGroupButton.setBackground(DefaultSettings.topTab_background_hover);
//            previousTab.setBackground(DefaultSettings.topTab_background);
            DesignClasses.change_image(StaticImages.Scale_6_h, newGroupButton);
            DesignClasses.change_image(StaticImages.Scale_6, previousTab);
        }
        if (index == profiel_index) {
//            newGroupButton.setBackground(DefaultSettings.topTab_background);
//            previousTab.setBackground(DefaultSettings.topTab_background_hover);
            DesignClasses.change_image(StaticImages.Scale_6, newGroupButton);
            DesignClasses.change_image(StaticImages.Scale_6_h, previousTab);
        }
    }

    public void dispose_group_windows() {

        GroupsWindow.groupWindow = null;
        currentSelectedGruop = null;
        clear_selected_members();
        this.setVisible(false);
        this.dispose();



    }

    public void show_group_windows() {
        this.setVisible(true);
    }

    public void set_in_front_group_windows() {
        //   this.setState(NORMAL);
        if (this.getState() == ICONIFIED) {
            this.setState(NORMAL);
        }
    }

    public void clear_selected_members() {
        TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
    }

    private void create_new_gruop_request() {
        //  CreateGroup.getInstance().getGroupMembers().clear();
        CreateGroup grpClass = new CreateGroup();
        for (String key : TempGruoupContainer.getInstance().getGroupMemberContainer().keySet()) {
            NewGruopFields grpMapp = new NewGruopFields();
            grpMapp.setUserIdentity(key);
            grpMapp.setAdmin(TempGruoupContainer.getInstance().getGroupMemberContainer().get(key));
            //   System.out.println("key: in line 517" + grpMapp.getUserIdentity());
            grpClass.getGroupMembers().add(grpMapp);
        }
        //  if (CreateGroup.getInstance().getGroupMembers() != null) {
        String pakId = PackageActions.create_packed_id_for(GroupConstants.TYPE_create_group);
        grpClass.setAction(SocketConstants.UPDATE);
        grpClass.setType(GroupConstants.TYPE_create_group);
        grpClass.setPacketId(pakId);
        grpClass.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        grpClass.setGroupName(group_Name_text_field.getText());

        pak_action.send_And_Store_Packed_As_String(grpClass, pakId);
        clear_selected_members();
        create_new_group_save_panel();
        draw_selected_frinedlist();
        show_admin_delete_text_panel();


    }

    public void create_new_group_save_panel() {

        save_button_panel_for_new_group.removeAll();

        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setPreferredSize(new Dimension(200, 35));
        rowPanel.setBackground(Color.white);
        int left = 130;
        JLabel number_of_change = DesignClasses.makeJLabel_with_text_color("Seleced : " + TempGruoupContainer.getInstance().getGroupMemberContainer().size(), 0, 2, 100, 11, 0, Color.BLUE);

        final JButton save_button = DesignClasses.create_button_with_text_color_bgColor("Save", 10, 1, left, 2, button_width, 20, Color.WHITE, new Color(0x666633));
        save_button.setEnabled(true);
        save_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              //  save_button.setEnabled(false);

                if (group_Name_text_field.getText().toString().length() >= 3) {
                    if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
                        //   save_button.setEnabled(false);
                        create_new_gruop_request();
//                        for (String friend_id : CreateGroup.getInstance().getGroupMemberContainer().keySet()) {
//                        }
                    } else {
                        HelperMethods.create_confrim_panel_no_cancel("Friend Not Added", 80);
                    }
                } else {
                    HelperMethods.create_confrim_panel_no_cancel("Invalid Group Name", 80);
                }

            }
        });
        final JButton calcel_button = DesignClasses.create_button_with_text_color_bgColor("Cancel", 10, 1, left + button_width + 2, 2, button_width, 20, Color.WHITE, Color.red);

        calcel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                clear_selected_members();
                create_new_group_save_panel();
                draw_selected_frinedlist();
                show_admin_delete_text_panel();
                // draw_frinedlist();
                //  draw_group_details(gruopid);
                // hide_save_canecl_panel_in_buttom();
            }
        });

//        send_group_request = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Save Gruop", 11, 1, margin_left + (containers_width / 2) - 30, top_height + select_container_height + 5, 74, 22);
//        rowPanel.add(send_group_request);
//        send_group_request.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//
//                if (group_Name_text_field.getText().toString().length() >= 3) {
//                    if (!TempGruoupContainer.getInstance().getGroupMemberContainer().isEmpty()) {
//                        send_group_request.setEnabled(false);
//                        create_new_gruop_request();
////                        for (String friend_id : CreateGroup.getInstance().getGroupMemberContainer().keySet()) {
////                        }
//                    } else {
//                        HelperMethods.create_confrim_panel_no_cancel("Friend Not Added", 80);
//                    }
//                } else {
//                    HelperMethods.create_confrim_panel_no_cancel("Invalid Group Name", 80);
//                }
//            }
//        });
        if (TempGruoupContainer.getInstance().getGroupMemberContainer().size() > 0) {
            rowPanel.add(number_of_change);
            rowPanel.add(calcel_button);
            rowPanel.add(save_button);
        }
        save_button_panel_for_new_group.add(rowPanel);


        save_button_panel_for_new_group.repaint();
        save_button_panel_for_new_group.revalidate();

    }

    private void show_admin_delete_text_panel() {


        admin_delete_text_panel_for_new_group.removeAll();
        //  save_button_panel_for_new_group.revalidate();
        JPanel rowPanel = new JPanel();
        rowPanel.setLayout(null);
        rowPanel.setBackground(DefaultSettings.blue_bar_background);
        rowPanel.setPreferredSize(new Dimension(200, text_label_height));

        int admin_label_width = 40;
        int left = margin_left + 150;
        JLabel admin_text_label = DesignClasses.makeJLabel_with_background_font_size_width_color("Admin", left, 0, admin_label_width, text_label_height, 12, 0, Color.WHITE, null, 2);
        rowPanel.add(admin_text_label);
        JLabel action_text_label = DesignClasses.makeJLabel_with_background_font_size_width_color("Delete", left + 45, 0, admin_label_width, text_label_height, 12, 0, Color.WHITE, null, 2);
        rowPanel.add(action_text_label);
        if (TempGruoupContainer.getInstance().getGroupMemberContainer().size() > 0) {
            admin_delete_text_panel_for_new_group.add(rowPanel);
        }
        admin_delete_text_panel_for_new_group.repaint();
        admin_delete_text_panel_for_new_group.revalidate();
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            listOfGroupsWindowObjects.remove(this);
            this.dispose_group_windows();
            //  clear_selected_members();
            // dispose();

        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        }
    }
}
