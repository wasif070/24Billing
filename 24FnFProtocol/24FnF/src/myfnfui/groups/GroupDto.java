/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import javax.xml.bind.annotation.XmlRootElement;



import java.util.List;

/**
 *
 * @author nazmul
 */
@XmlRootElement
public class GroupDto {

    private String groupName;
    private long groupId;
    private List<GroupMemberDto> groupMembers;
    private String superAdmin;

    public String getSuperAdmin() {
        return superAdmin;
    }

    public void setSuperAdmin(String superAdmin) {
        this.superAdmin = superAdmin;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public List<GroupMemberDto> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(List<GroupMemberDto> groupMembers) {
        this.groupMembers = groupMembers;
    }
}
