/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author user
 */
public class TempGruoupContainer implements java.io.Serializable {

    public static TempGruoupContainer tempCotaier;

    public static TempGruoupContainer getInstance() {
        if (tempCotaier == null) {
            tempCotaier = new TempGruoupContainer();
        }
        return tempCotaier;
    }
    private Map<String, Boolean> groupMemberContainer = new ConcurrentHashMap<String, Boolean>();

    public Map<String, Boolean> getGroupMemberContainer() {
        return groupMemberContainer;
    }

    public void setGroupMemberContainer(Map<String, Boolean> groupMemberContainer) {
        this.groupMemberContainer = groupMemberContainer;
    }
    private Map<Long, GroupDto> gruopFriendContainer = new ConcurrentHashMap<Long, GroupDto>();

    public Map<Long, GroupDto> getGruopFriendContainer() {
        return gruopFriendContainer;
    }

    public void setGruopFriendContainer(Map<Long, GroupDto> gruopFriendContainer) {
        this.gruopFriendContainer = gruopFriendContainer;
    }

    public int get_friend_index_number_Members_stats(Long gruoupID, String user_id) {
        int index = 0;
        GroupDto dto = this.gruopFriendContainer.get(gruoupID);
        List<GroupMemberDto> ll = dto.getGroupMembers();
        for (int i = 0; i <= ll.size(); i++) {
            if (ll.get(i).getUserIdentity().equals(user_id)) {
                index = i;
                break;
            }
        }
        return index;
    }
//    private Map<String, Object> sendPakedContainer = new ConcurrentHashMap<String, Object>();
//
//    public Map<String, Object> getSendPakedContainer() {
//        return sendPakedContainer;
//    }
//
//    public void setSendPakedContainer(Map<String, Object> sendPakedContainer) {
//        this.sendPakedContainer = sendPakedContainer;
//    }
    private HashMap<String, Object> sendPakcInformations = new HashMap<String, Object>();

    public HashMap<String, Object> getSendPakcInformations() {
        return sendPakcInformations;
    }

    public void setSendPakcInformations(HashMap<String, Object> sendPakcInformations) {
        this.sendPakcInformations = sendPakcInformations;
    }
}
