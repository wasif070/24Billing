/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

/**
 *
 * @author user
 */
public class GroupConstants {

    public static final String TYPE_create_group = "create_group";
    public static final String TYPE_new_group = "new_group";
    public static final String TYPE_delete_group = "delete_group";
    public static final String TYPE_leave_group = "leave_group";
    public static final String TYPE_remove_group_member = "remove_group_member";
      public static final String TYPE_remove_a_group_member_by_other = "remove_a_group_member_by_other";
    public static final String TYPE_add_group_member = "add_group_member";
    public static final String TYPE_add_member_in_previous_list = "add_member_in_previous_list";
    public static final String TYPE_edit_group_member = "edit_group_member";
      public static final String TYPE_edit_members_by_others = "_edit_members_by_others";
    //  edit_group_member
    public static final String TYPE_groupList = "groupList";
//    public static final String TYPE_add_group_member = "add_group_member";
//    public static final String TYPE_add_group_member = "add_group_member";
    public static final String FIELDS_groupName = "groupName";
    public static final String FIELDS_groupMembers = "groupMembers";
    public static final String FIELDS_admin = "admin";
    public static final String FIELDS_groupId = "groupId";
}
