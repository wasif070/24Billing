/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import myfnfui.FeedBackFields;

/**
 *
 * @author user
 */
public class GroupMapper extends FeedBackFields {

    private String groupName;
    private String sessionId;
      private Long groupId;

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
}
