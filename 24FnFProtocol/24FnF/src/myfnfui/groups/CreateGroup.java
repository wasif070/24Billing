/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.groups;

import java.util.ArrayList;

/**
 *
 * @author user
 */
public class CreateGroup extends GroupMapper {

    public static CreateGroup createGroup;

    public static CreateGroup getInstance() {
        if (createGroup == null) {
            createGroup = new CreateGroup();
        }
        return createGroup;
    }
    private ArrayList<NewGruopFields> groupMembers = new ArrayList<NewGruopFields>();

    public ArrayList<NewGruopFields> getGroupMembers() {
        return groupMembers;
    }

    public void setGroupMembers(ArrayList<NewGruopFields> groupMembers) {
        this.groupMembers = groupMembers;
    }
}
