/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import myfnfui.usedThreads.SetDynamicFriend;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import helpers.APIParametersFiles;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticImages;
import imageDownload.DownLoaderHelps;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
//import local.ua.GraphicalUA;
import local.ua.UserAgentProfile;
import myfnfui.Messages.NotificationMessages;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author FaizAhmed
 */
public class RefreshFriendList {

    Gson jsonLib;
    SingleUserProfile single_user;
    JLabel whatLable;
    JLabel statusImage_label;
    PackageActions pak_action = new PackageActions();
    DownLoaderHelps dLHelp = new DownLoaderHelps();
    HelperMethods helperMethods = new HelperMethods();
//--------------------------------- preventing call from call log to a non-friend user -------------------------------    
    private static ArrayList<UserAgentProfile> myUpdatedFriendList = new ArrayList<UserAgentProfile>();
//--------------------------------- preventing call from call log to a non-friend user -------------------------------    

    public static void change_my_status_image() {
        String image_status = StaticImages.IMAGE_OFFLINE;
        GUI24FnF.online_image_top.removeAll();

        if (MyFnFSettings.isRegisterred && !MyFnFSettings.registrationStop) {
            image_status = StaticImages.IMAGE_ONLINE;
        } else {
            image_status = StaticImages.IMAGE_OFFLINE;
        }
        GUI24FnF.online_image_top.setIcon(DesignClasses.return_image(image_status));
        GUI24FnF.online_image_top.revalidate();
        GUI24FnF.online_image_top.repaint();
    }

    public synchronized void getfriend_list() {

        if (SetDynamicFriend.repaint_friendlist) {
            GUI24FnF.set_visivility_search_box(true);
            // GUI24FnF.flash_JLabel.setVisible(false);


//--------------------------------- preventing call from call log to a non-friend user -------------------------------        
            myUpdatedFriendList = new ArrayList<UserAgentProfile>();
//--------------------------------- preventing call from call log to a non-friend user -------------------------------        

            if (FriendList.getInstance().getFriend_hash_map() != null) {
                jsonLib = new GsonBuilder().serializeNulls().create();
                if (GUI24FnF.columnpanel != null) {
                    GUI24FnF.columnpanel.removeAll();
                }
                for (String key : FriendList.getInstance().getFriend_hash_map().keySet()) {
                    //  System.out.println("key:" + key);
                    UserAgentProfile p = FriendList.getInstance().getFriend_hash_map().get(key);
                    if (p != null) {
                        int current_status = 0;
                        int add_status = 0;
                        if (GUI24FnF.status_cobmob_box != null) {
                            if (GUI24FnF.status_cobmob_box.getSelectedItem() == MyFnFSettings.STATUS_ARRAY[0]) {
                            } else if (GUI24FnF.status_cobmob_box.getSelectedItem() == MyFnFSettings.STATUS_ARRAY[1]) {
                                current_status = APIParametersFiles.PRESENCE_ONLINE;
                            } else if (GUI24FnF.status_cobmob_box.getSelectedItem() == MyFnFSettings.STATUS_ARRAY[2]) {
                                current_status = APIParametersFiles.PRESENCE_OFFLINE;
                            } else if (GUI24FnF.status_cobmob_box.getSelectedItem() == MyFnFSettings.STATUS_ARRAY[3]) {
                                current_status = 3;
                                //   add_status = 1;
                            }
                        }
                        if (current_status == 0) {
                            add_status = 1;
                        } else if (p.getPresence() == current_status && p.getFriendShipStatus() == 1) {
                            add_status = 1;
                        } else if (current_status == 3) {
                            if (p.getFriendShipStatus() != APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED) {
                                add_status = 1;
                            }
                        }
                        if (GUI24FnF.searchTextField != null) {
                            if (GUI24FnF.searchTextField.getText().length() <= 0 || GUI24FnF.searchTextField.getText().toString().equals(GUI24FnF.default_mobile_text)) {
                                if (add_status == 1) {
                                    try {
                                        if (GUI24FnF.columnpanel != null) {

                                            GUI24FnF.columnpanel.add(createSingleFriends(p, p.getFriendShipStatus(), p.getPresence(), p.getFirstName(), p.getLastName(), p.getUserIdentity(), p.getCountry(), p.getWhatisInYourMind()));
                                        }
                                    } catch (Exception e) {
                                        //    System.out.println("" + e.getMessage());
                                        //  GUI24FnF.columnpanel.add(createSingleFriends(p, p.getFriendShipStatus(), p.getPresence(), p.getFirstName(), p.getLastName(), p.getUserIdentity(), p.getCountry(), p.getWhatisInYourMind()));
                                    }

//                                GUI24FnF.columnpanel.add(createSingleFriends(p, 3, 2, "sdfsd","hello", "dsf", "dsdf", "dsf"));
                                }
                            } else if (HelperMethods.check_string_contains_substring(p.getFirstName(), GUI24FnF.searchTextField.getText()) || HelperMethods.check_string_contains_substring(p.getUserIdentity(), GUI24FnF.searchTextField.getText())) {

                                if (add_status == 1) {
                                    if (GUI24FnF.columnpanel != null) {
                                        GUI24FnF.columnpanel.add(createSingleFriends(p, p.getFriendShipStatus(), p.getPresence(), p.getFirstName(), p.getLastName(), p.getUserIdentity(), p.getCountry(), p.getWhatisInYourMind()));
                                    }
                                }
                            }
                            myUpdatedFriendList.add(p);
                        }
                    }
                }
                if (GUI24FnF.columnpanel != null) {

                    GUI24FnF.columnpanel.revalidate();
                    GUI24FnF.columnpanel.repaint();
                }
                //   GUI24FnF.flash_JLabel.setVisible(false);


            } else {
                GUI24FnF.show_no_freind_panel();
            }
            if (SingleUserProfile.listOfSingleUserObjects.size() > 0) {
                UserAgentProfile p;
                for (int i = 0; i < SingleUserProfile.listOfSingleUserObjects.size(); i++) {
                    try {
                        p = matchedProfile(SingleUserProfile.listOfSingleUserObjects.get(i).getProfile());
                        if (p != null) {
                            if (p.getPresence() == 1) {
                                SingleUserProfile.listOfSingleUserObjects.get(i).getProfile().setPresence(1);
                                SingleUserProfile.listOfSingleUserObjects.get(i).call_p2p.setEnabled(false);
                                SingleUserProfile.listOfSingleUserObjects.get(i).repaint();
                            } else {
                                SingleUserProfile.listOfSingleUserObjects.get(i).getProfile().setPresence(p.getPresence());
                                SingleUserProfile.listOfSingleUserObjects.get(i).call_p2p.setEnabled(true);
                            }
                        }
                    } catch (Exception e) {
                        //   System.err.println("Exception in disabling call button in single_user_profile");
                    }
                }
            }


        } else {
        }
    }

    public JPanel createSingleFriends(final UserAgentProfile single_p, final Integer friendsihp_stataus, final Integer presence, final String firstName, final String lastName, final String user_id, final String country, final String what_mind) {

        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(230, 40));
        rowPanel.setLayout(null);
        // rowPanel.setBackground(Color.red);
        // JLabel label = new JLabel();
        String what_in_our_mind = what_mind;
        //ImageIcon contactImg = null;
        // URL imgURL = null;
        int bar_width = 165;
        String status_image = "";
        //   System.out.println("isregisterd" + MyFnFSettings.isRegisterred);
        if (friendsihp_stataus.equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
            bar_width = 270;

            if (presence.equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred && !MyFnFSettings.registrationStop) {
                status_image = StaticImages.IMAGE_ONLINE;
                //  imgURL = getClass().getClassLoader().getResource(StaticImages.IMAGE_ONLINE);
            } else {
                status_image = StaticImages.IMAGE_OFFLINE;
                //    imgURL = getClass().getClassLoader().getResource(StaticImages.IMAGE_OFFLINE);
            }
        } else if (friendsihp_stataus.equals(APIParametersFiles.FRIENDSHIP_STATUS_INCOMMING)) {
            bar_width = 155;
            what_in_our_mind = "Incomming Request";
            status_image = StaticImages.IMAGE_PENDING;
            // final JButton reject_button = DesignClasses.create_reject_buttons(220, 5);
            final JButton reject_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Reject", 9, 1, 238, 5, 50, 25);
            final JButton button_accept = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Accept", 9, 1, 188, 5, 50, 25);
            button_accept.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //   System.out.println("accept request");
                    button_accept.setEnabled(false);
                    reject_button.setEnabled(false);
                    String pak_id = PackageActions.create_packed_id_for(SocketConstants.ACCEPT_FRIEND);
                    JsonFields jFeilds = new JsonFields();
                    jFeilds.setAction(SocketConstants.UPDATE);
                    jFeilds.setType(SocketConstants.ACCEPT_FRIEND);
                    jFeilds.setUserIdentity(user_id);
                    jFeilds.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                    jFeilds.setPacketId(pak_id);
                    PackageActions.sendPacketAsString(jFeilds);
                }
            });
            rowPanel.add(button_accept);
            //createAction_button("", img2, 220, 5, 52, 25);
            reject_button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    HelperMethods.create_confrim_panel("Are you sure you want to reject?", 20);
                    if (HelperMethods.confirm == 1) {
                        PackageActions.send_remove_or_delete_request(user_id);
                    }


                }
            });
            rowPanel.add(reject_button);
        } else if (friendsihp_stataus.equals(APIParametersFiles.FRIENDSHIP_STATUS_SEND)) {
            bar_width = 155;
            status_image = StaticImages.IMAGE_PENDING;
            //  imgURL = getClass().getClassLoader().getResource(StaticImages.IMAGE_PENDING);
            what_in_our_mind = "Pending Request";
            // final JButton remove_button = DesignClasses.create_remove_buttons(220, 5);//createAction_button("", img_remove, 220, 5, 52, 25);
            final JButton remove_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Remove", 9, 1, 238, 5, 50, 25);
            remove_button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    HelperMethods.create_confrim_panel(NotificationMessages.REMOVE_NOTIFICAITON, 25);
                    if (HelperMethods.confirm == 1) {
                        PackageActions.send_remove_or_delete_request(user_id);

                    }

                }
            });
            rowPanel.add(remove_button);
        } else {
        }
        // final JButton user_name_button = DesignClasses.create_button_no_image_with_font_size_width(firstName + " " + lastName, 0, 10, 150, 13, 0);
        JLabel user_name_button = DesignClasses.makeJLabel_with_text_color(firstName + " " + lastName, 0, 10, 150, 12, 0, DefaultSettings.text_color_normal);
        statusImage_label = DesignClasses.create_image_label(5, 0, 22, 20, status_image);
        rowPanel.add(statusImage_label);
        final JPanel container = new JPanel(new GridLayout(3, 1, 0, 0));
        container.setBounds(75, 0, bar_width, 36);
        // container.set
        container.setLayout(new BoxLayout(container, BoxLayout.Y_AXIS));
        container.setBackground(null);

        container.setFocusable(true);
        container.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        container.addMouseListener(new MouseListener() {
            @Override
            public void mouseEntered(MouseEvent e) {
                //     user_name_button.setFont(new Font(Font.SERIF, Font.BOLD, 13));
            }

            @Override
            public void mouseClicked(MouseEvent e) {

                single_user = new SingleUserProfile(single_p);
                single_user.setVisible(true);
            }

            @Override
            public void mousePressed(MouseEvent e) {
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            }

            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        int max_length = 46;
        String what = what_in_our_mind;
        if (what_in_our_mind.length() > max_length) {
            what = HelperMethods.truncate_string(what_in_our_mind, max_length) + " ....";
//            System.out.println("Concate=> " + what);
        }
        whatLable = new JLabel(what);
        whatLable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);


        container.add(user_name_button);
        container.add(whatLable);
        rowPanel.setBackground(Color.white);
        rowPanel.add(container);
        JLabel user_image = helperMethods.create_image_from_url(single_p.getGender(), single_p.getProfileImage(), 30, 2, 35, 35);
        rowPanel.add(user_image);
        return rowPanel;
    }

//--------------------------------- preventing call from call log to a non-friend user -------------------------------    
    public static boolean isFriend(UserAgentProfile paramProfile) {
        for (int i = 0; i < myUpdatedFriendList.size(); i++) {
            if (paramProfile.equals(myUpdatedFriendList.get(i))) {
                return true;
            }
        }
        return false;
    }

    public static UserAgentProfile matchedProfile(UserAgentProfile paramProfile) {
        for (int i = 0; i < myUpdatedFriendList.size(); i++) {
            if (paramProfile.getUserIdentity().equals(myUpdatedFriendList.get(i).getUserIdentity())) {
                return myUpdatedFriendList.get(i);
            }
        }
        return null;
    }
//--------------------------------- preventing call from call log to a non-friend user -------------------------------    
}
