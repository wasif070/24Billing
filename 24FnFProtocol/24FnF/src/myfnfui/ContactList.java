/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

/**
 *
 * @author FaizAhmed
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.util.ArrayList;
import local.ua.UserAgentProfile;

public class ContactList {

    UserProfile userProfile;
    Gson jsn;
    //String jsonString = "{\"contactList\":[{\"userIdentity\":\"bbb\",\"firstName\":\"bbb\",\"lastName\":\"bbb\",\"gender\":\"Male\",\"country\":2,\"mobilePhone\":\"bbb\",\"email\":\"test@gmail.com\",\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside \"},{\"userIdentity\":\"bbb\",\"firstName\":\"bbb\",\"lastName\":\"bbb\",\"gender\":\"Male\",\"country\":2,\"mobilePhone\":\"bbb\",\"email\":\"test@gmail.com\",\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside\"}],\"email\":\"test@gmail.com\",\"gender\":\"Male\",\"mobilePhone\":\"gggg\",\"firstName\":\"gggg\",\"lastName\":\"gggg\",\"country\":2,\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside\",\"success\":\"true\",\"switchIp\":\"192.168.1.126\",\"switchPort\":\"5060\"}";
    String jsonString = "";
//
//    public static void main(String args[]) {
//        String jsonString_found = "{\"contactList\":[{\"userIdentity\":\"bbb\",\"firstName\":\"bbb\",\"lastName\":\"bbb\",\"gender\":\"Male\",\"country\":2,\"mobilePhone\":\"bbb\",\"email\":\"test@gmail.com\",\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside \"},{\"userIdentity\":\"bbb\",\"firstName\":\"bbb\",\"lastName\":\"bbb\",\"gender\":\"Male\",\"country\":2,\"mobilePhone\":\"bbb\",\"email\":\"test@gmail.com\",\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside\"}],\"email\":\"test@gmail.com\",\"gender\":\"Male\",\"mobilePhone\":\"gggg\",\"firstName\":\"gggg\",\"lastName\":\"gggg\",\"country\":2,\"presence\":\"1\",\"whatisInYourMind\":\" i am busy.... its raining outside\",\"success\":\"true\",\"switchIp\":\"192.168.1.126\",\"switchPort\":\"5060\"}";
//        ContactList dd = new ContactList(jsonString_found);
//        dd.setJsonValsues();
//
//        //System.out.println(dd.get_value(0));
//    }

    public ContactList(String contactJson) {
        jsonString = contactJson;
        userProfile = new UserProfile();
        jsn = new GsonBuilder().serializeNulls().create();
        userProfile = jsn.fromJson(jsonString, UserProfile.class);
    }

    public void setJsonValsues() {

        ArrayList<UserAgentProfile> friendlist = new ArrayList<UserAgentProfile>();
        //System.out.println(jsonString);
        //   userProfile = jsn.fromJson(jsonString, UserProfile.class);
        friendlist = userProfile.getContactList();
        //System.out.println(friendlist.get(0).getGender());
    }

    public ArrayList<UserAgentProfile> return_friendList() {
        ArrayList<UserAgentProfile> friendlist = new ArrayList<UserAgentProfile>();
        friendlist = userProfile.getContactList();
        return friendlist;
    }

    public String get_value(int i) {
        return return_friendList().get(i).getFirstName();
    }

    public UserProfile return_user_details() {
        return userProfile;
    }
    //private void parseUserProfileJson(String serverResonse) throws Exception{
//		// TODO Auto-generated method stub
//		JSONObject jsonObject = new JSONObject(serverResonse);
//		UserProfile userProfile = new UserProfile();
//		if(jsonObject.getBoolean("success")){			
//			JSONArray friendsJsonArray = jsonObject.getJSONArray("contactList");
//			ArrayList<Profile> friendList= new ArrayList<Profile>();
//			for (int j = 0; j < friendsJsonArray.length(); j++) {
//				Profile friendsProfile = new Profile();				
//				friendsProfile.setUserIdentity(friendsJsonArray.getJSONObject(j).getString("userIdentity"));
//				friendsProfile.setEmail(friendsJsonArray.getJSONObject(j).getString("email"));
//				friendsProfile.setGender(friendsJsonArray.getJSONObject(j).getString("gender"));
//				friendsProfile.setMobilePhone(friendsJsonArray.getJSONObject(j).getString("mobilePhone"));
//				friendsProfile.setFastName(friendsJsonArray.getJSONObject(j).getString("firstName"));
//				friendsProfile.setLastName(friendsJsonArray.getJSONObject(j).getString("lastName"));
//				friendsProfile.setCountry(friendsJsonArray.getJSONObject(j).getString("country"));
//				friendsProfile.setPresence(friendsJsonArray.getJSONObject(j).getInt("presence"));
//				friendsProfile.setWhatisInYourMind(friendsJsonArray.getJSONObject(j).getString("whatisInYourMind"));
//				friendList.add(friendsProfile);
//			}
//
//			userProfile.setUserIdentity(userName.getText().toString());
//			userProfile.setEmail(jsonObject.getString("email"));
//			userProfile.setGender(jsonObject.getString("gender"));
//			userProfile.setMobilePhone(jsonObject.getString("mobilePhone"));
//			userProfile.setFastName(jsonObject.getString("firstName"));
//			userProfile.setLastName(jsonObject.getString("lastName"));
//			userProfile.setCountry(jsonObject.getString("country"));
//			userProfile.setPresence(jsonObject.getInt("presence"));
//			userProfile.setWhatisInYourMind(jsonObject.getString("whatisInYourMind"));			
//			String switchIp = jsonObject.getString("switchIp");
//			int switchPort = jsonObject.getInt("switchPort");
//		}
}