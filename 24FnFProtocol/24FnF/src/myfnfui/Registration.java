/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import local.ua.UserAgentProfile;
import local.ua.UserAgent;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.tools.ScheduledWork;

/**
 *
 * @author Wasif Islam
 */
public class Registration {

    protected UserAgentProfile ua_profile;
    protected UserAgent ua;

    public Registration(UserAgentProfile ua_profile, UserAgent ua) {

        this.ua_profile = ua_profile;
        this.ua = ua;

        if (ua_profile.re_invite_time > 0) {
            reInvite(ua_profile.re_invite_time);
        }

        // Set the transfer (REFER)
        if (ua_profile.transfer_to != null && ua_profile.transfer_time > 0) {
            callTransfer(ua_profile.transfer_to, ua_profile.transfer_time);
        }

        if (ua_profile.do_unregister_all) // ########## unregisters ALL contact URLs
        {
            ua.printLog("UNREGISTER ALL contact URLs");
            ua.unregisterall();
        }

//        if (ua_profile.do_unregister) // unregisters the contact URL
//        {
//            ua.printLog("UNREGISTER the contact URL");
//            ua.unregister();
//        }
        
        if (ua_profile.do_register) // ########## registers the contact URL with the registrar server
        {
            ua.printLog("REGISTRATION");
            ua.loopRegister(ua_profile.expires, ua_profile.expires / 2, ua_profile.keepalive_time);
        }

        if (ua_profile.call_to != null) // ########## make a call with the remote URL
        {
            ua.printLog("UAC: CALLING " + ua_profile.call_to);
            ua.call(ua_profile.call_to);
        }

        if (!ua_profile.audio && !ua_profile.video) {
            ua.printLog("ONLY SIGNALING, NO MEDIA");
        }
    }

    void reInvite(final int delay_time) {
        //printLog("AUTOMATIC RE-INVITING/MODIFING: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.modify(null);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.modify(null);
                }
            };
        }
    }

    void callTransfer(final NameAddress transfer_to, final int delay_time) {
        //printLog("AUTOMATIC REFER/TRANSFER: " + delay_time + " secs");
        if (delay_time == 0) {
            ua.transfer(transfer_to);
        } else {
            new ScheduledWork(delay_time * 1000) {
                public void doWork() {
                    ua.transfer(transfer_to);
                }
            };
        }
    }
}
