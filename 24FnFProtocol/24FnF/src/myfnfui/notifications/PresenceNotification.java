/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.notifications;

import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.StaticImages;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author user
 */
public class PresenceNotification extends JDialog {

    JLabel label;
    JLabel status_label;
    public JButton button_window_close;
    public JPanel main_container;
    int frame_width = 250;
    int frame_height = 100;
    int task_bar_height = 50;
    int top_bar_height = 25;

    //  JDialog notificaion_window = create_joption_panel_no_default_close(frame_width, frame_height, "24FnF");
    public PresenceNotification() {
        this.setSize(frame_width, frame_height);
        this.setLocationRelativeTo(null);
        // notifyWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setResizable(false);
        this.setUndecorated(true);
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
        Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
        int x = (int) rect.getMaxX() - this.getWidth();
        int y = (int) rect.getMaxY() - this.getHeight();
        y = y - task_bar_height;
        this.setLocation(x, y);
        this.setVisible(true);
        this.setFocusable(false);
        this.setFocusableWindowState(true);


        main_container = (JPanel) getContentPane();
        main_container.setBorder(BorderFactory.createMatteBorder(0, 0, 8, 0, DefaultSettings.blue_border_color));
        main_container.setBackground(Color.WHITE);
        main_container.setLayout(null);
        main_container.setFocusable(false);
        //   label = DesignClasses.makeJLabel_with_text_color("", 30, 40, 200, 12, 0, Color.BLACK);
        label = DesignClasses.makeJLabel_with_background_font_size_width_color("", 40, 35, 200, 25, 12, 1, Color.BLACK, Color.WHITE, 2);
        label.setFocusable(false);
        status_label = DesignClasses.makeJLabel_with_background_font_size_width_color("", 40, 55, 200, 25, 11, 2, Color.BLACK, Color.WHITE, 2);
        status_label.setFocusable(false);
        main_container.add(label);
        main_container.add(status_label);
        button_window_close = DesignClasses.create_button_with_image_fixed_location(StaticImages.ICON_CLOSE, StaticImages.ICON_CLOSE_HOVER, "Close Window", frame_width - 25, 5, 20, 20);
        button_window_close.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // status_label.setText("");
                dispose_this();

            }
        });
        button_window_close.setFocusable(false);
        main_container.add(button_window_close);
        if (DesignClasses.file_exists(MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.IMAGE_joption_header_scaled)) {
            JLabel label_image = HelperMethods.get_scale_image(0, 2, frame_width - 2, 25, MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.IMAGE_joption_header_scaled);//new JLabel("", DesignClasses.return_image(StaticImages.IMAGE_joption_header), JLabel.CENTER);
            label_image.setBackground(null);
            label_image.setFocusable(false);
            JPanel topBar = new JPanel(new BorderLayout());
            topBar.add(label_image, BorderLayout.CENTER);
            topBar.setBounds(2, 2, frame_width - 4, top_bar_height);
            main_container.add(topBar);
        }


    }

    public void dispose_this() {
        this.dispose();
    }

    public void createNotificationWindow(String Name, String status) {

        if (DesignClasses.file_exists(MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.IMAGE_joption_header_scaled)) {
            label.setText(Name);
            status_label.setText(status);
            new Thread(new NotifiyThread(label, status_label, this)).start();
        } else {
            this.dispose();
        }

    }
//    public static void main(String[] args) {
//        SwingUtilities.invokeLater(new Runnable() {
//            public void run() {
//                new PresenceNotification().createNotificationWindow("Deleted Succefully", "dddsjfld");
//
//            }
//        });
//    }
}
