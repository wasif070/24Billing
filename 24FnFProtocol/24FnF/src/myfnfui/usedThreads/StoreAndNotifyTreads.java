/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import myfnfui.containers.MyfnfHashMaps;
import myfnfui.tampClass.TempStorageOfContactList;

/**
 *
 * @author user
 */
public class StoreAndNotifyTreads {

    private ProfileImageDownloaderTread imageDownloader = null;
    private ReceivedPkdParser receiverPakParser = null;
    long counter = 0;
//    public StoreAndNotifyTreads() {
//        if (receiverPakParser == null) {
//            receiverPakParser = new ReceivedPkdParser();
//            receiverPakParser.start();
//        }
//    }
    public void store_and_notify_image_downloader_thread(String user_name, String url) {

        if (imageDownloader == null) {
            imageDownloader = new ProfileImageDownloaderTread();
            imageDownloader.start();
        }
        if (imageDownloader.getState() == Thread.State.WAITING) {
            synchronized (imageDownloader) {
                imageDownloader.notify();
            }
        }
        MyfnfHashMaps.getInstance().getProfileImageUrls().put(user_name, url);

    }

//    public void store_and_notify_packet_parser_thread(UdpPacket packet) {
//        if (receiverPakParser == null) {
//            receiverPakParser = new ReceivedPkdParser();
//            receiverPakParser.start();
//        }
//        if (receiverPakParser.getState() == Thread.State.WAITING) {
//            synchronized (receiverPakParser) {
//                receiverPakParser.notify();
//            }
//        }
//        MyFnFQueue.getInstance().getReceivedPackedQ().add(packet);
//
//    }

    public void store_and_notify_packet_parser_thread(String pak) {

//        System.out.println("MyfnfHashMaps.size() [b4] ==> " + MyfnfHashMaps.getInstance().getReceiced_msg_container().size());
        MyfnfHashMaps.getInstance().getReceiced_msg_container().put(counter++, pak);
//        System.out.println("MyfnfHashMaps.size() [after] ==> " + MyfnfHashMaps.getInstance().getReceiced_msg_container().size());
        if (receiverPakParser == null) {
            receiverPakParser = new ReceivedPkdParser();
            receiverPakParser.start();
        }


        if (receiverPakParser.getState() == Thread.State.WAITING) {
            synchronized (receiverPakParser) {
                receiverPakParser.notify();
            }
        }

        // MyFnFQueue.getInstance().getReceivedPackedQ().add(packet);

    }
    private PacketProcessor packetProcessor = null;

    public void store_received_msg_and_notify_packetProcessor(String pakId, String received_json) {

        if (packetProcessor == null) {
            packetProcessor = new PacketProcessor();
            packetProcessor.start();
        }
        if (packetProcessor.getState() == Thread.State.WAITING) {
            synchronized (packetProcessor) {
                packetProcessor.notify();
            }
        }
        TempStorageOfContactList.getInstance().getResponse_msg_container().put(pakId, received_json);

    }
}
