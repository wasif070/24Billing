/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import com.google.gson.Gson;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import myfnfui.JsonFields;
import myfnfui.RefreshFriendList;
import myfnfui.ResetUserProfileFriendList;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author FaizAhmed
 */
public class SetDynamicFriend extends Thread {

    Gson jsonLib;
    public static boolean isThredActive = false;
    public static boolean runningFlag = true;
    public static boolean repaint_friendlist = true;
    private static SetDynamicFriend presenceChecker;
    int counter = 0;

    public SetDynamicFriend() {
    }

    public static SetDynamicFriend getInstance() {
        if (presenceChecker == null) {
            presenceChecker = new SetDynamicFriend();
        }
        return presenceChecker;
    }
    int i = 0;
    int paint = 0;

    @Override
    public void run() {
        isThredActive = true;
        while (runningFlag) {
            try {
                RefreshFriendList.change_my_status_image();
                Thread.sleep(MyFnFSettings.AUTOREFRESH_TIME);
            } catch (Exception ex) {
                //  Logger.getLogger(SetDynamicFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            if (counter > 3) {
                send_keepAlive();
                counter = 0;
            } else {
                counter++;
            }
        }
    }

    public static void repaint_friend_list() {
        //  ResetUserProfileFriendList.refesh_user_profile();
        RefreshFriendList dd = new RefreshFriendList();
        ResetUserProfileFriendList.refesh_friend_clss(dd);
    }

    public static void send_keepAlive() {
        JsonFields field = new JsonFields();
        field.setAction(SocketConstants.UPDATE);
        field.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        field.setType(SocketConstants.KEEP_ALIVE);
        PackageActions.sendPacketAsString(field);
    }
}
