/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import helpers.APIParametersFiles;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import imageDownload.DownLoaderHelps;
import java.io.File;
import local.ua.UserAgentProfile;
import myfnfui.CallLogDetails;
import myfnfui.CreateNewAccount;
import myfnfui.GUI24FnF;
import myfnfui.JsonFields;
import myfnfui.Main;
import myfnfui.Messages.NotificationMessages;
import myfnfui.ProfileAddUpdate;
import myfnfui.RefreshFriendList;
import myfnfui.SendReceiveListener;
import myfnfui.UpdateCallLog;
import myfnfui.groups.CreateGroup;
import myfnfui.groups.GroupConstants;
import myfnfui.groups.GroupDto;
import myfnfui.groups.GroupMemberDto;
import myfnfui.groups.GroupsWindow;
import myfnfui.groups.MapGroupResponse;
import myfnfui.groups.NewGruopFields;
import myfnfui.groups.RemoveGruop;
import myfnfui.groups.TempGruoupContainer;
import myfnfui.groups.grouphelper.PreviousGruopHelpers;
import myfnfui.notifications.PresenceNotification;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.FriendlistMapping;
import myfnfui.packetsInfo.InviteFriendContainer;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.packetsInfo.PackedDistributor;
import myfnfui.packetsInfo.SendPackages;
import myfnfui.tampClass.SearchFreindlListMapping;
import myfnfui.tampClass.StaticFieldsForProfileUpdate;

/**
 *
 * @author reefat
 */
public class ReceivedPkdParserWorkerThread implements Runnable {

    public static boolean stop = false;
    PackageActions pak_action = new PackageActions();
    SendReceiveListener listner = null;
    DownLoaderHelps dHelp = new DownLoaderHelps();
    PackedDistributor distributor = null;
    RefreshFriendList get_frind_list = new RefreshFriendList();
    StoreAndNotifyTreads storeAndNotify = new StoreAndNotifyTreads();
    PreviousGruopHelpers groupsWindow = new PreviousGruopHelpers();
    String str = "";

    public ReceivedPkdParserWorkerThread(String param_str) {
        this.str = param_str;
    }

    @Override
    public void run() {



        try {
            JsonFields js = HelperMethods.response_json(str);
            String pakid = js.getPacketId();
            String action = js.getAction();
            String type = js.getType();
            Boolean success = js.getSuccess();

            String send_pak = "";
            if (js.getPacketId() != null && js.getPacketId().length() > 0) {
                if (!SendPackages.getInstance().getStore_packet().isEmpty()) {
                    send_pak = SendPackages.getInstance().getStore_packet().get(pakid).getPacketToSend();
                    SendPackages.getInstance().getStore_packet().remove(pakid);
                }
            }
            if (action != null) {
                if (action.equals(SocketConstants.RESPONSE)) {
                    if (type != null) {
                        if (type.equals(SocketConstants.SIGN_IN)) {
                            if (MyFnFSettings.userProfile == null) {
                                if (Main.objMain != null) {
                                    distributor = Main.objMain;
                                    distributor.onReceivedData(js);
                                }
                            }
                        } else if (type.equals(SocketConstants.CONTACT_LIST)) {
                            FriendlistMapping tamp_list = HelperMethods.friendlist_mapping(str);
                            if (!tamp_list.getContactList().isEmpty() && tamp_list.getSessionId() != null && tamp_list.getSessionId().equals(MyFnFSettings.LOGIN_SESSIONID)) {

                                for (final UserAgentProfile singleProfile : tamp_list.getContactList()) {
                                    FriendList.getInstance().getFriend_hash_map().put(singleProfile.getUserIdentity(), singleProfile);
                                    if (!singleProfile.getProfileImage().isEmpty()) {
                                        String image_name = HelperMethods.getImageName_from_url(singleProfile.getProfileImage());
                                        if (HelperMethods.check_string_contains_substring(image_name, ".")) {
                                            File f = new File(dHelp.getDestinationFolder() + image_name);
                                            if (!f.exists()) {
                                                if (singleProfile.getProfileImage() != null && singleProfile.getProfileImage().length() > 0) {
                                                    storeAndNotify.store_and_notify_image_downloader_thread(singleProfile.getUserIdentity(), singleProfile.getProfileImage());
                                                }

                                            }
                                        }
                                    }

                                }
                                if (GUI24FnF.getObjMain() != null) {
                                    GUI24FnF.set_visivility(true);
                                    get_frind_list.getfriend_list();
                                }

                            }
                            //         System.out.println("lsdjfkj");
                            //   storeAndNotify.store_received_msg_and_notify_packetProcessor(js.getPacketIdFromServer(), str);
                            //  pak_action.store_received_msg_and_notify(js.getPacketIdFromServer(), str);
                        } //                                        else if (type.equals(HeaderConstants.TYPE_fnf_headers)) {
                        //                                        }
                        else if (type.equals(GroupConstants.TYPE_groupList)) {
                            MapGroupResponse fetchGroup = HelperMethods.mappGroupResponse(str);

                            if (fetchGroup != null) {
                                for (GroupDto dto : fetchGroup.getGroupList()) {
                                    TempGruoupContainer.getInstance().getGruopFriendContainer().put(dto.getGroupId(), dto);

                                }
                            }

                        } else if (type.equals(SocketConstants.SIGN_UP)) {
                            if (SendPackages.getInstance().getSend_packet() != null) {
                                SendPackages.getInstance().getSend_packet().remove(pakid);
                            }
                            if (CreateNewAccount.getNewUserClass() != null) {
                                distributor = CreateNewAccount.getNewUserClass();
                                distributor.onReceivedData(js);
                            }

                        } else if (type.equals(SocketConstants.USER_ID_AVAILABLE)) {
                            if (CreateNewAccount.getNewUserClass() != null) {
                                distributor = CreateNewAccount.getNewUserClass();
                                distributor.onReceivedData(js);
                            }

                        } else if (type.equals(SocketConstants.CONTACT_SEARCH)) {
                            SearchFreindlListMapping tamp_list = HelperMethods.tamp_friendlist_mapping(str);
                            for (final UserAgentProfile singleProfile : tamp_list.getSearchedcontaclist()) {
                                InviteFriendContainer.getInstance().getInviteFriendsContainer().put(singleProfile.getUserIdentity(), singleProfile);
                            }
                            if (GUI24FnF.getObjMain() != null) {
                                distributor = GUI24FnF.getObjMain();
                                distributor.onReceivedData(js);
                            }


                        } else if (type.equals(SocketConstants.ADD_FRIEND) || type.equals(SocketConstants.DELETE_FRIEND)) {
                            if (GUI24FnF.getObjMain() != null) {
                                distributor = GUI24FnF.getObjMain();
                                distributor.onReceivedData(js);
                            }

                        } else if (type.equals(SocketConstants.ACCEPT_FRIEND)) {
                            FriendList.getInstance().add_single_friend_entry(js);
                            if (GUI24FnF.getObjMain() != null && SetDynamicFriend.repaint_friendlist) {
                                //   GUI24FnF.set_visivility(true);
                                get_frind_list.getfriend_list();

                            }
                            if (js.getProfileImage() != null && js.getProfileImage().length() > 0) {
                                storeAndNotify.store_and_notify_image_downloader_thread(js.getUserIdentity(), js.getProfileImage());
                                //   storeAndNotify.store_received_msg_and_notify(SocketConstants.PROFILE_IMAGE_TEXT, js.getProfileImage());
                            }
                        } else if (type.equals(SocketConstants.WHAT_IS_IN_YOUR_MIND)) {
                            if (ProfileAddUpdate.getProfileUpdateObject() != null) {
                                distributor = ProfileAddUpdate.getProfileUpdateObject();
                                distributor.onReceivedData(js);
                            }

                        } else if (type.equals(SocketConstants.USER_PROFILE)) {
                            if (js.getSuccess()) {
                                int[] indexes = new int[1];
                                indexes = js.getIndexOfHeaders();
                                //  System.out.println("chaned index: " + indexes[0]);
                                int in = indexes[0];
                                if (in == SocketConstants.CONS_FIRST_NAME) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.firstName);
                                } else if (in == SocketConstants.CONS_LAST_NAME) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.lastName);
                                } else if (in == SocketConstants.CONS_GENDER) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.gender);
                                } else if (in == SocketConstants.CONS_COUNTRY) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.country);
                                } else if (in == SocketConstants.CONS_CALLING_CODE) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.mobilePhoneDialingCode);
                                } else if (in == SocketConstants.CONS_MOBILE_PHONE) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.mobilePhone);
                                } else if (in == SocketConstants.CONS_EMAIL) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.email);
                                } else if (in == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
                                    GUI24FnF.set_userProfile(in, StaticFieldsForProfileUpdate.whatisInYourMind);
                                }
                                if (ProfileAddUpdate.getProfileUpdateObject() != null) {
                                    distributor = ProfileAddUpdate.getProfileUpdateObject();
                                    distributor.onReceivedData(js);
                                }

                            }
                        } else if (type.equals(SocketConstants.CHANGE_PASSWORD)) {
                            if (js.getSuccess()) {
                                MyFnFSettings.LOGIN_USER_PASSWORD = StaticFieldsForProfileUpdate.password;
                                MyFnFSettings.passwordChange = true;
                            } else {
                                if (ProfileAddUpdate.getProfileUpdateObject() != null) {
                                    HelperMethods.create_confrim_panel_no_cancel(js.getMessage(), 50);
                                }
                            }


                        } else if (type.equals(SocketConstants.CALL_LOG)) {
                            if (js.getPacketIdFromServer() != null) {
                                //  System.out.println("call log--");
                                CallLogDetails dd = HelperMethods.map_call_log(str);
                                MyFnFSettings.call_log_obj = dd;
                                //UpdateCallLog updateCallLog= new UpdateCallLog();
                                if (UpdateCallLog.getCallLogObj() != null) {
                                    UpdateCallLog.getCallLogObj().call_log_repaint();
                                }
                            }


                        } else if (type.equals(GroupConstants.TYPE_create_group)) {

                            if (success.equals(true)) {
                                GroupDto mapper = HelperMethods.map_into_GruopDto(send_pak);
                                mapper.setSuperAdmin(MyFnFSettings.LOGIN_USER_ID);
                                TempGruoupContainer.getInstance().getGruopFriendContainer().put(js.getGroupId(), mapper);
                                HelperMethods.create_confrim_panel_no_cancel(NotificationMessages.GROUP_CREATE_SUCCESS, 0);
                            } else {
                                HelperMethods.create_confrim_panel_no_cancel("Can not create group. " + js.getMessage(), 0);
                            }


                            if (GroupsWindow.getGroupWindow() != null) {
                                GroupsWindow.getGroupWindow().show_group_windows();
                            }
                        } else if (type.equals(GroupConstants.TYPE_edit_group_member)) {
                            PreviousGruopHelpers.processing_gruop_id = null;
                            if (type.equals(GroupConstants.TYPE_edit_group_member) && success.equals(true)) {
                                CreateGroup mapper = HelperMethods.createGruopMapper(send_pak);
                                Long gurop_id = mapper.getGroupId();
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                    for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                        int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());

                                    }
                                    HelperMethods.create_confrim_panel_no_cancel("Group edited successfully", 40);
                                }
                            }
                            if (GroupsWindow.getGroupWindow() != null) {
                                GroupsWindow.getGroupWindow().show_group_windows();
                            }
                        } else if (type.equals(GroupConstants.TYPE_remove_group_member)) {
                            //  String pak = SendPackages.getInstance().getStore_packet().get(pakid).getPacketToSend();
                            //   SendPackages.getInstance().getStore_packet().remove(pakid);
                            if (success.equals(true)) {

                                CreateGroup mapper = HelperMethods.createGruopMapper(send_pak);
                                Long gurop_id = mapper.getGroupId();
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                    for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                        int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
                                        if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                            TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().remove(index);
                                        }

                                    }
                                    //     new NotificationWindow().createNotificationWindow("Group member Deleted from " + TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName());
                                    if (GroupsWindow.getGroupWindow() != null) {
                                        if (GroupsWindow.currentSelectedGruop != null) {
                                            groupsWindow.draw_grouplist();
                                            groupsWindow.draw_group_details(gurop_id);
                                        }
                                    }
                                }
                                //   storeAndNotify.store_received_msg_and_notify_packetProcessor(pakid, send_pak);
                                // pak_action.store_received_msg_and_notify(pakid, pak);
                            } else {
                                HelperMethods.create_confrim_panel_no_cancel(js.getMessage(), 20);
                            }

                        } else if (type.equals(GroupConstants.TYPE_delete_group)) {
                            if (success.equals(true)) {

                                JsonFields fields = HelperMethods.response_json(send_pak);
                                String gruop_name_in_action = "";

                                if (fields.getGroupId() > 0) {

                                    GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId());
                                    if (dto != null) {
                                        gruop_name_in_action = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId()).getGroupName();
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().remove(fields.getGroupId());
                                    }

                                    if (GroupsWindow.getGroupWindow() != null) {
//                                                        HelperMethods.create_confrim_panel_no_cancel_no_left_margin("You are no longer member of " + gruop_name_in_action + " ");
                                        HelperMethods.create_confrim_panel_no_cancel_no_left_margin(gruop_name_in_action + " deleted successfully");

                                        if (GroupsWindow.currentSelectedGruop != null) {
                                            groupsWindow.draw_grouplist();
                                            groupsWindow.delete_single_group_details();
                                        }
                                    } else {
                                        // new PresenceNotification().createNotificationWindow("Group " + gruop_name_in_action, "Deleted");
                                    }
                                }
                                //  storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_delete_group, send_pak);
                            } else {
                                HelperMethods.create_confrim_panel_no_cancel("Failed! " + js.getMessage(), 20);
                            }

                        } else if (type.equals(GroupConstants.TYPE_add_group_member)) {
                            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
                            if (success.equals(true)) {
                                CreateGroup mapper = HelperMethods.createGruopMapper(send_pak);
                                Long gurop_id = mapper.getGroupId();
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    //         System.out.println(changed_friend.getUserIdentity());
                                    //  int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
                                    //     System.out.println(gurop_id + "  " + changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                    GroupMemberDto dto = new GroupMemberDto();
                                    dto.setAdmin(changed_friend.getAdmin());
                                    dto.setUserIdentity(changed_friend.getUserIdentity());

//                                if (changed_friend.getFirstName() != null) {
//                                    dto.setFirstName(changed_friend.getFirstName());
//                                }
//                                if (changed_friend.getLastName() != null) {
//                                    dto.setLastName(changed_friend.getLastName());
//                                    System.out.println(changed_friend.getLastName());
//                                }
//                                if (changed_friend.getPresence() != null) {
//                                    dto.setPresence(changed_friend.getPresence());
//                                }
//                                if (changed_friend.getGender() != null) {
//                                    dto.setGender(changed_friend.getGender());
//                                }
                                    try {
                                        if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                            TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().add(dto);
                                        }

                                    } catch (Exception ep) {
                                    }


                                }
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                        groupsWindow.hide_save_canecl_panel_in_buttom();
                                    }
                                }


                                //    storeAndNotify.store_received_msg_and_notify_packetProcessor(pakid, send_pak);
                                HelperMethods.create_confrim_panel_no_cancel("New members added successfully", 0);
                                //  pak_action.store_received_msg_and_notify(pakid, pak);
                            } else {
                                HelperMethods.create_confrim_panel_no_cancel("Failed! " + js.getMessage(), 20);
                            }

                        } else if (type.equals(GroupConstants.TYPE_leave_group)) {
                            TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
                            if (success.equals(true)) {

                                JsonFields fields = HelperMethods.response_json(send_pak);
                                String gruop_name_in_action = "";
                                //   System.out.println("key : " + key);
                                if (fields != null) {

                                    GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId());
                                    if (dto != null) {
                                        gruop_name_in_action = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId()).getGroupName();
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().remove(fields.getGroupId());
                                    }

                                    if (GroupsWindow.getGroupWindow() != null) {
                                        HelperMethods.create_confrim_panel_no_cancel_no_left_margin("You are no longer member of " + gruop_name_in_action + " ");
                                        if (GroupsWindow.currentSelectedGruop != null) {
                                            groupsWindow.draw_grouplist();
                                            groupsWindow.delete_single_group_details();
                                        }
                                    } else {
                                        //  new PresenceNotification().createNotificationWindow(gruop_name_in_action, "Deleted");
                                    }
                                }
                                //   storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_leave_group, send_pak);
                            } else {
                                HelperMethods.create_confrim_panel_no_cancel("Failed! " + js.getMessage(), 20);
                            }

                        } else if (action.equals(SocketConstants.CONFIRMATION)) {
                            if (type.equals(SocketConstants.CONTACT_LIST) && success.equals(true)) {
                                if (GUI24FnF.getObjMain() != null) {
                                    GUI24FnF.set_visivility(true);
                                    get_frind_list.getfriend_list();

                                }
                                //     SendPackages.getInstance().getSend_packet().remove(pakid);
                                //  storeAndNotify.store_received_msg_and_notify_packetProcessor(SocketConstants.CONTACT_LIST, str);

                            }
                            if (type.equals(GroupConstants.TYPE_groupList) && success.equals(true)) {
                                TempGruoupContainer.getInstance().getGroupMemberContainer().clear();
                            }
                        }
                    }
                } else if (action.equals(SocketConstants.UPDATE)) {
                    if (type != null) {
                        if (type.equals(GroupConstants.TYPE_delete_group)) {

                            JsonFields fields = HelperMethods.response_json(str);

                            String gruop_name_in_action = "";

                            if (fields.getGroupId() > 0) {

                                GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId());
                                if (dto != null) {
                                    gruop_name_in_action = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId()).getGroupName();
                                    TempGruoupContainer.getInstance().getGruopFriendContainer().remove(fields.getGroupId());
                                }

                                if (GroupsWindow.getGroupWindow() != null) {
                                    HelperMethods.create_confrim_panel_no_cancel(gruop_name_in_action + " Deleted succefully", 20);
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.delete_single_group_details();
                                    }
                                } else {
                                    // new PresenceNotification().createNotificationWindow("Group " + gruop_name_in_action, "Deleted");
                                }
                            }
                            // storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_delete_group, str);
                        } else if (type.equals(GroupConstants.TYPE_new_group)) {
                            GroupDto mapper = HelperMethods.map_into_GruopDto(str);
                            TempGruoupContainer.getInstance().getGruopFriendContainer().put(mapper.getGroupId(), mapper);
                            //   new PresenceNotification().createNotificationWindow("Group " + gruop, "Created now ");
                            //   storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_new_group, str);
                        } else if (type.equals(GroupConstants.TYPE_add_group_member)) {
                            CreateGroup mapper = HelperMethods.createGruopMapper(str);
                            Long gurop_id = mapper.getGroupId();
                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                GroupMemberDto dto = new GroupMemberDto();
                                dto.setAdmin(changed_friend.getAdmin());
                                dto.setUserIdentity(changed_friend.getUserIdentity());
                                dto.setFirstName(changed_friend.getUserIdentity());
                                dto.setLastName(changed_friend.getLastName());
                                dto.setPresence(changed_friend.getPresence());
                                dto.setGender(changed_friend.getGender());
                                try {
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().add(dto);
                                    }

                                } catch (Exception ep) {
                                }


                            }
                            if (GroupsWindow.getGroupWindow() != null) {
                                if (GroupsWindow.currentSelectedGruop != null) {
                                    groupsWindow.draw_grouplist();
                                    groupsWindow.draw_group_details(gurop_id);
                                    groupsWindow.hide_save_canecl_panel_in_buttom();
                                }
                            }

                            //       storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_new_group, str);
                        } else if (type.equals(GroupConstants.TYPE_remove_group_member)) {
                            //  String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //System.out.println("remove group member by other" + gruop_list);
                            RemoveGruop mapper = HelperMethods.removeGruopMembersMapper(str);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());

                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().remove(index);
                                    }
                                }

                                String group_name = " ";
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName().length() > 0) {
                                    group_name = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName();
                                }
                                // new PresenceNotification().createNotificationWindow("Group " + group_name, "Member Deleted");
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                    }
                                }
                            }

                            //     storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_remove_a_group_member_by_other, str);
                        } else if (type.equals(GroupConstants.TYPE_edit_group_member)) {

//                                            System.out.println("sd fjlsdkfffkdfkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk");
                            CreateGroup mapper = HelperMethods.createGruopMapper(str);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
//                                                    System.out.println(changed_friend.getUserIdentity());
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
//                                                    System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());
                                    }

                                }
                                String group_name = " ";
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName().length() > 0) {
                                    group_name = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName();
                                }
                                new PresenceNotification().createNotificationWindow("Group " + group_name, "members updated");
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                        groupsWindow.hide_save_canecl_panel_in_buttom();
                                    }
                                }
                            }
                            // storeAndNotify.store_received_msg_and_notify_packetProcessor(GroupConstants.TYPE_edit_members_by_others, str);
                        } else if (type.equals("balance")) {
                            MyFnFSettings.userProfile.setBalance(js.getBalance());
                            //    pak_action.sendConfirmationPak(js.getPacketIdFromServer());
                        } else {
                            if (GUI24FnF.getObjMain() != null) {
                                distributor = GUI24FnF.getObjMain();
                                distributor.onReceivedData(js);
                            }
                        }

                    }
                } else if (action.equals(SocketConstants.PRESENCE)) {
                    //   System.out.println("sdjlkfjsd lflkjlkdsjflkjds");
                    if (!js.getUserIdentity().equals(MyFnFSettings.LOGIN_USER_ID)) {

                        if (js.getIndexOfHeaders() != null) {
                            //System.out.println("changes in presence" + js.getIndexOfHeaders().length);
                            int[] indexes = new int[js.getIndexOfHeaders().length];
                            indexes = js.getIndexOfHeaders();
                            UserAgentProfile up = FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity());
                            if (up != null) {
                                if (!up.getUserIdentity().isEmpty() && up.getUserIdentity().length() > 0) {
                                    for (int indexNumber : indexes) {
                                        if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setFirstName(js.getFirstName());
                                        }
                                        if (indexNumber == SocketConstants.CONS_LAST_NAME) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setLastName(js.getLastName());
                                        }
                                        if (indexNumber == SocketConstants.CONS_GENDER) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setGender(js.getGender());
                                        }
                                        if (indexNumber == SocketConstants.CONS_COUNTRY) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setCountry(js.getCountry());
                                        }
                                        if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setMobilePhone(js.getMobilePhone());
                                        }
                                        if (indexNumber == SocketConstants.CONS_EMAIL) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setEmail(js.getEmail());
                                        }
                                        if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setWhatisInYourMind(js.getWhatisInYourMind());
                                        }
                                        if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setMobilePhoneDialingCode(js.getMobilePhoneDialingCode());
                                        }
                                        if (indexNumber == SocketConstants.CONS_PRESENCE) {



                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setPresence(js.getPresence());
                                            String online_offline = "";
                                            if (js.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE)) {
                                                online_offline = "Online";
                                                new PresenceNotification().createNotificationWindow(up.getFirstName() + " " + up.getLastName(), " is now " + online_offline);
                                            } else {
                                                online_offline = "Offline";
                                            }

                                        }
                                        if (indexNumber == SocketConstants.CONS_FRIENDSHIP_STATUS) {
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setFriendShipStatus(js.getFriendShipStatus());
                                        }
                                        if (indexNumber == SocketConstants.CONS_PROFILE_IMAGE) {
//                                                            System.out.println("previos " + FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).getProfileImage());
                                            String previous_image = HelperMethods.getImageName_from_url(FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).getProfileImage());
                                            File f_pre = new File(dHelp.getDestinationFolder() + previous_image);
                                            if (f_pre.exists()) {
                                                f_pre.delete();
                                            }
                                            FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setProfileImage(js.getProfileImage());
                                            if (js.getProfileImage() != null && js.getProfileImage().length() > 0) {
                                                storeAndNotify.store_and_notify_image_downloader_thread(js.getUserIdentity(), js.getProfileImage());
                                            }

                                        }
                                        //      PackageActions.sendConfirmationPak(js.getPacketIdFromServer());

                                        if (GUI24FnF.getObjMain() != null && SetDynamicFriend.repaint_friendlist) {
                                            GUI24FnF.set_visivility_flash_loading_image(false);
                                            GUI24FnF.set_visivility(true);
                                            get_frind_list.getfriend_list();
                                        }
                                        if (GroupsWindow.currentSelectedGruop != null) {
                                            if (GroupsWindow.getGroupWindow() != null) {
                                                groupsWindow.draw_group_details(GroupsWindow.currentSelectedGruop);
                                            }
                                        }
                                    }
                                } else {
                                    //    TempStorageOfContactList.getInstance().getResponse_msg_container().put(key, TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                                }
                            }
                        }
                        // storeAndNotify.store_received_msg_and_notify_packetProcessor(js.getPacketIdFromServer(), str);
                    }
                    // }
                } else if (action.equals("SIGN_OUT") && js.getSessionId().equals(MyFnFSettings.LOGIN_SESSIONID)) {
                    MyFnFSettings.LOGIN_SESSIONID = null;
                    MyFnFSettings.LOGIN_USER_ID = null;
                    MyFnFSettings.LOGIN_USER_PASSWORD = null;
                    if (GUI24FnF.getObjMain() != null) {
                        distributor = GUI24FnF.getObjMain();
                        distributor.onReceivedData(js);
                    }
                }
            }
            if (js.getPacketIdFromServer() != null && js.getPacketIdFromServer().length() > 0) {
                PackageActions.sendConfirmationPak(js.getPacketIdFromServer());
            }
        } catch (Exception e) {
//                            System.out.println("+++++++++++++++++++++ Exception in \"ReceivedPkdParser class\"................................");
        }


    }
}
