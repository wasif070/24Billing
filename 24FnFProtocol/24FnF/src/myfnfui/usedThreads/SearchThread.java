/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import helpers.GetResponseJsons;
import helpers.SearchItem;
import static myfnfui.usedThreads.SearchThread.start;
import local.ua.UA;
import utils.SearchStringQueue;
import utils.UserNameAvailabilityQueue;

/**
 *
 * @author Ashraful
 */
public class SearchThread extends Thread {

    //------------------------------------------- added_by_wasif_08062013 -----------------------------------------
    public static volatile boolean start = true;

    public static void stopSearchThread() {
        start = false;
    }

    public static void startSearchThread() {
        start = true;
    }

    @Override
    public void run() {
        while (start) {
    //------------------------------------------- added_by_wasif_08062013 -----------------------------------------
            try {
                if (!SearchStringQueue.getInstance().isEmpty()) {
                    while (SearchStringQueue.getInstance().size() != 1) {
                        SearchStringQueue.getInstance().pop();

                    }
                    SearchItem item = (SearchItem) SearchStringQueue.getInstance().pop();
                    GetResponseJsons.get_add_contact_json(item.getUser_id(), item.getSession_id(), item.getSearch_string(), item.getInvite_frnd());
                } else if (!UserNameAvailabilityQueue.getInstance().isEmpty()) {
                    while (UserNameAvailabilityQueue.getInstance().size() != 1) {
                        UserNameAvailabilityQueue.getInstance().pop();
                    }
                    SearchItem item = (SearchItem) UserNameAvailabilityQueue.getInstance().pop();
                    GetResponseJsons.check_user_avaiability(item.getSearch_string(), item.getCreate_new_account());
                } else {
                    SearchThread.sleep(1250);//wait for key presses
                }

            } catch (Exception ex) {
            }
        }

    }
}
