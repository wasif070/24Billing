/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import myfnfui.packetsInfo.PackageActions;
import myfnfui.packetsInfo.SendPackages;

/**
 *
 * @author user
 */
public class SendPacketCheckerThread extends Thread {

    //  private MessageCheckSendListner messageCheckSendListner;
    public static boolean stop = false;
    private int SLEEP_TIME = 2500;
    private int REMOVING_TIME = SLEEP_TIME * 4;
    //  private PackageActions pkg_aActions = new PackageActions();

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (!stop) {
            try {
                Thread.sleep(SLEEP_TIME);
                if (SendPackages.getInstance().getStore_packet() != null && SendPackages.getInstance().getStore_packet().size() > 0) {
                    for (String key : SendPackages.getInstance().getStore_packet().keySet()) {
                        int counter = SendPackages.getInstance().getStore_packet().get(key).getCounter();
                        if (counter > 2) {
                            SendPackages.getInstance().getStore_packet().remove(key);
                        } else {
                            long msgTime = SendPackages.getInstance().getStore_packet().get(key).getSent_time();
                            long currentTime = System.currentTimeMillis();
                            if ((currentTime - msgTime) < REMOVING_TIME) {

                                if (SendPackages.getInstance().getStore_packet() != null) {
                                    SendPackages.getInstance().getStore_packet().get(key).setCounter(counter + 1);
                                    PackageActions.sendPackedAsString_as_plan_string(SendPackages.getInstance().getStore_packet().get(key).getPacketToSend());
                                }

                            } else {
                                SendPackages.getInstance().getStore_packet().remove(key);
                            }
                        }
                        if (SendPackages.getInstance().getStore_packet().size() <= 0) {
                            synchronized (this) {
                                this.wait();
                            }
                        }
                    }
                } else {
                    synchronized (this) {
                        this.wait();
                    }
                }
            } catch (Exception ex) {
            }
        }
    }

    public void halt() {
        stop = true;
    }
}
