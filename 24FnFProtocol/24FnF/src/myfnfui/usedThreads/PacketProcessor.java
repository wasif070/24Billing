/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import helpers.APIParametersFiles;
import helpers.HelperMethods;
import helpers.SocketConstants;
import imageDownload.DownLoaderHelps;
import java.io.File;
import local.ua.UserAgentProfile;
import myfnfui.GUI24FnF;
import myfnfui.InviteFrined;
import myfnfui.JsonFields;
import myfnfui.Messages.NotificationMessages;
import myfnfui.ProfileAddUpdate;
import myfnfui.RefreshFriendList;
//import myfnfui.SetDynamicFriend;
import myfnfui.groups.CreateGroup;
import myfnfui.groups.GroupConstants;
import myfnfui.groups.GroupDto;
import myfnfui.groups.GroupMemberDto;
import myfnfui.groups.GroupsWindow;
import myfnfui.groups.MapGroupResponse;
import myfnfui.groups.NewGruopFields;
import myfnfui.groups.RemoveGruop;
import myfnfui.groups.TempGruoupContainer;
import myfnfui.groups.grouphelper.PreviousGruopHelpers;
import myfnfui.headers.HeaderConstants;
import myfnfui.notifications.PresenceNotification;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.FriendlistMapping;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.tampClass.TempStorageOfContactList;

/**
 *
 * @author user
 */
public class PacketProcessor extends Thread {

    public static boolean stop = false;
    private PackageActions pkg_aActions = new PackageActions();
    RefreshFriendList get_frind_list = new RefreshFriendList();
    PreviousGruopHelpers groupsWindow = new PreviousGruopHelpers();
    DownLoaderHelps dLHelp = new DownLoaderHelps();
    InviteFrined invite_obj = new InviteFrined();
    ProfileAddUpdate profile_update = new ProfileAddUpdate();
    StoreAndNotifyTreads storeAndNotify = new StoreAndNotifyTreads();
//    ExecutorService executor = Executors.newFixedThreadPool(50);
//    Runnable worker;

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (!stop) {
            try {

//                UdpPacket packet_to_be_processed = Repository.getInstance().getCall().poll();
//                worker = new CallWorkerThread(packet_to_be_processed);
//                executor.execute(worker);

                if (TempStorageOfContactList.getInstance().getResponse_msg_container() != null) {
                    for (String key : TempStorageOfContactList.getInstance().getResponse_msg_container().keySet()) {

                        if (key.equals(SocketConstants.CONTACT_LIST)) {
                            //System.out.println(" contact list:" + TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                            if (GUI24FnF.getObjMain() != null) {
                                GUI24FnF.set_visivility(true);
                                if (GUI24FnF.flash_JLabel != null) {
                                    GUI24FnF.flash_JLabel.setVisible(false);
                                }

                                get_frind_list.getfriend_list();
                                if (GUI24FnF.buttons_gui24fnf[ GUI24FnF.invite_index] != null) {
                                    GUI24FnF.buttons_gui24fnf[ GUI24FnF.invite_index].setVisible(true);
                                }
                            }
                        } else if (HelperMethods.check_string_contains_substring(key, HeaderConstants.TYPE_fnf_headers)) {
//                            String response_msg = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
//                            HeadersPOJO headers = HelperMethods.mapHeaders(response_msg);
//                            if (headers.getHeaderValues() != null) {
//                                for (HeaderDTO dtos : headers.getHeaderValues()) {
//                                   // HeaderContainer.getInstance().getHeaders().put(dtos.getSipHeader(), dtos.get24fnfHeader().toString());
//                                   System.out.println("public static final String " + dtos.getSipHeader() + " = \"" + dtos.getSipHeader() + "\";");
//                                }
//                            }
                            //  System.out.println("sdfsdf" + response_msg);
                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_edit_group_member)) {


                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            System.out.println("edit gruop list" + gruop_list);
                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    System.out.println(changed_friend.getUserIdentity());
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
                                    System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                    TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());

                                }
                                HelperMethods.create_confrim_panel_no_cancel(NotificationMessages.GROUP_EDIT_SUCCESS, 40);
                            }

                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_edit_members_by_others)) {
                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            System.out.println("edit gruop list" + gruop_list);
                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    System.out.println(changed_friend.getUserIdentity());
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
                                    System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());
                                    }

                                }
                                String group_name = " ";
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName().length() > 0) {
                                    group_name = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName();
                                }
                                new PresenceNotification().createNotificationWindow("Group " + group_name, "Members Updated");
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                        groupsWindow.hide_save_canecl_panel_in_buttom();
                                    }
                                }
                            }
                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_add_member_in_previous_list)) {


                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            System.out.println("edit gruop list" + gruop_list);
                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                // System.out.println(changed_friend.getUserIdentity());
                                //  int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
                                //    System.out.println(gurop_id + "  " + changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                GroupMemberDto dto = new GroupMemberDto();
                                dto.setAdmin(changed_friend.getAdmin());
                                dto.setUserIdentity(changed_friend.getUserIdentity());

                                if (changed_friend.getFirstName() != null) {
                                    dto.setFirstName(changed_friend.getFirstName());
                                }
                                if (changed_friend.getLastName() != null) {
                                    dto.setLastName(changed_friend.getLastName());
                                    System.out.println(changed_friend.getLastName());
                                }
                                if (changed_friend.getPresence() != null) {
                                    dto.setPresence(changed_friend.getPresence());
                                }
                                if (changed_friend.getGender() != null) {
                                    dto.setGender(changed_friend.getGender());
                                }
                                try {
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().add(dto);
                                        //    new NotificationWindow().createNotificationWindow(dto.getFirstName() + " " + dto.getLastName() + " added in " + TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName());
                                        new PresenceNotification().createNotificationWindow(dto.getFirstName() + " " + dto.getLastName(), " Added in " + TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName());
                                    }

                                } catch (Exception ep) {
                                }


                            }
                            if (GroupsWindow.getGroupWindow() != null) {
                                if (GroupsWindow.currentSelectedGruop != null) {
                                    groupsWindow.draw_grouplist();
                                    groupsWindow.draw_group_details(gurop_id);
                                    groupsWindow.hide_save_canecl_panel_in_buttom();
                                }
                            }

                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_add_group_member)) {


                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //  System.out.println("edit gruop list" + gruop_list);
                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                System.out.println(changed_friend.getUserIdentity());
                                //  int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                        Boolean admin = TempGruoupContainer.getInstance().getGroupMemberContainer().get(userid);
                                System.out.println(gurop_id + "  " + changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
                                GroupMemberDto dto = new GroupMemberDto();
                                dto.setAdmin(changed_friend.getAdmin());
                                dto.setUserIdentity(changed_friend.getUserIdentity());

//                                if (changed_friend.getFirstName() != null) {
//                                    dto.setFirstName(changed_friend.getFirstName());
//                                }
//                                if (changed_friend.getLastName() != null) {
//                                    dto.setLastName(changed_friend.getLastName());
//                                    System.out.println(changed_friend.getLastName());
//                                }
//                                if (changed_friend.getPresence() != null) {
//                                    dto.setPresence(changed_friend.getPresence());
//                                }
//                                if (changed_friend.getGender() != null) {
//                                    dto.setGender(changed_friend.getGender());
//                                }
                                try {
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().add(dto);
                                    }

                                } catch (Exception ep) {
                                }


                            }
                            if (GroupsWindow.getGroupWindow() != null) {
                                if (GroupsWindow.currentSelectedGruop != null) {
                                    groupsWindow.draw_grouplist();
                                    groupsWindow.draw_group_details(gurop_id);
                                    groupsWindow.hide_save_canecl_panel_in_buttom();
                                }
                            }

                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_create_group)) {
//                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
//                            System.out.println("created gruop list" + gruop_list);
//                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
//                            Long gurop_id = mapper.getGroupId();
//                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
//                                System.out.println(changed_friend.getUserIdentity());
//                                int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                                System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
//                                TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());
//
//                            }
                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_new_group)) {
                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //   String gruop_list = return_data;
                            String gruop = "Unknown";

                            GroupDto mapper = HelperMethods.map_into_GruopDto(gruop_list);
                            if (mapper.getGroupName().length() > 0) {
                                gruop = mapper.getGroupName();
                            }
                            TempGruoupContainer.getInstance().getGruopFriendContainer().put(mapper.getGroupId(), mapper);
                            new PresenceNotification().createNotificationWindow("Group " + gruop, "Created now ");
                        } else if (key.equals(GroupConstants.TYPE_delete_group)) {
                            String response_pak = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            JsonFields fields = HelperMethods.response_json(response_pak);
                            String gruop_name_in_action = "";

                            if (fields.getGroupId() > 0) {

                                GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId());
                                if (dto != null) {
                                    gruop_name_in_action = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId()).getGroupName();
                                    TempGruoupContainer.getInstance().getGruopFriendContainer().remove(fields.getGroupId());
                                }

                                if (GroupsWindow.getGroupWindow() != null) {
                                    HelperMethods.create_confrim_panel_no_cancel(gruop_name_in_action + NotificationMessages.DELETE_SUCCESS, 20);
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.delete_single_group_details();
                                    }
                                } else {
                                    // new PresenceNotification().createNotificationWindow("Group " + gruop_name_in_action, "Deleted");
                                }
                            }

//                            System.out.println("created gruop list" + gruop_list);
//                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
//                            Long gurop_id = mapper.getGroupId();
//                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
//                                System.out.println(changed_friend.getUserIdentity());
//                                int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                                System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
//                                TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());
//
//                            }
                        } else if (key.equals(GroupConstants.TYPE_leave_group)) {
                            //   System.out.println("key : " + key);
                            String response_pak = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            JsonFields fields = HelperMethods.response_json(response_pak);
                            String gruop_name_in_action = "";
                            //   System.out.println("key : " + key);
                            if (fields != null) {

                                GroupDto dto = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId());
                                if (dto != null) {
                                    gruop_name_in_action = TempGruoupContainer.getInstance().getGruopFriendContainer().get(fields.getGroupId()).getGroupName();
                                    TempGruoupContainer.getInstance().getGruopFriendContainer().remove(fields.getGroupId());
                                }

                                if (GroupsWindow.getGroupWindow() != null) {
                                    HelperMethods.create_confrim_panel_no_cancel("Successfully left " + gruop_name_in_action + " ", 20);
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.delete_single_group_details();
                                    }
                                } else {
                                    //  new PresenceNotification().createNotificationWindow(gruop_name_in_action, "Deleted");
                                }
                            }

//                            System.out.println("created gruop list" + gruop_list);
//                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
//                            Long gurop_id = mapper.getGroupId();
//                            for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
//                                System.out.println(changed_friend.getUserIdentity());
//                                int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
//                                System.out.println(changed_friend.getUserIdentity() + ":" + changed_friend.getAdmin());
//                                TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index).setAdmin(changed_friend.getAdmin());
//
//                            }
                        } else if (key.equals(GroupConstants.TYPE_groupList)) {
                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //   System.out.println("gruoplist:" + gruop_list);
                            MapGroupResponse fetchGroup = HelperMethods.mappGroupResponse(gruop_list);
                            //       GroupDto dd = fetchGroup.getGroupList().get(0);
                            for (GroupDto dto : fetchGroup.getGroupList()) {
                                TempGruoupContainer.getInstance().getGruopFriendContainer().put(dto.getGroupId(), dto);
                                //  System.out.println("Gruop name 124: " + dto.getGroupId());
                            }
                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_remove_group_member)) {
                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //   System.out.println("remove group member" + gruop_list);
                            CreateGroup mapper = HelperMethods.createGruopMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());
                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().remove(index);
                                    }

                                }

                                //     new NotificationWindow().createNotificationWindow("Group member Deleted from " + TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName());
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                    }
                                }
                            }

                        } else if (HelperMethods.check_string_contains_substring(key, GroupConstants.TYPE_remove_a_group_member_by_other)) {
                            String gruop_list = TempStorageOfContactList.getInstance().getResponse_msg_container().get(key);
                            //System.out.println("remove group member by other" + gruop_list);
                            RemoveGruop mapper = HelperMethods.removeGruopMembersMapper(gruop_list);
                            Long gurop_id = mapper.getGroupId();
                            if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id) != null) {
                                for (NewGruopFields changed_friend : mapper.getGroupMembers()) {
                                    int index = TempGruoupContainer.getInstance().get_friend_index_number_Members_stats(gurop_id, changed_friend.getUserIdentity());

                                    if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().get(index) != null) {
                                        TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupMembers().remove(index);
                                    }
                                }

                                String group_name = " ";
                                if (TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName().length() > 0) {
                                    group_name = TempGruoupContainer.getInstance().getGruopFriendContainer().get(gurop_id).getGroupName();
                                }
                                // new PresenceNotification().createNotificationWindow("Group " + group_name, "Member Deleted");
                                if (GroupsWindow.getGroupWindow() != null) {
                                    if (GroupsWindow.currentSelectedGruop != null) {
                                        groupsWindow.draw_grouplist();
                                        groupsWindow.draw_group_details(gurop_id);
                                    }
                                }
                            }



                        } else {
                            //  System.out.println("--------------------Parsing contact list---------------");
                            //System.out.println("Msg:" + TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                            JsonFields js = HelperMethods.response_json(TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                            if (js.getAction().equals(SocketConstants.RESPONSE)) {

                                if (js.getType().equals((SocketConstants.CONTACT_LIST))) {

                                    //   System.out.println("--------------------Parsing contact list---------------2");
                                    FriendlistMapping tamp_list = HelperMethods.friendlist_mapping(TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                                    if (!tamp_list.getContactList().isEmpty()) {
                                        for (final UserAgentProfile singleProfile : tamp_list.getContactList()) {
                                            //    System.out.println("user :" + singleProfile.getUserIdentity());
                                            FriendList.getInstance().getFriend_hash_map().put(singleProfile.getUserIdentity(), singleProfile);
                                            if (!singleProfile.getProfileImage().isEmpty()) {
                                                // System.out.println("--------------------Parsing contact list---------------3");
                                                String image_name = HelperMethods.getImageName_from_url(singleProfile.getProfileImage());

                                                if (HelperMethods.check_string_contains_substring(image_name, ".")) {

                                                    File f = new File(dLHelp.getDestinationFolder() + image_name);
                                                    if (!f.exists()) {
                                                        // System.out.println("sdjflksdj ffffffffffffffffffffffffffff-----------");
                                                        if (singleProfile.getProfileImage() != null && singleProfile.getProfileImage().length() > 0) {
                                                            storeAndNotify.store_and_notify_image_downloader_thread(singleProfile.getUserIdentity(), singleProfile.getProfileImage());
                                                        }

                                                    } else {
                                                        //System.out.println("File exist " + image_name);
                                                    }
                                                }
                                            }

                                        }

                                    } else {
//                                        System.out.println("empty");
//                                        if (GUI24FnF.flash_JLabel != null) {
//                                            GUI24FnF.flash_JLabel.setVisible(false);
//                                        }
                                    }
//                                    FriendlistMapping tamp_list = HelperMethods.friendlist_mapping(TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
//                                    if (!tamp_list.getContactList().isEmpty()) {
//                                        for (final UserAgentProfile singleProfile : tamp_list.getContactList()) {
//                                            FriendList.getInstance().getFriend_hash_map().put(singleProfile.getUserIdentity(), singleProfile);
//                                            if (singleProfile.getProfileImage().length() > 0) {
//                                                String image_name = HelperMethods.getImageName_from_url(singleProfile.getProfileImage());
//
//                                                if (HelperMethods.check_string_contains_substring(image_name, ".")) {
//                                                    File f = new File(dLHelp.getDestinationFolder() + image_name);
//                                                    if (!f.exists()) {
//                                                        if (js.getProfileImage() != null && js.getProfileImage().length() > 0) {
//                                                            storeAndNotify.store_and_notify_image_downloader_thread(js.getUserIdentity(), js.getProfileImage());
//                                                        }
//
//                                                    } else {
//                                                        //System.out.println("File exist " + image_name);
//                                                    }
//                                                }
//                                            }
//
//                                        }
//
//                                    } else {
////                                        System.out.println("empty");
////                                        if (GUI24FnF.flash_JLabel != null) {
////                                            GUI24FnF.flash_JLabel.setVisible(false);
////                                        }
//                                    }
                                    //   GUI24FnF.flash_JLabel.setVisible(false);
                                }

                                if (key.equals(GroupConstants.TYPE_groupList)) {
                                } else {

                                    if (GUI24FnF.getObjMain() != null && SetDynamicFriend.repaint_friendlist) {

                                        GUI24FnF.set_visivility_flash_loading_image(false);
                                        GUI24FnF.set_visivility(true);
                                        get_frind_list.getfriend_list();
                                    }
                                }
                            } else if (js.getAction().equals(SocketConstants.PRESENCE)) {

                                // if (js.getAction().equals(SocketConstants.PRESENCE)) {
                                // System.out.println("Presence request: " + data);
                                if (js.getIndexOfHeaders() != null) {
                                    //System.out.println("changes in presence" + js.getIndexOfHeaders().length);
                                    int[] indexes = new int[js.getIndexOfHeaders().length];
                                    indexes = js.getIndexOfHeaders();
                                    UserAgentProfile up = FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity());
                                    if (up != null) {
                                        if (!up.getUserIdentity().isEmpty() && up.getUserIdentity().length() > 0) {
                                            for (int indexNumber : indexes) {
                                                if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setFirstName(js.getFirstName());
                                                }
                                                if (indexNumber == SocketConstants.CONS_LAST_NAME) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setLastName(js.getLastName());
                                                }
                                                if (indexNumber == SocketConstants.CONS_GENDER) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setGender(js.getGender());
                                                }
                                                if (indexNumber == SocketConstants.CONS_COUNTRY) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setCountry(js.getCountry());
                                                }
                                                if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setMobilePhone(js.getMobilePhone());
                                                }
                                                if (indexNumber == SocketConstants.CONS_EMAIL) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setEmail(js.getEmail());
                                                }
                                                if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setWhatisInYourMind(js.getWhatisInYourMind());
                                                }
                                                if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setMobilePhoneDialingCode(js.getMobilePhoneDialingCode());
                                                }
                                                if (indexNumber == SocketConstants.CONS_PRESENCE) {



                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setPresence(js.getPresence());
                                                    String online_offline = "";
                                                    if (js.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE)) {
                                                        online_offline = "Online";
                                                        new PresenceNotification().createNotificationWindow(up.getFirstName() + " " + up.getLastName(), " is now " + online_offline);
                                                    } else {
                                                        online_offline = "Offline";
                                                    }


                                                }
                                                if (indexNumber == SocketConstants.CONS_FRIENDSHIP_STATUS) {
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setFriendShipStatus(js.getFriendShipStatus());
                                                }
                                                if (indexNumber == SocketConstants.CONS_PROFILE_IMAGE) {
                                                    System.out.println("previos " + FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).getProfileImage());
                                                    String previous_image = HelperMethods.getImageName_from_url(FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).getProfileImage());
                                                    File f_pre = new File(dLHelp.getDestinationFolder() + previous_image);
                                                    if (f_pre.exists()) {
                                                        f_pre.delete();
                                                    }
                                                    FriendList.getInstance().getFriend_hash_map().get(js.getUserIdentity()).setProfileImage(js.getProfileImage());
                                                    if (js.getProfileImage() != null && js.getProfileImage().length() > 0) {

                                                        storeAndNotify.store_and_notify_image_downloader_thread(js.getUserIdentity(), js.getProfileImage());
                                                    }

                                                }
                                                //      PackageActions.sendConfirmationPak(js.getPacketIdFromServer());

                                                if (GUI24FnF.getObjMain() != null && SetDynamicFriend.repaint_friendlist) {
                                                    GUI24FnF.set_visivility_flash_loading_image(false);
                                                    GUI24FnF.set_visivility(true);
                                                    get_frind_list.getfriend_list();
                                                }
                                                if (GroupsWindow.currentSelectedGruop != null) {
                                                    if (GroupsWindow.getGroupWindow() != null) {
                                                        groupsWindow.draw_group_details(GroupsWindow.currentSelectedGruop);
                                                    }
                                                }
                                            }
                                        } else {
                                            TempStorageOfContactList.getInstance().getResponse_msg_container().put(key, TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                                        }
                                    }
                                }

                            }
                        }
                        TempStorageOfContactList.getInstance().getResponse_msg_container().remove(key);
                    }


                } else {
                    synchronized (this) {
                        this.wait();
                    }
                }
            } catch (Exception ex) {
                //ex.printStackTrace();
            }
        }
    }

    public void halt() {
        stop = true;
    }
}
