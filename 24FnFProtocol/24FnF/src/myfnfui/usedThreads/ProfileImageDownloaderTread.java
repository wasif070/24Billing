/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.usedThreads;

import helpers.CheckInternetConnection;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import imageDownload.DownLoaderHelps;
import imageDownload.DownloadThread;
import imageDownload.Lock;
import myfnfui.GUI24FnF;
import myfnfui.ProfileAddUpdate;
import myfnfui.RefreshFriendList;
import myfnfui.SendReceiveListener;
import myfnfui.containers.MyfnfHashMaps;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author user
 */
public class ProfileImageDownloaderTread extends Thread {

    //  private MessageCheckSendListner messageCheckSendListner;
    public static boolean stop = false;
    PackageActions pak_action = new PackageActions();
    SendReceiveListener listner = null;
    DownLoaderHelps dHelp = new DownLoaderHelps();

    @Override
    public void run() {
        // TODO Auto-generated method stub
        while (!stop) {
            try {
                if (!MyfnfHashMaps.getInstance().getProfileImageUrls().isEmpty()) {
                    //   System.out.println("---------------------jsdflksdjf lsdjfl sdlfjsdlkjfl ksdjfjsdlkf jlk--------");
                    for (String key : MyfnfHashMaps.getInstance().getProfileImageUrls().keySet()) {
                        String profile_url = MyfnfHashMaps.getInstance().getProfileImageUrls().get(key);
                    //    System.out.println("Profile Url :" + profile_url);
                        String image_name = HelperMethods.getImageName_from_url(profile_url);
                        //System.out.println("my profile:" + profile_url);
                        if (HelperMethods.check_string_contains_substring(image_name, ".")) {
                            if (CheckInternetConnection.getResponseCode(profile_url) == 200) {

                                Lock lock = new Lock();
                                DownloadThread dt = new DownloadThread(profile_url, dHelp.getDestinationFolder() + image_name, lock);
                                dt.start();
                                while (lock.getRunningThreadsNumber() > 0) {
                                    synchronized (lock) {
                                        lock.wait();
                                    }

                                }
                            } else {
                                //   System.out.println("Not Found:" + TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
                            }
                        }
                        MyfnfHashMaps.getInstance().getProfileImageUrls().remove(key);

                        if (key.equals(MyFnFSettings.LOGIN_USER_ID)) {
                            if (ProfileAddUpdate.getProfileUpdateObject() != null) {
//                                System.out.println("profile Upadate window");
                                // ProfileAddUpdate.getProfileUpdateObject().profile_repaint();
                            }
                        } else {
                            if (GUI24FnF.getObjMain() != null && SetDynamicFriend.repaint_friendlist) {
                                RefreshFriendList get_frind_list = new RefreshFriendList();
                                get_frind_list.getfriend_list();
                            }
                        }

                    }

                    if (MyfnfHashMaps.getInstance().getProfileImageUrls().isEmpty()) {
                        synchronized (this) {
                            this.wait();
                        }
                    }

                } else {
//                    System.out.println("watting Profile Image Downloader Thread");
                    synchronized (this) {
                        this.wait();
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    public void halt() {
        stop = true;
    }
}
