/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import com.lti.civil.CaptureException;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticFields;
import helpers.StaticImages;
import imageDownload.DownLoaderHelps;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import myfnfui.packetsInfo.PackedDistributor;
import myfnfui.tampClass.StaticFieldsForProfileUpdate;
import takePicture.ChangeProfileImage;
//import javax.swing.ImageIcon;

/**
 *
 * @author FaizAhmed
 */
public class ProfileAddUpdate implements MouseListener, PackedDistributor {

    JLabel errorLabel_new;
    JLabel firstNameLable;
    JLabel lastNameLable = null;
    JLabel userIDLable = null;
    JLabel passwordLable = null;
    JLabel conPasswordLable = null;
    JLabel emailLable = null;
    JLabel mobileNoLable = null;
    JLabel countryLable = null;
    JLabel genderLable = null;
    JPanel userImagePanel;
    JPanel imgageContactPanel;
    JPanel userPart;
    JPanel rowPanel;
    JLabel country_code_Lable;
    JButton button_edit_country_code;
    JTextArea what_in_mind_textarea;
    String what_in_mind = "";
    JButton button_what_in_mind;
    public static ProfileAddUpdate profileUpdateObject;
    DownLoaderHelps dLHelp = new DownLoaderHelps();
    String location = "";
//    JoptionFrame joptionFrame;
    //JButton button_edit_country_code;
    HelperMethods helperMethods = new HelperMethods();

    public static ProfileAddUpdate getProfileUpdateObject() {
        return profileUpdateObject;
    }

    public static void setProfileUpdateObject(ProfileAddUpdate profileUpdateObject) {
        ProfileAddUpdate.profileUpdateObject = profileUpdateObject;
    }

    public ProfileAddUpdate() {
        setProfileUpdateObject(this);
    }

    public void profile_repaint() {

        GUI24FnF.columnpanel.removeAll();

        GUI24FnF.columnpanel.add(createProfileScreen());
        GUI24FnF.columnpanel.revalidate();
        GUI24FnF.columnpanel.repaint();
    }

    private JPanel createProfileScreen() {
        int top_height = 140;
        int border_width = 280;
        int margin_left = 10;
        int value_width = 200;
        int value_font_size = 12;
        int label_height_increment_factor = 5;
        int value_height_increment_factor = 15;
        int edit_button_right_margin = 50;
        int border_height_increment_factor = 30;

        rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(250, 550));
        rowPanel.setBackground(Color.WHITE);
        //rowPanel.setBorder(DefaultSettings.friend_list_border_color);

        // JLabel change_picture = DesignClasses.makeJLabel_with_background_font_size_width_color("Change", 5, 0, 100, 20, 11, 1, Color.BLUE, Color.white, 0);
        // rowPanel.add(change_picture);
        rowPanel.setLayout(null);

        int image_size = 115;
        BufferedImage img = null;
        location = "";
        if (MyFnFSettings.userProfile.getProfileImage().length() > 0) {
            String testString = MyFnFSettings.userProfile.getProfileImage();
            String image_name = HelperMethods.getImageName_from_url(testString);
            //   System.out.println("******************************" + image_name);
            File f = new File(dLHelp.getDestinationFolder() + image_name);
            if (f.exists()) {
                location = dLHelp.getUser_images_location() + image_name;
            } else {
                location = DesignClasses.get_scaled_male_or_female_image(MyFnFSettings.userProfile.getGender());
            }
        } else {
            // String ikk = DesignClasses.get_male_or_female_image(single_p.getGender());
            location = DesignClasses.get_scaled_male_or_female_image(MyFnFSettings.userProfile.getGender());

        }

        try {
            if (DesignClasses.file_exists(location)) {
                BufferedImage tmp = ImageIO.read(new File(location));
                img = DesignClasses.scaleImage(image_size, image_size, tmp);
            } else {
                //   System.out.println("file not exits");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // JLabel user_image = new JLabel(new ImageIcon((Image) img));
        JLabel user_image = helperMethods.create_image_from_url(MyFnFSettings.userProfile.getGender(), MyFnFSettings.userProfile.getProfileImage(), 5, 2, image_size, image_size);
        user_image.setBorder(DefaultSettings.border_light_blue);
        // user_image.setBounds(5, 2, image_size, image_size);
        rowPanel.add(user_image);


        JButton image_change_btton = DesignClasses.create_image_button_with_text_scale1("Change", 40, 118);
        image_change_btton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ChangeProfileImage help = new ChangeProfileImage();

//                DesignClasses.edit_funtions(StaticFields.PHONE_NO_TEXT, mobileNoLable.getText(), StaticFields.MOBILE_PHONE);
//               
                help.change_profile_pic(location);
                if (ChangeProfileImage.takeShot) {
                    try {
                        ChangeProfileImage.takeShot = false;
                        ChangeProfileImage.captureStream.stop();
                        // profile_repaint();
                    } catch (CaptureException ex) {
                        Logger.getLogger(ProfileAddUpdate.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            }
        });
        rowPanel.add(image_change_btton);



        what_in_mind = "";
        if (MyFnFSettings.userProfile.getWhatisInYourMind() != null) {
            what_in_mind = MyFnFSettings.userProfile.getWhatisInYourMind().toString();
        } else {
            what_in_mind = "What's on mind?";
        }
        //   final JTextField what_is = DesignClasses.what_is_in_ur_mind(what_in_mind, 125, 0);
        String image_source = StaticImages.IMAGE_NOTIFY;
        URL imgURL = getClass().getClassLoader().getResource(image_source);
        final ImageIcon imageIcon = new ImageIcon(imgURL);
//        what_in_mind_textarea = new JTextArea(what_in_mind) {
//            Image image = imageIcon.getImage();
//
//            public void paint(Graphics g) {
//                setOpaque(false);
//                g.drawImage(image, 0, 0, this);
//                super.paint(g);
//            }
//        };
//        //  what_in_mind_textarea = new JTextArea("dd");
//        what_in_mind_textarea.setBounds(125, 0, 145, 65);
//        what_in_mind_textarea.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
//        what_in_mind_textarea.setForeground(DefaultSettings.text_color1);
//       // what_in_mind_textarea.setMargin(new Insets(8, 10, 10, 10));
//        what_in_mind_textarea.setEditable(false);
//        what_in_mind_textarea.setWrapStyleWord(true);
//        what_in_mind_textarea.setLineWrap(true);
        //  what_in_mind_textarea.setBorder(BorderFactory.createCompoundBorder(what_in_mind_textarea.getBorder(), BorderFactory.createEmptyBorder(5, 10, 15, 5)));
        int margin_top = 2;
        int text_arear_height = 70;
        what_in_mind_textarea = DesignClasses.create_text_area(what_in_mind, 125, margin_top, 10, text_arear_height);
        LightScrollPane what_in_mind_textarea_sroll = new LightScrollPane(what_in_mind_textarea);
        what_in_mind_textarea.setEditable(false);
        what_in_mind_textarea_sroll.setBorder(DefaultSettings.border_light_blue);
        what_in_mind_textarea_sroll.setBounds(125, margin_top, 150, text_arear_height);
        what_in_mind_textarea_sroll.setBorder(BorderFactory.createCompoundBorder(
                what_in_mind_textarea_sroll.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        //  what_mind_text_area= DesignClasses.create_text_area_with_scroll_panel(what_in_mind, 125, margin_top, 145, text_arear_height);
        rowPanel.add(what_in_mind_textarea_sroll);
        String edit = "Edit";
        String save = "Save";
        // button_what_in_mind = DesignClasses.create_SAVE_buttons(border_width - edit_button_right_margin, 65);
        button_what_in_mind = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, margin_top + text_arear_height + 8);
//        DesignClasses.create_SAVE_buttons(border_width - edit_button_right_margin, 65);
        button_what_in_mind.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.change_whats_in_mind(MyFnFSettings.userProfile.getWhatisInYourMind());
//                HelperMethods.change_profile_pic();
//                String what_in_changed = what_in_mind_textarea.getText().toString();
//                try {
//                    //System.out.println(what_in_changed);
//                    Thread.sleep(1000);
//                    if (GetResponseJsons.change_what_is_in_mind(MyFnFSettings.userProfile.getSessionId(), what_in_changed)) {
//                        what_in_mind_textarea.setText(what_in_changed);
//                        MyFnFSettings.userProfile.setWhatisInYourMind(what_in_changed);
//                    } else {
//                        what_in_mind_textarea.setText("Try Again !!");
//                    }
//                } catch (InterruptedException ex) {
//                    Thread.currentThread().interrupt();
//                }
            }
        });
        // button_what_in_mind.setEnabled(false);
        rowPanel.add(button_what_in_mind);
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.MY_FNF_NAME_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        //  userIDLable = DesignClasses.makeJLabel_text_color2(MyFnFSettings.LOGIN_USER_ID, margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size);
        userIDLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.LOGIN_USER_ID, margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        userIDLable.addMouseListener(this);
        rowPanel.add(userIDLable);
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.FIRST_NAME_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        firstNameLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getFirstName(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        firstNameLable.addMouseListener(this);
        rowPanel.add(firstNameLable);
        // JButton button_edit_first_name = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_first_name = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        button_edit_first_name.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                DesignClasses.edit_funtions(StaticFields.FIRST_NAME_TEXT, firstNameLable.getText(), StaticFields.FIRST_NAME);
//
//                profile_repaint();
                HelperMethods.openDialog(StaticFields.FIRST_NAME_TEXT, firstNameLable.getText(), StaticFields.FIRST_NAME);
//                profile_repaint();
//                GUI24FnF.login_user_name.setText(MyFnFSettings.userProfile.getFirstName() + " " + MyFnFSettings.userProfile.getLastName());
//                //  GUI24FnF.



            }
        });
        rowPanel.add(button_edit_first_name);
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.LAST_NAME_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        lastNameLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getLastName(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(lastNameLable);
        //rowPanel.add();
        JButton button_edit_last_name = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);

        button_edit_last_name.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.openDialog(StaticFields.LAST_NAME_TEXT, lastNameLable.getText(), StaticFields.LAST_NAME);
                //  profile_repaint();
//                DesignClasses.edit_funtions(StaticFields.LAST_NAME_TEXT, lastNameLable.getText(), StaticFields.LAST_NAME);
//                profile_repaint();
                //     GUI24FnF.login_user_name.setText(MyFnFSettings.userProfile.getFirstName() + " " + MyFnFSettings.userProfile.getLastName());

            }
        });
        rowPanel.add(button_edit_last_name);
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));
//change password
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.CHANGE_PASSWORD_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 120, 11, 1, DefaultSettings.text_color2));
        passwordLable = DesignClasses.makeJLabel_with_text_color("***********", margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(passwordLable);
        //rowPanel.add();
        //JButton button_edit_password = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_password = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        button_edit_password.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.change_password_openDialog("Change Password");
            }
        });
        rowPanel.add(button_edit_password);
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));
//change country
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.COUNTRY_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        countryLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getCountry(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(countryLable);
        //   JButton button_edit_country = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_country = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        rowPanel.add(button_edit_country);
        button_edit_country.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.change_country_name();
//                edit_Contry_from_joption(StaticFields.COUNTRY_CODE_TEXT, countryLable.getText(), StaticFields.COUNTRY);
                //   profile_repaint();


            }
        });
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));


        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.PHONE_NO_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        mobileNoLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getMobilePhoneDialingCode() + MyFnFSettings.userProfile.getMobilePhone(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(mobileNoLable);

        //  JButton button_edit_mobile = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_mobile = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        rowPanel.add(button_edit_mobile);
        button_edit_mobile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                DesignClasses.edit_funtions(StaticFields.PHONE_NO_TEXT, mobileNoLable.getText(), StaticFields.MOBILE_PHONE);
//               
                HelperMethods.change_mobile_no();
                profile_repaint();

            }
        });
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));


        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.EMAIL_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        emailLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getEmail(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(emailLable);
        //  JButton button_edit_email = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_email = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        rowPanel.add(button_edit_email);
        button_edit_email.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
//                DesignClasses.edit_funtions(StaticFields.EMAIL_TEXT, emailLable.getText(), StaticFields.EMAIL);
                HelperMethods.openDialog(StaticFields.EMAIL_TEXT, emailLable.getText(), StaticFields.EMAIL);
                // profile_repaint();
            }
        });
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));

        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.GENDER_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        genderLable = DesignClasses.makeJLabel_with_text_color(MyFnFSettings.userProfile.getGender(), margin_left, top_height = top_height + value_height_increment_factor, value_width, value_font_size, 1, DefaultSettings.text_color1);
        rowPanel.add(genderLable);
        //   JButton button_edit_gender = DesignClasses.create_edit_buttons(border_width - edit_button_right_margin, top_height + 5);
        JButton button_edit_gender = DesignClasses.create_image_button_with_text_scale1(edit, border_width - edit_button_right_margin, top_height + 5);
        rowPanel.add(button_edit_gender);
        button_edit_gender.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                HelperMethods.change_gender();
                //    profile_repaint();
            }
        });
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height = top_height + border_height_increment_factor, border_width));



        return rowPanel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == firstNameLable) {
            if (e.getClickCount() == 2) {
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //   throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }
//
//    @Override
//    public void onReceivedPacket(SendReceive udp, UdpPacket packet) {
//        String return_data = new String(packet.getData());
//        //  System.out.println("returned in profile" + return_data);
//        JsonFields getJson = HelperMethods.response_json(return_data);
//        if (getJson.getType().equals(SocketConstants.WHAT_IS_IN_UR_MIND_TYPE)) {
//            if (getJson.getSuccess()) {
//                //  System.out.println(HelperMethods.whatsInMind);
//                MyFnFSettings.userProfile.setWhatisInYourMind(StaticFieldsForProfileUpdate.whatisInYourMind);
//                what_in_mind_textarea.setText(StaticFieldsForProfileUpdate.whatisInYourMind);
//                StaticFieldsForProfileUpdate.whatisInYourMind = null;
//            } else {
//                // StaticFieldsForProfileUpdate.whatisInYourMind = null;
//                HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 50);
//            }
//        } else if (getJson.getType().equals(SocketConstants.USER_PROFILE)) {
//            if (getJson.getSuccess()) {
//                int[] indexes = new int[1];
//                indexes = getJson.getIndexOfHeaders();
//                if (indexes != null) {
//                    int in = indexes[0];
//                    if (in == 1 || in == 2) {
//                        GUI24FnF.login_user_name.setText(MyFnFSettings.userProfile.getFirstName() + " " + MyFnFSettings.userProfile.getLastName());
//                    }
//                }
//                profile_repaint();
//            } else {
//                HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 50);
//            }
//        }
//    }

    @Override
    public void onReceivedData(JsonFields getJsonClass) {
         System.out.println("returned in profile");
        JsonFields getJson = getJsonClass;
        if (getJson.getType().equals(SocketConstants.WHAT_IS_IN_UR_MIND_TYPE)) {
            if (getJson.getSuccess()) {
                //  System.out.println(HelperMethods.whatsInMind);
                MyFnFSettings.userProfile.setWhatisInYourMind(StaticFieldsForProfileUpdate.whatisInYourMind);
                what_in_mind = MyFnFSettings.userProfile.getWhatisInYourMind();
                System.out.println("What =>"+ MyFnFSettings.userProfile.getWhatisInYourMind());
                what_in_mind_textarea.setText(StaticFieldsForProfileUpdate.whatisInYourMind);
                StaticFieldsForProfileUpdate.whatisInYourMind = null;
            } else {
                // StaticFieldsForProfileUpdate.whatisInYourMind = null;
                HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 50);
            }
        } else if (getJson.getType().equals(SocketConstants.USER_PROFILE)) {
            if (getJson.getSuccess()) {
                int[] indexes = new int[1];
                indexes = getJson.getIndexOfHeaders();
                if (indexes != null) {
                    int in = indexes[0];
                    if (in == 1 || in == 2) {
                        GUI24FnF.login_user_name.setText(MyFnFSettings.userProfile.getFirstName() + " " + MyFnFSettings.userProfile.getLastName());
                    }
                }
                profile_repaint();
            } else {
                HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 50);
            }
        }
    }
}
