/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import helpers.APIParametersFiles;
import helpers.GetResponseJsons;
import helpers.MyFnFSettings;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import utils.ContactListProvider;

/**
 *
 * @author FaizAhmed
 */
public class ResetUserProfileFriendList {

    private static Gson jsonLib;
    DatagramSocket socket;
    DatagramPacket packet;
    InetAddress address;

    public ResetUserProfileFriendList() {
        // jsonLib = new GsonBuilder().serializeNulls().create();
    }

    public static void refesh_friend_clss(RefreshFriendList rfl) {
//        jsonLib = new GsonBuilder().serializeNulls().create();
//        String response_string = GetResponseJsons.get_friend_list(MyFnFSettings.userProfile.getSessionId());
//        //System.out.println(response_string);
//        if (response_string.length() > 0) {
//            MyFnFSettings.map_freinds = jsonLib.fromJson(response_string, MapFriendList.class);
//            MyFnFSettings.userProfile.setContactList(MyFnFSettings.map_freinds.getContactList());
//        }

        ContactListProvider provider = new ContactListProvider();
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactList + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId();
        provider.setContactList(url, rfl);
    }

    public static void set_refesh_friend_clss(String response_string) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        if (response_string.length() > 0) {
            MyFnFSettings.map_freinds = new MapFriendList();
            MyFnFSettings.map_freinds = jsonLib.fromJson(response_string, MapFriendList.class);
            MyFnFSettings.userProfile.setContactList(MyFnFSettings.map_freinds.getContactList());
        }
    }

    public static void refesh_user_profile() {
        try {
            // //System.out.println("Inside refesh_user_profile");
            jsonLib = new GsonBuilder().serializeNulls().create();
            String response_string = GetResponseJsons.get_login_json(MyFnFSettings.LOGIN_USER_ID, MyFnFSettings.LOGIN_USER_PASSWORD);
//            System.err.println("b4  " + MyFnFSettings.userProfile);
            if (response_string.length() > 0) {
                MyFnFSettings.userProfile = jsonLib.fromJson(response_string, UserProfile.class);
            } else {
                //MyFnF_Settings.userProfile ke offline banao
            }
            //    System.err.println("after_s  " + response_string);
            //      System.err.println("after  " + MyFnFSettings.userProfile);

        } catch (Exception ex) {
            //System.out.println("Network connection is not available");
        }
    }
}
