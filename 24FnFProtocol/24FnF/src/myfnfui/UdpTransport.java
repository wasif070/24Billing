/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import imageDownload.DownLoaderHelps;
import java.io.IOException;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.usedThreads.StoreAndNotifyTreads;
//import util.AppConstants;

/**
 *
 * @author reefat
 */
public class UdpTransport implements SendReceiveListener {

    SendReceive send_receive;
//    HandlerListener listener;
    int port = 0;
    public static UdpTransport udp_transport;
    DownLoaderHelps dHelp = new DownLoaderHelps();
    PackageActions pak_action = new PackageActions();
    StoreAndNotifyTreads storeAndNotify = new StoreAndNotifyTreads();
    //  PreviousGruopHelpers prevGroup= new PreviousGruopHelpers();

    public static void setUdp_transport(UdpTransport udp_transport) {
        UdpTransport.udp_transport = udp_transport;
    }

    public static UdpTransport getInstance() {
//        if (udp_transport == null) {
//          udp_transport = new UdpTransport(HelperMethods.getAvailablePort());
//            udp_transport = new UdpTransport();
//        }
        return udp_transport;
    }

    public void send(UdpPacket packet) throws IOException {
        send_receive.send(packet);
    }

    public void close_socket() {
        send_receive.halt();
    }
//HelperMethods.getAvailablePort()
//    public UdpTransport(int port) {

    public UdpTransport() {
//        this.port = port;
        initUdp();
        udp_transport = this;
    }

    private void initUdp() {
        UdpSocket udp_socket = null;
        int udp_port = 9998;
        try {
            while (true) {
                try {
                    udp_socket = new UdpSocket(udp_port++);
                    break;
                } catch (Exception e) {
                    try {
                        if (udp_socket != null) {
                            udp_socket.close();
                        }
                    } catch (Exception ex) {
                    }
                }
            }
        } catch (Exception e) {
        }

        port = udp_socket.getSocket().getLocalPort();
        send_receive = new SendReceive(udp_socket, this);
    }

    @Override
    public void onReceivedPacket(SendReceive udp, UdpPacket packet) {
        String data = new String(packet.getData());
//      System.out.println("Received data=> "+data);
        storeAndNotify.store_and_notify_packet_parser_thread(data);
    }
}
