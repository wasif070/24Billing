///*
// * To change this template, choose Tools | Templates
// * and open the template in the editor.
// */
//package myfnfui;
//
//import com.google.gson.Gson;
//import com.google.gson.GsonBuilder;
//import helpers.GetResponseJsons;
//import helpers.MyFnFSettings;
//import java.awt.Color;
//import local.ua.UserAgent;
//import local.ua.UserAgentProfile;
//
///**
// *
// * @author FaizAhmed
// */
//public class SendNewUserRequst extends java.util.TimerTask {
//
//    CreateNewAccount creat_user;
//    protected UserAgent ua;
//    protected UserAgentProfile ua_profile;
//
//    public SendNewUserRequst(CreateNewAccount new_user, UserAgentProfile ua_profile, UserAgent ua) {
//        creat_user = new_user;
//        this.ua_profile = ua_profile;
//        this.ua = ua;
//
//    }
//    private int times = 0;
//
//    public void run() {
//        times++;
//        //  try {
//        //   if (GetResponseJsons.isInternetReachable()) {
//        // //System.out.println("I'm alive...");
//        String new_user_json = GetResponseJsons.get_new_user_json(creat_user.country_prefixTextField.getText().toString(), creat_user.genderComboBox.getSelectedItem().toString(), creat_user.firstNameTextField.getText().trim(), creat_user.lastNameTextField.getText().trim(), creat_user.userIDTextField.getText().trim(), creat_user.countryComboBox.getSelectedItem().toString(), creat_user.emailTextField.getText().trim(), creat_user.mobileNoTextField.getText().trim(), creat_user.passwordTextField.getText(), "add");
//        //System.out.println(new_user_json);
//        Gson jsn;
//        jsn = new GsonBuilder().serializeNulls().create();
//        MyFnFSettings.userProfile = jsn.fromJson(new_user_json, UserProfile.class);
//        if (MyFnFSettings.userProfile.getSuccess().equals(true)) {
//            String user_json = GetResponseJsons.get_login_json(creat_user.userIDTextField.getText().trim(), creat_user.passwordTextField.getText());
//            jsn = new GsonBuilder().serializeNulls().create();
//            MyFnFSettings.userProfile = jsn.fromJson(user_json, UserProfile.class);
//            creat_user.setMainUserInterface();
//            this.cancel();
//        } else {
//            creat_user.save_button.setEnabled(true);
//            this.cancel();
//            creat_user.errorLabel_new.setForeground(Color.RED);
//            creat_user.errorLabel_new.setText(MyFnFSettings.userProfile.getMessage());
//        }
////            } else {
////                creat_user.save_button.setEnabled(true);
////                //System.out.println("Failed Internet connection");
////                creat_user.errorLabel_new.setForeground(Color.RED);
////                creat_user.errorLabel_new.setText("Network Failed");
////                this.cancel();
////            }
////        } catch (MalformedURLException ex) {
////            Logger.getLogger(SendNewUserRequst.class.getName()).log(Level.SEVERE, null, ex);
////        } catch (IOException ex) {
////            Logger.getLogger(SendNewUserRequst.class.getName()).log(Level.SEVERE, null, ex);
////        }
//
//    }
//}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import myfnfui.usedThreads.SetDynamicFriend;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import helpers.GetResponseJsons;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import java.awt.Color;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import local.ua.UserAgent;
import local.ua.UserAgentProfile;

/**
 *
 * @author FaizAhmed
 */
public class SendNewUserRequst extends java.util.TimerTask {
    
    CreateNewAccount creat_user;
    protected UserAgent ua;
    protected UserAgentProfile ua_profile;
    DatagramSocket socket;
    DatagramPacket packet;
    InetAddress address;
    byte[] data;
    public static Gson jsonLib;
    
    public SendNewUserRequst(CreateNewAccount new_user, UserAgentProfile ua_profile, UserAgent ua) {
        creat_user = new_user;
        this.ua_profile = ua_profile;
        this.ua = ua;
        
    }
    private int times = 0;
    
    public void run() {
        times++;
        
        
        
        String mobile = HelperMethods.bd_mobile_number(creat_user.country_prefixTextField.getText().toString(), creat_user.mobileNoTextField.getText().toString());
        creat_user.mobileNoTextField.setText(mobile);
//        String new_user_json = GetResponseJsons.get_new_user_json(
//                creat_user.country_prefixTextField.getText().toString(),
//                creat_user.genderComboBox.getSelectedItem().toString(),
//                creat_user.firstNameTextField.getText().trim(),
//                creat_user.lastNameTextField.getText().trim(),
//                MyFnFSettings.LOGIN_USER_ID,
//                creat_user.countryComboBox.getSelectedItem().toString(),
//                creat_user.emailTextField.getText().trim(),
//                mobile,
//                //creat_user.mobileNoTextField.getText().trim(), 
//                MyFnFSettings.LOGIN_USER_PASSWORD,
//                "add");
//        if (new_user_json.length() > 0) {
//            //System.out.println(new_user_json);
//            Gson jsn;
//            jsn = new GsonBuilder().serializeNulls().create();
//            MyFnFSettings.userProfile = jsn.fromJson(new_user_json, UserProfile.class);
//            if (MyFnFSettings.userProfile.getSuccess().equals(true)) {
//                MyFnFSettings.isAuthenticated = true;
//                MyFnFSettings.registrationStop = false;
//                ResetUserProfileFriendList.refesh_user_profile();
//
////            String user_json = GetResponseJsons.get_login_json(creat_user.userIDTextField.getText().trim(), creat_user.passwordTextField.getText());
////            jsn = new GsonBuilder().serializeNulls().create();
////            MyFnFSettings.userProfile = jsn.fromJson(user_json, UserProfile.class);
//                Registration registration = new Registration(ua_profile, ua);
//                //  this.dispose();
//                if (!SetDynamicFriend.runningFlag) {
//                    SetDynamicFriend.runningFlag = true;
//                }
//                creat_user.setMainUserInterface();
//                this.cancel();
//            } else {
//                creat_user.save_button.setEnabled(true);
//                this.cancel();
//                creat_user.errorLabel_new.setForeground(Color.RED);
//                creat_user.errorLabel_new.setText(MyFnFSettings.userProfile.getMessage());
//            }
//
//
//        } else {
//            creat_user.save_button.setEnabled(true);
//            creat_user.errorLabel_new.setForeground(Color.RED);
//            creat_user.errorLabel_new.setText("No Network");
//        }

        //     creat_user.firstNameTextField.getText().trim(),
//                creat_user.lastNameTextField.getText().trim(),
//                MyFnFSettings.LOGIN_USER_ID,
//                creat_user.countryComboBox.getSelectedItem().toString(),
//                creat_user.emailTextField.getText().trim(),
//                mobile,
//                //creat_user.mobileNoTextField.getText().trim(), 
//                MyFnFSettings.LOGIN_USER_PASSWORD,
        JsonFields fields = new JsonFields();
        fields.setAction(SocketConstants.AUTHENTICATION);
        fields.setType(SocketConstants.SIGN_UP);
        String packet_id = SocketConstants.SIGN_UP + System.currentTimeMillis();
        fields.setPacketId(packet_id);
        fields.setMobilePhoneDialingCode(creat_user.country_prefixTextField.getText().toString());
        fields.setGender(creat_user.genderComboBox.getSelectedItem().toString());
        fields.setFirstName(creat_user.firstNameTextField.getText().trim());
        fields.setLastName(creat_user.lastNameTextField.getText().trim());
        fields.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
        fields.setCountry(creat_user.countryComboBox.getSelectedItem().toString());
        fields.setEmail(creat_user.emailTextField.getText().trim());
        fields.setMobilePhone(mobile);
        fields.setPassword(MyFnFSettings.LOGIN_USER_PASSWORD);
        
        try {
            int trying_time = 0;
            address = InetAddress.getByName(SocketConstants.SOCKET_IP);
            
            while (trying_time < 3) {
                // times++;
                String response = new Gson().toJson(fields);
                socket = new DatagramSocket();
                data = response.getBytes();
                //  System.err.println("response\n" + response);
                packet = new DatagramPacket(data, data.length, address, SocketConstants.SOCKET_PORT);
                socket.send(packet);
                data = new byte[data.length];
                packet = new DatagramPacket(data, data.length);
                socket.receive(packet);
                if (packet.getLength() > 0) {
                    //  jsonLib = new GsonBuilder().serializeNulls().create();
                    String messageReturn = new String(data, 0, packet.getLength());
                    JsonFields response_fields = HelperMethods.response_json(messageReturn);
                    if (response_fields.getSuccess()) {
                        MyFnFSettings.isAuthenticated = true;
                        MyFnFSettings.registrationStop = true;
                        //   ResetUserProfileFriendList.refesh_user_profile();
                        Registration registration = new Registration(ua_profile, ua);
                        //  this.dispose();
                        if (!SetDynamicFriend.runningFlag) {
                            SetDynamicFriend.runningFlag = true;
                        }
                        creat_user.setMainUserInterface();
                        this.cancel();
                    } else {
                        creat_user.save_button.setEnabled(true);
                        creat_user.errorLabel_new.setForeground(Color.RED);
                        creat_user.errorLabel_new.setText(response_fields.getMessage());
                    }
                    //   if(response_fields.get)
                    //    System.out.println("Message Returned : " + messageReturn);

                    socket.close();
                    break;
                } else {
                    trying_time++;
                }
                
            }
            if (trying_time > 3) {
                socket.close();
                creat_user.save_button.setEnabled(true);
                creat_user.errorLabel_new.setForeground(Color.RED);
                creat_user.errorLabel_new.setText("Network Problem");
            }
        } catch (IOException iee) {
            socket.close();
            creat_user.save_button.setEnabled(true);
            creat_user.errorLabel_new.setForeground(Color.RED);
            creat_user.errorLabel_new.setText("Network Problem");
        }
    }
}
