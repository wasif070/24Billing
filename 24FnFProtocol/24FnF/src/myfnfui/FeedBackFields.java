/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

/**
 *
 * @author faizahmed
 */
public class FeedBackFields {

    private String type;
    private Boolean success;
    private String message;
    private String action;
    private Long tempId;
    private String packetId;
    // public static FeedBackFields feadback;
    private String seq;
    private String packetIdFromServer;
  

    public String getPacketIdFromServer() {
        return packetIdFromServer;
    }

    public void setPacketIdFromServer(String packetIdFromServer) {
        this.packetIdFromServer = packetIdFromServer;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }

//    public static FeedBackFields getInstance() {
//        if (feadback == null) {
//            feadback = new FeedBackFields();
//        }
//
//        return feadback;
//    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public Long getTempId() {
        return tempId;
    }

    public void setTempId(Long tempId) {
        this.tempId = tempId;
    }

    public String getPacketId() {
        return packetId;
    }

    public void setPacketId(String packetId) {
        this.packetId = packetId;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }
}
