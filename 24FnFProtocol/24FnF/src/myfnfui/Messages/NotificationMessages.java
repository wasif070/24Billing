/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.Messages;

/**
 *
 * @author user
 */
public class NotificationMessages {
//NotificationMessages.EXIT_NOTIFICAITON
    public static final String COM_PORT_PROBLEM = "Currently unable to communicate with server";
    public static final String SIGN_OUT_NOTIFICAITON = "Are you sure you want to sign out of 24FnF?";
    public static final String EXIT_NOTIFICAITON = "Are you sure to exit?";
    public static final String CANCEL_NOTIFICAITON = "Are you sure to cancel?";
    public static final String REJECT_NOTIFICAITON = "Are you sure to reject?";
    public static final String REMOVE_NOTIFICAITON = "Are you sure to remove?";
    public static final String DELETE_NOTIFICAITON = "Are you sure to delete?";
    public static final String CALL_TERMINATION_NOTIFICAITON = "Previous call will be terminated?";
    public static final String ERROR_IN_UPLOAD_NOTIFICAITON = "Can not upload image";
    public static final String NO_LONGER_FRIEND_NOTIFICAITON = "This friend is no longer in your contacts";
    public static final String CALL_LIMIT_NOTIFICATION = "You have reached your call limit for today";
    public static final String NO_FRIEND = "No friend";
    public static final String NOT_SLECTE_FRIEND = "Friend not selected";
    public static final String INVALID_GROUP_NAME = "Need valid group name";
    public static final String OFFLINE_NOTIFICATION = "You are currently offline";
    public static final String GROUP_CREATE_SUCCESS = "Group created successfully";
    public static final String GROUP_EDIT_SUCCESS = "Group edited successfully";
    public static final String DELETE_SUCCESS = " Deleted successfully";
    // You have no friend.
}
