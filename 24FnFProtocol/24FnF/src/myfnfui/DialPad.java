/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.StaticImages;
import helpers.UiMethods;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BorderFactory;
import javax.swing.ComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicComboBoxUI;

/**
 *
 * @author faizahmed
 */
public class DialPad implements ActionListener {

    public static JButton[] buttons = new JButton[19];
    int index_star = 10;
    int index_hash = 11;
    public static int call_index = 12;
    int clear_index = 13;
    JComboBox countryComboBox = null;
    JTextField mobileNoTextField = null;
    JLabel errorLabel;
    public String[] country_list = HelperMethods.get_only_country_list();
    ComboBoxModel contry_comob_model;
    String default_mobile_text = "Enter Number";
    UiMethods extra_methods;

    public DialPad() {
        extra_methods = new UiMethods();
    }

    public void create_show_pad() {

        GUI24FnF.columnpanel.removeAll();
        GUI24FnF.columnpanel.add(createDailScreen());
        GUI24FnF.columnpanel.revalidate();
        GUI24FnF.columnpanel.repaint();
    }

    public JPanel createDailScreen() {

        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(230, 380));
        rowPanel.setBackground(Color.WHITE);
        rowPanel.setLayout(null);
        int margin_left = 50;
        int margin_top = 50;
        int gap_vertical = 2;
        int key_pad_width = 200;
        int key_pad_height = 200;
        int cancel_call_lable_height = 28;
        int country_combobox_width = 60;
        int top_bottom_margin = 4;
        // System.out.println(country_list[0]);
        errorLabel = DesignClasses.makeJLabelCenterAling_with_font_size_width_color("", margin_left, margin_top - 10, key_pad_width, 11, 2, DefaultSettings.errorLabelColor);
        rowPanel.add(errorLabel);
        margin_top = (margin_top + 25);
        countryComboBox = DesignClasses.create_ComboBox_from_array(country_list, margin_left + 1, margin_top - top_bottom_margin, country_combobox_width);
        countryComboBox.setEditable(true);
        countryComboBox.setSelectedItem(HelperMethods.get_country_code_from_contry_name("Bangladesh"));
        countryComboBox.addActionListener(this);
        countryComboBox.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, new Color(0x08, 0xAB, 0xED)));

        countryComboBox.setUI(new BasicComboBoxUI() {
            @Override
            protected JButton createArrowButton() {
                JButton btn = new JButton();
                btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/combo_arrow.png")));
                btn.setBackground(Color.white);
                btn.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, new Color(0x08, 0xAB, 0xED)));
                return btn;
            }
        });
        new CoboboxWithSearch(countryComboBox);
        rowPanel.add(countryComboBox);

        mobileNoTextField = DesignClasses.makeTextField(default_mobile_text, margin_left + country_combobox_width + 3, margin_top - top_bottom_margin, key_pad_width - (country_combobox_width + 4));
        rowPanel.add(mobileNoTextField);
        mobileNoTextField.setForeground(DefaultSettings.disable_font_color);
        mobileNoTextField.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, new Color(0x08, 0xAB, 0xED)));
        mobileNoTextField.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                extra_methods.set_reset_defalut_text(mobileNoTextField, default_mobile_text, true);
            }

            public void focusGained(FocusEvent e) {
                if (mobileNoTextField.getText().toString().equals(default_mobile_text) || mobileNoTextField.getText().toString().length() < 1) {
                    extra_methods.set_reset_defalut_text(mobileNoTextField, default_mobile_text, false);
                } else if (mobileNoTextField.getText().toString().length() < 1) {
                    extra_methods.set_reset_defalut_text(mobileNoTextField, default_mobile_text, true);
                }
            }
        });
        JLabel border_as_text_box = DesignClasses.makeJLabel_with_background_font_size_width_color(null, margin_left, margin_top - (2 * top_bottom_margin), key_pad_width, DefaultSettings.texBoxHeight + (2 * top_bottom_margin), 11, 0, DefaultSettings.text_color_normal, Color.WHITE, 2);
        rowPanel.add(border_as_text_box);
        border_as_text_box.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, new Color(0x08, 0xAB, 0xED)));
        JPanel addContactPanel = new JPanel(new GridLayout(4, 3, 2, 3));
        addContactPanel.setBounds(margin_left, margin_top = margin_top + 30, key_pad_width, key_pad_height);
        addContactPanel.setBackground(Color.WHITE);
        buttons[0] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[0], StaticImages.DIAL_PAD_IMAGE_HOVER[0]);
        buttons[0].addActionListener(this);

        buttons[1] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[1], StaticImages.DIAL_PAD_IMAGE_HOVER[1]);
        buttons[1].addActionListener(this);

        buttons[2] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[2], StaticImages.DIAL_PAD_IMAGE_HOVER[2]);
        buttons[2].addActionListener(this);

        buttons[3] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[3], StaticImages.DIAL_PAD_IMAGE_HOVER[3]);
        buttons[3].addActionListener(this);

        buttons[4] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[4], StaticImages.DIAL_PAD_IMAGE_HOVER[4]);
        buttons[4].addActionListener(this);

        buttons[5] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[5], StaticImages.DIAL_PAD_IMAGE_HOVER[5]);
        buttons[5].addActionListener(this);

        buttons[6] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[6], StaticImages.DIAL_PAD_IMAGE_HOVER[6]);
        buttons[6].addActionListener(this);

        buttons[7] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[7], StaticImages.DIAL_PAD_IMAGE_HOVER[7]);
        buttons[7].addActionListener(this);

        buttons[8] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[8], StaticImages.DIAL_PAD_IMAGE_HOVER[8]);
        buttons[8].addActionListener(this);

        buttons[9] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[9], StaticImages.DIAL_PAD_IMAGE_HOVER[9]);
        buttons[9].addActionListener(this);

        buttons[index_star] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[index_star], StaticImages.DIAL_PAD_IMAGE_HOVER[index_star]);
        buttons[index_star].addActionListener(this);

        buttons[index_hash] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[index_hash], StaticImages.DIAL_PAD_IMAGE_HOVER[index_hash]);
        buttons[index_hash].addActionListener(this);

        buttons[call_index] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[call_index], StaticImages.DIAL_PAD_IMAGE_HOVER[call_index]);
        buttons[call_index].addActionListener(this);

        if (!MyFnFSettings.isRegisterred) {
            buttons[call_index].setEnabled(false);
        }

        buttons[clear_index] = DesignClasses.create_image_button_with_out_bounds(StaticImages.DIAL_PAD_IMAGE[clear_index], StaticImages.DIAL_PAD_IMAGE_HOVER[clear_index]);
        buttons[clear_index].addActionListener(this);

        addContactPanel.add(buttons[1]);
        addContactPanel.add(buttons[2]);
        addContactPanel.add(buttons[3]);

        addContactPanel.add(buttons[4]);
        addContactPanel.add(buttons[5]);
        addContactPanel.add(buttons[6]);

        addContactPanel.add(buttons[7]);
        addContactPanel.add(buttons[8]);
        addContactPanel.add(buttons[9]);

        addContactPanel.add(buttons[index_star]);
        addContactPanel.add(buttons[0]);
        addContactPanel.add(buttons[index_hash]);


        rowPanel.add(addContactPanel);
        JPanel call_erase_buttons = new JPanel(new GridLayout(1, 2, gap_vertical, 0));
        call_erase_buttons.setBounds(margin_left, margin_top = margin_top + key_pad_height + 3, key_pad_width, cancel_call_lable_height);
        call_erase_buttons.setBackground(Color.WHITE);
        call_erase_buttons.add(buttons[call_index]);

        call_erase_buttons.add(buttons[clear_index]);
        rowPanel.add(call_erase_buttons);

        return rowPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event) {

        if (event.getSource() == countryComboBox) {
            String c_code = HelperMethods.get_country_code_from_contry_name(countryComboBox.getSelectedItem().toString());
            countryComboBox.setSelectedItem(c_code);

        }
        if (event.getSource() == buttons[call_index]) {
            int success = 0;
            String msg = "";
            buttons[call_index].setEnabled(false);
            //   System.out.println(countryComboBox.getSelectedItem().toString());
            // String country_code = HelperMethods.get_country_code_from_contry_name(countryComboBox.getSelectedItem().toString());
            String country_code = countryComboBox.getSelectedItem().toString();
            if (country_code.length() > 2 && mobileNoTextField.getText().length() > 3) {

                if (mobileNoTextField.getText().contains("[a-zA-Z]") == true) {
                    success = 0;
                } else if (HelperMethods.check_only_digit(mobileNoTextField.getText().toString()).length() < 1) {
                    success = 1;
                } else if (HelperMethods.check_string_contains_substring(mobileNoTextField.getText().toString(), "*")) {
                    success = 1;
                } else if (HelperMethods.check_string_contains_substring(mobileNoTextField.getText().toString(), "#")) {
                    success = 1;
                } else {
                    msg = "Invalid mobile number";
                }
            } else {
                if (mobileNoTextField.getText().length() < 1) {
                    msg = "Please enter valid mobile number";

                } else {
                    msg = "Invalid mobile number";
                }
            }
            if (success == 1) {

                errorLabel.setText("");
                String mobile_number = HelperMethods.bd_mobile_number(country_code, mobileNoTextField.getText().toString());
                mobileNoTextField.setText(mobile_number);
                extra_methods.call_calling_interface(SingleUserProfile.call_to_mobile, country_code + mobile_number);

            } else {
                buttons[call_index].setEnabled(true);
                errorLabel.setText(msg);
            }
        } else if (event.getSource() == buttons[clear_index]) {

            String input = mobileNoTextField.getText();
            try {
                if (mobileNoTextField.getText().toString().equals(default_mobile_text)) {
                    extra_methods.set_reset_defalut_text(mobileNoTextField, default_mobile_text, false);
                }
                String selected_text = mobileNoTextField.getSelectedText();
                if (selected_text != null) {
                    extra_methods.clear_selected_text(mobileNoTextField);
                } else {
                    int selectedPoint = mobileNoTextField.getSelectionStart();
                    if (selectedPoint == input.length() && input.length() > 0) {
                        String part = input.substring(0, selectedPoint - 1);
                        mobileNoTextField.setText(part);
                        mobileNoTextField.requestFocus();
                        mobileNoTextField.setSelectionStart(selectedPoint);
                    } else {
                        String part1 = input.substring(0, selectedPoint - 1);
                        String part2 = input.substring(selectedPoint);
                        mobileNoTextField.setText(part1 + part2);
                        mobileNoTextField.requestFocus();
                        mobileNoTextField.setSelectionStart(selectedPoint - 1);
                        mobileNoTextField.setSelectionEnd(selectedPoint - 1);
                    }
                }

            } catch (Exception ex) {
            }
        } else if (event.getSource() == buttons[index_star]) {
            add_string_into_text_box_not_number("*");
        } else if (event.getSource() == buttons[index_hash]) {
            add_string_into_text_box_not_number("#");
        } else {
            for (int i = 0; i < 10; i++) {
                if (event.getSource() == buttons[i]) {
                    if (mobileNoTextField.getText().toString().equals(default_mobile_text)) {
                        extra_methods.set_reset_defalut_text(mobileNoTextField, default_mobile_text, false);
                    }
                    extra_methods.clear_selected_text(mobileNoTextField);
                    add_string_into_text_box(i);
                }
            }
        }
    }

    public void add_string_into_text_box(int i) {

        mobileNoTextField.setText(mobileNoTextField.getText() + i);
        mobileNoTextField.requestFocus();
    }

    public void add_string_into_text_box_not_number(String i) {
        mobileNoTextField.setText(mobileNoTextField.getText() + i);
        mobileNoTextField.requestFocus();
    }
}
