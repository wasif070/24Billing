package myfnfui;
//import com.google.gson.Gson;
import myfnfui.usedThreads.SetDynamicFriend;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.GetResponseJsons;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticFields;
import helpers.StaticImages;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.Properties;
import java.util.Vector;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import local.ua.UserAgent;
import local.ua.UserAgentListener;
import local.ua.UserAgentProfile;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.provider.SipProvider;
import com.google.gson.Gson;
import helpers.HelperMethods;
import helpers.InternetConnection;
import imageDownload.DownLoaderHelps;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import myfnfui.Messages.NotificationMessages;
import myfnfui.groups.GroupConstants;
import myfnfui.headers.HeaderContainer;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.packetsInfo.PackedDistributor;
import myfnfui.usedThreads.StoreAndNotifyTreads;
import org.omg.CORBA.portable.UnknownException;
//import java.j.json.JSONObject;

/**
 *
 * @author FaizAhmed
 */
public class Main extends MainFrame implements PackedDistributor, ActionListener, KeyListener, FocusListener, ItemListener, MouseListener, MouseMotionListener, UserAgentListener {

//    IncomingGraphicalUA single_user;
    public static Main objMain;
    private boolean stopElapsedTimeCounting = false;
    //   private boolean stopElapsedTimeCountingForServer = false;
    int trying_time = 8;
    DatagramSocket socket;
    DatagramPacket packet;
    PackageActions pak_action;
    //  IpAddress address; //InetAddress.getByName(SocketConstants.SOCKET_IP);
    byte[] data;
    public Gson jsonLib;
    String checking_network = "Please wait";
    String checking_auth_server = "Authenticating";
    String loging_packedId;
    public Boolean got_response = false;
    boolean ver_msg_show = false;
//    public void setStopElapsedTimeCountingForServer(boolean stopElapsedTimeCountingForServer) {
//        this.stopElapsedTimeCountingForServer = stopElapsedTimeCountingForServer;
//    }
    int counter = 0;
    int flag = 1;
    int cancel_flag = 0;
    int login_flag = 0;
    int login_status = 0;
    private String return_string;
    StoreAndNotifyTreads storeAndNotify = new StoreAndNotifyTreads();
    DownLoaderHelps dHelp = new DownLoaderHelps();

    public String getReturn_string() {
        return return_string;
    }

    public void setReturn_string(String return_string) {
        this.return_string = return_string;
    }

    public void setStopElapsedTimeCounting(boolean stopElapsedTimeCounting) {
        this.stopElapsedTimeCounting = stopElapsedTimeCounting;
    }

    public static Main getObjMain() {
        return objMain;
    }
//    private String return_string() {
//    }
//------------------------------------------ 
    // public static Main loginPanel;
    JLabel newAccount;
    JPanel loginContainer;
    JLabel userIDTextLabel;
    JLabel passTextLabel;
    JTextField userIDTextField;
    JPasswordField passTextField;
    JCheckBox saveCheckBox;
    JCheckBox saveBox;
    JCheckBox autoStart;
    JCheckBox signinAutomatically;
    JButton login;
    JButton cancel;
    int saveInfo = ItemEvent.DESELECTED;
    Point start_drag;
    Point start_loc;
    int errorType = 0;
    JLabel errorLabel;
    // Gson jsonLib;
    JButton new_u;
    JLabel logo_label;
    protected static UserAgent ua;
    protected UserAgentProfile ua_profile;
    protected SipProvider sip_provider;
    public static boolean first_time_login = false;
    private boolean socketTester = false;
    private boolean socketTester_stop = false;
    private int socket_counter = 0;
    private boolean check_and_send = false;
    /*
     * changed
     */
    java.util.Timer authTimer;
    java.util.Timer regTimer;

    public static void setUserAgent(UserAgent param_ua) {
        ua = param_ua;
    }

    public static UserAgent getUserAgent() {
        return ua;
    }

    public Main(SipProvider sip_provider, UserAgentProfile ua_profile) {
        super();
        objMain = this;
        pak_action = new PackageActions();

//        try {
//            address = IpAddress.getByName(SocketConstants.SOCKET_IP);
//        } catch (IOException ex) {
//            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
//        }
        // }
        this.sip_provider = sip_provider;
        this.ua_profile = ua_profile;
        ua = new UserAgent(sip_provider, ua_profile, this);
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT, DefaultSettings.DEFAULT_LOCATION_TOP);
        int loginpanelstart = 240;
        int labelLeft = 20;
        int labelText = 90;
        Color label_color = Color.WHITE;
        int label_font_width = 1;
        int label_font_size = 11;
        JLabel brand_image = DesignClasses.create_image_label(60, 85, 180, 160, StaticImages.IMAGE_LOGO_BIG);
        main_container.add(brand_image);
        //  errorLabel = DesignClasses.makeJLabel("", labelText, loginpanelstart, 200, DefaultSettings.errorLabelColor);
        //       errorLabel = DesignClasses.makeJLabel_with_text_color("", labelText, loginpanelstart + 5, 180, 11, 1, DefaultSettings.errorLabelColor);
        errorLabel = DesignClasses.makeJLabel_with_background_font_size_width_color(" ", 25, loginpanelstart + 5, 220, 20, label_font_size, label_font_width, DefaultSettings.errorLabelColor, label_color, 0);
        userIDTextLabel = DesignClasses.make_label_for_field_text(StaticFields.MY_FNF_NAME_TEXT, labelLeft, loginpanelstart + 30, 70);
        //userIDTextLabel.setHorizontalAlignment(JLabel.RIGHT);
        userIDTextField = DesignClasses.makeTextField("", labelText, loginpanelstart + 30, DefaultSettings.textBoxWidth);
        userIDTextField.addFocusListener(this);
        userIDTextField.addKeyListener(this);
        passTextLabel = DesignClasses.make_label_for_field_text(StaticFields.PASSWORD_TEXT, labelLeft, loginpanelstart + 60, 70);
        //  passTextLabel.setHorizontalAlignment(JLabel.RIGHT);
        passTextField = DesignClasses.makeJPasswordField("", labelText, loginpanelstart + 60);
        passTextField.addFocusListener(this);
        passTextField.addKeyListener(this);

        login = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Sign In", 11, 1, labelText, loginpanelstart + 93, 74, 22);
        login.addActionListener(this);
        login.addKeyListener(this);
        cancel = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 11, 1, labelText + 78, loginpanelstart + 93, 74, 22);
        // new_u = DesignClasses.create_button_with_image_fixed_location(StaticImages.IMAGE_BLUE_BAR, StaticImages.IMAGE_BLUE_BAR, "Create Account", 2, DefaultSettings.DEFAULT_HEIGHT - 47, DefaultSettings.DEFAULT_WIDTH - 4, 45);

        saveBox = new JCheckBox("Remember Password");
        saveBox.setBounds(labelText - 4, loginpanelstart + 120, 150, 20);
        saveBox.setBackground(null);
        saveBox.addItemListener(this);
        saveBox.addKeyListener(this);
        saveBox.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));

        signinAutomatically = new JCheckBox("Sign in when 24FnF start");
        signinAutomatically.setBounds(labelText - 4, loginpanelstart + 140, 210, 20);
        signinAutomatically.setBackground(null);
        signinAutomatically.addItemListener(this);
        signinAutomatically.addKeyListener(this);
        signinAutomatically.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));

        autoStart = new JCheckBox("Start 24FnF when windows start");
        autoStart.setBounds(labelText - 4, loginpanelstart + 160, 210, 20);
        autoStart.setBackground(null);
        autoStart.addItemListener(this);
        autoStart.addKeyListener(this);
        autoStart.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));

        new_u = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_5, StaticImages.IMAGE_Scale_5_h, "Create a 24FnF Account", 12, 1, 2, DefaultSettings.DEFAULT_HEIGHT - 46, DefaultSettings.DEFAULT_WIDTH - 4, 43);

        main_container.add(saveBox);
        main_container.add(signinAutomatically);
        main_container.add(autoStart);
        main_container.add(login, BorderLayout.CENTER);
        cancel.addActionListener(this);

        main_container.add(cancel); //Create a 24FnF Account
        main_container.add(errorLabel);
        main_container.add(userIDTextLabel);
        main_container.add(userIDTextField);
        main_container.add(passTextLabel);
        main_container.add(passTextField);
        new_u.addActionListener(this);
        main_container.add(new_u);
        first_time_login = ua_profile.first_time_login;
        set_socket_port();
        addWindowListener(new WindowAdapter() {
            public void windowOpened(WindowEvent e) {
                readLoginInfo();
                if (signinAutomatically.isSelected() && first_time_login) {
                    login.doClick();
                }
                userIDTextField.requestFocus();
            }
        });
    }

    public void setCreateUserPanel() {
        this.dispose();
        MyFnFSettings.userProfile = null;
        CreateNewAccount crt = new CreateNewAccount(sip_provider, ua_profile, ua);
        crt.setVisible(true);

    }

    public void readLoginInfo() {
        try {
            InputStream input = null;
            File file = new File(MyFnFSettings.LOGIN_INFO_FILE);
            if (file.exists()) {
                input = new FileInputStream(file);
                Properties fileProp = new Properties();
                fileProp.load(input);
                input.close();

                if (fileProp.containsKey(StaticFields.SAVED_USER_NAME)) {
                    String accStr = fileProp.getProperty(StaticFields.SAVED_USER_NAME);
                    if (accStr != null && accStr.trim().length() > 0) {
                        userIDTextField.setText(accStr);
                    }
                }
                if (fileProp.containsKey(StaticFields.SAVED_PASSWORD)) {
                    String pass = fileProp.getProperty(StaticFields.SAVED_PASSWORD);
                    if (pass != null && pass.trim().length() > 0) {
                        String newPass = "";
                        for (int i = 0; i < pass.length(); i++) {
                            int val = (int) pass.charAt(i);
                            if (val < 2 || val > 126) {
                                newPass += pass.charAt(i);
                            } else if (val % 2 == 0) {
                                newPass += (char) (pass.charAt(i) - 1);
                            } else {
                                newPass += (char) (pass.charAt(i) + 1);
                            }
                        }
                        passTextField.setText(newPass);
                        saveBox.setSelected(true);
                    }
                }
                if (fileProp.containsKey(StaticFields.SIGN_IN_AUTOMATICALLY)) {
                    String sia = fileProp.getProperty(StaticFields.SIGN_IN_AUTOMATICALLY);
                    try {
                        if (Integer.parseInt(sia) == 1) {
                            signinAutomatically.setSelected(true);
                        }
                    } catch (Exception ex) {
                        signinAutomatically.setSelected(false);
                    }
                }
                if (fileProp.containsKey(StaticFields.AUTO_START)) {
                    String as = fileProp.getProperty(StaticFields.AUTO_START);
                    try {
                        if (Integer.parseInt(as) == 1) {
                            autoStart.setSelected(true);
                        }
                    } catch (Exception ex) {
                        autoStart.setSelected(false);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    public void saveLoginInfo() {
        try {
            try {
                File loginInfoFile = new File(MyFnFSettings.LOGIN_INFO_FILE);
                if (loginInfoFile.exists()) {
                    loginInfoFile.delete();
                }
            } catch (Exception ex) {
            }

            BufferedWriter outFile = new BufferedWriter(new FileWriter(MyFnFSettings.LOGIN_INFO_FILE));
            outFile.write(StaticFields.SAVED_USER_NAME + "=" + userIDTextField.getText().trim() + "\n");

            if (saveBox.isSelected()) {
                String pass = String.valueOf(passTextField.getPassword());
                String newPass = "";
                if (pass != null && pass.length() > 0) {
                    for (int i = 0; i < pass.length(); i++) {
                        int val = (int) pass.charAt(i);
                        if (val < 2 || val > 126) {
                            newPass += pass.charAt(i);
                        } else if (val % 2 == 0) {
                            newPass += (char) (pass.charAt(i) - 1);
                        } else {
                            newPass += (char) (pass.charAt(i) + 1);
                        }
                    }
                }
                outFile.write(StaticFields.SAVED_PASSWORD + "=" + newPass + "\n");
            }

            if (signinAutomatically.isSelected()) {
                outFile.write(StaticFields.SIGN_IN_AUTOMATICALLY + "=1" + "\n");
            }

            if (autoStart.isSelected()) {
                outFile.write(StaticFields.AUTO_START + "=1" + "\n");
            }

            outFile.close();
        } catch (Exception ex) {
        }
    }

    public void set_socket_port() {
        socketTester = false;
        socketTester_stop = false;
        socket_counter = 0;
        final ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask_for_set_socket_port = new Runnable() {
            public void run() {
                int com_port = 0;
                if (socketTester_stop) {
                    executor.shutdown();

                }
                String port_string = GetResponseJsons.getApiHost("test", "***");
                if (HelperMethods.check_string_contains_substring(port_string, "true")) {
                    com_port = GetResponseJsons.getComPort(port_string);
                }
                if (com_port > 0) {
                    SocketConstants.SOCKET_PORT = com_port;
                    socketTester_stop = true;

                }
                if (socket_counter == 3) {
                    socketTester_stop = true;
                }
                socket_counter++;
            }
        };

        executor.scheduleAtFixedRate(periodicTask_for_set_socket_port,
                0, 1, TimeUnit.SECONDS);

    }

    public void actionPerformed(ActionEvent event) {
        actionSource = event.getSource();
        if (actionSource == button_window_close) {
//            MyFnFSettings.registrationStop = true;
            this.dispose();
            System.exit(0);
        } else if (actionSource == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        } else if (actionSource == cancel) {
            cancel_flag = 1;
            setStopElapsedTimeCounting(true);
            errorLabel.setText(" ");
            //   login.setEnabled(true);
            set_reset_login_new_button(true);

            MyFnFSettings.isAuthenticated = false;
            MyFnFSettings.isRegisterred = false;
            if (ua_profile.do_unregister && login_flag == 1) // unregisters the contact URL
            {
                ua.printLog("UNREGISTER the contact URL");
                ua.unregister();
                login_flag = 0;
                if (MyFnFSettings.isRegisterred) {
                    MyFnFSettings.isRegisterred = false;

                }
            }


            if (userIDTextField.getText().length() > 0) {
//                stopElapsedTimeCountingForServer = true;
                stopElapsedTimeCounting = true;
                String accept_success = GetResponseJsons.logout_user(MyFnFSettings.LOGIN_USER_ID);
                //System.out.println(accept_success);

            }
        } else if (actionSource == login) {
            check_and_send = false;
            if (MyFnFSettings.userProfile != null) {
                MyFnFSettings.userProfile = null;
            }
            MyFnFSettings.registrationStop = false;
            SetDynamicFriend.repaint_friendlist = true;
            flag = 1;
            cancel_flag = 0;
            counter = 0;
            got_response = false;
            setStopElapsedTimeCounting(false);
            //  setStopElapsedTimeCountingForServer(false);
            if (!login.isEnabled()) {
                return;
            }
            if (userIDTextField.getText().trim().length() < 1) {
                errorType = 1;
                //   errorLabel.setForeground(Color.RED);
                errorLabel.setText("User name required");
                userIDTextField.requestFocus();
            } else if (passTextField.getText().trim().length() < 1) {
                errorType = 2;
                errorLabel.setForeground(Color.RED);
                errorLabel.setText("Password Required");
                passTextField.requestFocus();
            } else {
                errorLabel.setForeground(DefaultSettings.text_color4);
//                errorLabel.setText("Authenticating....");
                errorLabel.setText(checking_network + "...     ");
                // login.setEnabled(false);
                set_reset_login_new_button(false);
                MyFnFSettings.LOGIN_USER_ID = userIDTextField.getText().trim();
                MyFnFSettings.LOGIN_USER_PASSWORD = passTextField.getText();
                ElapsedTimeChecker();

            }
        } else if (actionSource == new_u) {
            setCreateUserPanel();

        }
    }

    public void showLogin_panel() {
        MyFnFSettings.LOGIN_USER_ID = "";
        MyFnFSettings.LOGIN_USER_PASSWORD = "";
        MyFnFSettings.userProfile = null;
        this.setVisible(true);
    }

    @Override
    public void keyTyped(KeyEvent event) {
        actionSource = event.getSource();
        if (errorType == 1 && actionSource == userIDTextField) {
            errorLabel.setText("");
        } else if (errorType == 2 && actionSource == passTextField) {
            errorLabel.setText("");
        }
    }

    @Override
    public void keyPressed(KeyEvent event) {
        actionSource = event.getSource();
        int key = (int) event.getKeyChar();
        if (key == KeyEvent.VK_ENTER) {
            if (!login.isEnabled()) {
                errorLabel.setText(" ");
                // login.setEnabled(true);
                set_reset_login_new_button(true);
            } else {
                login.doClick();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent event) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void focusGained(FocusEvent e) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void focusLost(FocusEvent event) {
        actionSource = event.getSource();
        if ((signinAutomatically.isSelected() || saveBox.isSelected()) && (actionSource == userIDTextField || actionSource == passTextField)) {
            saveLoginInfo();
        } else if (actionSource == userIDTextField) {
            saveLoginInfo();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent event) {
        if (event.getSource() == signinAutomatically && signinAutomatically.isSelected()) {
            saveBox.setSelected(true);
        }

        if (event.getSource() == saveBox && !saveBox.isSelected()) {
            signinAutomatically.setSelected(false);
        }

        if (event.getSource() == saveBox || event.getSource() == signinAutomatically || event.getSource() == autoStart) {
            saveLoginInfo();
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mousePressed(MouseEvent event) {
        this.start_drag = this.getScreenLocation(event);
        this.start_loc = this.getFrame(main_container).getLocation();
    }

    public static JFrame getFrame(Container target) {
        if (target instanceof JFrame) {
            return (JFrame) target;
        }
        return getFrame(target.getParent());
    }

    Point getScreenLocation(MouseEvent event) {
        Point cursor = event.getPoint();
        Point target_location = main_container.getLocationOnScreen();
        return new Point((int) (target_location.getX() + cursor.getX()),
                (int) (target_location.getY() + cursor.getY()));
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        Point current = this.getScreenLocation(e);
        Point offset = new Point((int) current.getX() - (int) start_drag.getX(),
                (int) current.getY() - (int) start_drag.getY());
        JFrame frame = this.getFrame(main_container);
        Point new_location = new Point(
                (int) (this.start_loc.getX() + offset.getX()), (int) (this.start_loc.getY() + offset.getY()));
        frame.setLocation(new_location);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaRegistrationSucceeded(UserAgent ua, String result) {
//        MyFnFSettings.isRegisterred = true;
    }

    @Override
    public void onUaRegistrationFailed(UserAgent ua, String result) {
        MyFnFSettings.isRegisterred = false;
    }

    @Override
    public void onUaIncomingCall(UserAgent ua, NameAddress callee, NameAddress caller, Vector media_descs) {
//        SipProvider instanceSipProvider = GraphicalUA.getSipProvider();
//        String[] temp = caller.getAddress().toString().split("@");
//        String[] var = temp[0].split(":");
//
//        UserAgentProfile single_p = Helper_methods.get_all_for_a_profile(var[1]);
//        objMain = this;
//        single_user = new IncomingGraphicalUA(instanceSipProvider, single_p);
//        single_user.setVisible(true);
    }

    @Override
    public void onUaCallCancelled(UserAgent ua) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallProgress(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallRinging(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallAccepted(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallTransferred(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallFailed(UserAgent ua, String reason) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallClosed(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaMediaSessionStarted(UserAgent ua, String type, String codec) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaMediaSessionStopped(UserAgent ua, String type) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    public void try_to_registered() {

        try {
            counter++;
            if (counter > trying_time) {
                setStopElapsedTimeCounting(true);
                set_reset_login_new_button(true);
                errorLabel.setForeground(Color.RED);
                errorLabel.setText("Please check your network .");
            }

            if (!got_response) {
                errorLabel.setForeground(DefaultSettings.text_color4);
                switch (counter) {
                    case 2:
                        errorLabel.setText(checking_network + "....      ");
                        break;
                    case 3:
                        errorLabel.setText(checking_network + ".....     ");
                        break;
                    case 4:
                        errorLabel.setText(checking_network + "......    ");
                        break;
                    case 5:
                        errorLabel.setText(checking_network + ".......   ");
                        break;
                    case 6:
                        errorLabel.setText(checking_network + "........  ");
                        break;
                    case 7:
                        errorLabel.setText(checking_network + "......... ");
                        break;
                    case 8:
                        errorLabel.setText(checking_network + "..........");
                        break;
                }
            }
            //    System.out.println("counter+++++++++++++++" + counter);
            if (!check_and_send && cancel_flag == 0) {
                if (InternetConnection.isInternetReachable()) {
                    set_reset_login_new_button(false);
                    //login.setEnabled(false);
                    MyFnFSettings.LOGIN_USER_ID = userIDTextField.getText().trim();
                    MyFnFSettings.LOGIN_USER_PASSWORD = passTextField.getText();
                    int com_port = 0;
                    if (SocketConstants.SOCKET_PORT <= 0) {
                        String port_string = GetResponseJsons.getApiHost(MyFnFSettings.LOGIN_USER_ID, MyFnFSettings.LOGIN_USER_PASSWORD);
                        if (HelperMethods.check_string_contains_substring(port_string, "true")) {
                            com_port = GetResponseJsons.getComPort(port_string);
                        }
                    } else {
                        com_port = SocketConstants.SOCKET_PORT;
                    }

                    if (com_port != 0) {
                        //  System.out.println(port_string);
                        SocketConstants.SOCKET_PORT = com_port;
                        JsonFields singInpacked = new JsonFields();
                        singInpacked.setAction(SocketConstants.AUTHENTICATION);
                        singInpacked.setType(SocketConstants.SIGN_IN);
                        loging_packedId = PackageActions.create_packed_id_for(MyFnFSettings.LOGIN_USER_ID);
                        singInpacked.setPacketId(loging_packedId);
                        singInpacked.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
                        singInpacked.setPassword(MyFnFSettings.LOGIN_USER_PASSWORD);
                        singInpacked.setVersion(MyFnFSettings.Version_24FnF_PC);
                        singInpacked.setDevice(MyFnFSettings.DEVICE);
                        // String response3 = new Gson().toJson(singInpacked);
                        pak_action.send_And_Store_Packed_As_String(singInpacked, loging_packedId);
                        check_and_send = true;
                    } else {
                        errorLabel.setText("");
                        setStopElapsedTimeCounting(true);
                        //    stopElapsedTimeCountingForServer = true;
                        HelperMethods.create_confrim_panel_no_cancel("Communication Port Problem", 50);
                    }
                    //     setStopElapsedTimeCounting(true);
                }
            }

            if (cancel_flag == 0 && check_and_send) {
                if (MyFnFSettings.userProfile != null) {
                    if (!HeaderContainer.getInstance().getHeaders().isEmpty()) {
                        setStopElapsedTimeCounting(true);
                        if (cancel_flag == 0) {
                            JsonChecker();
                        }
                    }
                }
            } else {
                set_reset_login_new_button(true);
            }

        } catch (UnknownException ex) {
            errorLabel.setText("Network Problem !");
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

//---------------------------------------
    public void ElapsedTimeChecker() {

        final ScheduledExecutorService executor =
                Executors.newSingleThreadScheduledExecutor();
        Runnable periodicTask2 = new Runnable() {
            public void run() {
                try_to_registered();
                if (stopElapsedTimeCounting) {
                    try {
                        executor.shutdown();
                    } catch (Exception e) {
                    }
                }
            }
        };

        executor.scheduleAtFixedRate(periodicTask2,
                0, 1, TimeUnit.SECONDS);

    }

    public void JsonChecker() {

        if (MyFnFSettings.userProfile != null) {
            MyFnFSettings.isAuthenticated = true;
            login_flag = 1;

            Registration registration = new Registration(ua_profile, ua);
            this.dispose();
            if (!SetDynamicFriend.runningFlag) {
                SetDynamicFriend.runningFlag = true;
            }
            // Main.objMain = null;
            GUI24FnF mainuserUi = new GUI24FnF(sip_provider, ua_profile, ua);
            mainuserUi.setVisible(true);
            //  }
        } else {
            errorLabel.setForeground(Color.RED);
            // login.setEnabled(true);
            set_reset_login_new_button(true);
            errorLabel.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 11));
            errorLabel.setText("Invalid 24FnF id or password.");
        }

    }

    public void counterValueCheck() {

        counter++;

        switch (counter) {
            case 2:
                errorLabel.setText(checking_auth_server + "..     ");
                break;
            case 4:
                errorLabel.setText(checking_auth_server + "...     ");
                break;
            case 6:
                errorLabel.setText(checking_auth_server + "....    ");
                break;
            case 8:
                errorLabel.setText(checking_auth_server + ".....   ");
                break;
            case 10:
                errorLabel.setText(checking_auth_server + "......  ");
                break;
            case 12:
                errorLabel.setText(checking_auth_server + "....... ");
                break;
            case 14:
                errorLabel.setText(checking_auth_server + "........");
                break;
        }

        if (counter > trying_time) {
//            setStopElapsedTimeCountingForServer(true);
            errorLabel.setText("Server Not Responding");

        }
//        System.err.println("counter ==> " + counter);
    }

    public void set_reset_login_new_button(Boolean status) {
        login.setEnabled(status);
        new_u.setEnabled(status);
        if (status == false) {
            new_u.setDisabledIcon(DesignClasses.return_image(StaticImages.IMAGE_Scale_5));
        }

    }

    @Override
    public void onReceivedData(JsonFields js) {
        //    System.out.println("getJson :" + getJsonClass.getFirstName());
        if (js.getType().equals(SocketConstants.SIGN_IN)) {
            got_response = true;
            if (js.getSuccess() == false) {
                //  System.out.println("ldsjfklsjfkl5555555555555555555555");
                setStopElapsedTimeCounting(true);
                errorLabel.setForeground(Color.RED);
                set_reset_login_new_button(true);
                errorLabel.setText(js.getMessage());
                if (MyFnFSettings.userProfile != null) {
                    MyFnFSettings.userProfile = null;
                }
            } else {

                if (js.getVersionMessage() != null && js.getVersionMessage().length() > 0 && ver_msg_show == false) {
                    ver_msg_show = true;
              //      HelperMethods.create_confrim_panel_no_cancel(js.getVersionMessage(), 20);
                    HelperMethods.show_long_notification_msg(js.getVersionMessage());
                    if (js.getDownloadMandatory() != null && js.getDownloadMandatory()) {
                        System.exit(0);
                    } else {
                    }
                }
                if (js.getProfileImage().length() > 3) {
                    String previous_image = HelperMethods.getImageName_from_url(js.getProfileImage());
                    MyFnFSettings.LOGIN_USER_PROFILE_IMAGE = previous_image;
                    File f_pre = new File(dHelp.getDestinationFolder() + previous_image);
//                    System.out.println("Profile Image folder:" + dHelp.getDestinationFolder() + previous_image);
                    if (!f_pre.exists()) {
                        if (js.getProfileImage() != null && js.getProfileImage().length() > 0) {
                            storeAndNotify.store_and_notify_image_downloader_thread(MyFnFSettings.LOGIN_USER_ID, js.getProfileImage());
                        }

                    }
                }


                if (cancel_flag == 0) {
                    String create_string = new Gson().toJson(js);
                    MyFnFSettings.LOGIN_SESSIONID = js.getSessionId();
                    HelperMethods.user_profile(create_string);
                    JsonFields fields2 = new JsonFields();
                    fields2.setAction(SocketConstants.REQUEST);
                    fields2.setType(SocketConstants.CONTACT_LIST);
                    String pakage_id = PackageActions.create_packed_id_for(SocketConstants.CONTACT_LIST);
                    fields2.setPacketId(pakage_id);
                    fields2.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                    pak_action.send_And_Store_Packed_As_String(fields2, pakage_id);

                    JsonFields fields_fetch_group = new JsonFields();
                    fields_fetch_group.setAction(SocketConstants.REQUEST);
                    fields_fetch_group.setType(GroupConstants.TYPE_groupList);
                    String pakage_id_group = PackageActions.create_packed_id_for(GroupConstants.TYPE_groupList);
                    fields_fetch_group.setPacketId(pakage_id_group);
                    fields_fetch_group.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                    pak_action.send_And_Store_Packed_As_String(fields_fetch_group, pakage_id_group);
                }

            }

        }


    }
}
