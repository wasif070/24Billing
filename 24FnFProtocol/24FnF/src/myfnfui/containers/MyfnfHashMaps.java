/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.containers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author user
 */
public class MyfnfHashMaps {

    public static MyfnfHashMaps hash_maps;

    public static MyfnfHashMaps getInstance() {
        if (hash_maps == null) {
            hash_maps = new MyfnfHashMaps();
        }
        return hash_maps;
    }
    private Map<Long, String> receiced_msg_container = new ConcurrentHashMap<Long, String>();

    public Map<Long, String> getReceiced_msg_container() {
        return receiced_msg_container;
    }

    public void setReceiced_msg_container(Map<Long, String> receiced_msg_container) {
        this.receiced_msg_container = receiced_msg_container;
    }
    private Map<String, String> profileImageUrls = new ConcurrentHashMap<String, String>();

    public Map<String, String> getProfileImageUrls() {
        return profileImageUrls;
    }

    public void setProfileImageUrls(Map<String, String> profileImageUrls) {
        this.profileImageUrls = profileImageUrls;
    }
}
