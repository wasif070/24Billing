/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import myfnfui.usedThreads.SetDynamicFriend;
import com.google.gson.Gson;
import helpers.APIParametersFiles;
import helpers.CallLogHelper;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import helpers.StaticFields;
import helpers.StaticImages;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import local.media.RtpConstants;
import local.ua.GraphicalUA;
import local.ua.UserAgentProfile;
import myfnfui.packetsInfo.PackageActions;
import org.zoolu.sip.provider.SipProvider;

/**
 *
 * @author FaizAhmed
 */
public class SingleUserProfile extends MainFrame implements ActionListener {

    String staus = "";

    public void setStaus(String staus) {
        this.staus = staus;
    }
    //---------------------------
    //  private static SipProvider sip_provider;
    //--------------------------
//-------------------------------- 24 (start) ----------------------------------------
    public static ArrayList<SingleUserProfile> listOfSingleUserObjects = new ArrayList<SingleUserProfile>();
    private UserAgentProfile profile;

    public UserAgentProfile getProfile() {
        return profile;
    }
    JPanel userImagePanel;
    JPanel userPart;
    JPanel call_container;
    Gson jsonLib;
    JPanel dialing_panel;
    JButton call_p2p;
    JButton call_mobile_button;
    JLabel fnf_lable;
    JLabel mobile_no_lable;
    JLabel errorLabel;
    public static SingleUserProfile app;
    JLabel user_Status;
    JButton remove_button_friend_request;
    JButton reject_button2;
    JButton button_friend_delete;
    JButton accept_button;
    public static final String call_to_pin = "p2p";
    public static final String call_to_mobile = "p2m";
    /*new in*/
    JPanel tabedPanel;
    JButton actionButton;
    JButton profileTab;
    int action_index = 1;
    int profiel_index = 2;
    JPanel tabConentContainer;
    LightScrollPane scrollPane;
    HelperMethods helperMethods = new HelperMethods();
    PackageActions pak_action;

    public SingleUserProfile(UserAgentProfile single_profile) {
        super();
        listOfSingleUserObjects.add(this);
        pak_action = new PackageActions();
        //     RefreshFriendList get_frind_list = new RefreshFriendList();
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT + 100, DefaultSettings.DEFAULT_LOCATION_TOP + 45);
        this.setSize(DefaultSettings.DEFAULT_WIDTH, DefaultSettings.DEFAULT_HEIGHT - 200);
        profile = single_profile;
        this.setTitle("24FnF - " + profile.getFirstName() + " " + profile.getLastName());
//        userImagePanel = new JPanel();
//        userImagePanel.setBounds(5, 51, 115, 115);
//        userImagePanel.setBackground(null);

        addWindowListener(
                new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                dispose();
                listOfSingleUserObjects.remove(this);
            }
        });

        tabedPanel = new JPanel(new GridLayout(1, 2, 1, 1));
        tabedPanel.setBounds(3, 53, DefaultSettings.DEFAULT_WIDTH - 6, 34);
        tabedPanel.setBackground(Color.WHITE);
        main_container.add(tabedPanel);


        //  actionButton = DesignClasses.create_button_with_text_color_bgColor("Action", 11, 1, 22, 22, 150, 22, Color.WHITE, DefaultSettings.topTab_background_hover);
        actionButton = DesignClasses.create_image_button_with_text_scale6("Action", 11, 1, Color.BLACK);
        DesignClasses.change_image(StaticImages.Scale_6_h, actionButton);
//        actionButton.setBackground(DefaultSettings.blue_bar_background);
//        actionButton.setForeground(Color.WHITE);
        actionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button_click_color_change(action_index);
                drawActionWindow();

            }
        });
        tabedPanel.add(actionButton);
        //profileTab = DesignClasses.create_button_with_text_color_bgColor("Profile", 11, 1, 22, 20, 150, 20, Color.WHITE, DefaultSettings.topTab_background);
        profileTab = DesignClasses.create_image_button_with_text_scale6("Profile", 11, 1, Color.BLACK);
        tabedPanel.add(profileTab);
        profileTab.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button_click_color_change(profiel_index);
                drawProfileWindow();

            }
        });
        JPanel borderlaoutpanel = new JPanel();
        scrollPane = new LightScrollPane(borderlaoutpanel);
        scrollPane.setBounds(2, 90, 298, 300);
        borderlaoutpanel.setLayout(new BorderLayout(0, 0));
        borderlaoutpanel.setBackground(Color.WHITE);
        main_container.add(scrollPane);
        tabConentContainer = new JPanel();
        borderlaoutpanel.add(tabConentContainer, BorderLayout.NORTH);
        tabConentContainer.setLayout(new GridLayout(0, 1, 0, 1));
        tabConentContainer.setBackground(DefaultSettings.friend_list_border_color);
        tabConentContainer.add(createSingleBar());
        //tabPanel=
//        userImagePanel = new JPanel();
//        userImagePanel.setBounds(5, 51, 115, 115);
//        userImagePanel.setBackground(null);
//        URL imgURL = null;
//        if (profile.getGender().equals("Male")) {
//            imgURL = getClass().getClassLoader().getResource(StaticImages.IMAGE_MALE);
//        } else {
//            imgURL = getClass().getClassLoader().getResource(StaticImages.IMAGE_FEMALE);
//        }
//        if (imgURL != null) {
//            JButton buttonContact = DesignClasses.create_contact_image_button(imgURL, profile.getFirstName() + " " + profile.getLastName());
//            userImagePanel.add(buttonContact);
//        }
//        int top_increment = 20;
//        int margin_top = 55;
//
//        main_container.add(userImagePanel);
//        userPart = new JPanel();
//        userPart.setBounds(130, 50, 165, 110);
//        userPart.setBackground(null); //#9fd8f5
//
//        userPart.setBorder(BorderFactory.createLineBorder(SystemColor.inactiveCaptionBorder));
//        JLabel dd = new JLabel(profile.getFirstName() + " " + profile.getLastName());
//        dd.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 13));
//        dd.setBounds(135, margin_top, 130, 20);
//        main_container.add(dd);
//
//        JLabel user_id = new JLabel(profile.getUserIdentity());
//        user_id.setBounds(135, margin_top = margin_top + top_increment, 130, 20);
//        user_id.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
//        main_container.add(user_id);
//
//        JLabel userCountry = new JLabel(profile.getCountry());
//        userCountry.setBounds(135, margin_top = margin_top + top_increment, 130, 20);
//        userCountry.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
//        main_container.add(userCountry);
//
//
//
//        JLabel emaillLbl = new JLabel(profile.getEmail());
//        emaillLbl.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
//        emaillLbl.setBounds(135, margin_top = margin_top + top_increment, 130, 20);
//        main_container.add(emaillLbl);
//
//        JLabel userMobile = new JLabel(profile.getMobilePhone());
//        userMobile.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
//        userMobile.setBounds(135, margin_top = margin_top + top_increment, 130, 20);
//        main_container.add(userMobile);
//
//        user_Status = new JLabel();
//        user_Status.setFont(new Font(Font.SANS_SERIF, Font.ITALIC, 12));
//        user_Status.setBounds(135, margin_top = margin_top + top_increment, 130, 20);
//
//        reject_button2 = DesignClasses.create_image_button_with_text_scale1("Reject", DefaultSettings.DEFAULT_WIDTH - 60, 175);// DesignClasses.create_reject_buttons(DefaultSettings.DEFAULT_WIDTH - 60, 175);
//        reject_button2.addActionListener(this);
//        //  final JButton accept_button = DesignClasses.create_accept_buttons(DefaultSettings.DEFAULT_WIDTH - 115, 175);
//        accept_button = DesignClasses.create_image_button_with_text_scale1("Accept", DefaultSettings.DEFAULT_WIDTH - 115, 175);// DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Accept", 9, 1, DefaultSettings.DEFAULT_WIDTH - 115, 175, 50, 25);
//        //button_friend_delete = DesignClasses.create_remove_buttons(DefaultSettings.DEFAULT_WIDTH - 60, 175);
//        button_friend_delete = DesignClasses.create_image_button_with_text_scale1("Remove", DefaultSettings.DEFAULT_WIDTH - 60, 175);//DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Remove", 9, 1, DefaultSettings.DEFAULT_WIDTH - 60, 175, 50, 25);
//        button_friend_delete.addActionListener(this);
//        final JLabel call_fnf_id = DesignClasses.makeJLabel_text_color_normal("Call to " + profile.getFirstName() + " " + profile.getLastName(), 60, 200, 200, 14);

        // call_p2p = DesignClasses.create_pin2pin_call_buttons(25, 250);
        // call_p2p = DesignClasses.create_im;
        //  call_mobile_button = DesignClasses.create_mobile_call_buttons(25, 290);
        //  fnf_lable = DesignClasses.makeJLabel_text_color_normal(profile.getUserIdentity(), 110, 248, 150, 13);
        //  mobile_no_lable = DesignClasses.makeJLabel_text_color_normal(profile.getMobilePhone(), 110, 288, 150, 13);
//
//        if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
//
//            if (profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) {
//
//                staus = "Online";
//            } else {
//                call_p2p.setEnabled(false);
//                staus = "Offline";
//                //  call_p2p.setEnabled(false);
//            }
//
//            if (!MyFnFSettings.isRegisterred) {
//                call_mobile_button.setEnabled(false);
//            }
//
//        } else if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_INCOMMING)) {
//            staus = "Incomming Request";
//        } else {
//            staus = "Request Sent";
//        }
//        //  user_Status.setText(staus);
//        main_container.add(user_Status);
//        main_container.add(userPart);
//        main_container.add(DesignClasses.create_bottom_border(10, 170, 280));
//
//        if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
//            main_container.add(button_friend_delete);
//            main_container.add(call_fnf_id);
//            main_container.add(DesignClasses.create_bottom_border(50, 230, 200));
//
//
//            call_p2p.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
////                    Call_Log_helper.sent_call_log_to_server_in_call_start(profile.getUserIdentity(), profile.getUserIdentity(), 0000);
//                    call_calling_interface(profile.getUserIdentity(), SingleUserProfile.call_to_pin);
//                }
//            });
//
//            call_mobile_button.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    /* need to send when call start */
////                    Call_Log_helper.sent_call_log_to_server_in_call_start(profile.getUserIdentity(), profile.getUserIdentity(), 0000);
////                    
////                    Call_Log_helper.sent_call_log_to_server_in_call_end(100);
//                    /*end*/
//                    call_calling_interface(profile.getUserIdentity(), SingleUserProfile.call_to_mobile);
//                }
//            });
//            main_container.add(call_p2p);
//            main_container.add(fnf_lable);
//
//            main_container.add(call_mobile_button);
//            main_container.add(mobile_no_lable);
//
//
//        } else if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_INCOMMING)) {
//            main_container.add(reject_button2);
//            jsonLib = new GsonBuilder().serializeNulls().create();
//
//            accept_button.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    //  //System.out.println("accepted");
//                    String accept_success = GetResponseJsons.accept_friend_request(MyFnFSettings.userProfile.getSessionId(), profile.getUserIdentity());
//                    if (accept_success.length() > 0) {
//                        MyFnFSettings.map_success_fail = jsonLib.fromJson(accept_success, MappingSuccessFailure.class);
//                        //  GetResponseJsons.set_success_failure(accept_success);
//                        System.out.print(accept_success);
//                        if (MyFnFSettings.map_success_fail.getSuccess().equals(true)) {
//                            System.out.print(accept_success);
//                            //   accept_button.setText("Accetped");
//                            accept_button.setEnabled(false);
//                            reject_button2.setEnabled(false);
//                        }
//                    }
//                }
//            });
//            main_container.add(accept_button);
//        } else {
//            //  remove_button_friend_request = DesignClasses.create_remove_buttons(DefaultSettings.DEFAULT_WIDTH - 60, 175);
//            remove_button_friend_request = DesignClasses.create_image_button_with_text_scale1("Remove", DefaultSettings.DEFAULT_WIDTH - 60, 175);
//            remove_button_friend_request.addActionListener(this);
//            main_container.add(remove_button_friend_request);
//        }

    }

    private void button_click_color_change(int index) {
        if (index == action_index) {

            DesignClasses.change_image(StaticImages.Scale_6_h, actionButton);
            DesignClasses.change_image(StaticImages.Scale_6, profileTab);
        }
        if (index == profiel_index) {
//            actionButton.setBackground(DefaultSettings.topTab_background);
//            profileTab.setBackground(DefaultSettings.topTab_background_hover);
            DesignClasses.change_image(StaticImages.Scale_6_h, profileTab);
            DesignClasses.change_image(StaticImages.Scale_6, actionButton);
        }
    }

    public void drawActionWindow() {
        tabConentContainer.removeAll();
        tabConentContainer.add(createSingleBar());
        tabConentContainer.revalidate();
        tabConentContainer.repaint();
    }

    public JPanel createSingleBar() {

        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(230, 300));
        rowPanel.setLayout(null);
        rowPanel.setBackground(Color.white);
        int top_height = 50;
        int border_width = 200;
        int margin_left = 30;
        int label_height_increment_factor = 50;
        int border_height_increment_factor = 40;
        int lable_WIDTH = 110;
        int margin_left_in_text = margin_left + 20;


        if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
            call_p2p = DesignClasses.create_pin2pin_call_buttons(margin_left_in_text, top_height);
            rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getUserIdentity(), margin_left + lable_WIDTH + 5, top_height, lable_WIDTH, 11, 1, DefaultSettings.text_color1));

            call_p2p.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    RtpConstants.is_ROUTE_CALL = false;
//                    CallLogHelper.sent_call_log_to_server_in_call_start(profile.getUserIdentity(), profile.getUserIdentity(), 0L);
                    call_calling_interface(profile.getUserIdentity(), SingleUserProfile.call_to_pin);
                }
            });
            if (profile.getPresence().equals(APIParametersFiles.PRESENCE_ONLINE) && MyFnFSettings.isRegisterred) {
                staus = "Online";
            } else {
                call_p2p.setEnabled(false);
                staus = "Offline";
            }

//            if (!MyFnFSettings.isRegisterred) {
//                if (call_mobile_button != null) {
//                    call_mobile_button.setEnabled(false);
//                }
//            }
            rowPanel.add(call_p2p);

            if (profile.getMobilePhone().length() > 3) {

                rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
                rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getMobilePhone(), margin_left + lable_WIDTH + 5, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
                call_mobile_button = DesignClasses.create_mobile_call_buttons(margin_left_in_text, top_height);
                if (MyFnFSettings.isRegisterred) {
                    call_mobile_button.setEnabled(true);
                } else {
                    call_mobile_button.setEnabled(false);
                }
                call_mobile_button.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        RtpConstants.is_ROUTE_CALL = true;

                        /* need to send when call start */
//                    Call_Log_helper.sent_call_log_to_server_in_call_start(profile.getUserIdentity(), profile.getUserIdentity(), 0000);
//                    
//                    Call_Log_helper.sent_call_log_to_server_in_call_end(100);
                    /*end*/
                        call_calling_interface(profile.getUserIdentity(), SingleUserProfile.call_to_mobile);
                        CallLogHelper.sent_call_log_to_server_in_call_start(profile.getUserIdentity(), profile.getMobilePhone(), 0L);
                    }
                });
                rowPanel.add(call_mobile_button);


            }

            rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
            rowPanel.add(DesignClasses.makeJLabel_with_text_color("Delete Frined", margin_left + lable_WIDTH + 5, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
            button_friend_delete = DesignClasses.create_image_button_with_text_scale1("Delete", margin_left_in_text, top_height);
            button_friend_delete.addActionListener(this);
            rowPanel.add(button_friend_delete);
            rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
        } else if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_INCOMMING)) {
//            main_container.add(reject_button2);
//            jsonLib = new GsonBuilder().serializeNulls().create();
            rowPanel.add(DesignClasses.makeJLabel_with_text_color("Incomming Request", margin_left + lable_WIDTH + 5, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
            accept_button = DesignClasses.create_image_button_with_text_scale1("Accept", margin_left_in_text, top_height);
            accept_button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    accept_button.setEnabled(false);
                    reject_button2.setEnabled(false);
                    String pak_id = PackageActions.create_packed_id_for(SocketConstants.ACCEPT_FRIEND);
                    JsonFields jFeilds = new JsonFields();
                    jFeilds.setAction(SocketConstants.UPDATE);
                    jFeilds.setType(SocketConstants.ACCEPT_FRIEND);
                    jFeilds.setUserIdentity(profile.getUserIdentity());
                    jFeilds.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                    jFeilds.setPacketId(pak_id);
                    PackageActions.sendPacketAsString(jFeilds);
                   // pak_action.sendPackageAsClass(jFeilds, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT);
                }
            });
            rowPanel.add(accept_button);

            rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
            rowPanel.add(DesignClasses.makeJLabel_with_text_color("Decline", margin_left + lable_WIDTH + 5, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
            reject_button2 = DesignClasses.create_image_button_with_text_scale1("Reject", margin_left_in_text, top_height);// DesignClasses.create_reject_buttons(DefaultSettings.DEFAULT_WIDTH - 60, 175);
            reject_button2.addActionListener(this);
            rowPanel.add(reject_button2);
            rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
        } else {
            rowPanel.add(DesignClasses.makeJLabel_with_text_color("Pending Request", margin_left + lable_WIDTH + 5, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
            button_friend_delete = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, "Remove", 9, 1, margin_left_in_text, top_height, 50, 25);
            button_friend_delete.addActionListener(this);
            rowPanel.add(button_friend_delete);
            rowPanel.add(DesignClasses.create_bottom_border(margin_left_in_text, top_height + border_height_increment_factor, border_width));
        }

//        if (profile.getFriendShipStatus().equals(APIParametersFiles.FRIENDSHIP_STATUS_ACCEPTED)) {
//            rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getUserIdentity(), margin_left_in_text, top_height, lable_WIDTH, 11, 1, DefaultSettings.text_color1));
//            call_p2p = DesignClasses.create_pin2pin_call_buttons(margin_left + lable_WIDTH + 5, top_height);
//            call_p2p.addActionListener(new ActionListener() {
//                @Override
//                public void actionPerformed(ActionEvent e) {
//                    HelperMethods.create_confrim_panel("Are you sure you want to Remove this friend?", 25);
//                    if (HelperMethods.confirm == 1) {
//                        //    PackageActions.send_remove_or_delete_request("faiz");
//                    }
//
//                }
//            });
//
//
//            if (profile.getMobilePhone().length() > 3) {
//                rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));
//                rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getMobilePhone(), margin_left_in_text, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color1));
//                call_mobile_button = DesignClasses.create_mobile_call_buttons(margin_left + lable_WIDTH + 5, top_height);
//                call_mobile_button.addActionListener(new ActionListener() {
//                    @Override
//                    public void actionPerformed(ActionEvent e) {
//                        HelperMethods.create_confrim_panel("Are you sure you want to Remove this friend?", 25);
//                        if (HelperMethods.confirm == 1) {
//                            //    PackageActions.send_remove_or_delete_request("faiz");
//                        }
//
//                    }
//                });
//                rowPanel.add(call_mobile_button);
//                rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));
//            }
//
//
//        }

        return rowPanel;
    }

    public JPanel createSingleProfilePic() {
        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(230, 300));
        rowPanel.setLayout(null);
        rowPanel.setBackground(Color.white);
        int top_height = 150;
        int border_width = 250;
        int margin_left = 20;
        int value_width = 200;
        int value_font_size = 12;
        int label_height_increment_factor = 20;
        int image_width = 120;

        int border_height_increment_factor = 20;
        int lable_WIDTH = 100;

        JLabel user_image = helperMethods.create_image_from_url(profile.getGender(), profile.getProfileImage(), margin_left, 10, image_width, image_width);
        user_image.setBorder(DefaultSettings.border_light_blue);
        rowPanel.add(user_image);

//        JLabel what_label = DesignClasses.makeJLabel_with_background_font_size_width_color(profile.getWhatisInYourMind(), margin_left + image_width + 5, 10, image_width, image_width, value_font_size, 0, Color.BLUE, Color.WHITE, 0);
//        rowPanel.add(what_label);
        LightScrollPane scrollPane = DesignClasses.create_text_area_with_scroll_panel(profile.getWhatisInYourMind(), margin_left + image_width + 8, 10, 145, 70);
        rowPanel.add(scrollPane);
        rowPanel.add(DesignClasses.makeJLabel_with_text_color("Name", margin_left, top_height, lable_WIDTH, 11, 1, DefaultSettings.text_color2));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getFirstName() + " " + profile.getLastName(), margin_left + lable_WIDTH + 5, top_height, value_width, value_font_size, 1, DefaultSettings.text_color1));
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));

        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.MY_FNF_NAME_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getUserIdentity(), margin_left + lable_WIDTH + 5, top_height, value_width, value_font_size, 1, DefaultSettings.text_color1));
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));

        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.COUNTRY_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getCountry(), margin_left + lable_WIDTH + 5, top_height, value_width, value_font_size, 1, DefaultSettings.text_color1));
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));

        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.PHONE_NO_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getMobilePhone(), margin_left + lable_WIDTH + 5, top_height, value_width, value_font_size, 1, DefaultSettings.text_color1));
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, top_height + border_height_increment_factor, border_width));

        rowPanel.add(DesignClasses.makeJLabel_with_text_color(StaticFields.GENDER_TEXT, margin_left, top_height = top_height + label_height_increment_factor, 100, 11, 1, DefaultSettings.text_color2));
        rowPanel.add(DesignClasses.makeJLabel_with_text_color(profile.getGender(), margin_left + lable_WIDTH + 5, top_height, value_width, value_font_size, 1, DefaultSettings.text_color1));
        return rowPanel;
    }

    public void drawProfileWindow() {
        tabConentContainer.removeAll();
        JLabel dd = new JLabel("00");
        dd.setBounds(2, 30, 250, 60);
        dd.setBackground(Color.red);

        tabConentContainer.add(createSingleProfilePic());
        tabConentContainer.revalidate();
        tabConentContainer.repaint();
    }

    public void call_calling_interface(String user_id, String call_type) {
        this.dispose();
        //UserAgentProfile pp = HelperMethods.get_all_for_a_profile(user_id);
        SipProvider instanceSipProvider = GraphicalUA.getSipProvider();

        if (GraphicalUA.getStaticObject() == null) {
            GraphicalUA incall = new GraphicalUA(instanceSipProvider, profile, call_type);
            incall.setVisible(true);
        } else {
            if (!GraphicalUA.getStatus().equals(GraphicalUA.UA_IDLE)) {
                HelperMethods.create_confrim_panel("Previous call will be terminated?", 60);
                if (HelperMethods.confirm == 1) {
                    GraphicalUA.getStaticObject().forceHangUp();
                    GraphicalUA incall = new GraphicalUA(instanceSipProvider, profile, call_type);
                    incall.setVisible(true);
                }
            } else {
                GraphicalUA.getStaticObject().forceHangUp();
                GraphicalUA incall = new GraphicalUA(instanceSipProvider, profile, call_type);
                incall.setVisible(true);
            }

        }


    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            this.dispose();
            listOfSingleUserObjects.remove(this);
            //System.out.println("Colsed windows of:" + profile.getUserIdentity());
        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        } else if (event.getSource() == reject_button2 || event.getSource() == button_friend_delete || event.getSource() == reject_button2) {

//            int reply = JOptionPane.showConfirmDialog(null, "Are You Sure ?", "Confirm ", JOptionPane.YES_NO_OPTION);
//            if (reply == JOptionPane.YES_OPTION) {
            HelperMethods.create_confrim_panel("Are you sure you want to remove?", 50);
            if (HelperMethods.confirm == 1) {
                PackageActions.send_remove_or_delete_request(profile.getUserIdentity());
                dispose_this_window();
                if (SetDynamicFriend.repaint_friendlist) {
                    RefreshFriendList get_frind_list = new RefreshFriendList();
                    get_frind_list.getfriend_list();
                }
//                if (GetResponseJsons.reject_delete_friend_request_final(profile.getUserIdentity())) {
//                    dispose_this_window();
//                    SetDynamicFriend.repaint_friend_list();
//                }
            }

        }
    }

    public void set_reset_call_button(Boolean dd) {
        call_p2p.setVisible(dd);
        call_mobile_button.setVisible(dd);
        fnf_lable.setVisible(dd);
        mobile_no_lable.setVisible(dd);
    }

    public void dispose_this_window() {
        this.dispose();
        this.setVisible(false);
    }
    //*********for registed to sip
}
