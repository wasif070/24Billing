/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import myfnfui.usedThreads.SetDynamicFriend;
import com.google.gson.Gson;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.GetResponseJsons;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import myfnfui.usedThreads.SearchThread;
import helpers.SocketConstants;
import helpers.StaticImages;
import helpers.UiMethods;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.net.URL;
import java.util.StringTokenizer;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import local.ua.GraphicalUA;
import local.ua.IncomingGraphicalUA;
import local.ua.UserAgent;
import local.ua.UserAgentListener;
import local.ua.UserAgentProfile;
import myfnfui.Messages.NotificationMessages;
import myfnfui.groups.GroupsWindow;
import myfnfui.notifications.PresenceNotification;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.InviteFriendContainer;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.packetsInfo.PackedDistributor;
import org.zoolu.sip.address.NameAddress;
import org.zoolu.sip.provider.SipParser;
import org.zoolu.sip.provider.SipProvider;

/**
 *
 * @author FaizAhmed
 */
public final class GUI24FnF extends MainFrame implements ActionListener, UserAgentListener, PackedDistributor {

    IncomingGraphicalUA single_user;
    private static JPanel addContactPanel;
    public static JPanel topMenubar;
    public static JPanel headerMenuContainer;
    public static JPanel menuBarContainer;
    public static JPanel columnpanel;
    public static JLabel flash_JLabel;
    Gson jsonLib;
    JPanel logout_invite_panle;
    public static JLabel login_user_name;
    //JButton logout_button;
    ProfileAddUpdate profile_update;
    UpdateCallLog call_log_paint;
    //  JButton add_friend;
    private static CreateNewAccount new_user_frame;
    protected static UserAgent ua;
    protected UserAgentProfile ua_profile;
    protected SipProvider sip_provider;
    public static JLabel online_image_top;
    //  JButton button_contact;
    JButton button_profile;
    //   JButton button_recent;
    public static JButton[] buttons_gui24fnf = new JButton[8];
    public static JComboBox status_cobmob_box;
    int contact_index = 0;
    int dail_pad_index = 1;
    public static int call_log_index = 2;
    private static int profile_index = 3;
    /**/
    public static int invite_index = 4;
    private static int group_index = 5;
    private static int logout_index = 6;
    private static int back_index = 7;
//    public static int invite_index = 4;
//    private static int back_index = 6;
//    private static int logout_index = 5;
//    private static int group_index = 7;
    InviteFrined invite_obj;
    private static LightScrollPane scrollPane;
    private static GUI24FnF objMain;
    public static JTextField searchTextField;
    RefreshFriendList get_frind_list;
    UiMethods extra_methods;
    //  private static JLabel background_in_search_box;
    public static JLabel search_image;
    //   RefreshFriendList freindListObj;
    PackageActions pak_send = new PackageActions();
    public static String default_mobile_text = "Search";
    String tab_image[] = {StaticImages.IMAGE_CONTACT, StaticImages.IMAGE_DIAL_PAD, StaticImages.IMAGE_RECENT, StaticImages.IMAGE_PROFILE, StaticImages.INVITE, StaticImages.GROUP, StaticImages.SIGNOUT};
    String tab_image_hover[] = {StaticImages.IMAGE_CONTACT_H, StaticImages.IMAGE_DIAL_PAD_H, StaticImages.IMAGE_RECENT_H, StaticImages.IMAGE_PROFILE_H, StaticImages.INVITE_H, StaticImages.GROUP_H, StaticImages.SIGNOUT_H};
//    String top_menu_image[] = {StaticImages.INVITE, StaticImages.GROUP, StaticImages.SIGNOUT};
//    String top_menu_image_h[] = {StaticImages.INVITE_H, StaticImages.GROUP_H, StaticImages.SIGNOUT_H};
//    

    public static GUI24FnF getObjMain() {
        return objMain;
    }

    public static void setObjMain(GUI24FnF objMain) {
        GUI24FnF.objMain = objMain;
    }

    public static void setUserAgent(UserAgent param_ua) {
        ua = param_ua;
    }

    public static UserAgent getUserAgent() {
        return ua;
    }

    public GUI24FnF(SipProvider sip_provider, final UserAgentProfile ua_profile, UserAgent param_ua) {

        super();
        setObjMain(this);
        get_frind_list = new RefreshFriendList();
        ua = param_ua;
        ua.setListener(this);
        this.sip_provider = sip_provider;
        this.ua_profile = ua_profile;
        extra_methods = new UiMethods();

        invite_obj = new InviteFrined();
        addWindowListener(
                new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent e) {
                HelperMethods.create_confrim_panel(NotificationMessages.EXIT_NOTIFICAITON, 20);
                if (HelperMethods.confirm == 1) {

                    GetResponseJsons.logout_user(MyFnFSettings.userProfile.getSessionId());
                    if (ua_profile.do_unregister) // unregisters the contact URL
                    {
                        ua.printLog("UNREGISTER the contact URL");
                        ua.unregister();
                    }
                    dispose();
                    System.exit(0);
                } else {
                    setVisible(true);
                }
            }
        });
        online_image_top = DesignClasses.create_image_label(10, 52, 20, 20, StaticImages.IMAGE_ONLINE);
        this.setLocation(DefaultSettings.DEFAULT_LOCATION_LEFT, DefaultSettings.DEFAULT_LOCATION_TOP);
        if (MyFnFSettings.userProfile != null) {
            int online_offline_height = 51;
            int top_location = DefaultSettings.DEFAULT_TOP_BAR;
            JLabel top_bar = DesignClasses.create_image_label(3, online_offline_height, 296, 25, StaticImages.IMAGE_MENU_BAR);
            flash_JLabel = DesignClasses.create_image_label(2, 100, 300, 500, StaticImages.LOADING_IMAGE);
            flash_JLabel.setBackground(Color.blue);
            flash_JLabel.setVisible(false);
            main_container.add(flash_JLabel);
            login_user_name = DesignClasses.makeJLabel_text_color1(MyFnFSettings.userProfile.getFirstName() + " " + MyFnFSettings.userProfile.getLastName(), 35, 48, 150, 11);
            main_container.add(online_image_top);
            main_container.add(login_user_name);
            main_container.add(top_bar);

            menuBarContainer = new JPanel(new GridLayout(1, 3, 1, 0));
            menuBarContainer.setBounds(2, 74, DefaultSettings.DEFAULT_WIDTH - 5, 38);
            //  menuBarContainer.setBackground(null);
            //   main_container.add(menuBarContainer);



            topMenubar = new JPanel(new GridLayout(1, 3, 1, 0));
            topMenubar.setBounds(2, top_location + 25, DefaultSettings.DEFAULT_WIDTH - 4, 34);
            topMenubar.setBackground(Color.WHITE);
            main_container.add(topMenubar);

            buttons_gui24fnf[back_index] = DesignClasses.create_image_button_with_text_color(StaticImages.IMAGE_Scale_2, StaticImages.IMAGE_Scale_2_h, "Back", 11, 0, 10, 81, 66, 22, DefaultSettings.text_color1);
            // buttons_gui24fnf[back_index] = DesignClasses.create_image_button_with_out_bounds(top_menu_image[0], top_menu_image[0]);
            buttons_gui24fnf[back_index].addActionListener(this);
            buttons_gui24fnf[back_index].setVisible(false);
            main_container.add(buttons_gui24fnf[back_index]);
            //  menuBarContainer.add(buttons_gui24fnf[back_index]);

            //buttons_gui24fnf[invite_index] = DesignClasses.create_image_button_with_text_color(StaticImages.IMAGE_Scale_2, StaticImages.IMAGE_Scale_2_h, "Invite", 11, 0, 10, 81, 66, 22, DefaultSettings.text_color1);
            buttons_gui24fnf[invite_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[invite_index], tab_image_hover[invite_index]);
            buttons_gui24fnf[invite_index].addActionListener(this);
            //  main_container.add(buttons_gui24fnf[invite_index]);
            topMenubar.add(buttons_gui24fnf[invite_index]);



            //buttons_gui24fnf[group_index] = DesignClasses.create_image_button_with_text_color(StaticImages.IMAGE_Scale_2, StaticImages.IMAGE_Scale_2_h, "Groups", 11, 0, 120, 81, 66, 22, DefaultSettings.text_color1);
            buttons_gui24fnf[group_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[group_index], tab_image_hover[group_index]);
            buttons_gui24fnf[group_index].addActionListener(this);
            topMenubar.add(buttons_gui24fnf[group_index]);
            //  main_container.add(buttons_gui24fnf[group_index]);


            //  buttons_gui24fnf[logout_index] = DesignClasses.create_image_button_with_text_color(StaticImages.IMAGE_Scale_2, StaticImages.IMAGE_Scale_2_h, "Sign out", 11, 0, 230, 81, 66, 22, DefaultSettings.text_color1);
            buttons_gui24fnf[logout_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[logout_index], tab_image_hover[logout_index]);
            buttons_gui24fnf[logout_index].addActionListener(this);
            //  main_container.add(buttons_gui24fnf[logout_index]);
            // menuBarContainer.add(buttons_gui24fnf[logout_index]);
            topMenubar.add(buttons_gui24fnf[logout_index]);
            //     main_container.add(DesignClasses.create_image_label(3, 74, 297, 34, StaticImages.IMAGE_Scale_4));



            addContactPanel = new JPanel(new GridLayout(1, 3, 1, 0));
            addContactPanel.setBounds(2, DefaultSettings.DEFAULT_HEIGHT - 48, DefaultSettings.DEFAULT_WIDTH - 4, 45);
            addContactPanel.setBackground(Color.WHITE);
            main_container.add(addContactPanel);



            headerMenuContainer = new JPanel(new FlowLayout());
            //  headerMenuContainer.setLayout(new BoxLayout(headerMenuContainer, BoxLayout.X_AXIS));
            headerMenuContainer.setBounds(2, 108, DefaultSettings.DEFAULT_WIDTH - 5, 38);
            headerMenuContainer.setBackground(Color.WHITE);
            //    headerMenuContainer.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, DefaultSettings.disable_font_color));
            main_container.add(headerMenuContainer);
            headerMenuContainer.setVisible(false);

            int search_box_left = 120;
            int search_box_top = 115;
            int search_image_width = 25;

            status_cobmob_box = DesignClasses.create_ComboBox_from_array(MyFnFSettings.STATUS_ARRAY, 10, search_box_top, 70);
            status_cobmob_box.setPreferredSize(new Dimension(70, 23));
            headerMenuContainer.add(status_cobmob_box);
            //main_container.add(status_cobmob_box);
            JPanel p = new JPanel(new FlowLayout(2, 0, 3));
            p.setBackground(null);
            int p_width = 200;
            p.setPreferredSize(new Dimension(p_width, 30));
            search_image = DesignClasses.create_image_label(search_box_left - search_image_width, search_box_top, search_image_width, DefaultSettings.texBoxHeight, StaticImages.SEARCH_IMG);
            search_image.setOpaque(true);
            search_image.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, DefaultSettings.blue_border_color));
            search_image.setPreferredSize(new Dimension(search_image_width, 25));
            p.add(search_image);


            //     main_container.add(search_image);
            searchTextField = DesignClasses.makeTextField(default_mobile_text, search_box_left, search_box_top, DefaultSettings.searchtextBoxWidth);
            searchTextField.setForeground(DefaultSettings.disable_font_color);
            searchTextField.setBorder(BorderFactory.createMatteBorder(1, 0, 1, 1, DefaultSettings.blue_border_color));
            //    main_container.add(searchTextField);

            searchTextField.setPreferredSize(new Dimension((p_width - search_image_width) - 8, 25));
            //headerMenuContainer.add(searchTextField);
            p.add(searchTextField);
            headerMenuContainer.add(p);
            searchTextField.addFocusListener(new FocusListener() {
                public void focusLost(FocusEvent e) {
                    extra_methods.set_reset_defalut_text(searchTextField, default_mobile_text, true);
                }

                public void focusGained(FocusEvent e) {
                    if (searchTextField.getText().length() <= 0 || searchTextField.getText().toString().equals(default_mobile_text)) {
                        extra_methods.set_reset_defalut_text(searchTextField, default_mobile_text, false);
                    } else if (searchTextField.getText().toString().length() < 1) {
                        extra_methods.set_reset_defalut_text(searchTextField, default_mobile_text, true);
                    }
                }
            });
            searchTextField.requestFocusInWindow();
            searchTextField.grabFocus();
            searchTextField.requestFocus();
            searchTextField.getDocument().addDocumentListener(new DocumentListener() {
                @Override
                public void removeUpdate(DocumentEvent e) {
                    get_frind_list.getfriend_list();
                }

                @Override
                public void insertUpdate(DocumentEvent e) {
                    get_frind_list.getfriend_list();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    get_frind_list.getfriend_list();
                }
            });

            status_cobmob_box.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    get_frind_list.getfriend_list();

                }
            });
//            background_in_search_box = DesignClasses.makeJLabel_with_background_font_size_width_color("", 2, 108, 298, 40, 12, 0, DefaultSettings.text_color1, new Color(0xDCFAF7), 2);
//            main_container.add(background_in_search_box);
            //  no_frinend_label = DesignClasses.makeJLabel_with_background_font_size_width_color("No Friend", 50, 200, 50, 50, 14, 1, Color.BLUE, Color.WHITE, 0);
            //  no_frinend_label.setVisible(false);
            //    main_container.add(no_frinend_label);
            JPanel borderlaoutpanel = new JPanel();
            scrollPane = new LightScrollPane(borderlaoutpanel);
            scrollPane.setBounds(2, 160, 298, 390);
            borderlaoutpanel.setLayout(new BorderLayout(0, 0));
            borderlaoutpanel.setBackground(Color.WHITE);
            main_container.add(scrollPane);

            columnpanel = new JPanel();
            borderlaoutpanel.add(columnpanel, BorderLayout.NORTH);
            columnpanel.setLayout(new GridLayout(0, 1, 0, 1));
            columnpanel.setBackground(DefaultSettings.friend_list_border_color);
            show_no_freind_panel();
            new SetDynamicFriend().start();
            //   tab_buttons[contact_index] = create_buttons("", DefaultSettings.blue_bar_background, StaticImages.IMAGE_CONTACT_H, null);
            buttons_gui24fnf[contact_index] = DesignClasses.create_image_button_with_out_bounds(tab_image_hover[contact_index], tab_image_hover[contact_index]);
            buttons_gui24fnf[dail_pad_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[dail_pad_index], tab_image_hover[dail_pad_index]);
            buttons_gui24fnf[call_log_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[call_log_index], tab_image_hover[call_log_index]);
            buttons_gui24fnf[profile_index] = DesignClasses.create_image_button_with_out_bounds(tab_image[profile_index], tab_image_hover[profile_index]);
            buttons_gui24fnf[contact_index].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SearchThread.stopSearchThread();
                    set_reset_tab_buttons(contact_index);
                    if (!SetDynamicFriend.repaint_friendlist) {
                        SetDynamicFriend.repaint_friendlist = true;
                    }
                    get_frind_list.getfriend_list();
                    // set_visivility_search_box(tr);

                }
            });

            buttons_gui24fnf[dail_pad_index].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SearchThread.stopSearchThread();
                    set_reset_tab_buttons(dail_pad_index);
                    DialPad dail = new DialPad();
                    dail.create_show_pad();
                    set_reset_invite_and_back();
                    //   set_visivility_search_box(false);

                }
            });

            buttons_gui24fnf[call_log_index].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    //   set_visivility_search_box(false);
                    SearchThread.stopSearchThread();
                    set_reset_tab_buttons(call_log_index);
                    String pak_id = PackageActions.create_packed_id_for(SocketConstants.CALL_LOG);
                    JsonFields jf = new JsonFields();
                    jf.setAction(SocketConstants.REQUEST);
                    jf.setType(SocketConstants.CALL_LOG);
                    jf.setPacketId(pak_id);
                    jf.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                    PackageActions.sendPacketAsString(jf);
                    call_log_paint = new UpdateCallLog();
                    call_log_paint.call_log_repaint();
                    set_reset_invite_and_back();
                    SetDynamicFriend.repaint_friendlist = false;


                }
            });
            buttons_gui24fnf[profile_index].addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    SearchThread.stopSearchThread();
                    //  SetDynamicFriend.repaint_friendlist = true;
                    //     System.out.println("After Press: " + SetDynamicFriend.repaint_friendlist);
                    set_reset_tab_buttons(profile_index);
                    profile_update = new ProfileAddUpdate();
                    //ResetUserProfileFriendList.refesh_friend_clss();
                    profile_update.profile_repaint();
                    set_reset_invite_and_back();


                }
            });
            addContactPanel.add(buttons_gui24fnf[contact_index]);
            addContactPanel.add(buttons_gui24fnf[dail_pad_index]);
            addContactPanel.add(buttons_gui24fnf[call_log_index]);
            addContactPanel.add(buttons_gui24fnf[profile_index]);
            set_visivility(true);
            // set_visivility_search_box(false);
        }
        set_visivility_search_box(false);
//        RefreshFriendList rfl = new RefreshFriendList();
//        rfl.getfriend_list();
    }

    public void set_reset_tab_buttons(int index) {
        if (!SetDynamicFriend.runningFlag) {
            SetDynamicFriend.runningFlag = true;
        }

        for (int k = 0; k <= 6; k++) {
            if (index != group_index) {
                if (index == k) {
                    buttons_gui24fnf[k].setIcon(DesignClasses.return_image(tab_image_hover[k]));
                    buttons_gui24fnf[k].setRolloverIcon(DesignClasses.return_image(tab_image_hover[k]));
                } else {
                    buttons_gui24fnf[k].setIcon(DesignClasses.return_image(tab_image[k]));
                    buttons_gui24fnf[k].setRolloverIcon(DesignClasses.return_image(tab_image_hover[k]));
                }
            }

        }
        if (index == contact_index) {

            scrollPane.setBounds(2, 160, 298, 390);
            set_visivility_search_box(true);
        } else {
            if (index != group_index) {

                SetDynamicFriend.repaint_friendlist = false;
                //        GUI24FnF.no_frinend_label.setVisible(false);
                set_visivility_search_box(SetDynamicFriend.repaint_friendlist);
                scrollPane.setBounds(2, 130, 298, 420);
            }



        }
    }

//    private void set_reset_search_in_friend(Boolean dd) {
//        // searchButton.setVisible(dd);
//        searchTextField.setVisible(dd);
//        background_in_search_box.setVisible(dd);
//        status_cobmob_box.setVisible(dd);
//        status_cobmob_box.setVisible(dd);
//        search_image.setVisible(dd);
//
//    }
    public JButton create_buttons(final String text, final Color colr, final String image_source, final String image_hover_soruce) {

        JButton btn = new JButton();
        btn.setBackground(colr);
        if (image_source != null) {
            URL imgURL = getClass().getClassLoader().getResource(image_source);
            btn.setIcon(new ImageIcon(imgURL));
        } else {
            btn.setText(text);
        }
        btn.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        if (image_hover_soruce != null) {
            btn.setRolloverIcon(new ImageIcon(getClass().getResource(image_hover_soruce)));
        }
        return btn;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (event.getSource() == button_window_close) {
            HelperMethods.create_confrim_panel("Are you sure you want to sign out of 24FnF?", 10);
            if (HelperMethods.confirm == 1) {
                this.dispose();
                if (ua_profile.do_unregister) // unregisters the contact URL
                {
                    ua.printLog("UNREGISTER the contact URL");
                    ua.unregister();
                }
                GetResponseJsons.logout_user(MyFnFSettings.userProfile.getSessionId());

                MyFnFSettings.registrationStop = true;
                MyFnFSettings.isRegisterred = false;
                System.exit(0);
            }

        } else if (event.getSource() == button_window_minimize) {
            this.setState(Frame.ICONIFIED);
        }

        if (event.getSource() == buttons_gui24fnf[invite_index]) {
            //  set_top_menu_bar_items(invite_index);
            SearchThread th = new SearchThread();
            SearchThread.startSearchThread();
            th.start();
            invite_obj.repaint_invite();
            set_reset_tab_buttons(invite_index);
//            buttons_gui24fnf[back_index].setVisible(true);
//            buttons_gui24fnf[invite_index].setVisible(false);


        }
        if (event.getSource() == buttons_gui24fnf[group_index]) {
            if (FriendList.getInstance().getFriend_hash_map().isEmpty()) {
                HelperMethods.create_confrim_panel_no_cancel("You have no friend.", 50);
            } else {
                set_reset_tab_buttons(group_index);
                if (GroupsWindow.groupWindow != null) {
                    //  System.out.println("ddddsfjlksdjf sdjkl********************");
                    GroupsWindow.groupWindow.show_group_windows();
                    GroupsWindow.groupWindow.set_in_front_group_windows();
                } else {
                    GroupsWindow group = new GroupsWindow();
                    group.setVisible(true);
                }
            }


            //  grp.repaint_group_window();
            //  invite_obj = new InviteFrined();
            //   invite_obj.repaint_invite();




        }
        if (event.getSource() == buttons_gui24fnf[back_index]) {
            set_reset_tab_buttons(contact_index);
            if (!SetDynamicFriend.repaint_friendlist) {
                SetDynamicFriend.repaint_friendlist = true;

            }
            RefreshFriendList rfl = new RefreshFriendList();
            rfl.getfriend_list();
            set_reset_invite_and_back();
            buttons_gui24fnf[back_index].setVisible(false);
            buttons_gui24fnf[invite_index].setVisible(true);


        }
        if (event.getSource() == buttons_gui24fnf[logout_index]) {
            HelperMethods.create_confrim_panel(NotificationMessages.SIGN_OUT_NOTIFICAITON, 20);
            if (HelperMethods.confirm == 1) {
                ua_profile.first_time_login = false;
                closeAllGroupWindows();
                closeAllSingleUserProfileWindow();

                if (GraphicalUA.getStaticObject() != null) {
                    GraphicalUA.getStaticObject().forceHangUp();
                }
                if (IncomingGraphicalUA.getStaticObject() != null) {
                    IncomingGraphicalUA.getStaticObject().forceHangUp();
                }
                MyFnFSettings.isRegisterred = false;
                SetDynamicFriend.isThredActive = false;
                SetDynamicFriend.runningFlag = false;
                GetResponseJsons.logout_user(MyFnFSettings.userProfile.getSessionId());
                if (ua_profile.do_unregister) // unregisters the contact URL
                {
                    ua.printLog("UNREGISTER the contact URL");
                    ua.unregister();
                    if (MyFnFSettings.isRegisterred) {
                        MyFnFSettings.isRegisterred = false;

                    }
                }

                Main lgn = new Main(sip_provider, ua_profile);
                lgn.setVisible(true);
                this.dispose();
                if (new_user_frame != null) {
                    new_user_frame.dispose();
                }

                MyFnFSettings.registrationStop = true;
                MyFnFSettings.isRegisterred = false;

            }


        }


    }

    public void set_reset_invite_and_back() {
        if (!buttons_gui24fnf[invite_index].isVisible()) {
            buttons_gui24fnf[back_index].setVisible(false);
            buttons_gui24fnf[invite_index].setVisible(true);
        }
    }

    public void set_top_menu_bar_items(int index) {
        for (int k = 4; k <= 6; k++) {
            if (index == k && index != group_index) {
                buttons_gui24fnf[k].setIcon(DesignClasses.return_image(tab_image_hover[k]));
                buttons_gui24fnf[k].setRolloverIcon(DesignClasses.return_image(tab_image_hover[k]));
            } else {
                buttons_gui24fnf[k].setIcon(DesignClasses.return_image(tab_image[k]));
                buttons_gui24fnf[k].setRolloverIcon(DesignClasses.return_image(tab_image_hover[k]));
            }


        }
//        if (!buttons_gui24fnf[invite_index].i) {
//            buttons_gui24fnf[back_index].setVisible(false);
//            buttons_gui24fnf[invite_index].setVisible(true);
//        }
    }

    public void showLogin_User_panel(boolean new_user) {
        if (new_user) {
            //   System.out.println("new regitraions");
            if (flash_JLabel != null) {
                flash_JLabel.setVisible(false);
            }
            set_visivility(true);
            set_visivility_search_box(true);
        }
        this.setVisible(true);
    }

    private void closeAllSingleUserProfileWindow() {
        for (int i = 0; i < SingleUserProfile.listOfSingleUserObjects.size(); i++) {
            SingleUserProfile.listOfSingleUserObjects.get(i).dispose_this_window();
        }
    }

    private void closeAllGroupWindows() {
        if (GroupsWindow.listOfGroupsWindowObjects.size() > 0) {
            for (int i = 0; i < GroupsWindow.listOfGroupsWindowObjects.size(); i++) {
                GroupsWindow.listOfGroupsWindowObjects.get(i).dispose_group_windows();
            }
        }

    }

    @Override
    public void onUaRegistrationSucceeded(UserAgent ua, String result) {
//        MyFnFSettings.isRegisterred = true;
    }

    @Override
    public void onUaRegistrationFailed(UserAgent ua, String result) {
        MyFnFSettings.isRegisterred = false;
    }

    @Override
    public void onUaIncomingCall(UserAgent ua, NameAddress callee, NameAddress caller, Vector media_descs) {
//        SipProvider instanceSipProvider = GraphicalUA.getSipProvider();
//        String[] temp = caller.getAddress().toString().split("@");
//        String[] var = temp[0].split("|");
//        System.out.println(var[1]);
//        UserAgentProfile single_p = HelperMethods.get_all_for_a_profile(var[1]);
//        \



        SipProvider instanceSipProvider = GraphicalUA.getSipProvider();
        String[] temp = caller.getAddress().toString().split("@");
//        String[] var = temp[0].split("|");
        StringTokenizer st = new StringTokenizer(temp[0], SipParser.header_colon);
        st.nextToken();

        UserAgentProfile single_p = HelperMethods.get_all_for_a_profile(st.nextToken());

        
        if (single_p != null) {

            if (single_p.getUserIdentity() != null && single_p.getUserIdentity().length() > 0) {
                objMain = this;
                single_user = null;
                single_user = new IncomingGraphicalUA(instanceSipProvider, single_p);
                single_user.setVisible(true);
                IncomingGraphicalUA.getStaticObject().toFront();
                IncomingGraphicalUA.getStaticObject().setAlwaysOnTop(true);
                IncomingGraphicalUA.getStaticObject().setState(Frame.NORMAL);
            } else {
                ua.hangup();
            }
        }

    }

    @Override
    public void onUaCallCancelled(UserAgent ua) {
        // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallProgress(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallRinging(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallAccepted(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallTransferred(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallFailed(UserAgent ua, String reason) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaCallClosed(UserAgent ua) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaMediaSessionStarted(UserAgent ua, String type, String codec) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void onUaMediaSessionStopped(UserAgent ua, String type) {
        //throw new UnsupportedOperationException("Not supported yet.");
    }

//    @Override
//    public void onReceivedPacket(SendReceive udp, UdpPacket packet) {
//        String data = new String(packet.getData());
//        JsonFields getJson = HelperMethods.response_json(data);
//
//        if (getJson.getType().equals(SocketConstants.CONTACT_LIST)) {
//            RefreshFriendList rfl = new RefreshFriendList();
//            rfl.getfriend_list();
//        } else if (getJson.getType().equals(SocketConstants.CONTACT_SEARCH)) {
//            invite_obj.set_all_matches();
//        } else if (getJson.getType().equals(SocketConstants.ADD_FRIEND)) {
//            if (getJson.getAction().equals(SocketConstants.RESPONSE)) {
//                if (getJson.getSuccess()) {
//                    FriendList.getInstance().add_single_friend_entry(getJson);
//                    InviteFriendContainer.getInstance().getInviteFriendsContainer().remove(getJson.getUserIdentity());
//                    invite_obj.set_all_matches();
//
//                    // System.out.println("add in hash map frinend after add");
//                }
//            } else if (getJson.getAction().equals(SocketConstants.UPDATE)) {
//                String f_name = "Unknown";
//                String l_name = "   ";
//
//                if (getJson.getSuccess()) {
//
//                    /* *****************************not tested *******************************************************************/
//                    FriendList.getInstance().add_single_friend_entry(getJson);
//                    //    RefreshFriendList rfl = new RefreshFriendList();
//                    get_frind_list.getfriend_list();
//                    JsonFields fld = new JsonFields();
//                    fld.setAction(SocketConstants.CONFIRMATION);
//                    fld.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
//                    fld.setPacketIdFromServer(getJson.getPacketIdFromServer());
//
//                    if (getJson.getFirstName().length() > 0) {
//                        f_name = getJson.getFirstName();
//                        l_name = getJson.getLastName();
//                    }
//                    pak_send.sendPackageAsClass(fld, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT);
//                }
//                if (MyFnFSettings.isRegisterred) {
//
//                    new PresenceNotification().createNotificationWindow(f_name + " " + l_name, "Wants to be frined");
//                }
//
//            }
//        } else if (getJson.getType().equals(SocketConstants.DELETE_FRIEND)) {
//            if (getJson.getSuccess()) {
//                FriendList.getInstance().getFriend_hash_map().remove(getJson.getUserIdentity());
//                get_frind_list.getfriend_list();
//
//            } else {
//                if (SetDynamicFriend.repaint_friendlist) {
//                    HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 20);
//                }
//            }
//
//            if (getJson.getAction().equals(SocketConstants.UPDATE)) {
//                FriendList.getInstance().getFriend_hash_map().remove(getJson.getUserIdentity());
//                get_frind_list.getfriend_list();
//                pak_send.sendConfirmationPak(getJson.getPacketIdFromServer());
//
//            }
//        } else if (getJson.getType().equals(SocketConstants.ACCEPT_FRIEND)) {
//            if (getJson.getSuccess()) {
//                FriendList.getInstance().add_single_friend_entry(getJson);
//                get_frind_list.getfriend_list();
//
//            } else {
//                if (SetDynamicFriend.repaint_friendlist) {
//                    HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 20);
//                }
//            }
//            if (getJson.getAction().equals(SocketConstants.UPDATE)) {
//                /**
//                 * ****************************** not tested
//                 * ****************************************
//                 */
//                FriendList.getInstance().add_single_friend_entry(getJson);
//                get_frind_list.getfriend_list();
//                pak_send.sendConfirmationPak(getJson.getPacketIdFromServer());
//
//            }
//
//        }
//
//
//    }
    public static void set_userProfile(int indexNumber, String value) {

        if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
            MyFnFSettings.userProfile.setFirstName(value);
        }
        if (indexNumber == SocketConstants.CONS_LAST_NAME) {
            MyFnFSettings.userProfile.setLastName(value);
        }
        if (indexNumber == SocketConstants.CONS_GENDER) {
            MyFnFSettings.userProfile.setGender(value);
        }
        if (indexNumber == SocketConstants.CONS_COUNTRY) {
            MyFnFSettings.userProfile.setCountry(value);
        }
        if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
            MyFnFSettings.userProfile.setMobilePhone(value);
        }
        if (indexNumber == SocketConstants.CONS_EMAIL) {
            MyFnFSettings.userProfile.setEmail(value);
        }
        if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
            MyFnFSettings.userProfile.setWhatisInYourMind(value);
        }
        if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
            MyFnFSettings.userProfile.setMobilePhoneDialingCode(value);
        }

    }

//    private String get_value_for_index(int indexNumber, JsonFields js) {
//
//        if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
//            MyFnFSettings.userProfile.setFirstName(value);
//        }
//        if (indexNumber == SocketConstants.CONS_LAST_NAME) {
//            MyFnFSettings.userProfile.setLastName(value);
//        }
//        if (indexNumber == SocketConstants.CONS_GENDER) {
//            MyFnFSettings.userProfile.setGender(value);
//        }
//        if (indexNumber == SocketConstants.CONS_COUNTRY) {
//            MyFnFSettings.userProfile.setCountry(value);
//        }
//        if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
//            MyFnFSettings.userProfile.setMobilePhone(value);
//        }
//        if (indexNumber == SocketConstants.CONS_EMAIL) {
//            MyFnFSettings.userProfile.setEmail(value);
//        }
//        if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
//            MyFnFSettings.userProfile.setWhatisInYourMind(value);
//        }
//        if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
//            MyFnFSettings.userProfile.setMobilePhoneDialingCode(value);
//        }
//
//    }
    public static void change_in_frined_hashMap(String key, int indexNumber, String value) {

        if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
            FriendList.getInstance().getFriend_hash_map().get(key).setFirstName(value);
        }
        if (indexNumber == SocketConstants.CONS_LAST_NAME) {
            FriendList.getInstance().getFriend_hash_map().get(key).setLastName(value);
        }
        if (indexNumber == SocketConstants.CONS_GENDER) {
            FriendList.getInstance().getFriend_hash_map().get(key).setGender(value);
        }
        if (indexNumber == SocketConstants.CONS_COUNTRY) {
            FriendList.getInstance().getFriend_hash_map().get(key).setCountry(value);
        }
        if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
            FriendList.getInstance().getFriend_hash_map().get(key).setMobilePhone(value);
        }
        if (indexNumber == SocketConstants.CONS_EMAIL) {
            FriendList.getInstance().getFriend_hash_map().get(key).setEmail(value);
        }
        if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
            FriendList.getInstance().getFriend_hash_map().get(key).setWhatisInYourMind(value);
        }
        if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
            FriendList.getInstance().getFriend_hash_map().get(key).setMobilePhoneDialingCode(value);
        }
//if (indexNumber == SocketConstants.CONS_PRESENCE) {
// 
//            FriendList.getInstance().getFriend_hash_map().get(key).setPresence(value);
//        }


    }

    public static void set_visivility(Boolean dd) {
//        buttons_gui24fnf[invite_index].setVisible(dd);
//        buttons_gui24fnf[logout_index].setVisible(dd);
//        buttons_gui24fnf[group_index].setVisible(dd);
        if (topMenubar != null) {
            topMenubar.setVisible(dd);
        }
        if (scrollPane != null) {
            scrollPane.setVisible(dd);
        }
        if (addContactPanel != null) {
            addContactPanel.setVisible(dd);
        }

        //  set_visivility_search_box(dd);
    }

//    public static void show_no_freind_options(boolean t) {
//        if (no_frinend_label != null) {
//            no_frinend_label.setVisible(t);
//        }
//    }
    public static void show_no_freind_panel() {
        columnpanel.removeAll();

        JPanel rowPanel = new JPanel();
        rowPanel.setBackground(Color.WHITE);
        rowPanel.setPreferredSize(new Dimension(230, 40));
        JLabel dddf = DesignClasses.makeJLabel_with_background_font_size_width_color("No Friend", 50, 2, 200, 25, 14, 1, Color.BLUE, Color.WHITE, 0);
        rowPanel.add(dddf);
        rowPanel.setLayout(null);
        columnpanel.add(rowPanel);
        columnpanel.revalidate();
        columnpanel.repaint();
    }

    public static void set_visivility_search_box(Boolean dd) {
        if (!FriendList.getInstance().getFriend_hash_map().isEmpty()) {
            if (headerMenuContainer != null) {
                headerMenuContainer.setVisible(dd);
            }
        }
    }

    public static void set_visivility_flash_loading_image(Boolean dd) {

        if (flash_JLabel != null) {
            flash_JLabel.setVisible(dd);
        }
    }
//    private void setContactList() {
//        for (String key : TempStorageOfContactList.getInstance().getResponse_msg_container().keySet()) {
//            System.out.println("key" + TempStorageOfContactList.getInstance().getResponse_msg_container().get(key));
//        }
//
//    }

    @Override
    public void onReceivedData(JsonFields getJson) {
        if (getJson.getAction().equals("SIGN_OUT")) {
            
//            if(MyFnFSettings.isAuthenticated){
                HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 20);
//            }
            

            ua_profile.first_time_login = false;
            closeAllGroupWindows();
            closeAllSingleUserProfileWindow();

            if (GraphicalUA.getStaticObject() != null) {
                GraphicalUA.getStaticObject().forceHangUp();
            }
            if (IncomingGraphicalUA.getStaticObject() != null) {
                IncomingGraphicalUA.getStaticObject().forceHangUp();
            }
            MyFnFSettings.isRegisterred = false;
            
            MyFnFSettings.isAuthenticated = false;
            
            SetDynamicFriend.isThredActive = false;
            SetDynamicFriend.runningFlag = false;
            //GetResponseJsons.logout_user(MyFnFSettings.userProfile.getSessionId());
            
            if (ua_profile.do_unregister) // unregisters the contact URL
            {
                ua.printLog("UNREGISTER the contact URL");
                ua.unregister();
                if (MyFnFSettings.isRegisterred) {
                    MyFnFSettings.isRegisterred = false;

                }
            }

            Main lgn = new Main(sip_provider, ua_profile);
            lgn.setVisible(true);
            this.dispose();
            if (new_user_frame != null) {
                new_user_frame.dispose();
            }
        } else {
            if (getJson.getType().equals(SocketConstants.CONTACT_LIST)) {
                RefreshFriendList rfl = new RefreshFriendList();
                rfl.getfriend_list();
            } else if (getJson.getType().equals(SocketConstants.CONTACT_SEARCH)) {
                invite_obj.set_all_matches();
            } else if (getJson.getType().equals(SocketConstants.ADD_FRIEND)) {
                if (getJson.getAction().equals(SocketConstants.RESPONSE)) {
                    if (getJson.getSuccess()) {
                        FriendList.getInstance().add_single_friend_entry(getJson);
                        InviteFriendContainer.getInstance().getInviteFriendsContainer().remove(getJson.getUserIdentity());
                        invite_obj.set_all_matches();

                        // System.out.println("add in hash map frinend after add");
                    }
                } else if (getJson.getAction().equals(SocketConstants.UPDATE)) {
                    String f_name = "Unknown";
                    String l_name = "   ";

                    if (getJson.getSuccess()) {

                        /* *****************************not tested *******************************************************************/
                        FriendList.getInstance().add_single_friend_entry(getJson);
                        get_frind_list.getfriend_list();
//                    JsonFields fld = new JsonFields();
//                    fld.setAction(SocketConstants.CONFIRMATION);
//                    fld.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
//                    fld.setPacketIdFromServer(getJson.getPacketIdFromServer());
//                    if (getJson.getPacketIdFromServer().length() > 0) {
//                        PackageActions.sendConfirmationPak(getJson.getPacketIdFromServer());
//                    }

                        if (getJson.getFirstName().length() > 0) {
                            f_name = getJson.getFirstName();
                            l_name = getJson.getLastName();
                        }

                    }
                    if (MyFnFSettings.isRegisterred) {
                        new PresenceNotification().createNotificationWindow(f_name + " " + l_name, "Wants to be frined");
                    }

                }
            } else if (getJson.getType().equals(SocketConstants.DELETE_FRIEND)) {
                if (getJson.getSuccess()) {
                    FriendList.getInstance().getFriend_hash_map().remove(getJson.getUserIdentity());
                    get_frind_list.getfriend_list();

                } else {
                    if (SetDynamicFriend.repaint_friendlist) {
                        HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 20);
                    }
                }

                if (getJson.getAction().equals(SocketConstants.UPDATE)) {
                    FriendList.getInstance().getFriend_hash_map().remove(getJson.getUserIdentity());
                    get_frind_list.getfriend_list();
                    pak_send.sendConfirmationPak(getJson.getPacketIdFromServer());

                }
            } else if (getJson.getType().equals(SocketConstants.ACCEPT_FRIEND)) {
                if (getJson.getSuccess()) {
                    FriendList.getInstance().add_single_friend_entry(getJson);
                    get_frind_list.getfriend_list();

                } else {
                    if (SetDynamicFriend.repaint_friendlist) {
                        HelperMethods.create_confrim_panel_no_cancel(getJson.getMessage(), 20);
                    }
                }
                if (getJson.getAction().equals(SocketConstants.UPDATE)) {
                    /**
                     * ****************************** not tested
                     * ****************************************
                     */
                    FriendList.getInstance().add_single_friend_entry(getJson);
                    get_frind_list.getfriend_list();
                    pak_send.sendConfirmationPak(getJson.getPacketIdFromServer());

                }

            }
        }
    }
}
