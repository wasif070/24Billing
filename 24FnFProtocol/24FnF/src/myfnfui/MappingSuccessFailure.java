/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

/**
 *
 * @author FaizAhmed
 */
public class MappingSuccessFailure {

    private Boolean success;
    private String message = "";

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
