/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

import java.util.ArrayList;
import local.ua.UserAgentProfile;
import myfnfui.FeedBackFields;

/**
 *
 * @author faizahmed
 */
public class FriendlistMapping extends FeedBackFields {

    private ArrayList<UserAgentProfile> contactList = null;
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public ArrayList<UserAgentProfile> getContactList() {
        return contactList;
    }

    public void setContactList(ArrayList<UserAgentProfile> contactList) {
        this.contactList = contactList;
    }
}
