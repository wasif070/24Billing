/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

//import dddd.StoreMsgParser;
import myfnfui.usedThreads.SendPacketCheckerThread;
import com.google.gson.Gson;
import helpers.HelperMethods;
import helpers.MyFnFSettings;
import helpers.SocketConstants;
import myfnfui.UdpPacket;
import myfnfui.UdpTransport;
import myfnfui.IpAddress;
import myfnfui.JsonFields;
//import myfnfui.containers.TempStorageOfContactList;

/**
 *
 * @author faizahmed
 */
public class PackageActions {

    private SendPacketCheckerThread sendPackedParserThread = null;

    public static void sendConfirmationPak(String packetIDfromServer) {
        JsonFields fld = new JsonFields();
        fld.setAction(SocketConstants.CONFIRMATION);
        fld.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        fld.setPacketIdFromServer(packetIDfromServer);
        sendPacketAsString(fld);

    }

    public static void sendPacketAsString(Object ob) {
        if (SocketConstants.SOCKET_PORT > 0) {
            try {
                String packet = new Gson().toJson(ob);
                //  System.out.println("converted____"+packet);
                UdpPacket pkd_to_server = new UdpPacket(packet.getBytes());
                pkd_to_server.setIpAddress(new IpAddress(SocketConstants.authIPAdress()));
                pkd_to_server.setPort(SocketConstants.SOCKET_PORT);
                UdpTransport.getInstance().send(pkd_to_server);
//                System.err.println("Sent packet  : " + packet);
            } catch (Exception e) {
            }
        } else {
            HelperMethods.create_confrim_panel_no_cancel("Communication port problem", 50);
        }

    }

    public static void sendPackedAsString_as_plan_string(String packaet) {
        if (SocketConstants.SOCKET_PORT > 0) {
            try {
                UdpPacket pkd_to_server = new UdpPacket(packaet.getBytes());
                pkd_to_server.setIpAddress(new IpAddress(SocketConstants.authIPAdress()));
                pkd_to_server.setPort(SocketConstants.SOCKET_PORT);
                UdpTransport.getInstance().send(pkd_to_server);
                //    System.err.println("Resend packet  : " + packet);
            } catch (Exception e) {
            }
        } else {
            HelperMethods.create_confrim_panel_no_cancel("Communication port problem", 50);
        }

    }

    public void send_And_Store_Packed_As_String(Object js, String packID) {
        String create_string = new Gson().toJson(js);
        if (create_string.length() > 0) {
            try {
                //   System.out.println("d2222222222222222222222222222222222");
                sendPacketAsString(js);
                StorePacketFormat packe_format = new StorePacketFormat();
                packe_format.setPacketToSend(create_string);
                packe_format.setSent_time(SendPackages.getInstance().get_current_time());
                SendPackages.getInstance().getStore_packet().put(packID, packe_format);
                if (sendPackedParserThread == null) {
                    sendPackedParserThread = new SendPacketCheckerThread();
                    sendPackedParserThread.start();
                }
                if (sendPackedParserThread.getState() == Thread.State.WAITING) {
                    synchronized (sendPackedParserThread) {
                        sendPackedParserThread.notify();
                    }
                }

            } catch (Exception e) {
            }
        }

    }

    public static String create_packed_id_for(String action_type) {
        String pak_id;
        pak_id = MyFnFSettings.LOGIN_USER_ID + action_type + System.currentTimeMillis();
        return pak_id;
    }

    public static void send_remove_or_delete_request(String user_id) {
        String delete_paked_id = create_packed_id_for(SocketConstants.DELETE_FRIEND);
        JsonFields js_fields = new JsonFields();
        js_fields.setAction(SocketConstants.UPDATE);
        js_fields.setType(SocketConstants.DELETE_FRIEND);
        js_fields.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        js_fields.setPacketId(delete_paked_id);
        js_fields.setUserIdentity(user_id);
        sendPacketAsString(js_fields);
//                     
    }
}
