/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

/**
 *
 * @author user
 */
public class StorePacketFormat {

    private long sent_time;
    private int counter = 0;
    String packetToSend;

    public String getPacketToSend() {
        return packetToSend;
    }

    public void setPacketToSend(String packetToSend) {
        this.packetToSend = packetToSend;
    }

//    public String getPacket() {
//        return packet;
//    }
//
//    public void setPacket(String packet) {
//        this.packet = packet;
//    }
    public long getSent_time() {
        return sent_time;
    }

    public void setSent_time(long sent_time) {
        this.sent_time = sent_time;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
}
