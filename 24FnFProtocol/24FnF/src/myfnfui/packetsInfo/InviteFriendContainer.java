/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

import java.util.HashMap;
import java.util.Map;
import local.ua.UserAgentProfile;

/**
 *
 * @author faizahmed
 */
public class InviteFriendContainer {

    private Map<String, UserAgentProfile> inviteFriendsContainer = new HashMap<String, UserAgentProfile>();

    public Map<String, UserAgentProfile> getInviteFriendsContainer() {
        return inviteFriendsContainer;
    }

    public void setInviteFriendsContainer(Map<String, UserAgentProfile> inviteFriendsContainer) {
        this.inviteFriendsContainer = inviteFriendsContainer;
    }
    public static InviteFriendContainer inviteFriends;

    public static InviteFriendContainer getInstance() {
        if (inviteFriends == null) {
            inviteFriends = new InviteFriendContainer();
        }
        return inviteFriends;
    }
//    public void add_single_friend_entry(JsonFields jsfld) {
////        "action":"RESPONSE",
////"type":"add_friend",
////"packetId":"packetId",        //given by source
////"success":"true",
////"userIdentity":"bbb",
////"firstName":"bbb",
////"lastName":"bbb",			
////"gender":"Male",
////"country":2,
////"mobilePhone":"",			
////"email":"",
////"presence":"1",
////"whatisInYourMind":" i am busy.... its raining outside ",
////"friendShipStatus":"1"
//        UserAgentProfile user = new UserAgentProfile();
//        user.setFirstName(jsfld.getFirstName());
//        user.setUserIdentity(jsfld.getUserIdentity());
//        user.setLastName(jsfld.getLastName());
//        user.setGender(jsfld.getGender());
//        user.setCountry(jsfld.getCountry());
//        user.setMobilePhone(jsfld.getMobilePhone());
//        user.setEmail(jsfld.getEmail());
//        user.setPresence(jsfld.getPresence());
//        user.setWhatisInYourMind(jsfld.getWhatisInYourMind());
//        user.setFriendShipStatus(jsfld.getFriendShipStatus());
//        FriendList.getInstance().getFriend_hash_map().put(jsfld.getUserIdentity(), user);
//        //this.getInviteFriendsContainer().put(jsfld.getUserIdentity(), user);
//        // user.setFirstName(jsfld.getFirstName());
//
//    }
}
