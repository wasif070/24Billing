/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import local.ua.UserAgentProfile;
import myfnfui.JsonFields;

/**
 *
 * @author faizahmed
 */
public class FriendList {

    private Map<String, UserAgentProfile> friend_hash_map = new ConcurrentHashMap<String, UserAgentProfile>();

    public Map<String, UserAgentProfile> getFriend_hash_map() {
        return friend_hash_map;
    }

    public void setFriend_hash_map(Map<String, UserAgentProfile> friend_hash_map) {
        this.friend_hash_map = friend_hash_map;
    }
    public static FriendList contactListObject;
    public static FriendList group_friend_map;

    public static FriendList getGroup_friend_map() {
        if (group_friend_map == null) {
            group_friend_map = new FriendList();
        }
        return group_friend_map;
    }

    public static FriendList getInstance() {
        if (contactListObject == null) {
            contactListObject = new FriendList();
        }
        return contactListObject;
    }

    public void add_single_friend_entry(JsonFields jsfld) {
//        "action":"RESPONSE",
//"type":"add_friend",
//"packetId":"packetId",        //given by source
//"success":"true",
//"userIdentity":"bbb",
//"firstName":"bbb",
//"lastName":"bbb",			
//"gender":"Male",
//"country":2,
//"mobilePhone":"",			
//"email":"",
//"presence":"1",
//"whatisInYourMind":" i am busy.... its raining outside ",
//"friendShipStatus":"1"
        UserAgentProfile user = new UserAgentProfile();
        user.setFirstName(jsfld.getFirstName());
        user.setUserIdentity(jsfld.getUserIdentity());
        user.setLastName(jsfld.getLastName());
        user.setGender(jsfld.getGender());
        user.setCountry(jsfld.getCountry());
        user.setMobilePhone(jsfld.getMobilePhone());
        user.setEmail(jsfld.getEmail());
        user.setPresence(jsfld.getPresence());
        user.setWhatisInYourMind(jsfld.getWhatisInYourMind());
        user.setFriendShipStatus(jsfld.getFriendShipStatus());
        FriendList.getInstance().getFriend_hash_map().put(jsfld.getUserIdentity(), user);
        //this.getInviteFriendsContainer().put(jsfld.getUserIdentity(), user);
        // user.setFirstName(jsfld.getFirstName());

    }
     
}
