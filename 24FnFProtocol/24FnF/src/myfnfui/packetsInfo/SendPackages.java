/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

import java.util.HashMap;
import java.util.Map;
import myfnfui.groups.CreateGroup;

/**
 *
 * @author faizahmed
 */
public class SendPackages {

    private Map<String, SendPackedFormat> send_packet = new HashMap<String, SendPackedFormat>();
    private Map<String, StorePacketFormat> store_packet = new HashMap<String, StorePacketFormat>();
    public static SendPackages stored_paked_in_hash_map;

    public long get_current_time() {
        return System.currentTimeMillis();
    }

    public static SendPackages getInstance() {
        if (stored_paked_in_hash_map == null) {
            stored_paked_in_hash_map = new SendPackages();
        }
        return stored_paked_in_hash_map;
    }

    public Map<String, SendPackedFormat> getSend_packet() {
        return send_packet;
    }

    public void setSend_packet(Map<String, SendPackedFormat> send_packet) {
        this.send_packet = send_packet;
    }

    public static SendPackages getStored_paked_in_hash_map() {
        return stored_paked_in_hash_map;
    }

    public static void setStored_paked_in_hash_map(SendPackages stored_paked_in_hash_map) {
        SendPackages.stored_paked_in_hash_map = stored_paked_in_hash_map;
    }

    public Map<String, StorePacketFormat> getStore_packet() {
        return store_packet;
    }

    public void setStore_packet(Map<String, StorePacketFormat> store_packet) {
        this.store_packet = store_packet;
    }
}
