/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.packetsInfo;

import myfnfui.IpAddress;
import myfnfui.JsonFields;

/**
 *
 * @author faizahmed
 */
public class SendPackedFormat {

    //private String packet;
    private long sent_time;
    private IpAddress dest_ip;
    private int dest_port = 0;
    private int counter = 0;
    private JsonFields fields;

    public JsonFields getFields() {
        return fields;
    }

    public void setFields(JsonFields fields) {
        this.fields = fields;
    }

//    public String getPacket() {
//        return packet;
//    }
//
//    public void setPacket(String packet) {
//        this.packet = packet;
//    }
    public IpAddress getDest_ip() {
        return dest_ip;
    }

    public void setDest_ip(IpAddress dest_ip) {
        this.dest_ip = dest_ip;
    }

    public int getDest_port() {
        return dest_port;
    }

    public void setDest_port(int dest_port) {
        this.dest_port = dest_port;
    }

    public long getSent_time() {
        return sent_time;
    }

    public void setSent_time(long sent_time) {
        this.sent_time = sent_time;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }
//    public UdpPacket getPacket() {
//        return packet;
//    }
//
//    public void setPacket(UdpPacket packet) {
//        this.packet = packet;
//    }
//    public void setSent_time(Long sent_time) {
//        this.sent_time = sent_time;
//    }
}
