/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import local.ua.UserAgentProfile;

/**
 *
 * @author faizahmed
 */
public class LoginUserProfile extends UserAgentProfile {

    public static LoginUserProfile login_user_profile = null;

    public static LoginUserProfile getLogin_user_profile() {
        return login_user_profile;
    }

    public static void setLogin_user_profile(LoginUserProfile login_user_profile) {
        LoginUserProfile.login_user_profile = login_user_profile;
    }

    public static LoginUserProfile getInstance() {
        if (login_user_profile == null) {
            login_user_profile = new LoginUserProfile();
        }

        return login_user_profile;
    }
}
