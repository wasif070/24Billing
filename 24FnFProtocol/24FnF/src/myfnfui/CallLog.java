package myfnfui;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

public class CallLog {

    private static CallLog callLog = null;
    String columnNames[] = {"Dialed Number", "Call Duration"};
    String rowData[][] = null;
    JFrame frame = null;
    JTable table = null;
    JScrollPane scrollPane = null;

    private CallLog() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ImageIcon imageIcon = new ImageIcon(getClass().getResource("/images/icon.png"));
        Image image = imageIcon.getImage();
        frame.setIconImage(image);
        frame.setTitle("24FnF");
        frame.setLocationRelativeTo(null);
        rowData = new String[20][2];
    }

    public static CallLog getInstance() {
        if (callLog == null) {
            createCallLog();
        }
        return callLog;
    }

    private synchronized static void createCallLog() {
        if (callLog == null) {
            callLog = new CallLog();
        }
    }

    public synchronized void showTable(ArrayList<String> list) {
        int length = list.size();
        for (int iY = 0; iY < length; iY++) {
            String str = list.get(length - (iY + 1));
            for (int iX = 0; iX < 2; iX++) {
                if (iX == 0) {
                    rowData[iY][iX] = str.substring(0, str.indexOf(","));
                } else {
                    rowData[iY][iX] = str.substring(str.indexOf(",") + 1);
                }
            }
        }
        table = new JTable(rowData, columnNames);
        scrollPane = new JScrollPane(table);
        frame.add(scrollPane, BorderLayout.CENTER);
        frame.setState(Frame.NORMAL);
        frame.setSize(400, 250);
        frame.setVisible(true);
    }
}