/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import helpers.CallLog;
import java.util.ArrayList;

/**
 *
 * @author FaizAhmed
 */
public class CallLogDetails extends FeedBackFields {

    private String friendId;

    public String getFriendId() {
        return friendId;
    }

    public void setFriendId(String friendId) {
        this.friendId = friendId;
    }
    private ArrayList<CallLog> callLog = null;

    public ArrayList<CallLog> getCallLog() {
        return callLog;
    }

    public void setCallLog(ArrayList<CallLog> callLog) {
        this.callLog = callLog;
    }
}
