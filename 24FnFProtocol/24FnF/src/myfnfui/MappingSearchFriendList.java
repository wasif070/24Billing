/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import java.util.ArrayList;
import local.ua.UserAgentProfile;

/**
 *
 * @author FaizAhmed
 */
public class MappingSearchFriendList extends UserAgentProfile {

    private UserAgentProfile uniqueProfile = null;
    private ArrayList<UserAgentProfile> searchedcontaclist = null;

    public UserAgentProfile getUniqueProfile() {
        return uniqueProfile;
    }

    public void setUniqueProfile(UserAgentProfile uniqueProfile) {
        this.uniqueProfile = uniqueProfile;
    }

    public ArrayList<UserAgentProfile> getSearchedcontaclist() {
        return searchedcontaclist;
    }

    public void setSearchedcontaclist(ArrayList<UserAgentProfile> searchedcontaclist) {
        this.searchedcontaclist = searchedcontaclist;
    }
}
