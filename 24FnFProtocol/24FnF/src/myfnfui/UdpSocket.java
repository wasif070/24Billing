/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 *
 * @author reefat
 */
public class UdpSocket {

    DatagramSocket socket;

    public DatagramSocket getSocket() {
        return socket;
    }

    public UdpSocket() {
        this.socket = null;
    }

    public UdpSocket(int port) throws java.net.SocketException {
        socket = new DatagramSocket(port);
    }

    public UdpSocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public UdpSocket(int port, IpAddress ipaddr) throws java.net.SocketException {
        socket = new DatagramSocket(port, ipaddr.getInetAddress());
    }

    public void close() {
        socket.close();
    }

    public void receive(UdpPacket pkt) throws java.io.IOException {
        DatagramPacket dgram = pkt.getDatagramPacket();
        socket.receive(dgram);
        pkt.setDatagramPacket(dgram);
    }

    public void send(UdpPacket pkt) throws java.io.IOException {
        socket.send(pkt.getDatagramPacket());
    }
}
