/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import helpers.DefaultSettings;
import helpers.DesignClasses;
import helpers.MyFnFSettings;
import helpers.SearchItem;
import helpers.SocketConstants;
import utils.SearchStringQueue;
import helpers.StaticImages;
import helpers.UiMethods;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import local.ua.UserAgentProfile;
import myfnfui.packetsInfo.InviteFriendContainer;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author FaizAhmed
 */
public class InviteFrined {

    JTextField searchTextField;
    JPanel frindlist_container;
    //   JPanel columnpanel;
    JButton searchButton;
    JPanel rowPanel;
    LightScrollPane scrollPane;
    JPanel columnpanel;
    Gson jsonLib;
    int margin_left = 18;
    UiMethods extra_methods;
    String defalut_text = "Search";
    PackageActions pak_action;

    public InviteFrined() {
        extra_methods = new UiMethods();
        pak_action = new PackageActions();
    }

    public void repaint_invite() {

        GUI24FnF.columnpanel.removeAll();
        GUI24FnF.columnpanel.add(createProfileScreen());
        GUI24FnF.columnpanel.revalidate();
        GUI24FnF.columnpanel.repaint();
        searchTextField.requestFocus();
    }

    private JPanel createProfileScreen() {
        int top_height = 00;

        rowPanel = new JPanel();
        columnpanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(230, 400));
        rowPanel.setBackground(Color.WHITE);
        rowPanel.setLayout(null);
        searchTextField = DesignClasses.makeTextField(defalut_text, margin_left, top_height, DefaultSettings.searchtextBoxWidth);
        rowPanel.add(searchTextField);
        searchTextField.setForeground(DefaultSettings.disable_font_color);
        //  searchTextField.addKeyListener(this);
        // highScore.setFont(font);
        //  searchTextField.requestFocusInWindow();
        //  searchTextField.grabFocus();
        // searchTextField.requestFocus();
        searchButton = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_SEARCH_IMAGE, StaticImages.IMAGE_SEARCH_IMAGE_H, "", 12, 0, margin_left + DefaultSettings.searchtextBoxWidth, top_height, 80, 25);
        rowPanel.add(searchButton);
        searchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                get_all_matches(MyFnFSettings.userProfile.getUserIdentity(), MyFnFSettings.userProfile.getSessionId(), searchTextField.getText());
            }
        });
        searchTextField.addFocusListener(new FocusListener() {
            public void focusLost(FocusEvent e) {
                extra_methods.set_reset_defalut_text(searchTextField, defalut_text, true);
            }

            public void focusGained(FocusEvent e) {
                if (searchTextField.getText().length() <= 0 || searchTextField.getText().toString().equals(defalut_text)) {
                    extra_methods.set_reset_defalut_text(searchTextField, defalut_text, false);
                } else if (searchTextField.getText().toString().length() < 1) {
                    extra_methods.set_reset_defalut_text(searchTextField, defalut_text, true);
                }
            }
        });

        JPanel borderlaoutpanel = new JPanel();
        scrollPane = new LightScrollPane(borderlaoutpanel);
        scrollPane.setBounds(2, top_height + 50, 295, 350);
        //   scrollPane.setViewportView(borderlaoutpanel);
        borderlaoutpanel.setLayout(new BorderLayout(0, 0));
        borderlaoutpanel.setBackground(Color.WHITE);
        borderlaoutpanel.add(columnpanel, BorderLayout.NORTH);
        columnpanel.setLayout(new GridLayout(0, 1, 0, 1));
        columnpanel.setBackground(Color.WHITE);
        borderlaoutpanel.add(columnpanel, BorderLayout.NORTH);
// searchTextField.add
        searchTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                get_all_matches(MyFnFSettings.userProfile.getUserIdentity(), MyFnFSettings.userProfile.getSessionId(), searchTextField.getText());
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                get_all_matches(MyFnFSettings.userProfile.getUserIdentity(), MyFnFSettings.userProfile.getSessionId(), searchTextField.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                get_all_matches(MyFnFSettings.userProfile.getUserIdentity(), MyFnFSettings.userProfile.getSessionId(), searchTextField.getText());
            }
        });

        rowPanel.add(scrollPane);


        return rowPanel;
    }
    private JPanel create_panel;
    static int number = 0;
    static int number_of_friend;
    static int button_index_friend;
    //JButton[] buttons;

    public void get_all_matches(String userid, String sessionID, String search_string) {
        SearchItem item = new SearchItem();
        item.setSearch_string(search_string);
        item.setSearch_time(System.currentTimeMillis());
        item.setUser_id(userid);
        item.setSession_id(sessionID);
        item.setInvite_frnd(this);
        SearchStringQueue.getInstance().push(item);
    }

    public void set_all_matches() {
        try {
            columnpanel.removeAll();
            jsonLib = new GsonBuilder().serializeNulls().create();
            //  System.out.println(searchTextField.getText());
            if (InviteFriendContainer.getInstance().getInviteFriendsContainer() != null) {
                if (searchTextField.getText().length() > 0) {
                    for (String key : InviteFriendContainer.getInstance().getInviteFriendsContainer().keySet()) {

                        if (InviteFriendContainer.getInstance().getInviteFriendsContainer().get(key).getUserIdentity().contains(searchTextField.getText())) {
                            UserAgentProfile p = InviteFriendContainer.getInstance().getInviteFriendsContainer().get(key);
                            createJbutton(p.getFirstName() + " " + p.getLastName(), MyFnFSettings.userProfile.getSessionId(), p.getUserIdentity(), p.getCountry());
                            columnpanel.add(create_panel);
                        }
                    }
                }
            } else {
                columnpanel.removeAll();
            }
//            ArrayList<UserAgentProfile> searchList = null;
//            if (response_string != null) {
//                MyFnFSettings.search_list = jsonLib.fromJson(response_string, MappingSearchFriendList.class);
//            }
//            if (MyFnFSettings.search_list != null) {
//                searchList = MyFnFSettings.search_list.getSearchedcontaclist();
//                if (searchList != null) {
//                    if (searchList.isEmpty()) {
//                        columnpanel.removeAll();
//                    } else {
//                        columnpanel.removeAll();
//                        columnpanel.removeAll();
//                        for (final UserAgentProfile p : searchList) {
//                            createJbutton(p.getFirstName() + " " + p.getLastName(), MyFnFSettings.userProfile.getSessionId(), p.getUserIdentity(), p.getCountry());
//                            columnpanel.add(create_panel);
//                        }
//                    }
//                } else {
//                    columnpanel.removeAll();
//                }
//            } else {
//                columnpanel.removeAll();
//            }

            columnpanel.revalidate();
            columnpanel.repaint();
        } catch (Exception ex) {
        }
    }

    public void createJbutton(final String text, final String session_id2, final String friend_user_id2, final String country2) {
        JPanel rowPanel = new JPanel();
        rowPanel.setPreferredSize(new Dimension(250, 40));
        //   columnpanel.add(rowPanel);
        rowPanel.setLayout(null);

        final JButton add_action = DesignClasses.create_image_button_with_text_scale1("Invite", 220, 8);
        // add_action.setBounds(220, 8, 52, 25);
//        myB.add(add_action);
        add_action.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                add_action.setEnabled(false);
//                String response_string2 = GetResponseJsons.sent_add_contact_json(friend_user_id2, session_id2);
//                if (response_string2.length() > 0) {
//                    //System.out.println(response_string2);
//                    //  option = JOptionPane.showConfirmDialog(null, "Add friend " + text + " ?");
//                    MyFnFSettings.map_success_fail = jsonLib.fromJson(response_string2, MappingSuccessFailure.class);
//
//                    if (MyFnFSettings.map_success_fail.getSuccess() == true) {
//                        add_action.setEnabled(false);
//                        get_all_matches(MyFnFSettings.userProfile.getUserIdentity(), MyFnFSettings.userProfile.getSessionId(), searchTextField.getText());
//
//
//                    } else {
//                        add_action.setEnabled(false);
//                        HelperMethods.create_confrim_panel_no_cancel("Already Added " + text, 75);
//                    }
//                } else {
//                    add_action.setEnabled(true);
//                }

//                action":"UPDATE",
//"type":"add_friend",
//"sessionId":"sessionid",
//"userIdentity":"userIdentity",
//"packetId":"packetId"      
                String pak_id = PackageActions.create_packed_id_for(SocketConstants.ADD_FRIEND);
                JsonFields js_fields = new JsonFields();
                js_fields.setAction(SocketConstants.UPDATE);
                js_fields.setType(SocketConstants.ADD_FRIEND);
                js_fields.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                js_fields.setPacketId(pak_id);
                js_fields.setUserIdentity(friend_user_id2);
                //   pak_action.sendPackageAsClass(js_fields, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT);
                PackageActions.sendPacketAsString(js_fields);

            }
        });

        add_action.setToolTipText(text + "(" + friend_user_id2 + ")");
        add_action.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rowPanel.add(add_action);
        JPanel container2 = new JPanel(new GridLayout(3, 1, 0, 0));
        container2.setBounds(margin_left, 0, 200, 33);
        container2.setLayout(new BoxLayout(container2, BoxLayout.Y_AXIS));
        container2.setBackground(null);
        JButton button = DesignClasses.create_button_no_image_with_font_size_width(friend_user_id2, 0, 10, 150, 13, 0);
        container2.add(button);
        JLabel whatLable = new JLabel(text + ", " + country2);
        whatLable.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 10));
        whatLable.setForeground(DefaultSettings.text_color2);
        container2.add(whatLable);
        rowPanel.setBackground(null);
        rowPanel.add(container2);
        rowPanel.add(DesignClasses.create_bottom_border(margin_left, 35, 250));
        create_panel = rowPanel;

    }
}
