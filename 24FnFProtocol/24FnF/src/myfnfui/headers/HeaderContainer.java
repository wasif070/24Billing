/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.headers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author user
 */
public class HeaderContainer {

    public static HeaderContainer headersContainer;

    public static HeaderContainer getInstance() {
        if (headersContainer == null) {
            headersContainer = new HeaderContainer();
        }
        return headersContainer;
    }
    private Map<String, String> headers = new ConcurrentHashMap<String, String>();

    public Map<String, String> getHeaders() {
         return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
