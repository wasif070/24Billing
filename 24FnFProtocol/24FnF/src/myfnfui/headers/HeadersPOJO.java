/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.headers;

import java.util.ArrayList;
import myfnfui.FeedBackFields;

/**
 *
 * @author user
 */
public class HeadersPOJO extends FeedBackFields {

    private ArrayList<HeaderDTO> headerValues = new ArrayList<HeaderDTO>();

    public ArrayList<HeaderDTO> getHeaderValues() {
        return headerValues;
    }

    public void setHeaderValues(ArrayList<HeaderDTO> headerValues) {
        this.headerValues = headerValues;
    }
}
