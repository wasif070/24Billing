/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myfnfui.headers;

/**
 *
 * @author user
 */
public class HeaderDTO {
    private String sipHeader;
    private String _24fnfHeader;

    public String getSipHeader() {
        return sipHeader;
    }

    public void setSipHeader(String sipHeader) {
        this.sipHeader = sipHeader;
    }

    public String get24fnfHeader() {
        return _24fnfHeader;
    }

    public void set24fnfHeader(String _24fnfHeader) {
        this._24fnfHeader = _24fnfHeader;
    }
}
