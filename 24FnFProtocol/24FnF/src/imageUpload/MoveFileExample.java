/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imageUpload;

/**
 *
 * @author user
 */
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class MoveFileExample {

//    public static void main(String[] args) {
//
//
//
//        File afile = new File("E:\\Afile.txt");
//        File bfile = new File("src\\Afile.txt");
//        copyifle(afile, bfile);
//    }
    public static void copyifle(File src, File dest) {
        InputStream inStream = null;
        OutputStream outStream = null;

        try {
            inStream = new FileInputStream(src);
            outStream = new FileOutputStream(dest);

            byte[] buffer = new byte[1024];

            int length;
            //copy the file content in bytes 
            while ((length = inStream.read(buffer)) > 0) {

                outStream.write(buffer, 0, length);

            }

            inStream.close();
            outStream.close();

            //delete the original file
            //   afile.delete();

          //  System.out.println("File is copied successful!");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}