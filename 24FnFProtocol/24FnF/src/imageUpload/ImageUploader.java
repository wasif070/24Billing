/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imageUpload;

import helpers.HelperMethods;
import helpers.MyFnFSettings;
import imageDownload.DownLoaderHelps;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import myfnfui.JsonFields;
import myfnfui.ProfileAddUpdate;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author faizahmed
 */
public class ImageUploader {

    PackageActions pak_action = new PackageActions();

    public void uploader_method(String image_path) {
        HttpURLConnection conn = null;
        BufferedReader br = null;
        DataOutputStream dos = null;
        DataInputStream inStream = null;

        InputStream is = null;
        OutputStream os = null;
        boolean ret = false;
        String StrMessage = "";
        // String exsistingFileName = "E:\\Flamma Again\\24FNF backups\\Released_ source_codes\\24FnF\\src\\images\\logo_big.png";
        String exsistingFileName = image_path;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        DownLoaderHelps dHelps = new DownLoaderHelps();

        int bytesRead, bytesAvailable, bufferSize;

        byte[] buffer;

        int maxBufferSize = 1 * 1024 * 1024;


        String responseFromServer = "";

        //   String urlString = "http://192.168.1.101:8080/myfnf/rest/ImageUploadHandler";
        String urlString = "http://24fnf.com/rest/ImageUploadHandler";
//http://myfnf.com/rest/ImageUploadHandler
        try {
            //------------------ CLIENT REQUEST  

            FileInputStream fileInputStream = new FileInputStream(new File(exsistingFileName));

            // open a URL connection to the Servlet   

            URL url = new URL(urlString);


            // Open a HTTP connection to the URL  

            conn = (HttpURLConnection) url.openConnection();

            // Allow Inputs  
            conn.setDoInput(true);

            // Allow Outputs  
            conn.setDoOutput(true);

            // Don't use a cached copy.  
            conn.setUseCaches(false);

            // Use a post method.  
            conn.setRequestMethod("POST");

            conn.setRequestProperty("Connection", "Keep-Alive");

            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

            dos = new DataOutputStream(conn.getOutputStream());
//
//            dos.writeBytes(twoHyphens + boundary + lineEnd);
//            dos.writeBytes("Content-Disposition: form-data; name=\"upload\";"
//                    + " filename=\"" + exsistingFileName + "\"" + lineEnd);
//            dos.writeBytes(lineEnd);

            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"sessionId\"" + lineEnd + lineEnd + MyFnFSettings.LOGIN_SESSIONID + lineEnd);
            dos.writeBytes(twoHyphens + boundary + lineEnd);
            dos.writeBytes("Content-Disposition: form-data; name=\"uploadedFile\";filename=\"" + exsistingFileName + "\"" + lineEnd);
            dos.writeBytes(lineEnd);

            // create a buffer of maximum size  

            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, maxBufferSize);
            buffer = new byte[bufferSize];

            // read file and write it into form...  

            bytesRead = fileInputStream.read(buffer, 0, bufferSize);

            while (bytesRead > 0) {
                dos.write(buffer, 0, bufferSize);
                bytesAvailable = fileInputStream.available();
                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
            }

            // send multipart form data necesssary after file data...  

            dos.writeBytes(lineEnd);
            dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

            // close streams  

            fileInputStream.close();
            dos.flush();
            dos.close();


        } catch (MalformedURLException ex) {
            //  System.out.println("From ServletCom CLIENT REQUEST:" + ex);
        } catch (IOException ioe) {
            HelperMethods.create_confrim_panel_no_cancel("Cann not upload image", 50);
            // System.out.println("From ServletCom CLIENT REQUEST:" + ioe);
        }


        //------------------ read the SERVER RESPONSE  


        try {
//            if (HelperMethods.takeShot) {
//                try {
//                    HelperMethods.takeShot = false;
//                    HelperMethods.captureStream.stop();
//                    // profile_repaint();
//                } catch (CaptureException ex) {
//                    Logger.getLogger(ProfileAddUpdate.class.getName()).log(Level.SEVERE, null, ex);
//                }
//
//            }
            inStream = new DataInputStream(conn.getInputStream());
            String str;
            while ((str = inStream.readLine()) != null) {
                JsonFields js = HelperMethods.response_json(str);
                if (js.getSuccess()) {

                    String previous_image = HelperMethods.getImageName_from_url(MyFnFSettings.userProfile.getProfileImage());

                    File f_pre = new File(dHelps.getDestinationFolder() + "\\" + previous_image);
                    if (f_pre.exists()) {
                        f_pre.delete();
                    }

                    String new_image = HelperMethods.getImageName_from_url(js.getProfileImage());
                    MyFnFSettings.userProfile.setProfileImage(js.getProfileImage());
                    File trgDir = new File(exsistingFileName);
                    File srcDir = new File("/" + dHelps.getDestinationFolder());
                    //   System.out.println(srcDir);
                    File afile = new File(exsistingFileName);
                    File bfile = new File(MyFnFSettings.temp_image_folder + "\\" + new_image);
                    MoveFileExample.copyifle(afile, bfile);
                    ProfileAddUpdate profile_update = new ProfileAddUpdate();
                    profile_update.profile_repaint();
//                    if (!SetDynamicFriend.repaint_friendlist) {
//                        if (ProfileAddUpdate.getProfileUpdateObject() != null) {
//                           
//                        }
//                    }
                    //pak_acti on.store_received_msg_and_notify(SocketConstants.PROFILE_IMAGE_TEXT, js.getProfileImage());
                } else {
                    HelperMethods.create_confrim_panel_no_cancel(js.getMessage(), 50);
                }
            }
            inStream.close();

        } catch (IOException ioex) {
            HelperMethods.create_confrim_panel_no_cancel("Cann not upload image", 50);
            //   System.out.println("From (ServerResponse): " + ioex);

        }


    }
//    public void copyImageFiles(File sourceFile, File destinationDir) throws IOException {
//        System.out.println(sourceFile);
//        FileInputStream fis = new FileInputStream(sourceFile);
//        FileOutputStream fos = new FileOutputStream(destinationDir);
//        FileChannel srcChannel = fis.getChannel();
//        FileChannel destChannel = fos.getChannel();
//        srcChannel.transferTo(0, sourceFile.length(), destChannel);
//        srcChannel.close();
//        destChannel.close();
//        fis.close();
//        fos.close();
//    }
}
