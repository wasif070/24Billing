/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package imageDownload;

import helpers.MyFnFSettings;

/**
 *
 * @author user
 */
public class DownLoaderHelps {

    private String destinationFolder = MyFnFSettings.temp_image_folder + "\\";
    private String user_images_location = MyFnFSettings.temp_image_folder + "/";

    public String getUser_images_location() {
        return user_images_location;
    }

    public void setUser_images_location(String user_images_location) {
        this.user_images_location = user_images_location;
    }

    public String getDestinationFolder() {
        return destinationFolder;
    }

    public void setDestinationFolder(String destinationFolder) {
        this.destinationFolder = destinationFolder;
    }
}
