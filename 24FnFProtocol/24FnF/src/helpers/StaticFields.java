/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author FaizAhmed
 */
public class StaticFields {

    public static final String SAVED_USER_NAME = "saved_user";
    public static final String SAVED_PASSWORD = "saved_password";
    public static final String AUTO_START = "auto_start";
    public static final String SIGN_IN_AUTOMATICALLY = "sign_in_automatically";
    /*fields in API*/
    public static final String GENDER = "gender";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";
    public static final String USER_IDENTITY = "userIdentity";
    public static final String COUNTRY = "country";
    public static final String EMAIL = "email";
    public static final String MOBILE_PHONE = "mobilePhone";
    public static final String PARAM_DAILING_CODE = "mobilePhoneDialingCode";
    public static final String PASSWORD = "password";
    public static final String ACTION_NAME = "actionName";
    public static final String SUCCESS = "success";
    public static final String SWITCH_IP = "switchIp";
    public static final String SWITCH_PORT = "switchPort";
    public static final String SESSION_ID = "sessionId";
    public static final String PRESENCE = "presence";
    public static final String FRIEND_SHIP_STATUS = "friendShipStatus";
    public static final String Whats_In_Your_Mind = "WhatsInYourMind";
    /* Text user for different Purpose(Label,TextFields)*/
    public static final String FIRST_NAME_TEXT = "First Name";
    public static final String LAST_NAME_TEXT = "Last Name";
    public static final String MY_FNF_NAME_TEXT = "24FnF ID";
    public static final String EMAIL_TEXT = "Email";
    public static final String PHONE_NO_TEXT = "Phone No";
    public static final String GENDER_TEXT = "Gender";
    public static final String COUNTRY_TEXT = "Country";
    public static final String PASSWORD_TEXT = "Password";
    public static final String CONF_PASSWORD_TEXT = "Retype Password";
    public static final String COUNTRY_CODE_TEXT = "Country Prefix";
    public static final String Whats_In_Your_Mind_TEXT = "Whats on Your Mind";
    public static final String CHANGE_PASSWORD_TEXT = "Change Password";
}
