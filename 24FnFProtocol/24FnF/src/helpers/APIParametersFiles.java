/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author FaizAhmed
 */
public class APIParametersFiles {

    public static final String DIR_contactaddservice = "contactaddservice";
    public static final String DIR_contactDeleteService = "contactDeleteService";
    public static final String DIR_contactList = "contactList";
    public static final String DIR_contactSearchService = "contactSearchService";
    public static final String DIR_userAddEditService = "userAddEditService";
    public static final String DIR_userAuth = "userAuth";
    public static final String DIR_userAuth_without_contacts = "userAuth_without_contacts";
    public static final String DIR_userIdentityAvailable = "userIdentityAvailable";
    public static final String DIR_changePassword = "changePassword";
    public static final String DIR_presenceService = "presenceService";
    public static final String DIR_shareWhatsInYourMind = "shareWhatsInYourMind";
    public static final String DIR_logout = "logout";
    public static final String DIR_acceptFriendRequest = "acceptFriendRequest";
    public static final String DIR_updateUserProfileByFields = "updateUserProfileByFields";
    //parameters
    public static final String PARAM_sessionId = "sessionId";
    public static final String PARAM_userIdentity = "userIdentity";
    public static final String PARAM_searchparam = "searchparam";
    public static final String PARAM_user_data = "user_data";
    public static final String PARAM_user_name = "user_name";
    public static final String PARAM_password = "password";
    public static final String PARAM_oldPass = "oldPass";
    public static final String PARAM_newPass = "newPass";
    public static final String PARAM_WhatsInYourMind = "WhatsInYourMind";
    //PRESENCE & STATUS
    public static final Integer PRESENCE_ONLINE = 2;
    public static final Integer PRESENCE_OFFLINE = 1;
    public static final Integer FRIENDSHIP_STATUS_ACCEPTED = 1;
    public static final Integer FRIENDSHIP_STATUS_INCOMMING = 2;
    public static final Integer FRIENDSHIP_STATUS_SEND = 3;
    /*change 5-3-2013 4.09PM*/
}
