/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicComboBoxUI;
import myfnfui.CoboboxWithSearch;
import myfnfui.LightScrollPane;
import myfnfui.MyCombobox;
import org.omg.PortableInterceptor.LOCATION_FORWARD;

/**
 *
 * @author FaizAhmed
 */
public class DesignClasses {

    // private final Color selectionBackground = new Color(240, 200, 200);
    public static JTextField makeTextField(String text, int left, int top, int width) {

        JTextField textField = new JTextField(text);
        textField.setBackground(DefaultSettings.textFieldBackGroundColor);
        textField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, DefaultSettings.DEFAULT_FONT));
        textField.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, DefaultSettings.blue_border_color));
        textField.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        //   textField.setForeground(Color.GRAY);
        //   textField.setMargin(new Insets(8, 10, 0, 5));

        //  textField.set
        textField.setBorder(BorderFactory.createCompoundBorder(
                textField.getBorder(),
                BorderFactory.createEmptyBorder(2, 5, 5, 5)));
        return textField;
    }

    public static JPasswordField makeJPasswordField(String text, int left, int top) {

        JPasswordField textField = new JPasswordField(text);
        //  textField.setForeground(Color.GRAY);
        textField.setBackground(DefaultSettings.textFieldBackGroundColor);
        textField.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 14));
        textField.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, DefaultSettings.blue_border_color));
        textField.setBounds(left, top, DefaultSettings.textBoxWidth, DefaultSettings.texBoxHeight);
        textField.setBorder(BorderFactory.createCompoundBorder(
                textField.getBorder(),
                BorderFactory.createEmptyBorder(2, 5, 5, 5)));
        return textField;
    }

//    public static JLabel makeJLabel(String text, int left, int top, int width, Color forgroundcolor) {
//
//        JLabel jlble = new JLabel(text);
//        jlble.setBackground(null);
//        jlble.setForeground(forgroundcolor);
//        jlble.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 11));
//        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
//        return jlble;
//    }
//    public static JLabel makeJLabelCenterAling(String text, int left, int top, int width, Color forgroundcolor) {
//
//        JLabel jlble = new JLabel(text, JLabel.CENTER);
//        jlble.setBackground(null);
//        jlble.setForeground(forgroundcolor);
//        jlble.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
//        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
//        return jlble;
//    }
    public static JLabel makeJLabelCenterAling_with_font_size_width(String text, int left, int top, int width, int font_size, int font_width) {

        JLabel jlble = new JLabel(text, JLabel.CENTER);
        jlble.setBackground(null);
        jlble.setFont(new Font(Font.SANS_SERIF, font_width, font_size)); // Font.PLAIN=0, Font.Bold=1 Font.italic=2
        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        return jlble;
    }

    public static JLabel makeJLabelCenterAling_with_font_size_width_color(String text, int left, int top, int width, int font_size, int font_width, Color forgroundcolor) {

        JLabel jlble = new JLabel(text, JLabel.CENTER);
        jlble.setBackground(null);
        jlble.setFont(new Font(Font.SANS_SERIF, font_width, font_size)); // Font.PLAIN=0, Font.Bold=1 Font.italic=2
        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        jlble.setForeground(forgroundcolor);
        return jlble;
    }

    public static JLabel makeJLabel_with_background_font_size_width_color(String text, int left, int top, int width, int height, int font_size, int font_width, Color forgroundcolor, Color bg_color, int alingment) {

        JLabel jlble = new JLabel(text, alingment);
        jlble.setOpaque(true);
        jlble.setBackground(bg_color);
        jlble.setFont(new Font(Font.SANS_SERIF, font_width, font_size)); // Font.PLAIN=0, Font.Bold=1 Font.italic=2
        jlble.setBounds(left, top, width, height);
        jlble.setForeground(forgroundcolor);
        return jlble;
    }

    public static JLabel make_label_for_field_text(String text, int left, int top, int width) {
        Color label_color = Color.WHITE;
        int label_font_width = 1;
        int label_font_size = 11;
        int label_font_aling = 2;
        Color label_text_color = DefaultSettings.text_color1;
        JLabel jlble = makeJLabel_with_background_font_size_width_color(text, left, top, width, DefaultSettings.texBoxHeight, label_font_size, label_font_width, label_text_color, label_color, label_font_aling);
        return jlble;
    }

    public static JComboBox create_ComboBox_from_array(String items[], int left, int top, int width) {
        UIManager.put("ComboBox.selectionBackground", new ColorUIResource(new Color(0x6699FF)));//new Color(0x8688F5)
        UIManager.put("ComboBox.selectionForeground", new ColorUIResource(Color.WHITE));
        //   UIManager.put("ComboBox.setBorder", new ColorUIResource(Color.WHITE));
        //JComboBox cm = new JComboBox(items);
        MyCombobox cm = new MyCombobox(items);

        cm.setWide(true);

        cm.setBackground(DefaultSettings.textFieldBackGroundColor);
        cm.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, DefaultSettings.DEFAULT_FONT));
        cm.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        cm.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, new Color(0x08, 0xAB, 0xED)));
        cm.setUI(new BasicComboBoxUI() {
            @Override
            protected JButton createArrowButton() {
                JButton btn = new JButton();
                btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/combo_arrow.png")));
                btn.setBackground(Color.white);
                //  btn.setBorder(new LineBorder(new Color(0x08, 0xAB, 0xED), 1));
                btn.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, new Color(0x08, 0xAB, 0xED)));
                return btn;
            }
        });
        new CoboboxWithSearch(cm);
        // cm.setForeground(Color.GRAY);
        return cm;
    }

//    public static JComboBox create_ComboBox_from_array_dial_pad(String items[], int left, int top, int width) {
//        UIManager.put("ComboBox.selectionBackground", new ColorUIResource(new Color(0x6699FF)));//new Color(0x8688F5)
//        UIManager.put("ComboBox.selectionForeground", new ColorUIResource(Color.WHITE));
//        //   UIManager.put("ComboBox.setBorder", new ColorUIResource(Color.WHITE));
//        //JComboBox cm = new JComboBox(items);
//        MyCombobox cm = new MyCombobox(items);
//
//        cm.setWide(true);
//        cm.setBackground(DefaultSettings.textFieldBackGroundColor);
//        cm.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, DefaultSettings.DEFAULT_FONT));
//        cm.setBounds(left, top, width, DefaultSettings.texBoxHeight);
//        cm.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 0, new Color(0x08, 0xAB, 0xED)));
//        cm.setUI(new BasicComboBoxUI() {
//            @Override
//            protected JButton createArrowButton() {
//                JButton btn = new JButton();
//                btn.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/combo_arrow.png")));
//                btn.setBackground(Color.white);
//                //  btn.setBorder(new LineBorder(new Color(0x08, 0xAB, 0xED), 1));
//                btn.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 1, new Color(0x08, 0xAB, 0xED)));
//                return btn;
//            }
//        });
//        AutoCompletion.enable(cm);
//        // cm.setForeground(Color.GRAY);
//        return cm;
//    }
    public static JPanel create_bottom_border(int left, int top, int width) {
        JPanel row2 = new JPanel();
        // row2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.red));
        row2.setBounds(left, top, width, 1);
        row2.setBackground(null);
        row2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, DefaultSettings.friend_list_border_color));
        return row2;
    }

    public static final JLabel makeJLabel_text_color1(String text, int left, int top, int width, int font_size) {

        JLabel jlble = new JLabel(text);
        jlble.setBackground(null);
        jlble.setForeground(DefaultSettings.text_color1);
        jlble.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, font_size));
        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        return jlble;
    }

    public static final JLabel makeJLabel_with_text_color(String text, int left, int top, int width, int font_size, int font_width, Color text_dd) {

        JLabel jlble = new JLabel(text);
        jlble.setBackground(null);
        jlble.setForeground(text_dd);
        jlble.setFont(new Font(Font.SANS_SERIF, font_width, font_size));
        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        return jlble;
    }

    public static final JLabel makeJLabel_text_color_normal(String text, int left, int top, int width, int font_size) {

        JLabel jlble = new JLabel(text);
        jlble.setBackground(null);
        jlble.setForeground(DefaultSettings.text_color_normal);
        jlble.setFont(new Font(Font.SANS_SERIF, Font.BOLD, font_size));
        jlble.setBounds(left, top, width, DefaultSettings.texBoxHeight);
        return jlble;
    }

    public static JButton create_accept_buttons(int left, int top) {

        JButton btn = new JButton();
        String image_source = StaticImages.IMAGE_ACCEPT;
        if (image_source != null) {
            URL imgURL = new Object() {
            }.getClass().getClassLoader().getResource(image_source);
            btn.setIcon(new ImageIcon(imgURL));
        }
        btn.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        btn.setBackground(null);
        btn.setBorder(null);
        btn.setBounds(left, top, 52, 20);
        btn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return btn;
    }

    public static JButton create_mobile_call_buttons(int left, int top) {

        JButton btn = new JButton();
        String image_source = StaticImages.IMAGE_MOBILE_CALL;
        if (image_source != null) {
            URL imgURL = new Object() {
            }.getClass().getClassLoader().getResource(image_source);
            btn.setIcon(new ImageIcon(imgURL));
        }
        btn.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        btn.setBackground(null);
        btn.setBorder(null);
        btn.setBounds(left, top, 72, 25);
        btn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return btn;
    }

    public static JButton create_pin2pin_call_buttons(int left, int top) {

        JButton btn = new JButton();
        String image_source = StaticImages.IMAGE_P2P;
        if (image_source != null) {
            URL imgURL = new Object() {
            }.getClass().getClassLoader().getResource(image_source);
            btn.setIcon(new ImageIcon(imgURL));
        }
        btn.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY));
        btn.setBackground(null);
        btn.setBorder(null);
        btn.setBounds(left, top, 72, 25);
        btn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return btn;
    }

    public static JButton create_contact_image_button(URL imgURL, String hover_text) {
        JButton buttonContact = new JButton();
        ImageIcon contactImg = new ImageIcon(imgURL);
        buttonContact.setIcon(contactImg);
        buttonContact.setVerticalAlignment(SwingConstants.BOTTOM);  // of text and icon
        buttonContact.setHorizontalAlignment(SwingConstants.RIGHT); // of text and icon
        buttonContact.setHorizontalTextPosition(SwingConstants.LEFT); // of text relative to icon
        buttonContact.setVerticalTextPosition(SwingConstants.TOP);    // of text relative to icon
        buttonContact.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 15));
        buttonContact.setBackground(new Color(231, 240, 248));
        buttonContact.setForeground(Color.BLUE);
        buttonContact.setPreferredSize(new Dimension(115, 115));
        buttonContact.setToolTipText(hover_text);
        buttonContact.setOpaque(false);
        buttonContact.setBorder(BorderFactory.createLineBorder(Color.WHITE));
        return buttonContact;
    }

    public static JLabel create_image_label(int left, int top, int width, int height, String image_source) {
        JLabel label = new JLabel();
        ImageIcon img = null;
        URL imgURL = null;
        imgURL = new Object() {
        }.getClass().getClassLoader().getResource(image_source);
        if (imgURL != null) {
            img = new ImageIcon(imgURL);
            label.setIcon(img);
        }
        //  label.setOpaque(true);  // needed for JLabel to show the background color
        label.setBackground(null);  // light blue
        label.setOpaque(false);                 // foreground text color
        label.setBounds(left, top, width, height);
        label.setBorder(null);
        return label;
    }

    public static JLabel create_image_label_with_border(int left, int top, int width, int height, String image_source) {
        JLabel label = new JLabel();
        ImageIcon img = null;
        URL imgURL = null;
        imgURL = new Object() {
        }.getClass().getClassLoader().getResource(image_source);
        if (imgURL != null) {
            img = new ImageIcon(imgURL);
            label.setIcon(img);
        }
        label.setOpaque(true);  // needed for JLabel to show the background color
        label.setBackground(Color.white);  // light blue
        label.setOpaque(false);                        // foreground text color
        label.setBounds(left, top, width, height);
        label.setBorder(DefaultSettings.image_border_color);
        return label;
    }

    public static String get_male_or_female_image(String gender) {
        if (gender.equals("Male")) {
            return StaticImages.IMAGE_MALE;
        } else {
            return StaticImages.IMAGE_FEMALE;
        }
    }

    public static String get_scaled_male_or_female_image(String gender) {
        String location = "";
        if (gender.equalsIgnoreCase("Male")) {
            location = MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.IMAGE_MALE_SCALED;
        } else {
            location = MyFnFSettings.RESOURCE_FOLDER + File.separator + StaticImages.IMAGE_FEMALE_SCALED;
        }
        return location;
    }

    public static ImageIcon return_image(String image_source) {
        URL imgURL = null;
        ImageIcon img = null;
        try {
            imgURL = new Object() {
            }.getClass().getClassLoader().getResource(image_source);
            img = new ImageIcon(imgURL);
        } catch (Exception e) {
        }

        return img;
    }

    public static JButton create_button_with_image_fixed_location(String main_image, String hover_image, String hover_text, int left, int top, int width, int height) {
        JButton buttonContact = new JButton();
        buttonContact.setBackground(null);
        buttonContact.setBounds(left, top, width, height);
        buttonContact.setIcon(new ImageIcon(new Object() {
        }.getClass().getResource(main_image)));
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(true);
        buttonContact.setRolloverIcon(new ImageIcon(new Object() {
        }.getClass().getResource(hover_image)));
        buttonContact.setToolTipText(hover_text);
        buttonContact.setOpaque(false);
        return buttonContact;
    }

    public static JButton create_button_no_image_with_font_size_width(String text, int left, int top, int width, int font_size, int font_width) {

        JButton jbtn = new JButton(text);
        jbtn.setFont(new Font(Font.SERIF, font_width, font_size));
        jbtn.setBorder(null);
        jbtn.setForeground(DefaultSettings.text_color1);
        jbtn.setBackground(null);
        jbtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return jbtn;
    }

    public static JButton create_button_with_font_size_width(String text, int left, int top, int width, int height, int font_size, int font_width) {

        JButton jbtn = new JButton(text);
        jbtn.setFont(new Font(Font.SERIF, font_width, font_size));
        jbtn.setBounds(left, top, width, height);
        jbtn.setBorder(null);
        jbtn.setHorizontalTextPosition(SwingConstants.LEFT);
        jbtn.setHorizontalAlignment(SwingConstants.LEFT);
        jbtn.setBackground(null);
        jbtn.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        return jbtn;
    }

    public static JButton create_image_button_with_text(String main_image, String hover_image, String button_text, int font_size, int font_width, int left, int top, int width, int height) {

        JButton buttonContact = new JButton();
        buttonContact.setBackground(null);
        buttonContact.setBounds(left, top, width, height);
        buttonContact.setIcon(return_image(main_image));
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(true);
        buttonContact.setRolloverIcon(return_image(hover_image));
        buttonContact.setLayout(new BorderLayout());
        JLabel label = makeJLabelCenterAling_with_font_size_width_color(button_text, 0, 0, 0, font_size, font_width, Color.WHITE);
        buttonContact.add(label);

        buttonContact.setOpaque(false);
        return buttonContact;
    }

    public static JButton create_image_button_with_text(String main_image, String hover_image, String button_text, int font_size, int font_width, int left, int top, int width, int height, Color font_clr) {

        JButton buttonContact = new JButton();
        buttonContact.setBackground(null);
        buttonContact.setBounds(left, top, width, height);
        buttonContact.setIcon(return_image(main_image));
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(true);
        buttonContact.setRolloverIcon(return_image(hover_image));
        buttonContact.setLayout(new BorderLayout());
        JLabel label = makeJLabelCenterAling_with_font_size_width_color(button_text, 0, 0, 0, font_size, font_width, font_clr);
        buttonContact.add(label);

        buttonContact.setOpaque(false);
        return buttonContact;
    }

    public static void change_image(String main_image, JButton button) {
        button.setIcon(return_image(main_image));
    }

    public static JButton create_image_button_with_text_color(String main_image, String hover_image, String button_text, int font_size, int font_width, int left, int top, int width, int height, Color clor) {

        JButton buttonContact = new JButton();
        buttonContact.setBackground(null);
        buttonContact.setBounds(left, top, width, height);
        buttonContact.setIcon(return_image(main_image));
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(true);
        buttonContact.setRolloverIcon(return_image(hover_image));
        buttonContact.setLayout(new BorderLayout());
        buttonContact.add(makeJLabelCenterAling_with_font_size_width_color(button_text, 0, 0, 0, font_size, font_width, clor));

        buttonContact.setOpaque(false);
        return buttonContact;
    }

    public static final JButton create_image_button_with_out_bounds(String main_image, String hover_image) {

        JButton buttonContact = new JButton();
        buttonContact.setBackground(null);
        //   buttonContact.setBounds(left, top, width, height);
        buttonContact.setIcon(return_image(main_image));
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(true);
        buttonContact.setRolloverIcon(return_image(hover_image));
        //   buttonContact.setLayout(new BorderLayout());
        // buttonContact.add(makeJLabelCenterAling_with_font_size_width_color(button_text, 0, 0, 0, font_size, font_width, Color.WHITE));

        buttonContact.setOpaque(false);
        return buttonContact;
    }

    public static final JButton create_image_button_with_text_scale1(String text, int left, int top) {
        return create_image_button_with_text(StaticImages.IMAGE_Scale_1, StaticImages.IMAGE_Scale_1_h, text, 9, 1, left, top, 42, 20);
    }

    public static final JButton create_image_button_with_text_scale6(String text, int left, int top, Color font_color) {
        return create_image_button_with_text(StaticImages.Scale_6, StaticImages.Scale_6, text, 9, 1, left, top, 149, 35, font_color);
    }

    public static JPanel create_colored_bottom_border(int left, int top, int width, Color color) {
        JPanel row2 = new JPanel();
        // row2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 1, Color.red));
        row2.setBounds(left, top, width, 1);
        row2.setBackground(null);
        row2.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, color));
        return row2;
    }

    public static JTextArea create_text_area(String text, int left, int top, int width, int height) {
        JTextArea row2 = new JTextArea(text);
        row2.setBounds(left, top, width, height);
        row2.setBorder(null);
        row2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, 11));
        row2.setForeground(DefaultSettings.text_color_light_blue);
        row2.setWrapStyleWord(true);
        row2.setLineWrap(true);

//        row2.setBorder(BorderFactory.createCompoundBorder(
//                row2.getBorder(),
//                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        return row2;
    }

    public static LightScrollPane create_text_area_with_scroll_panel(String text, int left, int top, int width, int height) {
        JTextArea row2 = create_text_area(text, left, top, 10, height);
        LightScrollPane ligtScroll = new LightScrollPane(row2);
        row2.setEditable(false);
        ligtScroll.setBounds(left, top, width, height);
        ligtScroll.setBorder(DefaultSettings.border_light_blue);
        ligtScroll.setBorder(BorderFactory.createCompoundBorder(
                ligtScroll.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        return ligtScroll;
    }

    public static BufferedImage scaleImage(int w, int h, BufferedImage img) throws Exception {
        BufferedImage bi;
        bi = new BufferedImage(w, h, BufferedImage.TRANSLUCENT);
        Graphics2D g2d = (Graphics2D) bi.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.addRenderingHints(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY));
        g2d.drawImage(img, 0, 0, w, h, null);
        g2d.dispose();
        return bi;
    }

    public static boolean file_exists(String file) {
        // System.out.println("File being checked: " + file);
        return ((file.length()) > 0 && (new File(file).exists()));
    }

    public static JTextArea create_text_area(String text, int left, int top, int width, int height, int font_size) {
        JTextArea row2 = new JTextArea(text);
        row2.setBounds(left, top, width, height);
        row2.setBorder(null);
        row2.setFont(new Font(Font.SANS_SERIF, Font.PLAIN, font_size));
        row2.setForeground(DefaultSettings.text_color_light_blue);
        row2.setWrapStyleWord(true);
        row2.setLineWrap(true);

//        row2.setBorder(BorderFactory.createCompoundBorder(
//                row2.getBorder(),
//                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        return row2;
    }

    public static JButton create_button_with_text_color_bgColor(String button_text, int font_size, int font_width, int left, int top, int width, int height, Color clor, Color bgColor) {

        JButton buttonContact = new JButton(button_text);
        buttonContact.setBackground(bgColor);
        buttonContact.setBounds(left, top, width, height);
        buttonContact.setBorder(null);
        buttonContact.setRolloverEnabled(false);
        buttonContact.setFont(new Font(Font.SANS_SERIF, font_width, font_size));
        buttonContact.setForeground(clor);
        buttonContact.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        // buttonContact.setLayout(new BorderLayout());
        //  buttonContact.setOpaque(false);
        return buttonContact;
    }
}
