package helpers;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

public class HttpRequest {

    private static int READ_TIME_OUT = 2500;

    // static int problemCounter = 0;
    public static synchronized String getJson(String p_url) {

        URL url;
        HttpURLConnection conn = null;
        BufferedReader rd = null;
        String line = null;
        String result = "";
        if (p_url.length() > 0) {
            try {
              //  long l=System.currentTimeMillis();
                
                url = new URL(p_url);
                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(READ_TIME_OUT);
                conn.setRequestMethod("GET");
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
              
                
              //  System.out.println(System.currentTimeMillis()-l);
                
                while ((line = rd.readLine()) != null) {
                    result += line;
                }
            } catch (Exception ex) {
                //System.out.println("HttpRequest.getJson()-->" + ex);
            } finally {
                if (conn != null) {
                    conn.disconnect();
                }
                try {
                    if (rd != null) {
                        rd.close();
                    }
                } catch (Exception ex) {
                }
            }
        }

        return result;
    }
}
