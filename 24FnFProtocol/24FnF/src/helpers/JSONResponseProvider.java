/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;

/**
 *
 * @author Ashraful
 */
public class JSONResponseProvider extends Thread {

    private int socket_timeout = 10000;//default 10 seconds
    private String request_url = null;
    protected JSONResponseListener listener;
    /** Whether it has been halted */
    boolean stop;
    /** Whether it is running */
    boolean is_running;

    /** Maximum time that the URL receiver can remain active after been halted (in milliseconds) */
    public JSONResponseProvider() {
        socket_timeout = 10000;//default 10 seconds
        this.stop = false;
        this.is_running = true;
    }

    /** Inits the JSONResponseProvider */
    private void init(JSONResponseListener listener, String request_url) {
        this.listener = listener;
        this.request_url = request_url;
        this.stop = false;
        this.is_running = true;
        socket_timeout = 10000;//default 10 seconds
    }

    private void init(JSONResponseListener listener, String request_url, int read_time_out) {
        this.listener = listener;
        this.request_url = request_url;
        this.stop = false;
        this.is_running = true;
        this.socket_timeout = read_time_out;
    }

    /** Creates a new JSONResponseProvider */
    public JSONResponseProvider(JSONResponseListener listener, String request_url) {
        init(listener, request_url);
        start();
    }

    /** Creates a new JSONResponseProvider */
    public JSONResponseProvider(JSONResponseListener listener, String request_url, int read_time_out) {
        init(listener, request_url, read_time_out);
        start();
    }

    /** Whether the service is running */
    public boolean isRunning() {
        return is_running;
    }

    /** Sets the maximum time that the JSONResponseProvider service can remain active after been halted */
    public void setSoTimeout(int timeout) {
        socket_timeout = timeout;
    }

    /** Gets the maximum time that the JSONResponseProvider service can remain active after been halted */
    public int getSoTimeout() {
        return socket_timeout;
    }

    /** Stops running */
    public void halt() {
        stop = true;
    }

    /** The main thread */
    public synchronized void run() {

        URL url;
        HttpURLConnection conn = null;
        InputStream is = null;
//        long start_time = System.currentTimeMillis();
        if (request_url.length() > 0) {
            try {
                url = new URL(request_url);
                URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
                url = uri.toURL();
                conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(socket_timeout);
                is = conn.getInputStream();

                StringBuilder str_bulder = new StringBuilder();
                int i = 0;
                while ((i = is.read()) != -1) {
                    str_bulder.append((char) i);
                }

//                byte[] buffer = new byte[is.available()];
//                is.read(buffer);

                if (str_bulder != null && str_bulder.length() > 0) {
                    if (listener != null) {
                        listener.onReceivedResponse(this, str_bulder.toString(), request_url);
                    }
                }
//                System.out.println(request_url+"-->" + (System.currentTimeMillis() - start_time));
            } catch (Exception ex) {
            } finally {
                try {
                    if (conn != null) {
                        conn.disconnect();
                    }
                } catch (Exception iox) {
                }
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException iox) {
                }
            }
        }
    }
}
