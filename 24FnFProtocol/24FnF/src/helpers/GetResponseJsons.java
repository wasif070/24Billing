/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import local.ua.GraphicalUA;
import local.ua.IncomingGraphicalUA;
import myfnfui.CreateNewAccount;
import myfnfui.InviteFrined;
import myfnfui.IpAddress;
import myfnfui.JsonFields;
import myfnfui.usedThreads.SetDynamicFriend;
import myfnfui.UdpTransport;
import myfnfui.groups.GroupsWindow;
import myfnfui.groups.TempGruoupContainer;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.tampClass.CommunicationPortMapping;

/**
 *
 * @author FaizAhmed
 */
public class GetResponseJsons extends HttpRequest {

    public static Gson jsonLib;
    PackageActions pakAction;
    //   public static final String API_HOST = "http://myfnf.com /rest/communicationPort?user_name=kjkkjlk&password=aaaaaa";

    public static String getApiHost(String userName, String password) {
        // System.out.println(userName);
        //  String url = "http://24fnf.com/rest/communicationPort?user_name=" + userName + "&password=" + password;
        String url = "";
        if (!SocketConstants.SOCKET_IP.equals("38.127.68.246")) {
            url = "http://" + SocketConstants.SOCKET_IP + ":8080/myfnf/rest/communicationPort?user_name=" + userName + "&password=" + password;
        } else {
            url = "http://24fnf.com/rest/communicationPort?user_name=" + userName + "&password=" + password;
        }
        //  System.out.println(url);
        return getJson(url);
    }

    public GetResponseJsons() {
        pakAction = new PackageActions();
        jsonLib = new GsonBuilder().serializeNulls().create();
    }

    public static int getComPort(String jsSting) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        CommunicationPortMapping cm = jsonLib.fromJson(jsSting, CommunicationPortMapping.class);
        //  System.out.println("Communication Port: " + cm.getCommunicationPort());
        return cm.getCommunicationPort();
    }

    public static String get_new_user_json(String prefix, String gender, String firstName, String lastName, String userIdentity, String country, String email, String mobilePhone, String password, String actionName) {
        String fullJson = "{\"mobilePhoneDialingCode\":\"" + prefix + "\",\"gender\":\"" + gender + "\",\"firstName\":\"" + firstName + "\",\"lastName\":\"" + lastName + "\",\"userIdentity\":\"" + userIdentity + "\",\"country\":\"" + country + "\",\"email\":\"" + email + "\",\"mobilePhone\":\"" + mobilePhone + "\",\"password\":\"" + password + "\",\"actionName\":\"" + actionName + "\"}";
        String url = MyFnFSettings.API_HOST + "userAddEditService?user_data=" + fullJson;
        //System.out.println(url);
        return getJson(url);
    }

    public static synchronized String get_login_json(String userIdentity, String password) {
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_userAuth + "?" + APIParametersFiles.PARAM_user_name + "=" + userIdentity + "&&" + APIParametersFiles.PARAM_password + "=" + password;
        // return url;
        // String url = MyFnFSettings.API_HOST + "userAddEditService?user_data=" + fullJson;
        return getJson(url);
    }

    public static void get_add_contact_json(String userIdentity, String sessionID, String searchString, InviteFrined inv_frnd) {
        String search_pak = MyFnFSettings.LOGIN_USER_ID + SocketConstants.CONTACT_SEARCH + System.currentTimeMillis();
        JsonFields fld = new JsonFields();
        fld.setAction(SocketConstants.REQUEST);
        fld.setType(SocketConstants.CONTACT_SEARCH);
        fld.setPacketId(search_pak);
        fld.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        fld.setSearchParam(searchString);
        //     PackageActions pakAction = new PackageActions();
        //    pakAction.sendP
        PackageActions.sendPacketAsString(fld);

//        //  http://192.168.1.110:8080/rest/contactSearchService?sessionId=gggg&searchparam=aa
//        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactSearchService + "?" + APIParametersFiles.PARAM_userIdentity + "=" + userIdentity + "&" + APIParametersFiles.PARAM_sessionId + "=" + sessionID + "&" + APIParametersFiles.PARAM_searchparam + "=" + searchString;
//        FriendSearchProvider fsp = new FriendSearchProvider();
//        fsp.setFriendList(url, searchString, inv_frnd);
    }

    public static String sent_add_contact_json(String userIdentity, String sessionID) {

        // http://192.168.1.110:8080/rest/contactaddservice?sessionId=gggg&userIdentity=userIdentity
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactaddservice + "?" + APIParametersFiles.PARAM_sessionId + "=" + sessionID + "&" + APIParametersFiles.PARAM_userIdentity + "=" + userIdentity;
        //System.out.println(url);
        //return url;
        return getJson(url);
    }

//    public static synchronized String get_friend_list(String session_id) {
//        //    http://192.168.1.110:8080/rest/contactList?sessionId=gggg
//        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactList + "?" + APIParametersFiles.PARAM_sessionId + "=" + session_id;
//        return getJson(url);
//    }
    public static String accept_friend_request(String session_id, String user_id) {
        //    http://192.168.1.110:8080/rest/acceptFriendRequest?sessionId=gggg&userIdentity=userIdentity
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_acceptFriendRequest + "?" + APIParametersFiles.PARAM_sessionId + "=" + session_id + "&" + APIParametersFiles.PARAM_userIdentity + "=" + user_id;
        //  return url;
        return getJson(url);
    }
//
//    public static synchronized void set_success_failure(String json_string) {
//        MyFnFSettings.map_success_fail = jsonLib.fromJson(json_string, MappingSuccessFailure.class);
//    }

//    public static synchronized String reject_delete_friend_request(String session_id, String user_id) {
//        //     http://192.168.1.110:8080/rest/contactDeleteService?sessionId=gggg&userIdentity=userIdentity
//        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactDeleteService + "?" + APIParametersFiles.PARAM_sessionId + "=" + session_id + "&" + APIParametersFiles.PARAM_userIdentity + "=" + user_id;
//        //System.out.println(url);
//        return getJson(url);
//    }
    public static synchronized Boolean reject_delete_friend_request_final(String user_id) {
        //     http://192.168.1.110:8080/rest/contactDeleteService?sessionId=gggg&userIdentity=userIdentity
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_contactDeleteService + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId() + "&" + APIParametersFiles.PARAM_userIdentity + "=" + user_id;
        //  return url;
        //System.out.println(url);
        String get_string = getJson(url);
        //System.out.println(get_string);

        if (get_string.length() > 0) {
            if (HelperMethods.check_string_contains_substring(get_string, "true")) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    public static synchronized String logout_user(String session_id) {
        String ret = "";
        SetDynamicFriend.runningFlag = false;

        MyFnFSettings.isAuthenticated = false;
        MyFnFSettings.isRegisterred = false;
        //    StoreMsgParser.stop = true;
        //   MessageChecker.stop = true;
        if (UdpTransport.getInstance() != null) {
            //    MyFnFSettings.userProfile = null;
            //  UdpTransport.getInstance().close_socket();
            // UdpTransport.udp_transport = null;
        }
        //  MessageChecker.stop = true;
        if (GraphicalUA.getStaticObject() != null) {
            GraphicalUA.getStaticObject().forceHangUp();
        }
        // System.out.println("*****************************dsf***************************");
        if (IncomingGraphicalUA.getStaticObject() != null) {
            //    System.out.println("*****************************dsf***************************");
            IncomingGraphicalUA.getStaticObject().forceHangUp();
        }
        if (GroupsWindow.groupWindow != null) {
            GroupsWindow.groupWindow = null;
        }
        //   TempGruoupContainer.getInstance().getGruopFriendContainer().
        TempGruoupContainer.getInstance().getGruopFriendContainer().clear();
        TempGruoupContainer.getInstance().getGroupMemberContainer().clear();

        FriendList.getInstance().getFriend_hash_map().clear();
        //   FriendList.getInstance().getFriend_hash_map().clear();
        String sigunout_pak = MyFnFSettings.LOGIN_USER_ID + SocketConstants.SIGN_OUT + System.currentTimeMillis();
        JsonFields fld = new JsonFields();
        fld.setAction(SocketConstants.AUTHENTICATION);
        fld.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        fld.setType(SocketConstants.SIGN_OUT);
        fld.setPacketId(sigunout_pak);
        // String response = new Gson().toJson(fld);
//        // System.err.println("Signout pak:" + response);
//        IpAddress address = new IpAddress(SocketConstants.SOCKET_IP);
//        PackageActions pakAction = new PackageActions();
        PackageActions.sendPacketAsString(fld);
        return ret;
    }
//http://192.168.1.110:8080/rest/updateUserProfileByFields?sessionId=226032gggg&firstName=individual&mobilePhoneDialingCode=+088

    public static String edit_profile_url(String session_id) {
        //http://192.168.1.110:8080/rest/updateUserProfileByFields?sessionId=226032gggg&firstName=individual&mobilePhoneDialingCode=+088
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_updateUserProfileByFields + "?" + APIParametersFiles.PARAM_sessionId + "=" + session_id + "&";
        return url;
    }

    public static synchronized boolean change_what_is_in_mind(String session_id, String text) {
        //   http://192.168.1.110:8080/rest/shareWhatsInYourMind?sessionId=gggg&WhatsInYourMind=jjjjj
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_shareWhatsInYourMind + "?" + APIParametersFiles.PARAM_sessionId + "=" + session_id + "&" + APIParametersFiles.PARAM_WhatsInYourMind + "=" + text;
        //System.out.println(url);
        String get_string = getJson(url);
        //System.out.println(get_string);

        if (get_string.length() > 0) {
            if (HelperMethods.check_string_contains_substring(get_string, "true")) {
                return true;
            } else {
                return false;
            }
        }

        return false;

    }

    public static synchronized boolean edit_profile_field(String param_name, String text) {
        try {
            //   http://192.168.1.110:8080/rest/updateUserProfileByFields?sessionId=gggg&firstName=individual&mobilePhoneDialingCode=+088
            String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_updateUserProfileByFields + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId() + "&" + param_name + "=" + text;
            //System.out.println(url);
            String get_string = getJson(url);
            //System.out.println(get_string);
            if (get_string.length() > 0) {
                if (HelperMethods.check_string_contains_substring(get_string, "true")) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
        }
        return false;

    }

    public static boolean change_password(String old_passowrd, String new_passowrd) {
        //   http://192.168.1.110:8080/rest/changePassword?sessionId=zzzz&oldPass=old&newPass=new
        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_changePassword + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId() + "&" + APIParametersFiles.PARAM_oldPass + "=" + old_passowrd + "&" + APIParametersFiles.PARAM_newPass + "=" + new_passowrd;
        //System.out.println(url);
        String get_string = getJson(url);
        if (get_string.length() > 0) {
            if (HelperMethods.check_string_contains_substring(get_string, "true")) {
                return true;
            } else {
                return false;
            }
        }
        return false;


    }

//    public static boolean isInternetReachable() throws MalformedURLException, IOException {
//        try {
//            //make a URL to a known source
//            URL url = new URL("http://www.google.com");
//
//            //open a connection to that source
//            HttpURLConnection urlConnect = (HttpURLConnection) url.openConnection();
//
//            //trying to retrieve data from the source. If there
//            //is no connection, this line will fail
//            Object objData = urlConnect.getContent();
//
//        } catch (UnknownHostException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            return false;
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            return false;
//        }
//        return true;
//    }
    public static void check_user_avaiability(String user_id, CreateNewAccount create_new) {
//        //   http://192.168.1.110:8080/rest/userIdentityAvailable?userId=zzzz
//        String url = MyFnFSettings.API_HOST + APIParametersFiles.DIR_userIdentityAvailable + "?userId=" + user_id;
//        UserNameProvider unp = new UserNameProvider();
//        unp.setUserAvailability(url, user_id, create_new);

        String search_pak = MyFnFSettings.LOGIN_USER_ID + SocketConstants.USER_ID_AVAILABLE + System.currentTimeMillis();
        JsonFields fld = new JsonFields();
        fld.setAction(SocketConstants.REQUEST);
        fld.setType(SocketConstants.USER_ID_AVAILABLE);
        //  fld.setType(SocketConstants.SIGN_OUT);
        fld.setPacketId(search_pak);
        fld.setUserIdentity(user_id);
        //fld.setSearchParam(searchString);
        //   PackageActions pakAction = new PackageActions();
        if (user_id.length() > 4) {
            PackageActions.sendPacketAsString(fld);
        }//.sendPackageAsClass(fld, SocketConstants.authIPAdress(), SocketConstants.SOCKET_PORT);
    }
}
