/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import javax.swing.JTextField;
import local.ua.GraphicalUA;
import local.ua.UserAgentProfile;
import org.zoolu.sip.provider.SipProvider;

/**
 *
 * @author faizahmed
 */
public class UiMethods {

    public void clear_selected_text(JTextField textfield) {
        String selected_text = textfield.getSelectedText();
        if (selected_text != null) {
            int start = textfield.getSelectionStart();
            int end = textfield.getSelectionEnd();
            String startText = textfield.getText().substring(0, start);
            String endText = textfield.getText().substring(end, textfield.getText().length());
            // System.out.println(startText + endText);
            textfield.setText(startText + endText);
        }
    }

    public void set_reset_defalut_text(JTextField textfield, String default_text, Boolean status) {
        if (status) {
            if (textfield.getText().toString().length() < 1) {
                textfield.setText(default_text);
                textfield.setForeground(DefaultSettings.disable_font_color);
            }
        } else {
            textfield.setText("");
            textfield.setForeground(null);
        }
    }

    public void call_calling_interface(String call_type, String mobile_no) {
        //  this.dispose();
        //UserAgentProfile pp = HelperMethods.get_all_for_a_profile(user_id);
        SipProvider instanceSipProvider = GraphicalUA.getSipProvider();
        UserAgentProfile new_user_profile = new UserAgentProfile();
        new_user_profile.setUserIdentity(MyFnFSettings.LOGIN_USER_ID);
        new_user_profile.setFirstName(MyFnFSettings.userProfile.getFirstName());
        new_user_profile.setLastName(MyFnFSettings.userProfile.getLastName());
        //  new_user_profile.setLastName(MyFnFSettings.userProfile.getLastName());
        new_user_profile.setCountry(MyFnFSettings.userProfile.getCountry());
        new_user_profile.setGender(MyFnFSettings.userProfile.getGender());
        new_user_profile.setMobilePhone(mobile_no);
        //new_user_profile.setMobilePhoneDialingCode(mobileCode);

        if (GraphicalUA.getStaticObject() == null) {
            GraphicalUA incall = new GraphicalUA(instanceSipProvider, new_user_profile, call_type);
            incall.setVisible(true);
        } else {
            if (!GraphicalUA.getStatus().equals(GraphicalUA.UA_IDLE)) {
                HelperMethods.create_confrim_panel("Previous call will be terminated?", 60);
                if (HelperMethods.confirm == 1) {
                    GraphicalUA.getStaticObject().forceHangUp();
                    GraphicalUA incall = new GraphicalUA(instanceSipProvider, new_user_profile, call_type);
                    incall.setVisible(true);
                }
            } else {
                GraphicalUA.getStaticObject().forceHangUp();
                GraphicalUA incall = new GraphicalUA(instanceSipProvider, new_user_profile, call_type);
                incall.setVisible(true);
            }

        }


    }
}
