/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author FaizAhmed
 */
public class StaticImages {

    public static final String ICON_24MYFNF = "images/icon.png";
    public static final String ICON_CLOSE = "/images/close.png";
    public static final String ICON_CLOSE_HOVER = "/images/close_h.png";
    public static final String IMAGE_MINIMIZE = "/images/minimize.png";
    public static final String IMAGE_MINIMIZE_HOVER = "/images/minimize_h.png";
    public static final String IMAGE_MALE = "images/male.png";
    public static final String IMAGE_MALE_SCALED = "male.png";
    public static final String IMAGE_FEMALE = "images/female.png";
    public static final String IMAGE_FEMALE_SCALED = "female.png";
    public static final String LOADING_IMAGE = "images/loading.gif";
    public static final String LOADING_IMAGE_SCALED = "loading.gif";
    //presence images
    public static final String IMAGE_ONLINE = "images/online.png";
    public static final String IMAGE_OFFLINE = "images/offline.png";
    public static final String IMAGE_ACCEPT = "images/accept.png";
    public static final String IMAGE_CONTACT = "images/contact.png";
    public static final String IMAGE_CONTACT_H = "images/contact_h.png";
    public static final String IMAGE_PROFILE = "images/profile.png";
    public static final String IMAGE_PROFILE_H = "images/profile_h.png";
    public static final String IMAGE_RECENT = "images/recent.png";
    public static final String IMAGE_RECENT_H = "images/recent_h.png";
    public static final String IMAGE_DIAL_PAD = "images/dial_pad.png";
    public static final String IMAGE_DIAL_PAD_H = "images/dial_pad_h.png";
    public static final String IMAGE_MENU_BAR = "images/menu_bar.png";
    public static final String IMAGE_PENDING = "images/pending.png";
    public static final String IMAGE_LOGO_BIG = "images/logo_big.png";
    public static final String IMAGE_P2P = "images/call_pin2pin.png";
    public static final String IMAGE_MOBILE_CALL = "images/mobile_call.png";
    public static final String IMAGE_NOTIFY = "images/IMAGE_NOTIFY.png";
    public static final String IMAGE_CALL_CANCEL = "images/call_cancel.png";
    public static final String IMAGE_INVITE_H = "/images/invite_h.png";
    /*CALL IMAGES*/
    public static final String IMAGE_INCOMMING_CALL = "images/incoming_call.png";
    public static final String IMAGE_OUTGOING_CALL = "images/outgoing_call.png";
    public static final String IMAGE_MISS_CALL = "images/miss_call.png";
    public static final String IMAGE_OUTGOING_CALL_BIG = "images/outgoing_call_big.png";
    public static final String IMAGE_INCOMMING_CALL_BIG = "images/incoming_call_big.png";
    public static final String IMAGE_CALL_ANSWER = "images/Answer.png";
    public static final String IMAGE_CALL_ANSWER_H = "images/Answer_h.png";
    public static final String IMAGE_CALL_DECLINE = "images/Decline_m.png";
    public static final String IMAGE_CALL_DECLINE_H = "images/Decline_h.png";
    public static final String IMAGE_CALL = "images/call.png";
    public static final String IMAGE_CALL_H = "images/call_h.png";
    public static final String IMAGE_END = "images/end.png";
    public static final String IMAGE_END_H = "images/end_h.png";
    public static final String IMAGE_CANCEL = "images/cancel.png";
    public static final String IMAGE_CANCEL_H = "images/cancel_h.png";
    public static final String IMAGE_SEARCH_IMAGE = "images/search_button.png";
    public static final String IMAGE_SEARCH_IMAGE_H = "images/search_button_h.png";
    public static final String IMAGE_Scale_1 = "images/scale_1.png";
    public static final String IMAGE_Scale_1_h = "images/scale_1_h.png";
    public static final String IMAGE_Scale_2 = "images/scale_2.png";
    public static final String IMAGE_Scale_2_h = "images/scale_2_h.png";
    public static final String IMAGE_Scale_3 = "images/scale_3.png";
    public static final String IMAGE_Scale_3_h = "images/scale_3_h.png";
    public static final String IMAGE_Scale_4 = "images/scale_4.png";
    public static final String IMAGE_Scale_5 = "images/scale_5.png";
    public static final String IMAGE_Scale_5_h = "images/scale_5_h.png";
    //pending
    public static final String IMAGE_joption_header = "images/second_header.png";
    public static final String IMAGE_joption_header_scaled = "second_header.png";
    public static final String IMAGE_callingwindow_bar = "images/calling_window_bar.png";
    /*calling*/
    public static final String IMAGE_GREEN_CALL = "images/green_call.png";
    public static final String IMAGE_GREEN_H = "images/green_call_h.png";
    public static final String IMAGE_RED_CANCEL = "images/red_cancel.png";
    public static final String IMAGE_RED_CANCEL_H = "images/red_cancel_h.png";
    public static final String SEARCH_IMG = "images/search_img.png";
    /* when group*/
    public static final String INVITE = "images/invite.png";
    public static final String INVITE_H = "images/invite_h.png";
    public static final String GROUP = "images/groups.png";
    public static final String GROUP_H = "images/groups_h.png";
    public static final String SIGNOUT = "images/sign_out.png";
    public static final String SIGNOUT_H = "images/sign_out_h.png";
    public static final String GROUP_IMAGE = "images/groupIcon.png";
    public static final String GROUP_IMAGE_SCALED = "groupIcon.png";
    /**/
    public static final String Scale_6 = "images/scale_6.png";
    public static final String Scale_6_h = "images/scale_6_h.png";
    /**/
    public static final String DIAL_PAD_IMAGE[] = {
        "images/dialpad/0.png",
        "images/dialpad/1.png",
        "images/dialpad/2.png",
        "images/dialpad/3.png",
        "images/dialpad/4.png",
        "images/dialpad/5.png",
        "images/dialpad/6.png",
        "images/dialpad/7.png",
        "images/dialpad/8.png",
        "images/dialpad/9.png",
        "images/dialpad/star.png",
        "images/dialpad/hash.png",
        "images/dialpad/call.png",
        "images/dialpad/clear.png"
    };
    public static final String DIAL_PAD_IMAGE_HOVER[] = {
        "images/dialpad/0h.png",
        "images/dialpad/1h.png",
        "images/dialpad/2h.png",
        "images/dialpad/3h.png",
        "images/dialpad/4h.png",
        "images/dialpad/5h.png",
        "images/dialpad/6h.png",
        "images/dialpad/7h.png",
        "images/dialpad/8h.png",
        "images/dialpad/9h.png",
        "images/dialpad/star_h.png",
        "images/dialpad/hash_h.png",
        "images/dialpad/call_h.png",
        "images/dialpad/clear_h.png"
    };
}
