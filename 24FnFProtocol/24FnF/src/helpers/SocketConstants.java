/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import myfnfui.IpAddress;
import myfnfui.Main;

/**
 *
 * @author faizahmed
 */
public class SocketConstants {

    public static IpAddress add;
    public static final long TIME_DIFFERENCE_TO_RESEND = 50;
    public static final long TIME_DIFFERENCE_TO_REMOVE_SENT_PACKETS = 1000L * 60 * 10;
    public static final String REQUEST = "REQUEST";
    //------------------------------------------------- Settings  name (start) ------------------------------------------------    
    public static final String VERSION = "version";
    //------------------------------------------------- Settings  name (end) ------------------------------------------------    
//------------------------------------------------- ACTION (start) ------------------------------------------------    
    public static final String CALL = "CALL";
    public static final String UPDATE = "UPDATE";
    public static final byte[] UPDATE_BYTE = "UPDATE".getBytes();
    public static final String RESPONSE = "RESPONSE";
    public static final String CONFIRMATION = "CONFIRMATION";
    public static final byte[] CONFIRMATION_BYTE = CONFIRMATION.getBytes();
    public static final String PRESENCE = "PRESENCE";
    public static final byte[] REQUEST_BYTE = REQUEST.getBytes();
    public static final byte[] CALL_BYTE = "CALL".getBytes();
    public static final String AUTHENTICATION = "AUTHENTICATION";
    public static final byte[] AUTHENTICATION_BYTE = "AUTHENTICATION".getBytes();
//------------------------------------------------- ACTION (end) --------------------------------------------------
//------------------------------------------------- CALL TYPE (start) --------------------------------------------- 
    public static final String ADD_FRIEND = "add_friend";
    public static final String DELETE_FRIEND = "delete_friend";
    public static final String ACCEPT_FRIEND = "accept_friend";
    public static final String CHANGE_PASSWORD = "change_password";
    public static final String WHAT_IS_IN_UR_MIND_TYPE = "whatisInYourMind";
    public static final String USER_PROFILE = "user_profile";
    public static final String CALL_START = "call_start";
    public static final String CALL_END = "call_end";
    public static final String CONTACT_LIST = "contactList";
    public static final String CONTACT_SEARCH = "contactSearch";
    public static final String USER_ID_AVAILABLE = "userIdentityAvailable";
    public static final String CALL_LOG = "callLog";
    public static final String SIGN_IN = "signin";
    public static final String SIGN_UP = "signup";
    public static final String SIGN_OUT = "signout";
    public static final String KEEP_ALIVE = "keepAlive";
    public static final byte[] KEEP_ALIVE_BYTE = KEEP_ALIVE.getBytes();
    public static final int BUFFER_SIZE = 65535;
    public static final String CALLLogSingleFriend = "callLogSingleFriend";
//------------------------------------------------- CALL TYPE (end) ------------------------------------------------
//------------------------------------------------- HEADER (start) -------------------------------------------------   
    public static final byte[] ACTION_BYTE = "action".getBytes();
    public static final byte[] TYPE_BYTE = "type".getBytes();
    public static final String SESSION_ID = "sessionId";
    public static final byte[] SESSION_ID_BYTE = SESSION_ID.getBytes();
    public static final String PACKET_ID = "packetId";
    public static final byte[] PACKET_ID_BYTE = PACKET_ID.getBytes();
    public static final String TYPE = "type";
    public static final String NO_OF_HEADERS = "noOfHeaders";
    public static final String WHAT_IS_IN_YOUR_MIND = "whatisInYourMind";
    public static final String PROFILE_IMAGE_TEXT = "profileImage";
//     public static final String USER_IDENTITY = "userIdentity";
//    public static final String OLD_PASSWORD = "oldPass";
//    public static final String NEW_PASSWORD = "newPass";
//    public static final String GENDER = "gender";
//    public static final String FIRST_NAME = "firstName";
//    public static final String LAST_NAME = "lastName";
//    public static final String MOBILE_PHONE_DIALING_CODE = "mobilePhoneDialingCode";
//    public static final String COUNTRY = "country";
//    public static final String EMAIL = "email";
//    public static final String MOBILE_PHONE = "mobilePhone";
//    public static final String DURATION = "duration";
//    public static final String CALL_ID = "callId";
//    public static final String SEARCH_PARAM = "searchparam";
//    public static final String USER_NAME = "userName";
//    public static final String PASSWORD = "password";
//    public static final String PROFILE_IMAGE_TEXT = "profileImage";
//------------------------------------------------- HEADER (end) --------------------------------------------------
//----------------------------------------- PRESENCE HEADER VALUE (start) -----------------------------------------
    public static final int CONS_FIRST_NAME = 1;
    public static final int CONS_LAST_NAME = 2;
    public static final int CONS_GENDER = 3;
    public static final int CONS_COUNTRY = 4;
    public static final int CONS_MOBILE_PHONE = 5;
    public static final int CONS_EMAIL = 6;
    public static final int CONS_PRESENCE = 7;
    public static final int CONS_WHAT_IS_IN_UR_MIND = 8;
    public static final int CONS_FRIENDSHIP_STATUS = 9;
    public static final int CONS_CALLING_CODE = 10;
    public static final int CONS_PREV_PASS = 11;
    public static final int CONS_NEW_PASS = 12;
    public static final int CONS_PROFILE_IMAGE = 13;
//----------------------------------------- PRESENCE HEADER VALUE (end) -----------------------------------------    
//---------------------------------------------- Response Messages (start) -----------------------------------------
//    public static final String MULTIPLE_SESSION_WARNING = "You have just signed in from another device";
//    public static final String SIGNIN_FAILED = "user name /password does not match";
//    public static final String USER_ID_UNAVAILABLE = "user name is not available";
////---------------------------------------------- Response Messages (end) -----------------------------------------        
//    public static long LOADING_INTERVAL = 10000;
//    public static String REDIRECT = "?faces-redirect=true";
//    //  public static String REDIRECT = "";
//    public static String LOGEDIN_USER = "LOGEDIN_USER";
//   // private String redirect;
//    public static int SEND_ALL_INFO = 1;
//    public static int SEND_UPDATE_INFO = 2;
//    public static int SEND_ADD_INFO = 3;
//    public static int SEND_DELETE_INFO = 4;
//    public static int ONLINE = 2;
//    public static int OFFLINE = 1;
//    //call types  
//    public static int dialledCall = 1;
//    public static int receivedCall = 2;
//    public static int missedCalles = 3;
//    // friendship statys
    //public static String ACCEPTED_FRIEND = "1";
//    public static String INCOMING_FRIEND_REQUEST = "2";
//    public static String OUTGOING_FRIEND_REQUEST = "3";
//    public static String recever = "gr5eb8u";
    /*Socket IP and port*/
     //public static String SOCKET_IP = "192.168.1.100";
    public static String SOCKET_IP = "38.127.68.246";
    public static Integer SOCKET_PORT = 0;

//    public String getRedirect() {
//        redirect = "?faces-redirect=true";
//        return redirect;
//    }
//
//    public void setRedirect(String redirect) {
//        this.redirect = redirect;
//    }
//    public static String getLOGEDIN_USER() {
//        return LOGEDIN_USER;
//    }
//
//    public static void setLOGEDIN_USER(String LOGEDIN_USER) {
//        SocketConstants.LOGEDIN_USER = LOGEDIN_USER;
//    }
//
//    public static String getREDIRECT() {
//        return REDIRECT;
//    }
//
//    public static void setREDIRECT(String REDIRECT) {
//        SocketConstants.REDIRECT = REDIRECT;
//    }
    public static IpAddress authIPAdress() {

        try {
            add = new IpAddress(IpAddress.getByName(SocketConstants.SOCKET_IP));
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);

        }
        return add;
    }

    public static String getDataFromUdpPak(myfnfui.UdpPacket pkd) {
        String data = "";
        data = new String(pkd.getData());
        return data;
    }

    public static void set_user_data_as_index(int index, String data) {

        if (index == 1) {
        }

    }
}
