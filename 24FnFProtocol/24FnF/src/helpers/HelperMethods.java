/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import imageDownload.DownLoaderHelps;
import java.awt.Color;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.ColorUIResource;
import local.ua.UserAgentProfile;
import myfnfui.CallLogDetails;
import myfnfui.JsonFields;
import myfnfui.LightScrollPane;
import myfnfui.UserProfile;
import myfnfui.groups.CreateGroup;
import myfnfui.groups.GroupDto;
import myfnfui.groups.MapGroupResponse;
import myfnfui.groups.RemoveGruop;
import myfnfui.headers.HeadersPOJO;
import myfnfui.packetsInfo.FriendList;
import myfnfui.packetsInfo.FriendlistMapping;
import myfnfui.packetsInfo.PackageActions;
import myfnfui.tampClass.SearchFreindlListMapping;
import myfnfui.tampClass.StaticFieldsForProfileUpdate;
import org.zoolu.sip.utils.IpConverter;

/**
 *
 * @author FaizAhmed
 */
public class HelperMethods {

    PackageActions packetAction = new PackageActions();
    public static int confirm = 0;
    public static Gson jsonLib;
    private static int label_text_font_size = 11;
    DownLoaderHelps dHelp = new DownLoaderHelps();
    //  public static String whatsInMind;
//    ImageFilter1 fJavaFilter = new ImageFilter1();
//    JLabel profile_picture;
//    static File fFile;
//    BufferedImage img = null;
//    static File oldfile;
//    int image_panle_size = 300;
//    ImageUploader img_up = new ImageUploader();
//    private boolean stopElapsedTimeCounting = false;
//    DownLoaderHelps dHelp = new DownLoaderHelps();
//    /*camere*/
//    public static CaptureStream captureStream = null;
//    public static boolean takeShot = false;
//    boolean take_in_panel = true;
//    JPanel panel_profile_pic;
//    JButton take_photo;
//    JButton upload_botton;
//    JButton use_pic_botton;
//    List camare_list;
    /*end*/

    public HelperMethods() {
//        CaptureSystemFactory factory = DefaultCaptureSystemFactorySingleton.instance();
//        CaptureSystem system;
//        try {
//            system = factory.createCaptureSystem();
//            system.init();
//            camare_list = system.getCaptureDeviceInfoList();
//            int i = 0;
//            if (i < camare_list.size()) {
//                CaptureDeviceInfo info = (CaptureDeviceInfo) camare_list.get(i);
//                System.out.println((new StringBuilder()).append("Device ID ").append(i).append(": ").append(info.getDeviceID()).toString());
//                System.out.println((new StringBuilder()).append("Description ").append(i).append(": ").append(info.getDescription()).toString());
//                captureStream = system.openCaptureDeviceStream(info.getDeviceID());
//                captureStream.setObserver(HelperMethods.this);
//            }
//        } catch (CaptureException ex) {
//            ex.printStackTrace();
//        }
    }

    public static boolean check_string_contains_substring(String main_string, String string_to_test) {
        main_string = main_string.toLowerCase();
        string_to_test = string_to_test.toLowerCase();
        if (main_string.contains(string_to_test)) {
            return true;
        } else {
            return false;
        }
    }

    public static String convert_epoc_to_date_string(long epoc_date) {

        long date = epoc_date;
        String NormalDate = "";
        int hours = (int) (date / 3600);
        if (hours > 5) {
            NormalDate = new java.text.SimpleDateFormat("HH:mm:ss MM-dd-yy").format(new java.util.Date(date));
        } else {
            NormalDate = new java.text.SimpleDateFormat("MM-dd-yy").format(new java.util.Date(date));
        }
        return NormalDate;

    }

    public static String convert_seconds_to_h_m_s(long seconds_input) {
        int hours = (int) (seconds_input / 3600);
        int myremainder = (int) (seconds_input % 3600);
        int minutes = (int) (myremainder / 60);
        int seconds = myremainder % 60;
        return hours + " :" + minutes + ":" + seconds;
        //  return "";
    }

    public static String get_country_code_from_contry_name(String conuntry) {
        // String code = "";
        int index = 0;

        for (int f = 0; f < MyFnFSettings.COUNTRY_MOBILE_CODE.length; f++) {
            //  country_prefix_ComboBox.addItem(MyFnFSettings.COUNTRY_MOBILE_CODE[f][0].toString());
            if (MyFnFSettings.COUNTRY_MOBILE_CODE[f][0].toString().equals(conuntry)) {
                index = f;
                break;
            }

        }
        return MyFnFSettings.COUNTRY_MOBILE_CODE[index][1];
    }

    public static UserAgentProfile get_all_for_a_profile(String user_id) {
        UserAgentProfile pp = new UserAgentProfile();
        for (String key : FriendList.getInstance().getFriend_hash_map().keySet()) {
            if (key.equals(user_id)) {
                pp = FriendList.getInstance().getFriend_hash_map().get(key);
                break;
            }
        }
        return pp;
    }

    public static UserAgentProfile get_all_for_user_or_mobile_number(String mobile_no) {
        UserAgentProfile pp = new UserAgentProfile();
        for (String key : FriendList.getInstance().getFriend_hash_map().keySet()) {
            if (key.equals(mobile_no) || mobile_no.equals(FriendList.getInstance().getFriend_hash_map().get(key).getMobilePhone())) {
                pp = FriendList.getInstance().getFriend_hash_map().get(key);
                break;
            }
//            if (prfl.getUserIdentity().equals(mobile_no) || prfl.getMobilePhone().equals(mobile_no)) {
//                pp = prfl;
//                break;
//            }
        }
        return pp;
    }

    public static String password_check(String password) {
        String pp = "";
        if (password.length() < 6) {
            pp = StaticFields.PASSWORD_TEXT + " must be atleast 6 characters";
        } else if (HelperMethods.check_string_contains_substring(password, MyFnFSettings.LOGIN_USER_ID)) {
            pp = StaticFields.PASSWORD_TEXT + " can't contain  " + StaticFields.MY_FNF_NAME_TEXT;

        }
        return pp;
    }

    public static void change_whats_in_mind(final String currnt_text) {
        final JDialog dialog_mind = create_joption_panel(300, 250, StaticFields.Whats_In_Your_Mind_TEXT);
        int margin_left = 45;
        int margin_top = 5;
        int label_width = 100;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label = DesignClasses.makeJLabel_with_text_color("", margin_left, margin_top, 100, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color("Enter mood message", margin_left, margin_top = margin_top + 25, 150, label_text_font_size, 1, DefaultSettings.text_color1);

        final JTextArea jtextArea = DesignClasses.create_text_area(currnt_text, margin_left, margin_top + 30, 200, 100);
        LightScrollPane what_in_mind_textarea_sroll = new LightScrollPane(jtextArea);
        what_in_mind_textarea_sroll.setBorder(DefaultSettings.border_light_blue);
        what_in_mind_textarea_sroll.setBounds(margin_left, margin_top + 30, 200, 100);
        what_in_mind_textarea_sroll.setBorder(BorderFactory.createCompoundBorder(
                what_in_mind_textarea_sroll.getBorder(),
                BorderFactory.createEmptyBorder(5, 5, 5, 5)));
        panel.add(what_in_mind_textarea_sroll);


        final JButton ok_mind = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left, margin_top = margin_top + 140, 74, 22);
        final JButton cancel_button_mind = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + 115 + 10, margin_top, 74, 22);
        panel.add(f_label);
        panel.add(ok_mind);
        panel.add(cancel_button_mind);
        // panel.add(button);
        panel.add(error_text_label);
        //JButton[] buttons = {button};
        cancel_button_mind.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_mind.dispose();
            }
        });

        ok_mind.addActionListener(new ActionListener() {
            private String last_str;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (jtextArea.getText().length() > 0) {
                    if (!currnt_text.equals(jtextArea.getText())) {
                        String pak_id = PackageActions.create_packed_id_for(SocketConstants.WHAT_IS_IN_YOUR_MIND);
                        JsonFields jF = new JsonFields();
                        jF.setAction(SocketConstants.UPDATE);
                        jF.setType(SocketConstants.WHAT_IS_IN_YOUR_MIND);
                        jF.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                        jF.setPacketId(pak_id);
                        jF.setWhatisInYourMind(jtextArea.getText());
                        PackageActions.sendPacketAsString(jF);
                        dialog_mind.dispose();
                        StaticFieldsForProfileUpdate.whatisInYourMind = jtextArea.getText();
                        //{"sessionId":"nazmul","whatisInYourMind":"Hello 24Fnf","type":"whatisInYourMind","action":"UPDATE","packetId":"nazmulwhatisInYourMind1367125008202"}

                    } else {
                        StaticFieldsForProfileUpdate.whatisInYourMind = null;
                        error_text_label.setText("Same Text Used");
                    }
                } else {
                    error_text_label.setText("Empty Field");
                }
            }
        });
        dialog_mind.getContentPane().add(panel);
        dialog_mind.setVisible(true);
    }

    public static void openDialog(final String bar_text, final String currnt_text, final String parameter) {
        final JDialog dialog = create_joption_panel(300, 145, bar_text);
        int margin_left = 25;
        int margin_top = 5;
        int label_width = 60;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label = DesignClasses.makeJLabel_with_text_color("", margin_left + label_width + 10, margin_top, 100, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color(bar_text, margin_left, margin_top = margin_top + 25, 70, 11, 1, DefaultSettings.text_color1);
        final JTextField f_text = DesignClasses.makeTextField(currnt_text, margin_left + label_width + 10, margin_top, DefaultSettings.textBoxWidth + 10);
        final JButton ok_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left + label_width + 10, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 10, margin_top, 74, 22);
        panel.add(f_label);
        panel.add(f_text);
        panel.add(ok_button);
        panel.add(cancel_button);
        // panel.add(button);
        panel.add(error_text_label);
        //JButton[] buttons = {button};
        cancel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });

        ok_button.addActionListener(new ActionListener() {
            private String last_str;

            @Override
            public void actionPerformed(ActionEvent e) {
                last_str = f_text.getText().toString();
                if (last_str != null && !currnt_text.trim().equals(last_str)) {
                    ok_button.setEnabled(false);
                    int valid = 1;
                    if (parameter.equals(StaticFields.EMAIL)) {
                        EmailValidator email = new EmailValidator();
                        if (email.validate(last_str) == false) {
                            valid = 0;
                        }
                    }
                    if (valid == 1) {
                        //   if (GetResponseJsons.edit_profile_field(parameter, last_str)) {
                        error_text_label.setText("Changed... ");
                        if (parameter.equals(StaticFields.FIRST_NAME)) {
                            send_profile_update_request(SocketConstants.CONS_FIRST_NAME, last_str);
                            StaticFieldsForProfileUpdate.firstName = last_str;
                        } else if (parameter.equals(StaticFields.LAST_NAME)) {
                            send_profile_update_request(SocketConstants.CONS_LAST_NAME, last_str);
                            StaticFieldsForProfileUpdate.lastName = last_str;
                        } else if (parameter.equals(StaticFields.EMAIL)) {
                            send_profile_update_request(SocketConstants.CONS_EMAIL, last_str);
                            StaticFieldsForProfileUpdate.email = last_str;
                        }
                        //  PackageActions.sendOnly(jF);
                        dialog.dispose();
                        //  }
                    } else {
                        ok_button.setEnabled(true);
                        error_text_label.setText("Invalid Email");
                    }
                } else {
                    error_text_label.setText("Same text used");
                }
            }
        });
        dialog.getContentPane().add(panel);
//        dialog.setSize(350, 180);
//        dialog.setLocationRelativeTo(f);
        dialog.setVisible(true);
    }

    public static void change_password_openDialog(final String bar_text) {
        final JDialog dialog = create_joption_panel(300, 180, "Password");
        int margin_left = 10;
        int margin_top = 5;
        int label_width = 90;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label = DesignClasses.makeJLabel_with_text_color("", margin_left + 50, margin_top, 200, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color("Old Passowrd", margin_left, margin_top = margin_top + 28, label_width, 12, 0, DefaultSettings.text_color1);
        final JPasswordField old_password = DesignClasses.makeJPasswordField("", margin_left + label_width + 10, margin_top);
        JLabel new_pass_label = DesignClasses.makeJLabel_with_text_color("New Passowrd", margin_left, margin_top = margin_top + 30, label_width, 12, 0, DefaultSettings.text_color1);
        final JPasswordField new_pass_text = DesignClasses.makeJPasswordField("", margin_left + label_width + 10, margin_top);
        final JButton ok_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left + label_width + 10, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 5 + 74 + 10, margin_top, 74, 22);
        panel.add(f_label);
        panel.add(old_password);
        panel.add(ok_button);
        panel.add(cancel_button);
        // panel.add(button);
        panel.add(error_text_label);
        panel.add(new_pass_label);
        panel.add(new_pass_text);
        // JButton[] buttons = {button};
        cancel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog.dispose();
            }
        });

        ok_button.addActionListener(new ActionListener() {
            private String notify_msg = "";
            int success = 0;

            @Override
            public void actionPerformed(ActionEvent e) {


                if (old_password.getText().length() > 5 || new_pass_text.getText().length() > 5) {

                    if (!old_password.getText().equals(new_pass_text.getText())) {
                        String msg = HelperMethods.password_check(new_pass_text.getText());
                        if (msg.length() == 0) {
                            //  success = 1;
                            if (MyFnFSettings.LOGIN_USER_PASSWORD.equals(old_password.getText())) {
                                String pak_id = PackageActions.create_packed_id_for(SocketConstants.CHANGE_PASSWORD);
                                JsonFields jF = new JsonFields();
                                jF.setAction(SocketConstants.UPDATE);
                                jF.setType(SocketConstants.CHANGE_PASSWORD);
                                jF.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
                                jF.setPacketId(pak_id);
                                jF.setOldPass(old_password.getText());
                                jF.setNewPass(new_pass_text.getText());
                                StaticFieldsForProfileUpdate.password = new_pass_text.getText();
                                PackageActions.sendPacketAsString(jF);
                                notify_msg = "";
                                success = 1;
////                                System.out.println(old_password.getText());
////                                System.out.println(new_pass_text.getText());
//                                if (GetResponseJsons.change_password(old_password.getText().trim(), new_pass_text.getText().trim())) {
//                                    MyFnFSettings.LOGIN_USER_PASSWORD = new_pass_text.getText().trim();
//                                    MyFnFSettings.passwordChange = true;
//                                    notify_msg = "Password Changed Successfully";
//                                    success = 1;
//                                } else {
//                                    notify_msg = "Password doesn't match";
//                                    //  JOptionPane.showMessageDialog(null, notify_msg);
//                                }
                            } else {
                                notify_msg = "Password doesn't match";
                                //  JOptionPane.showMessageDialog(null, notify_msg);
                            }
                        } else {
                            notify_msg = msg;
                        }
                    } else {
                        notify_msg = "Same Password Used";

                    }
                } else {
                    notify_msg = "Password must be atleast 6 characters";
                    // JOptionPane.showMessageDialog(null, notify_msg);
                }
                if (success == 1) {
                    error_text_label.setText(notify_msg);
                    dialog.dispose();
                } else {
                    error_text_label.setText(notify_msg);
                }
            }
        });
        dialog.getContentPane().add(panel);
//        dialog.setSize(widht, height);
//        dialog.setLocationRelativeTo(f);
        dialog.setVisible(true);
    }

    public static int change_mobile_no() {
        int change = 0;
        final JDialog dialog_mble = create_joption_panel(300, 145, "Mobile Number");
        int margin_left = 10;
        int margin_top = 5;
        int label_width = 90;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label_mobile = DesignClasses.makeJLabel_with_text_color("", margin_left + label_width + 10, margin_top, 200, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color("New Mobile No", margin_left, margin_top = margin_top + 28, label_width, 12, 1, DefaultSettings.text_color1);
        final JTextField prefix = DesignClasses.makeTextField(MyFnFSettings.userProfile.getMobilePhoneDialingCode(), margin_left + label_width + 5, margin_top, DefaultSettings.DEFAULT_PREFIX_TEXT_BOX_WIDTH);
        //     JLabel new_pass_label = DesignClasses.makeJLabel_with_text_color("New Passowrd", margin_left, margin_top = margin_top + 35, label_width, 12, 1, DefaultSettings.text_color1);
        final JTextField mobile_no = DesignClasses.makeTextField(MyFnFSettings.userProfile.getMobilePhone(), margin_left + DefaultSettings.textBoxWidth + 0, margin_top, DefaultSettings.textBoxWidth - (DefaultSettings.DEFAULT_PREFIX_TEXT_BOX_WIDTH) + 5);
        final JButton ok_button2 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left + label_width + 5, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 5 + 74 + 12, margin_top, 74, 22);
        panel.add(f_label);
        panel.add(prefix);
        panel.add(ok_button2);
        panel.add(cancel_button);
        //  panel.add(button);
        panel.add(error_text_label_mobile);
        //   panel.add(new_pass_label);
        panel.add(mobile_no);
        // JButton[] buttons = {button};
        cancel_button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_mble.dispose();
            }
        });

        ok_button2.addActionListener(new ActionListener() {
            private String notify_msg = "";
            int success = 0;

            @Override
            public void actionPerformed(ActionEvent e) {
                String ss = "";

                if (!MyFnFSettings.userProfile.getMobilePhoneDialingCode().equals(prefix.getText())) {
                    ss = check_country_code(prefix.getText());

                    if (ss.length() > 0) {
                    } else {
                        success = 1;
                        send_profile_update_request(SocketConstants.CONS_CALLING_CODE, prefix.getText());
                        StaticFieldsForProfileUpdate.mobilePhoneDialingCode = prefix.getText();
//                        if (GetResponseJsons.edit_profile_field(StaticFields.PARAM_DAILING_CODE, prefix.getText())) {
//                            MyFnFSettings.userProfile.setMobilePhoneDialingCode(prefix.getText());
//                        }
                    }
                } else {
                    //  ss = "ddd";
                }
                if (!MyFnFSettings.userProfile.getMobilePhone().equals(mobile_no.getText())) {
                    ss = check_only_digit(mobile_no.getText());

                    if (ss.length() > 0) {
                    } else {

                        success = 1;
                        String changed_mobile_number = HelperMethods.bd_mobile_number(prefix.getText(), mobile_no.getText());
                        mobile_no.setText(changed_mobile_number);
                        send_profile_update_request(SocketConstants.CONS_MOBILE_PHONE, mobile_no.getText());
                        StaticFieldsForProfileUpdate.mobilePhone = mobile_no.getText();
//                        if (GetResponseJsons.edit_profile_field(StaticFields.MOBILE_PHONE, changed_mobile_number)) {
//                            MyFnFSettings.userProfile.setMobilePhone(mobile_no.getText());
//                        }
                    }
                } else {
                    ss = "No Change";
                }
                if (success == 1) {
                    error_text_label_mobile.setText("Mobile no changed");
                    dialog_mble.dispose();
                } else {
                    error_text_label_mobile.setText(ss);
                }
            }
        });
        dialog_mble.getContentPane().add(panel);
        dialog_mble.setVisible(true);
        return change;
    }

    public static int change_country_name() {
        int change = 0;

        final JDialog dialog_county = create_joption_panel(300, 150, "Country Name");
        int margin_left = 20;
        int margin_top = 5;
        int label_width = 90;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label_country = DesignClasses.makeJLabel_with_text_color("", margin_left + label_width + 10, margin_top, 200, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color("Select Country", margin_left, margin_top = margin_top + 28, label_width, 12, 1, DefaultSettings.text_color1);
        final JComboBox countryComboBox = DesignClasses.create_ComboBox_from_array(HelperMethods.get_only_country_list(), margin_left + label_width + 10, margin_top, DefaultSettings.country_combox_width);
        countryComboBox.setSelectedItem(MyFnFSettings.userProfile.getCountry());
        countryComboBox.setEditable(true);
        final JButton ok_button3 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left + label_width + 10, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button2 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 5, margin_top, 74, 22);
        panel.add(f_label);
        //  panel.add(prefix);
        panel.add(ok_button3);
        panel.add(cancel_button2);
        //   panel.add(button);
        panel.add(error_text_label_country);
        //   panel.add(new_pass_label);
        panel.add(countryComboBox);
        // JButton[] buttons = {button};
        cancel_button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_county.dispose();
            }
        });

        ok_button3.addActionListener(new ActionListener() {
            private String notify_msg = "";
            int success = 0;

            //   country_prefixTextField.setText (c_code);
//            private String old_pass = old_password.getText();
//            private String new_pass = new_pass_text.getText();
            @Override
            public void actionPerformed(ActionEvent e) {
                if (countryComboBox.getSelectedItem().toString().length() > 3) {
                    if (MyFnFSettings.userProfile.getCountry().equals(countryComboBox.getSelectedItem().toString())) {
                        error_text_label_country.setText("Same Conuntry Selected");
                    } else {
                        send_profile_update_request(SocketConstants.CONS_COUNTRY, countryComboBox.getSelectedItem().toString());
                        dialog_county.dispose();
                        StaticFieldsForProfileUpdate.country = countryComboBox.getSelectedItem().toString();
                        // 
                    }
                } else {
                    error_text_label_country.setText("Please Select valid country");
                }
            }
        });
        dialog_county.getContentPane().add(panel);
//        dialog_county.setSize(widht, height);
//        dialog_county.setLocationRelativeTo(f);
        dialog_county.setVisible(true);
        return change;
    }

    public static int change_gender() {
        int change = 0;
        final JDialog dialog_gender = create_joption_panel(320, 140, "Gender");
        int margin_left = 20;
        int margin_top = 0;
        int label_width = 80;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        final JLabel error_text_label_geder = DesignClasses.makeJLabel_with_text_color("", margin_left + label_width + 10, margin_top, 200, 11, 0, Color.RED);
        JLabel f_label = DesignClasses.makeJLabel_with_text_color("Select gerder", margin_left, margin_top = margin_top + 28, label_width, 12, 1, DefaultSettings.text_color1);
        final JComboBox genderComboBox = DesignClasses.create_ComboBox_from_array(MyFnFSettings.GENDER_ARRAY, margin_left + label_width + 5, margin_top, DefaultSettings.country_combox_width);
        genderComboBox.setSelectedItem(MyFnFSettings.userProfile.getGender());
        final JButton ok_button4 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "OK", 13, 1, margin_left + label_width + 5, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button5 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 5 + 74 + 5, margin_top, 74, 22);
        panel.add(f_label);
        panel.add(ok_button4);
        panel.add(cancel_button5);
        panel.add(error_text_label_geder);
        panel.add(genderComboBox);
        cancel_button5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_gender.dispose();
            }
        });

        ok_button4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (MyFnFSettings.userProfile.getGender().equals(genderComboBox.getSelectedItem().toString())) {
                    error_text_label_geder.setText("Same Gender Selected");
                } else {
                    send_profile_update_request(SocketConstants.CONS_GENDER, genderComboBox.getSelectedItem().toString());
                    StaticFieldsForProfileUpdate.gender = genderComboBox.getSelectedItem().toString();
                    dialog_gender.dispose();

//                    if (GetResponseJsons.edit_profile_field(StaticFields.GENDER, genderComboBox.getSelectedItem().toString())) {
//                        MyFnFSettings.userProfile.setGender(genderComboBox.getSelectedItem().toString());
//                        dialog_gender.dispose();
//
//                    } else {
//                        error_text_label_geder.setText("Try Again");
//                    }
//                    // 
                }
            }
        });
        dialog_gender.getContentPane().add(panel);
        dialog_gender.setVisible(true);
        return change;
    }

    public static JDialog create_joption_panel(int width, int height, String text) {
        UIManager UI = new UIManager();
        UI.put("OptionPane.background", new ColorUIResource(255, 255, 255));
        UI.put("Panel.background", new ColorUIResource(255, 255, 255));
        final JFrame frm = new JFrame();
        JDialog dialog_panel = new JDialog(frm, "24FnF " + text, true);
        ImageIcon imageIcon = DesignClasses.return_image(StaticImages.ICON_24MYFNF);
        Image image = imageIcon.getImage();
        dialog_panel.setIconImage(image);
        dialog_panel.setResizable(false);
        dialog_panel.setSize(width, height);
        dialog_panel.setLocationRelativeTo(frm);
        return dialog_panel;
    }

    public static JDialog create_joption_panel_no_default_close(int width, int height, String text) {
        int task_bar_height = 50;
        UIManager UI = new UIManager();
        UI.put("OptionPane.background", new ColorUIResource(255, 255, 255));
        UI.put("Panel.background", new ColorUIResource(255, 255, 255));
        final JFrame frm = new JFrame();
        JDialog dialog_panel = new JDialog(frm, text, true);
        ImageIcon imageIcon = DesignClasses.return_image(StaticImages.ICON_24MYFNF);
        Image image = imageIcon.getImage();
        dialog_panel.setIconImage(image);
//        dialog_panel.setResizable(false);
//        dialog_panel.setSize(width, height);
//        dialog_panel.setLocationRelativeTo(frm);
        //  this.setSize(frame_width, frame_height);
        dialog_panel.setLocationRelativeTo(frm);
        // notifyWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dialog_panel.setResizable(false);
        dialog_panel.setUndecorated(true);
        dialog_panel.setSize(width, height);
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice defaultScreen = ge.getDefaultScreenDevice();
        Rectangle rect = defaultScreen.getDefaultConfiguration().getBounds();
        int x = (int) rect.getMaxX() - dialog_panel.getWidth();
        int y = (int) rect.getMaxY() - dialog_panel.getHeight();
        y = y - task_bar_height;
        dialog_panel.setLocation(x, y);
        // dialog_panel.setVisible(true);

        return dialog_panel;
    }

    public static void show_long_notification_msg(String text) {
        confirm = 0;
        final JDialog dialog_confirm_long = HelperMethods.create_joption_panel(290, 120, "");

        int margin_left = 10;
        int margin_top = 10;
        int label_width = 50;
        JPanel panel = new JPanel();
        panel.setLayout(null);

        JTextArea jtextArea = DesignClasses.create_text_area(text, 40, 10, 250, 40, 11);
        jtextArea.setForeground(Color.BLACK);
        jtextArea.setEditable(false);
        //   LightScrollPane what_in_mind_textarea_sroll = new LightScrollPane(jtextArea);
        jtextArea.setBorder(null);
        //    what_in_mind_textarea_sroll.setBounds(margin_left, margin_top + 30, 200, 100);
        // JLabel lable = DesignClasses.makeJLabel_with_background_font_size_width_color(text, 2, margin_top, 300, 40, 12, 0, Color.BLACK, Color.WHITE, 0);
        panel.add(jtextArea);
        final JButton ok_button7 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Ok", 13, 1, margin_left + label_width + 45, margin_top = margin_top + 40, 74, 22);
        //final JButton cancel_button6 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 10, margin_top, 74, 22);
        panel.add(ok_button7);
        ok_button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok_button7.setEnabled(false);
                dialog_confirm_long.dispose();

            }
        });
        dialog_confirm_long.getContentPane().add(panel);
        dialog_confirm_long.setVisible(true);
        //   return confirm;
    }

    public static void create_confrim_panel(String text, int margin_left_from_use) {
        confirm = 0;
        final JDialog dialog_confirm = HelperMethods.create_joption_panel(290, 120, "");

        int margin_left = 10;
        int margin_top = 10;
        int label_width = 50;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        JLabel lable = DesignClasses.makeJLabel_with_background_font_size_width_color(text, 2, margin_top, 300, 40, 12, 0, Color.BLACK, Color.WHITE, 0);
        panel.add(lable);
        final JButton ok_button6 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Ok", 13, 1, margin_left + label_width + 10, margin_top = margin_top + 40, 74, 22);
        final JButton cancel_button6 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 10, margin_top, 74, 22);

        panel.add(ok_button6);
        panel.add(cancel_button6);
        cancel_button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dialog_confirm.dispose();
                // close_window = 0;
            }
        });

        ok_button6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok_button6.setEnabled(false);
                dialog_confirm.dispose();
                confirm = 1;
            }
        });
        dialog_confirm.getContentPane().add(panel);
        dialog_confirm.setVisible(true);
        //   return confirm;
    }

    public static void create_confrim_panel_no_cancel(String text, int margin_left_from_use) {
        final JDialog dialog_confirm_no_cancel = HelperMethods.create_joption_panel(290, 120, "");
        int margin_left = 10;
        int margin_top = 10;
        int label_width = 50;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        // JLabel lable=DesignClasses.makeJLabel_with_text_color(text, margin_left + margin_left_from_use, margin_top, 240, 12, 0, DefaultSettings.text_color_normal);
        JLabel lable = DesignClasses.makeJLabel_with_background_font_size_width_color(text, 2, margin_top, 300, 40, 12, 0, Color.BLACK, Color.WHITE, 0);
        panel.add(lable);
        final JButton ok_button7 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Ok", 13, 1, margin_left + label_width + 45, margin_top = margin_top + 40, 74, 22);
        //final JButton cancel_button6 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 10, margin_top, 74, 22);
        panel.add(ok_button7);
        ok_button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok_button7.setEnabled(false);
                dialog_confirm_no_cancel.dispose();

            }
        });
        dialog_confirm_no_cancel.getContentPane().add(panel);
        dialog_confirm_no_cancel.setVisible(true);
        //   return confirm;
    }

    public static void create_confrim_panel_no_cancel_no_left_margin(String text) {
        final JDialog dialog_confirm_no_cancel = HelperMethods.create_joption_panel(290, 120, "");
        int margin_left = 10;
        int margin_top = 10;
        int label_width = 50;
        JPanel panel = new JPanel();
        panel.setLayout(null);
        // JLabel lable=DesignClasses.makeJLabel_with_text_color(text, margin_left + margin_left_from_use, margin_top, 240, 12, 0, DefaultSettings.text_color_normal);
        JLabel lable = DesignClasses.makeJLabel_with_background_font_size_width_color(text, 2, margin_top, 300, 40, 12, 0, Color.BLACK, Color.WHITE, 0);
        panel.add(lable);
        final JButton ok_button7 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Ok", 13, 1, margin_left + label_width + 45, margin_top = margin_top + 40, 74, 22);
        //final JButton cancel_button6 = DesignClasses.create_image_button_with_text(StaticImages.IMAGE_Scale_3, StaticImages.IMAGE_Scale_3_h, "Cancel", 13, 1, margin_left + label_width + 10 + 74 + 10, margin_top, 74, 22);
        panel.add(ok_button7);
        ok_button7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ok_button7.setEnabled(false);
                dialog_confirm_no_cancel.dispose();

            }
        });
        dialog_confirm_no_cancel.getContentPane().add(panel);
        dialog_confirm_no_cancel.setVisible(true);
        //   return confirm;
    }

    public static String check_country_code(String code) {
        String return_msg = "";
        if (code.charAt(0) == '+') {
            String phoneNumber = code.replace("+", "");
            //         System.out.println(phoneNumber);
            return_msg = check_only_digit(phoneNumber);
        } else {
            return_msg = "Invalid Dialing Code";
        }
        return return_msg;
    }

    public static String check_only_digit(String phoneNumber) {
        String return_msg = "";
        Pattern pattern = Pattern.compile("\\d+");
        Matcher matcher = pattern.matcher(phoneNumber);
        if (matcher.matches()) {
            // System.out.println("valid" + phoneNumber);
        } else {
            return_msg = "Please Use Only Numeric number";
        }

        return return_msg;
    }

    public static String bd_mobile_number(String country_code, String mobile_num) {
        if ("+880".equals(country_code) && mobile_num.startsWith("0")) {
            mobile_num = mobile_num.substring(1);
        }
        return mobile_num;
    }

    public static String[] get_only_country_list() {
        String[] country_array = new String[MyFnFSettings.COUNTRY_MOBILE_CODE.length];
        // System.out.println("" + country_array.length);

        for (int i = 0; i < MyFnFSettings.COUNTRY_MOBILE_CODE.length; i++) {
            country_array[i] = MyFnFSettings.COUNTRY_MOBILE_CODE[i][0].toString();
        }

        return country_array;

    }

    public static JsonFields response_json(String inupt) {

        jsonLib = new GsonBuilder().serializeNulls().create();
        JsonFields fields = jsonLib.fromJson(inupt.trim(), JsonFields.class);
        return fields;

    }

    public static CallLogDetails map_call_log(String inupt) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        CallLogDetails fields = jsonLib.fromJson(inupt.trim(), CallLogDetails.class);
        return fields;

    }

    public static UserAgentProfile response_json_in_user_profile(String inupt) {

        jsonLib = new GsonBuilder().serializeNulls().create();
        UserAgentProfile fields = jsonLib.fromJson(inupt.trim(), UserAgentProfile.class);
        return fields;

    }

    public static FriendlistMapping friendlist_mapping(String inupt) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        FriendlistMapping fields = jsonLib.fromJson(inupt.trim(), FriendlistMapping.class);
        return fields;
    }

    public static SearchFreindlListMapping tamp_friendlist_mapping(String inupt) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        SearchFreindlListMapping fields = jsonLib.fromJson(inupt.trim(), SearchFreindlListMapping.class);
        return fields;
    }

    public static void user_profile(String inupt) {
        jsonLib = new GsonBuilder().serializeNulls().create();
        MyFnFSettings.userProfile = jsonLib.fromJson(inupt.trim(), UserProfile.class);
        if (MyFnFSettings.userProfile.getSwitchIp().contains(".")) {
            long swithcip = IpConverter.getIPVal(MyFnFSettings.userProfile.getSwitchIp());
            MyFnFSettings.userProfile.setSwitchIp(String.valueOf(swithcip));
        }
        if (MyFnFSettings.userProfile.getBalance() == null) {
            MyFnFSettings.userProfile.setBalance((long) 300 * 1000);
        }

//        MyFnFSettings.userProfile.setSwitchIp("-1502847858");
//        MyFnFSettings.userProfile.setSwitchPort(5080);

    }

    public static int getAvailablePort() throws IOException {
        int port = 9998;
        do {
            port++;
        } while (!isPortAvailable(port));

        return port;
    }

    public static boolean isPortAvailable(final int port) throws IOException {
        ServerSocket ss = null;
        try {
//            
            DatagramSocket socket = new DatagramSocket(port);
            socket.close();
            return true;

        } catch (final IOException e) {
        } finally {
            if (ss != null) {
                ss.close();
            }
        }

        return false;
    }

    private static void send_profile_update_request(int indexNumber, String value) {

        String pak_id = PackageActions.create_packed_id_for(SocketConstants.USER_PROFILE);
        JsonFields jF = new JsonFields();
        jF.setAction(SocketConstants.UPDATE);
        jF.setType(SocketConstants.USER_PROFILE);
        jF.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        jF.setPacketId(pak_id);
        jF.setNoOfHeaders("1");

        /*
         public static final int CONS_FIRST_NAME = 1;
         public static final int CONS_LAST_NAME = 2;
         public static final int CONS_GENDER = 3;
         public static final int CONS_COUNTRY = 4;
         public static final int CONS_MOBILE_PHONE = 5;
         public static final int CONS_EMAIL = 6;
         public static final int CONS_PRESENCE = 7;
         public static final int CONS_WHAT_IS_IN_UR_MIND = 8;
         public static final int CONS_FRIENDSHIP_STATUS = 9;
         public static final int CONS_CALLING_CODE = 10;
         public static final int CONS_PREV_PASS = 11;
         public static final int CONS_NEW_PASS = 12;
         public static final int CONS_PROFILE_IMAGE = 13;
         */
        if (indexNumber == SocketConstants.CONS_FIRST_NAME) {
            jF.setFirstName(value);
        }
        if (indexNumber == SocketConstants.CONS_LAST_NAME) {
            jF.setLastName(value);
        }
        if (indexNumber == SocketConstants.CONS_GENDER) {
            jF.setGender(value);
        }
        if (indexNumber == SocketConstants.CONS_COUNTRY) {
            jF.setCountry(value);
        }
        if (indexNumber == SocketConstants.CONS_MOBILE_PHONE) {
            jF.setMobilePhone(value);
        }
        if (indexNumber == SocketConstants.CONS_EMAIL) {
            jF.setEmail(value);
        }
        if (indexNumber == SocketConstants.CONS_WHAT_IS_IN_UR_MIND) {
            jF.setWhatisInYourMind(value);
        }
        if (indexNumber == SocketConstants.CONS_CALLING_CODE) {
            jF.setMobilePhoneDialingCode(value);
        }
        PackageActions.sendPacketAsString(jF);
    }

    public static BufferedImage scalled_image(String current_image_location, int image_height, int image_width) {
        BufferedImage img_2 = null;
        try {
            if (DesignClasses.file_exists(current_image_location)) {
                BufferedImage tmp = ImageIO.read(new File(current_image_location));
                img_2 = DesignClasses.scaleImage(image_height, image_width, tmp);
            } else {
                //    System.out.println("file not exits");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return img_2;
    }

    public JLabel create_image_from_url(String gender, String url, int left, int top, int image_height, int image_width) {
        BufferedImage img = null;
        String location = "";
        if (url.length() > 0) {
            String testString = url;
            String image_name = getImageName_from_url(testString);
            //   System.out.println("******************************" + image_name);
            File f = new File(dHelp.getDestinationFolder() + "\\" + image_name);
            if (f.exists()) {
                location = dHelp.getUser_images_location() + image_name;
            } else {
                location = DesignClasses.get_scaled_male_or_female_image(gender);
            }
        } else {
            location = DesignClasses.get_scaled_male_or_female_image(gender);
        }


        try {
            if (DesignClasses.file_exists(location)) {
                BufferedImage tmp = ImageIO.read(new File(location));
                img = DesignClasses.scaleImage(image_width, image_height, tmp);
            } else {
                //   System.out.println("file not exits");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        JLabel user_image = new JLabel(new ImageIcon((Image) img));
        user_image.setBounds(left, top, image_height, image_width);
        return user_image;

    }

    public static JLabel get_scale_image(int left, int top, int width, int height, String location) {

        BufferedImage img = null;
        try {
            if (DesignClasses.file_exists(location)) {
                BufferedImage tmp = ImageIO.read(new File(location));
                img = DesignClasses.scaleImage(width, height, tmp);
            } else {
                //  System.out.println("file not exits");
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        JLabel user_image = new JLabel(new ImageIcon((Image) img));
        user_image.setBounds(left, top, width, height);
        return user_image;
    }

    public static String getImageName_from_url(String image_url) {
        String img_2 = "";
        img_2 = image_url.substring(image_url.lastIndexOf('/') + 1);
        return img_2;
    }

    public static MapGroupResponse mappGroupResponse(String input) {
        MapGroupResponse dd = new MapGroupResponse();
        jsonLib = new GsonBuilder().serializeNulls().create();
        dd = jsonLib.fromJson(input.trim(), MapGroupResponse.class);
        return dd;
    }

    public static CreateGroup createGruopMapper(String input) {
        CreateGroup dd = new CreateGroup();
        jsonLib = new GsonBuilder().serializeNulls().create();
        dd = jsonLib.fromJson(input.trim(), CreateGroup.class);
        return dd;
    }

    public static RemoveGruop removeGruopMembersMapper(String input) {
        RemoveGruop dd = new RemoveGruop();
        jsonLib = new GsonBuilder().serializeNulls().create();
        dd = jsonLib.fromJson(input.trim(), RemoveGruop.class);
        return dd;
    }

    public static HeadersPOJO mapHeaders(String input) {
        HeadersPOJO dd = new HeadersPOJO();
        jsonLib = new GsonBuilder().serializeNulls().create();
        dd = jsonLib.fromJson(input.trim(), HeadersPOJO.class);
        return dd;
    }

    public static GroupDto map_into_GruopDto(String input) {
        GroupDto dd = new GroupDto();
        jsonLib = new GsonBuilder().serializeNulls().create();
        dd = jsonLib.fromJson(input.trim(), GroupDto.class);
        return dd;
    }

    public static String truncate_string(String value, int length) {
        if (value != null && value.length() > length) {
            value = value.substring(0, length);
        }
        return value;
    }
}
