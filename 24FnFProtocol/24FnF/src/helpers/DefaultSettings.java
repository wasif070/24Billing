/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import java.awt.Color;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 *
 * @author FaizAhmed
 */
public class DefaultSettings {

    public static final Integer DEFAULT_WIDTH = 304;
    public static final Integer DEFAULT_HEIGHT = 602;
    public static final Integer DEFAULT_LOCATION_LEFT = 500;
    public static final Integer DEFAULT_LOCATION_TOP = 50;
    public static final Integer DEFAULT_TOP_BAR = 50;
    public static final int texBoxHeight = 25;
    public static final int searchtextBoxWidth = 180;
    public static final int country_combox_width = 150;
    public static final int textBoxWidth = 150;
    //public static final Color MAIN_CONTAINER_COLOR = new Color(0xEFFFFF);
    //public static final Color topBarbackGroundColor = new Color(0xB5, 0xE0, 0xF6);
    // public static final Color mainBodybackGroundColor = new Color(0xE3, 0xF2, 0xFA);//EAF3F7//E3F2FA
    //public static final Color buttonBackGroundColor = new Color(0xE8, 0xF2, 0xFC);//EAF3F7//E3F2FA
    public static final Color textFieldBackGroundColor = new Color(0xFF, 0xFF, 0xFF);//EAF3F7//E3F2FA
    public static final Border thickBorder = new LineBorder(new Color(0x08, 0xAB, 0xED), 2);
    public static final Border thinBorder = new LineBorder(Color.LIGHT_GRAY, 1);
    public static final Color blue_border_color = new Color(0x08ABED);
    //  public static final Border thinBorder = new LineBorder(new Color(0x66, 0xCC, 0xFF), 1); //33CCFF//6699CC 66CCFF
    public static final Border border_color_1 = new LineBorder(new Color(0x08, 0xAB, 0xED), 1);
    public static final Border image_border_color = new LineBorder(new Color(0xFFFFFF), 1); //bbe8fb
    //  public static final Color backgroundColor_2 = new Color(0x08, 0xAB, 0xED);//[4:35:19 PM]  #5dc8f4
    public static final Color errorLabelColor = Color.RED;
    public static final int buttonDefaultHeight = 25;
    // public static final Color buttonDefaultcolor = new Color(0x08, 0xAB, 0xED);//D2ECF5;92D8F0//D4FAF6
    public static final Color friend_list_border_color = new Color(0xEDF0F1);
    // public static final Color text_color1 = new Color(0x8B, 0x89, 0x89); //8B8989
    public static final Color text_color2 = new Color(0x8B8989); //8B8989
    public static final Color text_color3 = new Color(0xCDC9CF); //CDC9C9 [3:35:18 PM] Miraz Mahamud: <color name="text_color1">#3C3939</color>
    public static final Color text_color1 = new Color(0x3C3939);
    public static final Color text_color4 = new Color(0x4D4DFF); //#7B68EE//0000CD //4D4DFF
    // <color name="text_color2">#8B8989</color>
    public static final Color text_color_normal = new Color(0x000001); //CDC9C9
    //  public static final Color WHITE = new Color(0xFF, 0xFF, 0xFF); //CDC9C9
    //  public static final int JLABEL_WIDTH = 20;
    public static final Border bottom_border = new LineBorder(new Color(0xEDF0F1), 1);
    public static final int DEFAULT_FONT = 12;
    // public static final Color DEFAULT_FONT_COLOR = Color.GRAY;
    //  public static final Color bg_color_3 = new Color(0x3B, 0x59, 0x98);//#3B5998
    //public static final Color font_color_white = Color.WHITE;
    public static final Integer DEFAULT_PREFIX_TEXT_BOX_WIDTH = 50;
    //public static final Integer DEFAULT_MOBILE_TEXTBOX_WIDTH = 55;
    public static final Color blue_bar_background_selected = new Color(0x88E1FF);//#88E1FF
    public static final Color blue_bar_background = new Color(0x0198E1);// #0198E1
    public static final Color calling_window_background_color = new Color(0x0198e1);// #0198E1//#0f315b//3366FF
    public static final Border calling_window_border_color = new LineBorder(new Color(0x0F31DB), 1);
    public static final Color disable_font_color = new Color(0xC4C4C4);// #0198E1//#0f315b//3366FF
    public static final Color topTab_background = new Color(0x87CEEB);// #0198E1
    public static final Color topTab_background_hover = new Color(0xF8F8FF);// #0198E1
    public static final Border border_light_blue = new LineBorder(new Color(0xE6E6FA));
    public static final Color text_color_light_blue = new Color(0x1E90FF);
    public static final Color table_even_background = new Color(0xFFFAFA);// #0198E1
    public static final Color table_odd_background = new Color(0xFFFFFF);// #0198E1
    public static final Color bar_bg_color = new Color(0x003399);// #0198E1//#0f315b//3366FF
    public static final Color button_bg_color = new Color(0xD8D8D8);
}
