/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author FaizAhmed
 */
public class CallLog {
    /*
     * http://192.168.1.110:8080/rest/getcallLogService?sessionId=930217gggg out
     * put:
     * {"callLog":[{"callType":1,"phoneNo":"","callDuration":10000,"callingTime":1234567891234},{"callType":1,"phoneNo":"","callDuration":10000,"callingTime":1234567891234},{"callType":1,"phoneNo":"","callDuration":10000,"callingTime":1234567891234},{"callType":1,"phoneNo":"","callDuration":10000,"callingTime":1234567891234},{"callType":1,"phoneNo":"","callDuration":100,"callingTime":1234567891234},{"callType":1,"phoneNo":"faiz","callDuration":100,"callingTime":1234567891234},{"callType":1,"phoneNo":"faiz","callDuration":10000,"callingTime":1234567891234}],"success":true}
     */

    private Integer callType = 0;
    private String phoneNo = "";
    private Long callDuration;
    private Long callingTime;
    public Long getCallDuration() {
        return callDuration;
    }

    public void setCallDuration(Long callDuration) {
        this.callDuration = callDuration;
    }

    public Integer getCallType() {
        return callType;
    }

    public void setCallType(Integer callType) {
        this.callType = callType;
    }

    public Long getCallingTime() {
        return callingTime;
    }

    public void setCallingTime(Long callingTime) {
        this.callingTime = callingTime;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
    
    
}
