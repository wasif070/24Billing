/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author Ashraful
 */
public interface JSONResponseListener {

    /** When a new JSON Response is received. */
    public void onReceivedResponse(JSONResponseProvider provider, String response,String request_url);
}
