/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

/**
 *
 * @author FaizAhmed
 */
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CheckInternetConnection {

    public static boolean internet_status = false;
    // String host = MyFnFSettings.API_SERVER_IP;

    public boolean check_internet_connection() {

        int timeOut = 3000;
        try {
            internet_status = InetAddress.getByName(MyFnFSettings.API_SERVER_IP).isReachable(timeOut);
        } catch (IOException ex) {
            Logger.getLogger(CheckInternetConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return internet_status;
    }

    public static int getResponseCode(String urlString) throws MalformedURLException, IOException {
        URL u = new URL(urlString);
        HttpURLConnection huc = (HttpURLConnection) u.openConnection();
        huc.setRequestMethod("GET");
        huc.connect();
        return huc.getResponseCode();
    }
}
