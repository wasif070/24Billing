/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import myfnfui.JsonFields;
import myfnfui.packetsInfo.PackageActions;

/**
 *
 * @author FaizAhmed
 */
public class CallLogHelper extends HttpRequest {

    public static Gson jsonLib;

    public CallLogHelper() {
        jsonLib = new GsonBuilder().serializeNulls().create();
    }
    public static final String DIR_callInitiatorService = "callInitiatorService";
    public static final String DIR_callTerminatorService = "callTerminatorService";
    public static final String DIR_getcallLogService = "getcallLogService";
    /*call log*/
    public static final String PARAM_calldetails = "calldetails";
    public static final String PARAM_duration = "duration";
    public static final String PARAM_callId = "callId";
    public static String callId;
    public static String callFriend_id;
    public static String called_in;

    public static String get_call_log() {
//http://192.168.1.110:8080/rest/getcallLogService?sessionId=930217gggg
        String url = MyFnFSettings.API_HOST + DIR_getcallLogService + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId();
        String dd = getJson(url);
        return dd;
    }

    public static Boolean sent_call_log_to_server_in_call_start(String friend_id, String phone_no, Long duration) {

//http://192.168.1.110:8080/rest/callInitiatorService?sessionId=226032gggg&calldetails={"friendId":"faiz","phoneNo":"faiz", "duration":"10000","callingTime":"1234567891234"}

        Boolean retrun_value = true;
//        "action":"CALL", 
//"type":"call_start", 
//"packetId":"packetId",                   //gen
//"sessionId":"sessionid", 
//"friendId":"faiz", 
//"phoneNo":"faiz",  
//"duration":"10000", 
//"callingTime":"1234567891234", 
//"callId":"63841553516gggggr5eb8u" 
        callFriend_id = friend_id;
        called_in = phone_no;
        callId = friend_id + "_" + SocketConstants.CALL + System.currentTimeMillis();
        String pak = PackageActions.create_packed_id_for(SocketConstants.CALL_START);
        JsonFields js = new JsonFields();
        js.setAction(SocketConstants.CALL);
        js.setType(SocketConstants.CALL_START);
        js.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
        js.setFriendId(callFriend_id);
        js.setCallingTime(System.currentTimeMillis());
        js.setDuration(duration);
        js.setPhoneNo(called_in);
        js.setCallId(callId);
        js.setPacketId(pak);
        PackageActions.sendPacketAsString(js);

        return retrun_value;
    }

    public static Boolean sent_call_log_to_server_in_call_end(String friend_id, String phone_no, Long duration) {
//http://192.168.1.110:8080/rest/callTerminatorService?sessionId=930217gggg&duration=1000000&callId=63841553516gggggr5eb8u
        Boolean ret = true;
        
        if (callId != null) {
            String pak = PackageActions.create_packed_id_for(SocketConstants.CALL_END);
            JsonFields js = new JsonFields();
            js.setAction(SocketConstants.CALL);
            js.setType(SocketConstants.CALL_END);
            js.setSessionId(MyFnFSettings.LOGIN_SESSIONID);
            js.setFriendId(friend_id);
            js.setCallingTime(System.currentTimeMillis());
            js.setDuration(duration);
            js.setCallId(callId);
            js.setPacketId(pak);
            PackageActions.sendPacketAsString(js);
            callId = null;
            callFriend_id = null;
            called_in = null;
//            String url = MyFnFSettings.API_HOST + DIR_callTerminatorService + "?" + APIParametersFiles.PARAM_sessionId + "=" + MyFnFSettings.userProfile.getSessionId() + "&" + PARAM_duration + "=" + duration + "&" + PARAM_callId + "=" + callId;
//            String get_string = getJson(url);
//            if (get_string.length() > 0) {
//                //System.out.println(url);
//                //System.out.println("Termination response: " + get_string);
//                callId = null;
//                if (HelperMethods.check_string_contains_substring(get_string, "false")) {
//                    ret = false;
//                }
//            }
        }
        return ret;
    }
}
