/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package local.sbc;

import java.io.*;
import java.net.*;

public class MessageMangler {

    public static void sendMsgToMediaGateway(String msg, String ip, int port, int is_route_call) {
        try {
            DatagramSocket socket = new DatagramSocket();
            byte sendData[] = new byte[msg.length() + 1];
//            sendData = msg.getBytes();
            sendData[0] = (byte) is_route_call;
            System.arraycopy(msg.getBytes(), 0, sendData, 1, msg.length());
            socket.send(new DatagramPacket(sendData, sendData.length, InetAddress.getByName(ip), port));
//            socket.send(new DatagramPacket(sendData, sendData.length, InetAddress.getByName(ip), port));
            socket.close();
        } catch (IOException ex) {
            System.out.println("Error in sending msg to MG-->" + ex.getMessage());
        }
    }
}
