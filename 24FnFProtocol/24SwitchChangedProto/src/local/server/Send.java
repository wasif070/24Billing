/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package local.server;

import databaseconnector.DBConnection;
import java.io.*;
import java.net.*;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.provider.SipParser;

public class Send {

    static Logger logger = Logger.getLogger(Send.class.getName());

    public static void sendEventMsg(String msg, String[] remote_domains, int remote_port) {
        try {

            DatagramSocket socket = new DatagramSocket();
            byte sendData[] = new byte[msg.length()];
            sendData = msg.getBytes();
            for (int i = 0; i < remote_domains.length; i++) {
                socket.send(new DatagramPacket(sendData, sendData.length, InetAddress.getByName(remote_domains[i]), remote_port));
            }
            socket.close();

        } catch (IOException ex) {
            System.out.println("Error in sending msg: " + ex.getMessage());
        }
    }
        public static void sendConfirmationMsg(String msg, String[] remote_domains, int remote_port, DatagramSocket socket) {
        try {
            //DatagramSocket socket = new DatagramSocket();
            byte sendData[] = new byte[msg.length()];
            sendData = msg.getBytes();
            for (int i = 0; i < remote_domains.length; i++) {
                socket.send(new DatagramPacket(sendData, sendData.length, InetAddress.getByName(remote_domains[i]), remote_port));
            }
            //socket.close();

        } catch (IOException ex) {
            System.out.println("Error in sending msg: " + ex.getMessage());
        }
    }

    public static void sendRegisteredUsers(InetAddress remote_server, int remote_port) {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            rs = stmt.executeQuery("select * from live_users");

            DatagramSocket socket = new DatagramSocket();

            while (rs.next()) {
                String line = "To: " + rs.getString("user_name") + "@" + rs.getString("contact_ip") + "\r\n";
                line += BaseSipHeaders.Contact + SipParser.header_colon + " " + SipParser.bracket_header_start + rs.getString("contact") + SipParser.bracket_header_end + ";expires=\"" + rs.getString("expire_time") + "\"" + "\r\n";
                byte sendData[] = new byte[line.length()];
                sendData = line.getBytes();
                socket.send(new DatagramPacket(sendData, sendData.length, remote_server, remote_port));
            }

            socket.close();

        } catch (Exception ex) {
            System.out.println("Error in sendRegisteredUsers: " + ex.getMessage());
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                }
            } catch (Exception ex) {
            }
        }
    }
}
