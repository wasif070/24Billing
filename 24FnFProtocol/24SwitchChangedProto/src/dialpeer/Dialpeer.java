/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dialpeer;

import databaseconnector.DBConnection;
import org.zoolu.net.SocketAddress;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import local.server.ProxyingRule;
import local.server.ServerProfile;
import org.apache.log4j.Logger;
import org.zoolu.tools.Parser;
import local.server.PrefixProxyingRule;

/**
 *
 * @author Ashraful
 */
public class Dialpeer {

    static Logger logger = Logger.getLogger(Dialpeer.class.getName());

    public Dialpeer() {
    }

    public static void getDialPeers(ServerProfile serverProfile) {
        DBConnection con = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Vector aux = new Vector();
            con = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = con.connection.createStatement();
            String sql = "select dialpeer_prefix,dialpeer_term_ip,dialpeer_term_port from dialpeers where dialpeer_status=1";
            rs = stmt.executeQuery(sql);

            while (rs.next()) {
                String prefix = rs.getString("dialpeer_prefix");
                String nexthop = rs.getString("dialpeer_term_ip") + ":" + rs.getString("dialpeer_term_port");
                aux.addElement(new PrefixProxyingRule(prefix, new SocketAddress(nexthop)));
            }

            serverProfile.phone_proxying_rules = new ProxyingRule[aux.size()];
            for (int i = 0; i < aux.size(); i++) {
                serverProfile.phone_proxying_rules[i] = (ProxyingRule) aux.elementAt(i);
            }

        } catch (Exception ex) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
            }
            try {
                if (con != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(con);
                }
            } catch (Exception ex) {
            }
        }
    }
}
