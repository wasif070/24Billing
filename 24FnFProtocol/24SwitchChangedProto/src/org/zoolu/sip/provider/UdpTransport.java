/*
 * Copyright (C) 2005 Luca Veltri - University of Parma - Italy
 * 
 * This file is part of MjSip (http://www.mjsip.org)
 * 
 * MjSip is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * MjSip is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MjSip; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
 * Author(s):
 * Luca Veltri (luca.veltri@unipr.it)
 */
package org.zoolu.sip.provider;

import header.FnFtoSIP;
import header.HeaderLoader;
import header.SIPtoFnF;
import org.zoolu.net.*;
import org.zoolu.sip.message.Message;
import java.io.IOException;
import local.server.ServerProfile;
import org.apache.log4j.Logger;
import org.zoolu.sip.header.BaseSipHeaders;
import org.zoolu.sip.utils.IpConverter;

/**
 * UdpTransport provides an UDP transport service for SIP.
 */
public class UdpTransport implements Transport, UdpProviderListener {

    Logger logger = Logger.getLogger(UdpTransport.class.getName());
    /**
     * UDP protocol type
     */
    public static String PROTO_UDP = HeaderLoader.getInstance().getHeader("udp");
    /**
     * UDP provider
     */
    UdpProvider udp_provider;
    /**
     * Transport listener
     */
    TransportListener listener = null;

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(UdpSocket socket) {
        udp_provider = new UdpProvider(socket, this);
    }

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(int local_port) throws IOException {
        initUdp(local_port, null);
    }

    /**
     * Creates a new UdpTransport
     */
    public UdpTransport(int local_port, IpAddress host_ipaddr) throws IOException {
        initUdp(local_port, host_ipaddr);
    }

    /**
     * Inits the UdpTransport
     */
    protected void initUdp(int local_port, IpAddress host_ipaddr) throws IOException {
        if (udp_provider != null) {
            udp_provider.halt();
        }
        // start udp
        UdpSocket socket = (host_ipaddr == null) ? new UdpSocket(local_port) : new UdpSocket(local_port, host_ipaddr);
        //UdpSocket socket=(host_ipaddr==null)? new org.zoolu.net.JumboUdpSocket(local_port,500) : new org.zoolu.net.JumboUdpSocket(local_port,host_ipaddr,500);
        udp_provider = new UdpProvider(socket, this);

    }

    /**
     * Gets protocol type
     */
    public String getProtocol() {
        return PROTO_UDP;
    }

    /**
     * Gets port
     */
    public int getLocalPort() {
        try {
            return udp_provider.getUdpSocket().getLocalPort();
        } catch (Exception e) {
            return 0;
        }

    }

    /**
     * Sets transport listener
     */
    public void setListener(TransportListener listener) {
        this.listener = listener;
    }

    /**
     * Sends a Message to a destination address and port
     */
    @Override
    public TransportConn sendMessage(Message msg, IpAddress dest_ipaddr, int dest_port, int ttl) throws IOException {
        for (int i = 0; i < ServerProfile.phone_proxying_rules.length; i++) {
            if (ServerProfile.phone_proxying_rules[i].toString().contains(dest_ipaddr.toString())) {
                try {
                    String switchIP = ServerProfile.host_ip;
                    String routeIP = String.valueOf(IpConverter.ipToLong(dest_ipaddr.toString()));
                    String call_id = msg.getHeader(BaseSipHeaders.Call_ID).getValue();
                    String userIP = call_id.substring(call_id.indexOf("@") + 1, call_id.length());
                    //String[] call_id = msg.getHeader(BaseSipHeaders.Call_ID).getValue().split("@");
                    //String userIP = call_id[1];
                    String mediaIP = String.valueOf(IpConverter.ipToLong(ServerProfile.media_gateway_addr));
                    if (FnFtoSIP.recv_long_ip == null) {
                        FnFtoSIP.recv_long_ip = "";
                    }
                    msg.setMessage(FnFtoSIP.changeToSIP(msg.toString(), switchIP, routeIP, userIP, mediaIP));
                    
                } catch (Exception e) {                
                    logger.debug("UdpTransport: sendMessage FnFtoSIP msg conversion error--> Error:" + e.getMessage() + "\n" + msg.toString().replaceAll("  ", "\n") + "\n\n");
                }
            }
        }
        //System.out.println("SEND-->" +  msg.toString().replaceAll("  ", "\n") + "\n");
        if (udp_provider != null) {
            byte[] data = msg.toString().getBytes();
            UdpPacket packet = new UdpPacket(data, data.length);
            packet.setIpAddress(dest_ipaddr);
            packet.setPort(dest_port);
            udp_provider.send(packet);
        }
        return null;
    }

    /**
     * Stops running
     */
    public void halt() {
        if (udp_provider != null) {
            udp_provider.halt();
        }
    }

    /**
     * Gets a String representation of the Object
     */
    public String toString() {
        if (udp_provider != null) {
            return udp_provider.toString();
        } else {
            return null;
        }
    }

    //************************* Callback methods *************************
    /**
     * When a new UDP datagram is received.
     */
    public void onReceivedPacket(UdpProvider udp, UdpPacket packet) {
        Message msg = new Message(packet);

        for (int i = 0; i < ServerProfile.phone_proxying_rules.length; i++) {
            //if (msg.toString().contains("sip")) {
            if (ServerProfile.phone_proxying_rules[i].toString().contains(packet.getIpAddress().toString())) {
                try {
                    String switchIP = IpConverter.longToIp(Long.valueOf(ServerProfile.host_ip));
                    String routeIP = packet.getIpAddress().toString();
                    String call_id = msg.getHeaderSIP("Call-ID").getValue();
                    String userIP = call_id.substring(call_id.indexOf("@") + 1, call_id.length());
                    //String[] call_id = msg.getHeaderSIP("Call-ID").getValue().split("@");
                    //String userIP = call_id[1];
                    String mediaIP = ServerProfile.media_gateway_addr;
                    if (SIPtoFnF.recv_ip == null) {
                        SIPtoFnF.recv_ip = "";
                    }
                    msg.setMessage(SIPtoFnF.changeToFnF(msg.toString(), switchIP, routeIP, userIP, mediaIP));
                    
                } catch (Exception e) {
                    logger.debug("UdpTransport: onReceivedPacket SIPtoFnF msg conversion error--> Error:" + e.getMessage() + "\n" + msg.toString().replaceAll("  ", "\n") + "\n\n");
                }
            }
        }
        //System.out.println("RCV-->" + msg.toString().replaceAll("  ", "\n") + "\n");
        msg.setRemoteAddress(packet.getIpAddress().toString());
        msg.setRemotePort(packet.getPort());
        msg.setTransport(PROTO_UDP);
        if (listener != null) {
            listener.onReceivedMessage(this, msg);
        }
    }

    /**
     * When DatagramService stops receiving UDP datagrams.
     */
    public void onServiceTerminated(UdpProvider udp, Exception error) {
        if (listener != null) {
            listener.onTransportTerminated(this, error);
        }
        UdpSocket socket = udp.getUdpSocket();
        if (socket != null) {
            try {
                socket.close();
            } catch (Exception e) {
            }
        }
        this.udp_provider = null;
        this.listener = null;
    }
}
