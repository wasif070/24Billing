<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOList();
%>
<html>
    <head>
        <title>24Billing :: Add Gateway</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("gateway/listGateway.do?list_all=1;Gateway");
                        navList.add(";Add New Gateway");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/gateway/addGateway" method="post">   
                        <%
                            int type = -1;
                        %>
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Add New Gateway IP</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="GatewayForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Gateway IP <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="gateway_ip" /><br/>
                                        <html:messages id="gateway_ip" property="gateway_ip">
                                            <bean:write name="gateway_ip"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Gateway Type <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:select property="gateway_type" styleId="jgateway_type" onchange="value">
                                            <html:option value="-1">Select Gateway Type</html:option>       
                                            <%
                                                for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="gateway_type" property="gateway_type">
                                            <bean:write name="gateway_type"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Destination Port <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="gateway_dst_port_h323" value="1720" styleId="gateway_dst_port_h323" disabled="<%=type == -1 ? true : false%>"/><br/>
                                        <html:messages id="gateway_dst_port_h323" property="gateway_dst_port_h323">
                                            <bean:write name="gateway_dst_port_h323"  filter="false" />
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Destination Port SIP <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="gateway_dst_port_sip" value="5060"  styleId="gateway_dst_port_sip" disabled="<%=type == -1 ? true : false%>"/><br/>
                                        <html:messages id="gateway_dst_port_sip" property="gateway_dst_port_sip">
                                            <bean:write name="gateway_dst_port_sip"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Client <span class="req_mark">*</span></th>
                                    <td valign="top">
                                        <html:select property="clientId" styleId="jclientId">
                                            <html:option value="">Select Client</html:option>                                            
                                        </html:select><br/>
                                        <html:messages id="clientId" property="clientId">
                                            <bean:write name="clientId"  filter="false"/>
                                        </html:messages>
                                    </td>                             
                                </tr>
                                <tr>
                                    <th valign="top" >Gateway Name <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="gateway_name" /><br/>
                                        <html:messages id="gateway_name" property="gateway_name">
                                            <bean:write name="gateway_name"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:select property="gateway_status">
                                            <%
                                                for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="gateway_status" property="gateway_status">
                                            <bean:write name="gateway_status"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>

    <script language="javascript" type="text/javascript">
        $j(document).ready(function(){
            gateway1.init();            
            if($j("#jgateway_type").val()>-1){
            $j.ajax({
                type:'post',
                url:'../gateway/load_client.jsp',
                data:{
                    type:$j("#jgateway_type").val()
                },
                success:function(html){
                    $j("#jclientId").html(html);
                }
            });
        }
    });
    </script>
</html>
