<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>

        <%
            int pageNo = 1;
            int sortingOrder = 0;
            int sortedItem = 0;
            int list_all = 0;
            int recordPerPage = 10;

            if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
               boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                 if(status==true){
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                       }
                 }
            if (request.getParameter("d-49216-s") != null) {
                sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
            }
            if (request.getParameter("d-49216-o") != null) {
                sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
            }
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }else{
                msg = "<div class='success'>"+msg+"</div>";
            }
        %>
        <title>24Billing :: Rate Plan List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("rateplan/listRateplan.do?list_all=1;Rate Plan");
                    navList.add(";");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>

                    <html:form action="/rateplan/listRateplan.do" method="post" >
                        <div class="half-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Rate Plan Name</th>
                                    <td><html:text property="rateplan_name" /></td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="full-div">
                                            <html:submit styleClass="search-button" value="Search" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <html:form action="/rateplan/multipleRateplan" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                        <div class="display_tag_content" align="center">
                            <bean:write name="RateplanForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <%=msg%>
                            <script type="text/javascript">count=<%=(pageNo - 1) * recordPerPage%>;</script>
                             <div class="over_flow_content_list">
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.RateplanForm.rateplanList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Rate Plan" /> 
                                <display:setProperty name="paging.banner.items_name" value="Rate Plans" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.rateplan_id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Rate Plan Name" sortProperty="rateplan_name" sortable="true"  style="width:20%" >
                                    <a href="../rates/listRate.do?list_all=1&amp;rate_id=${data.rateplan_id}">${data.rateplan_name}</a>
                                </display:column>
                                <display:column  property="rateplan_des" title="Description" sortable="true" style="width:20%" />
                                <display:column  property="user_name" class="center-align" title="User Name" sortable="true" style="width:15%" />
                                <display:column  property="rateplan_create_date" class="center-align" title="Created Date" sortable="true" style="width:15%" />
                                <display:column  property="rateplan_statusname" class="center-align" title="Status" sortable="true" style="width:10%" />
                                <display:column class="center-align" title="Task" style="width:10%;" >
                                    <a href="../rateplan/getRateplan.do?id=${data.rateplan_id}" class="edit"  title="Change"></a>
                                </display:column>
                            </display:table>
                             </div>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>