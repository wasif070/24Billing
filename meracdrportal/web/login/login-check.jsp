<%@page language="java"%>
<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>

<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>

<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants" %>

<%
    LoginDTO login_dto = null;
    if (request.getSession(true).getAttribute(Constants.LOGIN_DTO) != null) {
        login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
    }
    if (login_dto == null) {
        request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
        request.getSession(true).setAttribute(Constants.LOGIN_TIMEOUT, "yes");
        response.sendRedirect("../index.do");
        return;
    } else {
        if (request.getParameter("logout") != null) {
            login_dto = null;
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            response.sendRedirect("../index.do");
            return;
        }
        if ((login_dto.getLoginTime() + Constants.LOGIN_EXPIRE_TIME) < System.currentTimeMillis()) {
            login_dto = null;
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_TIMEOUT, "yes");
            response.sendRedirect("../index.do");
            return;
        } else {
            login_dto.setLoginTime(System.currentTimeMillis());
            request.getSession(true).setAttribute(Constants.LOGIN_DTO, login_dto);
        }
    }
%>
