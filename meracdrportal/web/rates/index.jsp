<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>

        <%
            int recordPerPage = 10;
            String currentPage = request.getParameter("page") == null ? "1" : request.getParameter("page");
            if (request.getParameter("d-49216-p") != null) {
                 boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                  if(status==true){
                    currentPage = request.getParameter("d-49216-p");
                     }
              }
            if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
            }

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } else {
                msg = "<div class='success'>" + msg + "</div>";
            }
        %>
        <title>24Billing :: Rate List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("rates/listRate.do?list_all=1&rate_id=1;Rate");
                    navList.add(";Rate List");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>

                    <html:form action="rates/listRate.do" method="post" >
                        <div class="half-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Prefix</th>
                                    <td><html:text property="rate_destination_code" /></td>
                                </tr>
                                <tr class="even">
                                    <th>Record Per Page</th>
                                    <td>
                                        <input type="text" name="recordPerPage" value="<%=recordPerPage%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="page" value="<%=currentPage%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="full-div">
                                            <html:submit styleClass="search-button" property="doSearch" value="Search" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>         
                    <div class="full-div height-5px"></div>
                    <div class="pagination center-align"><%=request.getSession(true).getAttribute("PAGER").toString()%></div>
                    <html:form action="/rates/multipleRate" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                        <div class="display_tag_content" align="center">
                            <ul class="nav_menu">
                                <li><a href="../rates/new_rate.jsp">Add Rate</a></li>
                                <li><a href="../rates/upload_rate.jsp">Upload Rate</a></li>
                                <li><a href="../rates/download.do">Download Rate</a></li>
                            </ul>
                            <bean:write name="RateForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <%=msg%>
                            <script type="text/javascript">count=<%=(Integer.valueOf(currentPage) - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="requestScope.RateForm.rateList"  requestURI="/rates/listRate.do" >
                                <display:setProperty name="paging.banner.item_name" value="Rate" /> 
                                <display:setProperty name="paging.banner.items_name" value="Rates" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.rate_id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Prefix" sortProperty="rate_destination_code" sortable="true"  style="width:20%" >
                                    ${data.rate_destination_code}
                                </display:column>
                                <display:column  property="rate_destination_name" title="Destination Name" sortable="true" style="width:20%" />
                                <display:column  property="rate_per_min" class="center-align" title="Rate (Per Min)" sortable="true" style="width:15%" />
                                <display:column  property="rate_first_pulse" class="center-align" title="First Pulse" sortable="true" style="width:15%" />
                                <display:column  property="rate_next_pulse" class="center-align" title="Next Pulse" sortable="true" style="width:15%" />
                                <display:column  property="rate_grace_period" class="center-align" title="Grace Period" sortable="true" style="width:10%" />
                                <display:column  property="rate_created_date" class="center-align" title="Date" sortable="true" style="width:10%" />
                                <display:column class="center-align" title="Task" style="width:10%;" >
                                    <a href="../rates/getRate.do?id=${data.rate_id}" class="edit"  title="Change"></a>
                                </display:column>
                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                <html:submit property="deleteBtn" styleClass="custom-button" value="Delete Selected" />
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
