<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<html>
    <head>
        <title>24Billing :: Add User</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                     <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("user/listUser.do?list_all=1;User");
                    navList.add(";Add User");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/user/addUser" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Add New User</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center" valign="bottom">
                                        <bean:write name="UserForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">User ID</th>
                                    <td valign="top">
                                        <html:text property="userId" /><br/>
                                        <html:messages id="userId" property="userId">
                                            <bean:write name="userId"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Password</th>
                                    <td valign="top">
                                        <html:password property="userPassword" /><br/>
                                        <html:messages id="userPassword" property="userPassword">
                                            <bean:write name="userPassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Retype Password</th>
                                    <td valign="top">
                                        <html:password property="retypePassword" /><br/>
                                        <html:messages id="retypePassword" property="retypePassword">
                                            <bean:write name="retypePassword"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Full Name</th>
                                    <td valign="top">
                                        <html:text property="fullName" /><br/>
                                        <html:messages id="fullName" property="fullName">
                                            <bean:write name="fullName"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top">Status</th>
                                    <td valign="top">
                                        <html:select property="userStatus">
                                            <%
                                                for (int i = 0; i < Constants.USER_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.USER_STATUS_VALUE[i]%>"><%=Constants.USER_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="userStatus" property="userStatus">
                                            <bean:write name="userStatus"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <input type="hidden" name="searchLink" value="nai" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>