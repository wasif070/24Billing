<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.gateway.GatewayDTO" %>
<%
    String output = "";
    int client_id = 0;
    int type = 0;

    if (request.getParameter("client_id") != null) {
        client_id = Integer.parseInt(request.getParameter("client_id"));
    }

    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }

    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(client_id);
    output += "<option value=\"\">All</option>";
    if (clientGateway != null && clientGateway.size() > 0) {
        int size = clientGateway.size();
        for (int i = 0; i < size; i++) {
            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
            if (c_dto.getGateway_type() == type) {
                output += "<option value=\"" + c_dto.getGateway_ip() + "\">" + c_dto.getGateway_ip() + "</option>";
            }
        }
    }

%>
<%=output%>