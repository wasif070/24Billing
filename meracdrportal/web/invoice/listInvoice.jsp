<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }
%>
<html>
    <head>
        <title>24Billing :: Invoice List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("invoice/listinvoice.do?list_all=1;Invoice");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/invoice/listinvoice.do" method="post" >
                        <div class="half-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Invoice Number</th>
                                    <td><html:text property="id" /></td>
                                </tr>
                                <tr>
                                    <th>Client ID</th>
                                    <td>
                                        <html:text property="client" />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="full-div">
                                            <html:submit styleClass="search-button" value="Search" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>                   
                        <html:form action="/invoice/deleteinvoice" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                            <div class="display_tag_content" align="center">                                
                                <div class="jerror_messge"><%=msg%></div> 
                                <div class="over_flow_content_list">
                                <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.InvoiceForm.invoicelist"  pagesize="<%=Integer.parseInt(String.valueOf(recordPerPage))%>" >
                                    <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='One Dial Plan' />" style="width:5%">
                                        <input type="checkbox" name="ids" value="${data.id}" class="select_id"/>
                                    </display:column>
                                    <display:column  title="Invoice Id"  sortable="true"  style="width:15%" >${data.id} </display:column>
                                    <display:column   class="" title="Client" sortable="true" style="width:18%">${data.client}</display:column>
                                    <display:column   class="" title="Client Name" sortable="true" style="width:18%">${data.clientName}</display:column>
                                    <display:column   class="left-align" title="From Date" sortable="true" style="width:18%">${data.startDate}</display:column>
                                    <display:column   class="center-align" title="To Date" sortable="true" style="width:10%">${data.endDate} </display:column>
                                    <display:column   class="center-align" title="Invoice Date" sortable="true" style="width:10%" >${data.invoiceDate}</display:column>
                                    <display:column  class="center-align" title="Task" style="width:10%;" >
                                        <a href="../invoice/invoiceDetail.do?id=${data.id}" class="detail"  title="Details"></a>
                                        <a href="../invoice/download.do?id=${data.id}" class="download" title="Download"></a>
                                    </display:column>
                                </display:table>
                                </div>
                                <%
                            request.getSession(true).removeAttribute(Constants.MESSAGE);
                                %>
                            </div>
                            <div class="button_area">
                                <html:submit property="deleteBtn" styleClass="custom-button1 jmultipebtn" value="Delete Selected" />
                            </div>
                            <div class="blank-height"></div>
                        </html:form>  
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>