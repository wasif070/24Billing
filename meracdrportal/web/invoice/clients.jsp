<%@page import="com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO,java.util.ArrayList" %>
<%
    String output = "";
    try {
        int searchText = Integer.parseInt(request.getParameter("text"));
        ArrayList<ClientDTO> data = ClientLoader.getInstance().getClientDTOByType(searchText);
        if (data != null && data.size() > 0) {
            output = "<select name=\"clientId\">";
            for (int i = 0; i < data.size(); i++) {
                ClientDTO dto = data.get(i);
                output += "<option value=\"" + dto.getId() + "\" class =\"customer\" >" + dto.getClient_id() + "</option>";
            }
            output += "</select>";
        }
    } catch (Exception ex) {
        output = ex.toString();
    }
%>
<%=output%>