----2011-05-16---
ALTER TABLE `users` ADD `user_delete` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `user_status` ;
----2011-05-19---
RENAME TABLE `client_prefix` TO `incoming_prefix` ;
ALTER TABLE `incoming_prefix` ADD `incoming_to` VARCHAR( 20 ) NOT NULL AFTER `prefix`;
ALTER TABLE `incoming_prefix` CHANGE `prefix` `incoming_prefix` VARCHAR( 20 ) NOT NULL ;

----2011-06-27-----
ALTER TABLE `mvts_rateplan` ADD `rateplan_create_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `rateplan_delete` ;
ALTER TABLE `mvts_rateplan` ADD `user_id` INT NOT NULL AFTER `rateplan_create_date` ;

----2011-07-12-----
ALTER TABLE `mvts_rates` ADD `rate_created_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `rate_grace_period` ;
ALTER TABLE `mvts_rates` ADD `rate_delete` TINYINT( 1 ) NOT NULL DEFAULT '0' AFTER `rate_status` ;

-----2011-07-14----
ALTER TABLE `clients` ADD `rateplan_id` INT NOT NULL AFTER `client_id` ;

-----2011-07-26-----
ALTER TABLE `mvts_rates` CHANGE `rate_id` `id` BIGINT( 11 ) NOT NULL AUTO_INCREMENT ;
ALTER TABLE `mvts_rates` ADD `rate_id` INT NOT NULL DEFAULT '0' AFTER `id` ;

-----2011-07-28---------
ALTER TABLE `flamma_cdr` ADD `origin_client_name` VARCHAR( 150 ) NULL DEFAULT NULL AFTER `origin_client_id` ;
ALTER TABLE `flamma_cdr` ADD `term_client_name` VARCHAR( 150 ) NULL DEFAULT NULL AFTER `term_client_id` ;
ALTER TABLE `mvts_rateplan`
  DROP `rateplan_firstpulse`,
  DROP `rateplan_pulse`;

ALTER TABLE `flamma_cdr` ADD `origin_bill_amount` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0' AFTER `origin_rate` ;
ALTER TABLE `flamma_cdr` ADD `term_bill_amount` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0' AFTER `term_rate` ;

-----2011-08-02----------
ALTER TABLE `clients` CHANGE `client_balance` `client_balance` DECIMAL( 10, 2 ) NOT NULL DEFAULT '0';