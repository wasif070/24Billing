<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<html>
    <head>
        <title>24Billing :: Settings</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <%
       java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
       navList.add("settings/listSettings.do?list_all=1;Settings");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/settings/listSettings" method="post" >
                        <thead>
                            <tr><th colspan="2" ><h3>Settings</h3></th></tr>
                        </thead>
                        <div class="display_tag_content" align="center">
                            <bean:write name="SettingsForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <bean:write name="SettingsForm" property="message" filter="false"/>

                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.SettingsForm.settingsList" >
                                <display:setProperty name="paging.banner.item_name" value="Gateway" /> 
                                <display:setProperty name="paging.banner.items_name" value="Gateways" />                               
                                <display:column  class="center-align" sortable="true" style="width:15%" >
                                    ${data.settingName}   
                                </display:column> 
                                <display:column  class="center-align" sortable="true" style="width:15%" >                            
                                    ${data.settingValue}
                                </display:column>             
                                <display:column class="center-align" title="Task" style="width:10%;" >
                                    <a href="../settings/getSettings.do?id=${data.id}" class="edit"  title="Change"></a>                                   
                                </display:column>
                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
