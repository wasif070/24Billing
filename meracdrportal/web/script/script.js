jQuery(document).ready(function(){
    clients.init();
    rates.init();
    $j(".jsel_all").change(function(){
        if($j(this).attr('checked')){
            $j('.select_id').attr('checked','checked');
        }else{
            $j('.select_id').attr('checked','');
        }
    });
});

var clients={
    init:function(){
        $j("#jgateway_type").change(function(){
            clients.load_client(this);
        });
        
        $j('.jmultipebtn').click(function(){
            var allVals = [];
            $j('.select_id:checked').each(function() {
                allVals.push($j(this).val());
            });
            
            if(allVals.length <=0){
                $j('.jerror_messge').html("<div class='error'>Please Select at least "+$j('.jsel_all').attr('title')+"!</div>")
                return false;
            }else{
                return true;
            }
        });
        $j('.jclient_id').keyup(function(){
            clients.get_client_list(this);
        });
        
        $j('.jtermClient_id').keyup(function(){
            clients.get_termClient_list(this);
        });
        
        $j('.joriginId').change(function(){
            var client_id=$j(this).val();
            clients.load_gatewayip(client_id);
            $j('.joriginIp').attr('disabled',false);
            $j('.jorigindest').attr('disabled',false);
        });
        
        $j('.jtermId').change(function(){
            var client_id=$j(this).val();
            clients.load_termgatewayip(client_id);
            $j('.jtermIp').attr('disabled',false);
            $j('.jtermdest').attr('disabled',false);
        });
        
    },
    load_client:function(obj){
        var gateway_type=$j(obj).val();
        $j.ajax({
            type:'post',
            url:'../gateway/load_client.jsp',
            data:{
                type:gateway_type
            },
            success:function(html){
                $j("#jclientId").html(html);
            }
        });
    },
    bind:function(){
        $j('.jclient_list').find('li').unbind('click');
        $j('.jclient_list').find('li').bind('click',function(){
            $j('.jclient_id').val($j(this).attr('title'));
            var client_id=$j(this).attr('rel');
            $j('.joriginId').val(client_id);
            clients.load_gatewayip(client_id);
            $j('.joriginIp').attr('disabled',false);
            $j('.jorigindest').attr('disabled',false);
            $j('.jclient_list').css('display','none');
        });
    }
    ,
    get_client_list:function(obj){
        $j('.jclient_list').css('display','block');
        var search=$j('.jclient_id').val();
        var type= $j(obj).attr('title');
        $j.ajax({
            type:'post',
            url:'../mvtscdr/load_client.jsp',
            data:{
                search:search,
                type: type
            },
            success:function(html){
                $j('.jclient_list').html(html);
                clients.bind();
            },
            error:function(a,b,c){
                //alert(a+'\n'+b+'\n'+c);
            }
        });
    },
    load_gatewayip:function(client_id){
        $j.ajax({
            type:'post',
            url:'../mvtscdr/load_gateway.jsp',
            data:{
                client_id:client_id,
                type: 0
            },
            success:function(html){
                $j('.joriginIp').html(html);
            },
            error:function(e,m,s){
                //alert(e+m+s);
            }
        });
    },
    bind_term:function(){
        $j('.jtermclient_list').find('li').unbind('click');
        $j('.jtermclient_list').find('li').bind('click',function(){
            $j('.jtermClient_id').val($j(this).attr('title'));
            var client_id=$j(this).attr('rel');
            $j('.jtermId').val(client_id);
            clients.load_termgatewayip(client_id);
            $j('.jtermIp').attr('disabled',false);
            $j('.jtermdest').attr('disabled',false);
            $j('.jtermclient_list').css('display','none');
        });
    }
    ,
    get_termClient_list:function(obj){
        $j('.jtermclient_list').css('display','block');
        var search=$j('.jtermclient_id').val();
        var type= $j(obj).attr('title');
        $j.ajax({
            type:'post',
            url:'../mvtscdr/load_client.jsp',
            data:{
                search:search,
                type: type
            },
            success:function(html){
                $j('.jtermclient_list').html(html);
                clients.bind_term();
            },
            error:function(a,b,c){
                //alert(a+'\n'+b+'\n'+c);
            }
        });
    },
    load_termgatewayip:function(client_id){
        $j.ajax({
            type:'post',
            url:'../mvtscdr/load_gateway.jsp',
            data:{
                client_id:client_id,
                type: 1
            },
            success:function(html){
                $j('.jtermIp').html(html);
            },
            error:function(e,m,s){
                //alert(e+m+s);
            }
        });
    }
}
var rates={
    init:function(){
        $j('.jadd_more_rate').live('click',function(){
            rates.load_rate_options();
        });
    },
    load_rate_options:function(){
        $j.ajax({
            type:'post',
            url:'../rates/load_rate_options.jsp',
            data:{
            },
            success:function(html){
                $j('.more_option').replaceWith(html);
            },
            error:function(e,m,s){
                //alert(e+m+s);
            }
        });
    }
}

var terminaton={    
    init:function(){     
        $j('#jgateway_type').live('click',function(){ 
            if(parseInt(this.value,10)==1){              
                $j("#term").html("<input type=\"text\" name=\"gateway_dst_port_sip\" id=\"gateway_dst_port_sip\"/>");
                }
            return false;
        });
    }
}

var gateway={
    init:function(){
        $j('#jgateway_type').live('click',function(){ 
            if(parseInt(this.value,10)==1){              
                $j("#term").html("<input type=\"text\" name=\"gateway_dst_port_sip\" id=\"gateway_dst_port_sip\"/>");
                }
            return false;
        });
    }
}


  /*  <tr>
    <th valign="top" >Destination Port SIP <span class="req_mark">*</span></th>
    <td valign="top" >
    <html:text property="gateway_dst_port_sip" /><br/>
    <html:messages id="gateway_dst_port_sip" property="gateway_dst_port_sip">
    <bean:write name="gateway_dst_port_sip"  filter="false"/>
    </html:messages>
    </td>
    </tr>*/
