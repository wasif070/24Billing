<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<html>
    <head>
        <title>24Billing :: Current Call List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body onload="JavaScript:timedRefresh(60000);">
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">    
                <div class="pad_10 border_left">
                    <%
                  java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                  navList.add("currentcall/listCurrentcalls.do?list_all=1;Current call");                   
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <c:set var="client_type" value="<%=String.valueOf(login_dto.getClientType())%>"></c:set>
                    <html:form action="/currentcall/listCurrentcalls" method="post" >
                        <div class="display_tag_content" align="center">
                            <bean:write name="CurrentcallForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <bean:write name="CurrentcallForm" property="message" filter="false"/>
                            <script type="text/javascript">count=0;</script>                                
                            <div class="over_flow_content_list">
                                <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.CurrentcallForm.currentcallList">
                                    <display:setProperty name="paging.banner.item_name" value="Gateway" /> 
                                    <display:setProperty name="paging.banner.items_name" value="Gateways" />
                                    <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                        <script type="text/javascript">
                                            document.write(++count+".");
                                        </script>
                                    </display:column>          
                                    <display:column  property="current_dial_no_in" class="left-align" title="Incoming Destination Number" sortable="true" style="width:18%" />                                     
                                    <c:choose>
                                        <c:when  test="${client_type==0 || client_type==2}">
                                            <display:column  property="current_org_gateway_ip" class="left-align" title="Origination Gateway Address" sortable="true" style="width:18%" />
                                        </c:when>
                                    </c:choose>                                    
                                    <display:column  property="current_dial_no_out" class="left-align" title="Outgoing Destination Number" sortable="true" style="width:18%" />                                      
                                    <c:choose>
                                        <c:when  test="${client_type==1 || client_type==2}">
                                            <display:column  property="current_term_gateway_ip" class="left-align" title="Termination Gateway Address" sortable="true" style="width:18%" /> 
                                        </c:when>
                                    </c:choose>
                                    <display:column  property="current_setup_time" class="center-align" title="Set Up Time" sortable="true" style="width:18%" />
                                    <display:column  property="current_connect_time" class="center-align" title="Connect Time" sortable="true" style="width:18%" /> 
                                    <display:column  property="active_duration" class="right-align" title="Duration (HH:SS)" sortable="true" style="width:18%" />   
                                    <display:column title="Status" sortable="true" headerClass="center-align" class="left-align">
                                        <c:choose>
                                            <c:when test="${data.status==0}"><span class="blue" >Calling</span></c:when>
                                            <c:when test="${data.status==1}"><span class="blue" >Connected</span></c:when>
                                        </c:choose>
                                    </display:column>
                                </display:table>
                            </div>    
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>                
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
    <script type="text/JavaScript">

        function timedRefresh(timeoutPeriod) {
            setTimeout("location.reload(true);",timeoutPeriod);
        }
    </script>
</html>





