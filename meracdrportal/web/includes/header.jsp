
<%
String baseURL = "";
if (request.getServerPort() > 0) {
baseURL = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + application.getContextPath() + "/";
} else {
baseURL = request.getScheme() + "://" + request.getServerName() + application.getContextPath() + "/";
}
request.getSession(true).setAttribute("BASE_URL",baseURL);

%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>

<link href="../stylesheets/custom-pagination.css" rel="stylesheet" type="text/css" />
<link href="../tools/treeview/jquery.treeview.css" rel="stylesheet" type="text/css" media="screen" />
<link href="../stylesheets/style.css" rel="stylesheet" type="text/css" />

<script src="../tools/jquery-1.4.2.min.js"></script>
<script src="../tools/treeview/jquery.treeview.js" type="text/javascript"></script>
<script src="../script/script.js"></script>
<script src="../script/utils.js"></script>

<script type="text/javascript">
    var $j=jQuery.noConflict();
    var base_url="<%=baseURL%>";
</script>