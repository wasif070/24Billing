<style type="text/css">
    #browser {
        font-family: Verdana, helvetica, arial, sans-serif;
        font-size: 100%;
    }
</style>
<script type="text/javascript" >
    $j(document).ready(function(){        
        $j(".treeview").treeview({
            persist: "location",
            collapsed: true,
            unique: false,
            control: "#container"
        });
    });    	
</script>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants" %>
<%  LoginDTO login_dto1 = null;
    if (request.getSession(true).getAttribute(Constants.LOGIN_DTO) != null) {
        login_dto1 = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
    }
%>
<div class="left-menu pad_10" id="browser">
    <ul class="treeview">
        <%
            if (login_dto1.isUser() || login_dto1.getSuperUser()) {
        %>
        <li class="expandable"><div class="hitarea collapsable-hitarea"></div><span ><a href="../home/home.jsp">Home</a></span></li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../clients/listClient.do?list_all=1&s=0">Client</a></span>
            <ul>
                <li><span ><a href="../clients/listClient.do?list_all=1" >View Client</a></span></li>
                <li><span ><a href="../clients/add-client.jsp" >Add Client</a></span></li>
                <li class="last"><span ><a href="../clients/listRechargeClient.do?list_all=1" >Recharge Client</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../gateway/listGateway.do?list_all=1&s=0">Gateway</a></span>
            <ul>
                <li><span ><a href="../gateway/listGateway.do?list_all=1" >View Gateway</a></span></li>
                <li class="last"><span ><a href="../gateway/add-gateway.jsp" >Add Gateway</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../dialplan/listDialplan.do?list_all=1&s=0">Dial Plan</a></span>
            <ul>
                <li><span ><a href="../dialplan/listDialplan.do?list_all=1" >View Dial Plan</a></span></li>
                <li class="last"><span ><a href="../dialplan/add-dialplan.jsp" >Add Dial Plan</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../rateplan/listRateplan.do?list_all=1&s=0">Rate Plan</a></span>
            <ul>
                <li><span ><a href="../rateplan/listRateplan.do?list_all=1" >View Rate Plan</a></span></li>
                <li class="last"><span ><a href="../rateplan/new_rateplan.jsp" >Add Rate Plan</a></span></li>
            </ul>
        </li>
        <%if (login_dto1.getSuperUser() && !login_dto1.isUser()) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../user/getUser.do?id=<%=login_dto1.getId()%>" >Edit Profile</a></span></li>         
        <%}%>
        <%if (login_dto1.isUser()) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../user/listUser.do?list_all=1&s=0">User</a></span>         
            <ul>
                <li><span ><a href="../user/listUser.do?list_all=1" >View User</a></span></li>
                <li class="last"><span ><a href="../user/add-user.jsp" >Add User</a></span></li>
            </ul>
        </li>
        <%}%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../invoice/listinvoice.do?list_all=1&s=0">Invoice</a></span>
            <ul>
                <li><span ><a href="../invoice/listinvoice.do?list_all=1" >View Invoice</a></span></li>
                <li class="last"><span ><a href="../invoice/addinvoice.jsp">Add Invoice</a></span></li>
            </ul>
        </li>

        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../reports/listCdr.do?list_all=1&s=0">Report</a></span></a>
            <ul>
                <li><span ><a href="../reports/listCdr.do?list_all=1">Monitoring Report</a></span></li>
                <li><span ><a href="../reports/qualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li><span ><a href="../transactions/listTransaction.do?list_all=1">Payment History</a></span></li>
                <li class="last"><span ><a href="../transactions/transSummery.do?list_all=1">Payment Summery</a></span></li>
            </ul>
        </li>   
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../settings/listSettings.do?list_all=1">Settings</a></span></li>
        <% } else {%>
        <li><span ><a href="../transactions/clientSummery.do">Home</a></span></li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../reports/listClientCdr.do?list_all=1&s=0">Report</a></span>
            <ul>                
                <li><span ><a href="../reports/listClientCdr.do?list_all=1&s=0">Monitoring Report</a></span></li>
                <li><span ><a href="../reports/cQualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li class="last"><span ><a href="../transactions/clientTrans.do?list_all=1">Payment History</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../clients/getClientInfo.do">Client</a></span>
            <ul>
                <li class="last"><span ><a href="../clients/getClientInfo.do">Edit Profile</a></span></li>
            </ul>
        </li>
        <li><span ><a href="../rates/clientRate.do">Rate Tariff</a></span></li>
        <% }%>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../currentcall/listCurrentcalls.do?list_all=1">Current call</a></span></li> 
        <li class="last"><span ><a href="../login/logout.do">Logout</a></span></li>
    </ul>
</div>