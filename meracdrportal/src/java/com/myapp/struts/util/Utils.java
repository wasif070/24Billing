package com.myapp.struts.util;

import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Utils {

    public void Utils() {
    }

    public static boolean checkEmailId(String address) {

        if (address == null) {
            return false;
        }
        if (address.length() == 0) {
            return false;
        }
        Pattern p = Pattern.compile("^[\\w\\-\\_\\.]+\\@[[\\w\\-\\_]+\\.]+[a-zA-Z]{2,}$");
        Matcher m = p.matcher(address);
        boolean b = m.matches();
        if (b == false) {
            return false;
        }

        return true;
    }

    public static boolean IntegerValidation(String string) {
        boolean status = false;
        try {
            Integer.parseInt(string);
            status = true;
        } catch (Exception e) {
            status = false;
        }
        return status;
    }

    public static long DateToLong(String date) {
        String[] values = date.split("/");
        int day = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int year = Integer.parseInt(values[2]);
        long dat = new GregorianCalendar(year, month - 1, day, 0, 0, 0).getTimeInMillis();
        return dat;
    }

    public static String LongToDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss a");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String ToDate(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String ToDateDDMMYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String ToDateDDMMYYhhmm(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yy HH:mm");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static long ToLong(String date) {
        String[] values = date.split("/");
        int day = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int year = Integer.parseInt(values[2]);
        long dat = new GregorianCalendar(year, month - 1, day, 0, 0, 0).getTimeInMillis();
        return dat;
    }

    public static long ToLong1(String date) {
        String[] values = date.split("-");
        int day = Integer.parseInt(values[0]);
        int month = Integer.parseInt(values[1]);
        int year = Integer.parseInt(values[2]);
        long dat = new GregorianCalendar(year, month - 1, day, 0, 0, 0).getTimeInMillis();
        return dat;
    }

    public static String[] getDateParts(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        String curDate = formatter.format(new java.util.Date(date));
        String date_parts[] = curDate.split("/");
        return date_parts;
    }

    public static int getCurrentMonth(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("MM");
        String curDate = formatter.format(new java.util.Date(date));
        return Integer.parseInt(curDate);
    }

    public static int getCurrentYear(long date) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        String curDate = formatter.format(new java.util.Date(date));
        return Integer.parseInt(curDate);
    }

    public static ArrayList<Integer> getDay() {
        ArrayList<Integer> options = new ArrayList<Integer>();
        for (int i = 1; i <= 31; i++) {
            options.add(i);
        }
        return options;
    }

    public static long getDateLong(String str_date) {
        long longDate = 0L;
        Date date = new Date();
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            date = (Date) formatter.parse(str_date);
            longDate = date.getTime();

        } catch (Exception e) {
            System.out.println("Exception :" + e);
        }
        return longDate;
    }

    public static ArrayList<String> getMonth() {
        ArrayList<String> options = new ArrayList<String>();
        options.add("January");
        options.add("February");
        options.add("March");
        options.add("April");
        options.add("May");
        options.add("June");
        options.add("July");
        options.add("August");
        options.add("September");
        options.add("October");
        options.add("November");
        options.add("December");
        return options;
    }

    public static ArrayList<Integer> getYear() {
        ArrayList<Integer> options = new ArrayList<Integer>();
        int currentYear = 2010;
        int i = currentYear;
        while (i - currentYear < 20) {
            options.add(i);
            i++;
        }
        return options;
    }

    public static ArrayList<Integer> getTimeValue(int length) {
        ArrayList<Integer> options = new ArrayList<Integer>();
        for (int i = 1; i <= length; i++) {
            options.add(i);
        }
        return options;
    }

    public static boolean checkValidUserId(String userId) {

        if (userId == null) {
            return false;
        }
        if (userId.length() == 0) {
            return false;
        }

        Pattern p = Pattern.compile("^[a-zA-Z0-9_.]+$");
        Matcher m = p.matcher(userId);
        boolean b = m.matches();
        if (b == false) {
            return false;
        }
        return true;
    }

    public static boolean isInteger(String number) {
        try {
            Integer.parseInt(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isLong(String number) {
        try {
            Long.parseLong(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isFloat(String number) {
        try {
            Float.parseFloat(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isDouble(String number) {
        try {
            Double.parseDouble(number);
        } catch (NumberFormatException ex) {
            return false;
        }
        return true;
    }

    public static boolean isFloatNumber(String number) {
        boolean isValid = false;

        /*Number: A numeric value will have following format:
        ^[-+]?: Starts with an optional "+" or "-" sign.
        [0-9]*: May have one or more digits.
        \\.? : May contain an optional "." (decimal point) character.
        [0-9]+$ : ends with numeric digit.
         */

        //Initialize reg ex for numeric data.
        String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
        CharSequence inputStr = number;
        Pattern pattern = Pattern.compile(expression);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public static boolean isValidIP(String ip) {
        if (ip == null) {
            return false;
        }
        if (ip.length() == 0) {
            return false;
        }

        String ip_parts[] = ip.split("\\.");
        if (ip_parts.length < 4 || ip_parts.length > 4) {
            return false;
        }

        for (String s : ip_parts) {
            int i = Integer.parseInt(s);
            if (i < 0 || i > 255) {
                return false;
            }
        }
        return true;
    }

    public static String getOnlyIP(String ip) {
        if (ip == null || ip.length() < 1) {
            return "";
        }
        String getIP = "";
        if (ip.indexOf(':') > 0) {
            getIP = ip.substring(0, ip.indexOf(':'));
        }
        return getIP;
    }

    public static String getTimeHHMMSS(int elapsed_time) {
        String time = "";
        float seconds = elapsed_time / 1000;
        int min = (int) seconds / 60;
        int hour = (int) min / 60;
        int sec = (int) (seconds - min * 60);
        min = (int) (min - hour * 60);

        if (hour < 10) {
            time += "0" + hour;
        } else {
            time += hour;
        }

        if (min < 10) {
            time += ":0" + min;
        } else {
            time += ":" + min;
        }

        if (sec < 10) {
            time += ":0" + sec;
        } else {
            time += ":" + sec;
        }

        return time;
    }

    public static String getTimeMMSS(long elapsed_time) {
        DecimalFormat df = new DecimalFormat("##00");
        long seconds = (long) (elapsed_time + 999) / 1000;
        String min = df.format((long) seconds / 60);
        String sec = df.format((long) (seconds % 60));
        return min + ":" + sec;
    }

    public static String removePrefix(String dialNo, String prefix) {
        String str = "";
        str = dialNo;
        if (prefix != null && dialNo.startsWith(prefix)) {
            str = str.substring(prefix.length());
        }
        return str;
    }

    /**
     * Method to join array elements of type string
     * @author Hendrik Will, imwill.com
     * @param inputArray Array which contains strings
     * @param glueString String between each array element
     * @return String containing all array elements seperated by glue string
     */
    public static String implodeArray(long[] inputArray, String glueString) {

        /** Output variable */
        String output = "";

        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);

            for (int i = 1; i < inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }

            output = sb.toString();
        }

        return output;
    }

    public static int[] explodeArray(String glueString) {

        String[] temp = glueString.split(";");
        int[] output = new int[temp.length];
        for (int i = 0; i < temp.length; i++) {
            output[i] = Integer.parseInt(temp[i]);
        }
        return output;
    }

    public static String implodeArrayName(String[] inputArray, String glueString) {

        /** Output variable */
        String output = "";

        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);

            for (int i = 1; i < inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }

            output = sb.toString();
        }

        return output;
    }

    public static String ReadTime() {
        String time = "";
        String configFile = "TimeConfig.txt";
        try {
            InputStream input = null;
            File file = new File(configFile);
            if (file.exists()) {
                input = new FileInputStream(file);
            } else {
                input = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFile);
            }
            Properties dbProp = new Properties();
            dbProp.load(input);
            input.close();

            if (dbProp.containsKey("timezone")) {
                time = dbProp.getProperty("timezone");
            }
        } catch (IOException e) {
            System.out.println("Exception-->" + e);
        }


        return time;

    }

    public static String ToDateDDMMYYYYhhmmss(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static String ToDateDDMMYYYY0h0m0s(long date) {
        if (date == 0) {
            return "";
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        String curDate = formatter.format(new java.util.Date(date));
        return curDate;
    }

    public static long getTimeLong(String str_time) {
        String[] temp;
        char sign_bit = str_time.charAt(0);
        if (sign_bit == '+' || sign_bit == '-') {
            str_time = str_time.substring(1);
        }
        temp = str_time.split(":");

        if (sign_bit == '-') {
            return -1 * (((Long.parseLong(temp[0]) * 60 * 60) + (Long.parseLong(temp[1]) * 60) + Long.parseLong(temp[2])) * 1000);
        }
        return (((Long.parseLong(temp[0]) * 60 * 60) + (Long.parseLong(temp[1]) * 60) + Long.parseLong(temp[2])) * 1000);

    }

    public static String MillisToTime(long timeMillis) {

        String s;
        long time = timeMillis / 1000;
        String seconds = Long.toString((long) (time % 60));
        String minutes = Long.toString((long) ((time % 3600) / 60));
        String hours = Long.toString((long) (time / 3600));
        for (int i = 0; i < 2; i++) {
            if (seconds.length() < 2) {
                seconds = "0" + seconds;
            }
            if (minutes.length() < 2) {
                minutes = "0" + minutes;
            }
            if (hours.length() < 2) {
                hours = "0" + hours;
            }

        }
        return s = hours + ":" + minutes + ":" + seconds;
    }

    public static long getUTCtimeLong(String str_date) {
        long longDate = 0L;
        Date date = new Date();
        try {
            DateFormat formatter;
            formatter = new SimpleDateFormat("HH:mm:ss.SSS z EEE MMM dd yyyy");
            date = (Date) formatter.parse(str_date);
            longDate = date.getTime();

        } catch (Exception e) {
            System.out.println("Exception :" + e);
        }
        return longDate;
    }

    public static String getFormatedDuration(long duration) {
        DecimalFormat df = new DecimalFormat("##00");
        int minutes = (int) (duration / 60);
        int secs = (int) duration % 60;
        return df.format(minutes) + ":" + df.format(secs);
    }
}
