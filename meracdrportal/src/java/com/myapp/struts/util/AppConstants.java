/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.util;

import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class AppConstants {

    static Logger logger = Logger.getLogger(AppConstants.class.getName());

    /*-----------Action Target Messages--------------------*/
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    //USER INFORMATION
    public static final String USER_LIST = "USER_LIST";
    public static final String USER_SUCCESS_MESSAGE = "USER_SUCCESS_MESSAGE";
    //ROLE INFORMATION
    public static final String ROLE_LIST = "ROLE_LIST";
    public static final String ROLE_SUCCESS_MESSAGE = "ROLE_SUCCESS_MESSAGE";
    //SETTINGS INFORMATION
    public static final String SETTINGS_LIST = "SETTINGS_LIST";
    public static final String SETTINGS_SUCCESS_MESSAGE = "SETTINGS_SUCCESS_MESSAGE";
    //MOBILE AREA INFORMATION
    public static final String MOBILE_AREA_LIST = "MOBILE_AREA_LIST";
    public static final String MOBILE_AREA_SUCCESS_MESSAGE = "MOBILE_AREA_SUCCESS_MESSAGE";
    //MAIL SERVER
    public static String MAIL_SERVER = "MAIL_SERVER";
    public static String AUTHENTICATION_EMAIL = "AUTHENTICATION_EMAIL";
    public static String AUTHENTICATION_EMAIL_PASSWORD = "AUTHENTICATION_EMAIL_PASSWORD";
    public static String NOTIFICATION_FROM = "NOTIFICATION_FROM";
    public static String NOTIFICATION_TO = "NOTIFICATION_TO";
    public static String NOTIFICATION_CC = "NOTIFICATION_CC";
    public static String NOTIFICATION_BCC = "NOTIFICATION_BCC";
    public static String RECORD_PER_PAGE = "20";
    //NUMBER MANAGEMENT
    public static String CITYCODE_SUCCESS_MESSAGE = "CITYCODE_SUCCESS_MESSAGE";
    public static String CITYCODE_LIST = "CITYCODE_LIST";
    public static String CALLER_SUCCESS_MESSAGE = "CALLER_SUCCESS_MESSAGE";
    public static String CALLER_LIST = "CALLER_LIST";
    public static String CALLEE_SUCCESS_MESSAGE = "CALLEE_SUCCESS_MESSAGE";
    public static String CALLEE_LIST = "CALLEE_LIST";

    public AppConstants() {
    }
}
