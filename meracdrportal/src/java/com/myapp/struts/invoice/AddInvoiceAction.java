package com.myapp.struts.invoice;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import java.io.*;

public class AddInvoiceAction extends Action {

    static Logger logger = Logger.getLogger(AddInvoiceAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        InvoiceForm formBean = (InvoiceForm) form;
        InvoiceDto dto = new InvoiceDto();
        String logoname = null;
        try {
            FormFile logo = formBean.getLogo();
            logoname = logo.getFileName();
            String logocontenttype = logo.getContentType();
            byte[] fileData = logo.getFileData();
            String realContextPath = request.getSession().getAttribute("realpath").toString();
            logger.debug("realpath--->" + realContextPath);
            String filePath = "C:/Users/Rajib/Documents/NetBeansProjects/meracdrportal Final 22.09.11/meracdrportal Final 22.09.11/meracdrportal/web/images";
            if (!logoname.equals("")) {
                File fileToCreate = new File(realContextPath, logoname);
                if (!fileToCreate.exists()) {
                    FileOutputStream fileOutStream = new FileOutputStream(fileToCreate);
                    fileOutStream.write(logo.getFileData());
                    fileOutStream.flush();
                    fileOutStream.close();
                }
            }
        } catch (FileNotFoundException fnfe) {
            System.out.println(fnfe.getMessage());
            logger.debug("fnfe.getMessage()" + fnfe.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
            logger.debug("ioe.getMessage()" + ioe.getMessage());
        }

        if (login_dto != null && login_dto.getSuperUser()) {


            InvoiceTaskSchedular scheduler = new InvoiceTaskSchedular();
            logger.debug("starting date" + formBean.getBday());
            String starting_date = formBean.getBday() + "/" + formBean.getBmonth() + "/" + formBean.getByear();
            String Ending_date = formBean.getEday() + "/" + formBean.getEmonth() + "/" + formBean.getEyear();
            String Invoice_Date = formBean.getIday() + "/" + formBean.getImonth() + "/" + formBean.getIyear();
            long id = formBean.getClientId();
            dto.setClientId(id);
            dto.setCompanyName(formBean.getCompanyName());
            dto.setCompanyAddress(formBean.getCompanyAddress());
            dto.setCompanyLocation(formBean.getCompanyLocation());
            dto.setLogoName(logoname);
            dto.setFooter(formBean.getFooter());
            dto.setStartDate(starting_date);
            dto.setEndDate(Ending_date);
            dto.setInvoiceDate(Invoice_Date);
            MyAppError error = scheduler.addInvoice(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "Invoice is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
