/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.invoice;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.AppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.clients.ClientDTO;

/**
 *
 * @author Rajib
 */
public class InvoiceDAO {

    static Logger logger = Logger.getLogger(InvoiceDAO.class.getName());

    public InvoiceDAO() {
    }

    public MyAppError addInvoice(InvoiceDto p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "insert into invoice(cid,client_id,client_name,client_email,company_name,company_address,company_location,logo,footer,start_date,end_date,invoice_date) values(?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);
            ArrayList<ClientDTO> data = ClientLoader.getInstance().getClientDTOById((int) p_dto.getClientId());
            ps.setLong(1, p_dto.getClientId());
            if (data != null && data.size() > 0) {

                for (int i = 0; i < data.size(); i++) {
                    ClientDTO dto = data.get(i);
                    ps.setString(2, dto.getClient_id());
                    ps.setString(3, dto.getClient_name());
                    ps.setString(4, dto.getClient_email());
                }

            }
            ps.setString(5, p_dto.getCompanyName());
            ps.setString(6, p_dto.getCompanyAddress());
            ps.setString(7, p_dto.getCompanyLocation());
            ps.setString(8, p_dto.getLogoName());
            ps.setString(9, p_dto.getFooter());
            ps.setString(10, p_dto.getStartDate());
            ps.setString(11, p_dto.getEndDate());
            ps.setString(12, p_dto.getInvoiceDate());
            ps.executeUpdate();
            InvoiceLoader.getInstance().forceReload();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while loading invoice: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public AppError deleteInvoiceInfo(String ids) {
        AppError error = new AppError();
        DBConnection dbConnection = null;
        Statement stmt = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            stmt.executeUpdate("delete from invoice where id in (" + ids + ")");
        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during delete invoice info-->" + e);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return error;
    }
}
