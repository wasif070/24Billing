/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.invoice;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.AppError;
import java.io.File;
import java.util.ArrayList;

/**
 *
 * @author Rajib
 */
public class InvoiceTaskSchedular {

    public InvoiceTaskSchedular() {
    }

    public MyAppError addInvoice(InvoiceDto p_dto) {
        InvoiceDAO invoiceDAO = new InvoiceDAO();
        return invoiceDAO.addInvoice(p_dto);
    }

    public ArrayList<InvoiceDto> getGatewayDTOs(LoginDTO l_dto) {
        return InvoiceLoader.getInstance().getinvoiceDTOList();
    }

    public InvoiceDto getpersonalinfo(long id) {
        return InvoiceLoader.getInstance().getpersonalInfo(id);
    }

    public ArrayList<InvoiceDto> getInvoiceDetail(long id) {
        return InvoiceLoader.getInstance().getinvoiceDetail(id);
    }

    public String GenerateInvoiceXLS(long id, File downloadPlace) {
        return InvoiceLoader.getInstance().GenerateInvoiceXLS(id, downloadPlace);
    }

    public AppError deleteInvoiveInfo(String ids) {
        InvoiceDAO dbFunction = new InvoiceDAO();
        AppError error = dbFunction.deleteInvoiceInfo(ids);
        if (error.getErrorType() == error.NO_ERROR) {
            InvoiceLoader.getInstance().forceReload();
        }
        return error;
    }

    public InvoiceDto gettotal() {
        return InvoiceLoader.getInstance().gettotal();
    }

    public ArrayList<InvoiceDto> getInvoiceDTOsWithSearchParam(InvoiceDto idto, LoginDTO l_dto) {
        return InvoiceLoader.getInstance().getInvoiceDTOsWithSearchParam(idto);
    }
}
