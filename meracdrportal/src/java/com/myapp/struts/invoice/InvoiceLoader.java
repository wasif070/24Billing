package com.myapp.struts.invoice;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import java.util.Iterator;
import com.myapp.struts.util.Utils;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.settings.SettingsDTO;

import java.io.File;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.Label;
import jxl.WorkbookSettings;
import jxl.write.WritableWorkbook;
import jxl.write.WritableSheet;
import java.util.Locale;
import jxl.Workbook;
import jxl.format.Border;
import jxl.format.BorderLineStyle;
import jxl.Cell;
import jxl.format.Alignment;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import jxl.write.WritableImage;

public class InvoiceLoader {

    static Logger logger = Logger.getLogger(InvoiceLoader.class.getName());
    private static long LOADING_INTERVAL = 0 * 60 * 1000;
    private long loadingTime = 0;
    private long totalMin = 0;
    private double totalRevenue = 0;
    private ArrayList<InvoiceDto> invoiceList = null;
    static InvoiceLoader invoiceLoader = null;
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();
    char sign_bit = timeZone.charAt(0);
    String timeZone1 = timeZone.substring(1);
    String curdate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));
    String now = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));
    NumberFormat amount_formatter = new DecimalFormat("#00.000000");

    public InvoiceLoader() {
        forceReload();
    }

    public static InvoiceLoader getInstance() {
        if (invoiceLoader == null) {
            createGatewayLoader();
        }
        return invoiceLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (invoiceLoader == null) {
            invoiceLoader = new InvoiceLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            invoiceList = new ArrayList<InvoiceDto>();

            String sql = "select * from invoice order by id DESC";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                InvoiceDto dto = new InvoiceDto();
                dto.setId(resultSet.getInt("id"));
                dto.setClientId(resultSet.getLong("cid"));
                dto.setClient(resultSet.getString("client_id"));
                dto.setClientName(resultSet.getString("client_name"));
                dto.setCompanyName(resultSet.getString("company_name"));
                dto.setCompanyAddress(resultSet.getString("company_address"));
                dto.setCompanyLocation(resultSet.getString("company_location"));
                dto.setLogoName(resultSet.getString("logo"));
                dto.setFooter(resultSet.getString("footer"));
                dto.setClientEmail(resultSet.getString("client_email"));
                dto.setStartDate(resultSet.getString("start_date"));
                dto.setEndDate(resultSet.getString("end_date"));
                dto.setInvoiceDate(resultSet.getString("invoice_date"));
                invoiceList.add(dto);

            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in Invoice loader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<InvoiceDto> getinvoiceDTOList() {
        checkForReload();
        return invoiceList;
    }

    public synchronized ArrayList<InvoiceDto> getinvoiceDetail(long id) {
        checkForReload();
        ArrayList<InvoiceDto> list = invoiceList;
        ArrayList<InvoiceDto> detailsInfo = new ArrayList<InvoiceDto>();
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                InvoiceDto dto = (InvoiceDto) i.next();
                if (dto.getId() == id) {
                    long clientId = dto.getClientId();
                    String condition = "";
                    condition += "(origin_client_id=" + clientId + " or term_client_id=" + clientId + ")";
                    if (dto.getStartDate() != null) {
                        condition += " and  connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.ToLong(dto.getStartDate()) - Utils.getTimeLong(timeZone)) + "'";
                    } else {
                        condition += " and connection_time >= '" + curdate + "' ";
                    }

                    if (dto.getEndDate() != null) {
                        condition += " and  connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.ToLong(dto.getEndDate()) - Utils.getTimeLong(timeZone) + 86400000) + "'";
                    } else {
                        condition += " and connection_time >= '" + now + "' ";
                    }
                    condition += " and duration>0 ";
                    
                    DBConnection dbConnection = null;
                    Statement statement = null;
                    try {
                        dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                        statement = dbConnection.connection.createStatement();
                        String sql = null;
                        if (sign_bit == '-') {
                            sql = "select DATE_FORMAT(DATE_SUB(connection_time, INTERVAL '0 " + timeZone1 + "' DAY_SECOND),'%Y-%m-%d') as Date,origin_destination,origin_rate,sum(origin_bill_amount) as TotalRevenue,sum(duration) as TotalMiniute from flamma_cdr where " + condition + " group by Date,origin_destination ";
                        } else {
                            sql = "select DATE_FORMAT(DATE_ADD(connection_time, INTERVAL '0 " + timeZone1 + "' DAY_SECOND),'%Y-%m-%d') as Date,origin_destination,origin_rate,sum(origin_bill_amount) as TotalRevenue,sum(duration) as TotalMiniute from flamma_cdr where " + condition + " group by Date,origin_destination ";
                        }
                        ResultSet resultSet = statement.executeQuery(sql);
                        while (resultSet.next()) {
                            InvoiceDto p_dto = new InvoiceDto();
                            p_dto.setDate(resultSet.getString("Date"));
                            p_dto.setPrefix(resultSet.getString("origin_destination"));
                            p_dto.setRatePerMin(resultSet.getFloat("origin_rate"));
                            p_dto.setTotalMin(Utils.getTimeMMSS((long) resultSet.getDouble("TotalMiniute")));
                            p_dto.setTotalTaka(amount_formatter.format(resultSet.getDouble("TotalRevenue")));
                            totalMin = totalMin + (long) resultSet.getDouble("TotalMiniute");
                            totalRevenue = totalRevenue + resultSet.getDouble("TotalRevenue");
                            detailsInfo.add(p_dto);
                        }
                        resultSet.close();
                    } catch (Exception e) {
                        logger.fatal("Exception in getinvoiceDetail-->", e);
                    } finally {
                        try {
                            if (statement != null) {
                                statement.close();
                            }
                        } catch (Exception e) {
                        }
                        try {
                            if (dbConnection.connection != null) {
                                databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                            }
                        } catch (Exception e) {
                        }
                    }

                }
            }
        }
        return detailsInfo;
    }

    public InvoiceDto gettotal() {
        InvoiceDto p_dto = new InvoiceDto();
        p_dto.setTotalMinute(Utils.getTimeMMSS(totalMin));
        p_dto.setTotalRevenue(totalRevenue);
        totalMin = 0;
        totalRevenue = 0;
        return p_dto;
    }

    public InvoiceDto getpersonalInfo(long id) {
        checkForReload();
        InvoiceDto dto = null;
        ArrayList<InvoiceDto> list = invoiceList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                InvoiceDto p_dto = (InvoiceDto) i.next();
                if (p_dto.getId() == id) {
                    return p_dto;
                }
            }
        }
        return dto;
    }

    public ArrayList<InvoiceDto> getInvoiceDTOsWithSearchParam(InvoiceDto idto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<InvoiceDto> list = invoiceList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();

            while (i.hasNext()) {
                InvoiceDto dto = (InvoiceDto) i.next();
                if ((idto.searchWithClientID && !dto.getClient().toLowerCase().startsWith(idto.getClient())) || (idto.searchWithInvoiceNumber && dto.getId() != idto.getId())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public String GenerateInvoiceXLS(long id, File realContextPathFile) {
        String separator = System.getProperty("file.separator");
        File fileDownload = null;
        try {

            InvoiceDto dto = getpersonalInfo(id);
            String filename = separator + "Invoice_Numbe_" + dto.getId() + System.currentTimeMillis() + ".xls";
            File downloadPlace = new File(realContextPathFile, "downloadedinvoice");
            fileDownload = new File(downloadPlace, filename);

            ArrayList<InvoiceDto> newlist = getinvoiceDetail(id);
            WorkbookSettings ws = new WorkbookSettings();
            ws.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook = Workbook.createWorkbook(fileDownload, ws);
            WritableSheet s = workbook.createSheet("Sheet1", 0);
            s.mergeCells(10, 0, 14, 0);
            s.mergeCells(0, 10, 4, 10);
            WritableFont wf = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
            WritableCellFormat cf = new WritableCellFormat(wf);
            WritableCellFormat cf5 = new WritableCellFormat(wf);
            cf5.setAlignment(Alignment.CENTRE);
            WritableFont wf1 = new WritableFont(WritableFont.ARIAL, 9, WritableFont.NO_BOLD);
            WritableCellFormat cf1 = new WritableCellFormat(wf1);
            cf.setBorder(Border.ALL, BorderLineStyle.MEDIUM);
            cf.setAlignment(Alignment.GENERAL);
            cf1.setBorder(Border.NONE, BorderLineStyle.NONE);
            WritableCellFormat cf2 = new WritableCellFormat(wf1);
            cf2.setBorder(Border.LEFT, BorderLineStyle.MEDIUM);
            WritableCellFormat cf3 = new WritableCellFormat(wf1);
            cf3.setBorder(Border.RIGHT, BorderLineStyle.MEDIUM);
            WritableCellFormat cf4 = new WritableCellFormat(wf1);
            cf4.setBorder(Border.TOP, BorderLineStyle.MEDIUM);
            WritableCellFormat cf6 = new WritableCellFormat(wf1);
            cf6.setBorder(Border.BOTTOM, BorderLineStyle.MEDIUM);
            cf.setWrap(true);
            cf1.setWrap(true);
            File imagePlace = new File(realContextPathFile, "images");
            File logoFile = new File(imagePlace, dto.getLogoName());
            WritableImage imgobj = new WritableImage(0, 0, 2, 3, logoFile);
            s.addImage(imgobj);
            Label l = new Label(0, 5, "Client", cf4);
            s.addCell(l);
            l = new Label(1, 5, dto.getClient(), cf4);
            s.addCell(l);
            l = new Label(2, 5, "", cf4);
            s.addCell(l);
            l = new Label(0, 6, "Client Name", cf1);
            s.addCell(l);
            l = new Label(1, 6, dto.getClientName(), cf1);
            s.addCell(l);
            l = new Label(0, 7, "Authorized Email", cf6);
            s.addCell(l);
            l = new Label(1, 7, dto.getClientEmail(), cf6);
            s.addCell(l);
            l = new Label(2, 7, "", cf6);
            s.addCell(l);
            l = new Label(4, 0, dto.getCompanyName(), cf1);
            s.addCell(l);
            l = new Label(4, 1, dto.getCompanyAddress(), cf1);
            s.addCell(l);
            l = new Label(4, 2, dto.getCompanyLocation(), cf1);
            s.addCell(l);
            l = new Label(3, 5, "Invoice Date", cf4);
            s.addCell(l);
            l = new Label(4, 5, dto.getInvoiceDate(), cf4);
            s.addCell(l);
            l = new Label(5, 5, "", cf2);
            s.addCell(l);
            l = new Label(3, 6, "Billing From", cf1);
            s.addCell(l);
            l = new Label(4, 6, dto.getStartDate(), cf3);
            s.addCell(l);
            l = new Label(3, 7, "Billing To", cf6);
            s.addCell(l);
            l = new Label(4, 7, dto.getEndDate(), cf6);
            s.addCell(l);
            l = new Label(5, 7, "", cf2);
            s.addCell(l);
            l = new Label(0, 10, "Invoice Detail", cf5);
            s.addCell(l);
            l = new Label(0, 11, "Date", cf);
            s.addCell(l);
            l = new Label(1, 11, "Prefix", cf);
            s.addCell(l);
            l = new Label(2, 11, "Rate(Per Min)", cf);
            s.addCell(l);
            l = new Label(3, 11, "Total Min", cf);
            s.addCell(l);
            l = new Label(4, 11, "Revenue", cf);
            s.addCell(l);
            if (newlist != null && newlist.size() > 0) {
                Iterator i = newlist.iterator();
                int k = 12;
                while (i.hasNext()) {
                    InvoiceDto p_dto = (InvoiceDto) i.next();
                    l = new Label(0, k, p_dto.getDate(), cf2);
                    s.addCell(l);
                    l = new Label(1, k, p_dto.getPrefix(), cf1);
                    s.addCell(l);
                    l = new Label(2, k, Float.toString(p_dto.getRatePerMin()), cf1);
                    s.addCell(l);
                    l = new Label(3, k, p_dto.getTotalMin(), cf1);
                    s.addCell(l);
                    l = new Label(4, k, p_dto.getTotalTaka(), cf3);
                    s.addCell(l);
                    k++;
                }
                l = new Label(0, k, "", cf4);
                s.addCell(l);
                l = new Label(1, k, "", cf4);
                s.addCell(l);
                l = new Label(2, k, "", cf4);
                s.addCell(l);
                l = new Label(3, k, Utils.getTimeMMSS(totalMin), cf4);
                s.addCell(l);
                l = new Label(4, k, amount_formatter.format(totalRevenue), cf4);
                s.addCell(l);
            }

            int i = 0, j = 0;
            int columns = s.getColumns();
            for (i = 0; i < columns; i++) {
                Cell[] cell = s.getColumn(i);
                int celLenght = cell.length;
                String content = "";
                int contentLength = 0;
                for (j = 0; j < celLenght; j++) {
                    content = cell[j].getContents();
                    if (contentLength < content.length()) {
                        contentLength = content.length();
                    }
                }
                int EXTRA_SPACE = 2;
                s.setColumnView(i, contentLength + EXTRA_SPACE);
            }
            workbook.write();
            workbook.close();
            totalMin = 0;
            totalRevenue = 0;
        } catch (Exception ex) {
            logger.debug("Exception in main part : " + ex);
        } finally {
            try {
            } catch (Exception ex) {
                logger.debug("Ex : " + ex);
            }
        }
        return fileDownload.getPath();
    }
}
