/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.invoice;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import java.util.ArrayList;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Rajib
 */
public class InvoiceForm extends org.apache.struts.action.ActionForm {

    private String name;
    private int number;
    public String message;
    public ArrayList invoicelist;
    private int id;
    private int recordPerPage;
    private String client_id;
    private long invoice_number;

    public String getTotalMinute() {
        return totalMinute;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public long getInvoice_number() {
        return invoice_number;
    }

    public void setInvoice_number(long invoice_number) {
        this.invoice_number = invoice_number;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public void setTotalMinute(String totalMinute) {
        this.totalMinute = totalMinute;
    }
    private String startDate;
    private long[] ids;
    private String totalMinute;
    private double totalRevenue;

    public double getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(double totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public long[] getIds() {
        return ids;
    }

    public void setIds(long[] ids) {
        this.ids = ids;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyLocation() {
        return companyLocation;
    }

    public void setCompanyLocation(String companyLocation) {
        this.companyLocation = companyLocation;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getFooter() {
        return footer;
    }

    public void setFooter(String footer) {
        this.footer = footer;
    }
    private String endDate;
    private String companyName;
    private String companyAddress;
    private String companyLocation;
    private FormFile logo;
    private String logoName;
    private String footer;

    public FormFile getLogo() {
        return logo;
    }

    public void setLogo(FormFile logo) {
        this.logo = logo;
    }

    public String getLogoName() {
        return logoName;
    }

    public void setLogoName(String logoName) {
        this.logoName = logoName;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }
    public ArrayList<InvoiceDto> invoiceDetail;
    public InvoiceDto gettotal;

    public InvoiceDto getGettotal() {
        return gettotal;
    }

    public void setGettotal(InvoiceDto gettotal) {
        this.gettotal = gettotal;
    }
    private String client;
    private String clientName;

    public String getClientEmail() {
        return clientEmail;
    }

    public void setClientEmail(String clientEmail) {
        this.clientEmail = clientEmail;
    }
    private String clientEmail;

    public ArrayList<InvoiceDto> getInvoiceDetail() {
        return invoiceDetail;
    }

    public void setInvoiceDetail(ArrayList<InvoiceDto> invoiceDetail) {
        this.invoiceDetail = invoiceDetail;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    private String invoiceDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList getInvoicelist() {
        return invoicelist;
    }

    public void setInvoicelist(ArrayList invoicelist) {
        this.invoicelist = invoicelist;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    public int getBday() {
        return bday;
    }

    public void setBday(int bday) {
        this.bday = bday;
    }

    public int getBmonth() {
        return bmonth;
    }

    public void setBmonth(int bmonth) {
        this.bmonth = bmonth;
    }

    public int getByear() {
        return byear;
    }

    public void setByear(int byear) {
        this.byear = byear;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public int getEday() {
        return eday;
    }

    public void setEday(int eday) {
        this.eday = eday;
    }

    public int getEmonth() {
        return emonth;
    }

    public void setEmonth(int emonth) {
        this.emonth = emonth;
    }

    public int getEyear() {
        return eyear;
    }

    public void setEyear(int eyear) {
        this.eyear = eyear;
    }

    public int getIday() {
        return iday;
    }

    public void setIday(int iday) {
        this.iday = iday;
    }

    public int getImonth() {
        return imonth;
    }

    public void setImonth(int imonth) {
        this.imonth = imonth;
    }

    public int getIyear() {
        return iyear;
    }

    public void setIyear(int iyear) {
        this.iyear = iyear;
    }
    private long clientId;
    private int byear;
    private int bmonth;
    private int bday;
    private int eyear;
    private int emonth;
    private int eday;
    private int iyear;
    private int imonth;
    private int iday;
    private String date;
    private String prefix;
    private float ratePerMin;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public float getRatePerMin() {
        return ratePerMin;
    }

    public void setRatePerMin(float ratePerMin) {
        this.ratePerMin = ratePerMin;
    }

    public double getTotalMin() {
        return totalMin;
    }

    public void setTotalMin(double totalMin) {
        this.totalMin = totalMin;
    }
    private double totalMin;
    private String totalTaka;

    public String getTotalTaka() {
        return totalTaka;
    }

    public void setTotalTaka(String totalTaka) {
        this.totalTaka = totalTaka;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param i
     */
    public void setNumber(int i) {
        number = i;
    }

    /**
     *
     */
    public InvoiceForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getClientId() == 0) {
            errors.add("clientId", new ActionMessage("error.clientId.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
}
