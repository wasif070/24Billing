/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class RateplanDTO {

    public boolean searchWithName = false;
    private long rateplan_id;
    private String rateplan_name;
    private String rateplan_des;
    private int rateplan_firstpulse;
    private int rateplan_pulse;
    private int rateplan_status;
    private String rateplan_statusname;
    private long rateplan_date;
    private String rateplan_create_date;
    private long user_id;
    private String user_name;
    private ArrayList rateplanList;

    public RateplanDTO() {
    }

    public String getRateplan_des() {
        return rateplan_des;
    }

    public void setRateplan_des(String rateplan_des) {
        this.rateplan_des = rateplan_des;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRateplan_name() {
        return rateplan_name;
    }

    public void setRateplan_name(String rateplan_name) {
        this.rateplan_name = rateplan_name;
    }

    public int getRateplan_status() {
        return rateplan_status;
    }

    public void setRateplan_status(int rateplan_status) {
        this.rateplan_status = rateplan_status;
    }

    public String getRateplan_statusname() {
        return rateplan_statusname;
    }

    public void setRateplan_statusname(String rateplan_statusname) {
        this.rateplan_statusname = rateplan_statusname;
    }

    public int getRateplan_firstpulse() {
        return rateplan_firstpulse;
    }

    public void setRateplan_firstpulse(int rateplan_firstpulse) {
        this.rateplan_firstpulse = rateplan_firstpulse;
    }

    public int getRateplan_pulse() {
        return rateplan_pulse;
    }

    public void setRateplan_pulse(int rateplan_pulse) {
        this.rateplan_pulse = rateplan_pulse;
    }

    public ArrayList getRateplanList() {
        return rateplanList;
    }

    public void setRateplanList(ArrayList rateplanList) {
        this.rateplanList = rateplanList;
    }

    public long getRateplan_date() {
        return rateplan_date;
    }

    public void setRateplan_date(long rateplan_date) {
        this.rateplan_date = rateplan_date;
    }

    public String getRateplan_create_date() {
        return rateplan_create_date;
    }

    public void setRateplan_create_date(String rateplan_create_date) {
        this.rateplan_create_date = rateplan_create_date;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
