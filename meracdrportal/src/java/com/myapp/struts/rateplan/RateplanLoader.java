/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.session.Constants;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class RateplanLoader {

    static Logger logger = Logger.getLogger(RateplanLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<RateplanDTO> rateplanList = null;
    private HashMap<Long, RateplanDTO> rateplanDTOByID = null;
    static RateplanLoader rateplanLoader = null;

    public RateplanLoader() {
        forceReload();
    }

    public static RateplanLoader getInstance() {
        if (rateplanLoader == null) {
            createRateplanLoader();
        }
        return rateplanLoader;
    }

    private synchronized static void createRateplanLoader() {
        if (rateplanLoader == null) {
            rateplanLoader = new RateplanLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            rateplanList = new ArrayList<RateplanDTO>();
            rateplanDTOByID = new HashMap<Long, RateplanDTO>();

            String sql = "select rateplan_id,rateplan_name,rateplan_des,rateplan_create_date,rateplan_status,user_id from mvts_rateplan where rateplan_delete=0 order by rateplan_id DESC";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RateplanDTO dto = new RateplanDTO();
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRateplan_name(resultSet.getString("rateplan_name"));
                dto.setRateplan_des(resultSet.getString("rateplan_des"));
                dto.setUser_id(resultSet.getLong("user_id"));
                dto.setRateplan_date(resultSet.getLong("rateplan_create_date"));
                dto.setRateplan_create_date(Utils.LongToDate(resultSet.getLong("rateplan_create_date")));
                UserDTO udto = UserLoader.getInstance().getUserDTOByID(dto.getUser_id());
                dto.setUser_name(udto.getUserId());
                dto.setRateplan_status(resultSet.getInt("rateplan_status"));
                dto.setRateplan_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRateplan_status()]);
                rateplanDTOByID.put(dto.getRateplan_id(), dto);
                rateplanList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in RateplanLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<RateplanDTO> getRateplanDTOList() {
        checkForReload();
        return rateplanList;
    }

    public synchronized RateplanDTO getRateplanDTOByID(long id) {
        checkForReload();
        return rateplanDTOByID.get(id);
    }

    public ArrayList<RateplanDTO> getRateplanDTOsWithSearchParam(RateplanDTO udto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<RateplanDTO> list = rateplanList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RateplanDTO dto = (RateplanDTO) i.next();
                if ((udto.searchWithName && !dto.getRateplan_name().toLowerCase().startsWith(udto.getRateplan_name()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<RateplanDTO> getRateplanDTOsSorted() {
        checkForReload();
        ArrayList<RateplanDTO> list = rateplanList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    RateplanDTO dto1 = (RateplanDTO) o1;
                    RateplanDTO dto2 = (RateplanDTO) o2;
                    if (dto1.getRateplan_id() < dto2.getRateplan_id()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
