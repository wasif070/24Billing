/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.util.MyAppError;
import com.mysql.jdbc.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class RateplanDAO {

    static Logger logger = Logger.getLogger(RateplanDAO.class.getName());

    public RateplanDAO() {
    }

    public MyAppError addRateplanInformation(RateplanDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select rateplan_name from mvts_rateplan where rateplan_name=? and rateplan_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rateplan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into mvts_rateplan(rateplan_name,rateplan_des,rateplan_status,rateplan_create_date,user_id) values(?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getRateplan_name());
            ps.setString(2, p_dto.getRateplan_des());
            ps.setInt(3, p_dto.getRateplan_status());
            ps.setLong(4, System.currentTimeMillis());
            ps.setLong(5, p_dto.getUser_id());

            ps.executeUpdate();
            RateplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editRateplanInformation(RateplanDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select rateplan_name from mvts_rateplan where rateplan_name = ? and rateplan_id!=" + p_dto.getRateplan_id() + " and rateplan_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRateplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Rate Plan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update mvts_rateplan set rateplan_name=?,rateplan_des=?,rateplan_status=?  where rateplan_id=" + p_dto.getRateplan_id();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getRateplan_name());
            ps.setString(2, p_dto.getRateplan_des());
            ps.setInt(3, p_dto.getRateplan_status());
            if(ps.executeUpdate()>0){
            RateplanLoader.getInstance().forceReload();
            }
            sql = "update mvts_rates set rate_status=? where rateplan_id=" + p_dto.getRateplan_id();
            ps = dbConnection.connection.prepareStatement(sql);
            
            ps.setInt(1, p_dto.getRateplan_status());
            ps.executeUpdate();
            
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String validIds = "-1";
        String invalidNames = "";
        String validNames = "";
        String msg = "";
        ArrayList<Long> allDestCodes = new ArrayList<Long>();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String destCodeIds = "-1";
            for (long destCode : destcodeIds) {
                destCodeIds += "," + destCode;
                allDestCodes.add(destCode);
            }

            sql = "select clients.rateplan_id from clients,mvts_rateplan where clients.rateplan_id=mvts_rateplan.rateplan_id and client_delete = 0 and clients.rateplan_id in(" + destCodeIds + ")";
            logger.debug("sql" + sql);
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            if (resultSet.next()) {
                long rate_paln_id = resultSet.getLong("rateplan_id");
                invalidNames += RateplanLoader.getInstance().getRateplanDTOByID(rate_paln_id).getRateplan_name() + ",";
                allDestCodes.remove(rate_paln_id);
            }
            for (long destCode : allDestCodes) {
                validIds += "," + destCode;
                validNames += RateplanLoader.getInstance().getRateplanDTOByID(destCode).getRateplan_name() + ",";
            }
            resultSet.close();

            if (validNames.length() > 0) {
                sql = "update mvts_rateplan set rateplan_delete = 1" + " where rateplan_id in (" + validIds + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    RateplanLoader.getInstance().forceReload();
                }
                sql = "update mvts_rates set rate_delete = 1,rate_delete_time=UNIX_TIMESTAMP() where rateplan_id in (" + validIds + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                ps.executeUpdate();

                msg += "These Rate plans have been deleted successfully : " + validNames.substring(0, validNames.length() - 1) + "<BR>";
            }

            if (invalidNames.length() > 0) {
                msg += "Rate Plan is assigned to a Client, can not be deleted : " + invalidNames.substring(0, invalidNames.length() - 1);
            }
            logger.debug("Message-->" + msg);

            error.setErrorMessage(msg);
        } catch (Exception ex) {
            logger.fatal("Error while deleting the Rate Plan!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
