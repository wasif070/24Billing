/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class GetRateplanAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        int id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            RateplanForm formBean = (RateplanForm) form;
            RateplanDTO dto = new RateplanDTO();
            RateplanTaskSchedular scheduler = new RateplanTaskSchedular();
            dto = scheduler.getRateplanDTO(id);
            if (dto != null) {
                formBean.setRateplan_id(dto.getRateplan_id());
                formBean.setRateplan_name(dto.getRateplan_name());
                formBean.setRateplan_des(dto.getRateplan_des());
                //formBean.setRateplan_firstpulse(dto.getRateplan_firstpulse());
                //formBean.setRateplan_pulse(dto.getRateplan_pulse());
                formBean.setRateplan_status(dto.getRateplan_status());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
