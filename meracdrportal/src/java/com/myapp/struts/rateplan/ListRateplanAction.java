/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class ListRateplanAction extends Action {

    static Logger logger = Logger.getLogger(ListRateplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            RateplanTaskSchedular scheduler = new RateplanTaskSchedular();
            RateplanForm rateplanForm = (RateplanForm) form;

            if (list_all == 0) {
                if (rateplanForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, rateplanForm.getRecordPerPage());
                }
                RateplanDTO ddto = new RateplanDTO();
                if (rateplanForm.getRateplan_name() != null && rateplanForm.getRateplan_name().trim().length() > 0) {
                    ddto.searchWithName = true;
                    ddto.setRateplan_name(rateplanForm.getRateplan_name().toLowerCase());
                }
                rateplanForm.setRateplanList(scheduler.getRateplanDTOsWithSearchParam(ddto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    rateplanForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                rateplanForm.setRateplanList(scheduler.getRateplanDTOs(login_dto));
            }

            if (rateplanForm.getRateplanList() != null && rateplanForm.getRateplanList().size() <= (rateplanForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                logger.debug("AAAA" + rateplanForm.getRateplanList().size());
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}