/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.home;

/**
 *
 * @author Wasif
 */
public class HomeDTO {

    private int org_client_id;
    private String org_client_name;
    private int org_calls;
    private int org_duration;
    private double org_client_balance;
    private int term_client_id;
    private String term_client_name;
    private int term_calls;
    private int term_duration;
    private double term_client_balance;

    public int getOrg_calls() {
        return org_calls;
    }

    public void setOrg_calls(int org_calls) {
        this.org_calls = org_calls;
    }

    public int getTerm_calls() {
        return term_calls;
    }

    public void setTerm_calls(int term_calls) {
        this.term_calls = term_calls;
    }

    public double getOrg_client_balance() {
        return org_client_balance;
    }

    public void setOrg_client_balance(double org_client_balance) {
        this.org_client_balance = org_client_balance;
    }

    public int getOrg_client_id() {
        return org_client_id;
    }

    public void setOrg_client_id(int org_client_id) {
        this.org_client_id = org_client_id;
    }

    public String getOrg_client_name() {
        return org_client_name;
    }

    public void setOrg_client_name(String org_client_name) {
        this.org_client_name = org_client_name;
    }

    public int getOrg_duration() {
        return org_duration;
    }

    public void setOrg_duration(int org_duration) {
        this.org_duration = org_duration;
    }

    public double getTerm_client_balance() {
        return term_client_balance;
    }

    public void setTerm_client_balance(double term_client_balance) {
        this.term_client_balance = term_client_balance;
    }

    public int getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(int term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_client_name() {
        return term_client_name;
    }

    public void setTerm_client_name(String term_client_name) {
        this.term_client_name = term_client_name;
    }

    public int getTerm_duration() {
        return term_duration;
    }

    public void setTerm_duration(int term_duration) {
        this.term_duration = term_duration;
    }
}
