/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.home;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class HomeLoader {

    static Logger logger = Logger.getLogger(HomeLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<HomeDTO> orgList = null;
    private ArrayList<HomeDTO> termList = null;
    static HomeLoader homeLoader = null;
    String yesterday = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())) - Utils.getTimeLong("24:00:00"));
    String now = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())));

    public HomeLoader() {
        forceReload();
    }

    public static HomeLoader getInstance() {
        if (homeLoader == null) {
            createHomeLoader();
        }
        return homeLoader;
    }

    private synchronized static void createHomeLoader() {
        if (homeLoader == null) {
            homeLoader = new HomeLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            orgList = new ArrayList<HomeDTO>();
            HomeDTO dto = new HomeDTO();
            String sql = "select origin_client_id,sum(duration),count(cdr_id) from flamma_cdr where connection_time>='" + yesterday + "' and connection_time<='" + now + "' and duration>0 and origin_client_id>0 group by origin_client_id";
            logger.debug("Sql: " + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dto.setOrg_client_id(resultSet.getInt("origin_client_id"));
                try {
                    ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(dto.getOrg_client_id());
                    dto.setOrg_client_name(clientDTO.getClient_id());
                    dto.setOrg_client_balance(Double.parseDouble(clientDTO.getClient_balance()));
                } catch (Exception ex) {
                }
                dto.setOrg_calls(resultSet.getInt("count(cdr_id)"));
                dto.setOrg_duration(resultSet.getInt("sum(duration)"));
                orgList.add(dto);
            }
            resultSet.close();


            sql = "select term_client_id,sum(duration),count(cdr_id) from flamma_cdr where connection_time>='" + yesterday + "' and connection_time<='" + now + "' and duration>0 and term_client_id>0 group by term_client_id";
            logger.debug("Sql: " + sql);
            resultSet = statement.executeQuery(sql);
            termList = new ArrayList<HomeDTO>();
            while (resultSet.next()) {
                dto.setTerm_client_id(resultSet.getInt("term_client_id"));
                try {
                    ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(dto.getTerm_client_id());
                    dto.setTerm_client_name(clientDTO.getClient_id());
                    dto.setTerm_client_balance(Double.parseDouble(clientDTO.getClient_balance()));
                } catch (Exception ex) {
                }
                dto.setTerm_calls(resultSet.getInt("count(cdr_id)"));
                dto.setTerm_duration(resultSet.getInt("sum(duration)"));
                termList.add(dto);
            }
        } catch (Exception e) {
            logger.fatal("Exception in HomeLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<HomeDTO> getOrgDTOList() {
        checkForReload();
        return orgList;
    }

    public synchronized ArrayList<HomeDTO> getTermDTOList() {
        checkForReload();
        return termList;
    }
}
