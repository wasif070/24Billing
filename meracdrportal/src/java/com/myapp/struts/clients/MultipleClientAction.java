/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.util.Utils;

/**
 *
 * @author Administrator
 */
public class MultipleClientAction extends Action {

    static Logger logger = Logger.getLogger(AddClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {

            ClientForm formBean = (ClientForm) form;
            ClientDTO dto = new ClientDTO();
            ClientTaskSchedular scheduler = new ClientTaskSchedular();

            MyAppError error = new MyAppError();

            if (formBean.getActivateBtn() != null && formBean.getActivateBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.activateMultiple(formBean.getSelectedIDs());
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (formBean.getInactiveBtn() != null && formBean.getInactiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.inactivateMultiple(formBean.getSelectedIDs());
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getBlockBtn() != null && formBean.getBlockBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.blockMultiple(formBean.getSelectedIDs());
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getDeleteBtn() != null && formBean.getDeleteBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.deleteMultiple(formBean.getSelectedIDs());
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getRechargeBtn() != null && formBean.getRechargeBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];
                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (Utils.isDouble(temp)) {
                                if (Double.parseDouble(temp) > 0) {
                                    valid_clients[i] = clients[i];
                                    amount[i] = Double.parseDouble(temp);
                                    description[i] = des;
                                } else {
                                    target = "failure_recharge";
                                    formBean.setMessage(true, error.getErrorMessage());
                                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                                    return changedActionForward;
                                }

                            } else {
                                target = "failure_recharge";
                                formBean.setMessage(true, error.getErrorMessage());
                                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                                return changedActionForward;
                            }
                        }
                    }
                    error = scheduler.rechargeMultiple(login_dto.getId(), valid_clients, amount, description);
                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    } else {
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance recharged successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getReturnBtn() != null && formBean.getReturnBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];
                    int inc = 0;
                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (temp != null) {
                                valid_clients[inc] = clients[i];
                                amount[inc] = Double.parseDouble(temp);
                                description[inc] = des;
                                inc++;
                            }
                        }
                    }
                    error = scheduler.returnMultiple(login_dto.getId(), valid_clients, amount, description);
                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    } else {
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance Returned successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getReceiveBtn() != null && formBean.getReceiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];
                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (temp != null) {
                                valid_clients[i] = clients[i];
                                amount[i] = Double.parseDouble(temp);
                                description[i] = des;
                            }
                        }
                    }
                    error = scheduler.receiveMultiple(login_dto.getId(), valid_clients, amount, description);
                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                    } else {
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance recieved successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "Client List is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}