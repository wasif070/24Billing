/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class RechargeListClientAction extends Action {

    static Logger logger = Logger.getLogger(RechargeListClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            ClientTaskSchedular scheduler = new ClientTaskSchedular();
            ClientForm clientForm = (ClientForm) form;

            if (list_all == 0) {
                if (clientForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, clientForm.getRecordPerPage());
                }
                ClientDTO cdto = new ClientDTO();
                if (clientForm.getClient_id() != null && clientForm.getClient_id().trim().length() > 0) {
                    cdto.searchWithClientID = true;
                    cdto.setClient_id(clientForm.getClient_id().toLowerCase());
                }
                if (clientForm.getClient_status() >= 0) {
                    cdto.searchWithStatus = true;
                    cdto.setClient_status(clientForm.getClient_status());
                }
                if (clientForm.getClient_type() >= 0) {
                    cdto.searchWithType = true;
                    cdto.setClient_type(clientForm.getClient_type());
                }
                clientForm.setClientList(scheduler.getClientDTOsWithSearchParam(cdto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    clientForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                clientForm.setClientList(scheduler.getClientDTOs(login_dto));
            }
            clientForm.setSelectedIDs(null);
            logger.debug("Number of Clients-->" + clientForm.getClientList().size());
            if (clientForm.getClientList() != null && clientForm.getClientList().size() <= (clientForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}