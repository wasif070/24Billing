package com.myapp.struts.clients;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetClientAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            ClientForm formBean = (ClientForm) form;
            ClientDTO dto = new ClientDTO();
            ClientTaskSchedular scheduler = new ClientTaskSchedular();
            dto = scheduler.getClientDTO(id);
            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setRetypePassword(dto.getClient_password());
                formBean.setClient_id(dto.getClient_id());
                formBean.setClient_password(dto.getClient_password());
                formBean.setClient_name(dto.getClient_name());
                formBean.setClient_email(dto.getClient_email());
                formBean.setIncoming_prefix(dto.getIncoming_prefix());
                formBean.setIncoming_to(dto.getIncoming_to());
                formBean.setOutgoing_prefix(dto.getOutgoing_prefix());
                formBean.setOutgoing_to(dto.getOutgoing_to());
                formBean.setClient_status(dto.getClient_status());
                formBean.setClient_type(dto.getClient_type());
                formBean.setRateplan_id(dto.getRateplan_id());
                formBean.setClient_credit_limit(dto.getClient_credit_limit());
                formBean.setClient_balance(dto.getClient_balance());
                formBean.setPrefix(dto.getPrefix());
                formBean.setClient_call_limit(dto.getClient_call_limit());

            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
