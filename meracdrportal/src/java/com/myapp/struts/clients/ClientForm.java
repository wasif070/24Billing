package com.myapp.struts.clients;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ClientForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int client_status;
    private long id;
    private String client_id;
    private long rateplan_id;
    private String client_password;
    private String retypePassword;
    private String client_name;
    private String client_email;
    private String prefix;
    private String incoming_prefix;
    private String incoming_to;
    private String outgoing_prefix;
    private String outgoing_to;
    private int client_type;
    private String message;
    private long[] selectedIDs;
    private ArrayList clientList;
    private String activateBtn;
    private String inactiveBtn;
    private String blockBtn;
    private String deleteBtn;
    private String rechargeBtn;
    private String returnBtn;
    private String receiveBtn;
    private String client_credit_limit;
    private String client_balance;
    private int client_call_limit;

    public int getClient_call_limit() {
        return client_call_limit;
    }

    public void setClient_call_limit(int client_call_limit) {
        this.client_call_limit = client_call_limit;
    }

    public ClientForm() {
        super();
        client_type = -1;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public ArrayList getClientList() {
        return clientList;
    }

    public void setClientList(ArrayList clientList) {
        this.clientList = clientList;
    }

    public String getClient_email() {
        return client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_password() {
        return client_password;
    }

    public void setClient_password(String client_password) {
        this.client_password = client_password;
    }

    public int getClient_status() {
        return client_status;
    }

    public void setClient_status(int client_status) {
        this.client_status = client_status;
    }

    public int getClient_type() {
        return client_type;
    }

    public void setClient_type(int client_type) {
        this.client_type = client_type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getIncoming_prefix() {
        return incoming_prefix;
    }

    public void setIncoming_prefix(String incoming_prefix) {
        this.incoming_prefix = incoming_prefix;
    }

    public String getIncoming_to() {
        return incoming_to;
    }

    public void setIncoming_to(String incoming_to) {
        this.incoming_to = incoming_to;
    }

    public String getOutgoing_prefix() {
        return outgoing_prefix;
    }

    public void setOutgoing_prefix(String outgoing_prefix) {
        this.outgoing_prefix = outgoing_prefix;
    }

    public String getOutgoing_to() {
        return outgoing_to;
    }

    public void setOutgoing_to(String outgoing_to) {
        this.outgoing_to = outgoing_to;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    public String getActivateBtn() {
        return activateBtn;
    }

    public void setActivateBtn(String activateBtn) {
        this.activateBtn = activateBtn;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public String getInactiveBtn() {
        return inactiveBtn;
    }

    public void setInactiveBtn(String inactiveBtn) {
        this.inactiveBtn = inactiveBtn;
    }

    public String getBlockBtn() {
        return blockBtn;
    }

    public void setBlockBtn(String blockBtn) {
        this.blockBtn = blockBtn;
    }

    public String getRechargeBtn() {
        return rechargeBtn;
    }

    public void setRechargeBtn(String rechargeBtn) {
        this.rechargeBtn = rechargeBtn;
    }

    public String getReturnBtn() {
        return returnBtn;
    }

    public void setReturnBtn(String returnBtn) {
        this.returnBtn = returnBtn;
    }

    public String getReceiveBtn() {
        return receiveBtn;
    }

    public void setReceiveBtn(String receiveBtn) {
        this.receiveBtn = receiveBtn;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(String client_balance) {
        this.client_balance = client_balance;
    }

    public String getClient_credit_limit() {
        return client_credit_limit;
    }

    public void setClient_credit_limit(String client_credit_limit) {
        this.client_credit_limit = client_credit_limit;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            boolean pass = true;
            boolean re_pass = true;
            if (getClient_id() == null || getClient_id().length() < 1) {
                errors.add("client_id", new ActionMessage("errors.client_id.required"));
            }

            if (getClient_password() == null || getClient_password().length() < 1) {
                errors.add("client_password", new ActionMessage("errors.client_password.required"));
                pass = false;
            }

            if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                re_pass = false;
            }

            if (pass == true && re_pass == true) {
                if (getClient_password().length() < 6) {
                    errors.add("retypePassword", new ActionMessage("errors.password_length"));
                } else if (!getRetypePassword().equals(getClient_password())) {
                    errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                }
            }
            if (getClient_email() == null || getClient_email().length() < 1) {
                errors.add("client_email", new ActionMessage("errors.client_email.required"));
            }
            if (getClient_email() != null || getClient_email().length() > 1) {
                boolean valid = Utils.checkEmailId(getClient_email());
                if (valid == false) {
                    errors.add("client_email", new ActionMessage("errors.client_email1.required"));
                }
            }
            if (getClient_type() == -1) {
                errors.add("client_type", new ActionMessage("errors.client_type.required"));
            }

            if (getClient_type() != 1 && getRateplan_id() == 0) {
                errors.add("rateplan_id", new ActionMessage("errors.rateplan_id.required"));
            }
        }
        return errors;
    }
}
