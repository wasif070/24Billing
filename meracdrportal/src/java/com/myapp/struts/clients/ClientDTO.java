package com.myapp.struts.clients;

public class ClientDTO {

    public boolean searchWithClientID = false;
    public boolean searchWithStatus = false;
    public boolean searchWithType = false;
    private boolean superUser;
    private int client_status;
    private long id;
    private String client_id;
    private long rateplan_id;
    private String rateplan_name;
    private String client_password;
    private String retypePassword;
    private String client_name;
    private String client_email;
    private int client_type;
    private String client_status_name;
    private String client_type_name;
    private String prefix;
    private String incoming_prefix;
    private String incoming_to;
    private String outgoing_prefix;
    private String outgoing_to;
    private long[] selectedIDs;
    private String client_credit_limit;
    private String client_balance;
    private int client_call_limit;

    public int getClient_call_limit() {
        return client_call_limit;
    }

    public void setClient_call_limit(int client_call_limit) {
        this.client_call_limit = client_call_limit;
    }

    public ClientDTO() {
        superUser = false;
    }

    public boolean getSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClient_email() {
        return client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getClient_password() {
        return client_password;
    }

    public void setClient_password(String client_password) {
        this.client_password = client_password;
    }

    public int getClient_status() {
        return client_status;
    }

    public void setClient_status(int client_status) {
        this.client_status = client_status;
    }

    public int getClient_type() {
        return client_type;
    }

    public void setClient_type(int client_type) {
        this.client_type = client_type;
    }

    public String getClient_type_name() {
        return client_type_name;
    }

    public void setClient_type_name(String client_type_name) {
        this.client_type_name = client_type_name;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String getClient_status_name() {
        return client_status_name;
    }

    public void setClient_status_name(String client_status_name) {
        this.client_status_name = client_status_name;
    }

    public boolean isSearchWithStatus() {
        return searchWithStatus;
    }

    public void setSearchWithStatus(boolean searchWithStatus) {
        this.searchWithStatus = searchWithStatus;
    }

    public boolean isSearchWithClientID() {
        return searchWithClientID;
    }

    public void setSearchWithClientID(boolean searchWithClientID) {
        this.searchWithClientID = searchWithClientID;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getIncoming_prefix() {
        return incoming_prefix;
    }

    public void setIncoming_prefix(String incoming_prefix) {
        this.incoming_prefix = incoming_prefix;
    }

    public String getIncoming_to() {
        return incoming_to;
    }

    public void setIncoming_to(String incoming_to) {
        this.incoming_to = incoming_to;
    }

    public String getOutgoing_prefix() {
        return outgoing_prefix;
    }

    public void setOutgoing_prefix(String outgoing_prefix) {
        this.outgoing_prefix = outgoing_prefix;
    }

    public String getOutgoing_to() {
        return outgoing_to;
    }

    public void setOutgoing_to(String outgoing_to) {
        this.outgoing_to = outgoing_to;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRateplan_name() {
        return rateplan_name;
    }

    public void setRateplan_name(String rateplan_name) {
        this.rateplan_name = rateplan_name;
    }

    public String getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(String client_balance) {
        this.client_balance = client_balance;
    }

    public String getClient_credit_limit() {
        return client_credit_limit;
    }

    public void setClient_credit_limit(String client_credit_limit) {
        this.client_credit_limit = client_credit_limit;
    }
}