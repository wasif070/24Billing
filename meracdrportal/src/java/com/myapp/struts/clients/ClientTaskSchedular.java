package com.myapp.struts.clients;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ClientTaskSchedular {

    public ClientTaskSchedular() {
    }

    public MyAppError addClientInformation(long user_id, ClientDTO p_dto) {
        ClientDAO userDAO = new ClientDAO();
        return userDAO.addClientInformation(user_id, p_dto);
    }

    public MyAppError editClientInformation(ClientDTO p_dto) {
        ClientDAO userDAO = new ClientDAO();
        return userDAO.editClientInformation(p_dto);
    }

    public MyAppError clientEditInformation(ClientDTO p_dto) {
        ClientDAO userDAO = new ClientDAO();
        return userDAO.clientEditInformation(p_dto);
    }

    public MyAppError deleteClient(int cid) {
        ClientDAO dao = new ClientDAO();
        return dao.deleteClient(cid);
    }

    public ClientDTO getClientDTO(long id) {
        return ClientLoader.getInstance().getClientDTOByID(id);
    }

    public ArrayList<ClientDTO> getClientDTOsSorted(LoginDTO l_dto) {
        return ClientLoader.getInstance().getClientDTOsSorted();
    }

    public ArrayList<ClientDTO> getClientDTOs(LoginDTO l_dto) {
        return ClientLoader.getInstance().getClientDTOList();
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ClientDTO udto, LoginDTO l_dto) {
        return ClientLoader.getInstance().getClientDTOsWithSearchParam(udto);
    }

    public MyAppError activateMultiple(long clientIds[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientStatusUpdate(clientIds, Constants.LIVE_ACTIVE);
    }

    public MyAppError inactivateMultiple(long clientIds[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientStatusUpdate(clientIds, Constants.LIVE_INACTIVE);
    }

    public MyAppError blockMultiple(long clientIds[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientStatusUpdate(clientIds, Constants.LIVE_BLOCK);
    }

    public MyAppError deleteMultiple(long clientIds[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientDelete(clientIds);
    }

    public MyAppError rechargeMultiple(long user_id, long clientIds[], double amount[], String des[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientRecharge(user_id, clientIds, amount, des);
    }

    public MyAppError returnMultiple(long user_id, long clientIds[], double amount[], String des[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientReturn(user_id, clientIds, amount, des);
    }

    public MyAppError receiveMultiple(long user_id, long clientIds[], double amount[], String des[]) {
        ClientDAO dao = new ClientDAO();
        return dao.multipleClientReceive(user_id, clientIds, amount, des);
    }
}
