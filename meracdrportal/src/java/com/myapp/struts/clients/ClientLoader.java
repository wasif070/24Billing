package com.myapp.struts.clients;

import com.myapp.struts.rateplan.RateplanDTO;
import com.myapp.struts.rateplan.RateplanLoader;
import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.text.DecimalFormat;

public class ClientLoader {

    static Logger logger = Logger.getLogger(ClientLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<ClientDTO> clientList = null;
    private HashMap<Long, ClientDTO> clientDTOByID = null;
    static ClientLoader clientLoder = null;
    DecimalFormat df = new DecimalFormat("#00.00");

    public ClientLoader() {
        forceReload();
    }

    public static ClientLoader getInstance() {
        if (clientLoder == null) {
            createClientLoader();
        }
        return clientLoder;
    }

    private synchronized static void createClientLoader() {
        if (clientLoder == null) {
            clientLoder = new ClientLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            clientList = new ArrayList<ClientDTO>();
            clientDTOByID = new HashMap<Long, ClientDTO>();

            String sql = "select id,client_id,client_password,client_name,client_email,client_type,client_status,rateplan_id,client_credit_limit,client_balance,prefix,client_call_limit from clients where client_delete=0 order by id DESC";
            logger.debug("sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            ClientDTO dto = new ClientDTO();
            while (resultSet.next()) {
                dto = new ClientDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setClient_id(resultSet.getString("client_id"));
                dto.setClient_password(resultSet.getString("client_password"));
                dto.setClient_name(resultSet.getString("client_name"));
                dto.setClient_email(resultSet.getString("client_email"));
                dto.setPrefix(resultSet.getString("prefix"));
                dto.setClient_type(resultSet.getInt("client_type"));
                dto.setClient_type_name(Constants.CLIENT_TYPE_NAME[dto.getClient_type()]);
                dto.setClient_status(resultSet.getInt("client_status"));
                dto.setClient_status_name(Constants.LIVE_STATUS_STRING[dto.getClient_status()]);
                dto.setRateplan_id(resultSet.getLong("rateplan_id"));
                if (dto.getRateplan_id() > 0) {
                    RateplanDTO rdto = RateplanLoader.getInstance().getRateplanDTOByID(dto.getRateplan_id());
                    if (rdto != null) {
                        dto.setRateplan_name(rdto.getRateplan_name());
                    }
                }
                dto.setClient_credit_limit(df.format(resultSet.getDouble("client_credit_limit")));
                dto.setClient_balance(df.format(resultSet.getDouble("client_balance")));
                dto.setClient_call_limit(resultSet.getInt("client_call_limit"));
                dto.setSuperUser(false);
                clientDTOByID.put(dto.getId(), dto);
                clientList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<ClientDTO> getClientDTOList() {
        checkForReload();
        return clientList;
    }

    public synchronized ClientDTO getClientDTOByID(long id) {
        checkForReload();
        return clientDTOByID.get(id);
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByType(int type) {
        checkForReload();
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH) {
                clientListByType.add(dto);
            }
        }
        return clientListByType;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOById(int id) {
        checkForReload();
        ArrayList<ClientDTO> clientListByid = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getId() == id) {
                clientListByid.add(dto);
            }
        }
        return clientListByid;
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ClientDTO udto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<ClientDTO> list = clientList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((udto.searchWithClientID && !dto.getClient_id().toLowerCase().startsWith(udto.getClient_id()))
                        || (udto.searchWithStatus && dto.getClient_status() != udto.getClient_status())
                        || (udto.searchWithType && dto.getClient_type() != udto.getClient_type())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted() {
        checkForReload();
        ArrayList<ClientDTO> list = clientList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
