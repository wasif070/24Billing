/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

/**
 *
 * @author Anwar
 */
public class TransactionDTO {

    private long client_id;
    private int transaction_type;
    private String client_name;
    private String client_full_name;
    private long transaction_id;
    private String transaction_recharge;
    private String transaction_return;
    private String transaction_receive;
    private String transaction_balance;
    private String transaction_des;
    private long transaction_date;
    private String transaction_date_string;
    private long user_id;
    private String user_name;
    private String fromDate;
    private String toDate;
    boolean searchWithClientID = false;

    public TransactionDTO() {
    }

    public String getClient_full_name() {
        return client_full_name;
    }

    public void setClient_full_name(String client_full_name) {
        this.client_full_name = client_full_name;
    }

    public long getClient_id() {
        return client_id;
    }

    public void setClient_id(long client_id) {
        this.client_id = client_id;
    }

    public int getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(int transaction_type) {
        this.transaction_type = transaction_type;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public long getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(long transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getTransaction_des() {
        return transaction_des;
    }

    public void setTransaction_des(String transaction_des) {
        this.transaction_des = transaction_des;
    }

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getTransaction_date_string() {
        return transaction_date_string;
    }

    public void setTransaction_date_string(String transaction_date_string) {
        this.transaction_date_string = transaction_date_string;
    }

    public String getTransaction_balance() {
        return transaction_balance;
    }

    public void setTransaction_balance(String transaction_balance) {
        this.transaction_balance = transaction_balance;
    }

    public String getTransaction_receive() {
        return transaction_receive;
    }

    public void setTransaction_receive(String transaction_receive) {
        this.transaction_receive = transaction_receive;
    }

    public String getTransaction_recharge() {
        return transaction_recharge;
    }

    public void setTransaction_recharge(String transaction_recharge) {
        this.transaction_recharge = transaction_recharge;
    }

    public String getTransaction_return() {
        return transaction_return;
    }

    public void setTransaction_return(String transaction_return) {
        this.transaction_return = transaction_return;
    }

  
    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
