/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class TransactionForm extends org.apache.struts.action.ActionForm {

    private long client_id;
    private int transaction_type;
    private long transaction_id;
    private String transaction_recharge;
    private String transaction_return;
    private String transaction_des;
    private long transaction_date;
    private long user_id;
    private int pageNo;
    private int recordPerPage;
    private ArrayList transactionList;
    private int fromDay;
    private int fromMonth;
    private int fromYear;
    private int fromHour;
    private int fromMin;
    private int toDay;
    private int toMonth;
    private int toYear;
    private int toHour;
    private int toMin;

    public TransactionForm() {

        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        fromDay = day;
        fromMonth = month;
        fromYear = year;

        toDay = day;
        toMonth = month;
        toYear = year;
        toHour = 23;
        toMin = 59;
    }

    public long getClient_id() {
        return client_id;
    }

    public void setClient_id(long client_id) {
        this.client_id = client_id;
    }

    public int getTransaction_type() {
        return transaction_type;
    }

    public void setTransaction_type(int transaction_type) {
        this.transaction_type = transaction_type;
    }

    public long getTransaction_date() {
        return transaction_date;
    }

    public void setTransaction_date(long transaction_date) {
        this.transaction_date = transaction_date;
    }

    public String getTransaction_des() {
        return transaction_des;
    }

    public void setTransaction_des(String transaction_des) {
        this.transaction_des = transaction_des;
    }

    public long getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(long transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getTransaction_recharge() {
        return transaction_recharge;
    }

    public void setTransaction_recharge(String transaction_recharge) {
        this.transaction_recharge = transaction_recharge;
    }

    public String getTransaction_return() {
        return transaction_return;
    }

    public void setTransaction_return(String transaction_return) {
        this.transaction_return = transaction_return;
    }


    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public ArrayList getTransactionList() {
        return transactionList;
    }

    public void setTransactionList(ArrayList transactionList) {
        this.transactionList = transactionList;
    }

    public int getFromDay() {
        return fromDay;
    }

    public void setFromDay(int fromDay) {
        this.fromDay = fromDay;
    }

    public int getFromHour() {
        return fromHour;
    }

    public void setFromHour(int fromHour) {
        this.fromHour = fromHour;
    }

    public int getFromMin() {
        return fromMin;
    }

    public void setFromMin(int fromMin) {
        this.fromMin = fromMin;
    }

    public int getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(int fromMonth) {
        this.fromMonth = fromMonth;
    }

    public int getFromYear() {
        return fromYear;
    }

    public void setFromYear(int fromYear) {
        this.fromYear = fromYear;
    }

    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public int getToHour() {
        return toHour;
    }

    public void setToHour(int toHour) {
        this.toHour = toHour;
    }

    public int getToMin() {
        return toMin;
    }

    public void setToMin(int toMin) {
        this.toMin = toMin;
    }

    public int getToMonth() {
        return toMonth;
    }

    public void setToMonth(int toMonth) {
        this.toMonth = toMonth;
    }

    public int getToYear() {
        return toYear;
    }

    public void setToYear(int toYear) {
        this.toYear = toYear;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
