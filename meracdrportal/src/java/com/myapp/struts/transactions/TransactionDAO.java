/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.util.Utils;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class TransactionDAO {

    static Logger logger = Logger.getLogger(TransactionDAO.class.getName());

    public TransactionDAO() {
    }

    public ArrayList<TransactionDTO> getTransactionDTOsWithSearchParam(ArrayList<TransactionDTO> list, TransactionDTO tdto) {
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();

            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                if (tdto.searchWithClientID && tdto.getClient_id() != dto.getClient_id()) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<TransactionDTO> getTransactionDTOs(ArrayList<TransactionDTO> list, TransactionDTO tdto) {
        ArrayList newList = null;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();

            while (i.hasNext()) {
                TransactionDTO dto = (TransactionDTO) i.next();
                if (dto.getTransaction_date() < Utils.getDateLong(tdto.getFromDate()) || dto.getTransaction_date() > Utils.getDateLong(tdto.getToDate())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<TransactionDTO> getTransactionDTOsSorted(ArrayList<TransactionDTO> list) {
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    TransactionDTO dto1 = (TransactionDTO) o1;
                    TransactionDTO dto2 = (TransactionDTO) o2;
                    if (dto1.getTransaction_id() < dto2.getTransaction_id()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
