/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class TransactionTaskScheduler {

    public TransactionTaskScheduler() {
    }

    public ArrayList<TransactionDTO> getTransactionDTOsSorted(LoginDTO l_dto) {
        return TransactionLoader.getInstance().getTransactionDTOsSorted();

    }

    public ArrayList<TransactionDTO> getTransactionDTOs(TransactionDTO udto, LoginDTO l_dto) {
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionDTOList();
        if (list != null) {
            return TransactionLoader.getInstance().getTransactionDTOs((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public ArrayList<TransactionDTO> setTransactionSummeryList(LoginDTO l_dto) {
        return TransactionLoader.getInstance().getTransactionSummeryDTOList();
    }

    public ArrayList<TransactionDTO> getTransactionSummeryDTOsWithSearchParam(TransactionDTO udto, LoginDTO l_dto) {
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionSummeryDTOList();
        if (list != null) {
            return TransactionLoader.getInstance().getTransactionDTOsWithSearchParam((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public ArrayList<TransactionDTO> getTransactionDTOsWithSearchParam(TransactionDTO udto, LoginDTO l_dto) {
        ArrayList<TransactionDTO> list = TransactionLoader.getInstance().getTransactionDTOList();
        if (list != null) {
            return TransactionLoader.getInstance().getTransactionDTOsWithSearchParam((ArrayList<TransactionDTO>) list.clone(), udto);
        }
        return null;
    }

    public TransactionDTO getTransactionSummeryByClient(long client_id) {
        TransactionDTO tdto = TransactionLoader.getInstance().getTransactionSummeryByClient(client_id);
        return tdto;
    }
}
