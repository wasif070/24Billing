package com.myapp.struts.settings;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Wasif
 */
public class SettingsLoader {

    static Logger logger = Logger.getLogger(SettingsLoader.class.getName());
    private static long LOADING_INTERVAL = 15 * 60 * 1000;
    private long loadingTime = 0;
    static SettingsLoader settingsLoader = null;
    private HashMap<String, SettingsDTO> settingsDTOMap = null;
    private HashMap<Integer, SettingsDTO> settingsDTOMapById = null;

    public SettingsLoader() {
        forceReload();
    }

    public static SettingsLoader getInstance() {
        if (settingsLoader == null) {
            createSettingsLoader();
        }
        return settingsLoader;
    }

    private synchronized static void createSettingsLoader() {
        if (settingsLoader == null) {
            settingsLoader = new SettingsLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            settingsDTOMap = new HashMap<String, SettingsDTO>();
            settingsDTOMapById = new HashMap<Integer, SettingsDTO>();
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from settings order by param ASC";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                SettingsDTO dto = new SettingsDTO();
                dto.setId(resultSet.getInt("id"));
                dto.setSettingName(resultSet.getString("param"));
                dto.setSettingValue(resultSet.getString("value"));
                settingsDTOMap.put(dto.getSettingName(), dto);
                settingsDTOMapById.put(dto.getId(), dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in SettingsLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<SettingsDTO> getSettingsDTOList() {
        checkForReload();
        ArrayList<SettingsDTO> data = new ArrayList<SettingsDTO>();
        Set set = settingsDTOMap.entrySet();
        Iterator i = set.iterator();
        while (i.hasNext()) {
            Map.Entry me = (Map.Entry) i.next();
            SettingsDTO dto = (SettingsDTO) me.getValue();
            data.add(dto);
        }
        return data;
    }

    public synchronized SettingsDTO getSettingsDTO(String settingName) {
        checkForReload();
        return settingsDTOMap.get(settingName);
    }

    public synchronized SettingsDTO getSettingsDTOById(int id) {
        checkForReload();
        return settingsDTOMapById.get(id);
    }
}