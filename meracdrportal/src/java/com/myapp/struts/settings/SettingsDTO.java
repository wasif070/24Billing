package com.myapp.struts.settings;

import java.util.ArrayList;

/**
 *
 * @author Wasif
 */
public class SettingsDTO {

    private int id;
    private int sign;
    private int hour;
    private int min;
    private int sec;
    private String settingName;
    private String settingValue;
    private ArrayList<SettingsDTO> settingsList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getSec() {
        return sec;
    }

    public void setSec(int sec) {
        this.sec = sec;
    }

    public int getSign() {
        return sign;
    }

    public void setSign(int sign) {
        this.sign = sign;
    }

    public String getSettingName() {
        return settingName;
    }

    public void setSettingName(String settingName) {
        this.settingName = settingName;
    }

    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    public ArrayList<SettingsDTO> getSettingsList() {
        return settingsList;
    }

    public void setSettingsList(ArrayList<SettingsDTO> settingsList) {
        this.settingsList = settingsList;
    }
}
