package com.myapp.struts.settings;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class GetSettingsAction extends Action {

    static Logger logger = Logger.getLogger(GetSettingsAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            SettingsForm formBean = (SettingsForm) form;
            SettingsDTO dto = new SettingsDTO();
            SettingsTaskSchedular scheduler = new SettingsTaskSchedular();
            dto = scheduler.getSettingsDTOById(Integer.parseInt(request.getParameter("id")));
            if (dto != null) {
                formBean.setId(dto.getId());
                logger.debug("dto.getId()-->" + dto.getId());
                if (dto.getId() == 1) {
                    String str_time = dto.getSettingValue();
                    String[] temp;
                    if (str_time.startsWith("+")) {
                        formBean.setSign(1);
                    } else {
                        formBean.setSign(2);
                    }
                    str_time = str_time.substring(1);
                    temp = str_time.split(":");
                    formBean.setHour(Integer.parseInt(temp[0]));
                    formBean.setMin(Integer.parseInt(temp[1]));
                    formBean.setSec(Integer.parseInt(temp[2]));
                }
                if (dto.getId() == 2) {
                    formBean.setSettingValue(dto.getSettingValue());
                }

            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";

        }
        request.getSession(true).setAttribute("id", Integer.parseInt(request.getParameter("id")));
        return (mapping.findForward(target));
    }
}
