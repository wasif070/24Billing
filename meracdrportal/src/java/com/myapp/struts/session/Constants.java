package com.myapp.struts.session;

public class Constants {
    /* Action value */

    public final static int ADD = 1;
    public final static int EDIT = 2;
    public final static int UPDATE = 3;
    public final static int DELETE = 4;
    public final static int BLOCK = 10;
    public final static int CHECK_VALIDATION = 1;
    public final static int UPLOAD = 5;
    /*Login related constants*/
    public final static String LOGIN_DTO = "LOGIN_DTO";
    public final static String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public final static String LOGIN_TIMEOUT = "Your Login Time Is Expired!!!";
    public final static String LOGIN_ACCESS_DENIED = "Access Denied!!!";
    public final static long LOGIN_EXPIRE_TIME = 60 * 60 * 1000L;
    public final static String LOGIN_TEXT = "A Best Solution for VOIP Billing";
    /*User & client related constants*/
    public final static int RECORD_PER_PAGE = 10;
    public final static String USER_LIST = "USER_LIST";
    public final static String USER_ID_LIST = "USER_ID_LIST";
    public final static String USER_RECORD_PER_PAGE = "USER_RECORD_PER_PAGE";
    public final static String MESSAGE = "message_str";
    public final static int CLIENT_ALL = 0;
    public final static int ORIGINATION = 0;
    public final static int TERMINATION = 1;
    public final static int BOTH = 2;
    public final static String[] CLIENT_TYPE = {"0", "1", "2"};
    public final static String[] CLIENT_TYPE_NAME = {"Origination", "Termination", "Both"};
    public final static int USER_STATUS_ACTIVE = 0;
    public final static int USER_STATUS_BLOCK = 1;
    public final static String USER_STATUS_VALUE[] = {"0", "1"};
    public final static String USER_STATUS_STRING[] = {"Active", "Block"};
    public final static int LIVE_ACTIVE = 0;
    public final static int LIVE_INACTIVE = 1;
    public final static int LIVE_BLOCK = 2;
    public final static String LIVE_STATUS_VALUE[] = {"0", "1", "2"};
    public final static String LIVE_STATUS_STRING[] = {"Active", "Inactive", "Block"};
    public final static String GATEWAY_STATUS_VALUE[] = {"0", "1"};
    public final static String GATEWAY_STATUS_STRING[] = {"Active", "Block"};
    public final static String UNIT_TIME_VALUE[] = {"0", "1", "2", "3", "4"};
    public final static String UNIT_TIME_STRING[] = {"None", "15 mins", "Hourly", "Daily", "Weekly", "Monthly"};
    public final static String SUMMARY_VALUE[] = {"0", "1", "2", "3"};
    public final static String SUMMARY_STRING[] = {"Origination IP", "Origin Destination", "Termination IP", "Termination Destination"};
    public final static int PER_PAGE_RECORD = 500;
    public final static String DAY_VALUE[] = {"-1", "0", "1", "2", "3", "4", "5", "6"};
    public final static String DAY_STRING[] = {"All", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public final static String TRANSACTION_TYPE[] = {"0", "1", "2", "3"};
    public final static String TRANSACTION_TYPE_STRING[] = {"All", "Recharge", "Return", "Receive"};
    public final static int RECHARGE_AMOUNT = 1;
    public final static int RETURN_AMOUNT = 2;
    public final static int RECEIVE_AMOUNT = 3;
    public final static String DIALPLAN_STATUS_VALUE[] = {"0", "1"};
    public final static String DIALPLAN_STATUS_STRING[] = {"Inactive", "Active"};
    public final static String DIALPLAN_BALANCING_METHOD_VALUE[] = {"0", "1", "2", "3"};
    public final static String DIALPLAN_BALANCING_METHOD_STRING[] = {"No balancing", "Round robin balancing", "Balancing by absolute load", "Balancing by load capacity ratio"};
    public final static String DIALPLAN_SCHEDULING_VALUE[] = {"0", "1"};
    public final static String DIALPLAN_SCHEDULING_STRING[] = {"No Schedule", "Time of Day"};
    public final static String MONTH_VALUE[] = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12"};
    public final static String MONTH_NAME[] = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
}
