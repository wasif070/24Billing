package com.myapp.struts.currentcall;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class ListCurrentcallAction extends Action {

    static Logger logger = Logger.getLogger(ListCurrentcallAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);        
        if (login_dto != null) {
            CurrentcallTaskSchedular scheduler = new CurrentcallTaskSchedular();
            CurrentcallForm currentcallForm = (CurrentcallForm) form;
            if (login_dto.getSuperUser() || login_dto.isUser()) {
                currentcallForm.setCurrentcallList(scheduler.getCurrentcallDTOs(0));
            } else {
                currentcallForm.setCurrentcallList(scheduler.getCurrentcallDTOs(login_dto.getId()));
            }
            target = "success";
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "failure";
        }

        return (mapping.findForward(target));
    }
}
