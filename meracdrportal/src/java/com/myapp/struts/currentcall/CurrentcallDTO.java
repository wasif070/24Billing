package com.myapp.struts.currentcall;

import java.util.ArrayList;

/**
 *
 * @author Wasif
 */
public class CurrentcallDTO {

    private String current_id;
    private String current_dial_no_in;
    private String current_dial_no_out;
    private String current_org_gateway_name;
    private String current_org_gateway_ip;
    private String current_term_gateway_name;
    private String current_term_gateway_ip;
    private String current_setup_time;
    private String current_connect_time;
    private String origin_client_name;
    private String term_client_name;
    private long origin_client_id;
    private long term_client_id;
    private long current_duration;
    private String active_duration;
    private ArrayList<CurrentcallDTO> currentcallList;

    public String getCurrent_dial_no_in() {
        return current_dial_no_in;
    }

    public void setCurrent_dial_no_in(String current_dial_no_in) {
        this.current_dial_no_in = current_dial_no_in;
    }

    public String getCurrent_dial_no_out() {
        return current_dial_no_out;
    }

    public void setCurrent_dial_no_out(String current_dial_no_out) {
        this.current_dial_no_out = current_dial_no_out;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    private int status;

    public String getCurrent_org_gateway_ip() {
        return current_org_gateway_ip;
    }

    public void setCurrent_org_gateway_ip(String current_org_gateway_ip) {
        this.current_org_gateway_ip = current_org_gateway_ip;
    }

    public String getCurrent_org_gateway_name() {
        return current_org_gateway_name;
    }

    public void setCurrent_org_gateway_name(String current_org_gateway_name) {
        this.current_org_gateway_name = current_org_gateway_name;
    }

    public String getCurrent_term_gateway_ip() {
        return current_term_gateway_ip;
    }

    public void setCurrent_term_gateway_ip(String current_term_gateway_ip) {
        this.current_term_gateway_ip = current_term_gateway_ip;
    }

    public String getCurrent_term_gateway_name() {
        return current_term_gateway_name;
    }

    public void setCurrent_term_gateway_name(String current_term_gateway_name) {
        this.current_term_gateway_name = current_term_gateway_name;
    }

    public String getCurrent_connect_time() {
        return current_connect_time;
    }

    public void setCurrent_connect_time(String current_connect_time) {
        this.current_connect_time = current_connect_time;
    }

    public String getCurrent_id() {
        return current_id;
    }

    public void setCurrent_id(String current_id) {
        this.current_id = current_id;
    }

    public String getCurrent_setup_time() {
        return current_setup_time;
    }

    public void setCurrent_setup_time(String current_setup_time) {
        this.current_setup_time = current_setup_time;
    }

    public ArrayList<CurrentcallDTO> getCurrentcallList() {
        return currentcallList;
    }

    public void setCurrentcallList(ArrayList<CurrentcallDTO> currentcallList) {
        this.currentcallList = currentcallList;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getOrigin_client_name() {
        return origin_client_name;
    }

    public void setOrigin_client_name(String origin_client_name) {
        this.origin_client_name = origin_client_name;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_client_name() {
        return term_client_name;
    }

    public void setTerm_client_name(String term_client_name) {
        this.term_client_name = term_client_name;
    }

    public long getCurrent_duration() {
        return current_duration;
    }

    public void setCurrent_duration(long current_duration) {
        this.current_duration = current_duration;
    }

    public String getActive_duration() {
        return active_duration;
    }

    public void setActive_duration(String active_duration) {
        this.active_duration = active_duration;
    }
}
