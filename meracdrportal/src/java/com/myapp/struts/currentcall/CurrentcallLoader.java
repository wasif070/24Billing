package com.myapp.struts.currentcall;

import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Wasif
 */
public class CurrentcallLoader {

    static Logger logger = Logger.getLogger(CurrentcallLoader.class.getName());
    static CurrentcallLoader currencallLoader = null;
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();

    public CurrentcallLoader() {
    }

    public static CurrentcallLoader getInstance() {
        if (currencallLoader == null) {
            createCurrentcallLoader();
        }
        return currencallLoader;
    }

    private synchronized static void createCurrentcallLoader() {
        if (currencallLoader == null) {
            currencallLoader = new CurrentcallLoader();
        }
    }

    public synchronized ArrayList<CurrentcallDTO> getCurrentcallDTOList(long client_id) {
        ArrayList<CurrentcallDTO> currentcallList = new ArrayList<CurrentcallDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select id, dial_no_in, org_gateway_ip, dial_no_out, term_gateway_ip, setup_time, connect_time, status, org_client_id, term_client_id, (UNIX_TIMESTAMP()-inserted_time) as duration"
                    + " from current_call where status < 2 and inserted_time>=UNIX_TIMESTAMP()-3600 " + (client_id > 0 ? (" and (org_client_id=" + client_id + " or term_client_id=" + client_id + ")") : "")
                    + " order by id DESC";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CurrentcallDTO dto = new CurrentcallDTO();
                dto.setCurrent_id(resultSet.getString("id"));
                dto.setCurrent_dial_no_in(resultSet.getString("dial_no_in"));
                dto.setCurrent_org_gateway_ip(resultSet.getString("org_gateway_ip"));
                dto.setCurrent_dial_no_out(resultSet.getString("dial_no_out"));
                dto.setCurrent_term_gateway_ip(resultSet.getString("term_gateway_ip"));
                dto.setStatus(resultSet.getInt("status"));
                if (dto.getStatus() == 1) {
                    dto.setCurrent_setup_time(Utils.ToDateDDMMYYYYhhmmss(Utils.getUTCtimeLong(resultSet.getString("setup_time")) + Utils.getTimeLong(timeZone)));
                    dto.setCurrent_connect_time(Utils.ToDateDDMMYYYYhhmmss(Utils.getUTCtimeLong(resultSet.getString("connect_time")) + Utils.getTimeLong(timeZone)));
                    dto.setCurrent_duration(resultSet.getInt("duration"));
                    dto.setActive_duration(Utils.getFormatedDuration(resultSet.getLong("duration")));
                } else {
                    dto.setCurrent_setup_time(" ");
                    dto.setCurrent_connect_time(" ");
                    dto.setCurrent_duration(0);
                    dto.setActive_duration("0");
                }
                dto.setOrigin_client_id(resultSet.getLong("org_client_id"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                try {
                    dto.setOrigin_client_name(ClientLoader.getInstance().getClientDTOByID(dto.getOrigin_client_id()).getClient_id());
                } catch (Exception ex) {
                }
                try {
                    dto.setTerm_client_name(ClientLoader.getInstance().getClientDTOByID(dto.getTerm_client_id()).getClient_id());
                } catch (Exception ex) {
                }
                currentcallList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in CurrentcallLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return currentcallList;
    }
}