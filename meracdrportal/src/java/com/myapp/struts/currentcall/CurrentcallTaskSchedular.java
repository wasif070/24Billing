package com.myapp.struts.currentcall;

import java.util.ArrayList;

public class CurrentcallTaskSchedular {

    public CurrentcallTaskSchedular() {
    }

    public ArrayList<CurrentcallDTO> getCurrentcallDTOs(long client_id) {
        return CurrentcallLoader.getInstance().getCurrentcallDTOList(client_id);
    }
}
