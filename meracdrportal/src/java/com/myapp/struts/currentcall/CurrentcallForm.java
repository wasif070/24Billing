package com.myapp.struts.currentcall;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CurrentcallForm extends org.apache.struts.action.ActionForm {

    private String current_id;
    private String current_dial_no_in;
    private String current_dial_no_out;
    private String current_org_gateway_name;
    private String current_org_gateway_ip;
    private String current_term_gateway_name;
    private String current_term_gateway_ip;
    private String current_setup_time;
    private String current_connect_time;
    private ArrayList<CurrentcallDTO> currentcallList;
    private String message;
    private int doValidate;
    private long current_duration;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
    private int status;

    public String getCurrent_org_gateway_ip() {
        return current_org_gateway_ip;
    }

    public void setCurrent_org_gateway_ip(String current_org_gateway_ip) {
        this.current_org_gateway_ip = current_org_gateway_ip;
    }

    public String getCurrent_org_gateway_name() {
        return current_org_gateway_name;
    }

    public void setCurrent_org_gateway_name(String current_org_gateway_name) {
        this.current_org_gateway_name = current_org_gateway_name;
    }

    public String getCurrent_term_gateway_ip() {
        return current_term_gateway_ip;
    }

    public void setCurrent_term_gateway_ip(String current_term_gateway_ip) {
        this.current_term_gateway_ip = current_term_gateway_ip;
    }

    public String getCurrent_term_gateway_name() {
        return current_term_gateway_name;
    }

    public void setCurrent_term_gateway_name(String current_term_gateway_name) {
        this.current_term_gateway_name = current_term_gateway_name;
    }

    public String getCurrent_connect_time() {
        return current_connect_time;
    }

    public void setCurrent_connect_time(String current_connect_time) {
        this.current_connect_time = current_connect_time;
    }

    public String getCurrent_dial_no_in() {
        return current_dial_no_in;
    }

    public void setCurrent_dial_no_in(String current_dial_no_in) {
        this.current_dial_no_in = current_dial_no_in;
    }

    public String getCurrent_dial_no_out() {
        return current_dial_no_out;
    }

    public void setCurrent_dial_no_out(String current_dial_no_out) {
        this.current_dial_no_out = current_dial_no_out;
    }

    public String getCurrent_id() {
        return current_id;
    }

    public void setCurrent_id(String current_id) {
        this.current_id = current_id;
    }

    public String getCurrent_setup_time() {
        return current_setup_time;
    }

    public void setCurrent_setup_time(String current_setup_time) {
        this.current_setup_time = current_setup_time;
    }

    public ArrayList<CurrentcallDTO> getCurrentcallList() {
        return currentcallList;
    }

    public void setCurrentcallList(ArrayList<CurrentcallDTO> currentcallList) {
        this.currentcallList = currentcallList;
    }

    public long getCurrent_duration() {
        return current_duration;
    }

    public void setCurrent_duration(long current_duration) {
        this.current_duration = current_duration;
    }



    public CurrentcallForm() {
        super();
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getCurrent_id() == null) {
                errors.add("current_id", new ActionMessage("errors.id.required"));
            }

        }
        return errors;
    }
}
