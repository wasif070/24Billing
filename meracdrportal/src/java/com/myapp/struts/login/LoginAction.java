package com.myapp.struts.login;

import com.myapp.struts.user.UserLoader;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class LoginAction extends org.apache.struts.action.Action {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    static Logger logger = Logger.getLogger(LoginAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = SUCCESS;

        LoginForm formBean = (LoginForm) form;
        LoginDTO login_dto = null;
        UserDTO userDTO = UserLoader.getInstance().getUserDTOByUserIDPass(formBean.getLoginId(), formBean.getLoginPass());
        if (userDTO != null) {
            login_dto = new LoginDTO();
            login_dto.setId(userDTO.getId());
            login_dto.setClientId(userDTO.getUserId());
            login_dto.setClientPassword(userDTO.getUserPassword());
            login_dto.setClientStatus(userDTO.getUserStatus());
            if (login_dto.getId() >= 1) {
                login_dto.setSuperUser(userDTO.getSuperUser());
            }
            if (login_dto.getId() == -1) {
                login_dto.setSuperUser(userDTO.getSuperUser());
                login_dto.setUser(true);
            }
            login_dto.setClientType(userDTO.getClientType());
            login_dto.setClient_balance(userDTO.getClient_blance());
        }

        if (login_dto == null) {
            formBean.setMessage(true, "User ID or Password is not valid");
            target = FAILURE;
            request.setAttribute(mapping.getAttribute(), form);
        } else if (login_dto.getClientStatus() == Constants.USER_STATUS_BLOCK) {
            formBean.setMessage(true, "User is blocked.");
            target = FAILURE;
            request.setAttribute(mapping.getAttribute(), form);
        } else if (login_dto.getId() >= 1) {
            login_dto.setLoginTime(System.currentTimeMillis());
            request.getSession(true).setAttribute(Constants.LOGIN_DTO, login_dto);
            if (login_dto.getSuperUser()) {
                ActionForward changedActionForward = new ActionForward("../home/home.jsp", true);
                return changedActionForward;
            }
        } else if (login_dto.getId() == -1) {
            login_dto.setLoginTime(System.currentTimeMillis());
            request.getSession(true).setAttribute(Constants.LOGIN_DTO, login_dto);
            if (login_dto.isUser()) {
                ActionForward changedActionForward = new ActionForward("../home/home.jsp", true);
                return changedActionForward;
            }
        }

        return mapping.findForward(target);
    }
}
