package com.myapp.struts.dialplan;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class DialplanDAO {

    static Logger logger = Logger.getLogger(DialplanDAO.class.getName());

    public DialplanDAO() {
    }

    public MyAppError addDialplanInformation(DialplanDTO p_dto) {
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "select dialpeer_name from mvts_dialpeer where dialpeer_name=? and stat_enable=1";
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDialplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dialplan");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into mvts_dialpeer(dialpeer_name,description,enable,dnis_pattern,capacity,ani_translate,dnis_translate,priority,hunt_mode,gateway_list,stat_enable,hunt_stop,sched_type,sched_tod_on,sched_tod_off) ";
            sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDialplan_name());
            ps.setString(2, p_dto.getDialplan_description());
            ps.setInt(3, p_dto.getDialplan_enable());
            ps.setString(4, p_dto.getDialplan_dnis_pattern());
            ps.setInt(5, p_dto.getDialplan_capacity());
            ps.setString(6, p_dto.getDialplan_ani_translate());
            ps.setString(7, p_dto.getDialplan_dnis_translate());
            ps.setInt(8, p_dto.getDialplan_priority());
            ps.setInt(9, p_dto.getDialplan_hunt_mode());
            ps.setString(10, p_dto.getDialplan_gateway_list());
            ps.setInt(11, 1);
            ps.setInt(12, 0);
            ps.setInt(13, p_dto.getDialplan_sched_type());
            ps.setString(14, p_dto.getDialplan_sched_tod_on());
            ps.setString(15, p_dto.getDialplan_sched_tod_off());

            ps.executeUpdate();
            DialplanLoader.getInstance().forceReload();

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding dialplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError editDialplanInformation(DialplanDTO p_dto) {
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "select dialpeer_name from mvts_dialpeer where dialpeer_name = ? and dialpeer_id!=" + p_dto.getId() + " and enable=1";
            ps = dbConn.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getDialplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dialplan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update mvts_dialpeer set dialpeer_name=?,description=?,enable=?,dnis_pattern=?,capacity=?,ani_translate=?,dnis_translate=?,priority=?,hunt_mode=?,gateway_list=?,sched_type=?,sched_tod_on=?,sched_tod_off=?  where dialpeer_id=" + p_dto.getId();
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDialplan_name());
            ps.setString(2, p_dto.getDialplan_description());
            ps.setInt(3, p_dto.getDialplan_enable());
            ps.setString(4, p_dto.getDialplan_dnis_pattern().concat("[0-9]*"));
            ps.setInt(5, p_dto.getDialplan_capacity());
            ps.setString(6, p_dto.getDialplan_ani_translate());
            ps.setString(7, p_dto.getDialplan_dnis_translate());
            ps.setInt(8, p_dto.getDialplan_priority());
            ps.setInt(9, p_dto.getDialplan_hunt_mode());
            ps.setString(10, p_dto.getDialplan_gateway_list());
            ps.setInt(11, p_dto.getDialplan_sched_type());
            ps.setString(12, p_dto.getDialplan_sched_tod_on());
            ps.setString(13, p_dto.getDialplan_sched_tod_off());

            ps.executeUpdate();
            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing dialplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteDialplan(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "delete from mvts_dialpeer where dialpeer_id =" + cid;
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                DialplanLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while deleting dialpeer: ", ex);
        } finally {

            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError multipleDialplanStatusUpdate(long dialplanIds[], int dialplan_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        PreparedStatement ps = null;
        remotedbconnector.DBConnection dbConn = null;

        String selectedIdsString = Utils.implodeArray(dialplanIds, ",");
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_dialpeer set enable = " + dialplan_status + " where dialpeer_id in(" + selectedIdsString + ")";
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                DialplanLoader.getInstance().forceReload();
            }
        } catch (Exception ex) {
            logger.fatal("Error while multipleDialplanStatusUpdate: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDialplanDelete(long dialplanIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        PreparedStatement ps = null;
        remotedbconnector.DBConnection dbConn = null;

        String selectedIdsString = Utils.implodeArray(dialplanIds, ",");
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "delete from mvts_dialpeer where dialpeer_id in(" + selectedIdsString + ")";
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                DialplanLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while multipleDialplanDelete-> ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}