package com.myapp.struts.dialplan;

public class DialplanDTO {

    public boolean searchWithDialplanName = false;
    public boolean searchWithStatus = false;
    public boolean searchWithPrefix = false;
    public boolean searchWithGatewayName = false;
    private boolean superUser;
    private int id;
    private String dialplan_name;
    private String dialplan_description;
    private int dialplan_enable;
    private int dialplan_sched_type;
    private String dialplan_sched_tod_on;
    private String dialplan_sched_tod_off;
    private String dialplan_sched_type_name;

    public String getDialplan_sched_tod_off_name() {
        return dialplan_sched_tod_off_name;
    }

    public void setDialplan_sched_tod_off_name(String dialplan_sched_tod_off_name) {
        this.dialplan_sched_tod_off_name = dialplan_sched_tod_off_name;
    }

    public String getDialplan_sched_tod_on_name() {
        return dialplan_sched_tod_on_name;
    }

    public void setDialplan_sched_tod_on_name(String dialplan_sched_tod_on_name) {
        this.dialplan_sched_tod_on_name = dialplan_sched_tod_on_name;
    }

    public String getDialplan_sched_type_name() {
        return dialplan_sched_type_name;
    }

    public void setDialplan_sched_type_name(String dialplan_sched_type_name) {
        this.dialplan_sched_type_name = dialplan_sched_type_name;
    }
    private String dialplan_sched_tod_on_name;
    private String dialplan_sched_tod_off_name;
    private int onhour;
    private int onmin;
    private int offhour;
    private int offmin;
    private String dialplan_enable_name;
    private String dialplan_gateway_list;
    private int[] dialplan_gateway_list_array;
    private String dialplan_gateway_list_name;
    private String dialplan_dnis_pattern;
    private String dialplan_ani_translate;
    private String dialplan_dnis_translate;
    private int dialplan_hunt_mode;
    private String dialplan_hunt_mode_name;
    private int dialplan_stat_enable;
    private int dialplan_capacity;
    private int dialplan_priority;
    private long[] selectedIDs;
    private int[] gateway_id;
    private String[] gateway_name;
    private int gateway_id_single;
    private String gateway_name_single;

    public DialplanDTO() {
        superUser = false;
        dialplan_enable = -1;
    }

    public String getDialplan_ani_translate() {
        return dialplan_ani_translate;
    }

    public void setDialplan_ani_translate(String dialplan_ani_translate) {
        this.dialplan_ani_translate = dialplan_ani_translate;
    }

    public String getDialplan_description() {
        return dialplan_description;
    }

    public void setDialplan_description(String dialplan_description) {
        this.dialplan_description = dialplan_description;
    }

    public String getDialplan_dnis_pattern() {
        return dialplan_dnis_pattern;
    }

    public void setDialplan_dnis_pattern(String dialplan_dnis_pattern) {
        this.dialplan_dnis_pattern = dialplan_dnis_pattern;
    }

    public String getDialplan_dnis_translate() {
        return dialplan_dnis_translate;
    }

    public void setDialplan_dnis_translate(String dialplan_dnis_translate) {
        this.dialplan_dnis_translate = dialplan_dnis_translate;
    }

    public int getDialplan_enable() {
        return dialplan_enable;
    }

    public void setDialplan_enable(int dialplan_enable) {
        this.dialplan_enable = dialplan_enable;
    }

    public int getDialplan_sched_type() {
        return dialplan_sched_type;
    }

    public void setDialplan_sched_type(int dialplan_sched_type) {
        this.dialplan_sched_type = dialplan_sched_type;
    }

    public String getDialplan_sched_tod_off() {
        return dialplan_sched_tod_off;
    }

    public void setDialplan_sched_tod_off(String dialplan_sched_tod_off) {
        this.dialplan_sched_tod_off = dialplan_sched_tod_off;
    }

    public String getDialplan_sched_tod_on() {
        return dialplan_sched_tod_on;
    }

    public void setDialplan_sched_tod_on(String dialplan_sched_tod_on) {
        this.dialplan_sched_tod_on = dialplan_sched_tod_on;
    }

    public int getOffhour() {
        return offhour;
    }

    public void setOffhour(int offhour) {
        this.offhour = offhour;
    }

    public int getOffmin() {
        return offmin;
    }

    public void setOffmin(int offmin) {
        this.offmin = offmin;
    }

    public int getOnhour() {
        return onhour;
    }

    public void setOnhour(int onhour) {
        this.onhour = onhour;
    }

    public int getOnmin() {
        return onmin;
    }

    public void setOnmin(int onmin) {
        this.onmin = onmin;
    }

    public String getDialplan_enable_name() {
        return dialplan_enable_name;
    }

    public void setDialplan_enable_name(String dialplan_enable_name) {
        this.dialplan_enable_name = dialplan_enable_name;
    }

    public String getDialplan_gateway_list() {
        return dialplan_gateway_list;
    }

    public void setDialplan_gateway_list(String dialplan_gateway_list) {
        this.dialplan_gateway_list = dialplan_gateway_list;
    }

    public int getDialplan_hunt_mode() {
        return dialplan_hunt_mode;
    }

    public void setDialplan_hunt_mode(int dialplan_hunt_mode) {
        this.dialplan_hunt_mode = dialplan_hunt_mode;
    }

    public String getDialplan_hunt_mode_name() {
        return dialplan_hunt_mode_name;
    }

    public void setDialplan_hunt_mode_name(String dialplan_hunt_mode_name) {
        this.dialplan_hunt_mode_name = dialplan_hunt_mode_name;
    }

    public String getDialplan_name() {
        return dialplan_name;
    }

    public void setDialplan_name(String dialplan_name) {
        this.dialplan_name = dialplan_name;
    }

    public int getDialplan_stat_enable() {
        return dialplan_stat_enable;
    }

    public void setDialplan_stat_enable(int dialplan_stat_enable) {
        this.dialplan_stat_enable = dialplan_stat_enable;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSearchWithDialplanName() {
        return searchWithDialplanName;
    }

    public void setSearchWithDialplanName(boolean searchWithDialplanName) {
        this.searchWithDialplanName = searchWithDialplanName;
    }

    public boolean isSearchWithStatus() {
        return searchWithStatus;
    }

    public void setSearchWithStatus(boolean searchWithStatus) {
        this.searchWithStatus = searchWithStatus;
    }

    public boolean isSearchWithGatewayName() {
        return searchWithGatewayName;
    }

    public void setSearchWithGatewayName(boolean searchWithGatewayName) {
        this.searchWithGatewayName = searchWithGatewayName;
    }

    public boolean isSearchWithPrefix() {
        return searchWithPrefix;
    }

    public void setSearchWithPrefix(boolean searchWithPrefix) {
        this.searchWithPrefix = searchWithPrefix;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public boolean isSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public int getDialplan_capacity() {
        return dialplan_capacity;
    }

    public void setDialplan_capacity(int dialplan_capacity) {
        this.dialplan_capacity = dialplan_capacity;
    }

    public int getDialplan_priority() {
        return dialplan_priority;
    }

    public void setDialplan_priority(int dialplan_priority) {
        this.dialplan_priority = dialplan_priority;
    }

    public int[] getDialplan_gateway_list_array() {
        return dialplan_gateway_list_array;
    }

    public void setDialplan_gateway_list_array(int[] dialplan_gateway_list_array) {
        this.dialplan_gateway_list_array = dialplan_gateway_list_array;
    }

    public String getDialplan_gateway_list_name() {
        return dialplan_gateway_list_name;
    }

    public void setDialplan_gateway_list_name(String dialplan_gateway_list_name) {
        this.dialplan_gateway_list_name = dialplan_gateway_list_name;
    }

    public int[] getGateway_id() {
        return gateway_id;
    }

    public void setGateway_id(int[] gateway_ids) {
        this.gateway_id = gateway_id;
    }

    public String[] getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String[] gateway_name) {
        this.gateway_name = gateway_name;
    }

    public int getGateway_id_single() {
        return gateway_id_single;
    }

    public void setGateway_id_single(int gateway_id_single) {
        this.gateway_id_single = gateway_id_single;
    }

    public String getGateway_name_single() {
        return gateway_name_single;
    }

    public void setGateway_name_single(String gateway_name_single) {
        this.gateway_name_single = gateway_name_single;
    }
}
