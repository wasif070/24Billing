package com.myapp.struts.dialplan;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class DialplanTaskSchedular {

    public DialplanTaskSchedular() {
    }

    public MyAppError addDialplanInformation(DialplanDTO p_dto) {
        DialplanDAO userDAO = new DialplanDAO();
        return userDAO.addDialplanInformation(p_dto);
    }

    public MyAppError editDialplanInformation(DialplanDTO p_dto) {
        DialplanDAO userDAO = new DialplanDAO();
        return userDAO.editDialplanInformation(p_dto);
    }

    public MyAppError deleteDialplan(int cid) {
        DialplanDAO dao = new DialplanDAO();
        return dao.deleteDialplan(cid);
    }

    public DialplanDTO getDialplanDTO(int id) {
        return DialplanLoader.getInstance().getDialplanDTOByID(id);
    }

    public ArrayList<DialplanDTO> getDialplanDTOsSorted(LoginDTO l_dto) {
        return DialplanLoader.getInstance().getDialplanDTOsSorted();
    }

    public ArrayList<DialplanDTO> getDialplanDTOs(LoginDTO l_dto) {
        return DialplanLoader.getInstance().getDialplanDTOList();
    }

    public ArrayList<DialplanDTO> getDialplanDTOsWithSearchParam(DialplanDTO udto, LoginDTO l_dto) {
        return DialplanLoader.getInstance().getDialplanDTOsWithSearchParam(udto);
    }

    public MyAppError activateMultiple(long dialplanIds[]) {
        DialplanDAO dao = new DialplanDAO();
        return dao.multipleDialplanStatusUpdate(dialplanIds, 1);
    }

    public MyAppError inactivateMultiple(long dialplanIds[]) {
        DialplanDAO dao = new DialplanDAO();
        return dao.multipleDialplanStatusUpdate(dialplanIds, 0);
    }

    public MyAppError deleteMultiple(long dialplanIds[]) {
        DialplanDAO dao = new DialplanDAO();
        return dao.multipleDialplanDelete(dialplanIds);
    }
}
