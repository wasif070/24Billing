package com.myapp.struts.dialplan;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class GetDialplanAction extends Action {

    static Logger logger = Logger.getLogger(GetDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        int id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            DialplanForm formBean = (DialplanForm) form;
            DialplanDTO dto = new DialplanDTO();
            DialplanTaskSchedular scheduler = new DialplanTaskSchedular();
            dto = scheduler.getDialplanDTO(id);
            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setDialplan_name(dto.getDialplan_name());
                formBean.setDialplan_gateway_list(dto.getDialplan_gateway_list());
                formBean.setGateway_id(Utils.explodeArray(dto.getDialplan_gateway_list()));
                //logger.debug("Gateway ids-->"+formBean.getGateway_id()[0]+","+formBean.getGateway_id()[1]);
                formBean.setDialplan_description(dto.getDialplan_description());
                formBean.setDialplan_dnis_pattern(dto.getDialplan_dnis_pattern());
                formBean.setDialplan_ani_translate(dto.getDialplan_ani_translate());
                formBean.setDialplan_dnis_translate(dto.getDialplan_dnis_translate());
                formBean.setDialplan_hunt_mode(dto.getDialplan_hunt_mode());
                formBean.setDialplan_priority(dto.getDialplan_priority());
                formBean.setDialplan_capacity(dto.getDialplan_capacity());
                formBean.setDialplan_enable(dto.getDialplan_enable());
                formBean.setDialplan_sched_type(dto.getDialplan_sched_type());
                if (dto.getDialplan_sched_tod_on() == null) {
                    formBean.setOnhour(0);
                    formBean.setOnmin(0);
                } else {
                    formBean.setDialplan_sched_tod_on(dto.getDialplan_sched_tod_on());
                    formBean.setOnhour(Integer.parseInt(dto.getDialplan_sched_tod_on().substring(0, 2)));
                    formBean.setOnmin(Integer.parseInt(dto.getDialplan_sched_tod_on().substring(2)));
                }
                if (dto.getDialplan_sched_tod_off() == null) {
                    formBean.setOffhour(0);
                    formBean.setOffmin(0);
                } else {
                    formBean.setDialplan_sched_tod_off(dto.getDialplan_sched_tod_off());
                    formBean.setOffhour(Integer.parseInt(dto.getDialplan_sched_tod_off().substring(0, 2)));
                    formBean.setOffmin(Integer.parseInt(dto.getDialplan_sched_tod_off().substring(2)));
                }


            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
