package com.myapp.struts.dialplan;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class DialplanForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int id;
    private String dialplan_name;
    private String dialplan_description;
    private int dialplan_enable;
    private int dialplan_sched_type;
    private String dialplan_sched_tod_on;
    private String dialplan_sched_tod_off;
    private int onhour;
    private int onmin;
    private int offhour;
    private int offmin;
    private String dialplan_enable_name;
    private String dialplan_gateway_list;
    private Integer[] dialplan_gateway_list_array;
    private String dialplan_gateway_list_name;
    private String dialplan_dnis_pattern;
    private String dialplan_ani_translate;
    private String dialplan_dnis_translate;
    private int dialplan_hunt_mode;
    private String dialplan_hunt_mode_name;
    private int dialplan_stat_enable;
    private int dialplan_capacity;
    private int dialplan_priority;
    private int[] gateway_id;
    private String[] gateway_name;
    private String message;
    private long[] selectedIDs;
    private ArrayList dialplanList;
    private ArrayList gatewayList;
    private String activateBtn;
    private String inactiveBtn;
    private String blockBtn;
    private String deleteBtn;

    public DialplanForm() {
        super();
        dialplan_hunt_mode = -1;
        dialplan_enable = -1;

    }

    public String getDialplan_ani_translate() {
        return dialplan_ani_translate;
    }

    public void setDialplan_ani_translate(String dialplan_ani_translate) {
        this.dialplan_ani_translate = dialplan_ani_translate;
    }

    public int getDialplan_capacity() {
        return dialplan_capacity;
    }

    public void setDialplan_capacity(int dialplan_capacity) {
        this.dialplan_capacity = dialplan_capacity;
    }

    public String getDialplan_description() {
        return dialplan_description;
    }

    public void setDialplan_description(String dialplan_description) {
        this.dialplan_description = dialplan_description;
    }

    public String getDialplan_dnis_pattern() {
        return dialplan_dnis_pattern;
    }

    public void setDialplan_dnis_pattern(String dialplan_dnis_pattern) {
        this.dialplan_dnis_pattern = dialplan_dnis_pattern;
    }

    public String getDialplan_dnis_translate() {
        return dialplan_dnis_translate;
    }

    public void setDialplan_dnis_translate(String dialplan_dnis_translate) {
        this.dialplan_dnis_translate = dialplan_dnis_translate;
    }

    public int getDialplan_enable() {
        return dialplan_enable;
    }

    public void setDialplan_enable(int dialplan_enable) {
        this.dialplan_enable = dialplan_enable;
    }

    public int getDialplan_sched_type() {
        return dialplan_sched_type;
    }

    public void setDialplan_sched_type(int dialplan_sched_type) {
        this.dialplan_sched_type = dialplan_sched_type;
    }

    public String getDialplan_sched_tod_off() {
        return dialplan_sched_tod_off;
    }

    public void setDialplan_sched_tod_off(String dialplan_sched_tod_off) {
        this.dialplan_sched_tod_off = dialplan_sched_tod_off;
    }

    public String getDialplan_sched_tod_on() {
        return dialplan_sched_tod_on;
    }

    public void setDialplan_sched_tod_on(String dialplan_sched_tod_on) {
        this.dialplan_sched_tod_on = dialplan_sched_tod_on;
    }

    public int getOffhour() {
        return offhour;
    }

    public void setOffhour(int offhour) {
        this.offhour = offhour;
    }

    public int getOffmin() {
        return offmin;
    }

    public void setOffmin(int offmin) {
        this.offmin = offmin;
    }

    public int getOnhour() {
        return onhour;
    }

    public void setOnhour(int onhour) {
        this.onhour = onhour;
    }

    public int getOnmin() {
        return onmin;
    }

    public void setOnmin(int onmin) {
        this.onmin = onmin;
    }

    public String getDialplan_enable_name() {
        return dialplan_enable_name;
    }

    public void setDialplan_enable_name(String dialplan_enable_name) {
        this.dialplan_enable_name = dialplan_enable_name;
    }

    public String getDialplan_gateway_list() {
        return dialplan_gateway_list;
    }

    public void setDialplan_gateway_list(String dialplan_gateway_list) {
        this.dialplan_gateway_list = dialplan_gateway_list;
    }

    public Integer[] getDialplan_gateway_list_array() {
        return dialplan_gateway_list_array;
    }

    public void setDialplan_gateway_list_array(Integer[] dialplan_gateway_list_array) {
        this.dialplan_gateway_list_array = dialplan_gateway_list_array;
    }

    public String getDialplan_gateway_list_name() {
        return dialplan_gateway_list_name;
    }

    public void setDialplan_gateway_list_name(String dialplan_gateway_list_name) {
        this.dialplan_gateway_list_name = dialplan_gateway_list_name;
    }

    public int getDialplan_hunt_mode() {
        return dialplan_hunt_mode;
    }

    public void setDialplan_hunt_mode(int dialplan_hunt_mode) {
        this.dialplan_hunt_mode = dialplan_hunt_mode;
    }

    public String getDialplan_hunt_mode_name() {
        return dialplan_hunt_mode_name;
    }

    public void setDialplan_hunt_mode_name(String dialplan_hunt_mode_name) {
        this.dialplan_hunt_mode_name = dialplan_hunt_mode_name;
    }

    public String getDialplan_name() {
        return dialplan_name;
    }

    public void setDialplan_name(String dialplan_name) {
        this.dialplan_name = dialplan_name;
    }

    public int getDialplan_priority() {
        return dialplan_priority;
    }

    public void setDialplan_priority(int dialplan_priority) {
        this.dialplan_priority = dialplan_priority;
    }

    public int getDialplan_stat_enable() {
        return dialplan_stat_enable;
    }

    public void setDialplan_stat_enable(int dialplan_stat_enable) {
        this.dialplan_stat_enable = dialplan_stat_enable;
    }

    public ArrayList getDialplanList() {
        return dialplanList;
    }

    public void setDialplanList(ArrayList dialplanList) {
        this.dialplanList = dialplanList;
    }

    public int[] getGateway_id() {
        return gateway_id;
    }

    public void setGateway_id(int[] gateway_id) {
        this.gateway_id = gateway_id;
    }

    public String[] getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String[] gateway_name) {
        this.gateway_name = gateway_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList getGatewayList() {
        return gatewayList;
    }

    public void setGatewayList(ArrayList gatewayList) {
        this.gatewayList = gatewayList;
    }

    public String getActivateBtn() {
        return activateBtn;
    }

    public void setActivateBtn(String activateBtn) {
        this.activateBtn = activateBtn;
    }

    public String getBlockBtn() {
        return blockBtn;
    }

    public void setBlockBtn(String blockBtn) {
        this.blockBtn = blockBtn;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getInactiveBtn() {
        return inactiveBtn;
    }

    public void setInactiveBtn(String inactiveBtn) {
        this.inactiveBtn = inactiveBtn;
    }

    public String getMessage() {
        return message;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getDialplan_name() == null || getDialplan_name().length() < 1) {
                errors.add("dialplan_name", new ActionMessage("errors.dialplan_name.required"));
            }
            if (getDialplan_dnis_pattern() == null || getDialplan_dnis_pattern().length() < 1) {
                errors.add("dialplan_dnis_pattern", new ActionMessage("errors.dialplan_dnis_pattern.required"));
            }

            if (getGateway_id() == null) {
                errors.add("gateway_id", new ActionMessage("errors.gateway_id.required"));
            }

            if (request.getParameter("dialplan_hunt_mode").length() < 1) {
                errors.add("dialplan_hunt_mode", new ActionMessage("errors.dialplan_hunt_mode.required"));
            }

            if (request.getParameter("dialplan_enable").length() < 1) {
                errors.add("dialplan_enable", new ActionMessage("errors.dialplan_enable.required"));
            }

        }
        return errors;
    }
}
