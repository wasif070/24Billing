package com.myapp.struts.dialplan;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class ListDialplanAction extends Action {

    static Logger logger = Logger.getLogger(ListDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            DialplanTaskSchedular scheduler = new DialplanTaskSchedular();
            DialplanForm dialplanForm = (DialplanForm) form;

            if (list_all == 0) {
                if (dialplanForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, dialplanForm.getRecordPerPage());
                }
                DialplanDTO udto = new DialplanDTO();
                if (dialplanForm.getDialplan_name() != null && dialplanForm.getDialplan_name().trim().length() > 0) {
                    udto.searchWithDialplanName = true;
                    udto.setDialplan_name(dialplanForm.getDialplan_name());
                }
                if (dialplanForm.getDialplan_gateway_list_name() != null && dialplanForm.getDialplan_gateway_list_name().trim().length() > 0) {
                    udto.searchWithGatewayName = true;
                    udto.setDialplan_gateway_list_name(dialplanForm.getDialplan_gateway_list_name());
                }
                if (dialplanForm.getDialplan_dnis_pattern() != null && dialplanForm.getDialplan_dnis_pattern().trim().length() > 0) {
                    udto.searchWithPrefix = true;
                    udto.setDialplan_dnis_pattern(dialplanForm.getDialplan_dnis_pattern());
                }
                if (dialplanForm.getDialplan_enable() >= 0) {
                    udto.searchWithStatus = true;
                    udto.setDialplan_enable(dialplanForm.getDialplan_enable());
                }
                dialplanForm.setDialplanList(scheduler.getDialplanDTOsWithSearchParam(udto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                    dialplanForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString()));
                }
                dialplanForm.setDialplanList(scheduler.getDialplanDTOs(login_dto));
            }
            dialplanForm.setSelectedIDs(null);
            if (dialplanForm.getDialplanList() != null && dialplanForm.getDialplanList().size() <= (dialplanForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
