package com.myapp.struts.dialplan;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class DeleteDialplanAction extends Action {

    static Logger logger = Logger.getLogger(DeleteDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int cid = Integer.parseInt(request.getParameter("id"));
        DialplanTaskSchedular rt = new DialplanTaskSchedular();

        MyAppError error = new MyAppError();
        error = rt.deleteDialplan(cid);
        request.getSession(true).setAttribute(Constants.MESSAGE, "Dialplan Deleted Successfully!");
        return mapping.findForward("success");
    }
}