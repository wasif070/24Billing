/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class ListCDRQualityAction extends Action {

    static Logger logger = Logger.getLogger(ListCDRQualityAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            int pageNo = 1;
            String links = "";

            ReportTaskScheduler scheduler = new ReportTaskScheduler();
            CDRReportForm mvtsCdrForm = (CDRReportForm) form;
            CDRReportDTO sdto = new CDRReportDTO();
            if (mvtsCdrForm.getDoSearch() != null) {
                if (mvtsCdrForm.getOrigin_client_id() > 0) {
                    sdto.setOrigin_client_id(mvtsCdrForm.getOrigin_client_id());
                    links += "&oid=" + sdto.getOrigin_client_id();
                }

                if (mvtsCdrForm.getTerm_client_id() > 0) {
                    sdto.setTerm_client_id(mvtsCdrForm.getTerm_client_id());
                    links += "&tid=" + sdto.getTerm_client_id();
                }
                if (mvtsCdrForm.getOrigin_ip() != null && mvtsCdrForm.getOrigin_ip().length() > 0) {
                    sdto.setOrigin_ip(mvtsCdrForm.getOrigin_ip());
                }

                if (mvtsCdrForm.getOrigin_destination() != null && mvtsCdrForm.getOrigin_destination().length() > 0) {
                    sdto.setOrigin_destination(mvtsCdrForm.getOrigin_destination());
                }

                if (mvtsCdrForm.getTerm_ip() != null && mvtsCdrForm.getTerm_ip().length() > 0) {
                    sdto.setTerm_ip(mvtsCdrForm.getTerm_ip());
                }

                if (mvtsCdrForm.getSummaryType() > -1) {
                    sdto.setSummaryType(mvtsCdrForm.getSummaryType());
                }

                if (mvtsCdrForm.getSummaryBy() != null && mvtsCdrForm.getSummaryBy().length > 0) {
                    sdto.setSummaryBy(mvtsCdrForm.getSummaryBy());
                    String sby = "";
                    if (sdto.getSummaryBy() != null) {
                        for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                            sby += sdto.getSummaryBy()[j] + ",";
                        }
                        links += "&sby=" + sby;
                    }
                }

                String fromDate = mvtsCdrForm.getFromYear() + "-" + formatter.format(mvtsCdrForm.getFromMonth()) + "-" + formatter.format(mvtsCdrForm.getFromDay()) + " " + formatter.format(mvtsCdrForm.getFromHour()) + ":" + formatter.format(mvtsCdrForm.getFromMin()) + ":00";
                sdto.setFromDate(fromDate);
                links += "&fdate=" + fromDate;

                String toDate = mvtsCdrForm.getToYear() + "-" + formatter.format(mvtsCdrForm.getToMonth()) + "-" + formatter.format(mvtsCdrForm.getToDay()) + " " + formatter.format(mvtsCdrForm.getToHour()) + ":" + formatter.format(mvtsCdrForm.getToMin()) + ":00";
                sdto.setToDate(toDate);
                links += "&tdate=" + toDate;

                request.getSession(true).setAttribute(Constants.MESSAGE, null);

                request.getSession(true).setAttribute("SearchCdrDTO", sdto);
            }
            mvtsCdrForm.setMvtsQualityList(scheduler.getCdrQualityDTOs(sdto, login_dto));
            request.getSession(true).setAttribute(mapping.getAttribute(), mvtsCdrForm);

            if (mvtsCdrForm.getMvtsQualityList() != null && mvtsCdrForm.getMvtsQualityList().size() > 0) {
                request.getSession(true).setAttribute("CDRReportDTO", mvtsCdrForm.getMvtsQualityList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("CDRReportDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}