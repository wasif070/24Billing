/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class ReportTaskScheduler {

    public ReportTaskScheduler() {
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOs(LoginDTO l_dto, int start, int end) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        CDRReportDTO stdo = new CDRReportDTO();
        if (l_dto.getSuperUser()) {
            return reportDAO.getMvtsCdrDTOList(start, end);
        } else {
            return reportDAO.getMvtsCdrDTOByClient(stdo, l_dto.getId(), start, end);
        }
    }

    public ArrayList<CDRReportDTO> getCdrDTOsWithSearchParam(CDRReportDTO sdto, LoginDTO l_dto, int start, int end) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        if (l_dto.getSuperUser()) {
            return reportDAO.getMvtsCdrDTOListWithSearch(sdto, start, end);
        } else {
            return reportDAO.getMvtsCdrDTOByClient(sdto, l_dto.getId(), start, end);
        }
    }

    public ArrayList<CDRQualityDTO> getCdrQualityDTOs(CDRReportDTO sdto, LoginDTO l_dto) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        if (l_dto.getSuperUser()) {
            return reportDAO.getCdrQualityDTOs(sdto);
        } else {
            return reportDAO.getClientCdrQualityDTOs(l_dto, sdto);
        }
    }
}
