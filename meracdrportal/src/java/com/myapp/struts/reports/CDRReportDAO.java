/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.util.Utils;
import com.myapp.struts.settings.SettingsLoader;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class CDRReportDAO {

    static Logger logger = Logger.getLogger(CDRReportDAO.class.getName());
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();
    String curdate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));
    String now = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));

    public CDRReportDAO() {
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOList(int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;

        String condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now+"' ";
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from flamma_cdr where " + condition + " limit " + start + "," + end;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                mvtsCdrList.add(dto);
            }
            sql = "select SUM(duration),SUM(origin_bill_amount),SUM(term_bill_amount) from flamma_cdr where " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("SUM(duration)"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("SUM(origin_bill_amount)"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("SUM(term_bill_amount)"));
                mvtsCdrList.add(dto);

            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOListWithSearch(CDRReportDTO sdto, int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "duration >0";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = "duration <= 0";
            }

            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }

            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip = '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }

            if (sdto.getFromDate() != null) {
                condition += " and  connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate+"' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and  connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + now+"' ";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now+"' ";
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from flamma_cdr where " + condition + " limit " + start + "," + end;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                mvtsCdrList.add(dto);
            }

            sql = "select SUM(duration),SUM(origin_bill_amount),SUM(term_bill_amount) from flamma_cdr where " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("SUM(duration)"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("SUM(origin_bill_amount)"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("SUM(term_bill_amount)"));
                mvtsCdrList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOListWithSearch:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public int getTotalMvtsCDR(CDRReportDTO sdto) {

        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;

        String condition = "";
        condition = "duration >0";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = "duration <= 0";
            }

            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }

            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }
            if (sdto.getFromDate() != null) {
                condition += " and connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate+"' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + now+"' ";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now+"' ";
        }
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from flamma_cdr where " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getTotalMvtsCDR:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOByClient(CDRReportDTO sdto, long client_id, int start, int end) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition += "(origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition += " and duration <= 0";
            } else {
                condition += " and duration > 0";
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }
            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }
            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }
            if (sdto.getFromDate() != null) {
                condition += " and connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate + "' ";
            }
            if (sdto.getToDate() != null) {
                condition += " and connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + now + "' ";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now + "' ";
            condition += " and (origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        }

        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from flamma_cdr where " + condition + " limit " + start + "," + end;
            logger.debug("client sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOByClient:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public int getClientTotalMvtsCDR(CDRReportDTO sdto, long client_id) {

        int totalRecord = 0;
        DBConnection dbConnection = null;
        Statement statement = null;

        String condition = "";
        condition += "(origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition += " and duration <= 0";
            } else {
                condition += " and duration > 0";
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }


            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }

            if (sdto.getFromDate() != null) {
                condition += " and connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate + "' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + now + "' ";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now + "' ";
            condition += " and (origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        }

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(cdr_id) as total_record from flamma_cdr where " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRecord = resultSet.getInt("total_record");
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getClientTotalMvtsCDR:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return totalRecord;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOList(CDRReportDTO sdto) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "1";
        if (sdto != null) {
            if (sdto.getFromDate() != null) {
                condition = " connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition = " connection_time >= '" + curdate+"' ";
            }
            if (sdto.getToDate() != null) {
                condition += " and connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time <= '" + now+"' ";
            }
            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }
            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }
            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }
            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now+"' ";
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from flamma_cdr where " + condition;
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOList(long client_id, CDRReportDTO sdto) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "(origin_client_id =" + client_id + " or term_client_id=" + client_id + ")";
        if (sdto != null) {
            if (sdto.getFromDate() != null) {
                condition += " and connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate+"' ";
            }
            if (sdto.getToDate() != null) {
                condition += " and connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time <= '" + now+"' ";
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }
            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }
            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }
        } else {
            condition = "duration >0 and connection_time >= '" + curdate + "' and connection_time <= '" + now+"' ";
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from flamma_cdr where " + condition;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getLong("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<CDRQualityDTO> getCdrQualityDTOs(CDRReportDTO sdto) {
        ArrayList<CDRQualityDTO> qualityCdrList = new ArrayList<CDRQualityDTO>();
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        mvtsCdrList = getMvtsCdrDTOList(sdto);

        boolean searchOriginIp = false;
        boolean searchOriginDest = false;
        boolean searchTermIp = false;
        boolean searchTermDest = false;

        if (sdto.getSummaryBy() != null) {
            for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                switch (sdto.getSummaryBy()[j]) {
                    case 0:
                        searchOriginIp = true;
                        break;
                    case 1:
                        searchOriginDest = true;
                    case 2:
                        searchTermIp = true;
                        break;
                    case 3:
                        searchTermDest = true;
                        break;
                }
            }
        }
        long inc_time = 0;
        if (sdto != null && sdto.getSummaryType() > 0) {
            switch (sdto.getSummaryType()) {
                case 1:
                    inc_time = 900000;
                    break;
                case 2:
                    inc_time = 3600000;
                    break;
                case 3:
                    inc_time = 86400000;
                    break;
                case 4:
                    inc_time = 604800000;
                    break;
            }
        }

        boolean isMatch = false;
        if (mvtsCdrList != null && mvtsCdrList.size() > 0) {
            for (int inc = 0; inc < mvtsCdrList.size(); inc++) {
                CDRReportDTO mcdr = new CDRReportDTO();
                mcdr = mvtsCdrList.get(inc);
                CDRQualityDTO temp = new CDRQualityDTO();
                temp.setOrigin_client_id(mcdr.getOrigin_client_id());
                temp.setOrigin_client_name(mcdr.getOrigin_client_name());
                temp.setOrigin_destination(mcdr.getOrigin_destination());
                temp.setOrigin_bill_amount(mcdr.getOrigin_bill_amount());
                temp.setOrigin_ip(mcdr.getOrigin_ip());
                temp.setTerm_client_id(mcdr.getTerm_client_id());
                temp.setTerm_client_name(mcdr.getTerm_client_name());
                temp.setTerm_ip(mcdr.getTerm_ip());
                temp.setTerm_destination(mcdr.getTerm_destination());
                temp.setTerm_bill_amount(mcdr.getTerm_bill_amount());
                temp.setDuration(mcdr.getDuration());
                temp.setTotal_fail(0);
                temp.setTotal_success(0);
                temp.setPeriod_start(Utils.getDateLong(mcdr.getConnection_time()));
                temp.setPeriod_end(Utils.getDateLong(mcdr.getConnection_time()));

                double pdd = mcdr.getPdd();
                temp.setAvg_pdd(pdd);

                if (qualityCdrList == null) {
                    qualityCdrList = new ArrayList<CDRQualityDTO>();
                }

                if (qualityCdrList != null && qualityCdrList.size() > 0) {
                    for (int j = 0; j < qualityCdrList.size(); j++) {
                        CDRQualityDTO d = new CDRQualityDTO();
                        d = qualityCdrList.get(j);
                        if ((searchOriginIp && !d.getOrigin_ip().equals(temp.getOrigin_ip()))
                                || (searchOriginDest && !d.getOrigin_destination().equals(temp.getOrigin_destination()))
                                || (searchTermIp && !d.getTerm_ip().equals(temp.getTerm_ip()))
                                || (searchTermDest && !d.getTerm_destination().equals(temp.getTerm_destination()))) {
                            isMatch = false;
                            continue;
                        } else {
                            if (sdto.getSummaryType() > 0) {
                                if (temp.getPeriod_start() >= d.getPeriod_start()
                                        && temp.getPeriod_start() <= d.getPeriod_end()) {
                                    isMatch = true;
                                    if (temp.getDuration() > 0) {
                                        temp.setTotal_success(1);
                                        temp.setTotal_fail(0);
                                    } else {
                                        temp.setTotal_success(0);
                                        temp.setTotal_fail(1);
                                    }

                                    long dur = d.getDuration() + temp.getDuration();
                                    d.setDuration(dur);
                                    d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                    int suc = d.getTotal_success() + temp.getTotal_success();
                                    d.setTotal_success(suc);
                                    d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                    d.setOrigin_bill_amount(d.getOrigin_bill_amount() + temp.getOrigin_bill_amount());
                                    d.setTerm_bill_amount(d.getTerm_bill_amount() + temp.getTerm_bill_amount());
                                    if (searchOriginIp) {
                                        d.setOrigin_ip(temp.getOrigin_ip());
                                    } else {
                                        d.setOrigin_ip("");
                                    }

                                    if (searchOriginDest) {
                                        d.setOrigin_destination(temp.getOrigin_destination());
                                    } else {
                                        d.setOrigin_destination("");
                                    }

                                    if (searchTermIp) {
                                        d.setTerm_ip(temp.getTerm_ip());
                                    } else {
                                        d.setTerm_ip("");
                                    }

                                    if (searchTermDest) {
                                        d.setTerm_destination(temp.getTerm_destination());
                                    } else {
                                        d.setTerm_destination("");
                                    }

                                    break;
                                } else {
                                    isMatch = false;
                                    continue;
                                }
                            } else {
                                isMatch = true;
                                if (temp.getDuration() > 0) {
                                    temp.setTotal_success(1);
                                    temp.setTotal_fail(0);
                                } else {
                                    temp.setTotal_success(0);
                                    temp.setTotal_fail(1);
                                }

                                long dur = d.getDuration() + temp.getDuration();
                                d.setDuration(dur);
                                d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                int suc = d.getTotal_success() + temp.getTotal_success();
                                d.setTotal_success(suc);
                                d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                d.setPeriod_end(temp.getPeriod_start());
                                d.setOrigin_bill_amount(d.getOrigin_bill_amount() + temp.getOrigin_bill_amount());
                                d.setTerm_bill_amount(d.getTerm_bill_amount() + temp.getTerm_bill_amount());
                                if (searchOriginIp) {
                                    d.setOrigin_ip(temp.getOrigin_ip());
                                } else {
                                    d.setOrigin_ip("");
                                }
                                if (searchOriginDest) {
                                    d.setOrigin_destination(temp.getOrigin_destination());
                                } else {
                                    d.setOrigin_destination("");
                                }

                                if (searchTermIp) {
                                    d.setTerm_ip(temp.getTerm_ip());
                                } else {
                                    d.setTerm_ip("");
                                }

                                if (searchTermDest) {
                                    d.setTerm_destination(temp.getTerm_destination());
                                } else {
                                    d.setTerm_destination("");
                                }
                                break;
                            }
                        }
                    }
                }
                if (!isMatch) {
                    long end_period = temp.getPeriod_end() + inc_time;
                    temp.setPeriod_end(end_period);

                    if (temp.getDuration() > 0) {
                        temp.setTotal_success(1);
                        temp.setTotal_fail(0);
                    } else {
                        temp.setTotal_success(0);
                        temp.setTotal_fail(1);
                    }
                    qualityCdrList.add(temp);
                }
            }
        }
        return qualityCdrList;
    }

    public ArrayList<CDRQualityDTO> getClientCdrQualityDTOs(LoginDTO login_dto, CDRReportDTO sdto) {

        ArrayList<CDRQualityDTO> qualityCdrList = new ArrayList<CDRQualityDTO>();
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        mvtsCdrList = getMvtsCdrDTOList(login_dto.getId(), sdto);

        boolean searchOriginIp = false;
        boolean searchOriginDest = false;
        boolean searchTermIp = false;
        boolean searchTermDest = false;

        if (sdto.getSummaryBy() != null) {
            for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                switch (sdto.getSummaryBy()[j]) {
                    case 0:
                        searchOriginIp = true;
                        break;
                    case 1:
                        searchOriginDest = true;
                    case 2:
                        searchTermIp = true;
                        break;
                    case 3:
                        searchTermDest = true;
                        break;
                }
            }
        }
        long inc_time = 0;
        if (sdto != null && sdto.getSummaryType() > 0) {
            switch (sdto.getSummaryType()) {
                case 1:
                    inc_time = 900000;
                    break;
                case 2:
                    inc_time = 3600000;
                    break;
                case 3:
                    inc_time = 86400000;
                    break;
                case 4:
                    inc_time = 604800000;
                    break;
            }
        }

        boolean isMatch = false;
        if (mvtsCdrList != null && mvtsCdrList.size() > 0) {
            for (int inc = 0; inc < mvtsCdrList.size(); inc++) {
                CDRReportDTO mcdr = new CDRReportDTO();
                mcdr = mvtsCdrList.get(inc);
                CDRQualityDTO temp = new CDRQualityDTO();
                temp.setOrigin_client_id(mcdr.getOrigin_client_id());
                temp.setOrigin_client_name(mcdr.getOrigin_client_name());
                temp.setOrigin_destination(mcdr.getOrigin_destination());
                temp.setOrigin_bill_amount(mcdr.getOrigin_bill_amount());
                temp.setOrigin_ip(mcdr.getOrigin_ip());
                temp.setTerm_client_id(mcdr.getTerm_client_id());
                temp.setTerm_client_name(mcdr.getTerm_client_name());
                temp.setTerm_ip(mcdr.getTerm_ip());
                temp.setTerm_destination(mcdr.getTerm_destination());
                temp.setTerm_bill_amount(mcdr.getTerm_bill_amount());
                temp.setDuration(mcdr.getDuration());

                temp.setTotal_fail(0);
                temp.setTotal_success(0);
                temp.setPeriod_start(Utils.getDateLong(mcdr.getConnection_time()));
                temp.setPeriod_end(Utils.getDateLong(mcdr.getConnection_time()));

                double pdd = mcdr.getPdd();
                temp.setAvg_pdd(pdd);

                if (qualityCdrList == null) {
                    qualityCdrList = new ArrayList<CDRQualityDTO>();
                }

                if (qualityCdrList != null && qualityCdrList.size() > 0) {
                    for (int j = 0; j < qualityCdrList.size(); j++) {
                        CDRQualityDTO d = new CDRQualityDTO();
                        d = qualityCdrList.get(j);
                        if ((searchOriginIp && !d.getOrigin_ip().equals(temp.getOrigin_ip()))
                                || (searchOriginDest && !d.getOrigin_destination().equals(temp.getOrigin_destination()))
                                || (searchTermIp && !d.getTerm_ip().equals(temp.getTerm_ip()))
                                || (searchTermDest && !d.getTerm_destination().equals(temp.getTerm_destination()))) {
                            isMatch = false;
                            continue;
                        } else {
                            if (sdto.getSummaryType() > 0) {
                                if (temp.getPeriod_start() >= d.getPeriod_start()
                                        && temp.getPeriod_start() <= d.getPeriod_end()) {
                                    isMatch = true;
                                    if (temp.getDuration() > 0) {
                                        temp.setTotal_success(1);
                                        temp.setTotal_fail(0);
                                    } else {
                                        temp.setTotal_success(0);
                                        temp.setTotal_fail(1);
                                    }

                                    long dur = d.getDuration() + temp.getDuration();
                                    d.setDuration(dur);
                                    d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                    int suc = d.getTotal_success() + temp.getTotal_success();
                                    d.setTotal_success(suc);
                                    d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                    d.setOrigin_bill_amount(d.getOrigin_bill_amount() + temp.getOrigin_bill_amount());
                                    d.setTerm_bill_amount(d.getTerm_bill_amount() + temp.getTerm_bill_amount());
                                    if (searchOriginIp) {
                                        d.setOrigin_ip(temp.getOrigin_ip());
                                    } else {
                                        d.setOrigin_ip("");
                                    }
                                    if (searchOriginDest) {
                                        d.setOrigin_destination(temp.getOrigin_destination());
                                    } else {
                                        d.setOrigin_destination("");
                                    }

                                    if (searchTermIp) {
                                        d.setTerm_ip(temp.getTerm_ip());
                                    } else {
                                        d.setTerm_ip("");
                                    }

                                    if (searchTermDest) {
                                        d.setTerm_destination(temp.getTerm_destination());
                                    } else {
                                        d.setTerm_destination("");
                                    }

                                    break;
                                } else {
                                    isMatch = false;
                                    continue;
                                }
                            } else {
                                isMatch = true;
                                if (temp.getDuration() > 0) {
                                    temp.setTotal_success(1);
                                    temp.setTotal_fail(0);
                                } else {
                                    temp.setTotal_success(0);
                                    temp.setTotal_fail(1);
                                }

                                long dur = d.getDuration() + temp.getDuration();
                                d.setDuration(dur);
                                d.setAvg_pdd((d.getAvg_pdd() + temp.getAvg_pdd()));
                                int suc = d.getTotal_success() + temp.getTotal_success();
                                d.setTotal_success(suc);
                                d.setTotal_fail(d.getTotal_fail() + temp.getTotal_fail());
                                d.setPeriod_end(temp.getPeriod_start());
                                d.setOrigin_bill_amount(d.getOrigin_bill_amount() + temp.getOrigin_bill_amount());
                                d.setTerm_bill_amount(d.getTerm_bill_amount() + temp.getTerm_bill_amount());
                                if (searchOriginIp) {
                                    d.setOrigin_ip(temp.getOrigin_ip());
                                } else {
                                    d.setOrigin_ip("");
                                }
                                if (searchOriginDest) {
                                    d.setOrigin_destination(temp.getOrigin_destination());
                                } else {
                                    d.setOrigin_destination("");
                                }

                                if (searchTermIp) {
                                    d.setTerm_ip(temp.getTerm_ip());
                                } else {
                                    d.setTerm_ip("");
                                }

                                if (searchTermDest) {
                                    d.setTerm_destination(temp.getTerm_destination());
                                } else {
                                    d.setTerm_destination("");
                                }
                                break;
                            }
                        }
                    }
                }
                if (!isMatch) {
                    long end_period = temp.getPeriod_end() + inc_time;
                    temp.setPeriod_end(end_period);

                    if (temp.getDuration() > 0) {
                        temp.setTotal_success(1);
                        temp.setTotal_fail(0);
                    } else {
                        temp.setTotal_success(0);
                        temp.setTotal_fail(1);
                    }
                    qualityCdrList.add(temp);
                }
            }
        }
        return qualityCdrList;
    }
}
