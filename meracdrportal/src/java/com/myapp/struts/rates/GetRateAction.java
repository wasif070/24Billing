/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class GetRateAction extends Action {

    static Logger logger = Logger.getLogger(GetRateAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            RateForm formBean = (RateForm) form;
            RateDTO dto = new RateDTO();
            RateTaskSchedular scheduler = new RateTaskSchedular();
            dto = scheduler.getRateDTO(id);
            if (dto != null) {
                request.getSession(true).setAttribute("rate_id", id);
                formBean.setRate_id(dto.getRate_id());
                formBean.setRateplan_id(dto.getRateplan_id());
                formBean.setRate_destination_code(dto.getRate_destination_code());
                formBean.setRate_destination_name(dto.getRate_destination_name());
                formBean.setRate_per_min(dto.getRate_per_min());
                formBean.setRate_first_pulse(dto.getRate_first_pulse());
                formBean.setRate_next_pulse(dto.getRate_next_pulse());
                formBean.setRate_grace_period(dto.getRate_grace_period());
                formBean.setRate_failed_period(dto.getRate_failed_period());
                formBean.setRate_status(dto.getRate_status());
                formBean.setRate_day(dto.getRate_day());
                formBean.setRate_fromhour(dto.getRate_fromhour());
                formBean.setRate_frommin(dto.getRate_frommin());
                formBean.setRate_tohour(dto.getRate_tohour());
                formBean.setRate_tomin(dto.getRate_tomin());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
