/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author Anwar
 */
public class RateTaskSchedular {

    public RateTaskSchedular() {
    }

    public MyAppError addRateInformation(RateDTO p_dto) {
        RateDAO rateDAO = new RateDAO();
        return rateDAO.addRateInformation(p_dto);
    }

    public ArrayList<RateDTO> getRateDTOs(long rateplan_id, LoginDTO l_dto) {
        return RateLoader.getInstance().getRateDTOListByRatePlanId(rateplan_id);
    }

    public ArrayList<RateDTO> getSearchRateDTOs(long rateplan_id, LoginDTO l_dto, RateDTO dto) {
        ArrayList<RateDTO> rlist = RateLoader.getInstance().getRateDTOListByRatePlanId(rateplan_id);
        return RateLoader.getInstance().getRateDTOsWithSearchParam(rlist, dto);
    }

    public MyAppError editRateInformation(RateDTO p_dto) {
        RateDAO rateDAO = new RateDAO();
        return rateDAO.editRateInformation(p_dto);
    }

    public RateDTO getRateDTO(long id) {
        return RateLoader.getInstance().getRateDTOByRateId(id);
    }

    public StringBuffer getRateCSVString(RateDTO dto) {
        RateDAO rateDAO = new RateDAO();
        StringBuffer csvString = rateDAO.getRateCSVStrings(dto);
        return csvString;
    }

    public MyAppError uploadFileData(ArrayList<RateDTO> p_dto) {
        RateDAO rateDAO = new RateDAO();
        return rateDAO.uploadFlieData(p_dto);
    }

    public MyAppError deleteMultiple(long rateIds[]) {
        RateDAO dao = new RateDAO();
        return dao.multipleDelete(rateIds);
    }

    public ArrayList<RateDTO> getRateDTOs(RateDTO p_dto) {
        return RateLoader.getInstance().getList(p_dto);
    }
}
