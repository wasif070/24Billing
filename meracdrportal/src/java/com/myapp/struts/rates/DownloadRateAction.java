/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Anwar
 */
public class DownloadRateAction extends Action {

    static Logger logger = Logger.getLogger(DownloadRateAction.class.getName());

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=rates.csv");
            long rate_id = 0;
            if (request.getParameter("rate_id") != null) {
                rate_id = Integer.parseInt(request.getParameter("rate_id"));
            } else {
                rate_id = Long.parseLong(request.getSession(true).getAttribute("sess_rate_id").toString());
            }
            //RateForm formBean = (RateForm) form;
            RateDTO dto = new RateDTO();
            RateTaskSchedular schedular = new RateTaskSchedular();
            dto.setRateplan_id(rate_id);
            try {
                ServletOutputStream out = response.getOutputStream();

                StringBuffer sb = schedular.getRateCSVString(dto);

                InputStream in =
                        new ByteArrayInputStream(sb.toString().getBytes("UTF-8"));
                int inSize = in.available();
                byte[] outputByte = new byte[inSize];
                while (in.read(outputByte, 0, inSize) != -1) {
                    out.write(outputByte, 0, inSize);
                }
                in.close();
                out.flush();
                out.close();

            } catch (Exception e) {
            }

        }
        return null;
    }
}