/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Anwar
 */
public class RateLoader {

    static Logger logger = Logger.getLogger(RateLoader.class.getName());
    static RateLoader rateLoader = null;

    public RateLoader() {
    }

    public static RateLoader getInstance() {
        if (rateLoader == null) {
            createRateLoader();
        }
        return rateLoader;
    }

    private synchronized static void createRateLoader() {
        if (rateLoader == null) {
            rateLoader = new RateLoader();
        }
    }

    public synchronized RateDTO getRateDTOByID(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = new RateDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from mvts_rates where rate_delete=0 and id=" + id+" order by id DESC";
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));
                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
            }

        } catch (Exception e) {
            logger.fatal("Exception in getRateDTOByID:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;

    }

    public synchronized ArrayList<RateDTO> getRateDTOListByRatePlanId(long rateplan_id) {
        ArrayList<RateDTO> list = new ArrayList<RateDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = new RateDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from mvts_rates where rate_delete=0 and rateplan_id=" + rateplan_id;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dto = new RateDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));
                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
                list.add(dto);
            }

        } catch (Exception e) {
            logger.fatal("Exception in getRateDTOListByRatePlanId:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public synchronized ArrayList<RateDTO> getRateDTOByRateIdList(long rate_id) {
        ArrayList<RateDTO> list = new ArrayList<RateDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = new RateDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from mvts_rates where rate_delete=0 and rate_id=" + rate_id;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dto = new RateDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));
                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
                list.add(dto);
            }

        } catch (Exception e) {
            logger.fatal("Exception in getRateDTOByRateIdList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public synchronized RateDTO getRateDTOByRateId(long rate_id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = new RateDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from mvts_rates where rate_delete=0 and rate_id=" + rate_id;
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));
                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
            }

        } catch (Exception e) {
            logger.fatal("Exception in getRateDTOByRateId:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public synchronized ArrayList<RateDTO> getList(RateDTO rate_dto) {
        DBConnection dbConnection = null;
        Statement statement = null;
        ArrayList<RateDTO> rateListFromDB = new ArrayList<RateDTO>();
        String sortOrder[] = {"DESC", "ASC"};
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String sql = "select * from mvts_rates where rate_delete=0 ";
            if (rate_dto.searchWithPrefixName) {
                sql += " and rate_destination_code like '" + rate_dto.getRate_destination_code() + "%' ";
            }
            if (rate_dto.getRate_id() > 0) {
                sql += " and rateplan_id=" + rate_dto.getRate_id();
            }

            switch (rate_dto.getSortItem()) {
                case 2:
                    sql += " order by rate_destination_code " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 3:
                    sql += " order by rate_destination_name " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 4:
                    sql += " order by rate_per_min " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 5:
                    sql += " order by rate_first_pulse " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 6:
                    sql += " order by rate_next_pulse " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 7:
                    sql += " order by rate_grace_period " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                case 8:
                    sql += " order by rate_created_date " + sortOrder[rate_dto.getSortOrder() - 1];
                    break;
                default:
                    sql += " order by rate_destination_code ASC ";
                    break;
            }

            sql += " limit " + rate_dto.getLimit() + " offset " + rate_dto.getStartingRow();
            logger.debug("query string-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                RateDTO dto = new RateDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setRate_id(resultSet.getLong("rate_id"));
                dto.setRateplan_id(resultSet.getInt("rateplan_id"));
                dto.setRate_destination_code(resultSet.getString("rate_destination_code"));
                dto.setRate_destination_name(resultSet.getString("rate_destination_name"));
                dto.setRate_per_min(resultSet.getFloat("rate_per_min"));
                dto.setRate_first_pulse(resultSet.getInt("rate_first_pulse"));
                dto.setRate_next_pulse(resultSet.getInt("rate_next_pulse"));
                dto.setRate_grace_period(resultSet.getInt("rate_grace_period"));
                dto.setRate_status(resultSet.getInt("rate_status"));
                dto.setRate_date(resultSet.getLong("rate_created_date"));
                dto.setRate_created_date(Utils.LongToDate(dto.getRate_date()));
                dto.setRate_failed_period(resultSet.getInt("rate_failed_period"));
                dto.setRate_sin_day(resultSet.getInt("rate_day"));
                dto.setRate_sin_fromhour(resultSet.getInt("rate_from_hour"));
                dto.setRate_sin_frommin(resultSet.getInt("rate_from_min"));
                dto.setRate_sin_tohour(resultSet.getInt("rate_to_hour"));
                dto.setRate_sin_tomin(resultSet.getInt("rate_to_min"));
                dto.setRate_statusname(Constants.GATEWAY_STATUS_STRING[dto.getRate_status()]);
                rateListFromDB.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getList:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rateListFromDB;
    }

    public synchronized int getTotalRows(RateDTO rate_dto) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int rowCount = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();

            String sql = "select * from mvts_rates where rate_delete=0 ";
            if (rate_dto.searchWithPrefixName) {
                sql += " and rate_destination_code like '" + rate_dto.getRate_destination_code() + "%' ";
            }
            if (rate_dto.getRate_id() > 0) {
                sql += " and rateplan_id=" + rate_dto.getRate_id();
            }
            ResultSet resultSet = statement.executeQuery(sql.replace("*", "count(id) as total_row"));
            if (resultSet.next()) {
                rowCount = resultSet.getInt("total_row");
            }
            if (resultSet != null) {
                resultSet.close();
            }

        } catch (Exception e) {
            logger.fatal("Exception in rate getTotalRows:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return rowCount;
    }

    public ArrayList<RateDTO> getRateDTOsWithSearchParam(ArrayList<RateDTO> list, RateDTO rdto) {
        ArrayList newList = null;

        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                RateDTO dto = (RateDTO) i.next();
                if ((rdto.searchWithPrefixName && !dto.getRate_destination_code().toLowerCase().startsWith(rdto.getRate_destination_code()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }
}
