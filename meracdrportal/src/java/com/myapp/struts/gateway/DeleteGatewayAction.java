/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class DeleteGatewayAction extends Action {

    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int cid = Integer.parseInt(request.getParameter("id"));
        GatewayTaskSchedular rt = new GatewayTaskSchedular();

        MyAppError error = new MyAppError();
        error = rt.deleteGateway(cid);
        request.getSession(true).setAttribute(Constants.MESSAGE, "Gateway Deleted Successfully!");
        return mapping.findForward("success");
    }
}
