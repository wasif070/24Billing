package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class GatewayLoader {

    static Logger logger = Logger.getLogger(GatewayLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<GatewayDTO> gatewayList = null;
    private HashMap<String, GatewayDTO> switchGatewayMap = null;
    private HashMap<Long, GatewayDTO> gatewayDTOByID = null;
    static GatewayLoader gatewayLoader = null;

    public GatewayLoader() {
        forceReload();
    }

    public static GatewayLoader getInstance() {
        if (gatewayLoader == null) {
            createGatewayLoader();
        }
        return gatewayLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (gatewayLoader == null) {
            gatewayLoader = new GatewayLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        Statement statement = null;
        Statement remoteStatement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            statement = dbConnection.connection.createStatement();
            gatewayList = new ArrayList<GatewayDTO>();
            gatewayDTOByID = new HashMap<Long, GatewayDTO>();
            switchGatewayMap = new HashMap<String, GatewayDTO>();

            String sql = "select gateway_id,gateway_name,dst_port_h323,dst_port_sip from mvts_gateway";
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            remoteStatement = dbConn.connection.createStatement();
            ResultSet rs = remoteStatement.executeQuery(sql);
            while (rs.next()) {
                GatewayDTO sGatewatDTO = new GatewayDTO();
                sGatewatDTO.setGateway_dst_port_h323(rs.getInt("dst_port_h323"));
                sGatewatDTO.setGateway_dst_port_sip(rs.getInt("dst_port_sip"));
                sGatewatDTO.setSwitchGatewayId(rs.getLong("gateway_id"));
                switchGatewayMap.put(rs.getString("gateway_name"), sGatewatDTO);
            }

            sql = "select id,gateway_ip,gateway_name,gateway_type,gateway_status,client_id from gateway where gateway_delete=0 order by id DESC";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                GatewayDTO dto = new GatewayDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setGateway_ip(resultSet.getString("gateway_ip"));
                dto.setGateway_name(resultSet.getString("gateway_name"));
                try {
                    dto.setSwitchGatewayId(switchGatewayMap.get(dto.getGateway_name()).getSwitchGatewayId());
                } catch (Exception ex) {
                }
                dto.setGateway_type(resultSet.getInt("gateway_type"));
                try {
                    dto.setGateway_type_name(Constants.CLIENT_TYPE_NAME[dto.getGateway_type()]);
                } catch (Exception ex) {
                }
                dto.setClientId(resultSet.getInt("client_id"));
                if (dto.getClientId() > 0) {
                    ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(resultSet.getLong("client_id"));
                    if (cdto != null) {
                        if (cdto.getClient_id() != null && cdto.getClient_id().length() > 0) {
                            dto.setClient_name(cdto.getClient_id());
                            dto.setClient_balance(Double.parseDouble(cdto.getClient_balance()));

                        }
                    }

                }
                if (dto.getGateway_name().length() > 0) {
                    GatewayDTO gdto = switchGatewayMap.get(dto.getGateway_name());
                    if (gdto != null) {
                        dto.setGateway_dst_port_h323(gdto.getGateway_dst_port_h323());
                        dto.setGateway_dst_port_sip(gdto.getGateway_dst_port_sip());
                    }
                }
                dto.setGateway_status(resultSet.getInt("gateway_status"));
                dto.setGateway_status_name(Constants.GATEWAY_STATUS_STRING[dto.getGateway_status()]);
                dto.setSuperUser(false);
                gatewayDTOByID.put(dto.getId(), dto);
                gatewayList.add(dto);

            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remoteStatement != null) {
                    remoteStatement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOList() {
        checkForReload();
        return gatewayList;
    }

    public synchronized GatewayDTO getGatewayDTOByID(long id) {
        checkForReload();
        return gatewayDTOByID.get(id);
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOByType(int type) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByType = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getGateway_type() == type) {
                gatewayListByType.add(dto);
            }
        }
        return gatewayListByType;
    }

    public synchronized ArrayList<GatewayDTO> getClientsGatewayList(long client_id) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByClient = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getClientId() == client_id) {
                gatewayListByClient.add(dto);
            }
        }
        return gatewayListByClient;
    }

    public synchronized ClientDTO getClientDTOByIp(String ip_address) {
        checkForReload();
        ClientDTO clientDto = new ClientDTO();
        if (ip_address == null) {
            return null;
        }
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (ip_address.startsWith(dto.getGateway_ip())) {
                clientDto = ClientLoader.getInstance().getClientDTOByID(dto.getClientId());
            }
        }
        return clientDto;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(GatewayDTO udto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<GatewayDTO> list = gatewayList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                GatewayDTO dto = (GatewayDTO) i.next();
                if ((udto.searchWithGatewayIP && !dto.getGateway_ip().toLowerCase().startsWith(udto.getGateway_ip()))
                        || (udto.searchWithStatus && dto.getGateway_status() != udto.getGateway_status())
                        || (udto.searchWithType && dto.getGateway_type() != udto.getGateway_type())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted() {
        checkForReload();
        ArrayList<GatewayDTO> list = gatewayList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    GatewayDTO dto1 = (GatewayDTO) o1;
                    GatewayDTO dto2 = (GatewayDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
