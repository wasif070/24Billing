package com.myapp.struts.gateway;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;

public class AddGatewayAction extends Action {

    static Logger logger = Logger.getLogger(AddGatewayAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            GatewayForm formBean = (GatewayForm) form;
            GatewayDTO dto = new GatewayDTO();
            GatewayTaskSchedular scheduler = new GatewayTaskSchedular();

            dto.setGateway_ip(formBean.getGateway_ip());
            dto.setGateway_name(formBean.getGateway_name());
            dto.setGateway_type(formBean.getGateway_type());
            dto.setGateway_status(formBean.getGateway_status());
            dto.setClientId(formBean.getClientId());
            dto.setGateway_dst_port_h323(formBean.getGateway_dst_port_h323());
            dto.setGateway_dst_port_sip(formBean.getGateway_dst_port_sip());

            MyAppError error = scheduler.addGatewayInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "Gateway is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
