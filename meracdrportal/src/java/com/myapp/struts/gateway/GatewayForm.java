package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class GatewayForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private long id;
    private String gateway_name;
    private String gateway_ip;
    private int gateway_status;
    private int gateway_type;
    private int clientId;
    private int gateway_dst_port_h323;
    private int gateway_dst_port_sip;
    private int mvts_id;
    private String mvts_name;
    private String message;
    private long[] selectedIDs;
    private ArrayList gatewayList;
    private String activateBtn;
    private String inactiveBtn;
    private String blockBtn;
    private String deleteBtn;
    private long switchGatewayId;
    private int jgateway_type;

    public GatewayForm() {
        super();
        gateway_type = -1;
        gateway_status = -1;
    }

    public int getJgateway_type() {
        return jgateway_type;
    }

    public void setJgateway_type(int jgateway_type) {
        this.jgateway_type = jgateway_type;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getGatewayList() {
        return gatewayList;
    }

    public void setGatewayList(ArrayList gatewayList) {
        this.gatewayList = gatewayList;
    }

    public String getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String gateway_name) {
        this.gateway_name = gateway_name;
    }

    public int getGateway_status() {
        return gateway_status;
    }

    public void setGateway_status(int gateway_status) {
        this.gateway_status = gateway_status;
    }

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }

    public String getGateway_ip() {
        return gateway_ip;
    }

    public void setGateway_ip(String gateway_ip) {
        this.gateway_ip = gateway_ip;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getGateway_dst_port_h323() {
        return gateway_dst_port_h323;
    }

    public void setGateway_dst_port_h323(int gateway_dst_port_h323) {
        this.gateway_dst_port_h323 = gateway_dst_port_h323;
    }

    public int getGateway_dst_port_sip() {
        return gateway_dst_port_sip;
    }

    public void setGateway_dst_port_sip(int gateway_dst_port_sip) {
        this.gateway_dst_port_sip = gateway_dst_port_sip;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getMessage() {
        return message;
    }

    public String getActivateBtn() {
        return activateBtn;
    }

    public void setActivateBtn(String activateBtn) {
        this.activateBtn = activateBtn;
    }

    public String getBlockBtn() {
        return blockBtn;
    }

    public void setBlockBtn(String blockBtn) {
        this.blockBtn = blockBtn;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public String getInactiveBtn() {
        return inactiveBtn;
    }

    public void setInactiveBtn(String inactiveBtn) {
        this.inactiveBtn = inactiveBtn;
    }

    public int getMvts_id() {
        return mvts_id;
    }

    public void setMvts_id(int mvts_id) {
        this.mvts_id = mvts_id;
    }

    public String getMvts_name() {
        return mvts_name;
    }

    public void setMvts_name(String mvts_name) {
        this.mvts_name = mvts_name;
    }

    public long getSwitchGatewayId() {
        return switchGatewayId;
    }

    public void setSwitchGatewayId(long switchGatewayId) {
        this.switchGatewayId = switchGatewayId;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getGateway_ip() == null || getGateway_ip().length() < 1) {
                errors.add("gateway_ip", new ActionMessage("errors.gateway_ip.required"));
            } else if (!Utils.isValidIP(getGateway_ip())) {
                errors.add("gateway_ip", new ActionMessage("errors.invalid.gateway_ip"));
            }

            if (getGateway_name() == null || getGateway_name().length() < 1) {
                errors.add("gateway_name", new ActionMessage("errors.gateway_name.required"));
            }

            if (request.getParameter("gateway_type").length() < 1) {
                errors.add("gateway_type", new ActionMessage("errors.gateway_type.required"));
            }

            if (request.getParameter("clientId").length() < 1) {
                errors.add("clientId", new ActionMessage("errors.client_id.required"));
            }
            if (getGateway_type() < 0) {
                errors.add("gateway_type", new ActionMessage("errors.gateway_type.required"));
            }
        }
        return errors;
    }
}
