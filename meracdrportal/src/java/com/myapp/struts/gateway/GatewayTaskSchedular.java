package com.myapp.struts.gateway;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class GatewayTaskSchedular {

    public GatewayTaskSchedular() {
    }

    public MyAppError addGatewayInformation(GatewayDTO p_dto) {
        GatewayDAO userDAO = new GatewayDAO();
        return userDAO.addGatewayInformation(p_dto);
    }

    public MyAppError editGatewayInformation(GatewayDTO p_dto) {
        GatewayDAO userDAO = new GatewayDAO();
        return userDAO.editGatewayInformation(p_dto);
    }

    public MyAppError deleteGateway(int cid) {
        GatewayDAO dao = new GatewayDAO();
        return dao.deleteGateway(cid);
    }

    public GatewayDTO getGatewayDTO(long id) {
        return GatewayLoader.getInstance().getGatewayDTOByID(id);
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted(LoginDTO l_dto) {
        return GatewayLoader.getInstance().getGatewayDTOsSorted();
    }

    public ArrayList<GatewayDTO> getGatewayDTOs(LoginDTO l_dto) {
        return GatewayLoader.getInstance().getGatewayDTOList();
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(GatewayDTO udto, LoginDTO l_dto) {
        return GatewayLoader.getInstance().getGatewayDTOsWithSearchParam(udto);
    }

    public MyAppError activateMultiple(long gatewayIds[]) {
        GatewayDAO dao = new GatewayDAO();
        return dao.multipleGatewayStatusUpdate(gatewayIds, Constants.LIVE_ACTIVE);
    }

    public MyAppError inactivateMultiple(long gatewayIds[]) {
        GatewayDAO dao = new GatewayDAO();
        return dao.multipleGatewayStatusUpdate(gatewayIds, Constants.LIVE_INACTIVE);
    }

    /* public MyAppError blockMultiple(long gatewayIds[]) {
    GatewayDAO dao = new GatewayDAO();
    return dao.multipleGatewayStatusUpdate(gatewayIds, Constants.LIVE_BLOCK);
    } */
    public MyAppError deleteMultiple(long gatewayIds[]) {
        GatewayDAO dao = new GatewayDAO();
        return dao.multipleGatewayDelete(gatewayIds);
    }
}
