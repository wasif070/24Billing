package com.myapp.struts.user;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

public class UserLoader {

    static Logger logger = Logger.getLogger(UserLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private ArrayList<UserDTO> clientList = null;
    private ArrayList<UserDTO> userList = null;
    private HashMap<String, UserDTO> userDTOByUserID = null;
    private HashMap<Long, UserDTO> userDTOByID = null;
    static UserLoader userLoader = null;

    public UserLoader() {
        forceReload();
    }

    public static UserLoader getInstance() {
        if (userLoader == null) {
            createUserLoader();
        }
        return userLoader;
    }

    private synchronized static void createUserLoader() {
        if (userLoader == null) {
            userLoader = new UserLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            userDTOByUserID = new HashMap<String, UserDTO>();
            userDTOByID = new HashMap<Long, UserDTO>();
            clientList = new ArrayList<UserDTO>();
            userList = new ArrayList<UserDTO>();
            String sql = "select id,user_id,user_password,user_full_name,user_status from users where user_delete=0 order by id DESC";
            logger.debug("Sql: " + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                UserDTO dto = new UserDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setUserId(resultSet.getString("user_id"));
                dto.setUserPassword(resultSet.getString("user_password"));
                dto.setFullName(resultSet.getString("user_full_name"));
                dto.setUserStatus(resultSet.getInt("user_status"));
                dto.setUserStatusName(Constants.LIVE_STATUS_STRING[dto.getUserStatus()]);
                dto.setSuperUser(true);
                dto.setClientType(Constants.BOTH);
                userDTOByID.put(dto.getId(), dto);
                userDTOByUserID.put(dto.getUserId(), dto);
                userList.add(dto);
            }
            resultSet.close();
            sql = "select * from clients where client_delete=0";
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                UserDTO dto = new UserDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setUserId(resultSet.getString("client_id"));
                dto.setUserPassword(resultSet.getString("client_password"));
                dto.setUserStatus(resultSet.getInt("client_status"));
                dto.setClient_blance(resultSet.getDouble("client_balance"));
                dto.setUserStatusName(Constants.LIVE_STATUS_STRING[dto.getUserStatus()]);
                dto.setSuperUser(false);
                dto.setClientType(resultSet.getInt("client_type"));

                userDTOByUserID.put(dto.getUserId(), dto);
                clientList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in UserLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized UserDTO getUserDTOByUserIDPass(String user_id, String password) {
        checkForReload();
        UserDTO dto = userDTOByUserID.get(user_id);
        if (dto != null) {
            if (dto.getUserPassword().equals(password)) {
                return dto;
            }
        }
        return null;
    }

    public synchronized ArrayList<UserDTO> getClientDTOList() {
        checkForReload();
        return clientList;
    }

    public synchronized ArrayList<UserDTO> getUserDTOList() {
        checkForReload();
        return userList;
    }

    public synchronized UserDTO getUserDTOByID(long id) {
        checkForReload();
        return userDTOByID.get(id);
    }

    public ArrayList<UserDTO> getUserDTOsWithSearchParam(UserDTO udto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<UserDTO> list = userList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                UserDTO dto = (UserDTO) i.next();
                if ((udto.searchWithUserID && !dto.getUserId().toLowerCase().startsWith(udto.getUserId()))
                        || (udto.searchWithStatus && dto.getUserStatus() != udto.getUserStatus())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<UserDTO> getUserDTOsSorted() {
        checkForReload();
        ArrayList<UserDTO> list = userList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    UserDTO dto1 = (UserDTO) o1;
                    UserDTO dto2 = (UserDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
