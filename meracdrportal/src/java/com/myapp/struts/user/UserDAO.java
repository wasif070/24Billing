package com.myapp.struts.user;

import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class UserDAO {

    static Logger logger = Logger.getLogger(UserDAO.class.getName());

    public UserDAO() {
    }

    public MyAppError addUserInformation(UserDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select user_id from users where user_id=? and user_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getUserId());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            
            sql = "select client_id from clients where client_id=? and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getUserId());
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("User ID conflict with user or client");
                resultSet.close();
                return error;
            }
            
            sql = "insert into users(user_id,user_password,user_full_name,user_status) values(?,?,?,?);";
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getUserId());
            ps.setString(2, p_dto.getUserPassword());
            ps.setString(3, p_dto.getFullName());
            ps.setInt(4, p_dto.getUserStatus());

            ps.executeUpdate();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editUserInformation(UserDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            
            String sql = "select client_id from clients where client_id=? and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getUserId());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("User ID conflict with user or client");
                resultSet.close();
                return error;
            }
            resultSet.close();
            
            sql = "select user_id from users where user_id = ? and id!=" + p_dto.getId() +" and user_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getUserId());
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate User ID.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update users set user_id=?,user_password=?,user_full_name=?,user_status=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getUserId());
            ps.setString(2, p_dto.getUserPassword());
            ps.setString(3, p_dto.getFullName());
            ps.setInt(4, p_dto.getUserStatus());

            ps.executeUpdate();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteUser(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update users set user_delete = 1 where id =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                UserLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while Deleting User: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
