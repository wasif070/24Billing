package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;

public class EditUserAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        UserForm formBean = (UserForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            UserDTO dto = new UserDTO();
            UserTaskSchedular scheduler = new UserTaskSchedular();
            dto.setId(formBean.getId());
            dto.setUserId(formBean.getUserId());
            dto.setUserPassword(formBean.getUserPassword());
            dto.setFullName(formBean.getFullName());
            if(dto.getId()>-1){
            dto.setUserStatus(formBean.getUserStatus());
            }else{
            dto.setUserStatus(0);
            }

            MyAppError error = scheduler.editUserInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                formBean.setMessage(false, "User is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
