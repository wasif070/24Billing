package com.myapp.struts.user;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class UserForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private int userStatus;
    private long id;
    private String userId;
    private String userPassword;
    private String retypePassword;
    private String fullName;
    private String message;
    private long[] selectedIDs;
    private ArrayList userList;

    public UserForm() {
        super();
        userStatus = -1;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public ArrayList getUserList() {
        return userList;
    }

    public void setUserList(ArrayList userList) {
        this.userList = userList;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getDoValidate() {
        return this.doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            boolean pass = true;
            boolean re_pass = true;
            if (getUserId() == null || getUserId().length() < 1) {
                errors.add("userId", new ActionMessage("errors.user_id.required"));
            } else if (!Utils.checkValidUserId(getUserId())) {
                errors.add("userId", new ActionMessage("errors.invalid.user_id"));
            }

            if (getUserPassword() == null || getUserPassword().length() < 1) {
                errors.add("userPassword", new ActionMessage("errors.user_pass.required"));
                pass = false;
            }

            if (getRetypePassword() == null || getRetypePassword().length() < 1) {
                errors.add("retypePassword", new ActionMessage("errors.retype_pass.required"));
                re_pass = false;
            }

            if (pass == true && re_pass == true) {
                if (getUserPassword().length() < 6) {
                    errors.add("retypePassword", new ActionMessage("errors.password_length"));
                } else if (!getRetypePassword().equals(getUserPassword())) {
                    errors.add("retypePassword", new ActionMessage("errors.password_matching"));
                }
            }
        }
        return errors;
    }
}
