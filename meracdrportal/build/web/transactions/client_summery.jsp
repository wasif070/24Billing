<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.transactions.TransactionDTO,java.text.DecimalFormat,java.text.NumberFormat" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <title>24Billing :: Balance Summery</title>
        <%@include file="../includes/header.jsp"%>
        <%
            NumberFormat formatter = new DecimalFormat("#0.00");
            TransactionDTO bdto = (TransactionDTO) request.getSession(true).getAttribute("BalanceDTO");
        %>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">              
                <div class="pad_10 border_left">
                       <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add(";");
                    navList.add(";");
                %>
                <%= navigation.Navigation_client.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <div class="half-div center-align">
                        <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="2"><h2>Welcome <%=login_dto.getClientId()%></h2></td>
                            </tr>
                            <tr style="color:#f00">
                                <th width="180">Current Balance: </th><th><%=formatter.format(login_dto.getClient_balance())%></th>
                            </tr>
                            <% if (bdto != null) {%>
                            <tr>
                                <th width="180">Total Received: </th><th><%=bdto.getTransaction_recharge()%></th>
                            </tr>
                            <tr>
                                <th>Total Returned: </th><th><%=bdto.getTransaction_return()%></th>
                            </tr>
                            <tr>
                                <th>Total Paid: </th><th><%=bdto.getTransaction_receive()%></th>
                            </tr>
                            <tr>
                                <th>Due: </th><th><%=bdto.getTransaction_balance()%></th>
                            </tr>
                            <% } else {%>
                            <tr>
                                <th width="180">Total Received: </th><th>0.00</th>
                            </tr>
                            <tr>
                                <th>Total Returned: </th><th>0.00</th>
                            </tr>
                            <tr>
                                <th>Total Paid: </th><th>0.00</th>
                            </tr>
                            <tr>
                                <th>Due: </th><th>0.00</th>
                            </tr>
                            <% }%>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>