<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <%
            CDRReportDAO mvtsDao = new CDRReportDAO();
            String links = "";

            String recordPerPage = (String) request.getParameter("recordPerPage");
            if (recordPerPage == null) {
                recordPerPage = "" + Constants.PER_PAGE_RECORD;
            }

            String pNo = (String) request.getParameter("pageNo");
            if (pNo == null) {
                pNo = "1";
            }


            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRReportDTO> mvtscdrList = (ArrayList<CDRReportDTO>) request.getSession(true).getAttribute("CDRReportDTO");
            CDRReportDTO searchDTO = (CDRReportDTO) request.getSession(true).getAttribute("SearchCdrDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat amount_formatter = new DecimalFormat("#00.000000");
%>
        <title>24Billing :: Mvts Cdr List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div class="top"></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right" style="width:80%">
                
                <div class="pad_10 border_left">
                     <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("reports/listCdr.do?list_all=1;Reports");
                    navList.add(";Both Report");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/listClientCdr.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:700px;" border="0" cellpadding="0" cellspacing="0">
                                <tr><td colspan="2" class="center-align bold">Origination</td><td align="center" class="center-align bold" colspan="2">Termination</td></tr>
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origin_ip">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (login_dto.getId() > 0) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                    <th>Termination  IP</th>
                                    <td><html:select property="term_ip">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (login_dto.getId() > 0) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                </tr>
                                <tr>
                                    <th>Destination</th>
                                    <td><html:text property="origin_destination" /></td>
                                    <th>Destination</th>
                                    <td><html:text property="term_destination" /></td>
                                </tr>
                                <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Year</div><div class="fl_left month">Month</div><div class="fl_left">Day</div><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                <tr>
                                    <th>From Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>To Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=recordPerPage%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="pageNo" value="<%=pNo%>" />
                                    </td>
                                </tr>
                                <tr><th>Call Type</th><td colspan="3"><html:radio property="callType" value="1" /> Successful <html:radio property="callType" value="2" /> Failed</td></tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>
                    <div class="over_flow_content" style="font-size:8pt;">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <%
                                    int dataListSize = 0;
                                    int pageNo = 1;
                                    int prevPageNo = 1;
                                    int nextPageNo = 1;
                                    int totalPages = 1;
                                    int total_time = 0;
                                    double total_origin_bill = 0;
                                    double total_term_bill = 0;
                                    dataListSize = mvtsDao.getTotalMvtsCDR(searchDTO);
                                    int perPageRecord = Integer.parseInt(recordPerPage);
                                    if (perPageRecord == 0) {
                                        perPageRecord = Constants.PER_PAGE_RECORD;
                                    }

                                    if (dataListSize > 0) {
                                        totalPages = dataListSize / perPageRecord;
                                        if (dataListSize % perPageRecord != 0) {
                                            totalPages++;
                                        }
                                    }

                                    if (request.getParameter("pageNo") != null) {
                                        pageNo = Integer.parseInt(request.getParameter("pageNo"));
                                    }
                                    int pageStart = (pageNo - 1) * perPageRecord;
                                    if (pageNo > 1) {
                                        prevPageNo = pageNo - 1;
                                    }
                                    nextPageNo = pageNo;
                                    if ((pageNo + 1) <= totalPages) {
                                        nextPageNo = pageNo + 1;
                                    }

                                    String firstLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=1&recordPerPage=" + perPageRecord + links;
                                    String prevLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + prevPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String nextLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + nextPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String lastLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + totalPages + "&recordPerPage=" + perPageRecord + links;
                                %>
                                <tr><td colspan="30" align="center"><input type="hidden" name="pageNo" value="<%=pageNo%>"/>Page&nbsp;<%=pageNo%>&nbsp;of&nbsp;<%=totalPages%></td></tr>
                                <tr class="header"><td colspan="30"  align="center"><a href="<%=firstLink%>">&laquo;First</a> | <a href="<%=prevLink%>">&laquo; Prev</a> | <a href="<%=nextLink%>">Next &raquo;</a> | <a href="<%=lastLink%>">Last &raquo;</a></td></tr>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <th colspan="7" width="30%">Origination</th>
                                    <th rowspan="2" width="10%">Termination No</th>
                                    <th colspan="7" width="30%">Termination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                    <th rowspan="2" width="5%">Disconnect Cause</th>
                                </tr>
                                <tr class="header">
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Rate Details</th>
                                    <th>Bill</th>
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Rate Details</th>
                                    <th>Bill</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    String bg_class = "odd";
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            CDRReportDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }

                                            total_time += obj.getDuration();
                                            total_origin_bill += obj.getOrigin_bill_amount();
                                            total_term_bill += obj.getTerm_bill_amount();

                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=pageStart + inc + 1%>.</td>
                                    <td><%=obj.getDialed_no()%></td>
                                    <td><%=obj.getOrigin_caller()%></td>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td><%=obj.getOrigin_ip()%></td>
                                    <td><%=obj.getOrigin_prefix()%></td>
                                    <td><%=obj.getOrigin_destination()%></td>
                                    <td><%=obj.getOrigin_rate_des()%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <td><%=obj.getTerminated_no()%></td>
                                    <td><%=obj.getTerm_caller()%></td>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <td><%=obj.getTerm_ip()%></td>
                                    <td><%=obj.getTerm_prefix()%></td>
                                    <td><%=obj.getTerm_destination()%></td>
                                    <td><%=obj.getTerm_rate_des()%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getTerm_bill_amount())%></td>
                                    <td><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=obj.getConnection_time()%></td>
                                    <td><%=obj.getPdd()%></td>
                                    <td><%=obj.getDisconnect_cause()%></td>
                                </tr>
                                <%   }%>
                                <tr><th colspan="8">Total: </th><th><%=amount_formatter.format(total_origin_bill)%></th><th colspan="7">&nbsp;</th><th><%=amount_formatter.format(total_term_bill)%></th><th align="center"><%=Utils.getTimeMMSS(total_time)%></th><td colspan="2">&nbsp;</td><td colspan="2">&nbsp;</td></tr>
                                <%   } else {%>
                                <tr><th colspan="20">No Content Found!</th></tr>
                                <% }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>