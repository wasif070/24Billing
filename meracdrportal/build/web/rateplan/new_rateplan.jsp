<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <title>24Billing :: Add Rate Plan</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                 
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("rateplan/listRateplan.do?list_all=1;Rate Plan");
                        navList.add(";Add New Rate Plan");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>                                
                    <html:form action="/rateplan/addRateplan" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Add New Rate Plan</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="RateplanForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Rate Plan Name <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:text property="rateplan_name" /><br/>
                                        <html:messages id="rateplan_name" property="rateplan_name">
                                            <bean:write name="rateplan_name"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Description</th>
                                    <td valign="top" >
                                        <html:text property="rateplan_des" /><br/>
                                        <html:messages id="rateplan_des" property="rateplan_des">
                                            <bean:write name="rateplan_des"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                        <html:select property="rateplan_status">
                                            <%
                                                for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                        <html:messages id="rateplan_status" property="rateplan_status">
                                            <bean:write name="rateplan_status"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <html:hidden property="action" value="<%=String.valueOf(Constants.ADD)%>" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>