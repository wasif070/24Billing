<%@page import="com.myapp.struts.rates.RateLoader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.rates.RateDTO,com.myapp.struts.rates.RateLoader" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <title>24Billing :: Edit Rate</title>
        <%@include file="../includes/header.jsp"%>
        <%
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            NumberFormat formatter = new DecimalFormat("00");
            long rate_id = Long.parseLong(request.getParameter("id"));
            ArrayList<RateDTO> rlist = RateLoader.getInstance().getRateDTOByRateIdList(rate_id);
            String Action = "/rates/editRate.do?id=" + rate_id;
        %>
    </head>    
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("rates/listRate.do?list_all=1&rate_id=1;Rate");
                        navList.add(";Edit Rate");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="<%=Action%>" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Edit Rate</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td align="center"  valign="bottom">
                                        <bean:write name="RateForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr><td>
                                        <fieldset><legend>Rate Information</legend>
                                            <table width="100%">
                                                <tr>
                                                    <th valign="top" >Destination Prefix <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:text readonly="true" property="rate_destination_code" /><br/>
                                                        <html:messages id="rate_destination_code" property="rate_destination_code">
                                                            <bean:write name="rate_destination_code"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Destination Name</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_destination_name" /><br/>
                                                        <html:messages id="rate_destination_name" property="rate_destination_name">
                                                            <bean:write name="rate_destination_name"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Rate (Per Min.)</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_per_min" /><br/>
                                                        <html:messages id="rate_per_min" property="rate_per_min">
                                                            <bean:write name="rate_per_min"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >First Pulse</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_first_pulse" /><br/>
                                                        <html:messages id="rate_first_pulse" property="rate_first_pulse">
                                                            <bean:write name="rate_first_pulse"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Next Pulse</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_next_pulse" /><br/>
                                                        <html:messages id="rate_next_pulse" property="rate_next_pulse">
                                                            <bean:write name="rate_next_pulse"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Grace Period</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_grace_period" /><br/>
                                                        <html:messages id="rate_grace_period" property="rate_grace_period">
                                                            <bean:write name="rate_grace_period"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Failed Period</th>
                                                    <td valign="top" >
                                                        <html:text property="rate_failed_period" /><br/>
                                                        <html:messages id="rate_failed_period" property="rate_failed_period">
                                                            <bean:write name="rate_failed_period"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="rate_status">
                                                            <%
                                                                for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                                            %>
                                                            <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                                            <%
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="rate_status" property="rate_status">
                                                            <bean:write name="rate_status"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset>
                                            <legend>Time Schedule</legend>
                                            <table width="100%" class="center-align">
                                                <tr class="bold">
                                                    <td valign="top">Day</td>
                                                    <td>From <br /><span style="width:50px;display:inline-block;font-size:8pt;">HH</span><span style="width:60px;display:inline-block;font-size:8pt;">MM</span></td>
                                                    <td>To <br /><span style="width:50px;display:inline-block;font-size:8pt;">HH</span><span style="width:60px;display:inline-block;font-size:8pt;">MM</span></td>
                                                </tr>
                                                <%
                                                    if (rlist != null) {
                                                        for (int inc = 0; inc < rlist.size(); inc++) {
                                                            RateDTO rdto = rlist.get(inc);
                                                            String sel = "";
                                                %>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="id[]" value="<%=rdto.getId()%>" />
                                                        <select name="rate_day[]" class="width_100">
                                                            <%
                                                                for (int i = 0; i < Constants.DAY_VALUE.length; i++) {
                                                                    if (rdto.getRate_sin_day() == Integer.parseInt(Constants.DAY_VALUE[i])) {
                                                                        sel = "selected='selected'";
                                                                    } else {
                                                                        sel = "";
                                                                    }
                                                            %>
                                                            <option value="<%=Constants.DAY_VALUE[i]%>" <%=sel%>><%=Constants.DAY_STRING[i]%></option>
                                                            <%
                                                                }
                                                            %>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="rate_fromhour[]" class="width_50">
                                                            <%
                                                                for (int i = 0; i < hours.size(); i++) {
                                                                    String increment = String.valueOf(i);
                                                                    String temp = formatter.format((i));
                                                                    if (rdto.getRate_sin_fromhour() == i) {
                                                                        sel = "selected='selected'";
                                                                    } else {
                                                                        sel = "";
                                                                    }
                                                            %>
                                                            <option value="<%=increment%>" <%=sel%>><%=temp%></option>
                                                            <% }%>
                                                        </select>
                                                        <select name="rate_frommin[]" class="width_50">
                                                            <%
                                                                for (int i = 0; i < minsec.size(); i++) {
                                                                    String increment = String.valueOf(i);
                                                                    String temp = formatter.format((i));
                                                                    if (rdto.getRate_sin_frommin() == i) {
                                                                        sel = "selected='selected'";
                                                                    } else {
                                                                        sel = "";
                                                                    }
                                                            %>
                                                            <option value="<%=increment%>" <%=sel%>><%=temp%></option>
                                                            <% }%>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <select name="rate_tohour[]" class="width_50">
                                                            <%
                                                                for (int i = 0; i < hours.size(); i++) {
                                                                    String increment = String.valueOf(i);
                                                                    String temp = formatter.format((i));
                                                                    if (rdto.getRate_sin_tohour() == i) {
                                                                        sel = "selected='selected'";
                                                                    } else {
                                                                        sel = "";
                                                                    }
                                                            %>
                                                            <option value="<%=increment%>" <%=sel%>><%=temp%></option>
                                                            <% }%>
                                                        </select>
                                                        <select name="rate_tomin[]" class="width_50">
                                                            <%
                                                                for (int i = 0; i < minsec.size(); i++) {
                                                                    String increment = String.valueOf(i);
                                                                    String temp = formatter.format((i));
                                                                    if (rdto.getRate_sin_tomin() == i) {
                                                                        sel = "selected='selected'";
                                                                    } else {
                                                                        sel = "";
                                                                    }
                                                            %>
                                                            <option value="<%=increment%>" <%=sel%>><%=temp%></option>
                                                            <% }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                                <% }
                                                    }%>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="center-align">
                                        <html:hidden property="rateplan_id" />
                                        <html:hidden property="action" value="<%=String.valueOf(Constants.ADD)%>" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button"  onclick="javascript:return confirm('Are you sure to update the Rate?');" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>