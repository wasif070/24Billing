<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.invoice.InvoiceDto,javax.servlet.ServletContext"%>

<html>
    <head>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRQualityDTO,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader,java.text.DecimalFormat,java.text.NumberFormat" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>

        <title>24Billing :: Invoice</title>
        <%@include file="../includes/header.jsp"%>
        <%InvoiceDto invoiceDto = (InvoiceDto) request.getSession(true).getAttribute("INVOICE_DTO");%>
        <%InvoiceDto invoiceDto1 = (InvoiceDto) request.getSession(true).getAttribute("total");%>
        <%String filePath = "C:/Users/Rajib/Documents/NetBeansProjects/meracdrportal Final 22.09.11/meracdrportal Final 22.09.11/meracdrportal/web/images/";%>
    </head>
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                      <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("invoice/listinvoice.do?list_all=1;Invoice");
                    navList.add(";Invoice Detail");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <div align="center">
                        <ul class="nav_menu_doenload">  
                            <%
                             String realContextPath = application.getRealPath("/")+"images\\";
                            %>
                            <%
                            long id=Long.parseLong(request.getSession(true).getAttribute("id").toString());
                            NumberFormat amount_formatter = new DecimalFormat("#00.000000");
                            %>
                            <li><a href="../invoice/download.do?id=<%=id%>">Download Invoice</a></li>
                        </ul>
                    </div>    
                    <div style="background-color: #000066; margin-top: 20px; height: 5px; font-size: 4pt">&nbsp;</div>
                    <div style="clear: both"></div>
                    <div style="margin: 0 auto; display: block">                                
                        <table border="0" cellpadding="0" cellspacing="1" style="text-align: left; width: 100%;font-size: 8pt;" >
                            <tr>
                                <td><img src="../images/<%=invoiceDto.getLogoName()%>" alt="logo"></img></td>
                                <td style="padding-left: 150px">
                                    <div style="font-size: 17px; clear: both; font-weight: bold"><%=invoiceDto.getCompanyName()%></div>
                                    <div style="clear:both"><%=invoiceDto.getCompanyAddress()%></div>
                                    <div style="clear:both"><%=invoiceDto.getCompanyLocation()%></div>
                                </td>
                            </tr>
                        </table>
                        <div style="clear: both"></div>
                    </div>
                    <div style="margin: 0 auto; display: block">
                        <table border="0" cellpadding="0" cellspacing="1" style="text-align: left; margin-top: 5px; width: 100%; border: 1px solid black; font-size: 8pt" >
                            <tr>
                                <td style="width: 50%;border-right: 1px solid black; ">
                                    <table border="0" cellpadding="0" cellspacing="1" style="text-align: left; margin-top: 0px; width: 100%; font-size: 8pt" >
                                        <tr>
                                            <th style="width: 50%">Client</th>
                                            <td style="width: 50%"><%=invoiceDto.getClient()%></td>
                                        </tr>
                                        <tr>
                                            <th>Client Name</th>
                                            <td><%=invoiceDto.getClientName()%></td>
                                        </tr>
                                        <tr>
                                            <th>Authorized Email</th>
                                            <td><%=invoiceDto.getClientEmail()%></td>
                                        </tr>
                                        <tr>
                                            <th>Client ID</th>
                                            <td><%=invoiceDto.getClientId()%></td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="width: 50%;padding-left: 10px">
                                    <table border="0" cellpadding="0" cellspacing="1" style="text-align: left; margin-top: 5px; width: 100%;font-size: 8pt" >
                                        <tr>
                                            <th style="width: 50%">Invoice Date</th>
                                            <td style="width: 50%"><%=invoiceDto.getInvoiceDate()%></td>
                                        </tr>
                                        <tr>
                                            <th>Invoice No.</th>
                                            <td><%=invoiceDto.getId()%></td>
                                        </tr>
                                        <tr>
                                            <th>Billing From</th>
                                            <td><%=invoiceDto.getStartDate()%></td>
                                        </tr>
                                        <tr>
                                            <th>Billing To</th>
                                            <td><%=invoiceDto.getEndDate()%></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <div style="height: 10px;">&nbsp;</div>
                    </div>
                    <div class="over_flow_content">
                        <div align="center">
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.InvoiceForm.invoiceDetail">     
                                <display:column  title="Date" sortable="true"  style="width:20%" >${data.date}</display:column>
                                <display:column  title="Prefix" sortable="true" style="width:20%" >${data.prefix}</display:column>
                                <display:column  class="right-align" title="Rate (Per Min)" sortable="true" style="width:15%" >${data.ratePerMin}</display:column>
                                <display:column  class="right-align" title="Minute" sortable="true" style="width:15%" >${data.totalMin}</display:column>
                                <display:column  class="right-align" title="Revenue" sortable="true" style="width:15%" >${data.totalTaka}</display:column>
                            </display:table>                         
                        </div>
                        <div style="margin: 0 auto; display: block">
                            <table cellspacing="0" cellpadding="0" border="0" style="width:50%;" class="reporting_table1">
                                <tbody><tr>
                                    <td align="right" style="color: #000000; font-weight: bold;padding-right: 5px;border-width: 0;">Total Minute:</td>
                                    <td align="right" width="35%" style="color: #000000; font-weight: bold;border-width: 0;">
                                     <%=invoiceDto1.getTotalMinute()%>   
                                    </td>
                                    <td align="right" width="15%" style="color: #0c66ca; font-weight: bold;">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" style="color: #000000; font-weight: bold;padding-right: 5px;border-width: 0;">Total Revenue:</td>
                                    <td align="right" width="35%" style="color: #000000; font-weight: bold;border-width: 0;">
                                        <%=amount_formatter.format(invoiceDto1.getTotalRevenue())%>
                                    </td>
                                    <td align="right" width="15%" style="color: #0c66ca; font-weight: bold;">&nbsp;</td>
                                </tr>
                            </tbody></table>
                        </div>

                        <div class="blank-height"></div>
                    </div>         
                    <div style="height: 10px"></div>
                    <div style="background-color: #000066; margin: 5px 0px 5px 0px; height: 5px;font-size: 4pt">&nbsp;</div>
                    <div style="margin-bottom: 5px; font-size: 8pt"><%=invoiceDto.getFooter()%></div>
                    <div style="clear: both;"></div>
                </div>
                <div align="center">
                    <ul class="nav_menu_doenload">                       
                        <li><a href="../invoice/download.do?id=<%=id%>">Download Invoice</a></li>
                    </ul>
                </div>    
                <div><%@include file="../includes/footer.jsp"%></div>
            </div>
            <div class="clear"></div>
        </div>
    </body>
</html>