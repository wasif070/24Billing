var gateway1={
    init:function(){
        $j('#jgateway_type').live('click',function(){         
            if(parseInt(this.value,10)==0 || parseInt(this.value,10)==-1){
               $j('#gateway_dst_port_h323').attr('disabled','disabled');
               $j('#gateway_dst_port_sip').attr('disabled','disabled');
            }
            else{
                $j('#gateway_dst_port_h323').attr('disabled',false);
               $j('#gateway_dst_port_sip').attr('disabled',false);
            }
            return true;
        });
    }
}

var customers={
    row_count:0,
    init:function(){        
        $j('.receiver').click(function(){             
            customers.customerList(this);
            return true;
        });
        $j('.customer').live('click',function(){         
            customers.customerInfo(this);
            return true;
        });
    },
    customerList:function(txt){
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'invoice/clients.jsp',
            data:{                
                text: txt.value,
                time: new Date().getMilliseconds()
            },
            success:function(html){           
                $j('.jcustomer_list').html(html);
                $j('.jcustomer_list').css('display','block');      
            },
            error:function(e,m,s){
                alert(e.responseText);
            }
        });
    },
    customerInfo:function(txt){
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'invoice/customer_info.jsp',
            data:{                
                text: txt.value,
                time: new Date().getMilliseconds()
            },
            success:function(html){           
                $j('.jcustomer_list1').html(html);
            },
            error:function(e,m,s){
                alert(e.responseText);
            }
        });
    }
}

