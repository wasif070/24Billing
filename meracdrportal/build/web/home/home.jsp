<%@page import="com.myapp.struts.util.Utils"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.home.HomeLoader"%>
<%@page import="com.myapp.struts.home.HomeDTO"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,java.text.DecimalFormat,java.text.NumberFormat" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>


<html>
    <head>
        <title>24Billing :: Home</title>
        <%@include file="../includes/header.jsp"%>
        <%
            NumberFormat formatter = new DecimalFormat("#0.00");
        %>
    </head>
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add(";");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <div class="over_flow_content">
                        <div class="display_tag_content" align="center">
                            <table class="reporting_table" border="0" cellpadding="0" cellspacing="0">
                                <tr style="color:#f00">
                                    <h2>Welcome <%=login_dto.getClientId()%></h2>
                                    <h4>Your Last 24 Hours' Client Report</h4>
                                </tr>
                                <tr>
                                    <th>Origination Client</th>
                                    <th>Total Successful Calls</th>
                                    <th>Total Call Duration</th>
                                    <th>Current Balance</th>
                                </tr>
                                <%
                                    ArrayList<HomeDTO> orgList = HomeLoader.getInstance().getOrgDTOList();
                                    for (int i = 0; i < orgList.size(); i++) {
                                        HomeDTO dto = orgList.get(i);
                                %>  
                                <tr>
                                    <td align="left"><%=dto.getOrg_client_name()%></td>
                                    <td align="right"><%=dto.getOrg_calls()%></td>
                                    <td align="center"><%=Utils.getTimeHHMMSS(dto.getOrg_duration())%></td>
                                    <td align="right"><%=formatter.format(dto.getOrg_client_balance())%></td>
                                </tr>                                    
                                <% }%> 
                            </table>
                            <div class="clear"></div>
                            <table class="reporting_table" cellpadding="0" cellspacing="0" export="false">
                                <tr>
                                    <th>Termination Client</th>
                                    <th>Total Successful Calls</th>
                                    <th>Total Call Duration</th>
                                    <th>Current Balance</th>
                                </tr>
                                <%
                                    ArrayList<HomeDTO> termlist = HomeLoader.getInstance().getTermDTOList();
                                    for (int i = 0; i < termlist.size(); i++) {
                                        HomeDTO dto = termlist.get(i);
                                %>  
                                <tr>
                                    <td align="left"><%=dto.getTerm_client_name()%></td>
                                    <td align="right"><%=dto.getTerm_calls()%></td>
                                    <td align="center"><%=Utils.getTimeHHMMSS(dto.getTerm_duration())%></td>
                                    <td align="right"><%=formatter.format(dto.getTerm_client_balance())%></td>
                                </tr>                                    
                                <% }%> 
                            </table>
                            <div class="button_area">
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
