<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.settings.SettingsLoader,com.myapp.struts.settings.SettingsDTO,java.text.NumberFormat,java.text.DecimalFormat" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>
<%
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> min = Utils.getTimeValue(60);
    ArrayList<Integer> sec = Utils.getTimeValue(60);
    NumberFormat formatter = new DecimalFormat("00");
%>
<html>
    <head>
        <title>24Billing :: Edit Settings</title>
        <div><%@include file="../includes/header.jsp"%></div>
    </head>
    <body>
        <div class="main_body">
            <div class="top" ></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right" style="width:80%">            
                <div class="pad_10 border_left">
                        <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("settings/listSettings.do?list_all=1;Settings");
                    navList.add(";Edit Settings");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/settings/editSettings" method="post">
                        <table class="input_table" style="width:700px;" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Edit Settings</h3></th></tr>
                            </thead>
                            <tbody>                  
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="SettingsForm" property="message" filter="false"/>
                                    </td>
                                </tr>   
                                <%int id = Integer.parseInt(request.getParameter("id"));
                                            if (id == 1) {%>     
                                <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Sign</div><div class="fl_left month">Hour</div><div class="fl_left">Min</div><div class="fl_left">Sec</div></td></tr>
                                <tr>
                                    <th>Time Configuration <span class="req_mark">*</span></th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="sign" styleClass="sign">
                                            <html:option value="1">+</html:option>
                                            <html:option value="2">-</html:option>
                                        </html:select>
                                        <html:select property="hour" styleClass="hour">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="min" styleClass="min">
                                            <%
                                                for (int i = 0; i < min.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="sec" styleClass="sec">
                                            <%
                                                for (int i = 0; i < sec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <%} else {%>
                                <tr>
                                    <th>Bill Calculation (Decimal Places) <span class="req_mark">*</span></th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="settingValue" styleClass="settingValue">
                                            <%
                                                for (int i = 1; i < 9; i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <%}%>
                                <tr> 
                                    <th>&nbsp;</th>
                                    <td>
                                        <html:hidden property="id" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" onclick="javascript:return confirm('Are you sure to want Update the Settings?');" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>
    </body>
</html>