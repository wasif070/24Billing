<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html>
    <head>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.rateplan.RateplanDTO,com.myapp.struts.rateplan.RateplanLoader" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <title>24Billing :: Edit Profile</title>
        <div><%@include file="../includes/header.jsp"%></div>
        <%
            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
        %>
        <%
            ArrayList<RateplanDTO> rateplanDto = RateplanLoader.getInstance().getRateplanDTOList();
        %>
    </head>
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">

                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";");
                        navList.add(";Edit Profile");
                    %>
                    <%= navigation.Navigation_client.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <%=msg%>
                    <html:form action="/clients/clientEdit" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Edit Profile</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <fieldset><legend>Client Information</legend>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2" align="center"  valign="bottom">
                                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client ID <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:text property="client_id" readonly="true" /><br/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Password <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:password property="client_password" /><br/>
                                                        <html:messages id="client_password" property="client_password">
                                                            <bean:write name="client_password"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Retype Password <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:password property="retypePassword" /><br/>
                                                        <html:messages id="retypePassword" property="retypePassword">
                                                            <bean:write name="retypePassword"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Name </th>
                                                    <td valign="top" >
                                                        <html:text property="client_name" /><br/>
                                                        <html:messages id="client_name" property="client_name">
                                                            <bean:write name="client_name"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Email </th>
                                                    <td valign="top" >
                                                        <html:text property="client_email" /><br/>
                                                        <html:messages id="client_email" property="client_email">
                                                            <bean:write name="client_email"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Type <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="client_type" disabled="true">
                                                            <html:option value="-1">Select Client Type</html:option>
                                                            <%
                                                                for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                                            %>
                                                            <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                                            <%
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="client_type" property="client_type">
                                                            <bean:write name="client_type"  filter="false"/>
                                                        </html:messages>
                                                        <html:hidden property="client_type" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select disabled="true" property="client_status">
                                                            <%
                                                                for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                                            %>
                                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                                            <%
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="client_status" property="client_status">
                                                            <bean:write name="client_status"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Rate Plan <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select disabled="true" property="rateplan_id">
                                                            <html:option value="">Select Rate Plan</html:option>
                                                            <%
                                                                if (rateplanDto != null && rateplanDto.size() > 0) {
                                                                    for (int i = 0; i < rateplanDto.size(); i++) {
                                                                        RateplanDTO rdto = new RateplanDTO();
                                                                        rdto = rateplanDto.get(i);
                                                            %>
                                                            <html:option value="<%=String.valueOf(rdto.getRateplan_id())%>"><%=rdto.getRateplan_name()%></html:option>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="rateplan_id" property="rateplan_id">
                                                            <bean:write name="rateplan_id"  filter="false"/>
                                                        </html:messages>
                                                        <html:hidden property="rateplan_id" />
                                                    </td>
                                                </tr>  
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset><legend>Incoming Prefix</legend>
                                            <table width="100%">
                                                <tr>
                                                    <th valign="top" style="width:35%;">Prefix</th>
                                                    <td valign="top">
                                                        <html:text property="prefix" disabled="true" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <html:hidden property="incoming_prefix" value="" />
                                        <html:hidden property="incoming_to" value="" />
                                        <html:hidden property="outgoing_prefix" value="" />
                                        <html:hidden property="outgoing_to" value="" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Update" onclick="javascript:return confirm('Are you sure to update information?');" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>        
    </body>
</html>