<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>

<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@taglib uri="http://displaytag.sf.net" prefix="display" %>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }
%>
<html>
    <head>
        <title>24Billing :: Client List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div class="top"></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                     <%
                   java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                   navList.add("clients/listClient.do?list_all=1;Clients");
                   navList.add(";");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/clients/listClient.do" method="post" >
                        <div class="half-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Client ID</th>
                                    <td><html:text property="client_id" /></td>
                                </tr>
                                <tr>
                                    <th>Client Type</th>
                                    <td>
                                        <html:select property="client_type">
                                            <html:option value="-1">Select Client Type</html:option>
                                            <%
                                                for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                            %>
                                            <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                            <%
                                                }
                                            %>
                                        </html:select><br/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="client_status">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                             for(int i=0;i<Constants.LIVE_STATUS_VALUE.length;i++)
                                             {
                                            %>
                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                            <%
                                             }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <div class="full-div">
                                            <html:submit styleClass="search-button" value="Search" />
                                            <html:reset styleClass="search-button" value="Reset" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                        <html:form action="/clients/multipleClient" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                            <div class="display_tag_content" align="center">
                                <%=msg%>
                                <bean:write name="ClientForm" property="message" filter="false"/>
                                <div class="jerror_messge"></div>
                                <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                                 <div class="over_flow_content_list">
                                <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.ClientForm.clientList"  pagesize="<%=recordPerPage%>" >
                                    <display:setProperty name="paging.banner.item_name" value="Client" /> 
                                    <display:setProperty name="paging.banner.items_name" value="Clients" />
                                    <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='a Client' />" style="width:3%">
                                        <input type="checkbox" name="selectedIDs[]" value="${data.id}" class="select_id"/>
                                    </display:column>
                                    <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                        <script type="text/javascript">
                                            document.write(++count+".");
                                        </script>
                                    </display:column>
                                    <display:column  title="Client ID" sortProperty="client_id" sortable="true"  style="width:12%" >
                                        ${data.client_id}
                                    </display:column>
                                    <display:column title="Incoming Prefix" class="center-align" style="width:10%">
                                        ${data.prefix}
                                    </display:column>
                                    <display:column  property="client_name" class="" title="Client Name" sortable="true" style="width:13%" />
                                    <display:column  property="client_type_name" class="center-align" title="Client Type" sortable="true" style="width:10%" />
                                    <display:column  property="rateplan_name" class="" title="Rate Plan" sortable="true" style="width:11%"/>
                                    <display:column  property="client_call_limit" class="" title="Current Call Limit" sortable="true" style="width:8%"/>
                                    <display:column  property="client_balance" class="right-align" title="Balance" sortable="true" style="width:10%" />
                                    <display:column  property="client_status_name" class="center-align" title="Status" sortable="true" style="width:8%" />
                                    <display:column class="center-align" title="Task" style="width:7%;" >
                                        <a href="../clients/getClient.do?id=${data.id}" class="edit" title="Change"></a>
                                        <a href="../clients/deleteClient.do?id=${data.id}" onclick="javascript:return confirm('Are you sure to want delete the Client?');" class="drop" title="Delete"></a>
                                    </display:column>
                                </display:table>
                                 </div>
                                <%
                                    request.removeAttribute(Constants.USER_ID_LIST);
                                    request.getSession(true).removeAttribute(Constants.MESSAGE);
                                %>
                                <div class="button_area">
                                    <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                    <html:submit property="activateBtn" styleClass="custom-button jmultipebtn" value="Activate" />
                                    <html:submit property="inactiveBtn" styleClass="custom-button jmultipebtn" value="Inactive" />
                                    <html:submit property="blockBtn" styleClass="custom-button jmultipebtn" value="Block" />
                                    <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                                </div>
                            </div>
                            <div class="blank-height"></div>
                        </html:form>                
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>