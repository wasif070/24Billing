DROP TABLE IF EXISTS `carrier_cdr`;
CREATE TABLE `carrier_cdr` (
  `cdr_date` timestamp NULL DEFAULT NULL,
  `dialed_no` varchar(50) NOT NULL DEFAULT '',
  `origin_ip` varchar(15) NOT NULL DEFAULT '',
  `terminated_no` varchar(100) DEFAULT NULL,
  `term_ip` varchar(15) DEFAULT NULL,
  `duration` int(11) NOT NULL,
  `connection_time` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`cdr_date`, `dialed_no`, `terminated_no`, `duration`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;