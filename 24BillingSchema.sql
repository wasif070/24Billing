
DROP TABLE IF EXISTS client_transactions;
CREATE TABLE client_transactions (
  transaction_id int(11) NOT NULL AUTO_INCREMENT,
  client_id int(11) NOT NULL,
  transaction_type tinyint(1) NOT NULL DEFAULT '1',
  transaction_recharge decimal(18,4) NOT NULL DEFAULT '0.0000',
  transaction_return decimal(18,4) NOT NULL DEFAULT '0.0000',
  transaction_receive decimal(18,4) NOT NULL DEFAULT '0.0000',
  transaction_des varchar(255) DEFAULT NULL,
  transaction_date decimal(18,0) NOT NULL,
  user_id int(11) NOT NULL,
  PRIMARY KEY (transaction_id)
);
DROP TABLE IF EXISTS clients;
CREATE TABLE clients (
  id int(11) NOT NULL AUTO_INCREMENT,
  client_id varchar(100) NOT NULL DEFAULT '',
  rateplan_id int(11) NOT NULL,
  client_password varchar(50) NOT NULL,
  client_name varchar(100) NOT NULL DEFAULT '',
  client_email varchar(100) NOT NULL,
  client_type tinyint(2) NOT NULL DEFAULT '0',
  client_created decimal(18,0) NOT NULL,
  client_status tinyint(1) NOT NULL DEFAULT '1',
  client_credit_limit decimal(18,4) NOT NULL DEFAULT '0.0000',
  client_balance decimal(18,4) NOT NULL DEFAULT '0.0000',
  client_delete tinyint(1) NOT NULL DEFAULT '0',
  prefix varchar(32) NOT NULL DEFAULT '',
  client_call_limit int(11) NOT NULL DEFAULT '0',
  inserted_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);
DROP TABLE IF EXISTS current_call;
CREATE TABLE current_call (
  id int(11) NOT NULL AUTO_INCREMENT,
  conf_id varchar(100) NOT NULL,
  dial_no_in varchar(50) NOT NULL,
  org_client_id int(11) DEFAULT NULL,
  org_gateway_ip varchar(50) NOT NULL,
  org_rateplan_id int(11) DEFAULT NULL,
  org_prefix varchar(50) DEFAULT NULL,
  org_rate_per_min float DEFAULT NULL,
  dial_no_out varchar(50) NOT NULL,
  term_client_id int(11) DEFAULT NULL,
  term_gateway_ip varchar(50) NOT NULL,
  term_rateplan_id int(11) DEFAULT NULL,
  term_prefix varchar(50) DEFAULT NULL,
  term_rate_per_min float DEFAULT NULL,
  setup_time varchar(100) DEFAULT NULL,
  connect_time varchar(100) DEFAULT NULL,
  disconnect_time varchar(100) DEFAULT NULL,
  inserted_time decimal(18,0) DEFAULT '0',
  status int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY conf_id (conf_id)
);
DROP TABLE IF EXISTS flamma_cdr;
CREATE TABLE flamma_cdr (
  cdr_id bigint(18) NOT NULL AUTO_INCREMENT,
  cdr_date timestamp NULL DEFAULT NULL,
  dialed_no varchar(50) NOT NULL DEFAULT '',
  origin_caller varchar(50) DEFAULT NULL,
  origin_client_id int(11) NOT NULL,
  origin_client_name varchar(100) DEFAULT NULL,
  origin_ip varchar(15) NOT NULL DEFAULT '',
  origin_prefix varchar(32) DEFAULT NULL,
  origin_destination varchar(50) DEFAULT NULL,
  origin_rate_id int(11) NOT NULL DEFAULT '0',
  origin_rate_des varchar(100) DEFAULT NULL,
  origin_rate decimal(10,4) NOT NULL DEFAULT '0.0000',
  origin_bill_amount decimal(18,8) NOT NULL DEFAULT '0.00000000',
  terminated_no varchar(100) DEFAULT NULL,
  term_caller varchar(50) DEFAULT NULL,
  term_client_id int(11) NOT NULL,
  term_client_name varchar(100) DEFAULT NULL,
  term_ip varchar(15) DEFAULT NULL,
  term_prefix varchar(32) DEFAULT NULL,
  term_destination varchar(100) DEFAULT NULL,
  term_rate_id int(11) NOT NULL DEFAULT '0',
  term_rate_des varchar(100) DEFAULT NULL,
  term_rate decimal(10,4) NOT NULL DEFAULT '0.0000',
  term_bill_amount decimal(18,8) NOT NULL DEFAULT '0.00000000',
  duration int(11) NOT NULL,
  connection_time timestamp NULL DEFAULT NULL,
  pdd decimal(10,4) NOT NULL DEFAULT '0.0000',
  disconnect_desc varchar(150) DEFAULT NULL,
  conf_id varchar(50) DEFAULT NULL,
  PRIMARY KEY (cdr_id),
  UNIQUE KEY conf_id (conf_id)
);
DROP TABLE IF EXISTS gateway;
CREATE TABLE gateway (
  id int(11) NOT NULL AUTO_INCREMENT,
  gateway_name varchar(100) NOT NULL,
  gateway_ip varchar(15) NOT NULL,
  gateway_status tinyint(1) NOT NULL DEFAULT '0',
  gateway_type tinyint(1) NOT NULL DEFAULT '0',
  client_id int(11) NOT NULL,
  gateway_delete tinyint(1) NOT NULL DEFAULT '0',
  inserted_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id)
);
DROP TABLE IF EXISTS invoice;
CREATE TABLE invoice (
  id int(11) NOT NULL AUTO_INCREMENT,
  cid int(11) NOT NULL,
  client_id varchar(100) DEFAULT NULL,
  client_name varchar(100) DEFAULT NULL,
  client_email varchar(100) DEFAULT NULL,
  company_name varchar(100) DEFAULT NULL,
  company_address varchar(255) DEFAULT NULL,
  company_location varchar(32) DEFAULT NULL,
  logo blob,
  footer text,
  start_date varchar(30) NOT NULL,
  end_date varchar(30) NOT NULL,
  invoice_date varchar(30) DEFAULT NULL,
  PRIMARY KEY (id)
);
DROP TABLE IF EXISTS mvts_rateplan;
CREATE TABLE mvts_rateplan (
  rateplan_id int(11) NOT NULL AUTO_INCREMENT,
  rateplan_name varchar(100) NOT NULL,
  rateplan_des varchar(100) NOT NULL,
  rateplan_status tinyint(1) NOT NULL DEFAULT '1',
  rateplan_delete tinyint(1) NOT NULL DEFAULT '0',
  rateplan_create_date decimal(18,0) NOT NULL,
  user_id int(11) NOT NULL,
  PRIMARY KEY (rateplan_id)
);
DROP TABLE IF EXISTS mvts_rates;
CREATE TABLE mvts_rates (
  id int(11) NOT NULL AUTO_INCREMENT,
  rate_id int(11) NOT NULL DEFAULT '0',
  rateplan_id int(11) NOT NULL DEFAULT '0',
  rate_destination_code varchar(32) NOT NULL,
  rate_destination_name varchar(50) NOT NULL,
  rate_per_min decimal(10,4) NOT NULL DEFAULT '0.0000',
  rate_first_pulse tinyint(2) NOT NULL DEFAULT '0',
  rate_next_pulse tinyint(2) NOT NULL DEFAULT '0',
  rate_grace_period tinyint(2) NOT NULL DEFAULT '0',
  rate_failed_period tinyint(2) NOT NULL DEFAULT '0',
  rate_day int(1) NOT NULL DEFAULT '0',
  rate_from_hour tinyint(2) NOT NULL DEFAULT '0',
  rate_from_min tinyint(2) NOT NULL DEFAULT '0',
  rate_to_hour tinyint(2) NOT NULL DEFAULT '0',
  rate_to_min tinyint(2) NOT NULL DEFAULT '0',
  rate_created_date decimal(18,0) NOT NULL,
  rate_delete_time decimal(18,0) DEFAULT '0',
  delete_time decimal(18,0) DEFAULT '0',
  rate_status tinyint(1) NOT NULL DEFAULT '0',
  rate_delete tinyint(1) NOT NULL DEFAULT '0',
  user_id int(11) NOT NULL,
  PRIMARY KEY (id)
);
DROP TABLE IF EXISTS settings;
CREATE TABLE settings (
  id int(4) NOT NULL AUTO_INCREMENT,
  param varchar(100) NOT NULL DEFAULT '',
  value varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (id)
)AUTO_INCREMENT=3;
INSERT INTO settings VALUES (1,'TIME_ZONE','+06:00:00'),(2,'DECIMAL_PLACES','6');


DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id int(11) NOT NULL AUTO_INCREMENT,
  user_id varchar(50) NOT NULL,
  user_password varchar(32) NOT NULL,
  user_full_name varchar(100) DEFAULT NULL,
  user_status tinyint(1) NOT NULL DEFAULT '1',
  user_delete tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
);
INSERT INTO users VALUES (-1,'admin','admin24','superadmin',0,0);