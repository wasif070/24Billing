<%@page import="com.myapp.struts.util.AppConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

        <%
            CDRReportDAO mvtsDao = new CDRReportDAO();
            String links = "";
            boolean disOrigin = true;
            boolean disTerm = true;
            int pNo = 1;
            int recordPerPage = 10;

            String origin_client_id = (String) request.getParameter("oid");
            if (origin_client_id == null) {
                origin_client_id = "";
                disOrigin = true;
            } else {
                disOrigin = false;
                links += "&oid=" + origin_client_id;
            }

            String term_client_id = (String) request.getParameter("tid");
            if (term_client_id == null) {
                term_client_id = "";
                disTerm = true;
            } else {
                disTerm = false;
                links += "&tid=" + term_client_id;
            }

            if (request.getParameter("recordPerPage") != null) {
                boolean status = Utils.IntegerValidation(request.getParameter("recordPerPage"));
                if (status == true) {
                    recordPerPage = Integer.parseInt(request.getParameter("recordPerPage"));
                }
            }

            if (request.getParameter("pageNo") != null) {
                boolean status = Utils.IntegerValidation(request.getParameter("pageNo"));
                if (status == true) {
                    pNo = Integer.parseInt(request.getParameter("pageNo"));
                }
            }


            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRReportDTO> mvtscdrList = (ArrayList<CDRReportDTO>) request.getSession(true).getAttribute("CDRReportDTO");
            CDRReportDTO searchDTO = (CDRReportDTO) request.getSession(true).getAttribute("SearchCdrDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());

            ArrayList<ClientDTO> originClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.ORIGINATION, login_dto.getOwn_id());
            ArrayList<ClientDTO> termClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.TERMINATION, login_dto.getOwn_id());
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: CDR List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right border_left" style="width:80%">
                <div class="">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";CDR Report");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/listCdr.do" method="post" >
                        <div>
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td class="center-align bold" style="color: #2663E0; text-align: right">Origination</td>
                                    <th>Client ID</th>
                                    <td>
                                        <html:select property="origin_client_id" styleClass="joriginId">
                                            <html:option value="-1">Select Client</html:option>
                                            <%
                                                if (originClientList != null && originClientList.size() > 0) {
                                                    int size = originClientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) originClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Origination IP</th>
                                    <td><html:select property="origin_ip" disabled="<%=disOrigin%>" styleClass="joriginIp">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (!origin_client_id.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(origin_client_id));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Destination Name/Prefix</th>
                                    <td colspan="2"><html:text property="origin_destination" styleClass="jorigindest" disabled="<%=disOrigin%>" /></td>
                                </tr>
                                <% if (login_dto.isUser() || login_dto.getSuperUser() && login_dto.getClient_level() < 0) {%>
                                <tr>
                                    <td align="center" class="center-align bold" style="color: #2663E0; text-align: right ">Termination</td>
                                    <th>Client ID</th>
                                    <td><html:select property="term_client_id" styleClass="jtermId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (termClientList != null && termClientList.size() > 0) {
                                                    int size = termClientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) termClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Termination  IP</th>
                                    <td><html:select property="term_ip" disabled="<%=disTerm%>" styleClass="jtermIp">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (!term_client_id.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(term_client_id));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select></td>
                                    <th>Destination Name/Prefix</th>
                                    <td colspan="2"><html:text property="term_destination" styleClass="jtermdest" disabled="<%=disTerm%>" /></td>
                                </tr>
                                <%}%>
                                <tr>
                                    <th>Start Time</th>
                                    <td colspan="3" class="selopt">
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="fromYear" styleClass="disable" styleId="fromYear">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromMonth" styleClass="month disable" styleId="fromMonth">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromDay" styleClass="disable">
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="fromHour" styleClass="disable" styleId="fromHour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));

                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromMin" styleClass="disable" styleId="fromMin">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromSec" styleClass="disable" styleId="fromSec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <th>End Time</th>
                                    <td colspan="3" class="selopt">   
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="toYear" styleClass="disable" styleId="toYear">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toMonth" styleClass="month disable" styleId="toMonth">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toDay" styleClass="disable">
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="toHour" styleClass="disable" styleId="toHour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toMin" styleClass="disable" styleId="toMin">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>  
                                                </td><td>
                                                    <html:select property="toSec" styleClass="disable" styleId="toSec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Caller</th>
                                    <td colspan="3" class="selopt">
                                        <html:text property="origin_caller" />
                                    </td>
                                    <th>Terminated No</th>
                                    <td colspan="3" class="selopt">      
                                        <html:text property="terminated_no"/>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Report Type</th>
                                    <td colspan="3"><html:radio property="reportingType" value="3" /> Summary By Parent <html:radio property="reportingType" value="1" /> Summary <html:radio property="reportingType" value="2" /> Details </td>
                                    <th>Call Type</th>
                                    <td colspan="3"><html:radio property="callType" value="1" /> Successful <html:radio property="callType" value="2" /> Failed</td>                                    
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td colspan="3">
                                        <html:select property="recordPerPage">
                                            <html:option value="10">10</html:option>
                                            <html:option value="25">25</html:option>
                                            <html:option value="50">50</html:option>
                                            <html:option value="100">100</html:option>
                                            <html:option value="250">250</html:option>
                                            <html:option value="500">500</html:option>
                                        </html:select>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td colspan="3">
                                        <input type="text" name="pageNo" style="width: 50px" value="<%=String.valueOf(pNo)%>" />
                                    </td>                                    
                                </tr>
                                <tr>
                                    <td colspan="8" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>                  
                    <div style="text-align: center;">
                        <table cellspacing="1" cellpadding="2" align="center">
                            <thead>
                                <%
                                    int dataListSize = 0;
                                    int pageNo = 1;
                                    int prevPageNo = 1;
                                    int nextPageNo = 1;
                                    int totalPages = 1;
                                    long total_time = 0;
                                    double total_origin_bill = 0;
                                    double total_term_bill = 0;
                                    dataListSize = Integer.parseInt(request.getSession(true).getAttribute("CDR_COUNT").toString());
                                    int perPageRecord = recordPerPage;
                                    if (perPageRecord == 0) {
                                        perPageRecord = Constants.PER_PAGE_RECORD;
                                    }

                                    if (dataListSize > 0) {
                                        totalPages = dataListSize / perPageRecord;
                                        if (dataListSize % perPageRecord != 0) {
                                            totalPages++;
                                        }
                                    }

                                    if (request.getParameter("pageNo") != null) {
                                        pageNo = Integer.parseInt(request.getParameter("pageNo"));
                                    }
                                    int pageStart = (pageNo - 1) * perPageRecord;
                                    if (pageNo > 1) {
                                        prevPageNo = pageNo - 1;
                                    }
                                    nextPageNo = pageNo;
                                    if ((pageNo + 1) <= totalPages) {
                                        nextPageNo = pageNo + 1;
                                    }

                                    String firstLink = request.getContextPath() + "/reports/listCdr.do?pageNo=1&recordPerPage=" + perPageRecord + links;
                                    String prevLink = request.getContextPath() + "/reports/listCdr.do?pageNo=" + prevPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String nextLink = request.getContextPath() + "/reports/listCdr.do?pageNo=" + nextPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String lastLink = request.getContextPath() + "/reports/listCdr.do?pageNo=" + totalPages + "&recordPerPage=" + perPageRecord + links;
                                %>
                                <tr><td colspan="30" align="center"><input type="hidden" name="pageNo" value="<%=pageNo%>"/>Total <%=dataListSize%> records found. Displaying Page&nbsp;<%=pageNo%>&nbsp;of&nbsp;<%=totalPages%></td></tr>
                                <tr><td colspan="30" align="center"><a href="<%=firstLink%>">&laquo;First</a> | <a href="<%=prevLink%>">&laquo; Prev</a> | <a href="<%=nextLink%>">Next &raquo;</a> | <a href="<%=lastLink%>">Last &raquo;</a></td></tr>
                            </thead>
                        </table>
                    </div>
                    <div class="over_flow_content" style="font-size:8pt;">
                        <table class="content_table" cellspacing="1" cellpadding="2"> 
                            <thead>
                                <tr class="header">
                                    <% if (searchDTO != null && searchDTO.getReportingType() == AppConstants.DETAILS_REPORT) {%>
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <%if (login_dto.getClientType() != 0) {%>
                                    <th rowspan="2" width="10%">Dialed No</th>
                                    <%}%>
                                    <th colspan="7" width="30%">Origination</th>
                                    <%if (login_dto.getClientType() != 0) {%>
                                    <th rowspan="2" width="10%">Termination No</th>
                                    <%}%>
                                    <% if (login_dto.getClient_level() != 2) {%>
                                    <th colspan="7" width="30%">Termination</th>
                                    <%} else {%>
                                    <th colspan="6" width="30%">Termination</th>
                                    <%}%>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                    <th rowspan="2" width="5%">Disconnect Cause</th>                                    
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <th rowspan="2" width="10%">Auth Error Cause</th>
                                    <%}
                                    } else {%>
                                    <th width="5%">Nr.</th>
                                    <th width="10%">Origin Client</th>
                                    <th width="10%">Origin IP</th>
                                    <% if (searchDTO.getCallType() != 2) {%>
                                    <th width="10%">Origin Bill</th>
                                    <%if (login_dto.getOwn_id() > 0) {%>
                                    <th width="10%">Parent Bill</th>
                                    <th width="10%">Profit</th>
                                    <%}
                                        }%>
                                    <%%>    
                                    <th width="10%">Term. Client</th>
                                    <% if (login_dto.getClient_level() != 2) {%>
                                    <th width="10%">Termination IP</th> 
                                    <%}%>
                                    <% if (searchDTO.getCallType() != 2) {%>
                                    <th width="10%">Term Bill</th>
                                    <th width="10%">Duration (Min:Sec)</th>                                    
                                    <%}%>
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <th width="10%">Fail/Total</th> 
                                    <%}%>
                                    <%}%>                                    
                                </tr>
                                <% if (searchDTO != null && searchDTO.getReportingType() == AppConstants.DETAILS_REPORT) {%>
                                <tr class="header">
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Rate Details</th>
                                    <th>Bill</th>
                                    <th>Caller</th>
                                    <th>Client</th>
                                    <% if (login_dto.getClient_level() != 2) {%>
                                    <th>IP</th>
                                    <%}%>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Rate Details</th>
                                    <th>Bill</th>
                                </tr>
                                <%}%>
                            </thead>                        
                            <tbody>
                                <%
                                    com.myapp.struts.settings.SettingsDTO settingsDTO = com.myapp.struts.settings.SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
                                    String timeZone = settingsDTO.getSettingValue();
                                    String bg_class = "odd";
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        CDRReportDTO obj = mvtscdrList.get(mvtscdrList.size() - 1);
                                        total_time = obj.getTotal_duration();
                                        total_origin_bill = obj.getTotal_origin_bill_amount();
                                        total_term_bill = obj.getTotal_term_bill_amount();
                                        double totalParentBillAmount = 0;
                                        for (int inc = 0; inc < mvtscdrList.size() - 1; inc++) {
                                            obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            if (searchDTO != null && searchDTO.getReportingType() == AppConstants.DETAILS_REPORT) {
                                                String connectionTime = Utils.ToDateDDMMYYYYhhmmss(Utils.getTimeLong(timeZone) + Utils.getDateLong(obj.getConnection_time()));

                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=pageStart + inc + 1%>.</td>
                                    <%if (login_dto.getClientType() != 0) {%>
                                    <td><%=obj.getDialed_no()%></td>
                                    <%}%>
                                    <td><%=obj.getOrigin_caller()%></td>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td align="center"><%=obj.getOrigin_ip()%></td>
                                    <td><%=obj.getOrigin_prefix()%></td>
                                    <td><%=obj.getOrigin_destination()%></td>
                                    <td><%=obj.getOrigin_rate_des()%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <%if (login_dto.getClientType() != 0) {%>
                                    <td><%=obj.getTerminated_no()%></td>
                                    <%}%>
                                    <td><%=obj.getTerm_caller()%></td>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <% if (login_dto.getClient_level() != 2) {%>
                                    <td align="center"><%=obj.getTerm_ip()%></td>
                                    <%}%>
                                    <td><%=obj.getTerm_prefix()%></td>
                                    <td><%=obj.getTerm_destination()%></td>
                                    <td><%=obj.getTerm_rate_des()%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getTerm_bill_amount())%></td>
                                    <td align="right"><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=connectionTime%></td>
                                    <td><%=obj.getPdd()%></td>
                                    <td><%=obj.getDisconnect_cause()%></td>
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <td><%=obj.getAuth_error_cause()%></td>
                                    <%}%>
                                </tr>
                                <%} else {%>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=pageStart + inc + 1%>.</td>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td align="center"><%=obj.getOrigin_ip()%></td>
                                    <% if (searchDTO.getCallType() != 2) {%>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <%if (login_dto.getOwn_id() > 0) {%>
                                    <td align="right"><%=amount_formatter.format(obj.getParentBillAmount())%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getProfit())%></td>
                                    <%}
                                        }%>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <% if (login_dto.getClient_level() != 2) {%>
                                    <td align="center"><%=obj.getTerm_ip()%></td>
                                    <%}%>
                                    <% if (searchDTO.getCallType() != 2) {%>
                                    <td align="right"><%=amount_formatter.format(obj.getTerm_bill_amount())%></td>
                                    <td align="right"><%=Utils.getMMSS(obj.getDuration())%></td>                                       
                                    <%}%>
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <td align="center"><%=obj.getTotalFailure()%>/<%=obj.getTotalCall()%></td>
                                    <%}%>
                                </tr>
                                <%  if (login_dto.getOwn_id() > 0) {
                                                totalParentBillAmount = totalParentBillAmount + obj.getParentBillAmount();
                                            }
                                        }
                                    }
                                    if (searchDTO != null && searchDTO.getReportingType() == AppConstants.DETAILS_REPORT) {
                                        if (login_dto.getClient_level() != 2) {%>          
                                <tr style="color: #CC0001"><th colspan="8">Total: </th><th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th><th colspan="7">&nbsp;</th><th style="text-align: right"><%=amount_formatter.format(total_term_bill)%></th><th style="text-align: right"><%=Utils.getMMSS(total_time)%></th><th colspan="2">&nbsp;</th><th colspan="2">&nbsp;</th></tr>
                                <%} else {%>
                                <tr style="color: #CC0001"><th colspan="7">Total: </th><th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th><th colspan="6">&nbsp;</th><th style="text-align: right" colspan="2"><%=amount_formatter.format(total_term_bill)%></th><th style="text-align: right"><%=Utils.getMMSS(total_time)%></th><th colspan="2">&nbsp;</th></tr>
                                <%}
                                } else {
                                    if (login_dto.getClient_level() != 2 && searchDTO.getCallType() == 1 && login_dto.getOwn_id() > 0) {%>
                                <tr style="color: #CC0001"><th colspan="3">Total: </th><th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th><th><%=amount_formatter.format(totalParentBillAmount)%></th><th><%=amount_formatter.format(total_origin_bill - totalParentBillAmount)%></th><th colspan="3" style="text-align: right"><%=amount_formatter.format(total_term_bill)%></th><th style="text-align: right"><%=Utils.getMMSS(total_time)%></th></tr>
                                <%} else if (searchDTO.getCallType() == 1 && login_dto.getOwn_id() > 0) {%>
                                <tr style="color: #CC0001"><th colspan="3">Total: </th><th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th><th><%=amount_formatter.format(totalParentBillAmount)%></th><th><%=amount_formatter.format(total_origin_bill - totalParentBillAmount)%></th><th colspan="2" style="text-align: right"><%=amount_formatter.format(total_term_bill)%></th><th style="text-align: right"><%=Utils.getMMSS(total_time)%></th></tr>     
                                <%} else if (searchDTO.getCallType() == 1 && login_dto.getOwn_id() < 0) {%>
                                <tr style="color: #CC0001"><th colspan="3">Total: </th><th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th><th></th><th></th><th style="text-align: right"><%=amount_formatter.format(total_term_bill)%></th><th style="text-align: right"><%=Utils.getMMSS(total_time)%></th></tr>     
                                <%}
                                    }
                                } else {%>
                                <tr><th colspan="30">No Content Found!</th></tr>
                                <% }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<script language="javascript">
    $j(document).ready(function(){
        clients.init();
        clients.load_gatewayip($j(".joriginId").val());
        $j('.joriginIp').attr('disabled',false);
        $j('.jorigindest').attr('disabled',false);
        
        clients.load_termgatewayip($j(".jtermId").val());
        $j('.jtermIp').attr('disabled',false);
        $j('.jtermdest').attr('disabled',false);
    });
</script>