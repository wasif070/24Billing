<%@page import="com.myapp.struts.reports.CDRReportForm"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRQualityDTO,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>
        <%
            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } else {
                msg = "<div class='error' style='width:700px;'>" + msg + "</div>";
            }

            String summaryBy = request.getParameter("sby") != null ? request.getParameter("sby").toString() : "";
            if (summaryBy != null && summaryBy.length() > 0) {
                summaryBy = summaryBy.substring(0, summaryBy.length() - 1);
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRQualityDTO> mvtscdrList = (ArrayList<CDRQualityDTO>) request.getSession(true).getAttribute("CDRReportDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());

            int total_time = 0;
            int total_success = 0;
            int total_fail = 0;
            int total_call = 0;
            double total_origin_bill = 0;
            String bg_class = "odd";
            NumberFormat pddformatter = new DecimalFormat("#00.00");

        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: CDR Quality Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right" style="width:80%">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";");
                        navList.add(";Quality Report");
                    %>
                    <%= navigation.Navigation_client.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/cQualityCdr.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:100%;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origin_ip" >
                                            <html:option value="">All</html:option>
                                            <%
                                                if (login_dto.getId() > 0) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Destination</th>
                                    <td><html:text property="origin_destination" /></td>
                                </tr>
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromSec" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>To Date</th>
                                    <td class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toSec" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Summary By</th>
                                    <td>
                                        <%
                                            for (int i = 0; i < 2; i++) {
                                        %>
                                        <input type="checkbox" name="summaryBy[]" <% if (summaryBy.contains(Constants.SUMMARY_VALUE[i])) {%> checked="checked" <% }%> value="<%=Constants.SUMMARY_VALUE[i]%>" /><%=Constants.SUMMARY_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <th>Unit Time </th>
                                    <td>
                                        <%
                                            for (int i = 0; i < Constants.UNIT_TIME_VALUE.length; i++) {
                                        %>
                                        <html:radio property="summaryType" value="<%=Constants.UNIT_TIME_VALUE[i]%>" /><%=Constants.UNIT_TIME_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="over_flow_content">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="3%">Nr.</th>
                                    <th rowspan="2" width="10%">Time</th>
                                    <%if (summaryBy.contains("0") && !summaryBy.contains("0,1")) {%>
                                    <th colspan="3" width="22%">Origination</th>
                                    <%} else if (summaryBy.contains("1") && !summaryBy.contains("0,1")) {%>
                                    <th colspan="2" width="22%">Origination</th>
                                    <%} else if (summaryBy.contains("0,1")) {%>
                                    <th colspan="4" width="22%">Origination</th>
                                    <%} else {%>
                                    <th colspan="1" width="10%">Origination</th>
                                    <%}%>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Success+Fail=Total</th>
                                    <th rowspan="2" width="5%">ASR(%)</th>
                                    <th rowspan="2" width="5%">ACD (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Avg PDD(Sec)</th>
                                </tr>
                                <tr class="header">
                                    <%if (summaryBy.contains("0")) {%>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <%}%>
                                    <%if (summaryBy.contains("1")) {%>
                                    <th>Destination</th>
                                    <%}%>
                                    <th>Bill</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            double avg_pdd = 0;
                                            int acd = 0;
                                            CDRQualityDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getDuration();
                                            total_success += obj.getTotal_success();
                                            total_fail += obj.getTotal_fail();
                                            total_call = obj.getTotal_success() + obj.getTotal_fail();
                                            avg_pdd = (double) (obj.getAvg_pdd());
                                            if (obj.getTotal_success() > 0) {
                                                acd = (int) (obj.getDuration() / obj.getTotal_success());
                                            }
                                            float success_rate = 0;
                                            if (total_call > 0) {
                                                success_rate = ((float) obj.getTotal_success() * 100 / total_call);
                                            }
                                            total_origin_bill += obj.getOrigin_bill_amount();
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td style="text-align: center"><%=obj.getStart_time()%></td>
                                    <%if (summaryBy.contains("0")) {%>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td><%=obj.getOrigin_ip()%></td>
                                    <%}%>
                                    <%if (summaryBy.contains("1")) {%>
                                    <td><%=obj.getOrigin_destination()%></td>
                                    <%}%>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <td style="text-align: right"><%=Utils.getMMSS(obj.getDuration())%></td>
                                    <td style="text-align: right"><%=obj.getTotal_success()%>+<%=obj.getTotal_fail()%>=<%=(obj.getTotal_success() + obj.getTotal_fail())%></td>
                                    <td style="text-align: right"><%=pddformatter.format(success_rate)%></td>
                                    <td style="text-align: right"><%=Utils.getMMSS(acd)%></td>
                                    <td style="text-align: right"><%=pddformatter.format(avg_pdd)%></td>
                                </tr>
                                <%}
                                    }%>
                            </tbody>
                        </table>                        
                        <div class="full-div" style="padding: 8px"> 
                            <div style="height: 15px"></div>
                            <table style="width: 70%; margin: 0 auto; border: 1px #ffffff solid" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid; padding: 5px; width: 25%">Total Duration(MM:SS)</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=Utils.getMMSS(total_time)%></td>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid;border-left: 1px #ffffff solid; padding: 5px; width: 25%">Total Origination Bill</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=amount_formatter.format(total_origin_bill)%></td>
                                </tr>
                                <tr>
                                    <th style="text-align: left; padding: 5px">ASR</th>
                                    <td style="text-align: right;border-left: 1px #ffffff solid; padding: 5px"><%=pddformatter.format(((double) total_success / (double) ((total_success + total_fail) <= 0 ? 1 : (total_success + total_fail))) * 100)%>%</td>
                                    <th style="text-align: left;border-left: 1px #ffffff solid; padding: 5px">Success+Fail=Total</th>
                                    <td colspan="3" style="text-align: right;border-left: 1px #ffffff solid; padding: 5px"><%=total_success%>+<%=total_fail%>=<%=(total_success + total_fail)%></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>