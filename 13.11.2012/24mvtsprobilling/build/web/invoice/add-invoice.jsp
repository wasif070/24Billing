<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page import="com.myapp.struts.session.Constants"%>
<%@page import="com.myapp.struts.util.Utils"%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Invoice</title>
        <%@include file="../login/login-check.jsp"%>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
                <%String[] currentDate = Utils.getDateParts(System.currentTimeMillis());%>
            </div>
            <%
                String s = currentDate[1];
                int month = Integer.parseInt(s);
                month = month - 1;
                String realContextPath = application.getRealPath("/") + "images";
                request.getSession().setAttribute("realpath", realContextPath);
            %>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "invoice");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("invoice/listInvoice.do;Invoice");
                        navList.add(";Add New Invoice");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/invoice/addInvoice" method="post" enctype="multipart/form-data">
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th><h3>Invoice Generation</h3></th></tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <fieldset><legend>Header Information</legend>
                                            <table width="100%" style="font-size: 8pt">                       
                                                <tr>
                                                    <th valign="top" class="font_size">Company Name </span></th>
                                                    <td valign="top" >
                                                        <html:text property="companyName" /><br/>
                                                        <html:messages id="companyName" property="companyName">
                                                            <bean:write name="companyName"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">Company Address </span></th>
                                                    <td valign="top" >
                                                        <html:text property="companyAddress" /><br/>
                                                        <html:messages id="companyAddress" property="companyAddress">
                                                            <bean:write name="companyAddress"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">Company Location </span></th>
                                                    <td valign="top" >
                                                        <html:text property="companyLocation" /><br/>
                                                        <html:messages id="companyLocation" property="companyLocation">
                                                            <bean:write name="companyLocation"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">Upload Logo</th>
                                                    <td>
                                                        <html:file property="logo"/><br/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size"></th>
                                                    <td valign="top">
                                                        Preferred Image Size 260 * 98 px
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset><legend>Invoice Information</legend>
                                            <table width="100%" cellpadding="0" cellspacing="0" style="font-size:8pt">
                                                <tr>
                                                    <th  valign="top" class="font_size">Client Type<span class="req_mark">*</span></th>
                                                    <td  valign="top" class="font_size_td"><input type="radio" name="clientType" value="0" class="receiver"/>Origination
                                                        <input type="radio" name="clientType" value="1" class="receiver"/>Termination
                                                        <input type="radio" name="clientType" value="2" class="receiver"/>Both</td>     
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">Client<span class="req_mark">*</span></th>
                                                    <td>
                                                        <div class ="jcustomer_list"></div>
                                                        <div class="clear"></div>
                                                        <html:messages id="clientId" property="clientId">
                                                            <bean:write name="clientId"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>    
                                                    <td colspan="2"> <div class ="jcustomer_list1"></div></td>                                            
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">From</th>
                                                    <td>
                                                        <select class="select_item2"  name="bday">
                                                            <option value="<%=currentDate[0]%>"><%=currentDate[0]%></option>
                                                            <%
                                                                for (int i = 1; i <= 31; i++) {%>
                                                            <option value="<%=Integer.toString(i)%>"><%=i%></option>
                                                            <%   }
                                                            %>
                                                        </select>
                                                        <select class="select_item1" name="bmonth">
                                                            <option value="<%=currentDate[1]%>"><%=Constants.MONTH_NAME[month]%></option>
                                                            <%
                                                                for (int i = 0; i < Constants.MONTH_VALUE.length; i++) {%>
                                                            <option value="<%=Constants.MONTH_VALUE[i]%>"><%=Constants.MONTH_NAME[i]%></option>
                                                            <%   }
                                                            %>
                                                        </select>
                                                        <select class="select_item1" name="byear" >
                                                            <option value="<%=currentDate[2]%>"><%=currentDate[2]%></option>
                                                            <%              int year = 2010;
                                                                for (int i = 0; i <= 50; i++) {%>
                                                            <option value="<%=Integer.toString(year)%>"><%=year%></option>
                                                            <%  year++;
                                                                }
                                                            %>
                                                        </select>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" class="font_size">To</th>
                                                    <td>
                                                        <select class="select_item2" name="eday">
                                                            <option value="<%=currentDate[0]%>"><%=currentDate[0]%></option>
                                                            <%
                                                                for (int i = 1; i <= 31; i++) {%>
                                                            <option value="<%=Integer.toString(i)%>"><%=i%></option>
                                                            <%   }
                                                            %>
                                                        </select>
                                                        <select class="select_item1" name="emonth">
                                                            <option value="<%=currentDate[1]%>"><%=Constants.MONTH_NAME[month]%></option>
                                                            <%
                                                                for (int i = 0; i < Constants.MONTH_VALUE.length; i++) {%>
                                                            <option value="<%=Constants.MONTH_VALUE[i]%>"><%=Constants.MONTH_NAME[i]%></option>
                                                            <%   }
                                                            %>
                                                        </select>

                                                        <select class="select_item1" name="eyear" >
                                                            <option value="<%=currentDate[2]%>"><%=currentDate[2]%></option>
                                                            <%             int year1 = 2010;
                                                                for (int i = 0; i <= 50; i++) {%>
                                                            <option value="<%=Integer.toString(year1)%>"><%=year1%></option>
                                                            <%  year1++;
                                                                }
                                                            %>
                                                        </select>
                                                    </td>
                                                </tr>    
                                                <tr>
                                                    <th valign="top" class="font_size">Invoice Date</th>
                                                    <td>
                                                        <select class="select_item2" name="iday">
                                                            <option value="<%=currentDate[0]%>"><%=currentDate[0]%></option>
                                                            <%
                                                                for (int i = 1; i <= 31; i++) {%>
                                                            <option value="<%=Integer.toString(i)%>"><%=i%></option>
                                                            <%   }
                                                            %>
                                                        </select>
                                                        <select class="select_item1" name="imonth">
                                                            <option value="<%=currentDate[1]%>"><%=Constants.MONTH_NAME[month]%></option>
                                                            <%
                                                                for (int i = 0; i < Constants.MONTH_VALUE.length; i++) {%>
                                                            <option value="<%=Constants.MONTH_VALUE[i]%>"><%=Constants.MONTH_NAME[i]%></option>
                                                            <%   }
                                                            %>
                                                        </select>

                                                        <select class="select_item1" name="iyear" >
                                                            <option value="<%=currentDate[2]%>"><%=currentDate[2]%></option>
                                                            <%             int year2 = 2010;
                                                                for (int i = 0; i <= 50; i++) {%>
                                                            <option value="<%=Integer.toString(year2)%>"><%=year2%></option>
                                                            <%  year2++;
                                                                }
                                                            %>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset><legend>Footer Information</legend>
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <th valign="top" class="font_size">Details Of Footer</th>
                                                    <td valign="top">
                                                        <html:text property="footer" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>                                        
                                    <td colspan="2" align="center">
                                        <div class="height-10px"></div>
                                        <input name="submit" type="submit" class="custom-button" value="Save" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        customers.init();
    });
</script>