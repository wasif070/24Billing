
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.clients.ClientLoader,com.myapp.struts.clients.ClientDTO" %>
<%
    int type = 0;
    int parent = -1;
    String output = "";
    if (request.getParameter("type") != null) {
        type = Integer.parseInt(request.getParameter("type"));
    }
    if (request.getParameter("parent_id") != null) {
        parent = Integer.parseInt(request.getParameter("parent_id"));
    }

    ArrayList<ClientDTO> clientListByType = ClientLoader.getInstance().getClientDTOByTypeAndParent(type, (int) login_dto.getOwn_id(), 1,login_dto);//1=clients
    output += "<option value=\"\">Select Client</option>";
    if (clientListByType != null && clientListByType.size() > 0) {
        int size = clientListByType.size();
        for (int i = 0; i < size; i++) {
            ClientDTO c_dto = (ClientDTO) clientListByType.get(i);
            output += "<option value=\"" + c_dto.getId() + "\">" + c_dto.getClient_id() + "</option>";
        }
    }
%>
<%=output%>