<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Add Rate Plan</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "rateplan");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">                 
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("rateplan/listRateplan.do?list_all=1;Rate Plan");
                        navList.add(";Add New Rate Plan");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>                                
                    <html:form action="/rateplan/addRateplan" method="post">     
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add Rate Plan</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="RateplanForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <!--
                                <tr>
                                    <th valign="top" >Reseller <span class="req_mark">*</span></th>
                                    <td valign="top" >
                                    <html:select property="parent_id" styleId="parent_id" >
                                        <html:option value="-1">--Select--</html:option>
                                        <%for (ClientDTO cl_dto : ClientLoader.getInstance().getClientDTOByLevel(login_dto.getOwn_id(), 2, login_dto.getClient_level())) {%>
                                        <html:option value="<%=String.valueOf(cl_dto.getId())%>"><%=cl_dto.getClient_id()%></html:option>
                                        <%}%>
                                    </html:select>
                                    <div class="clear"></div>
                                    <html:messages id="parent_id" property="parent_id">
                                        <bean:write name="parent_id"  filter="false"/>
                                    </html:messages>
                                </td>
                            </tr>
                            <tr>
                                <th valign="top" >Client <span class="req_mark">*</span></th>
                                <td valign="top">
                                    <html:select property="client_id" styleId="client_id">
                                        <html:option value="-1">--Select--</html:option>
                                        <%for (ClientDTO cl_dto : ClientLoader.getInstance().getClientDTOByLevel(login_dto.getOwn_id(), -1, login_dto.getClient_level())) {%>
                                        <html:option value="<%=String.valueOf(cl_dto.getId())%>"><%=cl_dto.getClient_id()%></html:option>
                                        <%}%>
                                    </html:select>
                                    <div class="clear"></div>
                                    <html:messages id="client_id" property="client_id">
                                        <bean:write name="client_id"  filter="false"/>
                                    </html:messages>
                                </td>
                            </tr>
                                    -->
                                    <tr>
                                        <th valign="top" >Rate Plan Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="rateplan_name" /><br/>
                                            <html:messages id="rateplan_name" property="rateplan_name">
                                                <bean:write name="rateplan_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Description</th>
                                        <td valign="top" >
                                            <html:text property="rateplan_des" /><br/>
                                            <html:messages id="rateplan_des" property="rateplan_des">
                                                <bean:write name="rateplan_des"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Status <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="rateplan_status">
                                                <%
                                                    for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="rateplan_status" property="rateplan_status">
                                                <bean:write name="rateplan_status"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <html:hidden property="action" value="<%=String.valueOf(Constants.ADD)%>" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        utilities.init();
    });
</script>