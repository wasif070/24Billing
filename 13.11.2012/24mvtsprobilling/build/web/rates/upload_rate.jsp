<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Add Destination Code</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <%
            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } else {
                msg = "<div style='color:#f00;'>" + msg + "</div>";
            }

        %>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "rate");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                     <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("rates/listRate.do?list_all=1&rate_id=1;Rate");
                    navList.add(";Upload Rate");
                %>
                <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/rates/uploadRate" method="post" enctype="multipart/form-data">                        
                        <table class="input_table" style="width:100%;" cellspacing="0" cellpadding="0" >
                            <thead>
                                <tr><th colspan="2"><h3>Upload Rate CSV File</h3></th></tr>
                            </thead>
                            <tbody>

                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <%=msg%>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"  valign="bottom">
                                        <bean:write name="RateForm" property="message" filter="false"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">Note: Data Format[Prefix, Destination Name,Rate(Per Min), First Pulse, Next Pulse, Grace Period, Failed Period,Day,From Hour,From Min,To Hour, To Min] <br />Example: 880,Bangladesh,1,30,10,10,10,0,0,0,23,59</td>
                                </tr>
                                <tr>
                                    <th valign="top" >Upload File</th>
                                    <td valign="top">
                                        <html:file property="theFile"/>
                                        <html:messages id="theFile" property="theFile">
                                            <bean:write name="theFile"  filter="false"/>
                                        </html:messages>
                                    </td>
                                </tr>
                                <tr>
                                    <th>&nbsp;</th>
                                    <td>
                                        <html:hidden property="action" value="<%=String.valueOf(Constants.UPLOAD)%>" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" value="Upload" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                                <tr><td colspan="2" align="center">Day[-1=>All, 0=>Sunday, 1=>Monday, 2=>Tuesday, 3=>Wednesday, 4=>Thursday, 5=>Friday,6=>Saturday]</td></tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>