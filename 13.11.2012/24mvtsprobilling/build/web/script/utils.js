
jQuery(document).ready(function(){
    utilities.init();
});

var customers={
    row_count:0,
    init:function(){        
        $j('.receiver').click(function(){             
            customers.customerList(this);
            return true;
        });
        $j('.customer').live('click',function(){         
            customers.customerInfo(this);
            return true;
        });
    },
    customerList:function(txt){
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'invoice/clients.jsp',
            data:{                
                text: txt.value,
                time: new Date().getMilliseconds()
            },
            success:function(html){           
                $j('.jcustomer_list').html(html);
                $j('.jcustomer_list').css('display','block');      
            },
            error:function(e,m,s){
                alert(e.responseText);
            }
        });
    },
    customerInfo:function(txt){
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'invoice/customer-info.jsp',
            data:{                
                text: txt.value,
                time: new Date().getMilliseconds()
            },
            success:function(html){           
                $j('.jcustomer_list1').html(html);
            },
            error:function(e,m,s){
                alert(e.responseText);
            }
        });
    }
}

var utilities={
    init:function(){        
        $j('#btnDialPlan').click(function(){             
            return utilities.selectAllOptions(this);
        });
        $j('#add').click(function() {  
            return !$j('#select1 option:selected').remove().appendTo('#select2');  
        });  
        $j('#remove').click(function() {  
            return !$j('#select2 option:selected').remove().appendTo('#select1');  
        });  
        $j('#moveup').click(function() {  
            return !$j('#select2 option:selected').remove().appendTo('#select1');  
        });
        $j('#movedown').click(function() {  
            return !$j('#select2 option:selected').remove().appendTo('#select1');  
        });
        
        $j('#btnMoveUp').click(function () {
            utilities.moveUp();             
            return false;
        });

        $j('#btnMoveDown').click(function () {
            utilities.moveDown();
            return false;
        });
        $j('#parent_id').live('change',function(){         
            utilities.getChilds(this);            
            return false;
        });
        $j('#parentid').live('change',function(){         
            utilities.getRatePlanList(this);           
            return false;
        });       
        
        $j('#jgateway_type').live('change',function(){   
            if(parseInt(this.value,10)==0 || parseInt(this.value,10)==-1){
                $j('#gateway_dst_port_h323').attr('disabled','disabled');
                $j('#gateway_dst_port_sip').attr('disabled','disabled');
                $j('.operators').addClass("display_off")
            }
            else{
                $j('#gateway_dst_port_h323').attr('disabled',"");
                $j('#gateway_dst_port_sip').attr('disabled',"");
                $j('.operators').removeClass("display_off")
            }            
            return true;
        });
    },
    selectAllOptions:function(obj){
        var selected=false;
        $j('#select2 option').each(function(i) {
            $j(this).attr("selected", true);              
            selected=true;
        });
        if(!selected){
            alert("Please select gateway(s)");
        }
        return selected;
    },
    moveUp:function() {
        var select= $j('#select2')[0];
        for (var i= 1, n=select.options.length; i<=n; i++)
            if (select.options[i].selected && !select.options[i-1].selected){
                select.insertBefore(select.options[i], select.options[i-1]);
            }
    },
    moveDown:function() {
        var select= $j('#select2')[0];
        for (var i= select.options.length; i-->=0;)
            if (select.options[i].selected && !select.options[i+1].selected)
                select.insertBefore(select.options[i+1], select.options[i]);
    },
    getChilds:function(obj){            
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'shared/clients.jsp',
            data:{                
                parent_id: $j(obj).val(),
                time: new Date().getMilliseconds()
            },
            success:function(html){     
                $j('#client_id').html(html);
            },
            error:function(e,m,s){
            //alert(e.responseText);
            }
        });    
    },
    getRatePlanList:function(obj){            
        $j.ajax({
            type:'POST',
            async:false,
            url:base_url+'shared/rateplan_loader.jsp',
            data:{                
                parent: $j(obj).val(),
                time: new Date().getMilliseconds()
            },
            success:function(html){     
                $j('#rateplan_id_container').html(html);
            },
            error:function(e,m,s){
            //alert(e.responseText);
            }
        });    
    }
}

