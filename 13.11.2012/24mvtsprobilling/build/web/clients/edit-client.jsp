<%@page import="parents.ParentLoader"%>
<%@page import="parents.ParentDTO"%>
<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Edit Client</title>
        <div><%@include file="../includes/header.jsp"%></div>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.rateplan.RateplanLoader,com.myapp.struts.rateplan.RateplanDTO" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
        <%
            long p_id = request.getParameter("parent") == null ? -1 : Integer.parseInt(request.getParameter("parent"));
            ArrayList<RateplanDTO> rateplanDto = RateplanLoader.getInstance().getRateplanDTOListByParentId(p_id);
            long _id = request.getParameter("id") != null ? Long.parseLong(request.getParameter("id")) : 0;
            long rateplanid = 0;
            if (login_dto.getOwn_id() > 0) {
                ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
                if (client_dto != null) {
                    rateplanid = client_dto.getRateplan_id();
                }
            }
        %>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "client");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">      
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("clients/listClient.do?list_all=1;Clients");
                        navList.add(";Edit Client");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/clients/editClient" method="post">
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <tbody>
                                <tr>
                                    <td>
                                        <fieldset style="width: 75%; margin: 0 auto;"><legend class="legnd-text-color">Edit <bean:write name="ClientForm" property="client_name" />[<%=ClientLoader.getInstance().getClientsTree(Long.parseLong(request.getParameter("id")))%>]</legend>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2" align="center"  valign="bottom">
                                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Mother Company <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="mother_company_id" >
                                                            <html:option value="0">Select</html:option>
                                                            <%
                                                                ArrayList<ParentDTO> parentList = ParentLoader.getInstance().getParentDTOList();
                                                                for (ParentDTO parent : parentList) {
                                                            %>
                                                            <html:option value="<%=String.valueOf(parent.getId())%>"><%=parent.getParent_name()%></html:option>
                                                            <%}%>
                                                        </html:select>
                                                        <div class="clear"></div>
                                                        <html:messages id="client_level" property="client_level">
                                                            <bean:write name="client_level"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Level <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="client_level" disabled="true">
                                                            <html:option value="1">Client</html:option>
                                                            <html:option value="2">Reseller</html:option>
                                                        </html:select>
                                                        <div class="clear"></div>
                                                        <html:messages id="client_level" property="client_level">
                                                            <bean:write name="client_level"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <% if (login_dto.getOwn_id() < 0) {
                                                %>
                                                <tr>
                                                    <th valign="top" >Parent <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="parent_id" styleId="parentid" disabled="true">
                                                            <html:option value="-1">--Select--</html:option>
                                                            <%
                                                                int req_id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
                                                                for (ClientDTO cl_dto : ClientLoader.getInstance().getClientDTOByLevel(2)) {
                                                                    if (cl_dto.getId() == req_id || ClientLoader.getInstance().getChildrenStr(req_id).contains("" + cl_dto.getId())) {
                                                                        continue;
                                                                    }
                                                            %>
                                                            <html:option value="<%=String.valueOf(cl_dto.getId())%>"><%=cl_dto.getClient_id()%></html:option>
                                                            <%}%>
                                                        </html:select>
                                                        <html:hidden property="parent_id" />
                                                        <div class="clear"></div>
                                                        <html:hidden property="old_parent_id" />
                                                        <html:messages id="parent_id" property="parent_id">
                                                            <bean:write name="parent_id"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <%}%>
                                                <tr>
                                                    <th valign="top" >Client ID <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:text property="client_id" /><br/>
                                                        <html:messages id="client_id" property="client_id">
                                                            <bean:write name="client_id"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Password <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:password property="client_password" /><br/>
                                                        <html:messages id="client_password" property="client_password">
                                                            <bean:write name="client_password"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Retype Password <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:password property="retypePassword" /><br/>
                                                        <html:messages id="retypePassword" property="retypePassword">
                                                            <bean:write name="retypePassword"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Name </th>
                                                    <td valign="top" >
                                                        <html:text property="client_name" /><br/>
                                                        <html:messages id="client_name" property="client_name">
                                                            <bean:write name="client_name"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Email <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:text property="client_email" /><br/>
                                                        <html:messages id="client_email" property="client_email">
                                                            <bean:write name="client_email"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Client Type <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="client_type" >
                                                            <html:option value="-1">Client Type</html:option>                                                            
                                                            <html:option value="0">Origination</html:option>
                                                            <%if (login_dto.getClient_level() != 2) {%>
                                                            <html:option value="1">Termination</html:option>
                                                            <html:option value="2">Both</html:option>
                                                            <%}%>
                                                        </html:select>
                                                        <html:hidden property="client_type" />
                                                        <div class="clear"></div>
                                                        <html:messages id="client_type" property="client_type">
                                                            <bean:write name="client_type"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Status <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="client_status" disabled="true">
                                                            <%
                                                                for (int i = 0; i < Constants.LIVE_STATUS_VALUE.length; i++) {
                                                            %>
                                                            <html:option value="<%=Constants.LIVE_STATUS_VALUE[i]%>"><%=Constants.LIVE_STATUS_STRING[i]%></html:option>
                                                            <%
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="client_status" property="client_status">
                                                            <bean:write name="client_status"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <!--                                                <tr>
                                                                                                    <th valign="top" >Is ICX</th>
                                                                                                    <td valign="top" >
                                                <html:select property="is_icx">                                                            
                                                    <html:option value="0">No</html:option>
                                                    <html:option value="1">Yes</html:option>
                                                </html:select>                                                   
                                            </td>
                                        </tr>-->
                                                <tr>
                                                    <th valign="top" >Rate Plan <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="rateplan_id" styleId="rateplan_id_container">
                                                            <html:option value="-1">Select Rate Plan</html:option>
                                                            <%
                                                                String rateplan_name = "Not Found";
                                                                if (rateplanDto != null && rateplanDto.size() > 0) {
                                                                    for (int i = 0; i < rateplanDto.size(); i++) {
                                                                        RateplanDTO rdto = new RateplanDTO();
                                                                        rdto = rateplanDto.get(i);
                                                                        rateplan_name = rdto.getRateplan_name();
                                                                        if (rateplanid == rdto.getRateplan_id()) {
                                                                            rateplan_name = "Base Rateplan";
                                                                        }
                                                            %>
                                                            <html:option value="<%=String.valueOf(rdto.getRateplan_id())%>"><%=rateplan_name%></html:option>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="rateplan_id" property="rateplan_id">
                                                            <bean:write name="rateplan_id"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>                                                
                                                <!--                                                <tr>
                                                                                                    <th valign="top" >Credit Limit</th>                                                    
                                                                                                    <td valign="top" >
                                                <% if (login_dto.getOwn_id() == ClientLoader.getInstance().getClientDTOByID(_id).getParent_id()) {%>
                                                <html:text property="client_credit_limit" />
                                                <%} else {%>
                                                <html:text property="client_credit_limit" readonly="true" />
                                                <%}%>
                                                <div class="clear"></div>
                                                <html:messages id="client_credit_limit" property="client_credit_limit">
                                                    <bean:write name="client_credit_limit"  filter="false"/>
                                                </html:messages>
                                            </td>
                                        </tr>                                                                                                -->
                                                <tr>
                                                    <th valign="top" >Balance</th>
                                                    <td valign="top" >
                                                        <html:text property="client_balance" readonly="true" /><br/>
                                                        <html:hidden property="client_credit_limit" value="0" />
                                                        <html:messages id="client_balance" property="client_balance">
                                                            <bean:write name="client_balance"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>  
                                                <tr>
                                                    <th valign="top" >Call Limit</th>
                                                    <td valign="top" >
                                                        <html:text property="client_call_limit"/><br/>
                                                        <html:messages id="client_call_limit" property="client_call_limit">
                                                            <bean:write name="client_call_limit"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>        
                                                <tr>
                                                    <th valign="top" >Number of CCT/BW</th>
                                                    <td valign="top" >
                                                        <html:text property="number_of_cct_or_bw" /><br/>   
                                                        <html:messages id="number_of_cct_or_bw" property="number_of_cct_or_bw">
                                                            <bean:write name="number_of_cct_or_bw"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Incoming Prefix</legend>
                                            <table width="100%">
                                                <tr>
                                                    <th valign="top" style="width:35%;">Prefix</th>
                                                    <td valign="top">
                                                        <html:text property="prefix" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center">
                                        <html:hidden property="incoming_prefix" value="" />
                                        <html:hidden property="incoming_to" value="" />
                                        <html:hidden property="outgoing_prefix" value="" />
                                        <html:hidden property="outgoing_to" value="" />
                                        <html:hidden property="is_icx" value="0" />
                                        <html:hidden property="id" />
                                        <html:hidden property="client_status" />
                                        <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                        <input name="submit" type="submit" class="custom-button" onclick="javascript:return confirm('Are you sure to update the Client?');" value="Update" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>        
    </body>
</html>