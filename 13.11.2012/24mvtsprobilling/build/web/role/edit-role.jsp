<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@page  import="java.util.ArrayList,role.PageDTO,role.RoleLoader" %>
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Edit Role</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>        
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "role");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("role/listRoles.do;Roles");
                        navList.add(";Edit Role");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/role/editRole" method="post">
                        <fieldset style="width: 75%; margin: 0 auto;"><legend class="legnd-text-color">Edit Role</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0">
                                <thead>
                                    <tr>
                                        <th colspan="2">
                                            <bean:write name="RoleForm" property="message" filter="false"/>
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th>Role Name <span class="mendatory">*</span></th>
                                        <td>
                                            <html:hidden property="id" />
                                            <html:text property="roleName" />
                                            <html:messages id="roleName" property="roleName">
                                                <bean:write name="roleName"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Role Desc</th>
                                        <td>
                                            <html:text property="roleDesc" />
                                            <html:messages id="roleDesc" property="roleDesc">
                                                <bean:write name="roleDesc"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Status</th>
                                        <td>
                                            <html:select property="status" styleClass="width-154px">
                                                <html:option value="1">Active</html:option>
                                                <html:option value="0">Inactive</html:option>                                        
                                            </html:select>
                                            <html:messages id="status" property="status">
                                                <bean:write name="status"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%">
                                            <table class="reporting_table three-fourth-div float-center" border="0" cellpadding="0" cellspacing="1">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align: left; color: black">Page Name</th>
                                                        <th style="text-align: left; color: black">Add</th>
                                                        <th style="text-align: left; color: black">View</th>
                                                        <th style="text-align: left; color: black">Edit</th>
                                                        <th style="text-align: left; color: black">Delete</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <%
                                                        String styleclass = "even";
                                                        long roleId = Long.valueOf(request.getSession(true).getAttribute("ROLEID").toString());
                                                        ArrayList<PageDTO> pages = role.RoleDBFunction.getPages();
                                                        for (int i = 0; i < pages.size(); i++) {
                                                            PageDTO pageDTO = pages.get(i);
                                                            String pageid = String.valueOf(pageDTO.getId());
                                                            String pageName = pageDTO.getPageName();
                                                            String pageURI = pageDTO.getPageUri();
                                                            int[] permissions = RoleLoader.getInstance().getPermissionsForEditPage(roleId, pageURI);
                                                            if (i % 2 == 0) {
                                                                styleclass = "even";
                                                            } else {
                                                                styleclass = "odd";
                                                            }
                                                    %>
                                                    <tr class="<%=styleclass%>">
                                                        <td width="60%">
                                                            <%=pageName%>
                                                            <input type="hidden" name="pageIds" value="<%=pageid%>" />
                                                        </td>
                                                        <td width="10%">
                                                            <input type="checkbox" name="adds" value="<%=pageid%>" <%=permissions[28] == 1 ? "checked=\"checked\"" : ""%> />
                                                        </td>
                                                        <td width="10%">
                                                            <input type="checkbox" name="views" value="<%=pageid%>" <%=permissions[29] == 1 ? "checked=\"checked\"" : ""%> />
                                                        </td>
                                                        <td width="10%">
                                                            <input type="checkbox" name="edits" value="<%=pageid%>" <%=permissions[30] == 1 ? "checked=\"checked\"" : ""%> />
                                                        </td>
                                                        <td width="10%">
                                                            <input type="checkbox" name="deletes" value="<%=pageid%>" <%=permissions[31] == 1 ? "checked=\"checked\"" : ""%> />
                                                        </td>                                                    
                                                    </tr>
                                                    <%}%>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="height-10px"></div>
                                            <input name="submit" type="submit" class="custom-button" value="Save" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>        
    </body>
</html>
