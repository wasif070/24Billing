/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ans;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class DeleteAnsAction extends Action {

    static Logger logger = Logger.getLogger(AnsDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        long cid = Long.parseLong(request.getParameter("id"));
        AnsTaskSchedular rt = new AnsTaskSchedular();
        AnsDTO dto=rt.getAnsDTO(cid);

        MyAppError error = new MyAppError();
        error = rt.deleteAns((int)cid);
        
        dto.setDeleted(1);
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        Gson json = new GsonBuilder().serializeNulls().create();
        ActivityDTO ac_dto = new ActivityDTO();
        ac_dto.setUserId(login_dto.getClientId());
        ac_dto.setChangedValue(json.toJson(rt.getAnsDTO(cid)));
        ac_dto.setActionName(Constants.DELETE_ACTION);
        ac_dto.setTableName("ans_clients");
        ac_dto.setPrimaryKey(dto.getPrimary_prefix());
        ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
        activityTaskScheduler.addActivityDTO(ac_dto);
        
        AnsForm formBean = new AnsForm();
        formBean.setMessage(error.getErrorType() > 0 ? true : false, error.getErrorMessage());
        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
        return mapping.findForward("success");
    }
}
