package ans;

public class AnsDTO {

    public long id;
    public String name;
    public String company_name;
    public String primary_prefix;
    public String other_prefixes;
    public int status;
    public int deleted;
    public boolean sw_primary_prefix;
    public boolean sw_other_prefix;
    public boolean sw_name;
    public boolean sw_company_name;

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public int getDeleted() {
        return deleted;
    }

    public void setDeleted(int deleted) {
        this.deleted = deleted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOther_prefixes() {
        return other_prefixes;
    }

    public void setOther_prefixes(String other_prefixes) {
        this.other_prefixes = other_prefixes;
    }

    public String getPrimary_prefix() {
        return primary_prefix;
    }

    public void setPrimary_prefix(String primary_prefix) {
        this.primary_prefix = primary_prefix;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
