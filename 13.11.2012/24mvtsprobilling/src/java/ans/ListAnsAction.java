package ans;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListAnsAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            AnsTaskSchedular scheduler = new AnsTaskSchedular();
            AnsForm ansForm = (AnsForm) form;

            if (list_all == 0) {
                if (ansForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, ansForm.getRecordPerPage());
                }
                AnsDTO udto = new AnsDTO();
                if (ansForm.getName() != null && ansForm.getName().trim().length() > 0) {
                    udto.sw_name = true;
                    udto.setName(ansForm.getName().toLowerCase());
                }
                if (ansForm.getCompany_name() != null && ansForm.getCompany_name().trim().length() > 0) {
                    udto.sw_company_name = true;
                    udto.setCompany_name(ansForm.getCompany_name().toLowerCase());
                }
                if (ansForm.getPrimary_prefix() != null && ansForm.getPrimary_prefix().trim().length() > 0) {
                    udto.sw_primary_prefix = true;
                    udto.setPrimary_prefix(ansForm.getPrimary_prefix().toLowerCase());
                }
                if (ansForm.getOther_prefixes() != null && ansForm.getOther_prefixes().trim().length() > 0) {
                    udto.sw_other_prefix = true;
                    udto.setOther_prefixes(ansForm.getOther_prefixes().toLowerCase());
                }

                ansForm.setList(scheduler.getAnsDTOsWithSearchParam(udto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    ansForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                ansForm.setList(scheduler.getAnsDTOs(login_dto));
            }
            ansForm.setSelectedIDs(null);
            if (ansForm.getList() != null && ansForm.getList().size() <= (ansForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
