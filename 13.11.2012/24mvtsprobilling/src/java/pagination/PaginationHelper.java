/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pagination;

/**
 *
 * @author computer
 */
public class PaginationHelper {

    /**
     * Generate the pagination links
     *
     * @access    public
     * @return    string
     */
    public String createLinks(Pagination p) {
        int curPage = p.getCurPage();
        String baseURL = p.getBaseUrl();
        String pagination_link = "";
        // If our item count or per-page total is zero there is no need to continue.
        if (p.getTotalRows() == 0 || p.getPerPage() == 0) {
            return "";
        }

        // Calculate the total number of pages
        int num_pages = p.getTotalRows() / p.getPerPage();
        if (p.getTotalRows() % p.getPerPage() > 0) {
            num_pages++;
        }

        // Is there only one page? Hm... nothing more to do here then.
        if (num_pages == 1) {
            return "";
        }

        // Determine the current page number.
            p.setCurPage(curPage);


        // Is the page number beyond the result range?
        // If so we show the last page
        if (p.getCurPage() > num_pages) {
            p.setCurPage(num_pages);
        }

        // Add a trailing slash to the base URL if needed
        p.setBaseUrl(baseURL);

        // And here we go...        

        // Render the "First" link
        if (p.getCurPage() > 1) {
            pagination_link += p.getFirstTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=1\">" + p.getFirstLink() + "</a>" + p.getFirstTagClose();
        } else {
            pagination_link += "<span class=\"disabled\">" + p.getFirstLink() + "</span>";
        }

        // Render the "previous" link
        int prev = 0;
        if (p.getCurPage() > 1) {
            prev = p.getCurPage() - 1;
            pagination_link += p.getPrevTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + prev + "\">" + p.getPrevLink() + "</a>" + p.getPrevTagClose();
        } else {
            pagination_link += p.getPrevTagOpen() + "<span class=\"disabled\"> " + p.getPrevLink() + "</span>" + p.getPrevTagClose();
        }

        int counter = 0;
        if (num_pages < 7 + (p.getNumLinks() * 2)) //not enough pages to bother breaking it up
        {
            for (counter = 1; counter <= num_pages; counter++) {
                if (counter == p.getCurPage()) {
                    pagination_link += "<span class=\"current\">" + counter + "</span>"; // Current page
                } else {
                    pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + counter + "\">" + counter + "</a>" + p.getNumTagClose();
                }
            }
        } else if (num_pages > 5 + (p.getNumLinks() * 2)) //enough pages to hide some
        {
            //close to beginning; only hide later pages
            if (p.getCurPage() < 1 + (p.getNumLinks() * 2)) {
                for (counter = 1; counter < 4 + (p.getNumLinks() * 2); counter++) {
                    if (counter == p.getCurPage()) {
                        pagination_link += "<span class=\"current\">" + counter + "</span>"; // Current page
                    } else {
                        pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + counter + "\">" + counter + "</a>" + p.getNumTagClose();
                    }
                }
                pagination_link += "...";

                int num_pages_minus = num_pages - 1;
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + num_pages_minus + "\">" + num_pages_minus + "</a>" + p.getNumTagClose();
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + num_pages + "\">" + num_pages + "</a>" + p.getNumTagClose();
            } //in middle; hide some front and some back
            else if (num_pages - (p.getNumLinks() * 2) > p.getCurPage() && p.getCurPage() > (p.getNumLinks() * 2)) {
                int one = 1;
                int two = 2;

                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + one + "\">" + one + "</a>" + p.getNumTagClose();
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + two + "\">" + two + "</a>" + p.getNumTagClose();

                pagination_link += "...";

                for (counter = p.getCurPage() - p.getNumLinks(); counter <= p.getCurPage() + p.getNumLinks(); counter++) {
                    if (counter == p.getCurPage()) {
                        pagination_link += "<span class=\"current\">" + counter + "</span>";
                    } else {
                        pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + counter + "\">" + counter + "</a>" + p.getNumTagClose();
                    }
                }
                pagination_link += "...";

                int num_pages_minus = num_pages - 1;
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + num_pages_minus + "\">" + num_pages_minus + "</a>" + p.getNumTagClose();
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + num_pages + "\">" + num_pages + "</a>" + p.getNumTagClose();
            } //close to end; only hide early pages
            else {
                int one = 1;
                int two = 2;

                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + one + "\">" + one + "</a>" + p.getNumTagClose();
                pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + two + "\">" + two + "</a>" + p.getNumTagClose();

                pagination_link += "...";

                for (counter = num_pages - (2 + (p.getNumLinks() * 2)); counter <= num_pages; counter++) {
                    if (counter == p.getCurPage()) {
                        pagination_link += "<span class=\"current\">" + counter + "</span>";
                    } else {
                        pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + counter + "\">" + counter + "</a>" + p.getNumTagClose();
                    }
                }
            }
        }
        // Render the "next" link
        if (p.getCurPage() < counter - 1) {
            int next = p.getCurPage() + 1;
            pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + next + "\">" + p.getNextLink() + "</a>" + p.getNumTagClose();
        } else {
            pagination_link += " <span class=\"disabled\">" + p.getNextLink() + "</span>";
        }


        // Render the "Last" link
        if (p.getCurPage() < counter - 1) {
            pagination_link += p.getNumTagOpen() + "<a href=\"" + p.getBaseUrl() + "&page=" + num_pages + "\">" + p.getLastLink() + "</a>" + p.getNumTagClose();
        } else {
            pagination_link += " <span class=\"disabled\">" + p.getLastLink() + "</span>";
        }

        // Kill double slashes.  Note: Sometimes we can end up with a double slash
        // in the penultimate link so we'll kill all double slashes.
        // pagination_link = preg_replace("#([^:])//+#", "\\1/", pagination_link);

        // Add the wrapper HTML if exists
        pagination_link = p.getFullTagOpen() + pagination_link + p.getFullTagClose();

        return pagination_link;
    }
}
