/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package igwreports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class OutgoingCallsAction extends Action {
    
    static Logger logger = Logger.getLogger(OutgoingCallsAction.class.getName());
    
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            
            int pageNo = 1;
            int perPageRecord = Constants.PER_PAGE_RECORD;
            
            if (request.getParameter("recordPerPage") != null) {
                perPageRecord = Integer.parseInt(request.getParameter("recordPerPage"));
            }
            if (request.getParameter("pageNo") != null) {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }
            int pageStart = (pageNo - 1) * perPageRecord;
            
            IGWReportTaskScheduler scheduler = new IGWReportTaskScheduler();
            IGWReportForm cdrReportForm = (IGWReportForm) form;
            if (cdrReportForm.getRecordPerPage() > 0) {
                request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, cdrReportForm.getRecordPerPage());
            }
            
            IGWReportDTO sdto = null;
            
            if (cdrReportForm.getDoSearch() != null) {                
                sdto = new IGWReportDTO();
                sdto.setOrigin_client_id(cdrReportForm.getOrigin_client_id());
                sdto.setTerm_client_id(cdrReportForm.getTerm_client_id());
                String fromDate = cdrReportForm.getFromYear() + "-" + formatter.format(cdrReportForm.getFromMonth()) + "-" + formatter.format(cdrReportForm.getFromDay()) + " 00:00:00";
                sdto.setFrom_date(fromDate);
                String toDate = cdrReportForm.getToYear() + "-" + formatter.format(cdrReportForm.getToMonth()) + "-" + formatter.format(cdrReportForm.getToDay()) + " 00:00:00";
                sdto.setTo_date(toDate);
                sdto.setReporting_type(cdrReportForm.getReporting_type());
                sdto.setAns_prefix(cdrReportForm.getAns_prefix());
            }
            cdrReportForm.setDtoList(scheduler.getOutgoingDTOs(sdto, login_dto, pageStart, perPageRecord));
            request.getSession(true).setAttribute("SearchDTO", sdto);
            
            if (cdrReportForm.getDtoList() != null && cdrReportForm.getDtoList().size() > 0) {
                request.getSession(true).setAttribute("IGWReportDTO", cdrReportForm.getDtoList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("IGWReportDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
