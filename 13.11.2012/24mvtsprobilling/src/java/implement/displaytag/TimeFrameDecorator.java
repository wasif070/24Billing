package implement.displaytag;

import java.text.DecimalFormat;
import javax.servlet.jsp.PageContext;
import org.displaytag.decorator.DisplaytagColumnDecorator;
import org.displaytag.exception.DecoratorException;
import org.displaytag.properties.MediaTypeEnum;

public class TimeFrameDecorator implements DisplaytagColumnDecorator {

    public Object decorate(Object columnValue, PageContext pageContext, MediaTypeEnum media) throws DecoratorException {
        String columnVal = String.valueOf(columnValue);
        String timeFrame = "";
        if (columnVal.contains(":")) {
            DecimalFormat df = new DecimalFormat("##00");
            String timeParts[] = columnVal.split(":", 4);
            int counter = 0;
            for (String timePart : timeParts) {
                counter++;
                timeFrame += ":" + df.format(timePart);
                timeFrame += counter == 2 ? " To " : "";
            }
            return timeFrame.substring(1);
        }
        return "00:00 To 00:00";
    }
}
