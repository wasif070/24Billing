/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.dialplan.DialplanDTO;
import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.rateplan.RateplanDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.user.UserDTO;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import databaseconnector.DBConnection;
import com.myapp.struts.util.Utils;

/**
 *
 * @author Administrator
 */
public class ActivityLoader {

    static ActivityLoader activityLoader = null;
    static Logger logger = Logger.getLogger(ActivityLoader.class.getName());

    public ActivityLoader() {
    }

    public static ActivityLoader getInstance() {
        if (activityLoader == null) {
            createActivityLoader();
        }
        return activityLoader;
    }

    private synchronized static void createActivityLoader() {
        if (activityLoader == null) {
            activityLoader = new ActivityLoader();
        }
    }

    public synchronized ActivityDTO getActivityDTO(ActivityDTO p_dto) {
        HashMap<String, Class> objClasses = new HashMap<String, Class>();
        objClasses.put("clients", ClientDTO.class);
        objClasses.put("dialpeer", DialplanDTO.class);
        objClasses.put("rateplan", RateplanDTO.class);
        objClasses.put("rate", RateplanDTO.class);
        objClasses.put("gateway", GatewayDTO.class);
        objClasses.put("user", UserDTO.class);
        objClasses.put("settings", SettingsDTO.class);


        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        ActivityDTO dto = new ActivityDTO();
        Gson json = new GsonBuilder().serializeNulls().create();
        Object prevObject = new Object();
        Object curObject = new Object();
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = null;
            if (p_dto.queryWithPKey) {
                sql = "select * from activity_log where is_deleted=0 and primary_key='" + p_dto.getPrimaryKey() + "' and table_name='" + p_dto.getTableName() + "' and id<=" + p_dto.getId() + " order by id DESC limit 2";
            } else {
                sql = "select * from activity_log where is_deleted=0 and table_name='" + p_dto.getTableName() + "' and id<=" + p_dto.getId() + " order by id DESC limit 2";
            }
            logger.debug("getActivityDTO SQL-->" + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                dto = new ActivityDTO();
                dto.setId(rs.getLong("id"));
                dto.setActionName(rs.getString("action_name"));
                dto.setChangedValue(rs.getString("changed_value"));
                dto.setIsdeleted(rs.getInt("is_deleted"));
                dto.setLogTime(rs.getLong("log_time"));
                dto.setChangeTime(Utils.LongToDate(dto.getLogTime()));
                dto.setPrimaryKey(rs.getString("primary_key"));
                dto.setStatus(rs.getInt("status"));
                dto.setTableName(rs.getString("table_name"));
                dto.setUserId(rs.getString("user_id"));
                curObject = json.fromJson(dto.getChangedValue(), objClasses.get(dto.getTableName()));
                if (rs.next()) {
                    //One step forward
                    dto = new ActivityDTO();
                    dto.setId(rs.getLong("id"));
                    dto.setActionName(rs.getString("action_name"));
                    dto.setChangedValue(rs.getString("changed_value"));
                    dto.setIsdeleted(rs.getInt("is_deleted"));
                    dto.setLogTime(rs.getLong("log_time"));
                    dto.setChangeTime(Utils.LongToDate(dto.getLogTime()));
                    dto.setPrimaryKey(rs.getString("primary_key"));
                    dto.setStatus(rs.getInt("status"));
                    dto.setTableName(rs.getString("table_name"));
                    dto.setUserId(rs.getString("user_id"));
                    prevObject = json.fromJson(dto.getChangedValue(), objClasses.get(dto.getTableName()));
                } else {
                    prevObject = json.fromJson("{}", objClasses.get(dto.getTableName()));
                }
            }
            dto.setComparisonList(ActivityLog.getActivityLog(prevObject, curObject));
        } catch (Exception e) {
            logger.debug("getActivityDTO exception-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }

        return dto;
    }

    public synchronized ArrayList<ActivityDTO> getActivityDTOs() {
        ArrayList<ActivityDTO> list = new ArrayList<ActivityDTO>();
        long BEFORE_72_HOURS = System.currentTimeMillis() - 72 * 60 * 60 * 1000L;
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select * from activity_log where is_deleted=0 and log_time >= " + BEFORE_72_HOURS + " order by id DESC";;
            logger.debug("reload SQL-->" + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ActivityDTO dto = new ActivityDTO();
                dto.setActionName(rs.getString("action_name"));
                dto.setChangedValue(rs.getString("changed_value"));
                dto.setId(rs.getLong("id"));
                dto.setIsdeleted(rs.getInt("is_deleted"));
                dto.setLogTime(rs.getLong("log_time"));
                dto.setChangeTime(Utils.LongToDate(dto.getLogTime()));
                dto.setPrimaryKey(rs.getString("primary_key"));
                dto.setStatus(rs.getInt("status"));
                dto.setTableName(rs.getString("table_name"));
                dto.setUserId(rs.getString("user_id"));
                list.add(dto);
            }

        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public ArrayList<String> getTableNames() {
        return ActivityDBFunction.tableNames();
    }

    public ArrayList<ActivityDTO> getActivityDTOsWithSearchParam(ActivityDTO act_dto) {
        ArrayList<ActivityDTO> searchedList = new ArrayList<ActivityDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        long ONE_DAY_IN_MILLIS = 24 * 60 * 60 * 1000L;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            long BEFORE_72_HOURS = System.currentTimeMillis() - 72 * 60 * 60 * 1000;
            String cond = "";
            act_dto.setToTime(act_dto.getToTime() + ONE_DAY_IN_MILLIS);

            String sql = "select * from activity_log where is_deleted=0 ";

            if (act_dto.swTimeDuration) {
                cond += " and log_time>=" + act_dto.getFromTime() + " and log_time<=" + act_dto.getToTime();
            } else {
                cond += " and log_time >= " + BEFORE_72_HOURS;
            }

            if (act_dto.swActionName) {
                cond += " and action_name like '%" + act_dto.getActionName() + "%' ";
            }
            if (act_dto.swUserId) {
                cond += " and user_id like '%" + act_dto.getUserId() + "%' ";
            }
            if (act_dto.swPKey) {
                cond += " and primary_key like '%" + act_dto.getPrimaryKey() + "%' ";
            }
            if (act_dto.swTableName) {
                cond += " and table_name like '%" + act_dto.getTableName() + "%' ";
            }
            sql += cond + " order by id DESC";


            logger.debug("getActivityDTOsWithSearchParam SQL-->" + sql);
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                ActivityDTO dto = new ActivityDTO();
                dto.setActionName(rs.getString("action_name"));
                dto.setChangedValue(rs.getString("changed_value"));
                dto.setId(rs.getLong("id"));
                dto.setIsdeleted(rs.getInt("is_deleted"));
                dto.setLogTime(rs.getLong("log_time"));
                dto.setChangeTime(Utils.LongToDate(dto.getLogTime()));
                dto.setPrimaryKey(rs.getString("primary_key"));
                dto.setStatus(rs.getInt("status"));
                dto.setTableName(rs.getString("table_name"));
                dto.setUserId(rs.getString("user_id"));
                searchedList.add(dto);
            }

        } catch (Exception e) {
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return searchedList;
    }
}
