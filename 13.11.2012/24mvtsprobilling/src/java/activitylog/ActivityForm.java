/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import org.apache.struts.action.ActionForm;

/**
 *
 * @author Administrator
 */
public class ActivityForm extends ActionForm {

    private long id;
    private String userId;
    private long logTime;
    private String tableName;
    private String actionName;
    private int fromDay;
    private int fromMonth;
    private int fromYear;
    private int toDay;
    private int toMonth;
    private int toYear;
    private String doSearch;
    private ArrayList<ActivityDTO> activityDTOs;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public int getFromDay() {
        return fromDay;
    }

    public void setFromDay(int fromDay) {
        this.fromDay = fromDay;
    }

    public int getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(int fromMonth) {
        this.fromMonth = fromMonth;
    }

    public int getFromYear() {
        return fromYear;
    }

    public void setFromYear(int fromYear) {
        this.fromYear = fromYear;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLogTime() {
        return logTime;
    }

    public void setLogTime(long logTime) {
        this.logTime = logTime;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public int getToMonth() {
        return toMonth;
    }

    public void setToMonth(int toMonth) {
        this.toMonth = toMonth;
    }

    public int getToYear() {
        return toYear;
    }

    public void setToYear(int toYear) {
        this.toYear = toYear;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public ArrayList<ActivityDTO> getActivityDTOs() {
        return activityDTOs;
    }

    public void setActivityDTOs(ArrayList<ActivityDTO> activityDTOs) {
        this.activityDTOs = activityDTOs;
    }

    public ActivityForm() {
        super();
        String[] startDateParts = Utils.getDateParts(System.currentTimeMillis() - AppConstants.ACTIVITY_LOG_DEFAULT_SEARCH_DURATION);// yyyy-MM-dd
        String[] endDateParts = Utils.getDateParts(System.currentTimeMillis());

        fromDay = Integer.parseInt(startDateParts[0]);
        fromMonth = Integer.parseInt(startDateParts[1]);
        fromYear = Integer.parseInt(startDateParts[2]);

        toDay = Integer.parseInt(endDateParts[0]);
        toMonth = Integer.parseInt(endDateParts[1]);
        toYear = Integer.parseInt(endDateParts[2]);
    }
}
