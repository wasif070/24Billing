/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

/**
 *
 * @author Ashraful
 */
public class ComparisonDTO {

    public ComparisonDTO() {
    }
    private String fieldName;
    private String previousValue;
    private String newValue;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(String previousValue) {
        this.previousValue = previousValue;
    }
}
