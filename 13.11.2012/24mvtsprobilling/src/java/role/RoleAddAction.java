/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.AppError;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;
import java.util.ArrayList;

public class RoleAddAction extends Action {

    static Logger logger = Logger.getLogger(RoleAddAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("RoleAddAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            RoleTaskScheduler scheduler = new RoleTaskScheduler();
            RoleForm roleForm = (RoleForm) form;
            PermissionDTO per_dto = new PermissionDTO();

            /*--------------------For geting searching parameter-------------------*/
            RoleDTO roleDTO = new RoleDTO();
            roleDTO.setRoleName(roleForm.getRoleName().trim());
            roleDTO.setStatus(roleForm.getStatus());
            roleDTO.setRoleDesc(roleForm.getRoleDesc().trim());

            ArrayList<PermissionDTO> permissions = new ArrayList<PermissionDTO>();

            try {
                int i = 0;
                int j = 0;
                for (i = 0; i < roleForm.getPageIds().length; i++) {
                    per_dto = new PermissionDTO();
                    int pageId = roleForm.getPageIds()[i];
                    per_dto.setPageId(pageId);

                    int add = 0;
                    int view = 0;
                    int edit = 0;
                    int delete = 0;

                    if (roleForm.getAdds() != null) {
                        for (j = 0; j < roleForm.getAdds().length; j++) {
                            if (roleForm.getAdds()[j] == pageId) {
                                add = 1;
                                break;
                            }
                        }
                    }
                    if (roleForm.getViews() != null) {
                        for (j = 0; j < roleForm.getViews().length; j++) {
                            if (roleForm.getViews()[j] == pageId) {
                                view = 1;
                                break;
                            }
                        }
                    }
                    if (roleForm.getEdits() != null) {
                        for (j = 0; j < roleForm.getEdits().length; j++) {
                            if (roleForm.getEdits()[j] == pageId) {
                                edit = 1;
                                break;
                            }
                        }
                    }
                    if (roleForm.getDeletes() != null) {
                        for (j = 0; j < roleForm.getDeletes().length; j++) {
                            if (roleForm.getDeletes()[j] == pageId) {
                                delete = 1;
                                break;
                            }
                        }
                    }

                    per_dto.setPermissions(Utils.getBinaryToInt(add + "" + view + "" + edit + "" + delete));
                    permissions.add(per_dto);
                }

            } catch (Exception ex) {
                logger.debug("Exception ex--------->" + ex);
            }
            roleDTO.setPermissionDTOs(permissions);

            AppError error = scheduler.addRoleInfo(roleDTO);
            if (error.errorType == error.NO_ERROR) {
                target = AppConstants.SUCCESS;
                request.getSession(true).setAttribute(AppConstants.ROLE_SUCCESS_MESSAGE, "Role information with role name " + roleDTO.getRoleName() + " added successfully.");
            } else if (error.errorType == error.DB_ERROR) {
                target = AppConstants.FAILURE;
                roleForm.setMessage(true, error.getErrorMessage());
            } else if (error.errorType == error.OTHERS_ERROR) {
                roleForm.setMessage(true, error.getErrorMessage());
            }
        } else {
            return (new ActionForward("/login/logout.do", true));
        }
        return (mapping.findForward(target));
    }
}
