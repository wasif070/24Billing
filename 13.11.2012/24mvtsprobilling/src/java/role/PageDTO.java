/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

public class PageDTO {

    public PageDTO() {
    }
    private int id;
    private String pageName;
    private String pageUri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPageName() {
        return pageName;
    }

    public void setPageName(String pageName) {
        this.pageName = pageName;
    }

    public String getPageUri() {
        return pageUri;
    }

    public void setPageUri(String pageUri) {
        this.pageUri = pageUri;
    }
}
