package com.myapp.struts.clients;

import com.myapp.struts.gateway.GatewayDAO;
import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import com.myapp.struts.transactions.TransactionLoader;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.AppConstants;
import java.util.ArrayList;

public class ClientDAO {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ClientDAO() {
    }

    public MyAppError addClientInformation(long user_id, ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("OVER_SALES");
        int over_sale = Integer.parseInt(settings.getSettingValue());

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select client_id from clients where client_id=? and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Client ID conflict with user or client");
                resultSet.close();
                return error;
            }
            sql = "select user_id from users where user_id=? and user_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Client ID conflict with user or client");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into clients(client_id,client_password,client_name,client_email,client_type,client_created,client_status,rateplan_id,client_credit_limit,client_balance,prefix,client_call_limit,client_level,parent_id,is_icx,number_of_cct_or_bw,mother_company_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            ps.setInt(5, p_dto.getClient_type());
            ps.setInt(6, 0);
            ps.setInt(7, p_dto.getClient_status());
            ps.setLong(8, p_dto.getRateplan_id());
            ps.setDouble(9, Double.parseDouble(p_dto.getClient_credit_limit()));
            ps.setDouble(10, Double.parseDouble(p_dto.getClient_balance()));
            ps.setString(11, p_dto.getPrefix());
            ps.setInt(12, p_dto.getClient_call_limit());
            ps.setInt(13, p_dto.getClient_level());
            ps.setInt(14, p_dto.getParent_id());
            ps.setInt(15, p_dto.getIs_icx());
            ps.setInt(16, p_dto.getNumber_of_cct_or_bw());
            ps.setLong(17, p_dto.getMother_company_id());


            ps.executeUpdate();

            int last_id = 0;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                last_id = rs.getInt(1);
                if (Double.parseDouble(p_dto.getClient_balance()) > 0) {
                    if (p_dto.getParent_id() > 0 && over_sale == AppConstants.NO) {//not admin or user i,e client or reseller
                        sql = "update clients set client_balance = (client_balance-" + Math.abs(Double.parseDouble(p_dto.getClient_balance())) + ") where id = " + p_dto.getParent_id();
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.executeUpdate();
                    }

                    sql = "insert into client_transactions(client_id,transaction_recharge,transaction_des,transaction_date,user_id,transaction_by,cur_balance) values(?,?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, last_id);
                    ps.setString(2, p_dto.getClient_balance());
                    ps.setString(3, "Initial Balance");
                    ps.setLong(4, System.currentTimeMillis());
                    ps.setLong(5, user_id);
                    ps.setInt(6, p_dto.getTransaction_by());
                    ps.setFloat(7, Float.parseFloat(p_dto.getClient_balance()));
                    ps.executeUpdate();
                }
                error.setErrorMessage(String.valueOf(last_id));
            }
            rs.close();
            ClientLoader.getInstance().forceReload();
            UserLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Client ID conflict with user or client");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError clientEditInformation(ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "update clients set client_id=?,client_password=?,client_name=?,client_email=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            ps.executeUpdate();

            ClientLoader.getInstance().forceReload();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editClientInformation(ClientDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select user_id from users where user_id=? and user_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Client ID conflict with user or client");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "select client_id from clients where client_id = ? and id!=" + p_dto.getId() + " and client_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getClient_id());
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Client ID.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update clients set client_id=?,client_password=?,client_name=?,client_email=?,client_type=?,client_status=?,rateplan_id=?,client_credit_limit=?,prefix=?,client_call_limit=?,parent_id=?,is_icx=?,number_of_cct_or_bw=?,mother_company_id=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            logger.debug("sqL:::" + sql);
            ps.setString(1, p_dto.getClient_id());
            ps.setString(2, p_dto.getClient_password());
            ps.setString(3, p_dto.getClient_name());
            ps.setString(4, p_dto.getClient_email());
            ps.setInt(5, p_dto.getClient_type());
            ps.setInt(6, p_dto.getClient_status());
            ps.setLong(7, p_dto.getRateplan_id());
            ps.setDouble(8, Double.parseDouble(p_dto.getClient_credit_limit()));
            ps.setString(9, p_dto.getPrefix());
            ps.setInt(10, p_dto.getClient_call_limit());
            ps.setInt(11, p_dto.getParent_id());
            ps.setInt(12, p_dto.getIs_icx());
            ps.setInt(13, p_dto.getNumber_of_cct_or_bw());
            ps.setLong(14, p_dto.getMother_company_id());
            ps.executeUpdate();

            /*
            //Gateway changed to another parent
            sql = "select id from gateway where client_id=" + p_dto.getId();
            resultSet = ps.executeQuery(sql);
            int gateway_id = 0;
            while (resultSet.next()) {
            gateway_id = resultSet.getInt("id");
            }
            if (gateway_id > 0) {
            sql = "update gateway set owner_id=? where id =" + gateway_id;
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setInt(1, p_dto.getParent_id());
            ps.executeUpdate();
            GatewayLoader.getInstance().forceReload();
            }
             */

            ClientLoader.getInstance().forceReload();
            UserLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteClient(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "select id from gateway where gateway_delete=0 and client_id in(" + cid + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            ArrayList<Long> gateway_ids = new ArrayList<Long>();
            while (resultSet.next()) {
                gateway_ids.add(resultSet.getLong("id"));
            }
            if (gateway_ids.size() > 0) {
                long gate_ids[] = new long[gateway_ids.size()];
                int incr = 0;
                for (long gateway_id : gateway_ids) {
                    gate_ids[incr++] = gateway_id;
                }
                try {
                    GatewayDAO gatewayDAO = new GatewayDAO();
                    gatewayDAO.multipleGatewayDelete(gate_ids);
                } catch (Exception ex) {
                    logger.debug("Exception during gateway delete-->" + ex);
                }
            }

            String selectedNamesString = "-1";
            sql = "select gateway_name from gateway where gateway_delete=0 and client_id=" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                selectedNamesString += ",'" + resultSet.getString("gateway_name") + "'";
            }
            if (selectedNamesString.length() > 2) {
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage(selectedNamesString.substring(3) + " gateways(s) is/are attached with this client.<div class='clear'></div>Please make free the gateway(s) first.");
            } else {
                sql = "update clients set client_delete = 1 where id =" + cid;
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    ClientLoader.getInstance().forceReload();
                    UserLoader.getInstance().forceReload();
                    error.setErrorType(MyAppError.NoError);
                    error.setErrorMessage("Client has been deleted successfuly.");
                } else {
                    error.setErrorType(MyAppError.OtherError);
                    error.setErrorMessage("Client did not deleted. Please try again");
                }
            }
        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientDelete(long clientIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            String error_message = "";
            String selectedNamesString = "-1";
            String idsWithGateway = "-1";
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "select id from gateway where gateway_delete=0 and client_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            ArrayList<Long> gateway_ids = new ArrayList<Long>();
            while (resultSet.next()) {
                gateway_ids.add(resultSet.getLong("id"));
            }
            if (gateway_ids.size() > 0) {
                long gate_ids[] = new long[gateway_ids.size()];
                int incr = 0;
                for (long gateway_id : gateway_ids) {
                    gate_ids[incr++] = gateway_id;
                }
                try {
                    GatewayDAO gatewayDAO = new GatewayDAO();
                    gatewayDAO.multipleGatewayDelete(gate_ids);
                } catch (Exception ex) {
                    logger.debug("Exception during gateway delete-->" + ex);
                }
            }

            sql = "select gateway_name,client_id from gateway where gateway_delete=0 and client_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                idsWithGateway += "," + resultSet.getLong("client_id");
                selectedNamesString += ",'" + resultSet.getString("gateway_name") + "'";
            }

            sql = "update clients set client_delete = 1 where id in(" + selectedIdsString + ") and id not in(" + idsWithGateway + ")";
            logger.debug("client delete sql--->" + sql);
            if (idsWithGateway.length() > 2) {
                error_message = selectedNamesString.substring(3) + " gateways(s) is/are attached with some clients.<div class='clear'></div>Please make free the gateway(s) first.";
            }
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ClientLoader.getInstance().forceReload();
                UserLoader.getInstance().forceReload();
                if (idsWithGateway.length() > 2) {
                    error.setErrorType(MyAppError.PartialUpdated);
                    error.setErrorMessage("Some clients are deleted and some are not, " + error_message);
                } else {
                    error.setErrorType(MyAppError.Updated);
                    error.setErrorMessage("Client has been deleted successfuly.");
                }
            } else {
                error.setErrorType(MyAppError.NotUpdated);
                if (idsWithGateway.length() > 2) {
                    error.setErrorMessage(error_message);
                } else {
                    error.setErrorMessage("Client did not deleted. Please try again.");
                }
            }

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientStatusUpdate(long clientIds[], int client_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String[] multipleNameList = new String[1000];
        int i = 0;
        int sts = 0;
        int mvts_sts = 0;

        String selectedIdsString = Utils.implodeArray(clientIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update clients set client_status = " + client_status + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ClientLoader.getInstance().forceReload();
                UserLoader.getInstance().forceReload();
            }
            if (client_status == 0) {
                sts = 0;
            } else {
                sts = 1;
            }

            sql = "update gateway set gateway_status = " + sts + " where client_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "select gateway_name from gateway" + " where client_id in(" + selectedIdsString + ")";
            resultSet = ps.executeQuery(sql);
            GatewayDTO dto = new GatewayDTO();
            while (resultSet.next()) {
                dto.setGateway_name(resultSet.getString("gateway_name"));
                multipleNameList[i] = "'" + dto.getGateway_name() + "'";
                i++;
            }

            if (client_status == 0) {
                mvts_sts = 1;
            } else {
                mvts_sts = 0;
            }

            String selectedNamesString = Utils.implodeArrayName(multipleNameList, ",");
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_gateway set enable = " + mvts_sts + " where gateway_name in(" + selectedNamesString + ")";
            ps = dbConn.connection.prepareStatement(sql);

            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway did not block. Please try again.");
            }

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientRecharge(long user_id, long clientIds[], double amount[], String des[], LoginDTO login_dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        Statement stmt = null;
        Statement remoteSTMT = null;
        ResultSet rs = null;

        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("OVER_SALES");
        int over_sale = Integer.parseInt(settings.getSettingValue());

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            for (int inc = 0; inc < clientIds.length; inc++) {
                if (login_dto.getOwn_id() > 0 && over_sale == AppConstants.NO) {//not admin or user i,e client or reseller
                    sql = "update clients set client_balance = (client_balance-" + Math.abs(amount[inc]) + ") where id = " + login_dto.getOwn_id();
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.executeUpdate();
                }

                sql = "update clients set client_balance = (client_balance+" + Math.abs(amount[inc]) + ") where id = " + clientIds[inc];
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    sql = "select client_balance from clients where id = " + clientIds[inc];
                    Statement balSTMT = dbConnection.connection.createStatement();
                    ResultSet balRS = balSTMT.executeQuery(sql);
                    if (balRS.next()) {
                        try {
                            sql = "insert into client_transactions(client_id,transaction_type,transaction_recharge,transaction_des,transaction_date,user_id,transaction_by,cur_balance) values(?,?,?,?,?,?,?,?)";
                            ps = dbConnection.connection.prepareStatement(sql);
                            ps.setLong(1, clientIds[inc]);
                            ps.setInt(2, Constants.RECHARGE_AMOUNT);
                            ps.setDouble(3, amount[inc]);
                            ps.setString(4, des[inc]);
                            ps.setLong(5, System.currentTimeMillis());
                            ps.setLong(6, user_id);
                            ps.setLong(7, login_dto.getOwn_id());
                            ps.setFloat(8, balRS.getFloat("client_balance"));
                            ps.executeUpdate();
                        } catch (Exception ex) {
                        }

                        if (balRS.getDouble("client_balance") > 0) {
                            stmt = dbConnection.connection.createStatement();
                            stmt.executeUpdate("update gateway set gateway_status=0 where gateway_delete=0 and client_id=" + clientIds[inc]);
                            stmt = dbConnection.connection.createStatement();
                            rs = stmt.executeQuery("select gateway_name from gateway where gateway_delete=0 and client_id=" + clientIds[inc]);
                            if (rs.next()) {
                                logger.debug("update mvts_gateway set enable=1 where gateway_name='" + rs.getString("gateway_name") + "'");
                                remoteSTMT = dbConn.connection.createStatement();
                                remoteSTMT.executeUpdate("update mvts_gateway set enable=1 where gateway_name='" + rs.getString("gateway_name") + "'");
                            }
                        }

                    }
                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();
            GatewayLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remoteSTMT != null) {
                    remoteSTMT.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientReturn(long user_id, long clientIds[], double amount[], String des[], LoginDTO login_dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        Statement stmt = null;
        Statement remoteSTMT = null;
        ResultSet rs = null;

        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("OVER_SALES");
        int over_sale = Integer.parseInt(settings.getSettingValue());

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            for (int inc = 0; inc < clientIds.length; inc++) {
                sql = "update clients set client_balance = (client_balance-" + Math.abs(amount[inc]) + ") where id = " + clientIds[inc] + " and (client_balance-" + amount[inc] + ") >=0";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    if (login_dto.getOwn_id() > 0 && over_sale == AppConstants.NO) {//not admin or user i,e client or reseller
                        sql = "update clients set client_balance = (client_balance+" + Math.abs(amount[inc]) + ") where id = " + login_dto.getOwn_id();
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.executeUpdate();
                    }
                    sql = "select client_balance from clients where id = " + clientIds[inc];
                    Statement balSTMT = dbConnection.connection.createStatement();
                    ResultSet balRS = balSTMT.executeQuery(sql);
                    if (balRS.next()) {
                        sql = "insert into client_transactions(client_id,transaction_type,transaction_return,transaction_des,transaction_date,user_id,transaction_by,cur_balance) values(?,?,?,?,?,?,?,?)";
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.setLong(1, clientIds[inc]);
                        ps.setInt(2, Constants.RETURN_AMOUNT);
                        ps.setDouble(3, amount[inc]);
                        ps.setString(4, des[inc]);
                        ps.setLong(5, System.currentTimeMillis());
                        ps.setLong(6, user_id);
                        ps.setLong(7, login_dto.getOwn_id());
                        ps.setFloat(8, balRS.getFloat("client_balance"));
                        ps.executeUpdate();

                        if (balRS.getDouble("client_balance") <= 0) {
                            stmt = dbConnection.connection.createStatement();
                            stmt.executeUpdate("update gateway set gateway_status=1 where gateway_delete=0 and client_id=" + clientIds[inc]);
                            rs = stmt.executeQuery("select gateway_name from gateway where gateway_delete=0 and client_id=" + clientIds[inc]);
                            if (rs.next()) {
                                logger.debug("update mvts_gateway set enable=0 where gateway_name='" + rs.getString("gateway_name") + "'");
                                remoteSTMT = dbConn.connection.createStatement();
                                remoteSTMT.executeUpdate("update mvts_gateway set enable=0 where gateway_name='" + rs.getString("gateway_name") + "'");
                            }
                        }
                    }
                } else {
                    error.setErrorType(2);
                    error.setErrorMessage("Your available balance is less than the return amount");
                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();
            GatewayLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remoteSTMT != null) {
                    remoteSTMT.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleClientReceive(long user_id, long clientIds[], double amount[], String des[], LoginDTO login_dto) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            for (int inc = 0; inc < clientIds.length; inc++) {
                sql = "select client_balance from clients where id = " + clientIds[inc];
                Statement balSTMT = dbConnection.connection.createStatement();
                ResultSet balRS = balSTMT.executeQuery(sql);
                if (balRS.next()) {
                    sql = "insert into client_transactions(client_id,transaction_type,transaction_receive,transaction_des,transaction_date,user_id,transaction_by,cur_balance) values(?,?,?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, clientIds[inc]);
                    ps.setInt(2, Constants.RECEIVE_AMOUNT);
                    ps.setDouble(3, amount[inc]);
                    ps.setString(4, des[inc]);
                    ps.setLong(5, System.currentTimeMillis());
                    ps.setLong(6, user_id);
                    ps.setLong(7, login_dto.getOwn_id());
                    ps.setFloat(8, balRS.getFloat("client_balance"));
                    ps.executeUpdate();
                }
            }

            ClientLoader.getInstance().forceReload();
            TransactionLoader.getInstance().forceReload();

        } catch (Exception ex) {
            logger.fatal("Error while editing client: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
