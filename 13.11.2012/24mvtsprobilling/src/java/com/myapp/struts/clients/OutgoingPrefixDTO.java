/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

/**
 *
 * @author Administrator
 */
public class OutgoingPrefixDTO {

    private int id;
    private int client_id;
    private String outgoing_prefix;
    private String outgoing_to;

    public OutgoingPrefixDTO() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public String getOutgoing_prefix() {
        return outgoing_prefix;
    }

    public void setOutgoing_prefix(String outgoing_prefix) {
        this.outgoing_prefix = outgoing_prefix;
    }

    public String getOutgoing_to() {
        return outgoing_to;
    }

    public void setOutgoing_to(String outgoing_to) {
        this.outgoing_to = outgoing_to;
    }
}
