package com.myapp.struts.user;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class UserTaskSchedular {

    public UserTaskSchedular() {
    }

    public MyAppError addUserInformation(UserDTO p_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.addUserInformation(p_dto);
    }

    public MyAppError editUserInformation(UserDTO p_dto) {
        UserDAO userDAO = new UserDAO();
        return userDAO.editUserInformation(p_dto);
    }

    public UserDTO getUserDTO(long id) {
        return UserLoader.getInstance().getUserDTOByID(id);
    }

    public ArrayList<UserDTO> getUserDTOsSorted(LoginDTO l_dto) {
        return UserLoader.getInstance().getUserDTOsSorted();
    }

    public ArrayList<UserDTO> getUserDTOs(LoginDTO l_dto) {
        return UserLoader.getInstance().getUserDTOList();
    }

    public ArrayList<UserDTO> getUserDTOsWithSearchParam(UserDTO udto, LoginDTO l_dto) {
        return UserLoader.getInstance().getUserDTOsWithSearchParam(udto);
    }

    public MyAppError deleteUser(int cid) {
        UserDAO dao = new UserDAO();
        return dao.deleteUser(cid);
    }
}
