package com.myapp.struts.user;

public class UserDTO {

    public boolean searchWithUserID = false;
    public boolean searchWithStatus = false;
    private boolean superUser;
    private int userStatus;
    private long id;
    private String userTypeList;
    private String userStatusName;
    private String userId;
    private String userPassword;
    private String fullName;
    private int clientType;
    private double client_blance;
    private int is_deleted;
    private int userRoleId;
    private String userRoleName;
    private int parentId;
    private int clientLevel;

    public UserDTO() {
        superUser = false;
    }

    public boolean getSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserTypeList() {
        return userTypeList;
    }

    public void setUserTypeList(String userTypeList) {
        this.userTypeList = userTypeList;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(int userStatus) {
        this.userStatus = userStatus;
    }

    public void setUserStatusName(String userStatusName) {
        this.userStatusName = userStatusName;
    }

    public String getUserStatusName() {
        return userStatusName;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public double getClient_blance() {
        return client_blance;
    }

    public void setClient_blance(double client_blance) {
        this.client_blance = client_blance;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }

    public int getClientLevel() {
        return clientLevel;
    }

    public void setClientLevel(int clientLevel) {
        this.clientLevel = clientLevel;
    }
    
}
