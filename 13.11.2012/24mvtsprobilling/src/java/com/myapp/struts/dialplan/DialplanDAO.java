package com.myapp.struts.dialplan;

import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class DialplanDAO {

    static Logger logger = Logger.getLogger(DialplanDAO.class.getName());

    public DialplanDAO() {
    }

    public MyAppError addDialplanInformation(DialplanDTO p_dto) {
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        Statement statement = null;
        DBConnection db = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "select dialpeer_name from mvts_dialpeer where dialpeer_name=? and stat_enable=1";
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDialplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dialplan");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into mvts_dialpeer(dialpeer_name,description,enable,dnis_pattern,capacity,ani_translate,dnis_translate,priority,hunt_mode,gateway_list,stat_enable,hunt_stop,sched_type,sched_tod_on,sched_tod_off) ";
            sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            ps = dbConn.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, p_dto.getDialplan_name());
            ps.setString(2, p_dto.getDialplan_description());
            ps.setInt(3, p_dto.getDialplan_enable());
            ps.setString(4, p_dto.getDialplan_dnis_pattern());
            ps.setInt(5, p_dto.getDialplan_capacity());
            ps.setString(6, p_dto.getDialplan_ani_translate());
            ps.setString(7, p_dto.getDialplan_dnis_translate());
            ps.setInt(8, p_dto.getDialplan_priority());
            ps.setInt(9, p_dto.getDialplan_hunt_mode());
            ps.setString(10, p_dto.getDialplan_gateway_list());
            ps.setInt(11, 1);
            ps.setInt(12, 0);
            ps.setInt(13, p_dto.getDialplan_sched_type());
            ps.setString(14, p_dto.getDialplan_sched_tod_on());
            ps.setString(15, p_dto.getDialplan_sched_tod_off());
            ps.executeUpdate();

            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                int dial_peer_id = rs.getInt(1);
                sql = "insert into mvts_dialplan(dialplan_id,client_id) ";
                sql += "values(?,?)";
                db = databaseconnector.DBConnector.getInstance().makeConnection();
                ps = db.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, dial_peer_id);
                ps.setInt(2, p_dto.getClient_id());
                logger.debug(Utils.getSQLString(ps));
                ps.executeUpdate();

                error.setErrorMessage(dial_peer_id + "");
                DialplanLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding dialplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editDialplanInformation(DialplanDTO p_dto) {
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        DBConnection db = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "select dialpeer_name from mvts_dialpeer where dialpeer_name = ? and dialpeer_id!=" + p_dto.getId() + " and enable=1";
            ps = dbConn.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getDialplan_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Dialplan.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update mvts_dialpeer set dialpeer_name=?,description=?,enable=?,dnis_pattern=?,capacity=?,ani_translate=?,dnis_translate=?,priority=?,hunt_mode=?,gateway_list=?,sched_type=?,sched_tod_on=?,sched_tod_off=?  where dialpeer_id=" + p_dto.getId();
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getDialplan_name());
            ps.setString(2, p_dto.getDialplan_description());
            ps.setInt(3, p_dto.getDialplan_enable());
            ps.setString(4, p_dto.getDialplan_dnis_pattern());
            ps.setInt(5, p_dto.getDialplan_capacity());
            ps.setString(6, p_dto.getDialplan_ani_translate());
            ps.setString(7, p_dto.getDialplan_dnis_translate());
            ps.setInt(8, p_dto.getDialplan_priority());
            ps.setInt(9, p_dto.getDialplan_hunt_mode());
            ps.setString(10, p_dto.getDialplan_gateway_list());
            ps.setInt(11, p_dto.getDialplan_sched_type());
            ps.setString(12, p_dto.getDialplan_sched_tod_on());
            ps.setString(13, p_dto.getDialplan_sched_tod_off());

            ps.executeUpdate();
//
//            sql = "update mvts_dialplan set client_id=? ";
//            sql += "where dialplan_id=" + p_dto.getId();
//            db = databaseconnector.DBConnector.getInstance().makeConnection();
//            ps = db.connection.prepareStatement(sql);
//            ps.setInt(1, p_dto.getClient_id());
//            ps.executeUpdate();

            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing dialplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError updateDialpeer(ArrayList<String> dialplans, long gateway_id, boolean removed) {
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "";
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            for (String dialplan_name : dialplans) {
                try {
                    if (removed) {
                        sql = "update mvts_dialpeer set gateway_list=REPLACE(gateway_list, '" + gateway_id + ";','') where dialpeer_name='" + dialplan_name + "'";
                        ps = dbConn.connection.prepareStatement(sql);
                        ps.executeUpdate();

                        stmt = dbConn.connection.createStatement();
                        rs = stmt.executeQuery("select gateway_list from mvts_dialpeer where dialpeer_name='" + dialplan_name + "'");
                        if (rs.next()) {
                            if (rs.getString("gateway_list").length() == 0) {
                                ps = dbConn.connection.prepareStatement("delete from mvts_dialpeer where dialpeer_name='" + dialplan_name + "'");
                                ps.executeUpdate();
                            }
                        }
                    } else {
                        sql = "update mvts_dialpeer set gateway_list=CONCAT(REPLACE(gateway_list, '" + gateway_id + ";', ''), '" + gateway_id + ";') where dialpeer_name='" + dialplan_name + "'";
                        ps = dbConn.connection.prepareStatement(sql);
                        ps.executeUpdate();
                    }
                    
                    logger.debug("update dialpeer sql-->" + sql);

                } catch (Exception ex) {
                    logger.debug("Exception during add/removing gateway from dialpeer-->" + ex);
                }
            }


            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing dialplan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteDialplan(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        DBConnection db = null;
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            db = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "delete from mvts_dialpeer where dialpeer_id =" + cid;
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                sql = "delete from mvts_dialplan where dialplan_id =" + cid;
                ps = db.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    DialplanLoader.getInstance().forceReload();
                }
            }
        } catch (Exception ex) {
            logger.fatal("Error while deleting dialpeer: ", ex);
        } finally {

            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDialplanStatusUpdate(long dialplanIds[], int dialplan_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        PreparedStatement ps = null;
        remotedbconnector.DBConnection dbConn = null;
        DBConnection db = null;

        String selectedIdsString = Utils.implodeArray(dialplanIds, ",");
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            db = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "update mvts_dialpeer set enable = " + dialplan_status + " where dialpeer_id in(" + selectedIdsString + ")";
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                sql = "update mvts_dialplan set status = " + dialplan_status + " where dialplan_id in(" + selectedIdsString + ")";
                ps = db.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    DialplanLoader.getInstance().forceReload();
                }
            }
        } catch (Exception ex) {
            logger.fatal("Error while multipleDialplanStatusUpdate: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDialplanDelete(long dialplanIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        PreparedStatement ps = null;
        remotedbconnector.DBConnection dbConn = null;
        DBConnection db = null;
        String selectedIdsString = Utils.implodeArray(dialplanIds, ",");
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            db = databaseconnector.DBConnector.getInstance().makeConnection();

            sql = "delete from mvts_dialpeer where dialpeer_id in(" + selectedIdsString + ")";
            ps = dbConn.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                sql = "delete from mvts_dialplan where dialplan_id in(" + selectedIdsString + ")";
                ps = db.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    DialplanLoader.getInstance().forceReload();
                }
            }

        } catch (Exception ex) {
            logger.fatal("Error while multipleDialplanDelete-> ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
