package com.myapp.struts.dialplan;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;

public class DialplanLoader {

    static Logger logger = Logger.getLogger(DialplanLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<DialplanDTO> dialplanList = null;
    private HashMap<Integer, DialplanDTO> dialplanDTOByID = null;
    private HashMap<Integer, Integer> dialplanClientsMap = null;
    private HashMap<Integer, DialplanDTO> dialplanDTOByDialpeerID = null;
    static DialplanLoader dialplanLoader = null;
    private ArrayList<DialplanDTO> gatewayList = null;

    public DialplanLoader() {
        forceReload();
    }

    public static DialplanLoader getInstance() {
        if (dialplanLoader == null) {
            createDialplanLoader();
        }
        return dialplanLoader;
    }

    private synchronized static void createDialplanLoader() {
        if (dialplanLoader == null) {
            dialplanLoader = new DialplanLoader();
        }
    }

    private void reload() {
        remotedbconnector.DBConnection dbConn = null;
        Statement statement = null;
        Statement stmt = null;
        DBConnection db = null;
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            statement = dbConn.connection.createStatement();

            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();

            dialplanList = new ArrayList<DialplanDTO>();
            dialplanDTOByID = new HashMap<Integer, DialplanDTO>();
            dialplanDTOByDialpeerID = new HashMap<Integer, DialplanDTO>();
            gatewayList = new ArrayList<DialplanDTO>();
            dialplanClientsMap = new HashMap<Integer, Integer>();

            HashMap<String, Integer> gatewayOwnerIdMap = new HashMap<String, Integer>();

            String sql = "select gateway_name,owner_id from gateway where gateway_type=1 or gateway_type=2";
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                gatewayOwnerIdMap.put(rs.getString("gateway_name"), rs.getInt("owner_id"));
            }

            sql = "select gateway_id,gateway_name from mvts_gateway where description='Termination' or description='Both'";
            ResultSet resultSet = statement.executeQuery(sql);
            HashMap<Integer, String> names = new HashMap<Integer, String>();
            DialplanDTO dto = new DialplanDTO();
            while (resultSet.next()) {
                dto = new DialplanDTO();
                dto.setGateway_id_single(resultSet.getInt("gateway_id"));
                dto.setGateway_name_single(resultSet.getString("gateway_name"));
                names.put(resultSet.getInt("gateway_id"), resultSet.getString("gateway_name"));
                dto.setGateway_owner_id(-1);
                try {
                    dto.setGateway_owner_id(gatewayOwnerIdMap.get(dto.getGateway_name_single()));
                } catch (Exception ex) {
                }
                gatewayList.add(dto);
                dialplanDTOByDialpeerID.put(dto.getGateway_id_single(), dto);
            }

            sql = "select * from mvts_dialplan";
            stmt = db.connection.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                dialplanClientsMap.put(rs.getInt("dialplan_id"), rs.getInt("client_id"));
            }

            sql = "select dialpeer_id,dialpeer_name,description,enable,dnis_pattern,capacity,ani_translate,dnis_translate,priority,hunt_mode,gateway_list,sched_type,sched_tod_on,sched_tod_off from mvts_dialpeer order by dialpeer_id DESC";
            logger.debug("Dialplan SQL-->" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dto = new DialplanDTO();
                dto.setId(resultSet.getInt("dialpeer_id"));
                dto.setClient_id(-1);
                try {
                    dto.setClient_id(dialplanClientsMap.get(dto.getId()));
                } catch (Exception ex) {
                    logger.debug("dialplan client mactching exception-->" + ex);
                }
                dto.setDialplan_name(resultSet.getString("dialpeer_name"));
                dto.setDialplan_description(resultSet.getString("description"));
                dto.setDialplan_enable(resultSet.getInt("enable"));
                dto.setDialplan_enable_name(Constants.DIALPLAN_STATUS_STRING[dto.getDialplan_enable()]);
                dto.setDialplan_dnis_pattern_without_modify(resultSet.getString("dnis_pattern"));

                String dnis_pattern = "";
                String temps[] = dto.getDialplan_dnis_pattern_without_modify().split(";");
                for (String str : temps) {
                    int index = str.indexOf("[");
                    if (index > -1) {
                        dnis_pattern += str.substring(0, index) + ";";
                    }
                }
                if (dnis_pattern.length() > 0) {
                    dnis_pattern = dnis_pattern.substring(0, dnis_pattern.length() - 1);
                }

                dto.setDialplan_dnis_pattern(dnis_pattern);
                dto.setDialplan_capacity(resultSet.getInt("capacity"));
                dto.setDialplan_ani_translate(resultSet.getString("ani_translate"));
                dto.setDialplan_dnis_translate(resultSet.getString("dnis_translate"));
                dto.setDialplan_priority(resultSet.getInt("priority"));
                dto.setDialplan_hunt_mode(resultSet.getInt("hunt_mode"));
                dto.setDialplan_hunt_mode_name(Constants.DIALPLAN_BALANCING_METHOD_STRING[dto.getDialplan_hunt_mode()]);
                dto.setDialplan_gateway_list(resultSet.getString("gateway_list"));
                dto.setDialplan_gateway_list_array(Utils.explodeArray(dto.getDialplan_gateway_list()));
                String temp = "";
                for (int i = 0; i < dto.getDialplan_gateway_list_array().length; i++) {
                    if (names.get(dto.getDialplan_gateway_list_array()[i]) != null) {
                        temp += "* " + names.get(dto.getDialplan_gateway_list_array()[i]) + "<br>";
                    }
                }
                dto.setDialplan_gateway_list_name(temp);
                dto.setDialplan_sched_type(resultSet.getInt("sched_type"));
                dto.setDialplan_sched_tod_on(resultSet.getString("sched_tod_on"));
                dto.setDialplan_sched_tod_off(resultSet.getString("sched_tod_off"));
                dto.setDialplan_sched_type_name(Constants.DIALPLAN_SCHEDULING_STRING[dto.getDialplan_sched_type()]);
                if (dto.getDialplan_sched_tod_on() == null) {
                    dto.setDialplan_sched_tod_on_name(null);
                } else {
                    dto.setDialplan_sched_tod_on_name(dto.getDialplan_sched_tod_on().substring(0, 2) + ":" + dto.getDialplan_sched_tod_on().substring(2));
                }
                if (dto.getDialplan_sched_tod_off() == null) {
                    dto.setDialplan_sched_tod_off_name(null);
                } else {
                    dto.setDialplan_sched_tod_off_name(dto.getDialplan_sched_tod_off().substring(0, 2) + ":" + dto.getDialplan_sched_tod_off().substring(2));
                }

                dto.setSuperUser(false);
                dialplanDTOByID.put(dto.getId(), dto);
                dialplanList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in DialplanLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<DialplanDTO> getDialplanDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<DialplanDTO> newList = new ArrayList<DialplanDTO>();
        for (DialplanDTO dto : dialplanList) {
            if (dto.getClient_id() == login_dto.getOwn_id()) {
                newList.add(dto);
            }
        }
        return newList;
    }

    public synchronized DialplanDTO getDialplanDTOByID(int id) {
        checkForReload();
        return dialplanDTOByID.get(id);
    }

    public synchronized DialplanDTO getDialplanDTOByDialpeerID(int id) {
        checkForReload();
        return dialplanDTOByDialpeerID.get(id);
    }

    public synchronized ArrayList<DialplanDTO> getGatewayList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<DialplanDTO> newList = new ArrayList<DialplanDTO>();
        for (DialplanDTO dto : gatewayList) {
            if (dto.getGateway_owner_id() == login_dto.getOwn_id()) {
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<DialplanDTO> getDialplanDTOsWithSearchParam(DialplanDTO udto, LoginDTO login_dto) {
        ArrayList<DialplanDTO> newList = new ArrayList<DialplanDTO>();
        checkForReload();
        ArrayList<DialplanDTO> list = dialplanList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                DialplanDTO dto = (DialplanDTO) i.next();
                if ((udto.searchWithDialplanName && !dto.getDialplan_name().contains(udto.getDialplan_name()))
                        || (udto.searchWithGatewayName && !dto.getDialplan_gateway_list_name().contains(udto.getDialplan_gateway_list_name()))
                        || (udto.searchWithPrefix && !dto.getDialplan_dnis_pattern().startsWith(udto.getDialplan_dnis_pattern()))
                        || (udto.searchWithStatus && dto.getDialplan_enable() != udto.getDialplan_enable())) {
                    continue;
                }
                newList.add(dto);
            }
        }

        ArrayList<DialplanDTO> finalList = new ArrayList<DialplanDTO>();
        for (DialplanDTO dto : newList) {
            if (dto.getClient_id() == login_dto.getOwn_id()) {
                finalList.add(dto);
            }
        }
        return finalList;
    }

    public ArrayList<DialplanDTO> getDialplanDTOsSorted() {
        checkForReload();
        ArrayList<DialplanDTO> list = dialplanList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    DialplanDTO dto1 = (DialplanDTO) o1;
                    DialplanDTO dto2 = (DialplanDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
