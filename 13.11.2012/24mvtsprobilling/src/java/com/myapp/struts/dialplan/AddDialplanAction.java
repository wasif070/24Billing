package com.myapp.struts.dialplan;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;

public class AddDialplanAction extends Action {

    static Logger logger = Logger.getLogger(AddDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
        if (login_dto != null && login_dto.getSuperUser() && Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
            DialplanForm formBean = (DialplanForm) form;
            DialplanDTO dto = new DialplanDTO();
            DialplanTaskSchedular scheduler = new DialplanTaskSchedular();
            String on_hour = "";
            String on_min = "";
            String off_hour = "";
            String off_min = "";

            dto.setDialplan_name(formBean.getDialplan_name());
            String gateway_ids = "";
            for (int i = 0; i < formBean.getGateway_id().length; i++) {
                gateway_ids += formBean.getGateway_id()[i] + ";";
            }
            String dialplan_dnis_pattern = "";
            String temp[] = formBean.getDialplan_dnis_pattern().split(";");
            for (String pattern : temp) {
                dialplan_dnis_pattern += pattern + "[0-9]*;";
            }
            dialplan_dnis_pattern = dialplan_dnis_pattern.substring(0, dialplan_dnis_pattern.length() - 1);

            dto.setGateway_id(formBean.getGateway_id());
            dto.setDialplan_gateway_list(gateway_ids);
            dto.setDialplan_description(formBean.getDialplan_description());
            dto.setDialplan_dnis_pattern(dialplan_dnis_pattern);
            dto.setDialplan_ani_translate(formBean.getDialplan_ani_translate());
            dto.setDialplan_dnis_translate(formBean.getDialplan_dnis_translate());
            dto.setDialplan_hunt_mode(formBean.getDialplan_hunt_mode());
            dto.setDialplan_priority(formBean.getDialplan_priority());
            dto.setDialplan_capacity(formBean.getDialplan_capacity());
            dto.setDialplan_enable(formBean.getDialplan_enable());
            dto.setDialplan_sched_type(formBean.getDialplan_sched_type());
            dto.setClient_id((int) login_dto.getOwn_id());
            if (formBean.getOnhour() < 10) {
                on_hour = "0" + String.valueOf(formBean.getOnhour());
            } else {
                on_hour = String.valueOf(formBean.getOnhour());
            }
            if (formBean.getOnmin() < 10) {
                on_min = "0" + String.valueOf(formBean.getOnmin());
            } else {
                on_min = String.valueOf(formBean.getOnmin());
            }
            dto.setDialplan_sched_tod_on(on_hour + on_min);

            if (formBean.getOffhour() < 10) {
                off_hour = "0" + String.valueOf(formBean.getOffhour());
            } else {
                off_hour = String.valueOf(formBean.getOffhour());
            }
            if (formBean.getOffmin() < 10) {
                off_min = "0" + String.valueOf(formBean.getOffmin());
            } else {
                off_min = String.valueOf(formBean.getOffmin());
            }
            dto.setDialplan_sched_tod_off(off_hour + off_min);

            MyAppError error = scheduler.addDialplanInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getDialplanDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("dialpeer");
                ac_dto.setPrimaryKey(dto.getDialplan_name());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "Dialplan is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
