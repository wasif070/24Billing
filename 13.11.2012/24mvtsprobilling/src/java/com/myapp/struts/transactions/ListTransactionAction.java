package com.myapp.struts.transactions;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Ashraful
 */
public class ListTransactionAction extends Action {

    static Logger logger = Logger.getLogger(ListTransactionAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;

            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }

            int pageNo = 1;
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            TransactionDTO cdto = new TransactionDTO();
            TransactionTaskScheduler scheduler = new TransactionTaskScheduler();
            TransactionForm transactionForm = (TransactionForm) form;

            if (list_all == 0) {
                String fromDate = transactionForm.getFromYear() + "-" + formatter.format(transactionForm.getFromMonth()) + "-" + formatter.format(transactionForm.getFromDay()) + " " + formatter.format(transactionForm.getFromHour()) + ":" + formatter.format(transactionForm.getFromMin()) + ":00";
                cdto.setFromDate(fromDate);
                String toDate = transactionForm.getToYear() + "-" + formatter.format(transactionForm.getToMonth()) + "-" + formatter.format(transactionForm.getToDay()) + " " + formatter.format(transactionForm.getToHour()) + ":" + formatter.format(transactionForm.getToMin()) + ":00";
                cdto.setToDate(toDate);
                if (transactionForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, transactionForm.getRecordPerPage());
                }
                if (transactionForm.getClient_id() > 0) {
                    cdto.setClient_id(transactionForm.getClient_id());
                    cdto.searchWithClientID = true;
                }
                cdto.setTransaction_type(transactionForm.getTransaction_type());
                transactionForm.setTransactionList(scheduler.getTransactionDTOsWithSearchParam(cdto, login_dto));
            } else {
                Calendar cal = new GregorianCalendar();
                int month = cal.get(Calendar.MONTH) + 1;
                int year = cal.get(Calendar.YEAR);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                transactionForm.setFromDay(day);
                transactionForm.setFromMonth(month);
                transactionForm.setFromYear(year);
                transactionForm.setToDay(day);
                transactionForm.setToMonth(month);
                transactionForm.setToYear(year);
                transactionForm.setToHour(23);
                transactionForm.setToMin(59);
                transactionForm.setFromHour(0);
                transactionForm.setFromMin(0);
                String fromDate = transactionForm.getFromYear() + "-" + formatter.format(transactionForm.getFromMonth()) + "-" + formatter.format(transactionForm.getFromDay()) + " 00:00:00";
                cdto.setFromDate(fromDate);
                String toDate = transactionForm.getToYear() + "-" + formatter.format(transactionForm.getToMonth()) + "-" + formatter.format(transactionForm.getToDay()) + " 23:59:59";
                cdto.setToDate(toDate);
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    transactionForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                cdto.setTransaction_type(transactionForm.getTransaction_type());
                transactionForm.setTransactionList(scheduler.getTransactionDTOs(cdto, login_dto));
            }
            if (transactionForm.getTransactionList() != null && transactionForm.getTransactionList().size() <= (transactionForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
