/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

/**
 *
 * @author Ashraful
 */
public class ClientSummeryAction extends Action {

    static Logger logger = Logger.getLogger(ListTransactionAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && !login_dto.getSuperUser()) {
            TransactionTaskScheduler scheduler = new TransactionTaskScheduler();
            TransactionDTO tdto = scheduler.getTransactionSummeryByClient(login_dto.getId());
            if (tdto != null) {
                request.getSession(true).setAttribute("BalanceDTO", tdto);
            } else {
                request.getSession(true).setAttribute("BalanceDTO", null);
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}