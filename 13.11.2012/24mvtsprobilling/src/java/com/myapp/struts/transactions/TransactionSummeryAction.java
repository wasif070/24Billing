/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.transactions;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

/**
 *
 * @author Ashraful
 */
public class TransactionSummeryAction extends Action {

    static Logger logger = Logger.getLogger(ListTransactionAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
                if (status == true) {
                    pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
            }
            TransactionTaskScheduler scheduler = new TransactionTaskScheduler();
            TransactionForm transactionForm = (TransactionForm) form;

            if (list_all == 0) {
                if (transactionForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, transactionForm.getRecordPerPage());
                }
                TransactionDTO cdto = new TransactionDTO();
                if (transactionForm.getClient_id() > 0) {
                    cdto.setClient_id(transactionForm.getClient_id());
                    cdto.searchWithClientID = true;
                }

                transactionForm.setTransactionList(scheduler.getTransactionSummeryDTOsWithSearchParam(cdto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    transactionForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                transactionForm.setTransactionList(scheduler.setTransactionSummeryList(login_dto));
            }
            if (transactionForm.getTransactionList() != null && transactionForm.getTransactionList().size() <= (transactionForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}