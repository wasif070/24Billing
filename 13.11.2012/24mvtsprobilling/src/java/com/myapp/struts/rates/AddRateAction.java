/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.rateplan.AddRateplanAction;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class AddRateAction extends Action {

    static Logger logger = Logger.getLogger(AddRateplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        long rate_id = Long.parseLong(request.getSession(true).getAttribute("sess_rate_id").toString());
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser() && rate_id > 0) {

            String[] rate_day = request.getParameterValues("rate_day[]");
            String[] rate_fromhour = request.getParameterValues("rate_fromhour[]");
            String[] rate_frommin = request.getParameterValues("rate_frommin[]");
            String[] rate_tohour = request.getParameterValues("rate_tohour[]");
            String[] rate_tomin = request.getParameterValues("rate_tomin[]");

            RateForm formBean = (RateForm) form;
            formBean.setAction(Constants.ADD);
            RateDTO dto = new RateDTO();
            RateTaskSchedular scheduler = new RateTaskSchedular();
            dto.setRateplan_id(rate_id);
            dto.setRate_destination_code(formBean.getRate_destination_code());
            dto.setRate_destination_name(formBean.getRate_destination_name());
            dto.setRate_status(formBean.getRate_status());

            dto.setRate_day(rate_day);
            dto.setRate_fromhour(rate_fromhour);
            dto.setRate_frommin(rate_frommin);
            dto.setRate_tohour(rate_tohour);
            dto.setRate_tomin(rate_tomin);

            dto.setRatePerMin(request.getParameterValues("ratePerMin[]"));
            dto.setFirstPulse(request.getParameterValues("firstPulse[]"));
            dto.setNextPulse(request.getParameterValues("nextPulse[]"));
            dto.setGracePeriod(request.getParameterValues("gracePeriod[]"));
            dto.setFailedPeriod(request.getParameterValues("failedPeriod[]"));

            MyAppError error = scheduler.addRateInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(error.getErrorMessage());
            } else {

                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getRateDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("rate");
                ac_dto.setPrimaryKey(dto.getRate_destination_code());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage("Rate is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
