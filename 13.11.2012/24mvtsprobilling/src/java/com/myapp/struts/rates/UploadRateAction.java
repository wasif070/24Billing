/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.io.DataInputStream;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Ashraful
 */
public class UploadRateAction extends Action {

    static Logger logger = Logger.getLogger(UploadRateAction.class.getName());

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            RateForm myForm = (RateForm) form;

            FormFile myFile = myForm.getTheFile();
            String fileName = myFile.getFileName();
            if ((fileName == null) || fileName.indexOf("csv") == -1) {
                request.getSession(true).setAttribute(Constants.MESSAGE, "Invalid File Type!");
                return mapping.findForward("failure");
            } else {
                String thisLine;
                DataInputStream myInput = new DataInputStream(myFile.getInputStream());

                ArrayList<RateDTO> dtoList = new ArrayList<RateDTO>();
                long rate_id = 0;
                rate_id = Long.parseLong(request.getSession(true).getAttribute("sess_rate_id").toString());
                while ((thisLine = myInput.readLine()) != null) {
                    if (thisLine.trim().length() > 0) {
                        String strar[] = thisLine.split(",");
                        if (strar.length >= 6) {
                            RateDTO dto = new RateDTO();
                            dto.setRateplan_id(rate_id);
                            dto.setRate_destination_code(strar[0].trim());
                            dto.setRate_destination_name(strar[1].trim());
                            dto.setRate_per_min(Float.parseFloat(strar[2].trim()));
                            dto.setRate_first_pulse(Integer.parseInt(strar[3].trim()));
                            dto.setRate_next_pulse(Integer.parseInt(strar[4].trim()));
                            dto.setRate_grace_period(Integer.parseInt(strar[5].trim()));
                            dto.setRate_failed_period(Integer.parseInt(strar[6].trim()));
                            dto.setRate_sin_day(Integer.parseInt(strar[7].trim()));
                            dto.setRate_sin_fromhour(Integer.parseInt(strar[8].trim()));
                            dto.setRate_sin_frommin(Integer.parseInt(strar[9].trim()));
                            dto.setRate_sin_tohour(Integer.parseInt(strar[10].trim()));
                            dto.setRate_sin_tomin(Integer.parseInt(strar[11].trim()));
                            dtoList.add(dto);
                        }
                    }
                }
                RateTaskSchedular scheduler = new RateTaskSchedular();
                scheduler.uploadFileData(dtoList);
                logger.debug("Upload List" + dtoList);
                request.getSession(true).setAttribute(Constants.MESSAGE, "Rate Uploaded Successfully!");
            }
        }
        return mapping.findForward("success");
    }
}
