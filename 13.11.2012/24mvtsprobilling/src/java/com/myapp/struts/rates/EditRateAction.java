/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

/**
 *
 * @author Ashraful
 */
import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class EditRateAction extends Action {

    static Logger logger = Logger.getLogger(EditRateAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        RateForm formBean = (RateForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            RateDTO dto = new RateDTO();
            RateTaskSchedular scheduler = new RateTaskSchedular();
            long rate_id = Long.parseLong(request.getSession(true).getAttribute("rate_id").toString());

            String[] rate_day = request.getParameterValues("rate_day[]");
            String[] id = request.getParameterValues("id[]");
            String[] rate_fromhour = request.getParameterValues("rate_fromhour[]");
            String[] rate_frommin = request.getParameterValues("rate_frommin[]");
            String[] rate_tohour = request.getParameterValues("rate_tohour[]");
            String[] rate_tomin = request.getParameterValues("rate_tomin[]");

            dto.setRate_id(rate_id);
            dto.setRateplan_id(formBean.getRateplan_id());
            dto.setRate_destination_code(formBean.getRate_destination_code());
            dto.setRate_destination_name(formBean.getRate_destination_name());
            dto.setRate_status(formBean.getRate_status());
            dto.setRate_day(rate_day);
            dto.setRate_fromhour(rate_fromhour);
            dto.setRate_frommin(rate_frommin);
            dto.setRate_tohour(rate_tohour);
            dto.setRate_tomin(rate_tomin);
            dto.setIds(id);

            dto.setRatePerMin(request.getParameterValues("ratePerMin[]"));
            dto.setFirstPulse(request.getParameterValues("firstPulse[]"));
            dto.setNextPulse(request.getParameterValues("nextPulse[]"));
            dto.setGracePeriod(request.getParameterValues("gracePeriod[]"));
            dto.setFailedPeriod(request.getParameterValues("failedPeriod[]"));

            MyAppError error = scheduler.editRateInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getRateDTO(rate_id)));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("rate");
                ac_dto.setPrimaryKey(dto.getRate_destination_code());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage("Rate is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
