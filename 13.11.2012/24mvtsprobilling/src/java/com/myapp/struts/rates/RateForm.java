/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author Ashraful
 */
public class RateForm extends org.apache.struts.action.ActionForm {

    private long rateplan_id;
    private long rate_id;
    private String rate_destination_code;
    private String rate_destination_name;
    private float rate_per_min;
    private int rate_first_pulse;
    private int rate_next_pulse;
    private int rate_grace_period;
    private int rate_failed_period;
    private int rate_status;
    int action;
    private long[] selectedIDs;
    private String deleteBtn;
    private int pageNo;
    private int recordPerPage;
    private String message;
    private int doValidate;
    private ArrayList rateList;
    private FormFile theFile;
    private String[] rate_day;
    private String[] rate_fromhour;
    private String[] rate_tohour;
    private String[] rate_frommin;
    private String[] rate_tomin;
    private String doSearch;
    private int sortItem;
    private int sortOrder;
//    private int rate_day;
//    private int rate_fromhour;
//    private int rate_tohour;
//    private int rate_frommin;
//    private int rate_tomin;
    private String[] ratePerMin;
    private String[] firstPulse;
    private String[] nextPulse;
    private String[] gracePeriod;
    private String[] failedPeriod;

    public RateForm() {
        rate_first_pulse = 1;
        rate_next_pulse = 1;
    }

    public String getRate_destination_code() {
        return rate_destination_code;
    }

    public void setRate_destination_code(String rate_destination_code) {
        this.rate_destination_code = rate_destination_code;
    }

    public String getRate_destination_name() {
        return rate_destination_name;
    }

    public void setRate_destination_name(String rate_destination_name) {
        this.rate_destination_name = rate_destination_name;
    }

    public int getRate_first_pulse() {
        return rate_first_pulse;
    }

    public void setRate_first_pulse(int rate_first_pulse) {
        this.rate_first_pulse = rate_first_pulse;
    }

    public int getRate_grace_period() {
        return rate_grace_period;
    }

    public void setRate_grace_period(int rate_grace_period) {
        this.rate_grace_period = rate_grace_period;
    }

    public int getRate_failed_period() {
        return rate_failed_period;
    }

    public void setRate_failed_period(int rate_failed_period) {
        this.rate_failed_period = rate_failed_period;
    }

    public long getRate_id() {
        return rate_id;
    }

    public void setRate_id(long rate_id) {
        this.rate_id = rate_id;
    }

    public int getRate_next_pulse() {
        return rate_next_pulse;
    }

    public void setRate_next_pulse(int rate_next_pulse) {
        this.rate_next_pulse = rate_next_pulse;
    }

    public float getRate_per_min() {
        return rate_per_min;
    }

    public void setRate_per_min(float rate_per_min) {
        this.rate_per_min = rate_per_min;
    }

    public int getRate_status() {
        return rate_status;
    }

    public void setRate_status(int rate_status) {
        this.rate_status = rate_status;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public ArrayList getRateList() {
        return rateList;
    }

    public void setRateList(ArrayList rateList) {
        this.rateList = rateList;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public FormFile getTheFile() {
        return theFile;
    }

    public void setTheFile(FormFile theFile) {
        this.theFile = theFile;
    }

    public String[] getRate_day() {
        return rate_day;
    }

    public void setRate_day(String[] rate_day) {
        this.rate_day = rate_day;
    }

    public String[] getRate_fromhour() {
        return rate_fromhour;
    }

    public void setRate_fromhour(String[] rate_fromhour) {
        this.rate_fromhour = rate_fromhour;
    }

    public String[] getRate_frommin() {
        return rate_frommin;
    }

    public void setRate_frommin(String[] rate_frommin) {
        this.rate_frommin = rate_frommin;
    }

    public String[] getRate_tohour() {
        return rate_tohour;
    }

    public void setRate_tohour(String[] rate_tohour) {
        this.rate_tohour = rate_tohour;
    }

    public String[] getRate_tomin() {
        return rate_tomin;
    }

    public void setRate_tomin(String[] rate_tomin) {
        this.rate_tomin = rate_tomin;
    }

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public int getSortItem() {
        return sortItem;
    }

    public void setSortItem(int sortItem) {
        this.sortItem = sortItem;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String[] getFailedPeriod() {
        return failedPeriod;
    }

    public void setFailedPeriod(String[] failedPeriod) {
        this.failedPeriod = failedPeriod;
    }

    public String[] getFirstPulse() {
        return firstPulse;
    }

    public void setFirstPulse(String[] firstPulse) {
        this.firstPulse = firstPulse;
    }

    public String[] getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String[] gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String[] getNextPulse() {
        return nextPulse;
    }

    public void setNextPulse(String[] nextPulse) {
        this.nextPulse = nextPulse;
    }

    public String[] getRatePerMin() {
        return ratePerMin;
    }

    public void setRatePerMin(String[] ratePerMin) {
        this.ratePerMin = ratePerMin;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getAction() == Constants.ADD || getAction() == Constants.EDIT) {
            if (getRate_destination_code() != null && getRate_destination_code().length() < 1) {
                errors.add("rate_destination_code", new ActionMessage("errors.rate_destination_code.required"));
            }
            if (getRate_destination_name() != null && getRate_destination_name().length() < 1) {
                errors.add("rate_destination_name", new ActionMessage("errors.rate_destination_name.required"));
            }

            if (getRate_fromhour() != null) {
                for (int i = 0; i < getRate_fromhour().length; i++) {
                    if ((Integer.parseInt(getRate_fromhour()[i]) * 60 + Integer.parseInt(getRate_frommin()[i]))>= (Integer.parseInt(getRate_tohour()[i]) * 60 + Integer.parseInt(getRate_tomin()[i]))) {
                        errors.add("rate_fromhour", new ActionMessage("errors.rate_fromhour.required", String.valueOf(i+1)));
                        break;
                    }
                }
            }

            String rates[] = request.getParameterValues("ratePerMin[]");
            int line_no = 1;
            for (String rate : rates) {
                if (!Utils.isDouble(rate) || Double.parseDouble(rate) < 0) {
                    errors.add("rate_per_min", new ActionMessage("errors.rate_per_min.required", String.valueOf(line_no)));
                }
                line_no++;
            }
            line_no = 1;
            String firstPulses[] = request.getParameterValues("firstPulse[]");
            for (String pulse : firstPulses) {
                if (!Utils.isInteger(pulse) || Integer.parseInt(pulse) <= 0) {
                    errors.add("rate_first_pulse", new ActionMessage("errors.rate_first_pulse.required", String.valueOf(line_no)));
                }
                line_no++;
            }
            line_no = 1;
            String nextPulses[] = request.getParameterValues("nextPulse[]");
            for (String pulse : nextPulses) {
                if (!Utils.isInteger(pulse) || Integer.parseInt(pulse) <= 0) {
                    errors.add("rate_next_pulse", new ActionMessage("errors.rate_next_pulse.required", String.valueOf(line_no)));
                }
                line_no++;
            }
            line_no = 1;
            String gracePeriods[] = request.getParameterValues("gracePeriod[]");
            for (String period : gracePeriods) {
                if (!Utils.isInteger(period) || Integer.parseInt(period) < 0) {
                    errors.add("rate_grace_period", new ActionMessage("errors.rate_grace_period.required", String.valueOf(line_no)));
                }
                line_no++;
            }
            line_no = 1;
            String failedPeriods[] = request.getParameterValues("failedPeriod[]");
            for (String period : failedPeriods) {
                if (!Utils.isInteger(period) || Integer.parseInt(period) < 0) {                    
                    errors.add("rate_failed_period", new ActionMessage("errors.rate_failed_period.required", String.valueOf(line_no)));
                }
                line_no++;
            }
        }
        return errors;
    }
}
