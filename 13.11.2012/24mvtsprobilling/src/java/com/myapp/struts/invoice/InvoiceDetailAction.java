package com.myapp.struts.invoice;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class InvoiceDetailAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";

        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        Logger logger = Logger.getLogger(InvoiceDetailAction.class.getName());
        logger.debug("id::" + id);
        if (login_dto != null && login_dto.getSuperUser()) {
            InvoiceTaskSchedular scheduler = new InvoiceTaskSchedular();
            InvoiceForm gatewayForm = (InvoiceForm) form;
            InvoiceDto dto = scheduler.getpersonalinfo(id);
            gatewayForm.setInvoiceDetail(scheduler.getInvoiceDetail(id));
            InvoiceDto dto1 = scheduler.gettotal();
            request.getSession(true).setAttribute(mapping.getAttribute(), gatewayForm);
            request.getSession(true).setAttribute("INVOICE_DTO", dto);
            request.getSession(true).setAttribute("total", dto1);
            request.getSession(true).setAttribute("id", id);
            logger.debug("invoice detail" + gatewayForm.getInvoiceDetail());
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
