/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.invoice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.util.AppConstants;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;
import com.myapp.struts.util.AppError;
import com.myapp.struts.session.Constants;
import com.myapp.struts.login.LoginDTO;

/**
 *
 * @author Administrator
 */
public class InvoiceDeleteAction extends org.apache.struts.action.Action {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";
    static Logger logger = Logger.getLogger(InvoiceDeleteAction.class.getName());

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            InvoiceTaskSchedular scheduler = new InvoiceTaskSchedular();
            InvoiceForm invoiceForm = (InvoiceForm) form;
            String ids = "";
            if (invoiceForm.getIds() == null || invoiceForm.getIds().length == 0) {
                target = AppConstants.FAILURE;
                request.getSession(true).setAttribute(AppConstants.USER_SUCCESS_MESSAGE, "Please select at least one user!");
                return (mapping.findForward(target));
            }
            for (long id : invoiceForm.getIds()) {

                ids += "," + id;
            }
            AppError error = scheduler.deleteInvoiveInfo(ids.substring(1));

            if (error.errorType == error.NO_ERROR) {
                target = AppConstants.SUCCESS;
                request.getSession(true).setAttribute(AppConstants.USER_SUCCESS_MESSAGE, "Selected Groups has been deleted successfully.");
            } else if (error.errorType == error.DB_ERROR) {
                target = AppConstants.FAILURE;
                invoiceForm.setMessage(true, error.getErrorMessage());
            } else if (error.errorType == error.OTHERS_ERROR) {
                invoiceForm.setMessage(true, error.getErrorMessage());
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return mapping.findForward(target);
    }
}
