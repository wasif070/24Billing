/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.util;

import java.sql.Statement;
import java.sql.ResultSet;
import org.apache.log4j.Logger;
import databaseconnector.DBConnection;

/**
 *
 * @author computer
 */
public class Sequencer {

    static Logger logger = Logger.getLogger(Sequencer.class.getName());
    static Sequencer sequencer = null;

    public Sequencer() {
    }

    public static Sequencer getInstance() {
        if (sequencer == null) {
            sequencer = new Sequencer();
        }
        return sequencer;
    }

    public synchronized long getNextId(String table_name) {
        DBConnection db = null;
        ResultSet rs = null;
        Statement stmt = null;
        long next_id = 0;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select max_id from sequencers where table_name='" + table_name + "' and " + stmt.executeUpdate("update sequencers set max_id=max_id+1 where table_name='" + table_name + "' ");
            System.out.println(sql);
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                next_id = rs.getLong("max_id");
            }
        } catch (Exception e) {
            logger.debug("Exception during getNextId()-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return next_id;
    }
}
