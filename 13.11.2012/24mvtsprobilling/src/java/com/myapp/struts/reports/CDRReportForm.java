/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 *
 * @author Ashraful
 */
public class CDRReportForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private String message;
    private long[] selectedIDs;
    private ArrayList<CDRReportDTO> mvtsCdrList;
    private long cdr_id;
    private long origin_client_id;
    private String origin_ip;
    private String origin_destination;
    private long term_client_id;
    private String term_ip;
    private String term_destination;
    private int fromDay;
    private int fromMonth;
    private int fromYear;
    private int fromHour;
    private int fromMin;
    private int fromSec;
    private int toDay;
    private int toMonth;
    private int toYear;
    private int toHour;
    private int toMin;
    private int toSec;
    private int callType;
    private String doSearch;
    private int[] summaryBy;
    private int summaryType;
    private ArrayList<CDRQualityDTO> mvtsQualityList;
    private String disconnect_cause;
    private int reportingType;
    private String origin_caller;
    private String terminated_no;
    private int errorType;
    private ArrayList<CDRReportDTO> errorList;

    public String getDisconnect_cause() {
        return disconnect_cause;
    }

    public void setDisconnect_cause(String disconnect_cause) {
        this.disconnect_cause = disconnect_cause;
    }

    public CDRReportForm() {
        doSearch = null;
        callType = 1;
        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        fromDay = day;
        fromMonth = month;
        fromYear = year;

        toDay = day;
        toMonth = month;
        toYear = year;
        toHour = 23;
        toMin = 59;
        toSec = 59;
        summaryType = 2;
        reportingType = 3;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getFromDay() {
        return fromDay;
    }

    public void setFromDay(int fromDay) {
        this.fromDay = fromDay;
    }

    public int getFromHour() {
        return fromHour;
    }

    public void setFromHour(int fromHour) {
        this.fromHour = fromHour;
    }

    public int getFromMin() {
        return fromMin;
    }

    public void setFromMin(int fromMin) {
        this.fromMin = fromMin;
    }

    public int getFromMonth() {
        return fromMonth;
    }

    public void setFromMonth(int fromMonth) {
        this.fromMonth = fromMonth;
    }

    public int getFromYear() {
        return fromYear;
    }

    public void setFromYear(int fromYear) {
        this.fromYear = fromYear;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrList() {
        return mvtsCdrList;
    }

    public void setMvtsCdrList(ArrayList<CDRReportDTO> mvtsCdrList) {
        this.mvtsCdrList = mvtsCdrList;
    }

    public ArrayList<CDRQualityDTO> getMvtsQualityList() {
        return mvtsQualityList;
    }

    public void setMvtsQualityList(ArrayList<CDRQualityDTO> mvtsQualityList) {
        this.mvtsQualityList = mvtsQualityList;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public int[] getSummaryBy() {
        return summaryBy;
    }

    public void setSummaryBy(int[] summaryBy) {
        this.summaryBy = summaryBy;
    }

    public int getSummaryType() {
        return summaryType;
    }

    public void setSummaryType(int summaryType) {
        this.summaryType = summaryType;
    }

    public int getToDay() {
        return toDay;
    }

    public void setToDay(int toDay) {
        this.toDay = toDay;
    }

    public int getToHour() {
        return toHour;
    }

    public void setToHour(int toHour) {
        this.toHour = toHour;
    }

    public int getToMin() {
        return toMin;
    }

    public void setToMin(int toMin) {
        this.toMin = toMin;
    }

    public int getToMonth() {
        return toMonth;
    }

    public void setToMonth(int toMonth) {
        this.toMonth = toMonth;
    }

    public int getToYear() {
        return toYear;
    }

    public void setToYear(int toYear) {
        this.toYear = toYear;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getOrigin_destination() {
        return origin_destination;
    }

    public void setOrigin_destination(String origin_destination) {
        this.origin_destination = origin_destination;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_destination() {
        return term_destination;
    }

    public void setTerm_destination(String term_destination) {
        this.term_destination = term_destination;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public int getReportingType() {
        return reportingType;
    }

    public void setReportingType(int reportingType) {
        this.reportingType = reportingType;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public String getTerminated_no() {
        return terminated_no;
    }

    public void setTerminated_no(String terminated_no) {
        this.terminated_no = terminated_no;
    }

    public int getFromSec() {
        return fromSec;
    }

    public void setFromSec(int fromSec) {
        this.fromSec = fromSec;
    }

    public int getToSec() {
        return toSec;
    }

    public void setToSec(int toSec) {
        this.toSec = toSec;
    }

    public ArrayList<CDRReportDTO> getErrorList() {
        return errorList;
    }

    public void setErrorList(ArrayList<CDRReportDTO> errorList) {
        this.errorList = errorList;
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }
}
