/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.AppConstants;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author Ashraful
 */
public class ReportTaskScheduler {

    public ReportTaskScheduler() {
    }

    public HashMap<String, Object> getCdrDTOsWithSearchParam(CDRReportDTO sdto, LoginDTO l_dto, int start, int end) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        if (l_dto.getSuperUser()) {
            if (sdto.getReportingType() == AppConstants.SUMMARY_REPORT) {
                return reportDAO.getSummary(sdto, start, end, l_dto);
            } else if (sdto.getReportingType() == AppConstants.SUMMARY_REPORT_BY_MOTHER) {
                return reportDAO.getSummaryByMotherClient(sdto, start, end, l_dto);
            }
            return reportDAO.getDTOList(sdto, start, end, l_dto);
        } else {
            return reportDAO.getDTOListByClient(sdto, l_dto.getId(), start, end);
        }
    }

    public ArrayList<CDRQualityDTO> getCdrQualityDTOs(CDRReportDTO sdto, LoginDTO l_dto) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        if (l_dto.getSuperUser()) {
            return reportDAO.getCdrQualityDTOs(sdto, l_dto, false);
        } else {
            return reportDAO.getCdrQualityDTOs(sdto, l_dto, true);
        }
    }

    public ArrayList<CDRReportDTO> getErrorReportDTOs(CDRReportDTO sdto, LoginDTO l_dto) {
        CDRReportDAO reportDAO = new CDRReportDAO();
        return reportDAO.getErrorReportDTOs(sdto, l_dto);
    }
}
