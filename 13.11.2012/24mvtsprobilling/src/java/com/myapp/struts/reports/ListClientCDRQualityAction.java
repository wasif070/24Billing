/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class ListClientCDRQualityAction extends Action {

    static Logger logger = Logger.getLogger(ListCDRQualityAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && !login_dto.getSuperUser()) {
            switch (login_dto.getClientType()) {
                case Constants.BOTH:
                    target = "success_both";
                    break;
                case Constants.ORIGINATION:
                    target = "success_origination";
                    break;
                case Constants.TERMINATION:
                    target = "success_termination";
                    break;
            }
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            int pageNo = 1;
            String links = "";

            ReportTaskScheduler scheduler = new ReportTaskScheduler();
            CDRReportForm mvtsCdrForm = (CDRReportForm) form;
            CDRReportDTO sdto = new CDRReportDTO();
            if (mvtsCdrForm.getDoSearch() != null) {

                if (mvtsCdrForm.getOrigin_ip() != null && mvtsCdrForm.getOrigin_ip().length() > 0) {
                    sdto.setOrigin_ip(mvtsCdrForm.getOrigin_ip());
                }

                if (mvtsCdrForm.getOrigin_destination() != null && mvtsCdrForm.getOrigin_destination().length() > 0) {
                    sdto.setOrigin_destination(mvtsCdrForm.getOrigin_destination());
                }

                if (mvtsCdrForm.getTerm_ip() != null && mvtsCdrForm.getTerm_ip().length() > 0) {
                    sdto.setTerm_ip(mvtsCdrForm.getTerm_ip());
                }

                if (mvtsCdrForm.getSummaryType() > -1) {
                    sdto.setSummaryType(mvtsCdrForm.getSummaryType());
                }

                String[] summary_values = request.getParameterValues("summaryBy[]");
                if (summary_values != null && summary_values.length > 0) {
                    int[] sumarry_int_vals = new int[summary_values.length];
                    for (int i = 0; i < summary_values.length; i++) {
                        sumarry_int_vals[i] = Integer.parseInt(summary_values[i]);
                    }
                    sdto.setSummaryBy(sumarry_int_vals);                    
                    String sby = "";
                    for (int j = 0; j < summary_values.length; j++) {
                        sby += summary_values[j] + ",";
                    }
                    links += "&sby=" + sby;
                }

                String fromDate = mvtsCdrForm.getFromYear() + "-" + formatter.format(mvtsCdrForm.getFromMonth()) + "-" + formatter.format(mvtsCdrForm.getFromDay()) + " " + formatter.format(mvtsCdrForm.getFromHour()) + ":" + formatter.format(mvtsCdrForm.getFromMin()) + ":" + formatter.format(mvtsCdrForm.getFromSec());
                sdto.setFromDate(fromDate);
                logger.debug("From Date-->" + sdto.getFromDate());
                links += "&fdate=" + fromDate;

                String toDate = mvtsCdrForm.getToYear() + "-" + formatter.format(mvtsCdrForm.getToMonth()) + "-" + formatter.format(mvtsCdrForm.getToDay()) + " " + formatter.format(mvtsCdrForm.getToHour()) + ":" + formatter.format(mvtsCdrForm.getToMin()) + ":" + formatter.format(mvtsCdrForm.getToSec());
                sdto.setToDate(toDate);
                logger.debug("To Date-->" + sdto.getToDate());
                links += "&tdate=" + toDate;
                request.getSession(true).setAttribute("SearchCdrDTO", sdto);
            }

            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            mvtsCdrForm.setMvtsQualityList(scheduler.getCdrQualityDTOs(sdto, login_dto));

            if (mvtsCdrForm.getMvtsQualityList() != null && mvtsCdrForm.getMvtsQualityList().size() > 0) {
                request.getSession(true).setAttribute("CDRReportDTO", mvtsCdrForm.getMvtsQualityList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("CDRReportDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}