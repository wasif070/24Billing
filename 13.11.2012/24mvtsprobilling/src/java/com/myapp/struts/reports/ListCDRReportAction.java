/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class ListCDRReportAction extends Action {

    static Logger logger = Logger.getLogger(ListCDRReportAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        NumberFormat formatter = new DecimalFormat("00");
        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute(Constants.MESSAGE, null);
            int pageNo = 1;
            String links = "";
            int perPageRecord = Constants.PER_PAGE_RECORD;
            if (request.getParameter("recordPerPage") != null) {
                perPageRecord = Integer.parseInt(request.getParameter("recordPerPage"));
            }
            if (request.getParameter("pageNo") != null) {
                pageNo = Integer.parseInt(request.getParameter("pageNo"));
            }

            int noOfPages = 1;
            try {
                noOfPages = Integer.parseInt(request.getSession(true).getAttribute("CDR_COUNT").toString()) / perPageRecord;
                if (Integer.parseInt(request.getSession(true).getAttribute("CDR_COUNT").toString()) % perPageRecord > 0) {
                    noOfPages++;
                }
            } catch (Exception ex) {
            }

            if (pageNo > noOfPages) {
                pageNo = noOfPages;
            }

            pageNo = pageNo <= 0 ? 1 : pageNo;
            int pageStart = (pageNo - 1) * perPageRecord;

            ReportTaskScheduler scheduler = new ReportTaskScheduler();
            CDRReportForm cdrReportForm = (CDRReportForm) form;
            if (cdrReportForm.getRecordPerPage() > 0) {
                request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, cdrReportForm.getRecordPerPage());
            }

            CDRReportDTO sdto = new CDRReportDTO();

            if (cdrReportForm.getOrigin_client_id() > 0) {
                sdto.setOrigin_client_id(cdrReportForm.getOrigin_client_id());
                links += "&oid=" + sdto.getOrigin_client_id();
            }
            sdto.setOrigin_ip(cdrReportForm.getOrigin_ip());
            sdto.setOrigin_destination(cdrReportForm.getOrigin_destination());

            if (cdrReportForm.getTerm_client_id() > 0) {
                sdto.setTerm_client_id(cdrReportForm.getTerm_client_id());
                links += "&tid=" + sdto.getTerm_rate_id();
            }
            sdto.setTerm_ip(cdrReportForm.getTerm_ip());
            sdto.setTerm_destination(cdrReportForm.getTerm_destination());

            if (cdrReportForm.getReportingType() > 0) {
                sdto.setReportingType(cdrReportForm.getReportingType());
                links += "&rtype=" + sdto.getReportingType();
            }

            sdto.setOrigin_caller(cdrReportForm.getOrigin_caller());
            sdto.setTerminated_no(cdrReportForm.getTerminated_no());

            String fromDate = cdrReportForm.getFromYear() + "-" + formatter.format(cdrReportForm.getFromMonth()) + "-" + formatter.format(cdrReportForm.getFromDay()) + " " + formatter.format(cdrReportForm.getFromHour()) + ":" + formatter.format(cdrReportForm.getFromMin()) + ":" + formatter.format(cdrReportForm.getFromSec());
            links += "&fdate=" + fromDate;
            sdto.setFromDate(fromDate);

            String toDate = cdrReportForm.getToYear() + "-" + formatter.format(cdrReportForm.getToMonth()) + "-" + formatter.format(cdrReportForm.getToDay()) + " " + formatter.format(cdrReportForm.getToHour()) + ":" + formatter.format(cdrReportForm.getToMin()) + ":" + formatter.format(cdrReportForm.getToSec());
            links += "&tdate=" + toDate;
            sdto.setToDate(toDate);
            if (cdrReportForm.getCallType() > 0) {
                sdto.setCallType(cdrReportForm.getCallType());
            }
            HashMap<String, Object> map = scheduler.getCdrDTOsWithSearchParam(sdto, login_dto, pageStart, perPageRecord);
            cdrReportForm.setMvtsCdrList((ArrayList<CDRReportDTO>) map.get("CDR_LIST"));
            request.getSession(true).setAttribute("CDR_COUNT", map.get("CDR_COUNT"));
            request.getSession(true).setAttribute("SearchCdrDTO", sdto);

            if (cdrReportForm.getMvtsCdrList() != null && cdrReportForm.getMvtsCdrList().size() > 0) {
                request.getSession(true).setAttribute("CDRReportDTO", cdrReportForm.getMvtsCdrList());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            } else {
                request.getSession(true).setAttribute("CDRReportDTO", null);
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?pageNo=" + pageNo + links, false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
