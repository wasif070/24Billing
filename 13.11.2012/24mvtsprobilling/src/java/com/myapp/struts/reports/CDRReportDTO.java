/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

/**
 *
 * @author Ashraful
 */
public class CDRReportDTO {

    private long cdr_id;
    private String cdr_date;
    private String dialed_no;
    private String origin_caller;
    private long origin_client_id;
    private String origin_client_name;
    private String origin_ip;
    private long origin_rate_id;
    private String origin_prefix;
    private String origin_destination;
    private double origin_rate;
    private String origin_rate_des;
    private double origin_bill_amount;
    private String terminated_no;
    private String term_caller;
    private long term_client_id;
    private String term_client_name;
    private String term_ip;
    private long term_rate_id;
    private String term_prefix;
    private String term_destination;
    private double term_rate;
    private String term_rate_des;
    private double term_bill_amount;
    private long duration;
    private String connection_time;
    private double pdd;
    private int callType;
    private String fromDate;
    private String toDate;
    private long total_duration;
    private double total_origin_bill_amount;
    private double total_term_bill_amount;
    private int[] summaryBy;
    private int summaryType;
    private String disconnect_cause;
    private String auth_error_cause;
    private int reportingType;
    private String startDate;
    private String endDate;
    private double acd;
    private long totalSuccess;
    private long totalFailure;
    private long totalCall;
    private double parentBillAmount;
    private double profit;
    private double totalParentBillAmount;
        private int errorType;
    private long errorCount;
    private String errorName;
    private long errorSum;
    private String hash_key;
    private int total_client;
    

    public double getProfit() {
        return profit;
    }

    public double getTotalParentBillAmount() {
        return totalParentBillAmount;
    }

    public void setTotalParentBillAmount(double totalParentBillAmount) {
        this.totalParentBillAmount = totalParentBillAmount;
    }

    public void setProfit(double profit) {
        this.profit = profit;
    }

    public double getParentBillAmount() {
        return parentBillAmount;
    }

    public void setParentBillAmount(double parentBillAmount) {
        this.parentBillAmount = parentBillAmount;
    }

    public long getTotalCall() {
        return totalCall;
    }

    public void setTotalCall(long totalCall) {
        this.totalCall = totalCall;
    }
    public long getTotalFailure() {
        return totalFailure;
    }
 
    public void setTotalFailure(long totalFailure) {
        this.totalFailure = totalFailure;
    }

    public long getTotalSuccess() {
        return totalSuccess;
    }

    public void setTotalSuccess(long totalSuccess) {
        this.totalSuccess = totalSuccess;
    }
    

    public double getAcd() {
        return acd;
    }

    public void setAcd(double acd) {
        this.acd = acd;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }
    
    public String getDisconnect_cause() {
        return disconnect_cause;
    }

    public void setDisconnect_cause(String disconnect_cause) {
        this.disconnect_cause = disconnect_cause;
    }

    public long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getCdr_date() {
        return cdr_date;
    }

    public void setCdr_date(String cdr_date) {
        this.cdr_date = cdr_date;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }

    public String getDialed_no() {
        return dialed_no;
    }

    public void setDialed_no(String dialed_no) {
        this.dialed_no = dialed_no;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getOrigin_client_name() {
        return origin_client_name;
    }

    public void setOrigin_client_name(String origin_client_name) {
        this.origin_client_name = origin_client_name;
    }

    public String getOrigin_destination() {
        return origin_destination;
    }

    public void setOrigin_destination(String origin_destination) {
        this.origin_destination = origin_destination;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getOrigin_prefix() {
        return origin_prefix;
    }

    public void setOrigin_prefix(String origin_prefix) {
        this.origin_prefix = origin_prefix;
    }

    public double getOrigin_rate() {
        return origin_rate;
    }

    public void setOrigin_rate(double origin_rate) {
        this.origin_rate = origin_rate;
    }

    public long getOrigin_rate_id() {
        return origin_rate_id;
    }

    public void setOrigin_rate_id(long origin_rate_id) {
        this.origin_rate_id = origin_rate_id;
    }

    public double getOrigin_bill_amount() {
        return origin_bill_amount;
    }

    public void setOrigin_bill_amount(double origin_bill_amount) {
        this.origin_bill_amount = origin_bill_amount;
    }

    public String getOrigin_rate_des() {
        return origin_rate_des;
    }

    public void setOrigin_rate_des(String origin_rate_des) {
        this.origin_rate_des = origin_rate_des;
    }

    public double getPdd() {
        return pdd;
    }

    public void setPdd(double pdd) {
        this.pdd = pdd;
    }

    public String getTerm_caller() {
        return term_caller;
    }

    public void setTerm_caller(String term_caller) {
        this.term_caller = term_caller;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_client_name() {
        return term_client_name;
    }

    public void setTerm_client_name(String term_client_name) {
        this.term_client_name = term_client_name;
    }

    public String getTerm_destination() {
        return term_destination;
    }

    public void setTerm_destination(String term_destination) {
        this.term_destination = term_destination;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getTerm_prefix() {
        return term_prefix;
    }

    public void setTerm_prefix(String term_prefix) {
        this.term_prefix = term_prefix;
    }

    public double getTerm_rate() {
        return term_rate;
    }

    public void setTerm_rate(double term_rate) {
        this.term_rate = term_rate;
    }

    public long getTerm_rate_id() {
        return term_rate_id;
    }

    public void setTerm_rate_id(long term_rate_id) {
        this.term_rate_id = term_rate_id;
    }

    public double getTerm_bill_amount() {
        return term_bill_amount;
    }

    public void setTerm_bill_amount(double term_bill_amount) {
        this.term_bill_amount = term_bill_amount;
    }

    public String getTerm_rate_des() {
        return term_rate_des;
    }

    public void setTerm_rate_des(String term_rate_des) {
        this.term_rate_des = term_rate_des;
    }

    public String getTerminated_no() {
        return terminated_no;
    }

    public void setTerminated_no(String terminated_no) {
        this.terminated_no = terminated_no;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public int[] getSummaryBy() {
        return summaryBy;
    }

    public void setSummaryBy(int[] summaryBy) {
        this.summaryBy = summaryBy;
    }

    public int getSummaryType() {
        return summaryType;
    }

    public void setSummaryType(int summaryType) {
        this.summaryType = summaryType;
    }

    public long getTotal_duration() {
        return total_duration;
    }

    public void setTotal_duration(long total_duration) {
        this.total_duration = total_duration;
    }

    public double getTotal_origin_bill_amount() {
        return total_origin_bill_amount;
    }

    public void setTotal_origin_bill_amount(double total_origin_bill_amount) {
        this.total_origin_bill_amount = total_origin_bill_amount;
    }

    public double getTotal_term_bill_amount() {
        return total_term_bill_amount;
    }

    public void setTotal_term_bill_amount(double total_term_bill_amount) {
        this.total_term_bill_amount = total_term_bill_amount;
    }

    public String getAuth_error_cause() {
        return auth_error_cause;
    }

    public void setAuth_error_cause(String auth_error_cause) {
        this.auth_error_cause = auth_error_cause;
    }

    public int getReportingType() {
        return reportingType;
    }

    public void setReportingType(int reportingType) {
        this.reportingType = reportingType;
    }

    public long getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(long errorCount) {
        this.errorCount = errorCount;
    }

    public String getErrorName() {
        return errorName;
    }

    public void setErrorName(String errorName) {
        this.errorName = errorName;
    }

    public long getErrorSum() {
        return errorSum;
    }

    public void setErrorSum(long errorSum) {
        this.errorSum = errorSum;
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int errorType) {
        this.errorType = errorType;
    }

    public String getHash_key() {
        return hash_key;
    }

    public void setHash_key(String hash_key) {
        this.hash_key = hash_key;
    }

    public int getTotal_client() {
        return total_client;
    }

    public void setTotal_client(int total_client) {
        this.total_client = total_client;
    }
}
