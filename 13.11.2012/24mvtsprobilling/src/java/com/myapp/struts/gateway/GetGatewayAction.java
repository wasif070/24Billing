package com.myapp.struts.gateway;

import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetGatewayAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {

            GatewayForm formBean = (GatewayForm) form;
            GatewayDTO dto = new GatewayDTO();
            GatewayTaskSchedular scheduler = new GatewayTaskSchedular();
            dto = scheduler.getGatewayDTO(id);

            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setGateway_ip(dto.getGateway_ip());
                formBean.setGateway_name(dto.getGateway_name());
                formBean.setPrev_gateway_name(dto.getGateway_name());
                formBean.setGateway_status(dto.getGateway_status());
                formBean.setGateway_type(dto.getGateway_type());
                formBean.setClientId(dto.getClientId());
                formBean.setGateway_dst_port_h323(dto.getGateway_dst_port_h323());
                formBean.setGateway_dst_port_sip(dto.getGateway_dst_port_sip());
                formBean.setSwitchGatewayId(dto.getSwitchGatewayId());
                formBean.setProtocol_id(dto.getProtocol_id());
                formBean.setEnable_radius(dto.getEnable_radius());
                formBean.setParent_id(ClientLoader.getInstance().getClientDTOByID(dto.getClientId()).getParent_id());
                formBean.setPrev_gateway_type(dto.getGateway_type());
                formBean.setSrc_dnis_prefix_allow(dto.getSrc_dnis_prefix_allow());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.getSession(true).setAttribute("type", dto.getGateway_type());
                request.getSession(true).setAttribute("GATEWAY_CLIENT_ID", dto.getClientId());
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
