package com.myapp.struts.gateway;

import java.util.ArrayList;

public class GatewayDTO {

    public boolean searchWithGatewayIP = false;
    public boolean searchWithStatus = false;
    public boolean searchWithType = false;
    private boolean superUser;
    private long id;
    private String gateway_ip;
    private String gateway_name;
    private int gateway_status;
    private int gateway_type;
    private String gateway_status_name;
    private String gateway_type_name;
    private int gateway_dst_port_h323;
    private int gateway_dst_port_sip;
    private int mvts_id;
    private String mvts_name;
    private int clientId;
    private String client_name;
    private double client_balance;
    private long[] selectedIDs;
    private long switchGatewayId;
    private int is_deleted;
    private int parent_id;
    private int owner_id;
    private String prev_gateway_name;
    private int protocol_id;
    private int enable_radius;
    private int prev_gateway_type;
    private String prefix;
    private int[] prefixes;
    private String op_name;
    ArrayList<Long> prev_prefixes;
    private String src_dnis_prefix_allow;

    public GatewayDTO() {
        superUser = false;
        gateway_status = -1;
        gateway_type = -1;
    }

    public boolean getSuperUser() {
        return superUser;
    }

    public void setSuperUser(boolean superUser) {
        this.superUser = superUser;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public boolean isSearchWithStatus() {
        return searchWithStatus;
    }

    public String getGateway_ip() {
        return gateway_ip;
    }

    public void setGateway_ip(String gateway_ip) {
        this.gateway_ip = gateway_ip;
    }

    public String getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String gateway_name) {
        this.gateway_name = gateway_name;
    }

    public int getGateway_status() {
        return gateway_status;
    }

    public void setGateway_status(int gateway_status) {
        this.gateway_status = gateway_status;
    }

    public String getGateway_status_name() {
        return gateway_status_name;
    }

    public void setGateway_status_name(String gateway_status_name) {
        this.gateway_status_name = gateway_status_name;
    }

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }

    public String getGateway_type_name() {
        return gateway_type_name;
    }

    public void setGateway_type_name(String gateway_type_name) {
        this.gateway_type_name = gateway_type_name;
    }

    public void setSearchWithStatus(boolean searchWithStatus) {
        this.searchWithStatus = searchWithStatus;
    }

    public boolean isSearchWithGatewayIP() {
        return searchWithGatewayIP;
    }

    public void setSearchWithGatewayIP(boolean searchWithGatewayIP) {
        this.searchWithGatewayIP = searchWithGatewayIP;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public int getMvts_id() {
        return mvts_id;
    }

    public void setMvts_id(int mvts_id) {
        this.mvts_id = mvts_id;
    }

    public String getMvts_name() {
        return mvts_name;
    }

    public void setMvts_name(String mvts_name) {
        this.mvts_name = mvts_name;
    }

    public double getClient_balance() {
        return client_balance;
    }

    public void setClient_balance(double client_balance) {
        this.client_balance = client_balance;
    }

    public int getGateway_dst_port_h323() {
        return gateway_dst_port_h323;
    }

    public void setGateway_dst_port_h323(int gateway_dst_port_h323) {
        this.gateway_dst_port_h323 = gateway_dst_port_h323;
    }

    public int getGateway_dst_port_sip() {
        return gateway_dst_port_sip;
    }

    public void setGateway_dst_port_sip(int gateway_dst_port_sip) {
        this.gateway_dst_port_sip = gateway_dst_port_sip;
    }

    public long getSwitchGatewayId() {
        return switchGatewayId;
    }

    public void setSwitchGatewayId(long switchGatewayId) {
        this.switchGatewayId = switchGatewayId;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getPrev_gateway_name() {
        return prev_gateway_name;
    }

    public void setPrev_gateway_name(String prev_gateway_name) {
        this.prev_gateway_name = prev_gateway_name;
    }

    public int getProtocol_id() {
        return protocol_id;
    }

    public void setProtocol_id(int protocol_id) {
        this.protocol_id = protocol_id;
    }

    public int getEnable_radius() {
        return enable_radius;
    }

    public void setEnable_radius(int enable_radius) {
        this.enable_radius = enable_radius;
    }

    public int getPrev_gateway_type() {
        return prev_gateway_type;
    }

    public void setPrev_gateway_type(int prev_gateway_type) {
        this.prev_gateway_type = prev_gateway_type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int[] getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(int[] prefixes) {
        this.prefixes = prefixes;
    }

    public String getOp_name() {
        return op_name;
    }

    public void setOp_name(String op_name) {
        this.op_name = op_name;
    }

    public ArrayList<Long> getPrev_prefixes() {
        return prev_prefixes;
    }

    public void setPrev_prefixes(ArrayList<Long> prev_prefixes) {
        this.prev_prefixes = prev_prefixes;
    }

    public String getSrc_dnis_prefix_allow() {
        return src_dnis_prefix_allow;
    }

    public void setSrc_dnis_prefix_allow(String src_dnis_prefix_allow) {
        this.src_dnis_prefix_allow = src_dnis_prefix_allow;
    }
}
