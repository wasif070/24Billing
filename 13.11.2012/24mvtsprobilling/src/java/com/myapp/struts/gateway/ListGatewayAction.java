package com.myapp.struts.gateway;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListGatewayAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            GatewayTaskSchedular scheduler = new GatewayTaskSchedular();
            GatewayForm gatewayForm = (GatewayForm) form;

            if (list_all == 0) {
                if (gatewayForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, gatewayForm.getRecordPerPage());
                }
                GatewayDTO udto = new GatewayDTO();
                if (gatewayForm.getGateway_ip() != null && gatewayForm.getGateway_ip().trim().length() > 0) {
                    udto.searchWithGatewayIP = true;
                    udto.setGateway_ip(gatewayForm.getGateway_ip().toLowerCase());
                }
                if (gatewayForm.getGateway_status() >= 0) {
                    udto.searchWithStatus = true;
                    udto.setGateway_status(gatewayForm.getGateway_status());
                }
                if (gatewayForm.getGateway_type() >= 0) {
                    udto.searchWithType = true;
                    udto.setGateway_type(gatewayForm.getGateway_type());
                }
                gatewayForm.setGatewayList(scheduler.getGatewayDTOsWithSearchParam(udto, login_dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    gatewayForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                gatewayForm.setGatewayList(scheduler.getGatewayDTOs(login_dto));
            }
            gatewayForm.setSelectedIDs(null);
            if (gatewayForm.getGatewayList() != null && gatewayForm.getGatewayList().size() <= (gatewayForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
