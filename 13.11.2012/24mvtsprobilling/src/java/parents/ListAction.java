package parents;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class ListAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            ParentTaskSchedular scheduler = new ParentTaskSchedular();
            ParentForm parentForm = (ParentForm) form;


            if (parentForm.getRecordPerPage() > 0) {
                request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, parentForm.getRecordPerPage());
            }
            ParentDTO pdto = new ParentDTO();
            if (parentForm.getParent_name() != null && parentForm.getParent_name().trim().length() > 0) {
                pdto.sw_name = true;
                pdto.setParent_name(parentForm.getParent_name().trim().toLowerCase());
            }

            parentForm.setParentList(scheduler.getParentDTOsWithSearchParam(pdto, login_dto));

            if (parentForm.getParentList() != null && parentForm.getParentList().size() <= (parentForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
