package parents;

import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;

public class ParentDAO {

    static Logger logger = Logger.getLogger(ParentDAO.class.getName());

    public ParentDAO() {
    }

    public MyAppError addParent(ParentDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select parent_name from parents where parent_name=? and is_deleted=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getParent_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Parent Name.");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "insert into parents(parent_name,parent_desc) values(?,?)";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, p_dto.getParent_name());
            ps.setString(2, p_dto.getParent_desc());

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                error.setErrorMessage(String.valueOf(rs.getLong(1)));
            }
            ParentLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding parent: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError editParent(ParentDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select parent_name from parents where parent_name=? and is_deleted=0 and id!=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getParent_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Parent name conflict with other parent name");
                resultSet.close();
                return error;
            }
            resultSet.close();

            sql = "update parents set parent_name=?,parent_desc=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getParent_name());
            ps.setString(2, p_dto.getParent_desc());

            ps.executeUpdate();
            ParentLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing parent: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteParent(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            sql = "update parents set is_deleted = 1 where id =" + cid;
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                ParentLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            logger.fatal("Error while Deleting Parent: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
