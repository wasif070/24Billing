<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="refresh" content="30">
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Current Call List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
               int pageNo = 1;
               int list_all = 0;
               int sortingOrder = 0;
               int sortedItem = 0;
               int recordPerPage = 10;
               if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
               recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
               }
               if (request.getParameter("d-49216-p") != null) {
               boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
               if(status==true){
               pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
               }
               if (request.getParameter("d-49216-s") != null) {
               sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
               }
               if (request.getParameter("d-49216-o") != null) {
               sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
               }
               if (request.getParameter("list_all") != null) {
               list_all = Integer.parseInt(request.getParameter("list_all"));
               }
            
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "currentcall");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right">    
                <div class="pad_10 border_left">
                    <%
                  java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                  navList.add("currentcall/listCurrentcalls.do?list_all=1;Current call");     

                  String own_id=request.getParameter("id")!=null? request.getParameter("id"):"-1";
                  String action="/currentcall/listCurrentcalls.do?id="+own_id;    

                    int client_level=login_dto.getClient_level();
                    int client_type = login_dto.getClientType();                                                                            
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>                    

                    <html:form action="<%=action%>" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                                                                                    
                                <% if(client_level==-1 || (client_level==1 && client_type==0)){%>
                                <tr>
                                    <th>Org. Client</th>
                                    <td><html:text property="origin_client_name" /></td>
                                    <th>Org. No.</th>
                                    <td><html:text property="current_dial_no_in" /></td>  
                                </tr> 
                                <%}%>
                                <% if(client_level==-1 || (client_level==1 && client_type==1)){%>
                                <tr>
                                    <th>Term. Client</th>
                                    <td><html:text property="term_client_name" /></td>
                                    <th>Term. No.</th>
                                    <td><html:text property="current_dial_no_out" /></td>
                                </tr>
                                <%}%>
                                <% if(client_level==2 && client_type==0){%>
                                <tr>
                                    <th>Org. No.</th>
                                    <td><html:text property="current_dial_no_in" /></td>  
                                    <th>Term. No.</th>
                                    <td><html:text property="current_dial_no_out" /></td>
                                </tr>
                                <%}%>
                                <tr>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="status">
                                            <html:option value="-1">All</html:option>   
                                            <html:option value="1">Connected</html:option>   
                                            <html:option value="0">Calling</html:option>   
                                        </html:select>
                                    </td>
                                    <th></th>
                                    <td>                                        
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="full-div" style="padding: 8px">
                        <table style="width: 40%; margin: 0 auto; border: 1px solid #ffffff;padding: 5px" cellpadding="0" cellspacing="0">
                            <tr>
                                <th style="text-align: right;">Calling&nbsp;:&nbsp;</th>
                                <th style="text-align: left;"><%=request.getSession(true).getAttribute("CALLING_CALLS").toString()%></th>
                                <th style="text-align: right;">Connected&nbsp;:&nbsp;</th>
                                <th style="text-align: left;"><%=request.getSession(true).getAttribute("CONNECTED_CALLS").toString()%></th>
                                <th style="text-align: right;">Total Calls&nbsp;:&nbsp;</th>
                                <th style="text-align: left;"><%=request.getSession(true).getAttribute("TOTAL_CURRENT_CALLS").toString()%></th>
                            </tr>                                                        
                        </table>
                    </div>
                    <div class="full-div height-5px"></div>
                    <c:set var="c_level" value="<%=client_level%>"></c:set>
                    <c:set var="c_type" value="<%=client_type%>"></c:set>
                    <html:form action="/currentcall/listCurrentcalls" method="post" >
                        <div class="over_flow_content display_tag_content" align="center">
                            <script type="text/javascript">count=0;</script>                                
                            <div class="over_flow_content_list">
                                <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.CurrentcallForm.currentcallList" pagesize="50">
                                    <display:setProperty name="paging.banner.item_name" value="Call" /> 
                                    <display:setProperty name="paging.banner.items_name" value="Calls" />
                                    <display:column class="custom_column2" title="Nr">
                                        <script type="text/javascript">
                                            document.write(++count+".");
                                        </script>
                                    </display:column>          
                                    <display:column  property="current_dial_no_in" class="left-align" title="Incoming Dest. Number" sortable="true" />                                     
                                    <c:choose>
                                        <c:when  test="${c_level==-1 || (c_level==1 && c_type==0)}">  
                                            <display:column  property="origin_client_name" class="left-align" title="Org. Client" sortable="true" />   
                                            <display:column  property="current_org_gateway_ip" class="left-align" title="Origination Gateway" sortable="true" />
                                        </c:when>
                                    </c:choose>                                    
                                    <display:column  property="current_dial_no_out" class="left-align" title="Outgoing Dest. Number" sortable="true"/>                                      
                                    <c:choose>
                                        <c:when  test="${c_level==-1 || (c_level==1 && c_type==1)}">
                                            <display:column  property="term_client_name" class="left-align" title="Term. Client" sortable="true"/>
                                            <display:column  property="current_term_gateway_ip" class="left-align" title="Termination Gateway" sortable="true"/> 
                                        </c:when>
                                    </c:choose>                                    
                                    <display:column  property="current_setup_time" class="center-align" title="Set Up Time" sortable="true"/>
                                    <display:column  property="current_connect_time" class="center-align" title="Connect Time" sortable="true"/> 
                                    <display:column  property="active_duration" class="right-align" title="Duration (HH:SS)" sortable="true"/>   
                                    <display:column title="Status" sortable="true" headerClass="center-align" class="left-align">
                                        <c:choose>
                                            <c:when test="${data.status==0}"><span class="blue" >Calling</span></c:when>
                                            <c:when test="${data.status==1}"><span class="blue" >Connected</span></c:when>
                                        </c:choose>
                                    </display:column>
                                </display:table>
                            </div>    
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>                
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>





