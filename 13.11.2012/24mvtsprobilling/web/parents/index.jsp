<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }else{
        msg="<div class='success'>"+msg+"</div>";
       }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Parent List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "client");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <div class="right_content_view fl_right">             
                <div class="pad_10 border_left">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("parents/listParent.do?list_all=1;Parent");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/parents/listParent.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Parent Name</th>
                                    <td><html:text property="parent_name" /></td>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="status">
                                            <html:option value="-1">Select</html:option>
                                            <html:option value="0">Inactive</html:option>
                                            <html:option value="1">Active</html:option>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>                                
                            </table>
                        </div>
                        <div class="over_flow_content display_tag_content" align="center">
                            <%=msg%>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.ParentForm.parentList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Parent" />
                                <display:setProperty name="paging.banner.items_name" value="Parents" />
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column property="parent_name" class="left-align" title="Name" sortable="true" style="width:30%" />
                                <display:column property="parent_desc" class="left-align" title="Desc" sortable="true" style="width:30%" />
                                <display:column property="statusStr" class="custom_column1" title="Status" sortable="true" style="width:10%" />
                                <c:choose>
                                    <c:when test="${editPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:10%;" >
                                            <a href="../parents/getParent.do?id=${data.id}" class="edit" title="Change"></a>
                                            <c:choose>
                                                <c:when test="${data.id!=-1}"><a href="../parents/deleteParent.do?id=${data.id}" onclick="javascript:return confirm('Are you sure to want delete the user?');" class="drop" title="Delete"></a></c:when>
                                            </c:choose>                                    
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <%@include file="../includes/footer.jsp"%>
        </div>
    </body>
</html>
<%
                        request.removeAttribute(Constants.USER_ID_LIST);
                        request.getSession(true).removeAttribute(Constants.MESSAGE);
%>