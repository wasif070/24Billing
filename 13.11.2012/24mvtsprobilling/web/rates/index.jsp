<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@page import="com.myapp.struts.clients.ClientLoader"%>
        <%@page import="com.myapp.struts.clients.ClientDTO"%>
        <%@include file="../login/login-check.jsp"%>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
        <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

        <%
            int recordPerPage = 10;
            String currentPage = request.getParameter("page") == null ? "1" : request.getParameter("page");
            if (request.getParameter("d-49216-p") != null) {
                 boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                  if(status==true){
                    currentPage = request.getParameter("d-49216-p");
                     }
              }
            if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
            }

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } else {
                msg = "<div class='success'>" + msg + "</div>";
            }
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Rate List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                long rateplan_id = 0;
                if (login_dto.getOwn_id() > 0) {
                    ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
                    if (client_dto != null) {
                        rateplan_id = client_dto.getRateplan_id();
                    }
                }
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "rate");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <c:set var="rateplan_id" value="<%=String.valueOf(rateplan_id)%>"></c:set>
            <div class="right_content_view fl_right">
                <div class="pad_10 border_left">
                    <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add("rates/listRate.do?list_all=1&rate_id=1;Rate");
                    navList.add(";Rate List");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>

                    <html:form action="rates/listRate.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Prefix</th>
                                    <td><html:text property="rate_destination_code" /></td>
                                    <th>Record Per Page</th>
                                    <td>
                                        <input type="text" name="recordPerPage" value="<%=recordPerPage%>" />
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="page" value="<%=currentPage%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="6" align="center">
                                        <html:submit styleClass="search-button" property="doSearch" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>         
                    <div class="full-div height-5px"></div>
                    <div class="pagination center-align"><span style="text-align: center"><%=request.getSession(true).getAttribute("PAGER").toString()%></span></div>
                    <ul class="nav_menu half-div" style="margin: 0 auto; display: block">
                        <% 
                        long rate_id = Long.parseLong(request.getSession(true).getAttribute("sess_rate_id").toString()); 
                        if(rateplan_id!=rate_id){%>
                        <li style="width: 95px; text-align: center; float: left"><a href="../rates/new_rate.jsp">Add Rate</a></li>
                        <li style="width: 95px; text-align: center; float: left"><a href="../rates/upload_rate.jsp">Upload Rate</a></li>
                        <%}%>
                        <li style="width: 115px; text-align: center; float: left"><a href="../rates/download.do">Download Rate</a></li>
                    </ul>
                    <div class="clear"></div>
                    <html:form action="/rates/multipleRate" method="post" onsubmit="return confirm('Are you sure to take action?');" >
                        <div class="over_flow_content display_tag_content" style="text-align: center">                            
                            <bean:write name="RateForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <%=msg%>
                            <script type="text/javascript">count=<%=(Integer.valueOf(currentPage) - 1) * recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="requestScope.RateForm.rateList"  requestURI="/rates/listRate.do" >
                                <display:setProperty name="paging.banner.item_name" value="Rate" /> 
                                <display:setProperty name="paging.banner.items_name" value="Rates" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.rate_id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:5%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Prefix" sortProperty="rate_destination_code" sortable="true"  style="width:10%" >
                                    ${data.rate_destination_code}
                                </display:column>
                                <display:column property="rate_destination_name" title="Destination Name" sortable="true" style="width:17%" />
                                <display:column property="rate_per_min" class="right-align" title="Rate (Per Min)" sortable="true" style="width:10%" />
                                <display:column property="rate_first_pulse" format="{0,number,##,00}" class="center-align" title="First Pulse" sortable="true" style="width:10%" />
                                <display:column property="rate_next_pulse" format="{0,number,##,00}" class="center-align" title="Next Pulse" sortable="true" style="width:10%" />
                                <display:column property="rate_grace_period" format="{0,number,##,00}" class="center-align" title="Grace Period" sortable="true" style="width:10%" />
                                <display:column property="rate_failed_period" format="{0,number,##,00}" class="center-align" title="Failed Period" sortable="true" style="width:10%" />
                                <display:column property="formated_time_frame" class="center-align" title="Time Frame" sortable="true" style="width:12%"/>
                                <c:choose>
                                    <c:when test="${editPermission==1 && data.rateplan_id!=rateplan_id}">
                                        <display:column class="center-align" title="Task" style="width:5%;" >
                                            <a href="../rates/getRate.do?id=${data.rate_id}" class="edit"  title="Change"></a>
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                <c:choose>
                                <c:when test="${editPermission==1 && data.rateplan_id!=rateplan_id}">
                                    <%if(perms[com.myapp.struts.util.AppConstants.DELETE]==1){%>
                                    <html:submit property="deleteBtn" styleClass="custom-button" value="Delete Selected" />
                                    <%}%>
                                </c:when>
                               </c:choose> 
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
