<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%@page import="com.myapp.struts.util.AppConstants"%>

<%

    String roleName = request.getParameter("roleName");
    if (roleName == null) {
        roleName = "";
    }
    String roleDesc = request.getParameter("roleDesc");
    if (roleDesc == null) {
        roleDesc = "";
    }

    String recordPerPage = (String) request.getParameter("recordPerPage");
    if (recordPerPage == null) {
        recordPerPage = AppConstants.RECORD_PER_PAGE;
    }
    String currentPage = (String) request.getParameter("d-49216-p");
    if (currentPage == null) {
        currentPage = "1";
    }

    String msg = (String) request.getSession(true).getAttribute(AppConstants.ROLE_SUCCESS_MESSAGE);
    if (msg == null) {
        msg = "";
    } else {
        msg = "<div class='success'>" + msg + "</div>";
    }
%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Role List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>        
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
            int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "role");
            if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <div class="right_content_view fl_right">             
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("role/listRoles.do;Roles");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/role/listRoles.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="1">
                                <tr class="odd">
                                    <th>Role Name</th>
                                    <td>
                                        <html:text property="roleName" styleId="roleName" value="<%=roleName%>" />
                                    </td>
                                    <th>Role Desc.</th>
                                    <td>
                                        <html:text property="roleDesc" styleId="roleDesc" value="<%=roleDesc%>" />
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <th>Record Per Page</th>
                                    <td>
                                        <input type="text" name="recordPerPage" value="<%=recordPerPage%>" />
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=currentPage%>" />
                                    </td>
                                </tr>
                                <tr class="odd">
                                    <td colspan="4" align="center">                                             
                                        <html:submit styleClass="search-button" property="doSearch" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </html:form>
                    </div>
                    <div class="over_flow_content display_tag_content" align="center">                        
                        <html:form styleId="delForm" action="role/deleteRole" method="post">
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.RoleForm.roleDTOs" requestURI="/role/listRoles.do" pagesize="<%=Integer.parseInt(recordPerPage)%>" >
                                <display:setProperty name="paging.banner.item_name" value="Role" />
                                <display:setProperty name="paging.banner.items_name" value="Roles" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='One Role' />" style="width:5%">
                                    <input type="checkbox" name="ids" value="${data.id}" class="select_id"/>
                                </display:column>
                                <display:column title="Role Name" sortable="true" headerClass="center-align" class="left-align">${data.roleName}</display:column>
                                <display:column title="Role Desc" sortable="true" headerClass="center-align" class="left-align">${data.roleDesc}</display:column>
                                <display:column title="Status" sortable="true" headerClass="center-align" class="center-align">
                                    <c:choose>
                                        <c:when test="${data.status==1}"><span class="blue" >Active</span></c:when>
                                        <c:otherwise>Inactive</c:otherwise>
                                    </c:choose>
                                </display:column>
                                <c:choose>
                                    <c:when test="${editPermission==1}">
                                        <display:column title="Edit" headerClass="center-align" class="center-align">
                                            <a href="../role/getRole.do?cid=${data.editId}">Edit</a>
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>
                            <div class="height-20px">
                                <bean:write name="RoleForm" property="message" filter="false"/>
                            </div>
                            <% if (perms[com.myapp.struts.util.AppConstants.DELETE] == 1) {%>
                            <input type="submit" id="delBtn" value="Delete Selected" style="width: 140px;" />
                            <%}%>
                            <div class="blank-height"></div>
                        </html:form>
                    </div>
                </div>                
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <%@include file="../includes/footer.jsp"%>
        </div>        
    </body>
</html>
