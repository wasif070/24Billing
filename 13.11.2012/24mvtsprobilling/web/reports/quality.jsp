<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRQualityDTO,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

        <%
            boolean disOrigin = true;
            boolean disTerm = true;
            String origin_client_id = (String) request.getParameter("oid");
            if (origin_client_id == null) {
                origin_client_id = "";
                disOrigin = true;
            } else {
                disOrigin = false;
            }

            String term_client_id = (String) request.getParameter("tid");
            if (term_client_id == null || term_client_id.length() < 1) {
                term_client_id = "";
                disTerm = true;
            } else {
                disTerm = false;
            }

            String summaryBy = request.getParameter("sby") != null ? request.getParameter("sby").toString() : "";
            if (summaryBy != null && summaryBy.length() > 0) {
                summaryBy = summaryBy.substring(0, summaryBy.length() - 1);
            }

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } else {
                msg = "<div class='error' style='width:700px;'>" + msg + "</div>";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRQualityDTO> mvtscdrList = (ArrayList<CDRQualityDTO>) request.getSession(true).getAttribute("CDRReportDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());
            ArrayList<ClientDTO> originClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.ORIGINATION, login_dto.getOwn_id());
            ArrayList<ClientDTO> termClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.TERMINATION, login_dto.getOwn_id());
            long total_time = 0;
            int total_success = 0;
            int total_fail = 0;
            int total_call = 0;
            double total_origin_bill = 0;
            double total_term_bill = 0;
            String bg_class = "odd";
            NumberFormat pddformatter = new DecimalFormat("#00.00");
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: CDR Quality Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right" style="width:80%">
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";Quality Report");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/qualityCdr.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width:100%;" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th class="right-align bold" style="color: #2663E0;">Origination</th>
                                    <th>Client ID</th>
                                    <td>
                                        <html:select property="origin_client_id" styleClass="joriginId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (originClientList != null && originClientList.size() > 0) {
                                                    int size = originClientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) originClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Origination IP</th>
                                    <td><html:select property="origin_ip" disabled="<%=disOrigin%>" styleClass="joriginIp">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (!origin_client_id.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(origin_client_id));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Destination</th>
                                    <td><html:text property="origin_destination" styleClass="jorigindest" disabled="<%=disOrigin%>" /></td>

                                </tr>
                                <% if (login_dto.isUser() || login_dto.getSuperUser() && login_dto.getClient_level() < 0) {%>
                                <tr>
                                    <th class="right-align bold" style="color: #2663E0;">Termination</th>
                                    <th>Client ID</th>
                                    <td><html:select property="term_client_id" value="<%=term_client_id%>" styleClass="jtermId">
                                            <html:option value="">Select Client</html:option>
                                            <%
                                                if (termClientList != null && termClientList.size() > 0) {
                                                    int size = termClientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) termClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Termination  IP</th>
                                    <td><html:select property="term_ip" disabled="<%=disTerm%>" styleClass="jtermIp">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (!term_client_id.isEmpty()) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(Long.parseLong(term_client_id));
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.TERMINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Destination</th>
                                    <td><html:text property="term_destination" styleClass="jtermdest" disabled="<%=disTerm%>" /></td>
                                </tr>
                                <%}%>                                
                                <tr>
                                    <th>Start Time</th>
                                    <td colspan="3" class="selopt">
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="fromYear" styleClass="disable" styleId="fromYear">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromMonth" styleClass="month disable" styleId="fromMonth">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromDay" styleClass="disable">
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="fromHour" styleClass="disable" styleId="fromHour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));

                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromMin" styleClass="disable" styleId="fromMin">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="fromSec" styleClass="disable" styleId="fromSec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <th>End Time</th>
                                    <td colspan="3" class="selopt">   
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="toYear" styleClass="disable" styleId="toYear">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toMonth" styleClass="month disable" styleId="toMonth">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toDay" styleClass="disable">
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="toHour" styleClass="disable" styleId="toHour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="toMin" styleClass="disable" styleId="toMin">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>  
                                                </td><td>
                                                    <html:select property="toSec" styleClass="disable" styleId="toSec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Caller</th>
                                    <td colspan="3" class="selopt">
                                        <html:text property="origin_caller" />
                                    </td>
                                    <th>Terminated No</th>
                                    <td colspan="3" class="selopt">      
                                        <html:text property="terminated_no"/>
                                    </td>
                                </tr>

                                <tr>
                                    <th>Summary By</th>
                                    <td colspan="3">
                                        <%
                                            for (int i = 0; i < Constants.SUMMARY_VALUE.length; i++) {
                                        %>
                                        <input type="checkbox" name="summaryBy[]" <% if (summaryBy.contains(Constants.SUMMARY_VALUE[i])) {%> checked="checked" <% }%> value="<%=Constants.SUMMARY_VALUE[i]%>" /><%=Constants.SUMMARY_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                    <th>Unit Time </th>
                                    <td colspan="2">
                                        <%
                                            for (int i = 0; i < Constants.UNIT_TIME_VALUE.length; i++) {
                                        %>
                                        <html:radio property="summaryType" value="<%=Constants.UNIT_TIME_VALUE[i]%>" /><%=Constants.UNIT_TIME_STRING[i]%>
                                        <%
                                            }
                                        %>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="7" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="over_flow_content">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="3%">Nr.</th>
                                    <th rowspan="2" width="7%">Time</th>
                                    <%if (summaryBy.contains("0") && !summaryBy.contains("0,1")) {%>
                                    <th colspan="3" width="22%">Origination</th>
                                    <%} else if (summaryBy.contains("1") && !summaryBy.contains("0,1")) {%>
                                    <th colspan="2" width="22%">Origination</th>
                                    <%} else if (summaryBy.contains("0,1")) {%>
                                    <th colspan="4" width="22%">Origination</th>
                                    <%} else {%>
                                    <th colspan="1" width="10%">Origination</th>
                                    <%}%>
                                    <%if (summaryBy.contains("2") && !summaryBy.contains("2,3")) {%>
                                    <th colspan="3" width="22%">Termination</th>
                                    <%} else if (summaryBy.contains("3") && !summaryBy.contains("2,3")) {%>
                                    <th colspan="2" width="22%">Termination</th>
                                    <%} else if (summaryBy.contains("2,3")) {%>
                                    <th colspan="4" width="22%">Termination</th>
                                    <%} else {%>
                                    <th colspan="1" width="10%">Termination</th>
                                    <%}%>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Success+Fail=Total</th>
                                    <th rowspan="2" width="5%">ASR(%)</th>
                                    <th rowspan="2" width="5%">ACD (Min:Sec)</th>
                                    <th rowspan="2" width="5%">Avg PDD(Sec)</th>
                                </tr>
                                <tr class="header">
                                    <%if (summaryBy.contains("0")) {%>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <%}%>
                                    <%if (summaryBy.contains("1")) {%>
                                    <th>Destination</th>
                                    <%}%>
                                    <th>Bill</th>

                                    <%if (summaryBy.contains("2")) {%>
                                    <th>Client</th>
                                    <th>IP</th>
                                    <%}%>
                                    <%if (summaryBy.contains("3")) {%>
                                    <th>Destination</th>
                                    <%}%>
                                    <th>Bill</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size(); inc++) {
                                            double avg_pdd = 0;
                                            long acd = 0;
                                            CDRQualityDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                            total_time += obj.getDuration();
                                            total_success += obj.getTotal_success();
                                            total_fail += obj.getTotal_fail();
                                            total_call = obj.getTotal_success() + obj.getTotal_fail();
                                            double success_rate = 0.0;
                                            if (obj.getTotal_success() > 0) {
                                                acd = (long) (obj.getDuration() / obj.getTotal_success());
                                                avg_pdd = (double) (obj.getAvg_pdd());
                                            }
                                            if (total_call > 0) {
                                                success_rate = ((double) obj.getTotal_success() / total_call) * 100;
                                            }
                                            total_origin_bill += obj.getOrigin_bill_amount();
                                            total_term_bill += obj.getTerm_bill_amount();
                                %>


                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=inc + 1%>.</td>
                                    <td style="text-align: center"><%=obj.getStart_time()%></td>
                                    <%if (summaryBy.contains("0")) {%>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td><%=obj.getOrigin_ip()%></td>
                                    <%}%>
                                    <%if (summaryBy.contains("1")) {%>
                                    <td><%=obj.getOrigin_destination()%></td>
                                    <%}%>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <%if (summaryBy.contains("2")) {%>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <td><%=obj.getTerm_ip()%></td>
                                    <%}%>
                                    <%if (summaryBy.contains("3")) {%>
                                    <td><%=obj.getTerm_destination()%></td>
                                    <%}%>
                                    <td align="right"><%=amount_formatter.format(obj.getTerm_bill_amount())%></td>
                                    <td style="text-align: right"><%=Utils.getMMSS(obj.getDuration())%></td>
                                    <td style="text-align: right"><%=obj.getTotal_success()%>+<%=obj.getTotal_fail()%>=<%=(obj.getTotal_success() + obj.getTotal_fail())%></td>
                                    <td style="text-align: right"><%=pddformatter.format(success_rate)%></td>
                                    <td style="text-align: right"><%=Utils.getMMSS((long) acd)%></td>
                                    <td style="text-align: right"><%=pddformatter.format(avg_pdd)%></td>
                                </tr>
                                <%}
                                    }%>
                            </tbody>
                        </table>
                        <div class="full-div" style="padding: 8px">
                            <div style="height: 15px"></div>
                            <table style="width: 70%; margin: 0 auto; border: 1px #ffffff solid" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid; padding: 5px; width: 25%">Total Origination Bill</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=amount_formatter.format(total_origin_bill)%></td>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px; width: 25%">Total Termination Bill</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=amount_formatter.format(total_term_bill)%></td>
                                </tr>
                                <tr>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid; padding: 5px; width: 25%">Total Duration(MM:SS)</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=Utils.getMMSS(total_time)%></td>
                                    <th style="text-align: left;border-bottom: 1px #ffffff solid;border-left: 1px #ffffff solid; padding: 5px; width: 25%">Success+Fail=Total Calls</th>
                                    <td style="text-align: right; border-bottom: 1px #ffffff solid; border-left: 1px #ffffff solid; padding: 5px"><%=total_success%>+<%=total_fail%>=<%=(total_success + total_fail)%></td>
                                </tr>
                                <tr>
                                    <th style="text-align: left; padding: 5px">ASR</th>
                                    <td style="text-align: right;border-left: 1px #ffffff solid; padding: 5px"><%=pddformatter.format(((double) total_success / (double) ((total_success + total_fail) <= 0 ? 1 : (total_success + total_fail))) * 100)%>%</td>
                                    <th style="text-align: left;border-left: 1px #ffffff solid; padding: 5px">&nbsp;</th>
                                    <td colspan="3" style="text-align: right;border-left: 1px #ffffff solid; padding: 5px">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>