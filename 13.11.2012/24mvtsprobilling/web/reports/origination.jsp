<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.reports.CDRReportDTO,com.myapp.struts.reports.CDRReportDAO,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

        <%
            CDRReportDAO mvtsDao = new CDRReportDAO();
            String links = "";

            String recordPerPage = (String) request.getParameter("recordPerPage");
            if (recordPerPage == null) {
                recordPerPage = "" + Constants.PER_PAGE_RECORD;
            }

            String pNo = (String) request.getParameter("pageNo");
            if (pNo == null) {
                pNo = "1";
            }


            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<Integer> hours = Utils.getTimeValue(24);
            ArrayList<Integer> minsec = Utils.getTimeValue(60);
            ArrayList<CDRReportDTO> mvtscdrList = (ArrayList<CDRReportDTO>) request.getSession(true).getAttribute("CDRReportDTO");
            CDRReportDTO searchDTO = (CDRReportDTO) request.getSession(true).getAttribute("SearchCdrDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: CDR List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right" style="width:80%">           
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";");
                        navList.add(";CDR Report");
                    %>
                    <%= navigation.Navigation_client.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/reports/listClientCdr.do" method="post" >
                        <div class="">
                            <table class="search-table" style="width: 100%" align="center" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Origination IP</th>
                                    <td><html:select property="origin_ip">
                                            <html:option value="">All</html:option>
                                            <%
                                                if (login_dto.getId() > 0) {
                                                    ArrayList<GatewayDTO> clientGateway = GatewayLoader.getInstance().getClientsGatewayList(login_dto.getId());
                                                    if (clientGateway != null && clientGateway.size() > 0) {
                                                        int size = clientGateway.size();
                                                        for (int i = 0; i < size; i++) {
                                                            GatewayDTO c_dto = (GatewayDTO) clientGateway.get(i);
                                                            if (c_dto.getGateway_type() == Constants.ORIGINATION || c_dto.getGateway_type() == Constants.BOTH) {%>
                                                            <html:option value="<%=c_dto.getGateway_ip()%>"><%=c_dto.getGateway_ip()%></html:option>;
                                            <% }
                                                        }
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Destination</th>
                                    <td><html:text property="origin_destination" /></td>
                                    <th>Call Type</th>
                                    <td>
                                        <html:radio property="callType" value="1" /> Successful <html:radio property="callType" value="2" /> Failed
                                    </td>
                                </tr>                             
                                <tr>
                                    <th>From Date</th>
                                    <td class="selopt" colspan="2">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromSec" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                    <th>To Date</th>
                                    <td class="selopt" colspan="2">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>

                                        <html:select property="toHour" styleClass="">
                                            <%
                                                for (int i = 0; i < hours.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMin" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toSec" styleClass="">
                                            <%
                                                for (int i = 0; i < minsec.size(); i++) {
                                                    String increment = String.valueOf(i);
                                                    String temp = formatter.format((i));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>      
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:select property="recordPerPage">
                                            <html:option value="10">10</html:option>
                                            <html:option value="25">25</html:option>
                                            <html:option value="50">50</html:option>
                                            <html:option value="100">100</html:option>
                                            <html:option value="250">250</html:option>
                                            <html:option value="500">500</html:option>
                                        </html:select>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" style="width:50px" name="pageNo" value="<%=pNo%>" />
                                    </td> 
                                    <td align="left">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <div class="clear height-20px">
                        <%=msg%>
                    </div>                    
                    <table class="content_table" cellspacing="0" cellpadding="0">
                        <thead>
                            <%
                                int dataListSize = 0;
                                int pageNo = 1;
                                int prevPageNo = 1;
                                int nextPageNo = 1;
                                int totalPages = 1;
                                long total_time = 0;
                                double total_origin_bill = 0;
                                dataListSize = Integer.parseInt(request.getSession(true).getAttribute("CDR_COUNT").toString());
                                int perPageRecord = Integer.parseInt(recordPerPage);
                                if (perPageRecord == 0) {
                                    perPageRecord = Constants.PER_PAGE_RECORD;
                                }

                                if (dataListSize > 0) {
                                    totalPages = dataListSize / perPageRecord;
                                    if (dataListSize % perPageRecord != 0) {
                                        totalPages++;
                                    }
                                }

                                if (request.getParameter("pageNo") != null) {
                                    pageNo = Integer.parseInt(request.getParameter("pageNo"));
                                }
                                int pageStart = (pageNo - 1) * perPageRecord;
                                if (pageNo > 1) {
                                    prevPageNo = pageNo - 1;
                                }
                                nextPageNo = pageNo;
                                if ((pageNo + 1) <= totalPages) {
                                    nextPageNo = pageNo + 1;
                                }

                                String firstLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=1&recordPerPage=" + perPageRecord + links;
                                String prevLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + prevPageNo + "&recordPerPage=" + perPageRecord + links;
                                String nextLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + nextPageNo + "&recordPerPage=" + perPageRecord + links;
                                String lastLink = request.getContextPath() + "/reports/listClientCdr.do?pageNo=" + totalPages + "&recordPerPage=" + perPageRecord + links;
                            %>
                            <tr><td colspan="30" align="center"><input type="hidden" name="pageNo" value="<%=pageNo%>"/>Total <%=dataListSize%> records found. Displaying Page&nbsp;<%=pageNo%>&nbsp;of&nbsp;<%=totalPages%></td></tr>
                            <tr><td colspan="18"  align="center"><a href="<%=firstLink%>">&laquo;First</a> | <a href="<%=prevLink%>">&laquo; Prev</a> | <a href="<%=nextLink%>">Next &raquo;</a> | <a href="<%=lastLink%>">Last &raquo;</a></td></tr>
                        </thead>
                    </table>
                    <div class="over_flow_content" style="font-size:8pt;">
                        <table class="content_table" cellspacing="1" cellpadding="2">
                            <thead>
                                <tr class="header">
                                    <th rowspan="2" width="5%">Nr.</th>
                                    <th colspan="7" width="30%">Origination</th>
                                    <th rowspan="2" width="10%">Duration (Min:Sec)</th>
                                    <th rowspan="2" width="10%">Connection Time</th>
                                    <th rowspan="2" width="5%">PDD (sec)</th>
                                    <th rowspan="2" width="5%">Disconnect Cause</th>
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <th rowspan="2" width="10%">Auth Error Cause</th>
                                    <%}%>
                                </tr>
                                <tr class="header">
                                    <th>Caller</th>
                                    <th>Org. No</th>
                                    <th>IP</th>
                                    <th>Prefix</th>
                                    <th>Destination</th>
                                    <th>Rate Details</th>
                                    <th>Bill</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    com.myapp.struts.settings.SettingsDTO settingsDTO = com.myapp.struts.settings.SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
                                    String timeZone = settingsDTO.getSettingValue();
                                    String bg_class = "odd";
                                    if (mvtscdrList != null && mvtscdrList.size() > 0) {
                                        for (int inc = 0; inc < mvtscdrList.size() - 1; inc++) {

                                            CDRReportDTO obj = mvtscdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }

                                            CDRReportDTO obj1 = mvtscdrList.get(mvtscdrList.size() - 1);
                                            total_time = obj1.getTotal_duration();
                                            total_origin_bill = obj1.getTotal_origin_bill_amount();
                                            String connectionTime = Utils.ToDateDDMMYYYYhhmmss(Utils.getTimeLong(timeZone) + Utils.getDateLong(obj.getConnection_time()));

                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="right"><%=pageStart + inc + 1%>.</td>
                                    <td><%=obj.getOrigin_caller()%></td>
                                    <td><%=obj.getDialed_no()%></td>
                                    <td align="center"><%=obj.getOrigin_ip()%></td>
                                    <td><%=obj.getOrigin_prefix()%></td>
                                    <td><%=obj.getOrigin_destination()%></td>
                                    <td><%=obj.getOrigin_rate_des()%></td>
                                    <td align="right"><%=amount_formatter.format(obj.getOrigin_bill_amount())%></td>
                                    <td align="right"><%=Utils.getTimeMMSS(obj.getDuration())%></td>
                                    <td><%=connectionTime%></td>
                                    <td><%=obj.getPdd()%></td>
                                    <td><%=obj.getDisconnect_cause()%></td>
                                    <% if (searchDTO != null && searchDTO.getCallType() == 2) {%>
                                    <td><%=obj.getAuth_error_cause()%></td>
                                    <%}%>
                                </tr>
                                <% }%>
                                <tr>
                                    <th colspan="7">Total: </th>
                                    <th style="text-align: right"><%=amount_formatter.format(total_origin_bill)%></th>
                                    <th style="text-align: right"><%=Utils.getMMSS(total_time)%></th>
                                    <th colspan="2">&nbsp;</th>
                                    <th colspan="2">&nbsp;</th>
                                </tr>
                                <%   } else {%>
                                <tr><th colspan="13">No Content Found!</th></tr>
                                <% }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>