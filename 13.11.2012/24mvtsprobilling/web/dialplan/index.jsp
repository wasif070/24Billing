<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }   
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Dial Plan List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "dialplan");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
            <c:set var="delPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.DELETE])%>"></c:set>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                   java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                   navList.add("dialplan/listDialplan.do?list_all=1;Dial Plan");
                   navList.add(";List Dial Plan");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/dialplan/listDialplan.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Dial Plan Name</th>
                                    <td><html:text property="dialplan_name" /></td>
                                    <th>Gateway Name</th>
                                    <td><html:text property="dialplan_gateway_list_name" /></td>
                                    <th>Allowed Prefixes</th>
                                    <td><html:text property="dialplan_dnis_pattern" /></td>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="dialplan_enable">
                                            <html:option value="-1">Select Status</html:option>
                                            <%
                                             for(int i=0;i<Constants.DIALPLAN_STATUS_VALUE.length;i++)
                                             {
                                            %>
                                            <html:option value="<%=Constants.DIALPLAN_STATUS_VALUE[i]%>"><%=Constants.DIALPLAN_STATUS_STRING[i]%></html:option>
                                            <%
                                             }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" style="width:50px" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" style="width:50px" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                    <td colspan="4">
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>
                    <html:form action="/dialplan/multipleDialplan" method="post" onsubmit="return confirm('Are you sure to take action?');">
                        <div class="over_flow_content display_tag_content " align="center">
                            <bean:write name="DialplanForm" property="message" filter="false"/>
                            <div class="jerror_messge"></div>
                            <bean:write name="DialplanForm" property="message" filter="false"/>
                            <%=msg%>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.DialplanForm.dialplanList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="Dialplan" /> 
                                <display:setProperty name="paging.banner.items_name" value="Dialplans" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='One Dial Plan' />" style="width:5%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="Dial Plan Name" sortProperty="dialplan_name" sortable="true"  style="width:15%" >
                                    ${data.dialplan_name}
                                </display:column>
                                <display:column  property="dialplan_description" class="left-align" title="Description" sortable="true" style="width:18%" />                 
                                <display:column  property="dialplan_dnis_pattern" class="left-align" title="Destination Prefix Allow" sortable="true" style="width:18%" />
                                <display:column  property="dialplan_gateway_list_name" class="left-align" title="Gateway Name" sortable="true" style="width:18%" />
                                <display:column  property="dialplan_ani_translate" class="left-align" title="SRC Number Translate" sortable="true" style="width:18%" />
                                <display:column  property="dialplan_dnis_translate" class="left-align" title="DST Number Translate" sortable="true" style="width:18%" /> 
                                <display:column  property="dialplan_hunt_mode_name" class="left-align" title="Balancing Method" sortable="true" style="width:18%" />
                                <display:column  property="dialplan_sched_type_name" class="center-align" title="Scheduling" sortable="true" style="width:10%"/>
                                <display:column  property="dialplan_sched_tod_on_name" class="center-align" title="On Time" sortable="true" style="width:10%"/>
                                <display:column  property="dialplan_sched_tod_off_name" class="center-align" title="Off Time" sortable="true" style="width:10%"/>
                                <display:column  property="dialplan_enable_name" class="center-align" title="Status" sortable="true" style="width:10%" />  
                                <c:choose>
                                    <c:when test="${editPermission==1 || delPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:10%;" >
                                            <c:choose>
                                                <c:when test="${editPermission==1}">
                                                    <a href="../dialplan/getDialplan.do?id=${data.id}" class="edit"  title="Change"></a>
                                                </c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${delPermission==1}">
                                                    <a href="../dialplan/deleteDialplan.do?id=${data.id}" onclick="javascript:return confirm('Are you sure to want delete the Dial Plan?');" class="drop" title="Delete"></a>
                                                </c:when>
                                            </c:choose>
                                        </display:column>
                                    </c:when>
                                </c:choose>
                            </display:table>                                                 
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>

                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                                <%if(perms[com.myapp.struts.util.AppConstants.EDIT]==1){%>
                                <html:submit property="activateBtn" styleClass="custom-button jmultipebtn" value="Activate" />
                                <html:submit property="inactiveBtn" styleClass="custom-button jmultipebtn" value="Inactive" />                                
                                <%}if(perms[com.myapp.struts.util.AppConstants.DELETE]==1){%>
                                <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                                <%}%>
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>