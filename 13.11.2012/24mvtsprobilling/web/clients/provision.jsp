<%@page import="com.myapp.struts.clients.ClientDTO"%>
<%@page import="com.myapp.struts.clients.ClientLoader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.rateplan.RateplanLoader,com.myapp.struts.rateplan.RateplanDTO" %>
        <title> <%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Client Provisioning</title>
        <%@include file="../includes/header.jsp"%>
        <%
            ArrayList<RateplanDTO> rateplanDto = RateplanLoader.getInstance().getRateplanDTOList(login_dto);
            long rateplanid = 0;
            if (login_dto.getOwn_id() > 0) {
                ClientDTO client_dto = com.myapp.struts.clients.ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
                if (client_dto != null) {
                    rateplanid = client_dto.getRateplan_id();
                }
            }
        %>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "client");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right"> 
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("clients/listClient.do?list_all=1;Clients");
                        navList.add(";Add New Client");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/clients/clientProvisioning" method="post">                        
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <tbody>
                                <tr>
                                    <td>
                                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add Client Information</legend>
                                            <table width="100%">
                                                <tr>
                                                    <td colspan="2" align="center"  valign="bottom">
                                                        <bean:write name="ClientForm" property="message" filter="false"/>
                                                    </td>
                                                </tr>                                                                                                                    
                                                <tr>
                                                    <th valign="top" >Client ID</th>
                                                    <td valign="top" >
                                                        <html:text property="client_id" /><br/>
                                                        <html:messages id="client_id" property="client_id">
                                                            <bean:write name="client_id"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <th valign="top" >Client Name</th>
                                                    <td valign="top" >
                                                        <html:text property="client_name" /><br/>
                                                        <html:messages id="client_name" property="client_name">
                                                            <bean:write name="client_name"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Origination IP<span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:text property="origination_ip" /><br/>
                                                        <html:messages id="origination_ip" property="origination_ip">
                                                            <bean:write name="origination_ip"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Rate Plan <span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="rateplan_id" styleId="rateplan_id_container">
                                                            <html:option value="-1">Select Rate Plan</html:option>
                                                            <%
                                                                String rateplan_name = "Not Found";
                                                                if (rateplanDto != null && rateplanDto.size() > 0) {
                                                                    for (int i = 0; i < rateplanDto.size(); i++) {
                                                                        RateplanDTO rdto = new RateplanDTO();
                                                                        rdto = rateplanDto.get(i);
                                                                        rateplan_name = rdto.getRateplan_name();
                                                                        if (rateplanid == rdto.getRateplan_id()) {
                                                                            rateplan_name = "Base Rateplan";
                                                                        }
                                                            %>
                                                            <html:option value="<%=String.valueOf(rdto.getRateplan_id())%>"><%=rateplan_name%></html:option>
                                                            <%
                                                                    }
                                                                }
                                                            %>
                                                        </html:select><br/>
                                                        <html:messages id="rateplan_id" property="rateplan_id">
                                                            <bean:write name="rateplan_id"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th valign="top" >Dest. No. Translate<span class="req_mark">*</span></th>
                                                    <td valign="top" >
                                                        <html:select property="dest_no_translate" >
                                                            <html:option value="0">No</html:option>
                                                            <html:option value="1">Yes</html:option>
                                                        </html:select>
                                                        <br/>
                                                        <html:messages id="dest_no_translate" property="dest_no_translate">
                                                            <bean:write name="dest_no_translate"  filter="false"/>
                                                        </html:messages>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td align="center">
                                        <input name="submit" type="submit" class="custom-button" value="Add" />
                                        <input type="reset" class="custom-button" value="Reset" />
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <%
                            request.getSession(true).removeAttribute(Constants.MESSAGE);
                        %>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>