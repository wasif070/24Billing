<%@page import="com.myapp.struts.gateway.GatewayLoader"%>
<%@page import="com.myapp.struts.gateway.GatewayDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Add Gateway</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "gateway");
                SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
                int is_hard_switch = Integer.parseInt(settings.getSettingValue());
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("gateway/listGateway.do?list_all=1;Gateway");
                        navList.add(";Add New Gateway");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/gateway/addGateway" method="post">   
                        <%
                            int type = -1;
                        %>
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add Gateway Information</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="GatewayForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Gateway <%=is_hard_switch == AppConstants.NO ? "IP" : "Identity"%><span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="gateway_ip" /><br/>
                                            <html:messages id="gateway_ip" property="gateway_ip">
                                                <bean:write name="gateway_ip"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <% if (is_hard_switch == AppConstants.NO) {%>
                                    <tr>
                                        <th valign="top" >Protocol <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="protocol_id" styleId="protocol_id" >
                                                <html:option value="-1">Select</html:option>
                                                <html:option value="0">H.323</html:option>
                                                <html:option value="1">SIP</html:option>
                                                <html:option value="4">SIP-T</html:option>
                                                <html:option value="2">SS7</html:option>
                                            </html:select>
                                            <div class="clear"></div>
                                            <html:messages id="protocol_id" property="protocol_id">
                                                <bean:write name="protocol_id"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr> 
                                    <tr>
                                        <th valign="top" >Enable Radius</th>
                                        <td valign="top" >
                                            <html:select property="enable_radius" styleId="enable_radius" >
                                                <html:option value="1">Yes</html:option>
                                                <html:option value="0">No</html:option>                                                
                                            </html:select>
                                            <div class="clear"></div>
                                            <html:messages id="enable_radius" property="enable_radius">
                                                <bean:write name="enable_radius"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <%}
                                        if (login_dto.getClient_level() == 2) {%>
                                    <tr>
                                        <th valign="top" >Gateway Type <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="gateway_type" styleId="jgateway_type" onchange="value">
                                                <html:option value="-1">Select Gateway Type</html:option>           
                                                <html:option value="<%=Constants.CLIENT_TYPE[0]%>"><%=Constants.CLIENT_TYPE_NAME[0]%></html:option>
                                            </html:select><br/>
                                            <html:messages id="gateway_type" property="gateway_type">
                                                <bean:write name="gateway_type"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                    
                                    <%} else {%>
                                    <tr>
                                        <th valign="top" >Gateway Type <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="gateway_type" styleId="jgateway_type" onchange="value">
                                                <html:option value="-1">Select Gateway Type</html:option>       
                                                <%
                                                    for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="gateway_type" property="gateway_type">
                                                <bean:write name="gateway_type"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <%}%> 
                                    <% if (is_hard_switch == AppConstants.NO) {%>
                                    <tr>
                                        <th valign="top" >Destination Port <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="gateway_dst_port_h323" value="1720" styleId="gateway_dst_port_h323" disabled="<%=type == -1 ? true : false%>"/><br/>
                                            <html:messages id="gateway_dst_port_h323" property="gateway_dst_port_h323">
                                                <bean:write name="gateway_dst_port_h323"  filter="false" />
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Destination Port SIP <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="gateway_dst_port_sip" value="5060"  styleId="gateway_dst_port_sip" disabled="<%=type == -1 ? true : false%>"/><br/>
                                            <html:messages id="gateway_dst_port_sip" property="gateway_dst_port_sip">
                                                <bean:write name="gateway_dst_port_sip"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>  
                                    <%}%>
                                    <tr>
                                        <th valign="top">Client <span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <html:select property="clientId" styleId="jclientId">
                                                <html:option value="">Select Client</html:option>                                            
                                            </html:select><br/>
                                            <html:messages id="clientId" property="clientId">
                                                <bean:write name="clientId"  filter="false"/>
                                            </html:messages>
                                        </td>                             
                                    </tr>
                                    <tr>
                                        <th valign="top" >Gateway Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="gateway_name" /><br/>
                                            <html:messages id="gateway_name" property="gateway_name">
                                                <bean:write name="gateway_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Status <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="gateway_status">
                                                <%
                                                    for (int i = 0; i < Constants.GATEWAY_STATUS_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.GATEWAY_STATUS_VALUE[i]%>"><%=Constants.GATEWAY_STATUS_STRING[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="gateway_status" property="gateway_status">
                                                <bean:write name="gateway_status"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Prefix Allow </th>
                                        <td valign="top" >
                                            <html:text property="src_dnis_prefix_allow" disabled="true" /><br/>
                                            <html:messages id="src_dnis_prefix_allow" property="src_dnis_prefix_allow">
                                                <bean:write name="src_dnis_prefix_allow"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <%if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("CLIENT_PROVISIONING").getSettingValue()) == AppConstants.YES) {%>
                                    <tr class="operators display_off">
                                        <th valign="top" >Operators</th>
                                        <td valign="top" >
                                            <%
                                                ArrayList<GatewayDTO> gate_list = GatewayLoader.getInstance().getOperators();
                                                int i = 1;
                                                String new_line = "";
                                                for (GatewayDTO gate : gate_list) {
                                                    new_line = (i % 3 == 0 ? "<br/>" : "");
                                                    i++;
                                            %>                                            
                                            <html:checkbox property="prefixes" value="<%=String.valueOf(gate.getId())%>"/><%=gate.getPrefix()%>
                                            <%=new_line%>
                                            <%}%>
                                            <br/>
                                            <html:messages id="prefixes" property="prefixes">
                                                <bean:write name="prefixes"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input type="hidden" name="searchLink" value="nai" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>                                    
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>

    <script language="javascript" type="text/javascript">
        $j(document).ready(function(){
            if($j("#jgateway_type").val()>-1){                
                $j.ajax({
                    type:'post',
                    url:'../gateway/load_client.jsp',
                    data:{
                        type:$j("#jgateway_type").val()
                    },
                    success:function(html){
                        $j("#jclientId").html(html);
                    }
                });
            }
        });
    </script>
</html>
