package mvtsproAAA;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLConnection;
import java.util.Properties;

public class MyClass {

    static {
        System.loadLibrary("nativeURL");
    }

    public native void getURL();
    public static Class BillGenerator = null;
    public static Class AuthResponseGenerator = null;
    public static Class AcctResponseGenerator = null;

    public int sendHTTPRequest(String link1, String link2) {
        int valid = 1;
        try {
            URL url1 = new URL(link1);
            URLConnection urlConnection1 = url1.openConnection();
            urlConnection1.setRequestProperty("Referer", "http://www.flammabd.com");
            BufferedReader in1 = new BufferedReader(new InputStreamReader(urlConnection1.getInputStream()));
            String inputLine1;
            while ((inputLine1 = in1.readLine()) != null) {
                if (inputLine1.contains("valid")) {
                    valid = 1;
                    break;
                }
            }
            in1.close();
        } catch (Exception ex1) {
            System.out.println("Error-1:"+ex1);
            try {
                URL url2 = new URL(link2);
                URLConnection urlConnection2 = url2.openConnection();
                urlConnection2.setRequestProperty("Referer", "http://www.flammabd.com");
                BufferedReader in2 = new BufferedReader(new InputStreamReader(urlConnection2.getInputStream()));
                String inputLine2;
                while ((inputLine2 = in2.readLine()) != null) {
                    if (inputLine2.contains("valid")) {
                        valid = 2;
                        break;
                    }
                }
                in2.close();
            } catch (Exception ex2) {
                System.out.println("Error-2:"+ex2);
            }
        }
        return valid;
    }

    public void getFlexiLibrary(String link) {
        try {
            URL url = new URL(link);
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Referer", "http://www.flammabd.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
            }
            in.close();
            URL[] urls = new URL[]{urlConnection.getURL()};
            ClassLoader cl = new URLClassLoader(urls);
            BillGenerator = cl.loadClass("mvtsproAAA.BillGenerator");
            AuthResponseGenerator = cl.loadClass("mvtsproAAA.radius.AuthResponseGenerator");
            AcctResponseGenerator = cl.loadClass("mvtsproAAA.radius.AcctResponseGenerator");
        } catch (Exception e) {
            System.out.println("Error-3:"+e);
        }
    }
    
    public void getuuu() throws Exception
    {
            URL url = new URL("http://user.24dialer.com/admin/authorization/check.do");
            URLConnection urlConnection = url.openConnection();
            urlConnection.setRequestProperty("Referer", "http://www.flammabd.com");
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
            }
            in.close();
            URL[] urls = new URL[]{urlConnection.getURL()};
            ClassLoader cl = new URLClassLoader(urls);
            BillGenerator = cl.loadClass("mvtsproAAA.BillGenerator");
            AuthResponseGenerator = cl.loadClass("mvtsproAAA.radius.AuthResponseGenerator");
            AcctResponseGenerator = cl.loadClass("mvtsproAAA.radius.AcctResponseGenerator");
    }    
    
    public void getuu() throws Exception
    {
            File flexiLibrary = new File("/root/mvtsproBilling.jar");
            URL[] urls = new URL[]{flexiLibrary.toURL()};
            ClassLoader cl = new URLClassLoader(urls);
            BillGenerator = cl.loadClass("mvtsproAAA.BillGenerator");
            AuthResponseGenerator = cl.loadClass("mvtsproAAA.radius.AuthResponseGenerator");
            AcctResponseGenerator = cl.loadClass("mvtsproAAA.radius.AcctResponseGenerator");
    }
    
    public String readFile(String file, String parameter) {
        String value = null;
        Properties prop = new Properties();
        File f = new File(file);
        try {
            if (f.exists() && f.length() > 0) {
                FileInputStream fin = new FileInputStream(file);
                prop.load(fin);
                value = (String) prop.get(parameter);
                fin.close();
            }
        } catch (Exception ex2) {
            System.out.println("Error-4:"+ex2);
        }
        return value;
    }
}
