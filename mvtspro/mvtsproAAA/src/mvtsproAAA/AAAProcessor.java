package mvtsproAAA;

import databaseconnector.DBConnection;
import java.io.File;
import java.io.FileInputStream;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Properties;
import mvtsproAAA.radius.Radius;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AAAProcessor {

    public final static long RELOAD_INTERVAL = 5 * 1000L;
    public final static long REMOVE_CHECKING_INTERVAL = 5 * 60 * 1000L;
    public static MyClass myClass = null;
    static BillGeneratorInterface billGenerator = null;
    static ShutdownDetection shutdownDetection = null;
    static HashMap<Integer, String> diconnectCauseMap = null;
    static Logger logger = Logger.getLogger(AAAProcessor.class.getName());

    public static void main(String[] args) throws Exception {
        PropertyConfigurator.configure("log4j.properties");
        shutdownDetection = new ShutdownDetection();
        shutdownDetection.start();
        myClass = new MyClass();
        myClass.getURL();
        //myClass.getuuu();
        billGenerator = (BillGeneratorInterface) myClass.BillGenerator.newInstance();
        Radius.radiusServiceStart();
        logger.debug("24 MVTSPRO AAA has been started at: " + new Date());
        System.out.println("24 MVTSPRO AAA has been started at: " + new Date());
        int enableBalanceDeductionValue = 1;
        long lastCDRIDValue = 0;
        long executionTime = 0;
        String condition = "1";
        
        try {
            long fromCDRID = 0;
            long toCDRID = 0;
            String cdrDate = null;

            Properties prop = new Properties();
            File cdrConfigFile = new File("cdrConfig.txt");
            File idFile = new File("cdrID.txt");
            if (cdrConfigFile.exists()) {

                FileInputStream fin = new FileInputStream(cdrConfigFile);
                prop.load(fin);
                if (prop.containsKey("BALANCE_DEDUCT")) {
                    try {
                        enableBalanceDeductionValue = Integer.parseInt((String) prop.get("BALANCE_DEDUCT"));
                    } catch (Exception e) {
                    }
                }
                if (prop.containsKey("FROM_CDR_ID")) {
                    try {
                        fromCDRID = Long.parseLong(String.valueOf(prop.get("FROM_CDR_ID")));
                    } catch (Exception e) {
                    }
                }
                if (prop.containsKey("TO_CDR_ID")) {
                    try {
                        toCDRID = Long.parseLong(String.valueOf(prop.get("TO_CDR_ID")));
                    } catch (Exception e) {
                    }
                }
                if (prop.containsKey("CDR_DATE")) {
                    cdrDate = prop.get("CDR_DATE").toString();
                    if (!isValidDate(cdrDate)) {
                        logger.debug("Invalid Date Format! Please Correct Date Format: yyyy-MM-dd HH:mm:ss Example: 2011-08-04 03:36:03");
                        System.out.println("Invalid Date Format!");
                        System.exit(0);
                    }
                }
                fin.close();
            }
            logger.debug("is balance deduction enabled: " + enableBalanceDeductionValue);
            if (enableBalanceDeductionValue == 0) {
                condition = "cdr_id >= " + fromCDRID + (toCDRID > 0 ? " and cdr_id <= " + toCDRID : " ");
            } else {
                if (idFile.exists()) {
                    FileInputStream fin1 = new FileInputStream(idFile);
                    prop.load(fin1);
                    try {
                        if (prop.containsKey("CDR_ID")) {
                            lastCDRIDValue = Long.parseLong(String.valueOf(prop.get("CDR_ID")));
                        }
                    } catch (Exception e) {
                        logger.fatal("Error:", e);
                    }
                    condition = "cdr_id > " + lastCDRIDValue;
                    logger.debug("LAST CDR ID: " + lastCDRIDValue);
                    fin1.close();
                } else if (cdrDate != null) {
                    condition = "cdr_date >= '" + cdrDate + "'";
                }
            }
        } catch (Exception e) {
            logger.fatal("Exception in congig loading: ", e);
        }
        
        billGenerator.init(enableBalanceDeductionValue, lastCDRIDValue);
        while (true) {
            try {
                if ((System.currentTimeMillis() - executionTime) > REMOVE_CHECKING_INTERVAL) {
                    removeOldCallFromCurrentCall();
                    removeOldRates();
                    executionTime = System.currentTimeMillis();
                }
                lastCDRIDValue = billGenerator.generateBill(condition);
                if (lastCDRIDValue > 0) {
                    condition = " cdr_id > " + lastCDRIDValue;
                }
                Thread.sleep(RELOAD_INTERVAL);
            } catch (Exception e) {
            }
        }
    }

    public static void removeOldCallFromCurrentCall() {
        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "delete from current_call where inserted_time<(UNIX_TIMESTAMP()-3660)";
            stmt.execute(sql);
        } catch (Exception ex) {
            logger.fatal("Exception in removeOldCallFromCurrentCall: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public static void removeOldRates() {
        DBConnection dbConnection = null;
        Statement stmt = null;
        long SEVEN_DAYS_IN_SECOND = 7 * 24 * 60 * 60;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "delete from mvts_rates where rate_delete_time<(UNIX_TIMESTAMP()-" + SEVEN_DAYS_IN_SECOND + ") and rate_delete = 1";
            stmt.execute(sql);
        } catch (Exception ex) {
            logger.fatal("Exception in removeOldCallFromCurrentCall: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
    
    public static boolean isValidDate(String dateStr) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        dateFormat.setLenient(false);
        try {
            dateFormat.parse(dateStr.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }    
}
