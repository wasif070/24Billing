package mvtsproAAA;

public interface BillGeneratorInterface {

    public void init(int enableBalanceDeductionVal, long lastCDRIDVal);

    public long generateBill(String checkingStr);
}
