package mvtsproAAA;

import java.io.File;
import java.util.Date;
import mvtsproAAA.radius.Radius;
import org.apache.log4j.Logger;

public class ShutdownDetection extends Thread {

    private boolean running = false;
    private static long shutdownDetectionInterval = 1000;
    private static File file = new File("shutdown.dat");
    static Logger logger = Logger.getLogger(ShutdownDetection.class.getName());

    public ShutdownDetection() {
        running = true;
        if (file.exists()) {
            try {
                file.delete();
            } catch (Exception e) {
                try {
                    Thread.sleep(shutdownDetectionInterval);
                    file.delete();
                } catch (Exception ex) {
                }
            }
        }
    }

    @Override
    public void run() {
        while (running) {
            if (file.exists()) {
                try {
                    file.delete();
                } catch (Exception e) {
                }
                break;
            }
            try {
                Thread.sleep(shutdownDetectionInterval);
            } catch (Exception e) {
            }
        }
        Radius.authPacketProcessor.stopService();
        Radius.authResponseProcessor.stopService();
        Radius.acctPacketProcessor.stopService();
        Radius.acctResponseProcessor.stopService();        
        logger.debug("24 MVTSPRO AAA has been stopped at: " + new Date());
        System.out.println("24 MVTSPRO AAA has been stopped at: " + new Date());
        System.exit(0);
    }
}
