package mvtsproAAA.radius;

import java.net.DatagramPacket;
import org.apache.log4j.Logger;

public class AuthResponseProcessor extends Thread {

    static Logger logger = Logger.getLogger(AuthResponseProcessor.class.getName());
    private boolean running;
    private boolean timeForStop;
    static long processingInterval = 50L;
    static String sharedSecret = null;
    static AuthResponseGeneratorInterface authResponseGenerator = null;

    public AuthResponseProcessor(String secret) {
        running = true;
        timeForStop = true;
        sharedSecret = secret;
        try {
            authResponseGenerator = (AuthResponseGeneratorInterface) mvtsproAAA.AAAProcessor.myClass.AuthResponseGenerator.newInstance();
        } catch (Exception e) {
            logger.debug("Exception in creating AuthResponseGenerator: ", e);
        }
    }

    @Override
    public void run() {
        logger.debug("RadiusAuthResponseProcessor is started.....");
        while (running) {
            try {
                while (!AuthPacketQueue.getInstance().isEmpty()) {
                    timeForStop = false;
                    DatagramPacket authPacket = AuthPacketQueue.getInstance().pop();
                    String clientIP = authPacket.getAddress().getHostAddress();
                    int clientPort = authPacket.getPort();
                    byte data[] = authPacket.getData();
                    byte[] response = authResponseGenerator.generateAuthResponse(data, sharedSecret);
                    if (response != null) {
                        Radius.authPacketProcessor.sendResponse(clientPort, clientIP, response);
                    }
                    if (!running) {
                        break;
                    }
                }
                timeForStop = true;
            } catch (Exception ex) {
                logger.fatal("Error while auth response packet Processing: ", ex);
            }
            try {
                Thread.sleep(processingInterval);
            } catch (Exception ex) {
            }
        }
        timeForStop = true;
    }

    public void stopService() {
        running = false;
        while (!timeForStop) {
            try {
                Thread.sleep(10);
            } catch (Exception ex) {
            }
        }
    }
}