package mvtsproAAA.radius;

import java.net.DatagramPacket;
import org.apache.log4j.Logger;

public class AcctResponseProcessor extends Thread {

    static Logger logger = Logger.getLogger(AcctResponseProcessor.class.getName());
    private boolean running;
    private boolean timeForStop;
    static long processingInterval = 50L;
    static String sharedSecret = null;
    static AcctResponseGeneratorInterface acctResponseGenerator = null;

    public AcctResponseProcessor(String secret) {
        running = true;
        timeForStop = true;
        sharedSecret = secret;
        try {
            acctResponseGenerator = (AcctResponseGeneratorInterface) mvtsproAAA.AAAProcessor.myClass.AcctResponseGenerator.newInstance();
        } catch (Exception e) {
            logger.debug("Exception in creating AcctResponseGenerator: ", e);
        }
    }

    @Override
    public void run() {
        logger.debug("RadiusAcctResponseProcessor is started.....");
        while (running) {
            try {
                while (!AcctPacketQueue.getInstance().isEmpty()) {
                    timeForStop = false;
                    DatagramPacket authPacket = AcctPacketQueue.getInstance().pop();
                    String clientIP = authPacket.getAddress().getHostAddress();
                    int clientPort = authPacket.getPort();
                    byte[] data = authPacket.getData();   
                    byte[] response = acctResponseGenerator.generateAcctResponse(data, sharedSecret);
                    if(response != null)
                    {    
                       Radius.acctPacketProcessor.sendResponse(clientPort, clientIP, response);
                    }   
                    if (!running) {
                        break;
                    }
                }
                timeForStop = true;
            } catch (Exception ex) {
                logger.fatal("Error while acc response packet Processing: ", ex);
            }
            try {
                Thread.sleep(processingInterval);
            } catch (Exception ex) {
            }
        }
        timeForStop = true;
    }

    public void stopService() {
        running = false;
        while (!timeForStop) {
            try {
                Thread.sleep(10);
            } catch (Exception ex) {
            }
        }
    }
}
