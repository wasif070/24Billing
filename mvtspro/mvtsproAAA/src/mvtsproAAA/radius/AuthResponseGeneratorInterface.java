package mvtsproAAA.radius;

public interface AuthResponseGeneratorInterface {

    public byte[] generateAuthResponse(byte[] data, String sharedSecret);
}
