package mvtsproAAA.radius;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Radius {

    static final String CONFIG = "mvtspro.conf";
    static String BIND_IP = "";
    static String SHARED_SECRET = "";
    static int AUTH_PORT = 0;
    static int ACCT_PORT = 0;
    public static AuthPacketProcessor authPacketProcessor = null;
    public static AuthResponseProcessor authResponseProcessor = null;
    public static AcctPacketProcessor acctPacketProcessor = null;
    public static AcctResponseProcessor acctResponseProcessor = null;
    static Logger logger = Logger.getLogger(Radius.class.getName());

    public static void radiusServiceStart() {
        try {
            InputStream input = null;
            File file = new File(CONFIG);
            if (file.exists()) {
                input = new FileInputStream(file);
            }
            Properties prop = new Properties();
            prop.load(input);
            input.close();
            if (prop.containsKey("BIND_IP")) {
                BIND_IP = prop.getProperty("BIND_IP");
                logger.debug("BIND_IP: " + BIND_IP);
            } else {
                logger.debug("No BIND_IP is found in mvtspro.conf");
            }
            if (prop.containsKey("AUTH_PORT")) {
                AUTH_PORT = Integer.parseInt(prop.getProperty("AUTH_PORT"));
                logger.debug("AUTH_PORT: " + AUTH_PORT);
            } else {
                logger.debug("No BIND_PORT is found in mvtspro.conf");
            }
            if (prop.containsKey("ACCT_PORT")) {
                ACCT_PORT = Integer.parseInt(prop.getProperty("ACCT_PORT"));
                logger.debug("ACCT_PORT: " + ACCT_PORT);
            } else {
                logger.debug("No ACCT_PORT is found in mvtspro.conf");
            }

            if (prop.containsKey("SHARED_SECRET")) {
                SHARED_SECRET = prop.getProperty("SHARED_SECRET");
                logger.debug("SHARED_SECRET: " + SHARED_SECRET);
            } else {
                logger.debug("No SHARED_SECRET is found in mvtspro.conf");
            }
        } catch (Exception e) {
            logger.fatal("Exception in reading mvtspro.conf:" + e);
        }
        
        authPacketProcessor = new AuthPacketProcessor(BIND_IP, AUTH_PORT);
        authPacketProcessor.start();
        authResponseProcessor = new AuthResponseProcessor(SHARED_SECRET);
        authResponseProcessor.start();
        logger.debug("24 MVTSPRO Auth is started at: " + new Date());
        System.out.println("24 MVTSPRO Auth is started at: " + new Date());
        acctPacketProcessor = new AcctPacketProcessor(BIND_IP, ACCT_PORT);
        acctPacketProcessor.start();
        acctResponseProcessor = new AcctResponseProcessor(SHARED_SECRET);
        acctResponseProcessor.start();
        logger.debug("24 MVTSPRO Acct is started at: " + new Date());
        System.out.println("24 MVTSPRO Acct is started at: " + new Date());
    }
}
