package mvtsproAAA.radius;

import java.net.DatagramPacket;

public class AcctPacketQueue {

    private static AcctPacketQueue packetQueue = null;
    DatagramPacket packets[];
    int h, t;
    int initialSize = 10000;

    private AcctPacketQueue() {
        packets = new DatagramPacket[initialSize];
        h = t = 0;
    }

    public static AcctPacketQueue getInstance() {
        if (packetQueue == null) {
            createPacketQueue();
        }
        return packetQueue;
    }

    private synchronized static void createPacketQueue() {
        if (packetQueue == null) {
            packetQueue = new AcctPacketQueue();
        }
    }

    public synchronized void destroy() {
        packets = null;
        packetQueue = null;
    }

    public synchronized void push(DatagramPacket packet) {
        if (isFull()) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        if (isEmpty()) {
            notify();
        }
        packets[t] = packet;
        t = (t + 1) % packets.length;
    }

    public synchronized DatagramPacket pop() {
        if (isEmpty()) {
            try {
                wait();
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        if (isFull()) {
            notify();
        }

        DatagramPacket packet = packets[h];
        packets[h] = null;
        h = (h + 1) % packets.length;
        return packet;
    }

    public boolean isFull() {
        return (t + 1) % packets.length == h;
    }

    public boolean isEmpty() {
        return (h == t);
    }
}
