package mvtsproAAA.radius;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import org.apache.log4j.Logger;

public class AuthPacketProcessor extends Thread {

    static Logger logger = Logger.getLogger(AuthPacketProcessor.class.getName());
    private boolean running;
    DatagramSocket auth_socket;

    public AuthPacketProcessor(String bindIP, int radiusAuthPort) {
        running = true;
        try {
            auth_socket = new DatagramSocket(radiusAuthPort, InetAddress.getByName(bindIP));
        } catch (Exception ex) {
            logger.fatal("Exception in creating Radius Auth Socket:", ex);
            running = false;
        }
    }

    @Override
    public void run() {
        logger.debug("RadiusAuthPacketProcessor is started.....");
        while (running) {
            try {
                byte data[] = new byte[4096];
                DatagramPacket receivePacket = new DatagramPacket(data, data.length);
                auth_socket.receive(receivePacket);
                AuthPacketQueue.getInstance().push(receivePacket);
            } catch (Exception ex) {
                logger.fatal("Exception in receiving packet:", ex);
            }
        }
        if (auth_socket != null && !auth_socket.isClosed()) {
            auth_socket.close();
        }
    }

    public void sendResponse(int clientPort, String clientIP, byte[] data) {
        try {
            SocketAddress clientAddress = new InetSocketAddress(clientIP, clientPort);
            auth_socket.send(new DatagramPacket(data, data.length, clientAddress));
        } catch (Exception ex) {
            logger.fatal("Exception in sending packet:", ex);
        }
    }

    public void stopService() {
        running = false;
    }
}
