-- phpMyAdmin SQL Dump
-- version 3.1.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 03, 2011 at 08:29 AM
-- Server version: 5.1.33
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `flamma_mvtspro`
--

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
CREATE TABLE IF NOT EXISTS `clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` varchar(50) NOT NULL,
  `rateplan_id` int(11) NOT NULL,
  `client_password` varchar(50) NOT NULL,
  `client_name` varchar(150) NOT NULL,
  `client_email` varchar(100) NOT NULL,
  `client_type` tinyint(4) NOT NULL,
  `client_created` decimal(18,0) NOT NULL,
  `client_status` tinyint(1) NOT NULL DEFAULT '1',
  `client_credit_limit` double NOT NULL DEFAULT '0',
  `client_balance` decimal(10,2) NOT NULL DEFAULT '0.00',
  `client_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `clients`
--

INSERT INTO `clients` (`id`, `client_id`, `rateplan_id`, `client_password`, `client_name`, `client_email`, `client_type`, `client_created`, `client_status`, `client_credit_limit`, `client_balance`, `client_delete`) VALUES
(2, 'flammabd', 7, 'aaaaaa', 'Flamma Corporation Ltd.', 'anwar@flammabd.com', 2, 1304858849953, 0, 500, 50000.00, 0),
(11, 'admin001', 7, 'aaaaaa', 'Tester', 'ssc@gmail.com', 0, 1304858849953, 0, 500, 11000.00, 0),
(15, 'meratest', 7, 'meratest', '', '', 1, 1304858849953, 0, 500, 50000.00, 0),
(16, 'Saiful Sir', 7, 's@iful', 'Saiful Sir', '', 1, 1304858849953, 0, 500, 50000.00, 0),
(18, 'Ataur', 7, 'aaaaaa', 'Ataur Bhai', '', 0, 1304858849953, 0, 500, 50000.00, 0),
(19, 'Ujjal', 7, 'ujj@lbtcl', 'Ujjal Bhai', '', 1, 1304858849953, 0, 500, 50000.00, 1),
(22, 'Tuhin', 7, 'tuhin321', 'Tuhin Bhai', '', 0, 1304858849953, 0, 500, 50000.00, 0),
(23, 'Teleworld', 7, 'teleworld', 'Teleworld', '', 0, 1304858849953, 0, 500, 50000.00, 0),
(24, 'BTrac', 7, 'btracipv', 'Bangla Trac', '', 1, 1304858849953, 0, 500, 50000.00, 0),
(25, 'testerforrate', 7, 'aaaaaa', 'aaaaaaa', 'aaaa@gmail.com', 1, 1304858849953, 0, 500, 50000.00, 1),
(26, 'Route1', 7, 'ipvroute', '', '', 1, 1304858849953, 0, 500, 50000.00, 0),
(28, 'flammaworld', 7, 'aaaaaa', 'Flamma', '', 0, 1304858849953, 0, 500, 10000.00, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_transactions`
--

DROP TABLE IF EXISTS `client_transactions`;
CREATE TABLE IF NOT EXISTS `client_transactions` (
  `transaction_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `transaction_type` tinyint(1) NOT NULL DEFAULT '1',
  `transaction_recharge` double NOT NULL,
  `transaction_return` double NOT NULL,
  `transaction_receive` double NOT NULL DEFAULT '0',
  `transaction_des` varchar(255) DEFAULT NULL,
  `transaction_date` decimal(18,0) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`transaction_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `client_transactions`
--

INSERT INTO `client_transactions` (`transaction_id`, `client_id`, `transaction_type`, `transaction_recharge`, `transaction_return`, `transaction_receive`, `transaction_des`, `transaction_date`, `user_id`) VALUES
(1, 11, 1, 10000, 0, 0, '', 1312281063626, 1),
(2, 18, 1, 50000, 0, 0, '', 1312281063640, 1),
(3, 22, 1, 50000, 0, 0, '', 1312281063641, 1),
(4, 23, 1, 50000, 0, 0, '', 1312281063642, 1),
(5, 28, 1, 10000, 0, 0, '', 1312281063643, 1),
(6, 2, 1, 50000, 0, 0, '', 1312281950608, 1),
(7, 15, 1, 50000, 0, 0, '', 1312281950609, 1),
(8, 16, 1, 50000, 0, 0, '', 1312281950611, 1),
(9, 24, 1, 50000, 0, 0, '', 1312281950612, 1),
(10, 26, 1, 50000, 0, 0, '', 1312281950613, 1),
(11, 11, 1, 1000, 0, 0, '', 1312282114969, 1);

-- --------------------------------------------------------

--
-- Table structure for table `flamma_cdr`
--

DROP TABLE IF EXISTS `flamma_cdr`;
CREATE TABLE IF NOT EXISTS `flamma_cdr` (
  `cdr_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cdr_date` timestamp NULL DEFAULT NULL,
  `dialed_no` varchar(100) DEFAULT NULL,
  `origin_caller` varchar(100) DEFAULT NULL,
  `origin_client_id` int(11) NOT NULL,
  `origin_client_name` varchar(150) DEFAULT NULL,
  `origin_ip` varchar(30) DEFAULT NULL,
  `origin_prefix` varchar(100) DEFAULT NULL,
  `origin_destination` varchar(50) DEFAULT NULL,
  `origin_rate_id` bigint(20) NOT NULL DEFAULT '0',
  `origin_rate_des` varchar(150) DEFAULT NULL,
  `origin_rate` float NOT NULL,
  `origin_bill_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `terminated_no` varchar(100) DEFAULT NULL,
  `term_caller` varchar(100) DEFAULT NULL,
  `term_client_id` int(11) NOT NULL,
  `term_client_name` varchar(150) DEFAULT NULL,
  `term_ip` varchar(30) DEFAULT NULL,
  `term_prefix` varchar(30) DEFAULT NULL,
  `term_destination` varchar(100) DEFAULT NULL,
  `term_rate_id` bigint(20) NOT NULL DEFAULT '0',
  `term_rate_des` varchar(150) DEFAULT NULL,
  `term_rate` float NOT NULL,
  `term_bill_amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `duration` int(11) NOT NULL,
  `connection_time` timestamp NULL DEFAULT NULL,
  `pdd` float NOT NULL,
  PRIMARY KEY (`cdr_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `flamma_cdr`
--


-- --------------------------------------------------------

--
-- Table structure for table `gateway`
--

DROP TABLE IF EXISTS `gateway`;
CREATE TABLE IF NOT EXISTS `gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gateway_name` varchar(100) NOT NULL,
  `gateway_ip` varchar(30) NOT NULL,
  `gateway_status` tinyint(1) NOT NULL DEFAULT '0',
  `gateway_type` tinyint(1) NOT NULL DEFAULT '0',
  `clientId` int(11) NOT NULL,
  `gateway_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `gateway`
--

INSERT INTO `gateway` (`id`, `gateway_name`, `gateway_ip`, `gateway_status`, `gateway_type`, `clientId`, `gateway_delete`) VALUES
(1, 'IP Name', '38.108.92.156', 0, 1, 15, 0),
(2, 'IP Name', '38.108.92.37', 0, 0, 2, 0),
(3, 'IP Name', '38.127.68.198', 0, 0, 1, 0),
(4, 'IP Name', '123.45.23.125', 0, 0, 4, 0),
(5, 'IP Name', '12.58.68.88', 0, 0, 1, 0),
(6, 'IP Name', '123.45.23.225', 0, 0, 7, 1),
(7, 'IP Name', '125.25.120.12', 0, 0, 4, 1),
(8, 'Just Test', '38.108.92.165', 0, 0, 2, 0),
(9, 'Saiful Sir BTCL', '173.233.93.4', 0, 1, 16, 0),
(10, 'Ataur-90', '66.152.183.90', 0, 0, 18, 0),
(11, 'Ataur-86', '198.104.137.86', 0, 0, 18, 0),
(12, 'Tuhin-59', '38.108.92.59', 0, 0, 22, 0),
(13, 'Tuhin-60', '38.108.92.60', 0, 0, 22, 0),
(14, 'Teleworld', '65.200.189.56', 0, 0, 23, 0),
(15, 'Bangla Trac CLI', '202.90.201.2', 0, 1, 24, 0),
(16, 'BD White CLI', '65.111.182.121', 0, 1, 26, 0);

-- --------------------------------------------------------

--
-- Table structure for table `incoming_prefix`
--

DROP TABLE IF EXISTS `incoming_prefix`;
CREATE TABLE IF NOT EXISTS `incoming_prefix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `incoming_prefix` varchar(20) NOT NULL,
  `incoming_to` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `incoming_prefix`
--

INSERT INTO `incoming_prefix` (`id`, `client_id`, `incoming_prefix`, `incoming_to`) VALUES
(1, 1, '', ''),
(2, 2, '', ''),
(3, 3, '', ''),
(4, 10, '777', '11'),
(5, 11, '', ''),
(6, 13, '55', '55'),
(7, 7, '', ''),
(8, 8, '', ''),
(9, 18, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `mvts_rateplan`
--

DROP TABLE IF EXISTS `mvts_rateplan`;
CREATE TABLE IF NOT EXISTS `mvts_rateplan` (
  `rateplan_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `rateplan_name` varchar(100) NOT NULL,
  `rateplan_des` varchar(200) NOT NULL,
  `rateplan_status` tinyint(1) NOT NULL DEFAULT '1',
  `rateplan_delete` tinyint(1) NOT NULL DEFAULT '0',
  `rateplan_create_date` decimal(18,0) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`rateplan_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `mvts_rateplan`
--

INSERT INTO `mvts_rateplan` (`rateplan_id`, `rateplan_name`, `rateplan_des`, `rateplan_status`, `rateplan_delete`, `rateplan_create_date`, `user_id`) VALUES
(7, 'Basic', 'Basic Package', 0, 0, 1311502661014, 1),
(8, 'Silver', 'Silver Package', 0, 0, 1311502661014, 1),
(9, 'Gold', 'Gold Package', 0, 0, 1311502661014, 1),
(10, 'Platinum', 'Platinum Package', 0, 0, 1311502661014, 1),
(11, 'Test', 'Tester', 0, 0, 1311856737447, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mvts_rates`
--

DROP TABLE IF EXISTS `mvts_rates`;
CREATE TABLE IF NOT EXISTS `mvts_rates` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT,
  `rate_id` int(11) NOT NULL DEFAULT '0',
  `rateplan_id` int(11) NOT NULL,
  `rate_destination_code` varchar(20) NOT NULL,
  `rate_destination_name` varchar(30) NOT NULL,
  `rate_per_min` float NOT NULL DEFAULT '0',
  `rate_first_pulse` int(11) NOT NULL DEFAULT '0',
  `rate_next_pulse` int(11) NOT NULL DEFAULT '0',
  `rate_grace_period` int(11) NOT NULL DEFAULT '0',
  `rate_failed_period` int(11) NOT NULL DEFAULT '0',
  `rate_day` int(11) NOT NULL DEFAULT '0',
  `rate_from_hour` int(11) NOT NULL DEFAULT '0',
  `rate_from_min` int(11) NOT NULL DEFAULT '0',
  `rate_to_hour` int(11) NOT NULL DEFAULT '0',
  `rate_to_min` int(11) NOT NULL DEFAULT '0',
  `rate_created_date` decimal(18,0) NOT NULL,
  `rate_status` tinyint(1) NOT NULL DEFAULT '0',
  `rate_delete` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `mvts_rates`
--

INSERT INTO `mvts_rates` (`id`, `rate_id`, `rateplan_id`, `rate_destination_code`, `rate_destination_name`, `rate_per_min`, `rate_first_pulse`, `rate_next_pulse`, `rate_grace_period`, `rate_failed_period`, `rate_day`, `rate_from_hour`, `rate_from_min`, `rate_to_hour`, `rate_to_min`, `rate_created_date`, `rate_status`, `rate_delete`, `user_id`) VALUES
(1, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 0, 4, 4, 10, 59, 1311857032372, 0, 0, 0),
(2, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 1, 6, 6, 7, 30, 1311857033186, 0, 0, 0),
(3, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 2, 9, 6, 23, 59, 1311857033986, 0, 0, 0),
(4, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 3, 7, 0, 14, 12, 1311857034796, 0, 0, 0),
(5, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 4, 14, 13, 12, 7, 1311857035621, 0, 0, 0),
(6, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 5, 10, 13, 7, 10, 1311857036476, 0, 0, 0),
(7, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 6, 12, 5, 13, 16, 1311857037291, 0, 0, 0),
(8, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 0, 4, 4, 10, 59, 1311857038196, 0, 0, 0),
(9, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 1, 6, 6, 7, 30, 1311857038976, 0, 0, 0),
(10, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 2, 9, 6, 23, 59, 1311857039791, 0, 0, 0),
(11, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 3, 7, 0, 14, 12, 1311857040581, 0, 0, 0),
(12, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 4, 14, 13, 12, 7, 1311857041426, 0, 0, 0),
(13, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 5, 10, 13, 7, 10, 1311857042296, 0, 0, 0),
(14, 0, 7, '3000', 'New Test', 1.98, 10, 10, 10, 10, 6, 12, 5, 13, 16, 1311857043126, 0, 0, 0),
(15, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 0, 0, 0, 23, 59, 1311857044330, 0, 0, 0),
(16, 2, 7, '088017', 'GP', 2.5, 0, 0, 0, 0, -1, 0, 0, 23, 59, 1311857045561, 0, 0, 0),
(17, 2, 7, '088017', 'GP', 2.5, 0, 0, 0, 0, 5, 0, 0, 5, 59, 1311857046385, 0, 0, 0),
(18, 3, 7, '0088019', 'Bangla Link', 1.25, 0, 0, 0, 0, -1, 0, 0, 29, 59, 1311857047590, 0, 0, 0),
(19, 3, 7, '0088019', 'Bangla Link', 1.25, 0, 0, 0, 0, -1, 0, 0, 29, 59, 1311857048385, 0, 0, 0),
(20, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 1, 0, 0, 23, 59, 1311857049176, 0, 0, 0),
(21, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 2, 0, 0, 23, 59, 1311857049972, 0, 0, 0),
(22, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 3, 0, 0, 23, 59, 1311857050791, 0, 0, 0),
(23, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 4, 0, 0, 23, 59, 1311857051591, 0, 0, 0),
(24, 1, 7, '88017', 'GP', 1.98, 10, 10, 5, 10, 5, 0, 0, 23, 59, 1311857052361, 0, 0, 0),
(25, 4, 7, '0088', 'BD white', 1.8, 0, 0, 0, 0, -1, 0, 0, 23, 59, 1311857053576, 0, 0, 0),
(26, 4, 7, '0088', 'BD white', 1.8, 0, 0, 0, 0, -1, 0, 0, 23, 59, 1311857054386, 0, 0, 0),
(27, 5, 7, '880', 'BD Common', 1.5, 0, 0, 0, 0, -1, 0, 0, 23, 59, 1311857055591, 0, 0, 0),
(28, 5, 7, '880', 'BD Common', 1.5, 0, 0, 0, 0, -1, 0, 0, 23, 59, 1311857056421, 0, 0, 0),
(29, 6, 7, '88017', 'GP', 2.5, 15, 10, 10, 5, -1, 0, 0, 23, 59, 1312352810505, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `outgoing_prefix`
--

DROP TABLE IF EXISTS `outgoing_prefix`;
CREATE TABLE IF NOT EXISTS `outgoing_prefix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `outgoing_prefix` varchar(20) NOT NULL,
  `outgoing_to` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `outgoing_prefix`
--

INSERT INTO `outgoing_prefix` (`id`, `client_id`, `outgoing_prefix`, `outgoing_to`) VALUES
(1, 1, '', ''),
(2, 2, '', ''),
(3, 3, '', ''),
(6, 11, '000', '000'),
(8, 7, '', ''),
(7, 13, '55', '55'),
(9, 10, '', ''),
(10, 8, '', ''),
(11, 18, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(50) NOT NULL,
  `user_password` varchar(32) NOT NULL,
  `user_full_name` varchar(200) DEFAULT NULL,
  `user_status` tinyint(1) NOT NULL DEFAULT '1',
  `user_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_id`, `user_password`, `user_full_name`, `user_status`, `user_delete`) VALUES
(1, 'admin', '111111', 'Md. Anwar Hossain', 0, 0),
(2, 'hellobd', 'aaaaaa', 'Md. Anwar Hossain', 0, 0),
(3, 'anwar001055', 'aaaaaa', 'Flamma Corporation Ltd.', 0, 0),
(4, 'anwar001', 'aaaaaa', 'Flamma Corporation Ltd.', 0, 0),
(5, 'flamma1', '111111', '111111', 0, 0),
(6, 'tester', '111111', 'Flamma Corporation Ltd.', 0, 0),
(7, 'shihab', 'shihab321', 'Md. Shihab Uddin', 0, 0);
