package mvtsproAAA;

public class ClientDTO {

    private int clientStatus;
    private int clientType;
    private int clientCallLimit;
    private long id;
    private long ratePlanID;
    private double clientCreditLimit;
    private double clientBalance;
    private String clientID;
    private String incomingPrefix;
    private String incomingTo;
    private String outgoingPrefix;
    private String outgoingTo;

    public ClientDTO() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public int getClientStatus() {
        return clientStatus;
    }

    public void setClientStatus(int clientStatus) {
        this.clientStatus = clientStatus;
    }

    public int getClientType() {
        return clientType;
    }

    public void setClientType(int clientType) {
        this.clientType = clientType;
    }

    public int getClientCallLimit() {
        return clientCallLimit;
    }

    public void setClientCallLimit(int clientCallLimit) {
        this.clientCallLimit = clientCallLimit;
    }

    public String getIncomingPrefix() {
        return incomingPrefix;
    }

    public void setIncomingPrefix(String incomingPrefix) {
        this.incomingPrefix = incomingPrefix;
    }

    public String getIncomingTo() {
        return incomingTo;
    }

    public void setIncomingTo(String incomingTo) {
        this.incomingTo = incomingTo;
    }

    public String getOutgoingPrefix() {
        return outgoingPrefix;
    }

    public void setOutgoingPrefix(String outgoingPrefix) {
        this.outgoingPrefix = outgoingPrefix;
    }

    public String getOutgoingTo() {
        return outgoingTo;
    }

    public void setOutgoing_to(String outgoingTo) {
        this.outgoingTo = outgoingTo;
    }

    public long getRatePlanID() {
        return ratePlanID;
    }

    public void setRatePlanID(long ratePlanID) {
        this.ratePlanID = ratePlanID;
    }

    public double getClientBalance() {
        return clientBalance;
    }

    public void setClientBalance(double clientBalance) {
        this.clientBalance = clientBalance;
    }

    public double getClientCreditLimit() {
        return clientCreditLimit;
    }

    public void setClientCreditLimit(double clientCreditLimit) {
        this.clientCreditLimit = clientCreditLimit;
    }
}
