package mvtsproAAA;

public class RateDTO {

    private long id;
    private long rateID;
    private long ratePlanID;
    private String rateDestinationCode;
    private String rateDestinationName;
    private String rateDescription;
    private double ratePerMin;
    private int rateFirstPulse;
    private int rateNextPulse;
    private int rateGracePeriod;
    private int rateFailedPeriod;
    private int rateDay;
    private int rateFromHour;
    private int rateFromMin;
    private int rateToHour;
    private int rateToMin;
    private int rateDeleted;
    private long rateCreatedDate;

    public RateDTO() {
    }
    
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }    

    public int getRateDeleted() {
        return rateDeleted;
    }

    public void setRateDeleted(int rateDeleted) {
        this.rateDeleted = rateDeleted;
    }

    public long getRateCreatedDate() {
        return rateCreatedDate;
    }

    public void setRateCreatedDate(long rateCreatedDate) {
        this.rateCreatedDate = rateCreatedDate;
    }

    public int getRateDay() {
        return rateDay;
    }

    public void setRateDay(int rateDay) {
        this.rateDay = rateDay;
    }

    public String getRateDestinationCode() {
        return rateDestinationCode;
    }

    public void setRateDestinationCode(String rateDestinationCode) {
        this.rateDestinationCode = rateDestinationCode;
    }

    public String getRateDestinationName() {
        return rateDestinationName;
    }

    public void setRateDestinationName(String rateDestinationName) {
        this.rateDestinationName = rateDestinationName;
    }

    public String getRateDescription() {
        return rateDescription;
    }

    public void setRateDescription(String rateDescription) {
        this.rateDescription = rateDescription;
    }
    
    public int getRateFailedPeriod() {
        return rateFailedPeriod;
    }

    public void setRateFailedPeriod(int rateFailedPeriod) {
        this.rateFailedPeriod = rateFailedPeriod;
    }

    public int getRateFirstPulse() {
        return rateFirstPulse;
    }

    public void setRateFirstPulse(int rateFirstPulse) {
        this.rateFirstPulse = rateFirstPulse;
    }

    public int getRateFromHour() {
        return rateFromHour;
    }

    public void setRateFromHour(int rateFromHour) {
        this.rateFromHour = rateFromHour;
    }

    public int getRateFromMin() {
        return rateFromMin;
    }

    public void setRateFromMin(int rateFromMin) {
        this.rateFromMin = rateFromMin;
    }

    public int getRateGracePeriod() {
        return rateGracePeriod;
    }

    public void setRateGracePeriod(int rateGracePeriod) {
        this.rateGracePeriod = rateGracePeriod;
    }

    public long getRateID() {
        return rateID;
    }

    public void setRateID(long rateID) {
        this.rateID = rateID;
    }

    public int getRateNextPulse() {
        return rateNextPulse;
    }

    public void setRateNextPulse(int rateNextPulse) {
        this.rateNextPulse = rateNextPulse;
    }

    public double getRatePerMin() {
        return ratePerMin;
    }

    public void setRatePerMin(double ratePerMin) {
        this.ratePerMin = ratePerMin;
    }

    public int getRateToHour() {
        return rateToHour;
    }

    public void setRateToHour(int rateToHour) {
        this.rateToHour = rateToHour;
    }

    public int getRateToMin() {
        return rateToMin;
    }

    public void setRateToMin(int rateToMin) {
        this.rateToMin = rateToMin;
    }

    public long getRatePlanID() {
        return ratePlanID;
    }

    public void setRatePlanID(long ratePlanID) {
        this.ratePlanID = ratePlanID;
    }
}
