package mvtsproAAA;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class RatePlanLoader {
    static Logger logger = Logger.getLogger(RatePlanLoader.class.getName());
    private static long LOADING_INTERVAL = 3 * 60 * 1000;
    private long loadingTime = 0;
    private HashMap<Long, String> ratePlanNameByID = null;
    static RatePlanLoader ratePlanLoader = null;

    public RatePlanLoader() {
        forceReload();
    }

    public static RatePlanLoader getInstance() {
        if (ratePlanLoader == null) {
            createRatePlanLoader();
        }
        return ratePlanLoader;
    }

    private synchronized static void createRatePlanLoader() {
        if (ratePlanLoader == null) {
            ratePlanLoader = new RatePlanLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            ratePlanNameByID = new HashMap<Long, String>();
            String sql = "select rateplan_id,rateplan_name from mvts_rateplan";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                ratePlanNameByID.put(resultSet.getLong("rateplan_id"), resultSet.getString("rateplan_name"));
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in RateplanLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized String getRatePlanDTOByID(long id) {
        checkForReload();
        return ratePlanNameByID.get(id);
    }   
}
