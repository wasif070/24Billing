package mvtsproAAA;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class BillGenerator implements BillGeneratorInterface{
   
    static int enableBalanceDeduction = 1;
    static long lastCDRID = 0;
    static double precesion = 1000000.0;
    static HashMap<Integer, String> diconnectCauseMap = null;
    static Logger logger = Logger.getLogger(BillGenerator.class.getName());

    public void init(int enableBalanceDeductionVal, long lastCDRIDVal) {
        enableBalanceDeduction = enableBalanceDeductionVal;
        lastCDRID = lastCDRIDVal;
        precesion = BillUtils.getPrecisionLength();
        BillUtils.getDisconnectCauseMappings();
    }

    public long generateBill(String checkingStr) {
        databaseconnector.DBConnection dbConnection = null;
        ddatabaseconnector.DBConnection ddbConnection = null;
        Statement dbStmt = null;
        Statement ddbStmt = null;
        PreparedStatement dbPs = null;
        PreparedStatement ddbPs = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            ddbConnection = ddatabaseconnector.DBConnector.getInstance().makeConnection();
            ddbStmt = ddbConnection.connection.createStatement();
            String sql = "select cdr_id,in_ani,out_ani,cdr_date,DATE_FORMAT(cdr_date,'%w/%H/%i') as date_options,remote_src_sig_address,remote_dst_sig_address,in_dnis,out_dnis,elapsed_time,setup_time,connect_time,pdd,disconnect_time,disconnect_code,conf_id from mvts_cdr"
                    + " where " + checkingStr + " limit 0,100";
            ResultSet resultSet = ddbStmt.executeQuery(sql);
            while (resultSet.next()) {
                try {
                    String dialNo = resultSet.getString("in_dnis");
                    if (dialNo != null) {
                        String termNo = resultSet.getString("out_dnis");
                        String originIP = BillUtils.getOnlyIP(resultSet.getString("remote_src_sig_address"));
                        String termIP = BillUtils.getOnlyIP(resultSet.getString("remote_dst_sig_address"));

                        long originID = 0;
                        long originRateID = 0;
                        double originRate = 0;
                        double originBilledAmount = 0;
                        String originRateDes = "";
                        String originClientName = "";
                        String originPrefix = "";
                        String originDest = "";

                        long termID = 0;
                        long termRateID = 0;
                        double termRate = 0;
                        double termBilledAmount = 0;
                        String termRateDes = "";
                        String termClientName = "";
                        String termPrefix = "";
                        String termDest = "";

                        ClientDTO originClient = BillUtils.getOrgClientDTO(originIP);
                        int duration = 0;
                        if (resultSet.getString("elapsed_time") != null) {
                            try {
                                duration = BillUtils.getSeconds(resultSet.getInt("elapsed_time"));
                            } catch (Exception ex) {
                                logger.debug("ELAPSED TIME EXCEPTION-->" + ex);
                            }
                        }
                        if (originClient != null) {
                            RateDTO rdto = BillUtils.getRateDTO(originClient.getRatePlanID(), resultSet.getString("date_options"), dialNo);
                            if (rdto != null) {
                                originID = originClient.getId();
                                originClientName = originClient.getClientID();
                                originRateID = rdto.getRateID();
                                originRate = rdto.getRatePerMin();                              
                                originDest = rdto.getRateDestinationName();
                                originPrefix = rdto.getRateDestinationCode();
                                originRateDes = rdto.getRateDescription();
                                if (duration > 0) {
                                    logger.debug(rdto.getRatePerMin());
                                    originBilledAmount = BillUtils.calculateBill(duration, rdto);
                                    if (originBilledAmount > 0 && enableBalanceDeduction == 1) {
                                        String sql2 = "update clients set client_balance = client_balance-" + originBilledAmount
                                                + " where id = " + originID;
                                        dbPs = dbConnection.connection.prepareStatement(sql2);
                                        dbPs.executeUpdate();
                                    }

                                    double presentBalance = originClient.getClientBalance() - originBilledAmount;

                                    if ((presentBalance == 0 || (presentBalance + originClient.getClientBalance()) < 0) && enableBalanceDeduction == 1) {
                                        // Our System Gateway Block
                                        String sql2 = "update gateway set gateway_status = 1"
                                                + " where client_id = " + originID;
                                        dbPs = dbConnection.connection.prepareStatement(sql2);
                                        dbPs.executeUpdate();
                                        // Our MvtsSystem Gateway Block
                                        sql2 = "update mvts_gateway set enable=0 where description='Origination' and src_address_list='" + originIP + "'";
                                        ddbStmt = ddbConnection.connection.createStatement();
                                        ddbStmt.execute(sql2);
                                    }
                                }
                            }
                        }

                        ClientDTO termClient = BillUtils.getTerClientDTO(termIP);

                        if (termClient != null) {
                            RateDTO rdto = BillUtils.getRateDTO(termClient.getRatePlanID(), resultSet.getString("date_options"), termNo);
                            if (rdto != null) {
                                termID = termClient.getId();
                                termClientName = termClient.getClientID();
                                termRateID = rdto.getRateID();
                                termRate = rdto.getRatePerMin();
                                termDest = rdto.getRateDestinationName();
                                termPrefix = rdto.getRateDestinationCode();
                                termRateDes = rdto.getRateDescription();
                                if (duration > 0) {
                                    termBilledAmount = BillUtils.calculateBill(duration, rdto);
                                    if (termBilledAmount > 0 && enableBalanceDeduction == 1) {
                                        String sql2 = "update clients set client_balance = client_balance-" + termBilledAmount
                                                + " where id = " + termID;
                                        dbPs = dbConnection.connection.prepareStatement(sql2);
                                        dbPs.executeUpdate();
                                    }

                                    double presentBalance = termClient.getClientBalance() - termBilledAmount;
                                    if ((presentBalance == 0 || (presentBalance + termClient.getClientBalance()) < 0) && enableBalanceDeduction == 1) {
                                        //Our System Termination Gateway Block
                                        String sql2 = "update gateway set gateway_status = 1"
                                                + " where client_id = " + termID;
                                        dbPs = dbConnection.connection.prepareStatement(sql2);
                                        dbPs.executeUpdate();
                                    }
                                }
                            }
                        }
                        sql = "insert into flamma_cdr(cdr_id,cdr_date,dialed_no,origin_caller,origin_client_id,origin_client_name,origin_ip,origin_rate_id,origin_prefix,origin_destination,origin_rate,origin_rate_des,origin_bill_amount,terminated_no,term_caller,term_client_id,term_client_name,term_ip,term_rate_id,term_prefix,term_destination,term_rate,term_rate_des,term_bill_amount,duration,connection_time,pdd,disconnect_desc,conf_id) "
                                + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
                        dbPs = dbConnection.connection.prepareStatement(sql);
                        lastCDRID = resultSet.getLong("cdr_id");
                        dbPs.setLong(1, lastCDRID);
                        dbPs.setString(2, resultSet.getString("cdr_date"));
                        dbPs.setString(3, dialNo);
                        dbPs.setString(4, resultSet.getString("in_ani"));
                        dbPs.setLong(5, originID);
                        dbPs.setString(6, originClientName);
                        dbPs.setString(7, originIP);
                        dbPs.setLong(8, originRateID);
                        dbPs.setString(9, originPrefix);
                        dbPs.setString(10, originDest);
                        dbPs.setDouble(11, originRate);
                        dbPs.setString(12, originRateDes);
                        dbPs.setDouble(13, originBilledAmount);
                        dbPs.setString(14, termNo);
                        dbPs.setString(15, resultSet.getString("out_ani"));
                        dbPs.setLong(16, termID);
                        dbPs.setString(17, termClientName);
                        dbPs.setString(18, termIP);
                        dbPs.setLong(19, termRateID);
                        dbPs.setString(20, termPrefix);
                        dbPs.setString(21, termDest);
                        dbPs.setDouble(22, termRate);
                        dbPs.setString(23, termRateDes);
                        dbPs.setDouble(24, termBilledAmount);

                        dbPs.setInt(25, resultSet.getInt("elapsed_time"));

                        if (resultSet.getString("connect_time") != null) {
                            dbPs.setString(26, resultSet.getString("connect_time"));
                        } else {
                            dbPs.setString(26, resultSet.getString("disconnect_time"));
                        }
                        dbPs.setDouble(27, resultSet.getDouble("pdd") / 1000);
                        String disconnect_cause = diconnectCauseMap.get(resultSet.getInt("disconnect_code"));

                        if (disconnect_cause != null) {
                            dbPs.setString(28, disconnect_cause);
                        } else {
                            dbPs.setString(28, "");
                        }
                        dbPs.setString(29, resultSet.getString("conf_id"));
                        dbPs.execute();
                    } else {
                        logger.debug("Dial No is NULL");
                    }
                } catch (Exception ex) {
                    logger.debug("Exception during a single row migration CDR ID:" + lastCDRID + " ", ex);
                }
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in generateBill:", e);
        } finally {
            try {
                if (dbPs != null) {
                    dbPs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ddbPs != null) {
                    ddbPs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbStmt != null) {
                    dbStmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ddbStmt != null) {
                    dbStmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (ddbConnection.connection != null) {
                    ddatabaseconnector.DBConnector.getInstance().freeConnection(ddbConnection);
                }
            } catch (Exception e) {
            }
        }
        if (lastCDRID > 0) {
            try {
                PrintWriter writeIDFile = new PrintWriter(new FileWriter("cdrID.txt", false), true);
                writeIDFile.println("CDR_ID=" + lastCDRID);
                writeIDFile.close();
            } catch (Exception ex) {
                logger.debug("Exception during CDR ID writing:" + ex);
            }
        }
        return lastCDRID;
    }

}
