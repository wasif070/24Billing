package mvtsproAAA.radius;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.Calendar;
import java.util.GregorianCalendar;
import mvtsproAAA.ClientDTO;
import mvtsproAAA.RateDTO;
import org.apache.log4j.Logger;

public class AuthResponseGenerator implements AuthResponseGeneratorInterface {

    static Logger logger = Logger.getLogger(AuthResponseGenerator.class.getName());

    public byte[] generateAuthResponse(byte[] data, String sharedSecret) {
        ByteArrayOutputStream byteArrayOutputStream = null;

        try {
            int index = 0;
            byte authCode = data[index++];
            byte authIdentifier = data[index++];
            int authPacketLength = (data[index++] & 0xff) << 8 | data[index++] & 0xff;
            if (authPacketLength < 20) {
                return null;
            }
            byte requestAuthenticator[] = new byte[16];
            for (int i = 0; i < requestAuthenticator.length; i++) {
                requestAuthenticator[i] = data[index++];
            }

            int xpgk_route_retries = 0;
            String xpgk_request_type = "";
            String xpgk_src_number_in = "";
            String xpgk_src_number_out = "";
            String xpgk_dst_number_in = "";
            String xpgk_dst_number_out = "";
            String xpgk_record_id = "";
            String xpgk_local_src_signaling_address = "";
            String h323_gw_address = "";
            String h323_incoming_call_id = "";
            String h323_incoming_conf_id = "";
            String h323_call_id = "";
            String h323_remote_id = "";
            String h323_remote_address = "";
            String h323_conf_id = "";
            String h323_gw_id = "";
            String h323_original_setup_time = "";
            String h323_call_origin = "";
            String h323_call_type = "";
            String h323_original_connect_time = "";
            String h323_original_disconnect_time = "";
            String h323_disconnect_cause = "";
            String h323_voice_quality = "";

            while (index < authPacketLength) {
                byte vendor[] = new byte[4];
                int attType = data[index++] & 0xff;
                int attLength = data[index++] & 0xff;

                if (attType == 26) {
                    for (int i = 0; i < vendor.length; i++) {
                        vendor[i] = data[index++];
                    }

                    int vendorAttType = data[index++] & 0xff;
                    int vendorAttLength = data[index++] & 0xff;
                    byte attValue[] = new byte[vendorAttLength - 2];
                    for (int i = 0; i < attValue.length; i++) {
                        attValue[i] = data[index++];
                    }
                    switch (vendorAttType) {
                        case 1: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("xpgk-src-number-in")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_src_number_in = attStr;
                            } else if (attStr.startsWith("xpgk-src-number-out")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_src_number_out = attStr;
                            } else if (attStr.startsWith("xpgk-dst-number-in=")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_dst_number_in = attStr;
                            } else if (attStr.startsWith("xpgk-dst-number-out=")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_dst_number_out = attStr;
                            } else if (attStr.startsWith("xpgk-request-type")) {
                                xpgk_request_type = attStr.substring(attStr.indexOf('=') + 1);
                            } else if (attStr.startsWith("xpgk-record-id")) {
                                xpgk_record_id = attStr.substring(attStr.indexOf('=') + 1);

                            } else if (attStr.startsWith("xpgk-route-retries")) {
                                xpgk_route_retries = Integer.parseInt(attStr.substring(attStr.indexOf('=') + 1).trim());
                            } else if (attStr.startsWith("xpgk-local-src-signaling-address")) {
                                xpgk_local_src_signaling_address = attStr.substring(attStr.indexOf('=') + 1);
                            } else if (attStr.startsWith("h323-incoming-conf-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_incoming_conf_id = attStr;
                            } else if (attStr.startsWith("h323-gw-address")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_gw_address = attStr;
                            } else if (attStr.startsWith("h323-incoming-call-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_incoming_call_id = attStr;
                            } else if (attStr.startsWith("h323-call-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_call_id = attStr;
                            } else if (attStr.startsWith("h323-remote-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_remote_id = attStr;
                            }
                            break;
                        }

                        case 23: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-remote-address")) {
                                h323_remote_address = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 24: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-conf-id")) {
                                h323_conf_id = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 25: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-setup-time")) {
                                h323_original_setup_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 26: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-call-origin")) {
                                h323_call_origin = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 27: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-call-type")) {
                                h323_call_type = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 28: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-connect-time")) {
                                h323_original_connect_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 29: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-disconnect-time")) {
                                h323_original_disconnect_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                        }

                        case 30: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-disconnect-cause")) {
                                h323_disconnect_cause = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 31: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-voice-quality")) {
                                h323_voice_quality = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 33: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-gw-id")) {
                                h323_gw_id = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        default:
                            break;
                    }

                } else {
                    byte attValue[] = new byte[attLength - 2];
                    for (int i = 0; i < attValue.length; i++) {
                        attValue[i] = data[index++];
                    }

                    switch (attType) {
                        case 1:
                            String User_Name = new String(attValue);
                            break;

                        case 2:
                            String User_Password = new String(attValue);
                            break;

                        case 4:
                            String NAS_IP_Address = RadiusUtils.convertToIPString(attValue);
                            break;

                        case 30:
                            String Called_Station_Id_In = new String(attValue);
                            break;

                        case 31:
                            String Calling_Station_Id = new String(attValue);
                            break;

                        case 40:
                            int Acct_Status_Type = (int) RadiusUtils.convertToLong(attValue);
                            break;

                        case 41:
                            int Acct_Delay_Time = (int) RadiusUtils.convertToLong(attValue);
                            break;

                        case 42:
                            int Acct_Input_Octets = (int) RadiusUtils.convertToLong(attValue);
                            break;

                        case 43:
                            long Acct_Output_Octets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 44:
                            String Acct_Session_Id = new String(attValue);
                            break;

                        case 46:
                            long Acct_Session_Time = RadiusUtils.convertToLong(attValue);
                            break;

                        case 47:
                            long Acct_Input_Packets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 48:
                            long Acct_Output_Packets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 61:
                            String NAS_Port_Type = new String(attValue);
                            break;

                        default:
                            break;
                    }
                }
            }

            byte responseCode[] = new byte[1];
            responseCode[0] = 3;
            byte responseAttributes[] = new byte[0];

            if (xpgk_request_type != null && xpgk_request_type.equalsIgnoreCase("user")) {
                int authenticationResult = 1;
                if (authenticationResult == 1) {
                    responseCode[0] = 2;
                    logger.debug("User authenticated");
                    responseAttributes = RadiusUtils.setAttribute(7, 103, responseAttributes, "h323-return-code=0");
                } else {
                    logger.debug("User denied");
                    responseAttributes = RadiusUtils.setAttribute(7, 103, responseAttributes, "h323-return-code=7");
                }
            } else if (xpgk_request_type != null && xpgk_request_type.equalsIgnoreCase("number")) {
                logger.debug("Number Found...");
                int authorizationResult = 1;
                long allocatedDuration = 0;
                ClientDTO originClient = RadiusUtils.getOrgClientDTO(h323_gw_address);
                ClientDTO termClient = RadiusUtils.getTerClientDTO(h323_remote_address);
                Calendar cal = new GregorianCalendar();
                String date_options = cal.get(Calendar.DAY_OF_WEEK) + "/" + cal.get(Calendar.HOUR) + "/" + cal.get(Calendar.MINUTE);
                logger.debug("date_options--->" + date_options);
                RateDTO orgRateDTO = null;
                if (originClient != null) {
                    orgRateDTO = RadiusUtils.getRateDTO(originClient.getRatePlanID(), date_options, xpgk_dst_number_in);
                    logger.debug("Client Balance-->" + originClient.getClientBalance());
                }

                if (originClient == null) {
                    authorizationResult = -1;
                } else if (orgRateDTO == null) {
                    authorizationResult = -2;
                } else if (originClient.getClientBalance() <= 0) {
                    authorizationResult = -3;
                } else if (originClient.getClientStatus() > 0) {
                    authorizationResult = -4;
                } else if (orgRateDTO.getRateFirstPulse() * (orgRateDTO.getRatePerMin() / 60) > originClient.getClientBalance()) {
                    authorizationResult = -5;
                } else {
                    allocatedDuration = RadiusUtils.checkCapacityToCall(originClient, orgRateDTO);
                    if (allocatedDuration == 0) {
                        authorizationResult = -6;
                    }
                }

                switch (authorizationResult) {
                    case 1:
                        if (h323_conf_id != null && h323_conf_id.trim().length() > 0) {
                            responseCode[0] = 2;
                            responseAttributes = RadiusUtils.setAttribute(9, 102, responseAttributes, "h323-credit-time=" + allocatedDuration);
                            responseAttributes = RadiusUtils.setAttribute(9, 103, responseAttributes, "h323-return-code=0");
                            h323_conf_id = h323_conf_id.toLowerCase().replace(" ", "");
                            if (termClient != null) {
                                RateDTO terRateDTO = RadiusUtils.getRateDTO(termClient.getRatePlanID(), date_options, xpgk_dst_number_out);
                                if (terRateDTO != null) {
                                    RadiusUtils.addCurrentCall(h323_conf_id, xpgk_dst_number_in, originClient.getId(), h323_gw_address, originClient.getRatePlanID(), orgRateDTO.getRateDestinationCode(), orgRateDTO.getRatePerMin(), xpgk_dst_number_out, termClient.getId(), h323_remote_address, termClient.getRatePlanID(), terRateDTO.getRateDestinationCode(), terRateDTO.getRatePerMin(), h323_original_setup_time, h323_original_connect_time);
                                } else {
                                    RadiusUtils.addCurrentCall(h323_conf_id, xpgk_dst_number_in, originClient.getId(), h323_gw_address, originClient.getRatePlanID(), orgRateDTO.getRateDestinationCode(), orgRateDTO.getRatePerMin(), xpgk_dst_number_out, termClient.getId(), h323_remote_address, termClient.getRatePlanID(), null, 0, h323_original_setup_time, h323_original_connect_time);
                                }
                            } else {
                                RadiusUtils.addCurrentCall(h323_conf_id, xpgk_dst_number_in, originClient.getId(), h323_gw_address, originClient.getRatePlanID(), orgRateDTO.getRateDestinationCode(), orgRateDTO.getRatePerMin(), xpgk_dst_number_out, 0, h323_remote_address, 0, null, 0, h323_original_setup_time, h323_original_connect_time);
                            }
                        } else {
                            logger.debug("Autherization Failed");
                            responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=99");
                        }
                        break;

                    case -1:
                        logger.debug("Originating client not found");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=1");
                        break;

                    case -2:
                        logger.debug("Invalid number is dialed. Rate not assigned");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=21");
                        break;

                    case -3:
                        logger.debug("Insufficient balance");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=12");
                        break;

                    case -4:
                        logger.debug("Account is Blocked");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=7");
                        break;


                    case -5:
                        logger.debug("Insufficient balance");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=12");
                        break;

                    case -6:
                        logger.debug("Call Limit is over");
                        responseAttributes = RadiusUtils.setAttribute(103, responseAttributes, "h323-return-code=3");
                        break;
                }
            }
            int responseLength = 20 + responseAttributes.length;
            byte responseAuthenticator[] = RadiusUtils.getResponseAuthenticator(responseCode[0], authIdentifier, responseLength, requestAuthenticator, responseAttributes, sharedSecret.getBytes());

            byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeByte(responseCode[0]);
            dataOutputStream.writeByte(authIdentifier);
            dataOutputStream.writeShort(responseLength);
            dataOutputStream.write(responseAuthenticator, 0, 16);
            if (responseAttributes != null) {
                dataOutputStream.write(responseAttributes, 0, responseAttributes.length);

            }
            dataOutputStream.close();
            byteArrayOutputStream.close();
        } catch (Exception ex) {
            logger.debug("Exception in generateAuthResponse: " + ex);
            return null;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
