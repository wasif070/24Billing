package mvtsproAAA.radius;

public interface AcctResponseGeneratorInterface {

    public byte[] generateAcctResponse(byte[] data, String sharedSecret);
}
