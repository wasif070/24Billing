package mvtsproAAA.radius;

import databaseconnector.DBConnection;
import java.security.MessageDigest;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import mvtsproAAA.ClientDTO;
import mvtsproAAA.RateDTO;
import mvtsproAAA.RatePlanLoader;
import org.apache.log4j.Logger;

public class RadiusUtils {

    static Logger logger = Logger.getLogger(RadiusUtils.class.getName());

    public static void changeCurrentCallToConnected(String h323_conf_id, String h323_original_setup_time, String h323_original_connect_time) {
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update current_call set setup_time=?, connect_time=?, status=?, inserted_time=UNIX_TIMESTAMP() where conf_id='" + h323_conf_id + "'";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, h323_original_setup_time);
            ps.setString(2, h323_original_connect_time);
            ps.setInt(3, 1);
            ps.execute();
        } catch (Exception e) {
            logger.debug("Exception in changeCurrentCallToConnected: " + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

    }

    public static void removeFromCurrentCall(String h323_conf_id) {

        DBConnection dbConnection = null;
        Statement stmt = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = dbConnection.connection.createStatement();
            String sql = "delete from current_call where conf_id = '" + h323_conf_id + "'";
            stmt.execute(sql);
        } catch (Exception ex) {
            logger.fatal("Exception in changeCurrentCallToDisconnected: ", ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public static void addCurrentCall(String h323_conf_id, String xpgk_dst_number_in, long org_client_id, String h323_gw_address, long org_rateplan_id, String org_prefix, double org_rate_per_min, String xpgk_dst_number_out, long term_client_id, String h323_remote_address, long term_rateplan_id, String term_prifix, double term_rate_per_min, String h323_original_setup_time, String h323_original_connect_time) {
        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "insert into current_call"
                    + " (conf_id,"
                    + "dial_no_in,"
                    + "org_client_id,"
                    + "org_gateway_ip,"
                    + "org_rateplan_id,"
                    + "org_prefix,"
                    + "org_rate_per_min,"
                    + "dial_no_out,"
                    + "term_client_id,"
                    + "term_gateway_ip,"
                    + "term_rateplan_id,"
                    + "term_prefix,"
                    + "term_rate_per_min,"
                    + "setup_time,"
                    + "connect_time,"
                    + "status,"
                    + "inserted_time)"
                    + " values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,UNIX_TIMESTAMP())";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, h323_conf_id);
            ps.setString(2, xpgk_dst_number_in);
            ps.setLong(3, org_client_id);
            ps.setString(4, h323_gw_address);
            ps.setLong(5, org_rateplan_id);
            ps.setString(6, org_prefix);
            ps.setDouble(7, org_rate_per_min);
            ps.setString(8, xpgk_dst_number_out);
            ps.setLong(9, term_client_id);
            ps.setString(10, h323_remote_address);
            ps.setLong(11, term_rateplan_id);
            ps.setString(12, term_prifix);
            ps.setDouble(13, term_rate_per_min);
            ps.setString(14, h323_original_setup_time);
            ps.setString(15, h323_original_connect_time);
            ps.setInt(16, 0);
            ps.executeUpdate();
        } catch (Exception e) {
            logger.fatal("Exception in CurrentCall:", e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public static long checkCapacityToCall(ClientDTO originClient, RateDTO orgRateDTO) {
        DBConnection dbConnection = null;
        Statement statement = null;
        int totalRunningCall = 0;
        long duration = 0;

        int callCapacity = originClient.getClientCallLimit();
        double balance = originClient.getClientBalance();
        double ratePerMin = orgRateDTO.getRatePerMin();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select count(*) as totalRunningCall from current_call where org_client_id=" + originClient.getId()
                    + " and inserted_time>=(UNIX_TIMESTAMP()-3600)";
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                totalRunningCall = resultSet.getInt("totalRunningCall");
            }
            resultSet.close();
        } catch (Exception ex) {
            logger.fatal("Exception in checkCapacityToCall: ", ex);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        if (callCapacity != -1 && totalRunningCall >= callCapacity) {
            return duration;
        }

        /*
        Fisrt deduct the First Pulse from actual duration cause first pulse already authorized.
        Then deduct the overhead of Next pulse and finaly add the deducted duration of First pulse
         */
        long actualDuration = Math.round(60 * (balance / ratePerMin)) - orgRateDTO.getRateFirstPulse();
        long overheadOfNextPulse = actualDuration % orgRateDTO.getRateNextPulse();
        actualDuration = actualDuration - overheadOfNextPulse + orgRateDTO.getRateFirstPulse();

        duration = actualDuration - (totalRunningCall * 3600);
        if (duration < 0) {
            duration = 0;
        } else if (duration > 3600) {
            duration = 3600;
        }
        return duration;
    }

    public static ClientDTO getOrgClientDTO(String ip) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            if (ip != null) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String sql = "select client_id from gateway where gateway_ip='" + ip
                        + "' and gateway_delete=0 and gateway_status=0 and (gateway_type=0 or gateway_type=2)";
                logger.debug(sql);
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                    cdto = getClientDTOByID(resultSet.getLong("client_id"));
                }
                resultSet.close();
            }
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTO:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static ClientDTO getTerClientDTO(String ip) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            if (ip != null) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String sql = "select client_id from gateway where gateway_ip='" + ip
                        + "' and gateway_delete=0 and gateway_status=0 and (gateway_type=1 or gateway_type=2)";
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                    cdto = getClientDTOByID(resultSet.getLong("client_id"));
                }
                resultSet.close();
            }
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTO:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static ClientDTO getClientDTOByID(long id) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from clients where id=" + id + " and client_delete=0 and client_status=0";
            logger.debug(sql);
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                cdto = new ClientDTO();
                cdto.setId(resultSet.getLong("id"));
                cdto.setClientID(resultSet.getString("client_id"));
                cdto.setClientType(resultSet.getInt("client_type"));
                cdto.setClientStatus(resultSet.getInt("client_status"));
                cdto.setRatePlanID(resultSet.getLong("rateplan_id"));
                cdto.setClientCreditLimit(resultSet.getDouble("client_credit_limit"));
                cdto.setClientBalance(resultSet.getDouble("client_balance"));
                cdto.setClientCallLimit(resultSet.getInt("client_call_limit"));
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTOByID:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static RateDTO getRateDTO(long ratePlanID, String dateStr, String dialNo) {
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String[] values = dateStr.split("/");
            int day = Integer.parseInt(values[0]);
            int hour = Integer.parseInt(values[1]);
            int min = Integer.parseInt(values[2]);
            int timeOfDay = (hour * 60) + min;
            int prevLength = 0;
            long prevTime = 0;
            dto = new RateDTO();
            String sql = "select * from mvts_rates where rateplan_id=" + ratePlanID
                    + " and INSTR('" + dialNo + "', rate_destination_code)=1 and rate_delete=0 and rate_status=0";
            logger.debug("dial plan sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);

            while (resultSet.next()) {
                int rateDay = resultSet.getInt("rate_day");
                int rateFromHour = resultSet.getInt("rate_from_hour");
                int rateToHour = resultSet.getInt("rate_to_hour");
                int rateFromMin = resultSet.getInt("rate_from_min");
                int rateToMin = resultSet.getInt("rate_to_min");
                long rateCreationTime = resultSet.getLong("rate_created_date");
                String rateDestinationCode = resultSet.getString("rate_destination_code");

                if ((rateDay == -1 || (rateDay == day
                        && (timeOfDay >= (rateFromHour * 60) + rateFromMin)
                        && (timeOfDay <= (rateToHour * 60) + rateToMin)))
                        && prevLength <= rateDestinationCode.length()
                        && prevTime < rateCreationTime) {
                    dto.setRatePlanID(ratePlanID);
                    dto.setId(resultSet.getLong("id"));
                    dto.setRateID(resultSet.getLong("rate_id"));
                    dto.setRateDestinationCode(rateDestinationCode);
                    dto.setRateDestinationName(resultSet.getString("rate_destination_name"));
                    dto.setRatePerMin(resultSet.getDouble("rate_per_min"));
                    dto.setRateFirstPulse(resultSet.getInt("rate_first_pulse"));
                    dto.setRateNextPulse(resultSet.getInt("rate_next_pulse"));
                    dto.setRateGracePeriod(resultSet.getInt("rate_grace_period"));
                    dto.setRateFailedPeriod(resultSet.getInt("rate_failed_period"));
                    dto.setRateDay(rateDay);
                    dto.setRateFromHour(rateFromHour);
                    dto.setRateFromMin(rateFromMin);
                    dto.setRateToHour(rateToHour);
                    dto.setRateToMin(rateToMin);
                    dto.setRateCreatedDate(rateCreationTime);
                    dto.setRateDescription(RatePlanLoader.getInstance().getRatePlanDTOByID(ratePlanID));
                    prevLength = dto.getRateDestinationCode().length();
                    prevTime = dto.getRateCreatedDate();
                }
            }
            resultSet.close();
            if (prevTime == 0) {
                dto = null;
            }
        } catch (Exception e) {
            logger.fatal("Exception in RateLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public static String getByteString(byte data[]) {
        String byteString = "";
        for (int i = 0; i < data.length; i++) {
            byteString = byteString + String.format("%02X", (data[i] & 0xff)) + " ";
        }
        return byteString;
    }

    public static String convertToIPString(byte ip[]) {
        String ipString = null;
        for (int i = 0; i < ip.length; i++) {
            int temp = ip[i] & 0xff;
            if (ipString == null) {
                ipString = Integer.toString(temp);
            } else {
                ipString = ipString + '.' + temp;
            }
        }
        return ipString;
    }

    public static long convertToLong(byte b[]) {
        long value = 0;
        int temp = 0;
        for (int i = 0; i < b.length; i++) {
            temp = b[i] & 0xff;
            value |= temp;
            if (i < b.length - 1) {
                value <<= 8;
            }
        }

        return value;
    }

    public static byte[] setAttribute(int attType, byte attBuffer[], String newAtt) {
        int length;
        byte[] newAttByte = newAtt.getBytes();
        byte[] result = new byte[attBuffer.length + newAttByte.length + 8];
        int index = 0;
        for (int i = 0; i < attBuffer.length; i++) {
            result[index++] = attBuffer[i];
        }
        result[index++] = 26;
        length = index;
        index++;
        result[index++] = 0;
        result[index++] = 0;
        result[index++] = 25;
        result[index++] = -38;
        result[index++] = (byte) attType;
        int vlength = index;
        index++;
        for (int i = 0; i < newAttByte.length; i++) {
            result[index++] = newAttByte[i];
        }
        result[length] = (byte) (newAttByte.length + 8);
        result[vlength] = (byte) (newAttByte.length + 2);
        return result;
    }

    public static byte[] setAttribute(int vendorID, int attType, byte attBuffer[], String newAtt) {
        byte[] newAttByte = newAtt.getBytes();
        byte[] result = new byte[attBuffer.length + newAttByte.length + 8];
        int index = 0;
        for (int i = 0; i < attBuffer.length; i++) {
            result[index++] = attBuffer[i];
        }
        result[index++] = 26;
        int length = index;
        index++;
        result[index++] = (byte) (vendorID >> 24 & 0xff);
        result[index++] = (byte) (vendorID >> 16 & 0xff);
        result[index++] = (byte) (vendorID >> 8 & 0xff);
        result[index++] = (byte) (vendorID & 0xff);
        result[index++] = (byte) attType;
        int vlength = index;
        index++;
        for (int i = 0; i < newAttByte.length; i++) {
            result[index++] = newAttByte[i];
        }
        result[length] = (byte) (newAttByte.length + 8);
        result[vlength] = (byte) (newAttByte.length + 2);
        return result;
    }

    public static byte[] getResponseAuthenticator(byte responseCode, byte identifier, int length, byte authenticator[], byte responseAttributes[], byte sharedSecret[]) {
        MessageDigest md5MessageDigest = null;
        try {
            md5MessageDigest = MessageDigest.getInstance("MD5");
            md5MessageDigest.reset();
            md5MessageDigest.update(responseCode);
            md5MessageDigest.update(identifier);
            md5MessageDigest.update((byte) (length >> 8));
            md5MessageDigest.update((byte) (length & 0xff));
            md5MessageDigest.update(authenticator, 0, authenticator.length);
            md5MessageDigest.update(responseAttributes, 0, responseAttributes.length);
            md5MessageDigest.update(sharedSecret);
        } catch (Exception e) {
        }
        return md5MessageDigest.digest();
    }
}
