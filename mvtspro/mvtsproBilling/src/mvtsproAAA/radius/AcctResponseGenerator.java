package mvtsproAAA.radius;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import org.apache.log4j.Logger;

public class AcctResponseGenerator implements AcctResponseGeneratorInterface{

    static Logger logger = Logger.getLogger(AcctResponseGenerator.class.getName());

    public byte[] generateAcctResponse(byte[] data, String sharedSecret) {
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            int index = 0;
            byte accCode = data[index++];
            byte accIdentifier = data[index++];
            int accPacketLength = (data[index++] & 0xff) << 8 | data[index++] & 0xff;
            if (accPacketLength < 20) {
                return null;
            }
            byte requestAuthenticator[] = new byte[16];
            for (int i = 0; i < requestAuthenticator.length; i++) {
                requestAuthenticator[i] = data[index++];
            }

            int xpgk_route_retries = 0;
            long accStatusType = 0;
            String xpgk_request_type = "";
            String xpgk_src_number_in = "";
            String xpgk_src_number_out = "";
            String xpgk_dst_number_in = "";
            String xpgk_dst_number_out = "";
            String xpgk_record_id = "";
            String xpgk_local_src_signaling_address = "";
            String h323_gw_address = "";
            String h323_incoming_call_id = "";
            String h323_incoming_conf_id = "";
            String h323_call_id = "";
            String h323_remote_id = "";
            String h323_remote_address = "";
            String h323_conf_id = "";
            String h323_gw_id = "";
            String h323_original_setup_time = "";
            String h323_call_origin = "";
            String h323_call_type = "";
            String h323_original_connect_time = "";
            String h323_original_disconnect_time = "";
            String h323_disconnect_cause = "";
            String h323_voice_quality = "";

            while (index < accPacketLength) {
                byte vendor[] = new byte[4];
                int attType = data[index++] & 0xff;
                int attLength = data[index++] & 0xff;

                if (attType == 26) {
                    for (int i = 0; i < vendor.length; i++) {
                        vendor[i] = data[index++];
                    }

                    int vendorAttType = data[index++] & 0xff;
                    int vendorAttLength = data[index++] & 0xff;
                    byte attValue[] = new byte[vendorAttLength - 2];
                    for (int i = 0; i < attValue.length; i++) {
                        attValue[i] = data[index++];
                    }
                    switch (vendorAttType) {
                        case 1: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("xpgk-src-number-in")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_src_number_in = attStr;
                            } else if (attStr.startsWith("xpgk-src-number-out")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_src_number_out = attStr;
                            } else if (attStr.startsWith("xpgk-dst-number-in=")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_dst_number_in = attStr;
                            } else if (attStr.startsWith("xpgk-dst-number-out=")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                xpgk_dst_number_out = attStr;
                            } else if (attStr.startsWith("xpgk-request-type")) {
                                xpgk_request_type = attStr.substring(attStr.indexOf('=') + 1);
                            } else if (attStr.startsWith("xpgk-record-id")) {
                                xpgk_record_id = attStr.substring(attStr.indexOf('=') + 1);

                            } else if (attStr.startsWith("xpgk-route-retries")) {
                                xpgk_route_retries = Integer.parseInt(attStr.substring(attStr.indexOf('=') + 1).trim());
                            } else if (attStr.startsWith("xpgk-local-src-signaling-address")) {
                                xpgk_local_src_signaling_address = attStr.substring(attStr.indexOf('=') + 1);
                            } else if (attStr.startsWith("h323-incoming-conf-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_incoming_conf_id = attStr;
                            } else if (attStr.startsWith("h323-gw-address")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_gw_address = attStr;

                            } else if (attStr.startsWith("h323-incoming-call-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_incoming_call_id = attStr;
                            } else if (attStr.startsWith("h323-call-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_call_id = attStr;
                            } else if (attStr.startsWith("h323-remote-id")) {
                                attStr = attStr.substring(attStr.indexOf('=') + 1);
                                h323_remote_id = attStr;

                            }
                            break;
                        }

                        case 23: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-remote-address")) {
                                h323_remote_address = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 24: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-conf-id")) {
                                h323_conf_id = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 25: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-setup-time")) {
                                h323_original_setup_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 26: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-call-origin")) {
                                h323_call_origin = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 27: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-call-type")) {
                                h323_call_type = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 28: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-connect-time")) {
                                h323_original_connect_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        case 29: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-disconnect-time")) {
                                h323_original_disconnect_time = attStr.substring(attStr.indexOf('=') + 1);

                            }
                        }

                        case 30: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-disconnect-cause")) {
                                h323_disconnect_cause = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 31: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-voice-quality")) {
                                h323_voice_quality = attStr.substring(attStr.indexOf('=') + 1);
                            }
                            break;
                        }

                        case 33: {
                            String attStr = new String(attValue);
                            if (attStr.startsWith("h323-gw-id")) {
                                h323_gw_id = attStr.substring(attStr.indexOf('=') + 1);

                            }
                            break;
                        }

                        default:
                            break;
                    }

                } else {
                    byte attValue[] = new byte[attLength - 2];
                    for (int i = 0; i < attValue.length; i++) {
                        attValue[i] = data[index++];
                    }

                    switch (attType) {
                        case 1:
                            String User_Name = new String(attValue);
                            break;

                        case 2:
                            String User_Password = new String(attValue);
                            break;

                        case 4:
                            String NAS_IP_Address = RadiusUtils.convertToIPString(attValue);
                            break;

                        case 30:
                            String Called_Station_Id_In = new String(attValue);
                            break;

                        case 31:
                            String Calling_Station_Id = new String(attValue);
                            break;

                        case 40:
                            int Acct_Status_Type = (int) RadiusUtils.convertToLong(attValue);
                            accStatusType = Acct_Status_Type;
                            break;

                        case 41:
                            int Acct_Delay_Time = (int) RadiusUtils.convertToLong(attValue);
                            break;

                        case 42:
                            int Acct_Input_Octets = (int) RadiusUtils.convertToLong(attValue);
                            break;

                        case 43:
                            long Acct_Output_Octets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 44:
                            String Acct_Session_Id = new String(attValue);
                            break;

                        case 46:
                            long Acct_Session_Time = RadiusUtils.convertToLong(attValue);
                            break;

                        case 47:
                            long Acct_Input_Packets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 48:
                            long Acct_Output_Packets = RadiusUtils.convertToLong(attValue);
                            break;

                        case 61:
                            String NAS_Port_Type = new String(attValue);
                            break;

                        default:
                            break;
                    }
                }
            }

            byte responseCode[] = new byte[1];
            responseCode[0] = 5;
            byte responseAttributes[] = new byte[0];
            int responseLength = 20;
            byte responseAuthenticator[] = RadiusUtils.getResponseAuthenticator(responseCode[0], accIdentifier, 20, requestAuthenticator, responseAttributes, sharedSecret.getBytes());

            byteArrayOutputStream = new ByteArrayOutputStream();
            DataOutputStream dataOutputStream = new DataOutputStream(byteArrayOutputStream);
            dataOutputStream.writeByte(responseCode[0]);
            dataOutputStream.writeByte(accIdentifier);
            dataOutputStream.writeShort(responseLength);
            dataOutputStream.write(responseAuthenticator, 0, 16);
            if (responseAttributes != null) {
                dataOutputStream.write(responseAttributes, 0, responseAttributes.length);
            }
            dataOutputStream.close();
            byteArrayOutputStream.close();
            if (h323_conf_id != null && h323_conf_id.trim().length() > 0) {
                h323_conf_id = h323_conf_id.toLowerCase().replace(" ", "");
                logger.debug("acct packet:" + accStatusType);
                if (accStatusType == 1) {
                    RadiusUtils.changeCurrentCallToConnected(h323_conf_id, h323_original_setup_time, h323_original_connect_time);
                } else if (accStatusType == 2) {
                    RadiusUtils.removeFromCurrentCall(h323_conf_id);
                } else {
                    logger.debug("Unknown Accounting Status Type");
                }
            }
        } catch (Exception ex) {
            logger.debug("Exception in generateAcctResponse: " + ex);
            return null;
        }
        return byteArrayOutputStream.toByteArray();
    }
}
