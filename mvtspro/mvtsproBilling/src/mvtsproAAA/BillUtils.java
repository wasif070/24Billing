package mvtsproAAA;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class BillUtils {
    
    static Logger logger = Logger.getLogger(BillGenerator.class.getName());

    public static double calculateBill(int duration, RateDTO rdto) {
        double bill = 0;
        int next_sec = 0;
        if (duration <= rdto.getRateFirstPulse()) {
            duration = rdto.getRateFirstPulse();
        }
        next_sec = duration % 60;
        duration = duration - next_sec;
        if (next_sec <= rdto.getRateNextPulse()) {
            next_sec = rdto.getRateNextPulse();
        }
        duration = duration + next_sec;

        if (duration <= rdto.getRateFailedPeriod()) {
            duration = 0;
        }

        if (duration > rdto.getRateGracePeriod()) {
            duration = duration - rdto.getRateGracePeriod();
        } else {
            duration = 0;
        }
        bill = (duration * rdto.getRatePerMin()) / 60;
        long l = (long) (bill * BillGenerator.precesion);
        double finalBill = (double) l / BillGenerator.precesion;
        return finalBill;
    }

    public static ClientDTO getOrgClientDTO(String ip) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            if (ip != null) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String sql = "select client_id from gateway where gateway_ip='" + ip
                        + "' and gateway_status=0 and (gateway_type=0 or gateway_type=2)"
                        + " order by inserted_time DESC";
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                    cdto = getClientDTOByID(resultSet.getLong("client_id"));
                }
                resultSet.close();
            }
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTO:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static ClientDTO getTerClientDTO(String ip) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            if (ip != null) {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String sql = "select client_id from gateway where gateway_ip='" + ip
                        + "' and gateway_status=0 and (gateway_type=1 or gateway_type=2) "
                        + " order by inserted_time DESC";
                ResultSet resultSet = statement.executeQuery(sql);

                if (resultSet.next()) {
                    cdto = getClientDTOByID(resultSet.getLong("client_id"));
                }
                resultSet.close();
            }
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTO:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static ClientDTO getClientDTOByID(long id) {
        ClientDTO cdto = null;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select * from clients where id=" + id + " order by inserted_time DESC";
            ResultSet resultSet = statement.executeQuery(sql);

            if (resultSet.next()) {
                cdto = new ClientDTO();
                cdto.setId(resultSet.getLong("id"));
                cdto.setClientID(resultSet.getString("client_id"));
                cdto.setClientType(resultSet.getInt("client_type"));
                cdto.setClientStatus(resultSet.getInt("client_status"));
                cdto.setRatePlanID(resultSet.getLong("rateplan_id"));
                cdto.setClientCreditLimit(resultSet.getDouble("client_credit_limit"));
                cdto.setClientBalance(resultSet.getDouble("client_balance"));
                cdto.setClientCallLimit(resultSet.getInt("client_call_limit"));
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getClientDTOByID:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return cdto;
    }

    public static RateDTO getRateDTO(long ratePlanID, String dateStr, String dialNo) {
        DBConnection dbConnection = null;
        Statement statement = null;
        RateDTO dto = null; 

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String[] values = dateStr.split("/");
            int day = Integer.parseInt(values[0]);
            int hour = Integer.parseInt(values[1]);
            int min = Integer.parseInt(values[2]);
            int timeOfDay = (hour * 60) + min;
            int prevLength = 0;
            long prevTime = 0;
            dto = new RateDTO();
            String sql = "select * from mvts_rates where rateplan_id=" + ratePlanID
                    + " and INSTR('" + dialNo + "', rate_destination_code)=1";
            logger.debug(sql);
            ResultSet resultSet = statement.executeQuery(sql);
          
            while (resultSet.next()) {
                int rateDay = resultSet.getInt("rate_day");
                int rateFromHour = resultSet.getInt("rate_from_hour");
                int rateToHour = resultSet.getInt("rate_to_hour");
                int rateFromMin = resultSet.getInt("rate_from_min");
                int rateToMin = resultSet.getInt("rate_to_min");
                long rateCreationTime = resultSet.getLong("rate_created_date");
                String rateDestinationCode = resultSet.getString("rate_destination_code");

                if ((rateDay == -1 || (rateDay == day
                        && (timeOfDay >= (rateFromHour * 60) + rateFromMin)
                        && (timeOfDay <= (rateToHour * 60) + rateToMin)))
                        && prevLength <= rateDestinationCode.length()
                        && prevTime < rateCreationTime) {
                    dto.setRatePlanID(ratePlanID);
                    dto.setId(resultSet.getLong("id"));
                    dto.setRateID(resultSet.getLong("rate_id"));
                    dto.setRateDestinationCode(rateDestinationCode);
                    dto.setRateDestinationName(resultSet.getString("rate_destination_name"));
                    dto.setRatePerMin(resultSet.getDouble("rate_per_min"));
                    dto.setRateFirstPulse(resultSet.getInt("rate_first_pulse"));
                    dto.setRateNextPulse(resultSet.getInt("rate_next_pulse"));
                    dto.setRateGracePeriod(resultSet.getInt("rate_grace_period"));
                    dto.setRateFailedPeriod(resultSet.getInt("rate_failed_period"));
                    dto.setRateDay(rateDay);
                    dto.setRateFromHour(rateFromHour);
                    dto.setRateFromMin(rateFromMin);
                    dto.setRateToHour(rateToHour);
                    dto.setRateToMin(rateToMin);
                    dto.setRateCreatedDate(rateCreationTime);
                    dto.setRateDescription(RatePlanLoader.getInstance().getRatePlanDTOByID(ratePlanID));
                    prevLength = dto.getRateDestinationCode().length();
                    prevTime = dto.getRateCreatedDate();
                }
            }
            resultSet.close();
            if (prevTime == 0) {
                dto = null;
            }
        } catch (Exception e) {
            logger.fatal("Exception in RateLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return dto;
    }

    public static double getPrecisionLength() {
        double precisionLength = 6.0;
        DBConnection dbConnection = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "select value from settings where param='DECIMAL_PLACES'";
            ResultSet resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                precisionLength = Double.parseDouble(resultSet.getString("value"));
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getPrecisionLength:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return Math.pow(10, precisionLength);
    }

    public static void getDisconnectCauseMappings() {
        BillGenerator.diconnectCauseMap = new HashMap<Integer, String>();
        ddatabaseconnector.DBConnection ddbConnection = null;
        Statement statement = null;

        try {
            ddbConnection = ddatabaseconnector.DBConnector.getInstance().makeConnection();
            statement = ddbConnection.connection.createStatement();
            String sql = "select ucode,reason from mvts_code";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                int ucode = resultSet.getInt("ucode");
                String reason = resultSet.getString("reason");
                BillGenerator.diconnectCauseMap.put(ucode, reason);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in disconnectCauseGetter:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ddbConnection.connection != null) {
                    ddatabaseconnector.DBConnector.getInstance().freeConnection(ddbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    public static String getOnlyIP(String ipStr) {
        String ip = "";

        if (ipStr == null || ipStr.length() < 1) {
            return "";
        }
        if (ipStr.indexOf(':') > 0) {
            ip = ipStr.substring(0, ipStr.indexOf(':'));
        }
        return ip;
    }

    public static String implodeArray(long[] inputArray, String glueString) {
        String output = "";

        if (inputArray.length > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append(inputArray[0]);
            for (int i = 1; i < inputArray.length; i++) {
                sb.append(glueString);
                sb.append(inputArray[i]);
            }
            output = sb.toString();
        }
        return output;
    }

    public static String removePrefix(String strWithPrefix, String prefix) {
        String strWithoutPrefix = strWithPrefix;

        if (strWithoutPrefix != null) {
            if (prefix != null && strWithPrefix.startsWith(prefix)) {
                strWithoutPrefix = strWithoutPrefix.substring(prefix.length());
            }
        }

        return strWithoutPrefix;
    }

    public static int getSeconds(int mmsec) {
        return (mmsec + 999) / 1000;
    }
}
