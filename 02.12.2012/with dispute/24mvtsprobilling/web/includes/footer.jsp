<%@page import="com.myapp.struts.util.Utils"%>
<div class="footer">
    <div class="right-align">
        <div class="fl_left" style="vertical-align: middle">Billing Server Time <span class="bold current_time"><%=Utils.getDateDDMMYYhhmmss(System.currentTimeMillis())%></span></div>
        <a href="http://www.ipvision-inc.com/" target="blank" title="IP Vision Ltd.">Powered by IPVision Inc.</a>    
    </div>    
    <div class="clear"></div>
</div>


<script type="text/javascript">
    $j(document).ready(function(){
        //85 is the sum of header and footer height
        $j(".left_menu").css("height", ($j(document).height()-85));
        $j(".left_menu").css("width", "15%");
        $j(".left_menu").addClass("border_right");
        $j(".left_menu").addClass("clear"); 
        get_current_time();
    });
    
    function get_current_time(){
        $j.ajax({
            type:'post',
            url:'../shared/current-time.jsp',
            data:{
                cur_time: new Date().getMilliseconds()
            },
            cache: false,
            dataType:'html',
            async:true,
            success:function(html){
                $j(".current_time").html(html);                
            }
        });        
    }
    
    
    
</script>