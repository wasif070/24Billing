<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.capacitygroup.CapacityGroupDTO,com.myapp.struts.capacitygroup.CapacityGroupLoader,java.text.NumberFormat,java.text.DecimalFormat" %>

<%
    ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOList(login_dto);
    int type = -1;

    String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
    if (msg == null) {
        msg = "";
    }
    request.getSession(true).removeAttribute(Constants.MESSAGE);
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Capacity Group assigning to clients</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "capacitygroup");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("/capacitygroup/listCapacityGroup.do?list_all=1;Capacity Group");
                        navList.add(";Assign CG to clients");
                    %> 
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <%=msg%>
                    <html:form action="/capacitygroup/assignCapacityGroup" method="post">      
                        <fieldset style="width: 75%; margin: 0 auto;"><legend class="legnd-text-color">Assign Capacity Group</legend>

                            <table class="input_table" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="CapacityGroupForm" property="message" filter="false"/>
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th valign="top">Capacity Groups</th>
                                        <td valign="top">                                            
                                            <html:select property="capacitygrp">
                                                <%
                                                    ArrayList<CapacityGroupDTO> list = CapacityGroupLoader.getInstance().getCapacityGrpDTOList();
                                                    for (CapacityGroupDTO capacitygrpDTO : list) {
                                                %>
                                                <html:option value="<%=String.valueOf(capacitygrpDTO.getGroup_id())%>"><%=capacitygrpDTO.getGroup_name()%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="capacitygrp" property="capacitygrp">
                                                <bean:write name="capacitygrp"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>

                                    <tr>
                                        <th valign="top" >Gateway Type <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="gateway_type" styleId="j_gateway_type" onchange="value">                                                     
                                                <%

                                                    for (int i = 0; i < Constants.CLIENT_TYPE_NAME.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.CLIENT_TYPE[i]%>"><%=Constants.CLIENT_TYPE_NAME[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="gateway_type" property="gateway_type">
                                                <bean:write name="gateway_type"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr> 

                                    <tr>
                                        <th valign="top">Clients<span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td> 
                                                        <div style="height: 75px; overflow: auto">                                                              
                                                            <html:select multiple="true" property="client_id" styleId="j_clientId">                                        
                                                            </html:select><br/>
                                                            <html:messages id="client_id" property="client_id">
                                                                <bean:write name="client_id"  filter="false"/>
                                                            </html:messages>                                                                
                                                        </div>  
                                                    </td>
                                                    <td valign="top">
                                                        <input type="button" id="add" value="&raquo;" />
                                                        <input type="button" id="remove" value="&laquo;" />                                                        
                                                    </td>
                                                    <td>
                                                        <div style="height: 75px; overflow: auto">  
                                                            <html:select multiple="true" property="client_id"  styleId="select2">
                                                            </html:select>  
                                                        </div>                                                        
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>                                        


                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input type="hidden" name="searchLink" value="nai" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <html:hidden property="capacityGroup" value="capacityGroup" />
                                            <input name="submit" type="submit" id="btnDialPlan" class="custom-button" value="Assign" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        move.init();        
        if($j("#j_gateway_type").val()>-1){                
            $j.ajax({
                type:'post',
                url:'../capacitygroup/load_clients.jsp',
                data:{
                    type:$j("#j_gateway_type").val()
                },
                success:function(html){
                    $j("#j_clientId").html(html);
                }
            });
        }   
    });
</script>
