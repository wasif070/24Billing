<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.capacitygroup.CapacityGroupLoader,com.myapp.struts.capacitygroup.CapacityGroupDTO,com.myapp.struts.session.Constants,java.util.ArrayList" %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Edit Client's Capacity Group</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "operator");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("/capacitygroup/listCapacityGroup.do?list_all=1;Capacity Group");
                        navList.add(";Edit ClientCapacityGroups");
                       // int id = Integer.parseInt(request.getSession().getAttribute("id").toString());
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/capacitygroup/editClientCapacityGroup" method="post">  
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Edit Client's Capacity Group</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="CapacityGroupForm" property="message" filter="false"/>
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th valign="top" >Client Name </th>
                                        <td valign="top" >
                                            <html:text property="clientId" readonly="true" /><br/>
                                            <html:messages id="clientId" property="clientId">
                                                <bean:write name="clientId"  filter="false"/>
                                            </html:messages>
                                        </td>
                                            
                                    </tr>        
                                    <tr>
                                        <th valign="top" >Gateway Type </th>
                                        <td valign="top" >
                                            <html:text property="gateway_type_name" readonly="true" /><br/>
                                            <html:messages id="gateway_type_name" property="gateway_type_name">
                                                <bean:write name="gateway_type_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                            
                                    </tr>  
                                    <tr>
                                        <th valign="top" >Gateway IP </th>
                                        <td valign="top" >
                                            <html:text property="gw_ip" readonly="true" /><br/>
                                            <html:messages id="gw_ip" property="gw_ip">
                                                <bean:write name="gw_ip"  filter="false"/>
                                            </html:messages>
                                        </td>
                                            
                                    </tr>
                                    <tr>
                                        <th valign="top">Capacity Group</th>
                                        <td valign="top">                                            
                                            <html:select property="capacitygrpName">
                                                <%
                                                    ArrayList<CapacityGroupDTO> list = CapacityGroupLoader.getInstance().getCapacityGrpDTOList();
                                                    for (CapacityGroupDTO capacitygrpDTO : list) {
                                                %>
                                                <html:option value="<%=String.valueOf(capacitygrpDTO.getGroup_id())%>"><%=capacitygrpDTO.getGroup_name()%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="capacitygrpName" property="capacitygrpName">
                                                <bean:write name="capacitygrpName"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                            
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>                                            
                                            <input name="submit" type="submit" class="custom-button" onclick="javascript:return confirm('Are you sure to update the destination Code?');" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        utilities.init();
    });
</script>