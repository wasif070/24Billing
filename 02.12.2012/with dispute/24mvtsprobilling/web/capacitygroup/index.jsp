<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }
    request.getSession(true).removeAttribute(Constants.MESSAGE);    
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Capacity Group</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "capacitygroup");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
                <div class="right_content_view fl_right">             
                <div class="pad_10 border_left">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("capacitygroup/listCapacityGroup.do?list_all=1;CapacityGroup");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/capacitygroup/listCapacityGroup.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Capacity Group Name</th>
                                    <td><html:text property="capacitygrpName" /></td>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                    <td>
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr> 
                            </table>
                        </div>
                    </html:form>                 
                    <html:form action="/capacitygroup/multiplecapacitygroup.do" method="post" onsubmit="return confirm('Are you sure to take action?');" >                                    
                        <div class="over_flow_content display_tag_content" align="center">
                            <div class="over_flow_content display_tag_content" align="center"> 
                                <%=msg%>                                
                            </div>
                            
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.CapacityGroupForm.capacityGrpList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="CapacityGroup" />
                                <display:setProperty name="paging.banner.items_name" value="CapacityGroups" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='a Capacity Group' />" style="width:3%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.group_id}" class="select_id"/>
                                </display:column>
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>                         
                                <display:column property="group_name" class="left-align" title="Name" sortable="true" style="width:20%" />
                                <display:column property="description" class="left-align" title="Description" sortable="true" style="width:20%" />
                                <display:column property="parent_group_name" class="custom_column1" title="Parent Group" sortable="true" style="width:10%" />
                                <display:column property="capacity" class="custom_column1" title="Capacity" sortable="true" style="width:10%" />
                                <c:choose>
                                    <c:when test="${editPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:10%;" >
                                            <a href="../capacitygroup/getCapacity.do?id=${data.group_id}" class="edit" title="Change"></a>
                                            <c:choose>
                                                <c:when test="${data.group_id!=-1}"><a href="../capacitygroup/deleteCapacityGroup.do?id=${data.group_id}" onclick="javascript:return confirm('Are you sure to want delete the CapacityGroup?');" class="drop" title="Delete"></a></c:when>
                                            </c:choose>                                    
                                        </display:column>
                                    </c:when>
                                </c:choose>

                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                            <div class="button_area">
                                <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />                                
                                <%if(perms[com.myapp.struts.util.AppConstants.DELETE]==1){%>
                                <html:submit property="deleteBtn" styleClass="custom-button jmultipebtn" value="Delete Selected" />
                                <%}%>
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <%@include file="../includes/footer.jsp"%>
        </div>
    </body>
</html>