<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.dialplan.DialplanLoader,com.myapp.struts.dialplan.DialplanDTO,java.text.NumberFormat,java.text.DecimalFormat" %>

<%
    ArrayList<DialplanDTO> gatewayList = DialplanLoader.getInstance().getGatewayList(login_dto);
    ArrayList<Integer> hours = Utils.getTimeValue(24);
    ArrayList<Integer> min = Utils.getTimeValue(60);
    NumberFormat formatter = new DecimalFormat("00");
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Add Dial Plan</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "dialplan");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("dialplan/listDialplan.do?list_all=1;Dial Plan");
                        navList.add(";Add New Dial Plan");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/dialplan/addDialplan" method="post">      
                        <fieldset style="width: 75%; margin: 0 auto;"><legend class="legnd-text-color">Add Dial Plan</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="DialplanForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                        <!--
                                    <tr>
                                        <th valign="top">Reseller <span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <html:select property="parent_id" styleId="parent_id">
                                                <html:option value="-1">--Select--</html:option>
                                                <%for (ClientDTO cl_dto : ClientLoader.getInstance().getClientDTOByLevel(login_dto.getOwn_id(), 2,login_dto.getClient_level())) {%>
                                                <html:option value="<%=String.valueOf(cl_dto.getId())%>"><%=cl_dto.getClient_id()%></html:option>
                                                <%}%>
                                            </html:select>
                                            <div class="clear"></div>
                                            <html:messages id="parent_id" property="parent_id">
                                                <bean:write name="parent_id"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Client <span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <html:select property="client_id" styleId="client_id">
                                                <html:option value="-1">--Select--</html:option>
                                                <%for (ClientDTO cl_dto : ClientLoader.getInstance().getClientDTOByLevel(login_dto.getOwn_id(), -1,login_dto.getClient_level())) {%>
                                                <html:option value="<%=String.valueOf(cl_dto.getId())%>"><%=cl_dto.getClient_id()%></html:option>
                                                <%}%>
                                            </html:select>
                                            <div class="clear"></div>
                                            <html:messages id="client_id" property="client_id">
                                                <bean:write name="client_id"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                        -->
                                    <tr>
                                        <th valign="top" >Dial Plan Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="dialplan_name" /><br/>
                                            <html:messages id="dialplan_name" property="dialplan_name">
                                                <bean:write name="dialplan_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Gateway<span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td>
                                                        <div style="height: 75px; overflow: auto">                                                              
                                                            <select multiple="true" id="select1" name="select1">  
                                                                <%
                                                                    if (gatewayList != null && gatewayList.size() > 0) {
                                                                        int size = gatewayList.size();
                                                                        for (int i = 0; i < size; i++) {
                                                                            DialplanDTO g_dto = (DialplanDTO) gatewayList.get(i);
                                                                %>
                                                                <option value="<%=String.valueOf(g_dto.getGateway_id_single())%>"><%=g_dto.getGateway_name_single()%></option>                                                                  
                                                                <% }
                                                                    }
                                                                %>
                                                                <html:messages id="gateway_id" property="gateway_id">
                                                                    <bean:write name="gateway_id"  filter="false"/>
                                                                </html:messages>
                                                            </select>                                                                   
                                                        </div>  
                                                    </td>
                                                    <td valign="top">
                                                        <input type="button" id="add" value="&raquo;" />
                                                        <input type="button" id="remove" value="&laquo;" />                                                        
                                                    </td>
                                                    <td>
                                                        <div style="height: 75px; overflow: auto">  
                                                            <html:select multiple="true" property="gateway_id"  styleId="select2">
                                                            </html:select>  
                                                        </div>                                                        
                                                    </td>
                                                    <td valign="top">
                                                        <input type="button" id="btnMoveUp" value="&uArr;" />
                                                        <input type="button" id="btnMoveDown" value="&dArr;" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Description</th>
                                        <td valign="top" >
                                            <html:text property="dialplan_description" /><br/>
                                            <html:messages id="dialplan_description" property="dialplan_description">
                                                <bean:write name="dialplan_description"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Prefix Allow <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="dialplan_dnis_pattern" /><br/>
                                            <html:messages id="dialplan_dnis_pattern" property="dialplan_dnis_pattern">
                                                <bean:write name="dialplan_dnis_pattern"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Source Number Translate</th>
                                        <td valign="top" >
                                            <html:text property="dialplan_ani_translate" /><br/>
                                            <html:messages id="dialplan_ani_translate" property="dialplan_ani_translate">
                                                <bean:write name="dialplan_ani_translate"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Destination Number Translate</th>
                                        <td valign="top" >
                                            <html:text property="dialplan_dnis_translate" /><br/>
                                            <html:messages id="dialplan_dnis_translate" property="dialplan_dnis_translate">
                                                <bean:write name="dialplan_dnis_translate"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Balancing Method <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="dialplan_hunt_mode">
                                                <%
                                                    for (int i = 0; i < Constants.DIALPLAN_BALANCING_METHOD_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.DIALPLAN_BALANCING_METHOD_VALUE[i]%>"><%=Constants.DIALPLAN_BALANCING_METHOD_STRING[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="dialplan_hunt_mode" property="dialplan_hunt_mode">
                                                <bean:write name="dialplan_hunt_mode"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Precedence</th>
                                        <td valign="top" >
                                            <html:text property="dialplan_priority" /><br/>
                                            <html:messages id="dialplan_priority" property="dialplan_priority">
                                                <bean:write name="dialplan_priority"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Capacity</th>
                                        <td valign="top" >
                                            <html:text property="dialplan_capacity" /><br/>
                                            <html:messages id="dialplan_capacity" property="dialplan_capacity">
                                                <bean:write name="dialplan_capacity"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Scheduling <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="dialplan_sched_type">
                                                <%
                                                    for (int i = 0; i < Constants.DIALPLAN_SCHEDULING_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.DIALPLAN_SCHEDULING_VALUE[i]%>"><%=Constants.DIALPLAN_SCHEDULING_STRING[i]%></html:option>
                                                <% }%>
                                            </html:select><br/>
                                            <html:messages id="dialplan_sched_type" property="dialplan_sched_type">
                                                <bean:write name="dialplan_sched_type"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td class="selopt">
                                            <div class="fl_left">Hour</div><div class="fl_left">Min</div></td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Time of day - on <span class="req_mark">*</span></th>
                                        <td class="selopt">
                                            <html:select property="onhour" styleClass="onhour">
                                                <%
                                                    for (int i = 0; i < hours.size(); i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="onmin" styleClass="onmin">
                                                <%
                                                    for (int j = 0; j < min.size(); j++) {
                                                        String increment = String.valueOf(j);
                                                        String temp = formatter.format((j));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>           
                                    </tr>
                                    <tr><th>&nbsp;</th><td colspan="3" class="selopt"><div class="fl_left">Hour</div><div class="fl_left">Min</div></td></tr>
                                    <tr>
                                        <th valign="top" >Time of day - off <span class="req_mark">*</span></th>
                                        <td class="selopt">
                                            <html:select property="offhour" styleClass="offhour">
                                                <%
                                                    for (int i = 0; i < hours.size(); i++) {
                                                        String increment = String.valueOf(i);
                                                        String temp = formatter.format((i));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                            <html:select property="offmin" styleClass="offmin">
                                                <%
                                                    for (int j = 0; j < min.size(); j++) {
                                                        String increment = String.valueOf(j);
                                                        String temp = formatter.format((j));
                                                %>
                                                <html:option value="<%=increment%>"><%=temp%></html:option>
                                                <%}%>
                                            </html:select>
                                        </td>           
                                    </tr>
                                    <tr>
                                        <th valign="top" >Status <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="dialplan_enable">
                                                <%
                                                    for (int i = 0; i < Constants.DIALPLAN_STATUS_VALUE.length; i++) {
                                                %>
                                                <html:option value="<%=Constants.DIALPLAN_STATUS_VALUE[i]%>"><%=Constants.DIALPLAN_STATUS_STRING[i]%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="dialplan_enable" property="dialplan_enable">
                                                <bean:write name="dialplan_enable"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input type="hidden" name="searchLink" value="nai" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" id="btnDialPlan" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>

<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        utilities.init();
    });
</script>