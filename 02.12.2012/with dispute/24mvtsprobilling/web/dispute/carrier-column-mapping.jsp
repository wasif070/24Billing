<%-- 
    Document   : carrier-column-mapping
    Created on : Sep 2, 2012, 11:28:13 AM
    Author     : reefat
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>
<%
   int numberOfColumns = Constants.NUMBER_OF_COLUMNS;  
   int count = 0;
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>24Billing :: Column Mapping</title>
        <%@include file="../includes/header.jsp"%>
    </head>
        <%
            ArrayList<String> columnList = new ArrayList<String>();
            columnList.add("cdr_date");
            columnList.add("dialed_no");
            columnList.add("terminated_no");
            columnList.add("duration");
            columnList.add("connection_time");
            columnList.add("origin_ip");
            columnList.add("term_ip");

            String result_msg = (String) request.getSession(true).getAttribute(Constants.RESULT_MESSAGE);
                       
            if (request.getParameter("url_msg") != null) {
                result_msg = null;
                
            }   
            
            if (result_msg == null) {
                result_msg = "";
            } else {
                if(result_msg.equalsIgnoreCase("Inserted Successfully!!"))                    
                result_msg = "<div class='success' style='width:700px;'>" + result_msg + "</div>";
                else
                    result_msg = "<div class='error' style='width:700px;'>" + result_msg + "</div>";
            }
          
        %>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">             
                <div class="pad_10 border_left">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("dispute/dispute-summary.jsp;Dispute");                 
                 navList.add(";Carrier column mapping");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                   <html:form action="/dispute/columnMapping.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr >
                                    <th width="50%" >Number of Columns</th>
                                    <td >
                                        <input type="text" style="width:50px" name="n_o_c" value="<%=numberOfColumns%>" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit property="dd" styleClass="search-button" value="Ok" />
                                    </td>
                                </tr>
                            </table>
                        </div> 
                    </html:form> 
                    <html:form action="/dispute/MappingSubmit.do" method="post">
                          
                       <div class="display_tag_content " align="center">
                                               <%=result_msg%>
                           <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                    <% 
                                        for(String s:columnList){
                                    %>
                                    <tr>
                                        <th width="50%" ><%= String.valueOf(s)%></th>                                        
                                        <td>
                                            <html:select property="our_db_columns">
                                                <%
                                                for(count=1;count<=numberOfColumns;count++){
                                                %>    
                                                <%
                                                    String optionValue = "Column # " + count;
                                                %>
                                                <html:option value="<%= optionValue%>"></html:option>
                                                <%
                                                }    
                                                %>
                                            </html:select> 
                                            <html:messages id="our_db_columns" property="our_db_columns">
                                                <bean:write name="our_db_columns"  filter="false"/>
                                            </html:messages>                                             
                                        </td>
                                    </tr>
                                    <%
                                        }
                                    %>                            
                              
                            <tr>
                                <td colspan="4" align="center">
                                    <html:submit property="column_submit_btn" styleClass="search-button" value="submit"></html:submit>
                                </td>
                            </tr>
                           </table>
                       </div>
                   </html:form>                       
                    
                </div>  

        </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>                   
        </div>
    </body>
</html>