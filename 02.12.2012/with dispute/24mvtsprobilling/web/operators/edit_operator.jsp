<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Edit Operator</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "operator");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("operators/listOperator.do?list_all=1;Operator");
                        navList.add(";Edit Operator");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/operators/editOperator" method="post">  
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Edit Operator</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="OperatorsForm" property="message" filter="false"/>
                                        </td>
                                    </tr>                                        
                                    <tr>
                                        <th valign="top" >Operator Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="operator_name" /><br/>
                                            <html:messages id="operator_name" property="operator_name">
                                                <bean:write name="operator_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Operator Prefix</th>
                                        <td valign="top" >
                                            <html:text property="operator_prefix" /><br/>
                                            <html:messages id="operator_prefix" property="operator_prefix">
                                                <bean:write name="operator_prefix"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <html:hidden property="operator_id" />
                                            <html:hidden property="action" value="<%=String.valueOf(Constants.UPDATE)%>" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" onclick="javascript:return confirm('Are you sure to update the destination Code?');" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
<script language="javascript" type="text/javascript">
    $j(document).ready(function(){
        utilities.init();
    });
</script>