<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Edit ANS</title>
        <div><%@include file="../includes/header.jsp"%></div>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "ans");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("ans/listAns.do?list_all=1;ANS");
                        navList.add(";Edit ANS");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/ans/editAns" method="post">
                        <fieldset style="width: 50%; margin: 0 auto"><legend class="legnd-text-color">Edit ANS Information</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center"  valign="bottom">
                                            <bean:write name="AnsForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >ANS Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="name" /><br/>
                                            <html:messages id="name" property="name">
                                                <bean:write name="name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Company Name <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="company_name" /><br/>
                                            <html:messages id="company_name" property="company_name">
                                                <bean:write name="company_name"  filter="false"/>
                                            </html:messages>                                                                                                                                    
                                        </td>
                                    </tr>       
                                    <tr>
                                        <th valign="top" >Primary Prefix <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:text property="primary_prefix" /><br/>
                                            <html:messages id="primary_prefix" property="primary_prefix">
                                                <bean:write name="primary_prefix"  filter="false"/>
                                            </html:messages>                                                                                                                                    
                                        </td>
                                    </tr>       
                                    <tr>
                                        <th valign="top" >Other Prefixes[Comma Separated]</th>
                                        <td valign="top" >
                                            <html:text property="other_prefixes" /><br/>
                                            <html:messages id="other_prefixes" property="other_prefixes">
                                                <bean:write name="other_prefixes"  filter="false"/>
                                            </html:messages>                                                                                                                                    
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top" >Status <span class="req_mark">*</span></th>
                                        <td valign="top" >
                                            <html:select property="status">
                                                <html:option value="0">Inactive</html:option>
                                                <html:option value="1">Active</html:option>
                                            </html:select>
                                            <br/>
                                            <html:messages id="status" property="status">
                                                <bean:write name="status"  filter="false"/>
                                            </html:messages>                                                                                                                                    
                                        </td>
                                    </tr>       
                                    
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <html:hidden property="id" />
                                            <input name="submit" type="submit" onclick="javascript:return confirm('Are you sure to want Update the ANS?');" class="custom-button" value="Update" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>
    </body>
</html>