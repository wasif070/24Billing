<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

<%
   ArrayList<Integer> days = Utils.getDay();
   ArrayList<String> months = Utils.getMonth();
   ArrayList<Integer> years = Utils.getYear();
   NumberFormat formatter = new DecimalFormat("00");
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }   
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Month Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>            
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                   java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                   navList.add("aggregatereports/monthlyReport.do?list_all=1;Monthly Report");
                   navList.add(";");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/aggregatereports/monthlyReport.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Client ID</th>
                                    <td colspan="3">
                                        <html:select property="origin_client_id">
                                            <html:option value="-1">Select Client</html:option>
                                            <%
                                            ArrayList<ClientDTO> originClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.ORIGINATION, login_dto.getOwn_id());
                                                if (originClientList != null && originClientList.size() > 0) {
                                                    int size = originClientList.size();
                                                    for (int i = 0; i < size; i++) {
                                                        ClientDTO c_dto = (ClientDTO) originClientList.get(i);
                                            %>
                                            <html:option value="<%=String.valueOf(c_dto.getId())%>"><%=c_dto.getClient_id()%></html:option>;
                                            <%
                                                    }
                                                }
                                            %>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Start Time</th>
                                    <td class="selopt">
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="from_year" styleId="from_year">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="from_month" styleClass="month" styleId="from_month">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="from_day" >
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="from_hour" styleId="from_hour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));

                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="from_min" styleId="from_min">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="from_sec" styleId="from_sec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <th>End Time</th>
                                    <td class="selopt">   
                                        <table>
                                            <tr>
                                                <th style="text-align: left">Year</th><th style="text-align: left">Month</th><th style="text-align: left">Day</th><th style="text-align: left">Hour</th><th style="text-align: left">Min</th><th style="text-align: left">Sec</th>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <html:select property="to_year" styleId="to_year">
                                                        <%
                                                            for (int i = 0; i < years.size(); i++) {
                                                                String year = String.valueOf(years.get(i));
                                                        %>
                                                        <html:option value="<%=year%>"><%=year%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="to_month" styleClass="month" styleId="to_month">
                                                        <%
                                                            for (int i = 0; i < months.size(); i++) {
                                                                String month = months.get(i);
                                                                String increment = String.valueOf(i + 1);
                                                        %>
                                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="to_day" >
                                                        <%
                                                            for (int i = 0; i < days.size(); i++) {
                                                                String increment = String.valueOf(i + 1);
                                                                String temp = formatter.format((i + 1));
                                                        %>
                                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                                        <%}%>
                                                    </html:select>         
                                                </td>
                                                <td>
                                                    <html:select property="to_hour" styleId="to_hour">
                                                        <%
                                                            for (int i = 0; i < 24; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                                <td>
                                                    <html:select property="to_min" styleId="to_min">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>  
                                                </td><td>
                                                    <html:select property="to_sec" styleId="to_sec">
                                                        <%
                                                            for (int i = 0; i < 60; i++) {
                                                                String increment = String.valueOf(formatter.format(i));
                                                        %>
                                                        <html:option value="<%=String.valueOf(i)%>"><%=increment%></html:option>
                                                        <%}%>
                                                    </html:select>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>                                
                                <tr>
                                    <td colspan="4" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="search_btn" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                                </tr>                               
                            </table>
                        </div>
                    </html:form>
                    <html:form action="/aggregatereports/monthlyReport" method="post" >
                        <div class="over_flow_content display_tag_content" align="center">
                            <%=msg%>
                            <script type="text/javascript">count=0;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="true" id="data" name="sessionScope.AggregateReportForm.list" requestURI="../aggregatereports/monthlyReport.do">
                                <display:setProperty name="paging.banner.item_name" value="Record" /> 
                                <display:setProperty name="paging.banner.items_name" value="Records" />
                                <display:column class="custom_column2" title="Nr" style="width:7%;" media="html">
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>    
                                <display:column property="disconnect_time_str" class="left-align" title="Date" sortable="true" />
                                <display:column property="client_id_str" class="left-align" title="Client ID" sortable="true"/>                                
                                <display:column property="total_duration" class="right-align" title="Duration(MM:SS)" sortable="true" />
                                <display:column property="total_duration_sec" class="right-align" title="Duration(Sec)" sortable="true" />
                            </display:table>
                            <% request.getSession(true).removeAttribute(Constants.MESSAGE); %>                            
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>