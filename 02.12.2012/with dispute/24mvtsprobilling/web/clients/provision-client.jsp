
<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Client Provisioning Info</title>
        <div><%@include file="../includes/header.jsp"%></div>
        <%@include file="../login/login-check.jsp"%>

        <%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.rateplan.RateplanLoader,com.myapp.struts.rateplan.RateplanDTO" %>
        <%@taglib uri="http://displaytag.sf.net" prefix="display" %>
    </head>
    <%
        String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
        if (msg == null) {
            msg = "";
        }
        String pw = (String) request.getSession(true).getAttribute("print");
    %>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">      
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("clients/provision-client.jsp;Clients");
                        navList.add(";Client Provisioning Info");
                    %>

                    <html:form action="/clients/listClient.do?id=1" method="post">                       
                        <table class="input_table" cellspacing="0" cellpadding="0" >
                            <tbody>                                                             
                                <tr>
                                    <td align="center">   
                                        <fieldset style="width: 75%; margin: 0 auto;"><legend class="legnd-text-color">Provisioning Info</legend>
                                            <%=msg%>
                                            <div id="copy"><%=pw%></div>
                                            <input type="button" id='copy-button' name="copy" value="Copy to Clipboard" />                      
                                        </fieldset>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>        
        </div>        
    </body>

    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
    <script type="text/javascript" src="http://www.steamdev.com/zclip/js/jquery.zclip.min.js"></script>
    <script type="text/javascript" src="http://www.steamdev.com/zclip/js/jquery.snippet.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#copy-button").zclip({
                path: "http://www.steamdev.com/zclip/js/ZeroClipboard.swf",
                copy: function() {
                    return $(this).prev().text();
                }
            });

        });
    </script>
</html>