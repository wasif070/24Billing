<%@page import="ans.AnsLoader"%>
<%@page import="ans.AnsDTO"%>
<%@page import="igwreports.IGWReportDTO"%>
<%@page import="igwreports.IncomingReportDAO"%>
<%@page import="com.myapp.struts.util.AppConstants"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,java.text.DecimalFormat,java.text.NumberFormat,com.myapp.struts.gateway.GatewayDTO,com.myapp.struts.gateway.GatewayLoader,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader" %>

        <%
            String links = "";
            int pNo = 1;
            int recordPerPage = 10;

            String origin_client_id = (String) request.getParameter("oid");
            if (origin_client_id == null) {
                origin_client_id = "";
            } else {
                links += "&oid=" + origin_client_id;
            }

            String term_client_id = (String) request.getParameter("tid");
            if (term_client_id == null) {
                term_client_id = "";
            } else {
                links += "&tid=" + term_client_id;
            }

            if (request.getParameter("recordPerPage") != null) {
                boolean status = Utils.IntegerValidation(request.getParameter("recordPerPage"));
                if (status == true) {
                    recordPerPage = Integer.parseInt(request.getParameter("recordPerPage"));
                }
            }

            if (request.getParameter("pageNo") != null) {
                boolean status = Utils.IntegerValidation(request.getParameter("pageNo"));
                if (status == true) {
                    pNo = Integer.parseInt(request.getParameter("pageNo"));
                }
            }


            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            }

            ArrayList<Integer> days = Utils.getDay();
            ArrayList<String> months = Utils.getMonth();
            ArrayList<Integer> years = Utils.getYear();
            ArrayList<IGWReportDTO> cdrList = (ArrayList<IGWReportDTO>) request.getSession(true).getAttribute("IGWReportDTO");
            NumberFormat formatter = new DecimalFormat("00");
            NumberFormat formatter1 = new DecimalFormat("##00.0000");

            ArrayList<ClientDTO> originClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.ORIGINATION, login_dto.getOwn_id());
            ArrayList<ClientDTO> termClientList = ClientLoader.getInstance().getClientDTOByTypeAndParent(Constants.TERMINATION, login_dto.getOwn_id());
        %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: IGW Incoming Report</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body" style="width:100%">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left" style="width:20%;">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "report");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right border_left" style="width:80%">
                <div class="">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";IGW Report");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/igwreports/listCDR.do" method="post" >
                        <div>
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">                                
                                <tr>
                                    <th>From Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="fromYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="fromDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>                                        
                                    </td>
                                    <th>To Date</th>
                                    <td colspan="3" class="selopt">
                                        <html:select property="toYear" styleClass="" styleId="fromYear">
                                            <%
                                                for (int i = 0; i < years.size(); i++) {
                                                    String year = String.valueOf(years.get(i));
                                            %>
                                            <html:option value="<%=year%>"><%=year%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toMonth" styleClass="month" styleId="fromMonth">
                                            <%
                                                for (int i = 0; i < months.size(); i++) {
                                                    String month = months.get(i);
                                                    String increment = String.valueOf(i + 1);
                                            %>
                                            <html:option value="<%=increment%>"><%=month%></html:option>
                                            <%}%>
                                        </html:select>
                                        <html:select property="toDay" styleClass="">
                                            <%
                                                for (int i = 0; i < days.size(); i++) {
                                                    String increment = String.valueOf(i + 1);
                                                    String temp = formatter.format((i + 1));
                                            %>
                                            <html:option value="<%=increment%>"><%=temp%></html:option>
                                            <%}%>
                                        </html:select>
                                    </td>
                                </tr>
                                <tr>                                    
                                    <th>ICX</th>
                                    <td colspan="3">
                                        <html:select property="icx" styleClass="">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                                ArrayList<ClientDTO> clientList = ClientLoader.getInstance().getClientDTOByType(Constants.TERMINATION);
                                                for (ClientDTO client_dto : clientList) {
                                                    if (client_dto.getIs_icx() == AppConstants.YES) {
                                            %>
                                            <html:option value="<%=client_dto.getId()%>"><%=client_dto.getClient_id()%></html:option>
                                            <%}
                                                }%>
                                        </html:select>
                                    </td> 
                                    <th>ANS</th>
                                    <td colspan="3">                                        
                                        <html:select property="ans_prefix" styleClass="">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                                ArrayList<AnsDTO> list = AnsLoader.getInstance().getAnsDTOList(login_dto);
                                                out.print(list.size());
                                                for (AnsDTO ansDTO : list) {
                                            %>
                                            <html:option value="<%=ansDTO.getPrimary_prefix()%>"><%=ansDTO.getCompany_name()%></html:option>
                                            <%}%>                                            
                                        </html:select>
                                    </td> 
                                </tr>
                                <tr>
                                    <th>Record Per Page</th>
                                    <td colspan="3">
                                        <html:text property="recordPerPage" style="width: 50px" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td colspan="3">
                                        <input type="text" name="pageNo" style="width: 50px" value="<%=String.valueOf(pNo)%>" />
                                    </td>                                    
                                </tr>                                
                                <tr>
                                    <td colspan="8" align="center">
                                        <html:submit styleClass="search-button" value="Search" property="doSearch" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>                  
                    <div style="text-align: center;">
                        <table cellspacing="1" cellpadding="2" align="center">
                            <thead>
                                <%
                                    int reporting_type = 1;
                                    if (request.getParameter("reporting_type") != null) {
                                        reporting_type = Integer.parseInt(request.getParameter("reporting_type"));
                                    }

                                    int dataListSize = 0;
                                    int pageNo = 1;
                                    int prevPageNo = 1;
                                    int nextPageNo = 1;
                                    int totalPages = 1;
                                    if (cdrList != null) {
                                        dataListSize = cdrList.get(cdrList.size() - 1).getTotal_cdr();
                                    }
                                    int perPageRecord = recordPerPage;
                                    if (perPageRecord == 0) {
                                        perPageRecord = Constants.PER_PAGE_RECORD;
                                    }

                                    if (dataListSize > 0) {
                                        totalPages = dataListSize / perPageRecord;
                                        if (dataListSize % perPageRecord != 0) {
                                            totalPages++;
                                        }
                                    }

                                    if (request.getParameter("pageNo") != null) {
                                        pageNo = Integer.parseInt(request.getParameter("pageNo"));
                                    }

                                    int pageStart = (pageNo - 1) * perPageRecord;
                                    if (pageNo > 1) {
                                        prevPageNo = pageNo - 1;
                                    }
                                    nextPageNo = pageNo;
                                    if ((pageNo + 1) <= totalPages) {
                                        nextPageNo = pageNo + 1;
                                    }

                                    String firstLink = request.getContextPath() + "/igwreports/listCDR.do?pageNo=1&recordPerPage=" + perPageRecord + links;
                                    String prevLink = request.getContextPath() + "/igwreports/listCDR.do?pageNo=" + prevPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String nextLink = request.getContextPath() + "/igwreports/listCDR.do?pageNo=" + nextPageNo + "&recordPerPage=" + perPageRecord + links;
                                    String lastLink = request.getContextPath() + "/igwreports/listCDR.do?pageNo=" + totalPages + "&recordPerPage=" + perPageRecord + links;
                                %>
                                <tr><td colspan="30" align="center"><input type="hidden" name="pageNo" value="<%=pageNo%>"/>Page&nbsp;<%=pageNo%>&nbsp;of&nbsp;<%=totalPages%></td></tr>
                                <tr><td colspan="30" align="center"><a href="<%=firstLink%>">&laquo;First</a> | <a href="<%=prevLink%>">&laquo; Prev</a> | <a href="<%=nextLink%>">Next &raquo;</a> | <a href="<%=lastLink%>">Last &raquo;</a></td></tr>
                            </thead>
                        </table>
                    </div>
                    <div class="over_flow_content" style="font-size:8pt;">
                        <table class="content_table" cellspacing="1" cellpadding="2"> 
                            <thead>
                                <tr class="header">                                    
                                    <th rowspan="2">Nr.</th>
                                    <%if (reporting_type == 1) {%>
                                    <th rowspan="2">Date</th>
                                    <%}
                                        if (reporting_type == 2) {%>
                                    <th rowspan="2">Route to ICX</th>
                                    <%}
                                        if (reporting_type == 3) {%>
                                    <th rowspan="2">Carrier Name</th>
                                    <th rowspan="2">Route to ICX</th>
                                    <%}%>
                                    <th colspan="2">IP Calls</th>
                                    <th colspan="2">TDM Calls</th>
                                    <th rowspan="2">Total No. Of Calls</th>
                                    <th rowspan="2">Total Paid Minutes</th>
                                    <%if (reporting_type == 3) {%>
                                    <th rowspan="2">Term. Rate</th>
                                    <%}
                                        if (reporting_type == 2 || reporting_type == 3) {%>
                                    <th rowspan="2">% of Calls</th>
                                    <th rowspan="2">% of Paid Minutes</th>
                                    <%}%>
                                </tr>                                
                                <tr class="header">                                    
                                    <th>No Of Calls</th>
                                    <th>Paid Minutes</th>
                                    <th>No Of Calls</th>
                                    <th>Paid Minutes</th>
                                </tr>                                
                            </thead>
                            <tbody>
                                <%
                                    String bg_class = "odd";
                                    if (cdrList != null && cdrList.size() > 0) {
                                        for (int inc = 0; inc < cdrList.size() - 1; inc++) {
                                            IGWReportDTO obj = cdrList.get(inc);
                                            if (inc % 2 == 0) {
                                                bg_class = "even";
                                            } else {
                                                bg_class = "odd";
                                            }
                                %>
                                <tr class="<%=bg_class%> center-align">
                                    <td align="center"><%=pageStart + inc + 1%>.</td>
                                    <%if (reporting_type == 1) {%>
                                    <td align="center"><%=obj.getCdr_date()%></td>                                    
                                    <%}
                                        if (reporting_type == 2) {%>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <%}
                                        if (reporting_type == 3) {%>
                                    <td><%=obj.getOrigin_client_name()%></td>
                                    <td><%=obj.getTerm_client_name()%></td>
                                    <%}%>
                                    <td align="right"><%=obj.getNo_of_ip_calls()%></td>
                                    <td align="right"><%=Utils.getMMSS(obj.getIp_calls_paid_minutes())%></td>
                                    <td align="right"><%=obj.getNo_of_tdm_calls()%></td>
                                    <td align="right"><%=Utils.getMMSS(obj.getTdm_calls_paid_minutes())%></td>
                                    <td align="right"><%=obj.getTotal_calls()%></td>
                                    <td align="right"><%=Utils.getMMSS(obj.getTotal_minutes())%></td>     
                                    <%if (reporting_type == 3) {%>
                                    <td align="right"><%=formatter1.format(obj.getTerm_rate())%></td>
                                    <%}
                                        if (reporting_type == 2 || reporting_type == 3) {%>
                                    <td><%=formatter1.format(obj.getPercentage_of_calls())%></td>
                                    <td><%=formatter1.format(obj.getPercentage_of_minutes())%></td>
                                    <%}%>
                                </tr>                                
                                <%}
                                } else {%>
                                <tr><th colspan="8">No Content Found!</th></tr>
                                <% }
                                %>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>