<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.home.HomeLoader"%>
<%@page import="com.myapp.struts.home.HomeDTO"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,java.text.DecimalFormat,java.text.NumberFormat" %>
<%@page import="com.myapp.struts.util.Utils"%>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Home</title>
        <%@include file="../includes/header.jsp"%>
        <%
            NumberFormat formatter = new DecimalFormat("#0.00");
            ClientDTO client_dto = null;
            try {
                client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
            } catch (Exception ex) {
            }
        %>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <div class="right_content_view fl_right">                
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add(";");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <div class="over_flow_content">
                        <div class="display_tag_content" align="center">
                            <div style="text-align: center; font-size: 20px; font-weight: bold">Welcome <%=login_dto.getClientId()%></div>
                            <%if (client_dto != null) {%>
                            <div style="text-align: center; font-size: 17px; font-weight: bold">Cur. Balance <%=client_dto.getClient_balance()%></div>
                            <%}%>
                            <div style="text-align: center; font-size: 14px; height: 30px">Today&apos;s Client Report</div>
                            <div class="clear"></div>
                            <table class="reporting_table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>Origination Client</th>
                                    <th>Total Successful Calls</th>
                                    <th>Total Call Duration</th>
                                    <th>Current Balance</th>
                                </tr>
                                <%
                                    ArrayList<HomeDTO> orgList = HomeLoader.getInstance().getOrgDTOList(login_dto);
                                    if (orgList != null && orgList.size() > 0) {
                                        for (int i = 0; i < orgList.size(); i++) {
                                            HomeDTO dto = orgList.get(i);
                                %>                         
                                <tr>
                                    <td align="left"><%=dto.getOrg_client_name()%></td>
                                    <td align="right"><%=dto.getOrg_calls()%></td>
                                    <td align="center"><%=Utils.getTimeMMSS(dto.getOrg_duration())%></td>
                                    <td align="right"><%=formatter.format(dto.getOrg_client_balance())%></td>
                                </tr>                                    
                                <% }
                                } else {%>
                                <tr>
                                    <td colspan="4" align="center" style="color: brown">No Activity Found</td>
                                </tr>
                                <%}%>
                            </table>
                            <div class="clear"></div>
                            <% if (login_dto.getClient_level() != 2) {%>
                            <table class="reporting_table" cellpadding="0" cellspacing="0" export="false">
                                <tr>
                                    <th>Termination Client</th>
                                    <th>Total Successful Calls</th>
                                    <th>Total Call Duration</th>
                                    <th>Current Balance</th>
                                </tr>
                                <%
                                    ArrayList<HomeDTO> termlist = HomeLoader.getInstance().getTermDTOList(login_dto);
                                    if (termlist != null && termlist.size() > 0) {
                                        for (int i = 0; i < termlist.size(); i++) {
                                            HomeDTO dto = termlist.get(i);
                                %>  
                                <tr>
                                    <td align="left"><%=dto.getTerm_client_name()%></td>
                                    <td align="right"><%=dto.getTerm_calls()%></td>
                                    <td align="center"><%=Utils.getTimeMMSS(dto.getTerm_duration())%></td>
                                    <td align="right"><%=formatter.format(dto.getTerm_client_balance())%></td>
                                </tr>                                    
                                <% }
                                } else {%>
                                <tr>
                                    <td colspan="4" align="center" style="color: brown">No Activity Found</td>
                                </tr>
                                <%}%>
                            </table>
                            <%}%>
                            <div class="button_area">
                            </div>
                        </div>
                        <div class="blank-height"></div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>
