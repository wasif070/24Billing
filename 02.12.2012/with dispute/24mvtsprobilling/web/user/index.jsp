<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils" %>

<%
   int pageNo = 1;
   int sortingOrder = 0;
   int sortedItem = 0;
   int list_all = 0;
   int recordPerPage = 10;

   if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
     recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString());
   }
   if (request.getParameter("d-49216-p") != null) {
     boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
     if(status==true){
     pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
      }
   }
   if (request.getParameter("d-49216-s") != null) {
     sortedItem = Integer.parseInt(request.getParameter("d-49216-s"));
   }
   if (request.getParameter("d-49216-o") != null) {
     sortingOrder = Integer.parseInt(request.getParameter("d-49216-o"));
   }
   if (request.getParameter("list_all") != null) {
     list_all = Integer.parseInt(request.getParameter("list_all"));
   }
   
   String msg=(String)request.getSession(true).getAttribute(Constants.MESSAGE);
       if(msg==null){
           msg="";
       }else{
        msg="<div class='success'>"+msg+"</div>";
       }
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: User List</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "user");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <c:set var="editPermission" value="<%=String.valueOf(perms[com.myapp.struts.util.AppConstants.EDIT])%>"></c:set>
                <div class="right_content_view fl_right">             
                    <div class="pad_10 border_left">
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("user/listUser.do?list_all=1;User");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/user/listUser.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <th>User ID</th>
                                    <td><html:text property="userId" /></td>
                                    <th>Status</th>
                                    <td>
                                        <html:select property="userStatus">
                                            <html:option value="-1">Select</html:option>
                                            <%
                                             for(int i=0;i<Constants.USER_STATUS_VALUE.length;i++)
                                             {
                                            %>
                                            <html:option value="<%=Constants.USER_STATUS_VALUE[i]%>"><%=Constants.USER_STATUS_STRING[i]%></html:option>
                                            <%
                                             }
                                            %>
                                        </html:select>
                                    </td>
                                    <th>Record Per Page</th>
                                    <td>
                                        <html:text property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                    </td>
                                    <th>Go To Page No.</th>
                                    <td>
                                        <input type="text" name="d-49216-p" value="<%=pageNo%>" />
                                    </td>
                                    <td>
                                        <html:submit styleClass="search-button" value="Search" />
                                        <html:reset styleClass="search-button" value="Reset" />
                                    </td>
                                </tr>                                
                            </table>
                        </div>
                        <div class="over_flow_content display_tag_content" align="center">
                            <%=msg%>
                            <script type="text/javascript">count=<%=(pageNo-1)*recordPerPage%>;</script>
                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.UserForm.userList"  pagesize="<%=recordPerPage%>" >
                                <display:setProperty name="paging.banner.item_name" value="User" />
                                <display:setProperty name="paging.banner.items_name" value="Users" />
                                <display:column class="custom_column2" title="Nr" style="width:7%;" >
                                    <script type="text/javascript">
                                        document.write(++count+".");
                                    </script>
                                </display:column>
                                <display:column  title="User ID" sortProperty="userId" sortable="true"  style="width:15%" >
                                    ${data.userId}
                                </display:column>
                                <display:column property="fullName" class="left-align" title="Full Name" sortable="true" style="width:20%" />
                                <display:column property="userRoleName" class="left-align" title="Role" sortable="true" style="width:20%" />
                                <display:column property="userStatusName" class="custom_column1" title="Status" sortable="true" style="width:10%" />
                                <c:choose>
                                    <c:when test="${editPermission==1}">
                                        <display:column class="center-align" title="Task" style="width:10%;" >
                                            <a href="../user/getUser.do?id=${data.id}" class="edit" title="Change"></a>
                                            <c:choose>
                                                <c:when test="${data.id!=-1}"><a href="../user/deleteUser.do?id=${data.id}" onclick="javascript:return confirm('Are you sure to want delete the user?');" class="drop" title="Delete"></a></c:when>
                                            </c:choose>                                    
                                        </display:column>
                                    </c:when>
                                </c:choose>

                            </display:table>
                            <%
                                request.removeAttribute(Constants.USER_ID_LIST);
                                request.getSession(true).removeAttribute(Constants.MESSAGE);
                            %>
                            <input type="hidden" name="key" value="<%=String.valueOf(System.currentTimeMillis())%>" />
                        </div>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <%@include file="../includes/footer.jsp"%>
        </div>
    </body>
</html>