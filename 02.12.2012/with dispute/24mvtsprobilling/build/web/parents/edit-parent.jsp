<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue()%> :: Edit Parent</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "client");
                if (perms[com.myapp.struts.util.AppConstants.EDIT] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("parents/listParent.do?list_all=1;Parent");
                        navList.add(";Edit Parent");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/parents/editParent" method="post">                
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add User</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center" valign="bottom">
                                            <bean:write name="ParentForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Parent Name</th>
                                        <td valign="top">
                                            <html:text property="parent_name" /><br/>
                                            <html:messages id="parent_name" property="parent_name">
                                                <bean:write name="parent_name"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Desc</th>
                                        <td valign="top">
                                            <html:text property="parent_desc" /><br/>
                                            <html:messages id="parent_desc" property="parent_desc">
                                                <bean:write name="parent_desc"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                       
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <html:hidden property="id" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>