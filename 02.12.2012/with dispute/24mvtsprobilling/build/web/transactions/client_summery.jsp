<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <%@include file="../login/login-check.jsp"%>
        <%@page import="com.myapp.struts.session.Constants,com.myapp.struts.util.Utils,java.util.ArrayList,com.myapp.struts.transactions.TransactionDTO,java.text.DecimalFormat,java.text.NumberFormat" %>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Balance Summary</title>
        <%@include file="../includes/header.jsp"%>
        <%
            NumberFormat formatter = new DecimalFormat("#0.00");
            TransactionDTO bdto = (TransactionDTO) request.getSession(true).getAttribute("BalanceDTO");
        %>
    </head>
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "transaction");
                if (perms[com.myapp.struts.util.AppConstants.VIEW] == 1) {
            %>
            <div class="right_content_view fl_right">              
                <div class="pad_10 border_left">
                       <%
                    java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                    navList.add(";");
                    navList.add(";");
                %>
                <%= navigation.Navigation_client.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <div class="full-div center-align">
                        <table class="half-div float-center" border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td colspan="2"><h2>Welcome <%=login_dto.getClientId()%></h2></td>
                            </tr>
                            <tr style="color:#f00">
                                <th width="50%" align="right">Current Balance : </th><td align="left"><%=formatter.format(login_dto.getClient_balance())%></td>
                            </tr>
                            <% if (bdto != null) {%>
                            <tr>
                                <th width="50%" align="right">Total Received : </th><td align="left"><%=bdto.getTransaction_recharge()%></td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Total Returned : </th><td align="left"><%=bdto.getTransaction_return()%></td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Total Paid : </th><td align="left"><%=bdto.getTransaction_receive()%></td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Due : </th><td align="left"><%=bdto.getTransaction_balance()%></td>
                            </tr>
                            <% } else {%>
                            <tr>
                                <th width="50%" align="right">Total Received : </th><td align="left">0.00</td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Total Returned : </th><td align="left">0.00</td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Total Paid : </th><td align="left">0.00</td>
                            </tr>
                            <tr>
                                <th width="50%" align="right">Due : </th><td align="left">0.00</td>
                            </tr>
                            <% }%>
                        </table>
                    </div>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>