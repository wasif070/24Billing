<%@page import="com.myapp.struts.clients.ClientLoader"%>
<%@page import="com.myapp.struts.clients.ClientDTO"%>
<%@page import="com.myapp.struts.rateplan.RateplanDTO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.myapp.struts.rateplan.RateplanLoader"%>
<%@include file="../login/login-check.jsp"%>
<%
    long p_id = request.getParameter("parent") == null ? -1 : Integer.parseInt(request.getParameter("parent"));
    ArrayList<RateplanDTO> rateplanDto = RateplanLoader.getInstance().getRateplanDTOListByParentId(p_id);
    long rateplanid = 0;
    if (login_dto.getOwn_id() > 0) {
        ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
        if (client_dto != null) {
            rateplanid = client_dto.getRateplan_id();
        }
    }
%>
<option value="">Select Rate Plan</option>
<%
    String rateplan_name = "Not Found";
    if (rateplanDto != null && rateplanDto.size() > 0) {
        for (int i = 0; i < rateplanDto.size(); i++) {
            RateplanDTO rdto = new RateplanDTO();
            rdto = rateplanDto.get(i);
            rateplan_name=rdto.getRateplan_name();
            if (rateplanid == rdto.getRateplan_id()) {
                rateplan_name = "Base Rateplan";
            }
%>
<option value="<%=String.valueOf(rdto.getRateplan_id())%>"><%=rateplan_name%></option>
<%
        }
    }
%>