<%@page import="com.myapp.struts.util.AppConstants"%>
<%@page import="com.myapp.struts.settings.SettingsLoader"%>
<%@page import="com.myapp.struts.settings.SettingsDTO"%>
<style type="text/css">
    #browser {
        font-family: Verdana, helvetica, arial, sans-serif;
        font-size: 100%;
    }
</style>
<script type="text/javascript" >
    $j(document).ready(function(){        
        $j(".treeview").treeview({
            persist: "location",
            collapsed: true,
            unique: false,
            control: "#container"
        });
    });    	
</script>
<%@page import="com.myapp.struts.login.LoginDTO,com.myapp.struts.session.Constants" %>
<%  LoginDTO login_dto1 = null;
    if (request.getSession(true).getAttribute(Constants.LOGIN_DTO) != null) {
        login_dto1 = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
    }
%>
<div class="left-menu pad_10" id="browser">    
    <ul class="treeview">
        <%
            if (login_dto1.isUser() || login_dto1.getSuperUser() || login_dto1.getClient_level() > 1) {
        %>
        <li class="collapsable"><div class="hitarea collapsable-hitarea"></div><span><a href="../home/home.jsp">Home</a></span></li>
        <li><div class="hitarea expandable-hitarea"></div><span><a href="../clients/listClient.do?list_all=1">Client</a></span>
            <ul>
                <li><span ><a href="../clients/listClient.do?list_all=1" >View Client</a></span></li>
                <%if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("CLIENT_PROVISIONING").getSettingValue()) == AppConstants.YES) {%>
                <li><span ><a href="../clients/provision.jsp" >Client Provisioning</a></span></li>
                <%}%>
                <li><span ><a href="../clients/add-client.jsp" >Add Client</a></span></li>
                <li class="last"><span ><a href="../clients/listRechargeClient.do?list_all=1" >Recharge Client</a></span></li>
            </ul>
        </li> 
        <li><div class="hitarea expandable-hitarea"></div><span><a href="../capacitygroup/listCapacityGroup.do?list_all=1">Capacity Group</a></span>
            <ul>
                <li><span ><a href="../capacitygroup/listCapacityGroup.do?list_all=1" >View Capacity Groups</a></span></li>                                
                <li><span ><a href="../capacitygroup/add-capacityGroup.jsp" >Add Capacity Group</a></span></li>               
                <li><span ><a href="../capacitygroup/assign-capacityGroup.jsp">Assign Capacity Group</a></span></li>
                <li class="last"><span ><a href="../capacitygroup/listClientCapacityGroup.do?list_all=1" >Client Wise Capacity Group</a></span></li>               
            </ul>
        </li>         
        <li><div class="hitarea expandable-hitarea"></div><span><a href="../operators/listOperator.do?list_all=1">Operator</a></span>
            <ul>
                <li><span ><a href="../operators/listOperator.do?list_all=1" >View Operator</a></span></li>
                <li><span ><a href="../operators/new_operators.jsp" >Add Operator</a></span></li>
            </ul>
        </li>        
        <li><div class="hitarea expandable-hitarea"></div><span><a href="../parents/listParent.do?list_all=1" >Parent</a></span>
            <ul>
                <li><span ><a href="../parents/listParent.do?list_all=1" >View Parent</a></span></li>
                <li class="last"><span ><a href="../parents/add-parent.jsp" >Add Parent</a></span></li>
            </ul>
        </li> 
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../gateway/listGateway.do?list_all=1">Gateway</a></span>
            <ul>
                <li><span ><a href="../gateway/listGateway.do?list_all=1" >View Gateway</a></span></li>
                <li class="last"><span ><a href="../gateway/add-gateway.jsp" >Add Gateway</a></span></li>
            </ul>
        </li>
        <%
            SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
            if ((login_dto1.isUser() || login_dto1.getSuperUser() && login_dto1.getClient_level() < 0) && Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../dialplan/listDialplan.do?list_all=1">Dial Plan</a></span>
            <ul>
                <li><span ><a href="../dialplan/listDialplan.do?list_all=1" >View Dial Plan</a></span></li>
                <li class="last"><span ><a href="../dialplan/add-dialplan.jsp" >Add Dial Plan</a></span></li>
            </ul>
        </li>
        <%}%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../rateplan/listRateplan.do?list_all=1">Rate Plan</a></span>
            <ul>
                <li><span ><a href="../rateplan/listRateplan.do?list_all=1" >View Rate Plan</a></span></li>
                <li class="last"><span ><a href="../rateplan/new_rateplan.jsp" >Add Rate Plan</a></span></li>
            </ul>
        </li>
        <%if (login_dto1.getSuperUser() && !login_dto1.isUser() && login_dto1.getOwn_id() < 0) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../user/getUser.do?id=<%=login_dto1.getId()%>" >Edit Profile</a></span></li>         
        <%} else if (login_dto1.getClient_level() == 2) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../clients/getClientInfo.do">Edit Profile</a></span></li>
        <%}%>
        <%if (login_dto1.isUser()) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../user/listUser.do?list_all=1">User</a></span>         
            <ul>
                <li><span ><a href="../user/listUser.do?list_all=1" >View User</a></span></li>
                <li class="last"><span ><a href="../user/add-user.jsp" >Add User</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../ans/listAns.do?list_all=1">ANS</a></span>         
            <ul>
                <li><span ><a href="../ans/listAns.do?list_all=1" >View ANS</a></span></li>
                <li class="last"><span ><a href="../ans/add-ans.jsp" >Add ANS</a></span></li>
            </ul>
        </li>
        <%}%>
        <li>
            <div class="hitarea collapsable-hitarea"></div><span ><a href="../invoice/listInvoice.do?list_all=1">Invoice</a></span>
            <ul>
                <li><span ><a href="../invoice/listInvoice.do?list_all=1" >View Invoice</a></span></li>
                <li class="last"><span ><a href="../invoice/add-invoice.jsp">Add Invoice</a></span></li>
            </ul>
        </li>
        <% if (login_dto1.isUser()) {%>
        <li>
            <div class="hitarea collapsable-hitarea"></div><span ><a href="../role/listRoles.do">Role</a></span>
            <ul>
                <li><span ><a href="../role/listRoles.do" >View Role</a></span></li>
                <li class="last"><span ><a href="../role/add-role.jsp">Add Role</a></span></li>
            </ul>
        </li>
        <%}%>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../reports/listCdr.do?list_all=1">Report</a></span></a>
            <ul>
                <li><span ><a href="../reports/listCdr.do?list_all=1">Monitoring Report</a></span></li>
                <li><span ><a href="../reports/qualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li><span ><a href="../aggregatereports/monthlyReport.do?list_all=1">Monthly Report</a></span></li>                
                <li><span ><a href="../reports/listError.do?list_all=1">Error Report</a></span></li>        
                <li><span ><a href="../aggregatereports/deleteReport.do?list_all=1">Delete Report</a></span></li> 
                <li><span ><a href="../transactions/listTransaction.do?list_all=1">Payment History</a></span></li>
                <li class="last"><span ><a href="../transactions/transSummery.do?list_all=1">Payment Summary</a></span></li>
                <!--            <li><span ><a href="../igwreports/listOutgoingCDR.do?list_all=1">Int. Outgoing</a></span></li>
                            <li class="last"><span><a href="../igwreports/listCDR.do?list_all=1">Int. Incoming</a></span></li>        -->
            </ul>
        </li> 
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../dispute/dispute-summary.jsp">Dispute</a></span>
            <ul>                
                <li><span ><a href="../dispute/dispute-summary.jsp">Dispute Summary</a></span></li>
                <li><span ><a href="../dispute/date_wise_summary.do?list_all=1">Datewise Summary</a></span></li>
                <li><span ><a href="../dispute/carrier-column-mapping.jsp?url_msg=1">Column Mapping</a></span></li>        
            </ul>
        </li>         
        <%if (login_dto1.isUser()) {//only super admin %>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../settings/listSettings.do?list_all=1">Settings</a></span></li>
        <%}%>
        <% } else {%>
        <li class="expandable"><div class="hitarea collapsable-hitarea"></div><span><a href="../transactions/clientSummery.do">Home</a></span></li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../reports/listClientCdr.do?list_all=1">Report</a></span>
            <ul>                
                <li><span ><a href="../reports/listClientCdr.do?list_all=1">Monitoring Report</a></span></li>
                <li><span ><a href="../reports/cQualityCdr.do?list_all=1">Quality Report</a></span></li>
                <li class="last"><span ><a href="../transactions/clientTrans.do?list_all=1">Payment History</a></span></li>
            </ul>
        </li>
        <li><div class="hitarea collapsable-hitarea"></div><span ><a href="../clients/getClientInfo.do">Client</a></span>
            <ul>
                <li class="last"><span ><a href="../clients/getClientInfo.do">Edit Profile</a></span></li>
            </ul>
        </li>
        <li><span ><a href="../rates/clientRate.do">Rate Tariff</a></span></li>
        <% }%>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../currentcall/listCurrentcalls.do?list_all=1">Current call</a></span></li> 
        <% if (login_dto1.isUser()) {%>
        <li><div class="hitarea collapsable-hitarea"></div><span><a href="../activitylog/listActivities.do">Activities</a></span></li> 
        <%}%>        
        <li class="last"><span><a href="../login/logout.do">Logout</a></span></li>
    </ul>
    <div class="clear"></div>
    You are logged in as - <span class="blue bold">
        <div class="clear"></div>
        <%=login_dto1.getClientId()%>        
        <%
            if (login_dto1.isUser()) {
                out.print("(super user)");
            } else if (login_dto1.getClient_level() == 2) {
                out.print("(reseller)");
            } else if (login_dto1.getClient_level() == 1) {
                out.print("(client)");
            } else {
                out.print("(general user)");
            }
        %>        
    </span>    
</div>