<%@page import="role.RoleDTO"%>
<%@page import="role.RoleLoader"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@include file="../login/login-check.jsp"%>
<%@page import="com.myapp.struts.capacitygroup.CapacityGroupLoader,com.myapp.struts.capacitygroup.CapacityGroupDTO,com.myapp.struts.session.Constants,java.util.ArrayList" %>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title><%=SettingsLoader.getInstance().getSettingsDTO("PAGE_TITLE").getSettingValue() %> :: Add Capacity Group</title>
        <%@include file="../includes/header.jsp"%>
    </head>    
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>
            <%
                int[] perms = role.RoleLoader.getInstance().getPermissions(login_dto.getRole_id(), "CapacityGroup");
                if (perms[com.myapp.struts.util.AppConstants.ADD] == 1) {
            %>
            <div class="right_content_view fl_right">               
                <div class="pad_10 border_left">
                    <%
                        java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                        navList.add("/capacitygroup/listCapacityGroup.do?list_all=1;CapacityGroup");
                        navList.add(";Add Capacity Group");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    <html:form action="/capacitygroup/addCapacityGroup" method="post">                
                        <fieldset style="width: 50%; margin: 0 auto;"><legend class="legnd-text-color">Add Capacity Group</legend>
                            <table class="input_table" cellspacing="0" cellpadding="0" >
                                <tbody>
                                    <tr>
                                        <td colspan="2" align="center" valign="bottom">
                                            <bean:write name="CapacityGroupForm" property="message" filter="false"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Capacity Group Name <span class="req_mark">*</span></th>
                                        <td valign="top">
                                            <html:text property="capacitygrpName" /><br/>
                                            <html:messages id="capacitygrpName" property="capacitygrpName">
                                                <bean:write name="capacitygrpName"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Capacity Group Description</th>
                                        <td valign="top">
                                            <html:text property="capacitygrpDesc" /><br/>
                                            <html:messages id="capacitygrpDesc" property="capacitygrpDesc">
                                                <bean:write name="capacitygrpDesc"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th valign="top">Capacity Group Capacity</th>
                                        <td valign="top">
                                            <html:text property="capacitygrpCapacity" /><br/>
                                            <html:messages id="capacitygrpCapacity" property="capacitygrpCapacity">
                                                <bean:write name="capacitygrpCapacity"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                    
                                    
                                    <tr>
                                        <th valign="top">Capacity Group Parent</th>
                                        <td valign="top">                                            
                                            <html:select property="capacitygrpParentGrp">
                                                <html:option value="0">--Select--</html:option>
                                                <%
                                                    ArrayList<CapacityGroupDTO> list = CapacityGroupLoader.getInstance().getCapacityGrpDTOList();
                                                    for (CapacityGroupDTO capacitygrpDTO : list) {
                                                %>
                                                <html:option value="<%=String.valueOf(capacitygrpDTO.getGroup_id())%>"><%=capacitygrpDTO.getGroup_name()%></html:option>
                                                <%
                                                    }
                                                %>
                                            </html:select><br/>
                                            <html:messages id="capacitygrpParentGrp" property="capacitygrpParentGrp">
                                                <bean:write name="capacitygrpParentGrp"  filter="false"/>
                                            </html:messages>
                                        </td>
                                    </tr>                                            
                                            
                                    <tr>
                                        <th>&nbsp;</th>
                                        <td>
                                            <input type="hidden" name="searchLink" value="nai" />
                                            <html:hidden property="doValidate" value="<%=String.valueOf(Constants.CHECK_VALIDATION)%>" />
                                            <input name="submit" type="submit" class="custom-button" value="Add" />
                                            <input type="reset" class="custom-button" value="Reset" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </fieldset>
                        <div class="blank-height"></div>
                    </html:form>
                </div>
            </div>
            <%} else {%>
            <h4 class="red center-align"><%=perms[com.myapp.struts.util.AppConstants.INACTIVE] == 1 ? com.myapp.struts.session.Constants.INACTIVE_ROLE : com.myapp.struts.session.Constants.ACCESS_INFORMATION_MESSAGE%></h4>
            <%}%>
            <div class="clear"></div>        
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>