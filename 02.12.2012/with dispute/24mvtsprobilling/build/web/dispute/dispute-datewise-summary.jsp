<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%-- 
    Document   : dispute-datewise-summary
    Created on : Sep 2, 2012, 11:27:19 AM
    Author     : reefat
--%>

<%@include file="../login/login-check.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.myapp.struts.session.Constants,java.util.ArrayList,com.myapp.struts.util.Utils,com.myapp.struts.clients.ClientDTO,com.myapp.struts.clients.ClientLoader,com.myapp.struts.util.Utils,java.text.NumberFormat,java.text.DecimalFormat,java.util.Enumeration,com.myapp.struts.dispute.DisputeDTO,com.myapp.struts.dispute.DisputeLoader" %>

<%
            int pageNo = 1;
            int recordPerPage = 10;

            if (request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE) != null) {
                recordPerPage = Integer.parseInt(request.getSession(true).getAttribute(Constants.USER_RECORD_PER_PAGE).toString());
            }
            if (request.getParameter("d-49216-p") != null) {
               boolean status=Utils.IntegerValidation(request.getParameter("d-49216-p"));  
                if(status==true){
                  pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
                }
             }

            String msg = (String) request.getSession(true).getAttribute(Constants.MESSAGE);
            if (msg == null) {
                msg = "";
            } 
%>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>24Billing :: Date wise Summary</title>
        <%@include file="../includes/header.jsp"%>
    </head>
    <%
        ArrayList<Integer> days = Utils.getDay();
        ArrayList<String> months = Utils.getMonth();
        ArrayList<Integer> years = Utils.getYear();
        NumberFormat formatter = new DecimalFormat("00");
    %>     
    <body>
        <div class="main_body">
            <div><%@include file="../includes/logo.jsp"%></div>
            <div class="left_menu fl_left">
                <div><%@include file="../includes/left_menu.jsp"%></div>
            </div>

            <div class="right_content_view fl_right border_left">               
                <div class="pad_10 ">                                       
                    <%
                 java.util.ArrayList<String> navList = new java.util.ArrayList<String>();
                 navList.add("dispute/dispute-summary.jsp;Dispute");                 
                 navList.add(";Date Wise summary");
                    %>
                    <%= navigation.Navigation.getNavigationStr(navList, request.getSession(true).getAttribute("BASE_URL").toString())%>
                    
                <html:form action="/dispute/date_wise_summary.do" method="post" >
                        <div class="full-div">
                            <table class="search-table" border="0" cellpadding="0" cellspacing="0"  >
                                <tr>
                                <th>From Date</th>
                                <td colspan="3" class="selopt">
                                    <html:select property="fromYear_dateWise" styleClass="" styleId="fromYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromMonth_dateWise" styleClass="month" styleId="fromMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="fromDay_dateWise" styleClass="">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                                String temp = formatter.format((i + 1));
                                        %>
                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>To Date</th>
                                <td colspan="3" class="selopt">
                                    <html:select property="toYear_dateWise" styleClass="" styleId="fromYear">
                                        <%
                                            for (int i = 0; i < years.size(); i++) {
                                                String year = String.valueOf(years.get(i));
                                        %>
                                        <html:option value="<%=year%>"><%=year%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toMonth_dateWise" styleClass="month" styleId="fromMonth">
                                        <%
                                            for (int i = 0; i < months.size(); i++) {
                                                String month = months.get(i);
                                                String increment = String.valueOf(i + 1);
                                        %>
                                        <html:option value="<%=increment%>"><%=month%></html:option>
                                        <%}%>
                                    </html:select>
                                    <html:select property="toDay_dateWise" styleClass="">
                                        <%
                                            for (int i = 0; i < days.size(); i++) {
                                                String increment = String.valueOf(i + 1);
                                                String temp = formatter.format((i + 1));
                                        %>
                                        <html:option value="<%=increment%>"><%=temp%></html:option>
                                        <%}%>
                                    </html:select>
                                </td>
                                <th>Carrier</th>
                                <td>
                                    <html:select property="carrier_id_datewise_summary" styleId="parentid" >
                                        <html:option value="-1">--Select--</html:option>
                                        <%for (DisputeDTO ds_dto : DisputeLoader.getInstance().getDisputeDTOCarrierList()) {%>
                                        <html:option value="<%=String.valueOf(ds_dto.getID())%>"><%=ds_dto.getClientID()%></html:option>
                                        <%}%>
                                    </html:select>                                        
                                </td> 
                                <th>Record Per Page</th>
                                <td>
                                    <html:text style="width:50px" property="recordPerPage" value="<%=String.valueOf(recordPerPage)%>"/>
                                </td>
                                <th>Go To Page No.</th>
                                <td>
                                    <input style="width:50px" type="text" name="d-49216-p" value="<%=pageNo%>" />
                                </td>                                
                                </tr>
                                <tr>
                                    <td align="center" colspan="60">
                                    <html:submit styleClass="search-button" value="Search" />
                                    <html:reset styleClass="search-button" value="Reset" />
                                </td>
                                </tr>
                            </table>
                        </div>
                    </html:form>

                    <html:form action="/dispute/hour_wise_summary" method="post">
                        <div class="display_tag_content" align="center">
                            <%=msg%>
                            <div class="jerror_messge"></div>
                            <script type="text/javascript">count=<%=(pageNo - 1) * recordPerPage%>;</script>

                            <display:table class="reporting_table" cellpadding="0" cellspacing="0" export="false" id="data" name="sessionScope.DisputeForm.disputeDTOs_datewise_summary"  pagesize="<%=recordPerPage%>">
                                <display:setProperty name="paging.banner.item_name" value="daywisesummary" /> 
                                <display:setProperty name="paging.banner.items_name" value="daywisesummary_s" />
                                <display:column class="custom_column1" title="<input type='checkbox' name='allbox' class='jsel_all' title='select dates' />" style="width:3%">
                                    <input type="checkbox" name="selectedIDs[]" value="${data.date_datewise_summary}" class="select_id"/>
                                </display:column>                            
                                <display:column  property="date_datewise_summary" class="center-align" title="Date" sortable="true" style="width:10%" />
                                <display:column  property="total_calls_ipvision_datewise_summary" class="center-align" title="Total Calls(VisionTel)" sortable="true" style="width:10%" />
                                <display:column property="total_duration_ipvision_datewise_summary" class="center-align" title="Total Duration(VisionTel)" sortable="true" style="width:10%"></display:column>
                                <display:column  property="total_calls_carrier_datewise_summary" class="center-align" title="Total Calls(carrier)" sortable="true" style="width:10%" />
                                <display:column property="total_duration_carrier_datewise_summary" class="center-align" title="Total Duration(carrier)" sortable="true" style="width:10%"></display:column>
                                <display:column  property="deviation_datewise_summary" class="center-align" title="Deviation(percentage)" sortable="true" style="width:10%" />
                            </display:table>    
                            
                            <div class="button_area">                                
                                <html:submit property="HourlyBtn" styleClass="custom-button jmultipebtn" value="Hourly Summary" />
                            </div>    
                            
                                                 
                        </div>
                        <div class="blank-height"></div>                                      
                    </html:form>                                   
                </div>
                <div class="clear"></div>
            </div>

            <div class="clear"></div>
            <div><%@include file="../includes/footer.jsp"%></div>
        </div>
    </body>
</html>