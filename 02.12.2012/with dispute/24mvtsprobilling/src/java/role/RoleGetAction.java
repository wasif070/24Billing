/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class RoleGetAction extends Action {

    static Logger logger = Logger.getLogger(RoleGetAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("RoleGetAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            RoleTaskScheduler scheduler = new RoleTaskScheduler();
            RoleForm roleForm = (RoleForm) form;

            /*--------------------For geting searching parameter-------------------*/
            RoleDTO roleDTO = new RoleDTO();
            roleDTO = scheduler.getRoleDTO(Long.parseLong(request.getParameter("cid")));
            request.getSession(true).setAttribute("ROLEID", roleDTO.getId());

            roleForm.setId(roleDTO.getId());
            roleForm.setRoleName(roleDTO.getRoleName());
            roleForm.setStatus(roleDTO.getStatus());
            roleForm.setRoleDesc(roleDTO.getRoleDesc());
            roleForm.setEditId(roleDTO.getEditId());

            request.getSession(true).setAttribute(mapping.getAttribute(), roleForm);

        } else {
            return (new ActionForward("/login/logout.do", true)); 
        }
        return (mapping.findForward(target));
    }
}
