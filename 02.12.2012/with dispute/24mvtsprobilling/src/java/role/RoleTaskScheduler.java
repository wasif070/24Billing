/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.AppError;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class RoleTaskScheduler {

    public RoleTaskScheduler() {
    }

    public ArrayList<RoleDTO> getRoleDTOs() {
        return RoleLoader.getInstance().getRoleDTOs();
    }

    public ArrayList<RoleDTO> getSearchedRoleDTOs(RoleDTO p_dto) {
        return RoleLoader.getInstance().getRoleDTOsWithSearchParam(p_dto);
    }

    public RoleDTO getRoleDTO(long id) {
        return RoleLoader.getInstance().getRoleDTO(id);
    }

    public AppError editRoleInfo(RoleDTO p_dto) {
        RoleDBFunction dbFunction = new RoleDBFunction();
        AppError error = dbFunction.editRoleInformation(p_dto);
        if (error.getErrorType() == error.NO_ERROR) {
            RoleLoader.getInstance().forceReload();

        }
        return error;
    }

    public AppError addRoleInfo(RoleDTO p_dto) {
        RoleDBFunction dbFunction = new RoleDBFunction();
        AppError error = dbFunction.addRoleInformation(p_dto);
        if (error.getErrorType() == error.NO_ERROR) {
            RoleLoader.getInstance().forceReload();
        }
        return error;
    }

    public AppError deleteRoleInfo(String ids) {
        RoleDBFunction dbFunction = new RoleDBFunction();
        AppError error = dbFunction.deleteRoleInfo(ids);
        if (error.getErrorType() == error.NO_ERROR) {
            UserLoader.getInstance().forceReload();
            RoleLoader.getInstance().forceReload();
        }
        return error;
    }
}
