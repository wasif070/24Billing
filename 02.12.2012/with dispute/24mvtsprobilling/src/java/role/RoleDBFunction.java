/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

/**
 *
 * @author Administrator
 */
import com.myapp.struts.util.AppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.HashMap;

public class RoleDBFunction {

    static Logger logger = Logger.getLogger(RoleDBFunction.class.getName());

    public AppError editRoleInformation(RoleDTO p_dto) {
        ArrayList<PermissionDTO> permissionDTOs = p_dto.getPermissionDTOs();
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select role_name from roles where role_name='" + p_dto.getRoleName() + "' and id!=" + p_dto.getId();
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setErrorType(error.OTHERS_ERROR);
                error.setErrorMessage("Duplicate Role Name");
                throw new Exception("Duplicate Role Name");
            }

            sql = "update roles set role_name=?,status=?,role_desc=? ";
            sql += "where id=" + p_dto.getId();
            logger.debug("edit role sql: " + sql);
            ps = db.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRoleName());
            ps.setInt(2, p_dto.getStatus());
            ps.setString(3, p_dto.getRoleDesc());
            if (ps.executeUpdate() < 1) {
                error.setErrorType(error.OTHERS_ERROR);
                error.setErrorMessage("Role information did not edited. Please try again.");
            }

            for (int i = 0; i < permissionDTOs.size(); i++) {
                PermissionDTO dto = permissionDTOs.get(i);
                sql = "update page_permissions set permissions=? ";
                sql += "where page_id=" + dto.getPageId() + " and role_id=" + p_dto.getId();
                logger.debug("edit role sql1: " + sql);
                ps = db.connection.prepareStatement(sql);
                ps.setInt(1, dto.getPermissions());
                int updateCount = ps.executeUpdate();
                if (updateCount < 1) {
                    sql = "insert into page_permissions(permissions,page_id,role_id) ";
                    sql += "values(?,?,?)";
                    logger.debug("edit role sql2: " + sql);
                    ps = db.connection.prepareStatement(sql);
                    ps.setInt(1, dto.getPermissions());
                    ps.setInt(2, dto.getPageId());
                    ps.setLong(3, dto.getId());
                    ps.executeUpdate();
                    ps.close();
                }
                ps.close();
            }

        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during update users info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public AppError addRoleInformation(RoleDTO p_dto) {
        ArrayList<PermissionDTO> permissionDTOs = p_dto.getPermissionDTOs();
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = ("select role_name from roles where role_name='" + p_dto.getRoleName()) + "' ";
            rs = stmt.executeQuery(sql);
            if (rs.next()) {
                error.setErrorType(error.OTHERS_ERROR);
                error.setErrorMessage("Duplicate Role Name");
                throw new Exception("Duplicate Role Name");
            }

            sql = "insert into roles (role_name, status,role_desc) values(?,?,?)";
            ps = db.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRoleName());
            ps.setInt(2, p_dto.getStatus());
            ps.setString(3, p_dto.getRoleDesc());

            if (ps.executeUpdate() > 0) {
                sql = "select id from roles where role_name='" + p_dto.getRoleName() + "' ";
                rs = stmt.executeQuery(sql);
                int role_id = 0;
                if (rs.next()) {
                    role_id = rs.getInt("id");
                }

                PermissionDTO dto = new PermissionDTO();
                for (int i = 0; i < permissionDTOs.size(); i++) {
                    sql = "insert into page_permissions (permissions,page_id,role_id) ";
                    sql += "values(?,?,?)";
                    logger.debug("add role sql1: " + sql);
                    ps = db.connection.prepareStatement(sql);
                    dto = permissionDTOs.get(i);
                    ps.setInt(1, dto.getPermissions());
                    ps.setInt(2, dto.getPageId());
                    ps.setInt(3, role_id);
                    ps.executeUpdate();
                    ps.close();
                }
            }
        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during add role info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }

        return error;
    }

    public static ArrayList<PageDTO> getPages() {
        ArrayList<PageDTO> data = new ArrayList<PageDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        PageDTO pageDTO = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = "select * from pages order by id ASC";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                pageDTO = new PageDTO();
                pageDTO.setId(rs.getInt("id"));
                pageDTO.setPageName(rs.getString("page_name"));
                pageDTO.setPageUri(rs.getString("page_uri"));
                data.add(pageDTO);
            }

        } catch (Exception e) {
            logger.debug("Exception during getting role info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }

        return data;
    }

    public static HashMap<Integer, PageDTO> getPagesMap() {
        HashMap<Integer, PageDTO> map = new HashMap<Integer, PageDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        PageDTO pageDTO = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = ("select * from pages order by id ASC");
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                pageDTO = new PageDTO();
                pageDTO.setId(rs.getInt("id"));
                pageDTO.setPageName(rs.getString("page_name"));
                map.put(pageDTO.getId(), pageDTO);
            }

        } catch (Exception e) {
            logger.debug("Exception during getting pages info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }

        return map;
    }

    public static ArrayList<RoleDTO> getRoles() {
        ArrayList<RoleDTO> data = new ArrayList<RoleDTO>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        RoleDTO roleDTO = null;
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            stmt = db.connection.createStatement();
            String sql = ("select * from roles order by id ASC");
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                roleDTO = new RoleDTO();
                roleDTO.setId(rs.getInt("id"));
                roleDTO.setRoleName(rs.getString("role_name"));
                data.add(roleDTO);
            }

        } catch (Exception e) {
            logger.debug("Exception during getting role info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return data;
    }

    public AppError deleteRoleInfo(String ids) {
        AppError error = new AppError();

        DBConnection db = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {

            db = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "update roles set is_deleted=1 where id in (" + ids + ") and id>1";
            logger.debug("delete roles sql: " + sql);
            ps = db.connection.prepareStatement(sql);
            if (ps.executeUpdate() < 1) {
                error.setErrorType(error.OTHERS_ERROR);
                error.setErrorMessage("No Role deleted for internal error");
            }

        } catch (Exception e) {
            error.setErrorType(error.DB_ERROR);
            error.setErrorMessage(e.toString().substring(e.toString().lastIndexOf(":") + 1));
            logger.debug("Exception during delete roles info-->" + e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public static HashMap<Long, HashMap<String, Integer>> getPermissionMap() {
        HashMap<Long, HashMap<String, Integer>> map = new HashMap<Long, HashMap<String, Integer>>();
        DBConnection db = null;
        Statement stmt = null;
        ResultSet rs = null;
        PermissionDTO permissionDTO = new PermissionDTO();
        try {
            db = databaseconnector.DBConnector.getInstance().makeConnection();
            ArrayList<PermissionDTO> list = new ArrayList<PermissionDTO>();
            stmt = db.connection.createStatement();
            String sql = "select * from page_permissions,pages where pages.id=page_permissions.page_id ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                permissionDTO = new PermissionDTO();
                permissionDTO.setPermissions(rs.getInt("permissions"));
                permissionDTO.setId(rs.getInt("role_id"));
                permissionDTO.setPageId(rs.getInt("page_id"));
                permissionDTO.setPageUri(rs.getString("page_uri"));
                list.add(permissionDTO);
            }
            int listSize = list.size();
            

            HashMap<String, Integer> permissionMap = new HashMap<String, Integer>();
            stmt = db.connection.createStatement();
            sql = "select id from roles where is_deleted=0 ";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                permissionMap = new HashMap<String, Integer>();
                for (int i = 0; i < listSize; i++) {
                    PermissionDTO dto = new PermissionDTO();
                    dto = list.get(i);
                    if (dto.getId() == rs.getInt("id")) {
                        permissionMap.put(dto.getPageUri(), dto.getPermissions());
                    }
                }
                map.put(rs.getLong("id"), permissionMap);
            }
        } catch (Exception e) {
            logger.fatal("Exception in permission map" + e);            
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                logger.fatal("Exception in closing resultset--> " + e);
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
                logger.fatal("Exception in closing statement--> " + e);
            }
            try {
                if (db.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(db);

                }
            } catch (Exception e) {
                logger.fatal("Exception in closing connection--> " + e);
            }
        }
        return map;
    }
}
