/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import activitylog.ActivityDTO;
import activitylog.ActivityLog;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.AppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class RoleDeleteAction extends Action {

    static Logger logger = Logger.getLogger(RoleDeleteAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("RoleDeleteAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            RoleTaskScheduler scheduler = new RoleTaskScheduler();
            RoleForm roleForm = (RoleForm) form;
            ActivityDTO ac_dto = new ActivityDTO();
            Gson json = new GsonBuilder().serializeNulls().create();
            ArrayList<ActivityDTO> data = new ArrayList<ActivityDTO>();

            /*--------------------For geting searching parameter-------------------*/
            String ids = "";
            if (roleForm.getIds() == null || roleForm.getIds().length == 0) {
                target = AppConstants.FAILURE;
                request.getSession(true).setAttribute(AppConstants.USER_SUCCESS_MESSAGE, "Please select at least one role!");
                return (mapping.findForward(target));
            }
            for (long id : roleForm.getIds()) {
                ac_dto = new ActivityDTO();
                ac_dto.setPreviousValue(json.toJson(RoleLoader.getInstance().getRoleDTO(id)));
                ac_dto.setId(id);
                data.add(ac_dto);
                ids += "," + id;
            }

            AppError error = scheduler.deleteRoleInfo(ids.substring(1));
            if (error.errorType == error.NO_ERROR) {             
                target = AppConstants.SUCCESS;
                request.getSession(true).setAttribute(AppConstants.ROLE_SUCCESS_MESSAGE, "Selected roles has been deleted successfully.");
            } else if (error.errorType == error.DB_ERROR) {
                target = AppConstants.FAILURE;
                roleForm.setMessage(true, error.getErrorMessage());
            } else if (error.errorType == error.OTHERS_ERROR) {
                roleForm.setMessage(true, error.getErrorMessage());
            }
        } else {
            return (new ActionForward("/login/logout.do", true));
        }
        return (mapping.findForward(target));
    }
}
