/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package role;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Administrator
 */
public class RoleForm extends ActionForm {

    public RoleForm() {        
        status = 1;
    }
    private long id;
    private long[] ids;
    private String roleName;
    private int pageId;
    private int add;
    private int edit;
    private int delete;
    private int view;
    private int status;
    private String roleDesc;
    private ArrayList roleDTOs;
    private long editId;
    private int[] pageIds;
    private int[] adds;
    private int[] edits;
    private int[] deletes;
    private int[] views;

    public int[] getPageIds() {
        return pageIds;
    }

    public void setPageIds(int[] pageIds) {
        this.pageIds = pageIds;
    }

    public int[] getAdds() {
        return adds;
    }

    public void setAdds(int[] adds) {
        this.adds = adds;
    }

    public int[] getDeletes() {
        return deletes;
    }

    public void setDeletes(int[] deletes) {
        this.deletes = deletes;
    }

    public int[] getEdits() {
        return edits;
    }

    public void setEdits(int[] edits) {
        this.edits = edits;
    }

    public int[] getViews() {
        return views;
    }

    public void setViews(int[] views) {
        this.views = views;
    }

    public int getAdd() {
        return add;
    }

    public void setAdd(int add) {
        this.add = add;
    }

    public int getDelete() {
        return delete;
    }

    public void setDelete(int delete) {
        this.delete = delete;
    }

    public int getEdit() {
        return edit;
    }

    public void setEdit(int edit) {
        this.edit = edit;
    }

    public long getEditId() {
        return editId;
    }

    public void setEditId(long editId) {
        this.editId = editId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long[] getIds() {
        return ids;
    }

    public void setIds(long[] ids) {
        this.ids = ids;
    }

    public int getPageId() {
        return pageId;
    }

    public void setPageId(int pageId) {
        this.pageId = pageId;
    }

    public ArrayList getRoleDTOs() {
        return roleDTOs;
    }

    public void setRoleDTOs(ArrayList roleDTOs) {
        this.roleDTOs = roleDTOs;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getRoleDesc() {
        return roleDesc;
    }

    public void setRoleDesc(String roleDesc) {
        this.roleDesc = roleDesc;
    }
    
    /*------COMMON FOR EACH FORM-----------------*/    
    private String doSearch;
    private String message;

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue'>" + message + "</span>";
        }
    }

    public String getMessage() {
        return message;
    }
    /*----------------END-----------------*/

    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        //boolean isList = mapping.getPath().contains("listRoles");
        boolean isAdd = mapping.getPath().contains("addInfo");
        boolean isEdit = mapping.getPath().contains("editInfo");

        if (isAdd || isEdit) {
            if (getRoleName() == null || getRoleName().length() < 1) {
                errors.add("roleName", new ActionMessage("errors.role_name.required"));
            }
        }
        return errors;
    }
}
