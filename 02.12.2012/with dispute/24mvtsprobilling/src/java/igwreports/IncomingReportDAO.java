/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package igwreports;

import ans.AnsLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.util.Utils;
import com.myapp.struts.settings.SettingsLoader;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class IncomingReportDAO {

    long THIRTY_DAYS = 30 * 24 * 60 * 60 * 1000L;
    static Logger logger = Logger.getLogger(IncomingReportDAO.class.getName());
    SettingsDTO settingp_dto = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingp_dto.getSettingValue();
    String curdate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis())) - Utils.getTimeLong(timeZone) - THIRTY_DAYS);
    String now = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));

    public IncomingReportDAO() {
    }

    public ArrayList<IGWReportDTO> getDTOs(IGWReportDTO p_dto, int start, int end, LoginDTO login_dto, String group_by) {
        HashMap<String, IGWReportDTO> TDMMap = new HashMap<String, IGWReportDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = " duration>0 and (INSTR(terminated_no,'88')=1 or INSTR(terminated_no,'0088')=1) and client_level=1 ";
        String sum_condition = condition;
//        if (login_dto.getOwn_id() < 0) {
//            condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
//        } else {
//            condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
//        }

        if (p_dto != null) {
            if (p_dto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + p_dto.getOrigin_client_id();
            }
            if (p_dto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + p_dto.getTerm_client_id();
            }
            if (p_dto.getAns_prefix().trim().length() > 0) {
                condition += " and ans_prefix='" + p_dto.getAns_prefix() + "' ";
            }
            if (p_dto.getFrom_date() != null) {
                condition += " and  connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(p_dto.getFrom_date()) - Utils.getTimeLong(timeZone)) + "'";
                sum_condition += " and  connection_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(p_dto.getFrom_date()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + curdate + "' ";
                sum_condition += " and connection_time >= '" + curdate + "' ";
            }

            if (p_dto.getTo_date() != null) {
                condition += " and  connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(p_dto.getTo_date()) - Utils.getTimeLong(timeZone)) + "'";
                sum_condition += " and  connection_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(p_dto.getTo_date()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and connection_time >= '" + now + "' ";
                sum_condition += " and connection_time >= '" + now + "' ";
            }
        } else {
            condition += " and connection_time >= '" + curdate + "' and connection_time <= '" + now + "' ";
            sum_condition += " and connection_time >= '" + curdate + "' and connection_time <= '" + now + "' ";
        }

        ArrayList<IGWReportDTO> list = new ArrayList<IGWReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String groupBy = group_by.length() > 0 ? (group_by.equals("connection_time") ? " group by date_pattern " : " group by " + group_by) : "";

            String sql = "select sum(term_paid_duration) as total_tdm_calls_duration,count(cdr_id) as total_tdm_calls,DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern from flamma_cdr where origin_ip='' and " + condition + groupBy + " order by cdr_id desc limit " + start + "," + end;
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                IGWReportDTO igwdto = new IGWReportDTO();
                igwdto.setTdm_calls_paid_minutes(resultSet.getInt("total_tdm_calls_duration"));
                igwdto.setNo_of_tdm_calls(resultSet.getInt("total_tdm_calls"));
                igwdto.setCdr_date(resultSet.getString("date_pattern"));
                TDMMap.put(igwdto.getCdr_date(), igwdto);
            }

            sql = "select count(cdr_id) as total_cdrs,sum(term_paid_duration) as p_duration,DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern from flamma_cdr where 1 and " + sum_condition + groupBy;
            logger.debug("total cdr list-->" + sql);
            resultSet = statement.executeQuery(sql);
            int total_rows = 0;
            int total_calls = 0;
            int total_duration = 0;
            while (resultSet.next()) {
                total_rows++;
                total_calls += resultSet.getInt("total_cdrs");
                total_duration += resultSet.getInt("p_duration");
            }

            sql = "select sum(term_paid_duration) as total_ip_calls_duration,count(cdr_id) as total_ip_calls,DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern,origin_client_name,term_client_name,term_rate,origin_rate,ans_prefix from flamma_cdr where origin_ip!='' and " + condition + groupBy + " order by cdr_id desc limit " + start + "," + end;
            logger.debug("all incoming cdr list-->" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                IGWReportDTO dto = new IGWReportDTO();
                dto.setIp_calls_paid_minutes(resultSet.getInt("total_ip_calls_duration"));
                dto.setNo_of_ip_calls(resultSet.getInt("total_ip_calls"));
                dto.setCdr_date(resultSet.getString("date_pattern"));
                try {
                    dto.setAns_name(AnsLoader.getInstance().getAnsNameByPrefix(resultSet.getString("ans_prefix")));
                } catch (Exception ex) {
                    dto.setAns_name(resultSet.getString("ans_prefix"));
                }
                dto.setTdm_calls_paid_minutes(0);
                dto.setNo_of_tdm_calls(0);
                try {
                    IGWReportDTO IGWDTO = TDMMap.get(dto.getCdr_date());
                    dto.setTdm_calls_paid_minutes(IGWDTO.getTdm_calls_paid_minutes());
                    dto.setNo_of_tdm_calls(IGWDTO.getNo_of_tdm_calls());
                } catch (Exception ex) {
                    logger.debug("No TDM call found at the day " + dto.getCdr_date());
                }
                dto.setTotal_calls(dto.getNo_of_ip_calls() + dto.getNo_of_tdm_calls());
                dto.setTotal_minutes(dto.getIp_calls_paid_minutes() + dto.getTdm_calls_paid_minutes());
                dto.setPercentage_of_calls(0);
                dto.setPercentage_of_minutes(0);
                if (total_calls > 0) {
                    dto.setPercentage_of_calls((double) (dto.getTotal_calls() * 100) / total_calls);
                    dto.setPercentage_of_minutes((double) (dto.getTotal_minutes() * 100) / total_duration);
                }
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                list.add(dto);
            }


            IGWReportDTO dto = new IGWReportDTO();
            dto.setTotal_cdr(total_rows);
            list.add(dto);

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in IncomingReportDAO:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }
}
