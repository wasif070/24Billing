/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package igwreports;

import java.util.ArrayList;

/**
 *
 * @author Ashraful
 */
public class IGWReportDTO {

    public IGWReportDTO() {
    }
    private int id;
    private String date;
    private long time;
    private int no_of_ip_calls;
    private int no_of_tdm_calls;
    private int ip_calls_paid_minutes;
    private String ip_calls_paid_minutes_str;
    private int tdm_calls_paid_minutes;
    private String tdm_calls_paid_minutes_str;
    private int total_calls;
    private int total_minutes;
    private String total_minutes_str;
    private long cdr_id;
    private String cdr_date;
    private String dialed_no;
    private String origin_caller;
    private int origin_client_id;
    private String origin_client_name;
    private String origin_ip;
    private String origin_prefix;
    private String origin_destination;
    private int origin_rate_id;
    private String origin_rate_des;
    private double origin_rate;
    private String origin_rate_str;
    private double origin_bill_amount;
    private String origin_bill_amount_str;
    private String terminated_no;
    private String term_caller;
    private int term_client_id;
    private String term_client_name;
    private String term_ip;
    private String term_prefix;
    private String term_destination;
    private int term_rate_id;
    private String term_rate_des;
    private double term_rate;
    private String term_rate_str;
    private double term_bill_amount;
    private String term_bill_amount_str;
    private int duration;
    private String connection_time;
    private double pdd;
    private String pdd_str;
    private String disconnect_desc;
    private String conf_id;
    private ArrayList<IGWReportDTO> dtoList;
    private long total_duration;
    private double total_origin_bill_amount;
    private double total_term_bill_amount;
    private String from_date;
    private String to_date;
    private int total_cdr;
    private double percentage_of_calls;
    private double percentage_of_minutes;
    private int reporting_type;
    private String origin_route;
    private String term_route;
    private String ans_prefix;
    private String ans_name;
    private int icx;
    private String icx_name;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIp_calls_paid_minutes() {
        return ip_calls_paid_minutes;
    }

    public void setIp_calls_paid_minutes(int ip_calls_paid_minutes) {
        this.ip_calls_paid_minutes = ip_calls_paid_minutes;
    }

    public String getIp_calls_paid_minutes_str() {
        return ip_calls_paid_minutes_str;
    }

    public void setIp_calls_paid_minutes_str(String ip_calls_paid_minutes_str) {
        this.ip_calls_paid_minutes_str = ip_calls_paid_minutes_str;
    }

    public int getNo_of_ip_calls() {
        return no_of_ip_calls;
    }

    public void setNo_of_ip_calls(int no_of_ip_calls) {
        this.no_of_ip_calls = no_of_ip_calls;
    }

    public int getNo_of_tdm_calls() {
        return no_of_tdm_calls;
    }

    public void setNo_of_tdm_calls(int no_of_tdm_calls) {
        this.no_of_tdm_calls = no_of_tdm_calls;
    }

    public int getTdm_calls_paid_minutes() {
        return tdm_calls_paid_minutes;
    }

    public void setTdm_calls_paid_minutes(int tdm_calls_paid_minutes) {
        this.tdm_calls_paid_minutes = tdm_calls_paid_minutes;
    }

    public String getTdm_calls_paid_minutes_str() {
        return tdm_calls_paid_minutes_str;
    }

    public void setTdm_calls_paid_minutes_str(String tdm_calls_paid_minutes_str) {
        this.tdm_calls_paid_minutes_str = tdm_calls_paid_minutes_str;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getTotal_calls() {
        return total_calls;
    }

    public void setTotal_calls(int total_calls) {
        this.total_calls = total_calls;
    }

    public int getTotal_minutes() {
        return total_minutes;
    }

    public void setTotal_minutes(int total_minutes) {
        this.total_minutes = total_minutes;
    }

    public String getTotal_minutes_str() {
        return total_minutes_str;
    }

    public void setTotal_minutes_str(String total_minutes_str) {
        this.total_minutes_str = total_minutes_str;
    }

    public String getCdr_date() {
        return cdr_date;
    }

    public void setCdr_date(String cdr_date) {
        this.cdr_date = cdr_date;
    }

    public long getCdr_id() {
        return cdr_id;
    }

    public void setCdr_id(long cdr_id) {
        this.cdr_id = cdr_id;
    }

    public String getConf_id() {
        return conf_id;
    }

    public void setConf_id(String conf_id) {
        this.conf_id = conf_id;
    }

    public String getConnection_time() {
        return connection_time;
    }

    public void setConnection_time(String connection_time) {
        this.connection_time = connection_time;
    }

    public String getDialed_no() {
        return dialed_no;
    }

    public void setDialed_no(String dialed_no) {
        this.dialed_no = dialed_no;
    }

    public String getDisconnect_desc() {
        return disconnect_desc;
    }

    public void setDisconnect_desc(String disconnect_desc) {
        this.disconnect_desc = disconnect_desc;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public double getOrigin_bill_amount() {
        return origin_bill_amount;
    }

    public void setOrigin_bill_amount(double origin_bill_amount) {
        this.origin_bill_amount = origin_bill_amount;
    }

    public String getOrigin_bill_amount_str() {
        return origin_bill_amount_str;
    }

    public void setOrigin_bill_amount_str(String origin_bill_amount_str) {
        this.origin_bill_amount_str = origin_bill_amount_str;
    }

    public String getOrigin_caller() {
        return origin_caller;
    }

    public void setOrigin_caller(String origin_caller) {
        this.origin_caller = origin_caller;
    }

    public int getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(int origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getOrigin_client_name() {
        return origin_client_name;
    }

    public void setOrigin_client_name(String origin_client_name) {
        this.origin_client_name = origin_client_name;
    }

    public String getOrigin_destination() {
        return origin_destination;
    }

    public void setOrigin_destination(String origin_destination) {
        this.origin_destination = origin_destination;
    }

    public String getOrigin_ip() {
        return origin_ip;
    }

    public void setOrigin_ip(String origin_ip) {
        this.origin_ip = origin_ip;
    }

    public String getOrigin_prefix() {
        return origin_prefix;
    }

    public void setOrigin_prefix(String origin_prefix) {
        this.origin_prefix = origin_prefix;
    }

    public String getAns_prefix() {
        return ans_prefix;
    }

    public void setAns_prefix(String ans_prefix) {
        this.ans_prefix = ans_prefix;
    }

    public double getOrigin_rate() {
        return origin_rate;
    }

    public void setOrigin_rate(double origin_rate) {
        this.origin_rate = origin_rate;
    }

    public String getOrigin_rate_des() {
        return origin_rate_des;
    }

    public void setOrigin_rate_des(String origin_rate_des) {
        this.origin_rate_des = origin_rate_des;
    }

    public int getOrigin_rate_id() {
        return origin_rate_id;
    }

    public void setOrigin_rate_id(int origin_rate_id) {
        this.origin_rate_id = origin_rate_id;
    }

    public String getOrigin_rate_str() {
        return origin_rate_str;
    }

    public void setOrigin_rate_str(String origin_rate_str) {
        this.origin_rate_str = origin_rate_str;
    }

    public double getPdd() {
        return pdd;
    }

    public void setPdd(double pdd) {
        this.pdd = pdd;
    }

    public String getPdd_str() {
        return pdd_str;
    }

    public void setPdd_str(String pdd_str) {
        this.pdd_str = pdd_str;
    }

    public double getTerm_bill_amount() {
        return term_bill_amount;
    }

    public void setTerm_bill_amount(double term_bill_amount) {
        this.term_bill_amount = term_bill_amount;
    }

    public String getTerm_bill_amount_str() {
        return term_bill_amount_str;
    }

    public void setTerm_bill_amount_str(String term_bill_amount_str) {
        this.term_bill_amount_str = term_bill_amount_str;
    }

    public String getTerm_caller() {
        return term_caller;
    }

    public void setTerm_caller(String term_caller) {
        this.term_caller = term_caller;
    }

    public int getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(int term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getTerm_client_name() {
        return term_client_name;
    }

    public void setTerm_client_name(String term_client_name) {
        this.term_client_name = term_client_name;
    }

    public String getTerm_destination() {
        return term_destination;
    }

    public void setTerm_destination(String term_destination) {
        this.term_destination = term_destination;
    }

    public String getTerm_ip() {
        return term_ip;
    }

    public void setTerm_ip(String term_ip) {
        this.term_ip = term_ip;
    }

    public String getTerm_prefix() {
        return term_prefix;
    }

    public void setTerm_prefix(String term_prefix) {
        this.term_prefix = term_prefix;
    }

    public double getTerm_rate() {
        return term_rate;
    }

    public void setTerm_rate(double term_rate) {
        this.term_rate = term_rate;
    }

    public String getTerm_rate_des() {
        return term_rate_des;
    }

    public void setTerm_rate_des(String term_rate_des) {
        this.term_rate_des = term_rate_des;
    }

    public int getTerm_rate_id() {
        return term_rate_id;
    }

    public void setTerm_rate_id(int term_rate_id) {
        this.term_rate_id = term_rate_id;
    }

    public String getTerm_rate_str() {
        return term_rate_str;
    }

    public void setTerm_rate_str(String term_rate_str) {
        this.term_rate_str = term_rate_str;
    }

    public String getTerminated_no() {
        return terminated_no;
    }

    public void setTerminated_no(String terminated_no) {
        this.terminated_no = terminated_no;
    }

    public ArrayList<IGWReportDTO> getDtoList() {
        return dtoList;
    }

    public void setDtoList(ArrayList<IGWReportDTO> dtoList) {
        this.dtoList = dtoList;
    }

    public long getTotal_duration() {
        return total_duration;
    }

    public void setTotal_duration(long total_duration) {
        this.total_duration = total_duration;
    }

    public double getTotal_origin_bill_amount() {
        return total_origin_bill_amount;
    }

    public void setTotal_origin_bill_amount(double total_origin_bill_amount) {
        this.total_origin_bill_amount = total_origin_bill_amount;
    }

    public double getTotal_term_bill_amount() {
        return total_term_bill_amount;
    }

    public void setTotal_term_bill_amount(double total_term_bill_amount) {
        this.total_term_bill_amount = total_term_bill_amount;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public int getTotal_cdr() {
        return total_cdr;
    }

    public void setTotal_cdr(int total_cdr) {
        this.total_cdr = total_cdr;
    }

    public double getPercentage_of_calls() {
        return percentage_of_calls;
    }

    public void setPercentage_of_calls(double percentage_of_calls) {
        this.percentage_of_calls = percentage_of_calls;
    }

    public double getPercentage_of_minutes() {
        return percentage_of_minutes;
    }

    public void setPercentage_of_minutes(double percentage_of_minutes) {
        this.percentage_of_minutes = percentage_of_minutes;
    }

    public int getReporting_type() {
        return reporting_type;
    }

    public void setReporting_type(int reporting_type) {
        this.reporting_type = reporting_type;
    }

    public String getOrigin_route() {
        return origin_route;
    }

    public void setOrigin_route(String origin_route) {
        this.origin_route = origin_route;
    }

    public String getTerm_route() {
        return term_route;
    }

    public void setTerm_route(String term_route) {
        this.term_route = term_route;
    }

    public String getAns_name() {
        return ans_name;
    }

    public void setAns_name(String ans_name) {
        this.ans_name = ans_name;
    }

    public int getIcx() {
        return icx;
    }

    public void setIcx(int icx) {
        this.icx = icx;
    }

    public String getIcx_name() {
        return icx_name;
    }

    public void setIcx_name(String icx_name) {
        this.icx_name = icx_name;
    }
}
