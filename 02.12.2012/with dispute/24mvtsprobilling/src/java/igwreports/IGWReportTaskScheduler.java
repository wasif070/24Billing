/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package igwreports;

import com.myapp.struts.login.LoginDTO;
import java.util.ArrayList;

/**
 *
 * @author Ashraful
 */
public class IGWReportTaskScheduler {

    public IGWReportTaskScheduler() {
    }

    public ArrayList<IGWReportDTO> getIncoimgDTOs(IGWReportDTO p_dto, LoginDTO l_dto, int start, int end) {
        IncomingReportDAO reportDAO = new IncomingReportDAO();
        if (l_dto.getSuperUser()) {
            if (p_dto != null) {
                if (p_dto.getReporting_type() == 1) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "connection_time");
                } else if (p_dto.getReporting_type() == 2) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "term_client_name");
                } else if (p_dto.getReporting_type() == 3) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "ans_prefix");
                } else if (p_dto.getReporting_type() == 4) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "term_client_name,origin_client_name");
                }
            } else {
                return reportDAO.getDTOs(p_dto, start, end, l_dto, "connection_time");
            }
        }
        return new ArrayList<IGWReportDTO>();
    }

    public ArrayList<IGWReportDTO> getOutgoingDTOs(IGWReportDTO p_dto, LoginDTO l_dto, int start, int end) {
        OutgoingReportDAO reportDAO = new OutgoingReportDAO();
        if (l_dto.getSuperUser()) {
            if (p_dto != null) {
                if (p_dto.getReporting_type() == 1) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "connection_time");
                } else if (p_dto.getReporting_type() == 2) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "origin_client_name");
                } else if (p_dto.getReporting_type() == 3) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "ans_prefix");
                } else if (p_dto.getReporting_type() == 4) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "term_client_name,term_ip,term_destination,term_prefix");
                } else if (p_dto.getReporting_type() == 5) {
                    return reportDAO.getDTOs(p_dto, start, end, l_dto, "term_client_name,term_ip,origin_ip,term_destination,term_prefix");
                }
            } else {
                return reportDAO.getDTOs(p_dto, start, end, l_dto, "connection_time");
            }
        }
        return new ArrayList<IGWReportDTO>();
    }

    public ArrayList<IGWReportDTO> getIncoimgRevenueDTOs(IGWReportDTO p_dto, LoginDTO l_dto, int start, int end) {
        IncomingRevenueDAO reportDAO = new IncomingRevenueDAO();
        if (l_dto.isUser()) {
            return reportDAO.getDTOs(p_dto, start, end, l_dto, "connection_time");
        }
        return new ArrayList<IGWReportDTO>();
    }
}
