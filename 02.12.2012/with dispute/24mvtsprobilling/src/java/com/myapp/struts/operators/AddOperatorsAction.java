/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class AddOperatorsAction extends Action {

    static Logger logger = Logger.getLogger(AddOperatorsAction.class.getName());

        int x = 0;
    
    public ActionForward execute(ActionMapping mapping, ActionForm form, 
    HttpServletRequest request, HttpServletResponse response) {
    
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        
        if (login_dto != null && login_dto.getSuperUser()) {
        OperatorsForm formBean = (OperatorsForm) form;
        OperatorDTO dto = new OperatorDTO();
        OperatorTaskScheduler scheduler = new OperatorTaskScheduler();
        
        dto.setOp_name(formBean.getOperator_name());
        dto.setOp_prefix(formBean.getOperator_prefix());
         
        MyAppError error = scheduler.addRateplanInformation(dto);
        if (error.getErrorType() > 0) {
            target = "failure";
            formBean.setMessage(error.getErrorMessage());
        } else {     
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getOperatorDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("operators");
                ac_dto.setPrimaryKey(dto.getOp_prefix());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);      
                
                formBean.setMessage("Operator is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;                 
        }            
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }     
        
        return mapping.findForward(target);
    }
    
}
