package com.myapp.struts.rates;

import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Sequencer;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class RateDAO {

    static Logger logger = Logger.getLogger(RateDAO.class.getName());

    public RateDAO() {
    }

    public MyAppError addRateInformation(RateDTO p_dto) {
        MyAppError error = new MyAppError();
        ArrayList<RateDTO> temp_arrayRateDTO = new ArrayList<RateDTO>();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        remotedbconnector.DBConnection dbCon = null;
        Statement remoteSTMT = null;
        int flag = 0;
        boolean at_least_one_row_inserted = false;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbCon = remotedbconnector.DBConnector.getInstance().makeConnection();

            String sql = "";
            long rate_id = 0;
            sql = "select rate_id,rateplan_id from mvts_rates where rate_destination_code=? and rateplan_id=? and rate_destination_name=? and rate_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getRate_destination_code().trim());
            ps.setLong(2, p_dto.getRateplan_id());
            ps.setString(3, p_dto.getRate_destination_name());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                rate_id = resultSet.getLong("rate_id");
            } else {
                rate_id = Sequencer.getInstance().getNextId("rate_id");
            }
            if (resultSet != null) {
                resultSet.close();
            }

                    if (p_dto.getRate_day() != null && p_dto.getRate_day().length > 0) {
                        for (int inc = 0; inc < p_dto.getRate_day().length; inc++) {
                            flag = 0;
                            temp_arrayRateDTO = RateLoader.getInstance().getRateDTOByRateIdList(rate_id);
                            Iterator i = temp_arrayRateDTO.iterator();

                            while(i.hasNext()){
                                RateDTO rDTO = (RateDTO) i.next();
                                if(rDTO.getRate_id() == rate_id && rDTO.getRateplan_id() == p_dto.getRateplan_id() &&
                                        rDTO.getRate_destination_code().equalsIgnoreCase(p_dto.getRate_destination_code().trim()) &&
                                        rDTO.getRate_destination_name().equalsIgnoreCase(p_dto.getRate_destination_name().trim()) &&
                                        rDTO.getRate_per_min()== Float.parseFloat(p_dto.getRatePerMin()[inc]) &&
                                        rDTO.getRate_first_pulse()== Integer.parseInt(p_dto.getFirstPulse()[inc]) &&
                                        rDTO.getRate_next_pulse()== Integer.parseInt(p_dto.getNextPulse()[inc]) &&
                                        rDTO.getRate_grace_period()== Integer.parseInt(p_dto.getGracePeriod()[inc]) &&
                                        rDTO.getRate_failed_period()== Integer.parseInt(p_dto.getFailedPeriod()[inc]) &&
                                        rDTO.getRate_sin_day()== Integer.parseInt(p_dto.getRate_day()[inc]) &&
                                        rDTO.getRate_sin_fromhour()== Integer.parseInt(p_dto.getRate_fromhour()[inc]) &&
                                        rDTO.getRate_sin_frommin()== Integer.parseInt(p_dto.getRate_frommin()[inc]) &&
                                        rDTO.getRate_sin_tohour()== Integer.parseInt(p_dto.getRate_tohour()[inc]) &&
                                        rDTO.getRate_sin_tomin()== Integer.parseInt(p_dto.getRate_tomin()[inc])&&
                                        rDTO.getRate_status()==0) {

                                    flag = 1;
                                    break;
                                }
                            }                            
                            
                            if(flag==0){
                                sql = "insert into mvts_rates(rate_id,rateplan_id,rate_destination_code,rate_destination_name,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_created_date,rate_status,user_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                ps = dbConnection.connection.prepareStatement(sql);
                                ps.setLong(1, rate_id);
                                ps.setLong(2, p_dto.getRateplan_id());
                                ps.setString(3, p_dto.getRate_destination_code().trim());
                                ps.setString(4, p_dto.getRate_destination_name().trim());
                                ps.setFloat(5, Float.parseFloat(p_dto.getRatePerMin()[inc]));
                                ps.setInt(6, Integer.parseInt(p_dto.getFirstPulse()[inc]));
                                ps.setInt(7, Integer.parseInt(p_dto.getNextPulse()[inc]));
                                ps.setInt(8, Integer.parseInt(p_dto.getGracePeriod()[inc]));
                                ps.setInt(9, Integer.parseInt(p_dto.getFailedPeriod()[inc]));
                                ps.setString(10, p_dto.getRate_day()[inc]);
                                ps.setString(11, p_dto.getRate_fromhour()[inc]);
                                ps.setString(12, p_dto.getRate_frommin()[inc]);
                                ps.setString(13, p_dto.getRate_tohour()[inc]);
                                ps.setString(14, p_dto.getRate_tomin()[inc]);
                                ps.setLong(15, System.currentTimeMillis());
                                ps.setInt(16, p_dto.getRate_status());
                                ps.setInt(17, 0);
                                ps.executeUpdate();     
                                at_least_one_row_inserted = true;
                            }
                            

                        }
                    }
                    error.setErrorMessage(String.valueOf(rate_id));
                    if(at_least_one_row_inserted){
                        

                        if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("POST_PAID").getSettingValue()) == AppConstants.YES) {
                            String prefixes = "";
                            String INT_PREFIX = SettingsLoader.getInstance().getSettingsDTO("INT_PREFIX").getSettingValue();
                            ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOListByRatePlanId(p_dto.getRateplan_id());
                            HashMap<String, String> map = new HashMap<String, String>();
                            for (RateDTO rateDTO : rateList) {
                                if (!rateDTO.getRate_destination_code().startsWith(INT_PREFIX)) {
                                    map.put(rateDTO.getRate_destination_code(), rateDTO.getRate_destination_code());
                                }
                            }
                            for (String prefix : map.keySet()) {
                                prefixes += prefix + "[0-9]*;";
                            }
                            prefixes = prefixes.endsWith(";") ? prefixes.substring(0, prefixes.length() - 1) : "";

                            sql = "select gateway_name from gateway,clients where clients.rateplan_id=" + p_dto.getRateplan_id() + " and gateway_delete=0 and clients.id=gateway.client_id";
                            ps = dbConnection.connection.prepareStatement(sql);
                            resultSet = ps.executeQuery();
                            while (resultSet.next()) {
                                remoteSTMT = dbCon.connection.createStatement();
                                remoteSTMT.executeUpdate("update mvts_gateway set src_dnis_prefix_allow='" + prefixes + "' where gateway_name='" + resultSet.getString("gateway_name") + "'");
                            }
                            GatewayLoader.getInstance().forceReload();
                        }                    
                    } else{
                        error.setErrorType(MyAppError.OtherError);
                    }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }

            try {
                if (remoteSTMT != null) {
                    remoteSTMT.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbCon.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbCon);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError editRateInformation(RateDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "";

            String ids = "-1";
            for (String id : p_dto.getIds()) {
                ids += "," + id;
            }

            sql = "update mvts_rates set rate_delete = 1,rate_delete_time=UNIX_TIMESTAMP() where id in(" + ids + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();

            if (ps != null) {
                ps.close();
            }

            if (p_dto.getRate_day() != null && p_dto.getRate_day().length > 0) {
                for (int inc = 0; inc < p_dto.getRate_day().length; inc++) {
                    sql = "insert into mvts_rates(rate_id,rateplan_id,rate_destination_code,rate_destination_name,"
                            + "rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,"
                            + "rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_created_date,"
                            + "rate_status,user_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setLong(1, p_dto.getRate_id());
                    ps.setLong(2, p_dto.getRateplan_id());
                    ps.setString(3, p_dto.getRate_destination_code().trim());
                    ps.setString(4, p_dto.getRate_destination_name());
                    ps.setFloat(5, Float.parseFloat(p_dto.getRatePerMin()[inc]));
                    ps.setInt(6, Integer.parseInt(p_dto.getFirstPulse()[inc]));
                    ps.setInt(7, Integer.parseInt(p_dto.getNextPulse()[inc]));
                    ps.setInt(8, Integer.parseInt(p_dto.getGracePeriod()[inc]));
                    ps.setInt(9, Integer.parseInt(p_dto.getFailedPeriod()[inc]));
                    ps.setString(10, p_dto.getRate_day()[inc]);
                    ps.setString(11, p_dto.getRate_fromhour()[inc]);
                    ps.setString(12, p_dto.getRate_frommin()[inc]);
                    ps.setString(13, p_dto.getRate_tohour()[inc]);
                    ps.setString(14, p_dto.getRate_tomin()[inc]);
                    ps.setLong(15, System.currentTimeMillis());
                    ps.setInt(16, p_dto.getRate_status());
                    ps.setInt(17, 0);
                    ps.executeUpdate();
                }
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        remotedbconnector.DBConnection dbCon = null;
        Statement remoteSTMT = null;

        String selectedIdsString = Utils.implodeArray(destcodeIds, ",");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbCon = remotedbconnector.DBConnector.getInstance().makeConnection();

            long rateplan_id = 0;
            sql = "select rateplan_id from mvts_rates where rate_id in (" + selectedIdsString + ") and rate_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                rateplan_id = resultSet.getLong("rateplan_id");
            }

            sql = "update mvts_rates set rate_delete = 1,rate_delete_time=UNIX_TIMESTAMP() where rate_id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.executeUpdate();

            if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("POST_PAID").getSettingValue()) == AppConstants.YES) {
                if (rateplan_id > 0) {
                    String prefixes = "";
                    String INT_PREFIX = SettingsLoader.getInstance().getSettingsDTO("INT_PREFIX").getSettingValue();
                    ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOListByRatePlanId(rateplan_id);

                    HashMap<String, String> map = new HashMap<String, String>();
                    for (RateDTO rateDTO : rateList) {
                        if (!rateDTO.getRate_destination_code().startsWith(INT_PREFIX)) {
                            map.put(rateDTO.getRate_destination_code(), rateDTO.getRate_destination_code());
                        }
                    }
                    for (String prefix : map.keySet()) {
                        prefixes += prefix + "[0-9]*;";
                    }
                    prefixes = prefixes.endsWith(";") ? prefixes.substring(0, prefixes.length() - 1) : "";

                    sql = "select gateway_name from gateway,clients where clients.rateplan_id=" + rateplan_id + " and gateway_delete=0 and clients.id=gateway.client_id and (gateway_type=0 or gateway_type=2)";
                    ps = dbConnection.connection.prepareStatement(sql);
                    resultSet = ps.executeQuery();
                    while (resultSet.next()) {
                        remoteSTMT = dbCon.connection.createStatement();
                        remoteSTMT.executeUpdate("update mvts_gateway set src_dnis_prefix_allow='" + prefixes + "' where gateway_name='" + resultSet.getString("gateway_name") + "'");
                    }
                    GatewayLoader.getInstance().forceReload();
                }
            }

        } catch (Exception ex) {
            logger.fatal("Error while deleting the Rate Plan!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }

            try {
                if (remoteSTMT != null) {
                    remoteSTMT.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbCon.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbCon);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public StringBuffer getRateCSVStrings(RateDTO rdto) {
        StringBuffer csvString = new StringBuffer();
        ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOListByRatePlanId(rdto.getRateplan_id());
        if (rateList.size() > 0) {
            for (int inc = 0; inc < rateList.size(); inc++) {
                RateDTO dto = rateList.get(inc);
                csvString.append(dto.getRate_destination_code().trim());
                csvString.append(',');
                csvString.append(dto.getRate_destination_name());
                csvString.append(',');
                csvString.append(dto.getRate_per_min());
                csvString.append(',');
                csvString.append(dto.getRate_first_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_next_pulse());
                csvString.append(',');
                csvString.append(dto.getRate_grace_period());
                csvString.append(',');
                csvString.append(dto.getRate_failed_period());
                csvString.append(',');
                csvString.append(dto.getRate_sin_day());
                csvString.append(',');
                csvString.append(dto.getRate_sin_fromhour());
                csvString.append(',');
                csvString.append(dto.getRate_sin_frommin());
                csvString.append(',');
                csvString.append(dto.getRate_sin_tohour());
                csvString.append(',');
                csvString.append(dto.getRate_sin_tomin());
                csvString.append('\n');
            }
        }
        return csvString;
    }

    public MyAppError uploadFlieData(ArrayList<RateDTO> dto_array) {
        ArrayList<RateDTO> temp_arrayRateDTO = new ArrayList<RateDTO>();
        MyAppError error = new MyAppError();
        int count_suc = 0, count_fail = 0;
        String msg_suc = " rates uploaded successfully";
        String msg_fail = " rates uploading failed";
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;
        ResultSet rs = null;
        String sql = "";
        int flag = 0;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            if (dto_array.size() > 0) {
                for (int inc = 0; inc < dto_array.size(); inc++) {
                    flag = 0;
                    RateDTO p_dto = dto_array.get(inc);
                    
                    long rate_id = 0;
                    sql = "select rate_id from mvts_rates where rate_destination_code=? and rateplan_id=? and rate_destination_name=? and rate_delete=0";
                    ps = dbConnection.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getRate_destination_code().trim());
                    ps.setLong(2, p_dto.getRateplan_id());
                    ps.setString(3, p_dto.getRate_destination_name());
                    ResultSet resultSet = ps.executeQuery();
                    if (resultSet.next()) {
                        rate_id = resultSet.getLong("rate_id");
                    } else {
                        rate_id = Sequencer.getInstance().getNextId("rate_id");
                    }
                    if (resultSet != null) {
                        resultSet.close();
                    }                    
                    
                    if(p_dto.getRate_sin_fromhour()<=23 && p_dto.getRate_sin_frommin() <= 59 && p_dto.getRate_sin_tohour()<=23 && p_dto.getRate_sin_tomin() <= 59 ){
                        
                        temp_arrayRateDTO = RateLoader.getInstance().getRateDTOByRateIdList(rate_id);
                        Iterator i = temp_arrayRateDTO.iterator();
                        
                        while(i.hasNext()){
                            RateDTO rDTO = (RateDTO) i.next();
                                if(rDTO.getRate_id() == rate_id && rDTO.getRateplan_id() == p_dto.getRateplan_id() &&
                                        rDTO.getRate_destination_code().equalsIgnoreCase(p_dto.getRate_destination_code().trim()) &&
                                        rDTO.getRate_destination_name().equalsIgnoreCase(p_dto.getRate_destination_name().trim()) &&
                                        rDTO.getRate_per_min()== p_dto.getRate_per_min() &&
                                        rDTO.getRate_first_pulse()== p_dto.getRate_first_pulse() &&
                                        rDTO.getRate_next_pulse()== p_dto.getRate_next_pulse() &&
                                        rDTO.getRate_grace_period()== p_dto.getRate_grace_period() &&
                                        rDTO.getRate_failed_period()== p_dto.getRate_failed_period() &&
                                        rDTO.getRate_sin_day()== p_dto.getRate_sin_day() &&
                                        rDTO.getRate_sin_fromhour()== p_dto.getRate_sin_fromhour() &&
                                        rDTO.getRate_sin_frommin()== p_dto.getRate_sin_frommin() &&
                                        rDTO.getRate_sin_tohour()== p_dto.getRate_sin_tohour() &&
                                        rDTO.getRate_sin_tomin()== p_dto.getRate_sin_tomin() &&
                                        rDTO.getRate_status()==0) {

                                    flag = 1;
                                    break;
                                }
                        }
                        
                        if(flag == 0){
                        sql = "insert into mvts_rates(rate_id,rateplan_id,rate_destination_code,rate_destination_name,rate_per_min,rate_first_pulse,rate_next_pulse,rate_grace_period,rate_failed_period,rate_day,rate_from_hour,rate_from_min,rate_to_hour,rate_to_min,rate_created_date,rate_status,user_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                                ps = dbConnection.connection.prepareStatement(sql);
                                ps.setLong(1, rate_id);
                                ps.setLong(2, p_dto.getRateplan_id());
                                ps.setString(3, p_dto.getRate_destination_code().trim());
                                ps.setString(4, p_dto.getRate_destination_name().trim());
                                ps.setFloat(5, p_dto.getRate_per_min());
                                ps.setInt(6, p_dto.getRate_first_pulse());
                                ps.setInt(7, p_dto.getRate_next_pulse());
                                ps.setInt(8, p_dto.getRate_grace_period());
                                ps.setInt(9, p_dto.getRate_failed_period());
                                ps.setInt(10, p_dto.getRate_sin_day());
                                ps.setInt(11, p_dto.getRate_sin_fromhour());
                                ps.setInt(12, p_dto.getRate_sin_frommin());
                                ps.setInt(13, p_dto.getRate_sin_tohour());
                                ps.setInt(14, p_dto.getRate_sin_tomin());
                                ps.setLong(15, System.currentTimeMillis());
                                ps.setInt(16, p_dto.getRate_status());
                                ps.setInt(17, 0);
                                ps.executeUpdate();
                                count_suc++;                        
                        } else{
                                error.setErrorType(MyAppError.PartialUpdated);
                                count_fail++;                        
                        }
//                        }
                    } else{
                        error.setErrorType(MyAppError.PartialUpdated);
                        count_fail++;
                    }
                }
                msg_suc = count_suc + msg_suc;
                msg_fail = count_fail + msg_fail;
                error.setErrorMessage(msg_suc + "\n" + msg_fail);
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding destcode: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
