/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.util;

/**
 *
 * @author Administrator
 */
public class AppConstants {

    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final long LOADING_INTERVAL = 5 * 60 * 1000L;
    //USER INFORMATION
    public static final String USER_LIST = "USER_LIST";
    public static final String USER_SUCCESS_MESSAGE = "USER_SUCCESS_MESSAGE";
    //ROLE INFORMATION
    public static final String ROLE_LIST = "ROLE_LIST";
    public static final String ROLE_SUCCESS_MESSAGE = "ROLE_SUCCESS_MESSAGE";
    //SETTINGS INFORMATION
    public static final String SETTINGS_LIST = "SETTINGS_LIST";
    public static final String SETTINGS_SUCCESS_MESSAGE = "SETTINGS_SUCCESS_MESSAGE";
    //MOBILE AREA INFORMATION
    public static final String MOBILE_AREA_LIST = "MOBILE_AREA_LIST";
    public static final String MOBILE_AREA_SUCCESS_MESSAGE = "MOBILE_AREA_SUCCESS_MESSAGE";
    //MAIL SERVER
    public static String MAIL_SERVER = "MAIL_SERVER";
    public static String AUTHENTICATION_EMAIL = "AUTHENTICATION_EMAIL";
    public static String AUTHENTICATION_EMAIL_PASSWORD = "AUTHENTICATION_EMAIL_PASSWORD";
    public static String NOTIFICATION_FROM = "NOTIFICATION_FROM";
    public static String NOTIFICATION_TO = "NOTIFICATION_TO";
    public static String NOTIFICATION_CC = "NOTIFICATION_CC";
    public static String NOTIFICATION_BCC = "NOTIFICATION_BCC";
    public static String RECORD_PER_PAGE = "20";
    //NUMBER MANAGEMENT
    public static String CITYCODE_SUCCESS_MESSAGE = "CITYCODE_SUCCESS_MESSAGE";
    public static String CITYCODE_LIST = "CITYCODE_LIST";
    public static String CALLER_SUCCESS_MESSAGE = "CALLER_SUCCESS_MESSAGE";
    public static String CALLER_LIST = "CALLER_LIST";
    public static String CALLEE_SUCCESS_MESSAGE = "CALLEE_SUCCESS_MESSAGE";
    public static String CALLEE_LIST = "CALLEE_LIST";
    public static long ACTIVITY_LOG_DEFAULT_SEARCH_DURATION = 72 * 60 * 60 * 1000L;
    public static String ACTIVITY_DTO = "ACTIVITY_DTO";
    public static String ACTIVITY_LIST = "ACTIVITY_LIST";
    public static String COMPARISON_LIST = "COMPARISON_LIST";
    public static int SUMMARY_REPORT = 1;
    public static int DETAILS_REPORT = 2;
    public static int SUMMARY_REPORT_BY_MOTHER = 3;
    /*-------------PERMISSIONS CONTANTS-------------------*/
    public static final int INACTIVE = 0;
    public static final int ADD = 28;
    public static final int VIEW = 29;
    public static final int EDIT = 30;
    public static final int DELETE = 31;
    /*----------------------------------------------*/
    public static final int NO = 0;
    public static final int YES = 1;
    //Error reporting
    public static final int SWITCH_ERROR = 1;
    public static final int AUTH_ERROR = 2;

    public AppConstants() {
    }
}
