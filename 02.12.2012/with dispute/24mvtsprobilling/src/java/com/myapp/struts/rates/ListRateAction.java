/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import pagination.Pagination;
import pagination.PaginationHelper;

/**
 *
 * @author Ashraful
 */
public class ListRateAction extends Action {

    static Logger logger = Logger.getLogger(ListRateAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        String queryString = ".do?test=1";
        int page = 1;
        int recordPerPage = Constants.RECORD_PER_PAGE;

        long rate_id = 0;
        if (request.getParameter("rate_id") != null) {
            rate_id = Integer.parseInt(request.getParameter("rate_id"));
        } else {
            if (request.getSession(true).getAttribute("sess_rate_id") != null) {
                rate_id = Long.parseLong(request.getSession(true).getAttribute("sess_rate_id").toString());
            }
        }
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);

        if (login_dto != null && login_dto.getSuperUser()) {
            request.getSession(true).setAttribute("sess_rate_id", rate_id);

            int sortItem = 1;
            int sortOrder = 0;

            if (request.getParameter("d-49216-s") != null) {
                sortItem = Integer.parseInt(request.getParameter("d-49216-s"));
                queryString += "&d-49216-s=" + sortItem;
            }
            if (request.getParameter("d-49216-o") != null) {
                sortOrder = Integer.parseInt(request.getParameter("d-49216-o"));
                queryString += "&d-49216-o=" + sortOrder;
            }

            RateTaskSchedular scheduler = new RateTaskSchedular();
            RateForm rateForm = (RateForm) form;

            if (rateForm.getRecordPerPage() > 0) {
                request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, rateForm.getRecordPerPage());
            } else {
                rateForm.setRecordPerPage(Constants.RECORD_PER_PAGE);
            }

            RateDTO dto = new RateDTO();
            if (request.getParameter("rate_destination_code") != null && request.getParameter("rate_destination_code").length() > 0) {
                dto.searchWithPrefixName = true;
                dto.setRate_destination_code(request.getParameter("rate_destination_code").trim());
                queryString += "&rate_destination_code=" + dto.getRate_destination_code();
            }

            recordPerPage = rateForm.getRecordPerPage();
            queryString += "&recordPerPage=" + recordPerPage;


            boolean status = Utils.IntegerValidation(request.getParameter("page"));
            if (status == true) {
                page = Integer.parseInt(request.getParameter("page"));
            }
            status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                page = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            dto.setLimit(recordPerPage);
            dto.setRate_id(rate_id);
            int totalRows = RateLoader.getInstance().getTotalRows(dto);
            int totalPages = totalRows / recordPerPage;
            if (totalRows % recordPerPage > 0) {
                totalPages++;
            }
            if (totalPages < page) {
                page = totalPages;
            }
            dto.setStartingRow((page - 1) * recordPerPage);
            dto.setSortItem(sortItem);
            dto.setSortOrder(sortOrder);

            rateForm.setRateList(scheduler.getRateDTOs(dto));

            Pagination pagination = new Pagination();
            pagination.setTotalRows(totalRows);
            pagination.setPerPage(recordPerPage);
            pagination.setBaseUrl("../" + mapping.getPath().substring(1) + queryString);
            pagination.setCurPage(page);
            PaginationHelper helpper = new PaginationHelper();
            request.getSession(true).setAttribute("PAGER", helpper.createLinks(pagination));
            if (rateForm.getRateList().isEmpty()) {
                page = 1;
            }
            if (request.getQueryString() == null) {
                return (new ActionForward(mapping.findForward(target).getPath() + "?page=" + page + "&rate_destination_code=" + (request.getParameter("rate_destination_code") == null ? "" : request.getParameter("rate_destination_code")) + "&recordPerPage=" + recordPerPage, false));
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }



        return (mapping.findForward(target));
    }
}
