/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 * 
 * @author reefat
 */ 
public class CapacityGroupTaskScheduler {
    public ArrayList<CapacityGroupDTO> getCapacityGrpDTOsWithSearchParam(CapacityGroupDTO udto, LoginDTO l_dto) {
        return CapacityGroupLoader.getInstance().getCapacityGrpDTOsWithSearchParam(udto);
    }    
    
    public ArrayList<CapacityGroupDTO> getCapacityGrpDTOs(LoginDTO l_dto) {
        return CapacityGroupLoader.getInstance().getCapacityGrpDTOList();
    }    
    
    public MyAppError addCapacityGroup(CapacityGroupDTO p_dto) {
        CapacityGroupDAO capacitygrpDAO = new CapacityGroupDAO();
        return capacitygrpDAO.addCapacityGroup(p_dto);
    }     
    
    public CapacityGroupDTO getCapacityGroupDTO(int id) {
        return CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(id);
    }  
    
    public MyAppError mapCapacityGroup(LoginDTO login_dto,CapacityGroupDTO p_dto) {
        CapacityGroupDAO capacitygrpDAO = new CapacityGroupDAO();
        return capacitygrpDAO.mapCapacityGroup(login_dto,p_dto);
    }    
    
    public MyAppError editCapacityGroup(CapacityGroupDTO p_dto) {
        CapacityGroupDAO capacitygrpDAO = new CapacityGroupDAO();
        return capacitygrpDAO.editCapacityGroup(p_dto);
    }
    
    public int  checkCapacityGrpChildParentCap(CapacityGroupDTO udto) {
        return CapacityGroupLoader.getInstance().checkCapacityGrpParentChildRelation(udto);
    }
    
    public int checkCapacityGrpParentLimit(CapacityGroupDTO udto) {
        return CapacityGroupLoader.getInstance().checkCapacityGrpParentLimit(udto);
    }    
    
    public MyAppError deleteCapacityGroup(int p_id) {
        CapacityGroupDAO capacitygrpDAO = new CapacityGroupDAO();
        return capacitygrpDAO.delCapacityGroup(p_id);
    } 
    
    public MyAppError deleteMultiple(int capacityGroupIds[]) {
        CapacityGroupDAO dao = new CapacityGroupDAO();
        return dao.multipleCapacityGroupDelete(capacityGroupIds);
    }
    
    public ArrayList<CapacityGroupDTO> getClientCapacityGrpDTOs(LoginDTO l_dto) {
        return CapacityGroupLoader.getInstance().getClientCapacityGrpDTOs(l_dto);
    }    
    
    public CapacityGroupDTO getClientCapacityGrpDTOByID(LoginDTO l_dto,String gateway_ip) {
        return CapacityGroupLoader.getInstance().getClientCapacityGrpDTOByID(l_dto,gateway_ip);
    }    
      
    public MyAppError editClientCapacityGroup(LoginDTO login_dto,CapacityGroupDTO p_dto) {
        CapacityGroupDAO capacitygrpDAO = new CapacityGroupDAO();
        return capacitygrpDAO.editClientCapacityInformation(login_dto,p_dto);
    }
    
    public ArrayList<CapacityGroupDTO> getClientCapacityGrpDTOsWithSearchParam(CapacityGroupDTO udto, LoginDTO l_dto) {
        return CapacityGroupLoader.getInstance().getClientCapacityGrpDTOsWithSearchParam(udto, l_dto);
    }    
}
