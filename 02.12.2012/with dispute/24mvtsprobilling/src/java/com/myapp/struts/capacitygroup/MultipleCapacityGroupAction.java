/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class MultipleCapacityGroupAction extends Action {

    static Logger logger = Logger.getLogger(MultipleCapacityGroupAction.class.getName());
    
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            boolean actionSuccessful = false;
            String actionName = Constants.EDIT_ACTION;
            ArrayList<CapacityGroupDTO> list = new ArrayList<CapacityGroupDTO>();


            CapacityGroupForm formBean = (CapacityGroupForm) form;

            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();

            MyAppError error = new MyAppError();

           
            if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                for (int id : formBean.getSelectedIDs()) {
                    CapacityGroupDTO DTO = scheduler.getCapacityGroupDTO(id);
                    //DTO.setDeleted(1);
                    list.add(DTO);
                }
                error = scheduler.deleteMultiple(formBean.getSelectedIDs());
                actionSuccessful = true;
                actionName = Constants.DELETE_ACTION;
            } else {
                error.setErrorType(1);
                error.setErrorMessage("Please Select at least one Capacity Group!");
            }
          

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            } else {
//                actionSuccessful = true;

//                if (actionSuccessful) {
//                    if (actionName.equals(Constants.EDIT_ACTION)) {
                        list = new ArrayList<CapacityGroupDTO>();
                        //CapacityGroupDAO cg_dao = new CapacityGroupDAO();
                        ArrayList<CapacityGroupDTO> v_arrayList =  CapacityGroupDAO.getDeletedDTOss();
                        for (CapacityGroupDTO del_dto : v_arrayList) {
                            list.add(del_dto);
                        }
//                    }
                    for (CapacityGroupDTO DTO : list) {
                        try {
                            Gson json = new GsonBuilder().serializeNulls().create();
                            ActivityDTO ac_dto = new ActivityDTO();
                            ac_dto.setUserId(login_dto.getClientId());
                            ac_dto.setChangedValue(json.toJson(DTO));
                            ac_dto.setActionName(actionName);
                            ac_dto.setTableName("mvts_capacity_group");
                            ac_dto.setPrimaryKey(String.valueOf(DTO.getGroup_id()));
                            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                            activityTaskScheduler.addActivityDTO(ac_dto);
                        } catch (Exception e) {
                            logger.debug("Exception during activity log-->" + e);
                        }
                    }
//                }
                formBean.setMessage(false, error.getErrorMessage());
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
    
}
