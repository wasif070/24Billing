/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class AddRateplanAction extends Action {

    static Logger logger = Logger.getLogger(AddRateplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {

            RateplanForm formBean = (RateplanForm) form;
            formBean.setAction(Constants.ADD);
            RateplanDTO dto = new RateplanDTO();
            RateplanTaskSchedular scheduler = new RateplanTaskSchedular();

            dto.setRateplan_name(formBean.getRateplan_name());
            dto.setRateplan_des(formBean.getRateplan_des());
            dto.setUser_id(login_dto.getId());
            dto.setRateplan_status(formBean.getRateplan_status());
//            if (login_dto.getOwn_id() > 0 && formBean.getClient_id() < 0) {
            dto.setClient_id((int) login_dto.getOwn_id());
//            } else {
//                dto.setClient_id(formBean.getClient_id());
//            }

            MyAppError error = scheduler.addRateplanInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getRateplanDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("rateplan");
                ac_dto.setPrimaryKey(dto.getRateplan_name());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "Rate Plan is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}