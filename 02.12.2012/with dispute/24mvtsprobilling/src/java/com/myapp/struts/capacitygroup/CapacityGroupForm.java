/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author reefat
 */
public class CapacityGroupForm extends org.apache.struts.action.ActionForm {

    private String name;
    private int number;
    private int doValidate;
    private String message;
    private int pageNo;
    private int recordPerPage;
    private String capacitygrpId;
    private String capacitygrpName;
    private String capacitygrpDesc;
    private String capacitygrpParentGrp;
    private int capacitygrpCapacity;
    private ArrayList clientList;
    private ArrayList<CapacityGroupDTO> capacityGrpList;
    private ArrayList<CapacityGroupDTO> clientcapacityGrpList;
    private int[] client_id;
    private String[] client_name;
    private String capacitygrp;
    private int gateway_type;
    private int[] selectedIDs;
    private String clientId;
    private String gw_ip;
    private String gateway_type_name;

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public String getGateway_type_name() {
        return gateway_type_name;
    }

    public void setGateway_type_name(String gateway_type_name) {
        this.gateway_type_name = gateway_type_name;
    }

    public ArrayList<CapacityGroupDTO> getClientcapacityGrpList() {
        return clientcapacityGrpList;
    }

    public String getGw_ip() {
        return gw_ip;
    }

    public void setGw_ip(String gw_ip) {
        this.gw_ip = gw_ip;
    }

    public void setClientcapacityGrpList(ArrayList<CapacityGroupDTO> clientcapacityGrpList) {
        this.clientcapacityGrpList = clientcapacityGrpList;
    }

    
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
    
    public int[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(int[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }
    
    public String getCapacitygrp() {
        return capacitygrp;
    }

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }

    public void setCapacitygrp(String capacitygrp) {
        this.capacitygrp = capacitygrp;
    }

    public int[] getClient_id() {
        return client_id;
    }

    public void setClient_id(int[] client_id) {
        this.client_id = client_id;
    }

    public String[] getClient_name() {
        return client_name;
    }

    public void setClient_name(String[] client_name) {
        this.client_name = client_name;
    }

    public ArrayList getClientList() {
        return clientList;
    }

    public void setClientList(ArrayList clientList) {
        this.clientList = clientList;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    public int getCapacitygrpCapacity() {
        return capacitygrpCapacity;
    }

    public void setCapacitygrpCapacity(int capacitygrpCapacity) {
        this.capacitygrpCapacity = capacitygrpCapacity;
    }

    public String getCapacitygrpDesc() {
        return capacitygrpDesc;
    }

    public void setCapacitygrpDesc(String capacitygrpDesc) {
        this.capacitygrpDesc = capacitygrpDesc;
    }

    public String getCapacitygrpName() {
        return capacitygrpName;
    }

    public void setCapacitygrpName(String capacitygrpName) {
        this.capacitygrpName = capacitygrpName;
    }

    public String getCapacitygrpParentGrp() {
        return capacitygrpParentGrp;
    }

    public void setCapacitygrpParentGrp(String capacitygrpParentGrp) {
        this.capacitygrpParentGrp = capacitygrpParentGrp;
    }

    public ArrayList<CapacityGroupDTO> getCapacityGrpList() {
        return capacityGrpList;
    }

    public void setCapacityGrpList(ArrayList<CapacityGroupDTO> capacityGrpList) {
        this.capacityGrpList = capacityGrpList;
    }

    public String getCapacitygrpId() {
        return capacitygrpId;
    }

    public void setCapacitygrpId(String capacitygrpId) {
        this.capacitygrpId = capacitygrpId;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param i
     */
    public void setNumber(int i) {
        number = i;
    }

    /**
     *
     */
    public CapacityGroupForm() {
        super();
        // TODO Auto-generated constructor stub
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getCapacitygrpName() == null || getCapacitygrpName().length() < 1) {
                errors.add("capacitygrpName", new ActionMessage("errors.capacitygroup_name.required"));
                // TODO: add 'error.name.required' key to your resources
            }        
        }        
        
//        if (getName() == null || getName().length() < 1) {
//            errors.add("name", new ActionMessage("error.name.required"));
//            // TODO: add 'error.name.required' key to your resources
//        }
        return errors;
    }
}
