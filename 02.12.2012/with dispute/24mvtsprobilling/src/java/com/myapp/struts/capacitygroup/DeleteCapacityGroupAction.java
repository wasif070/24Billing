/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class DeleteCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        String message;
//        CapacityGroupForm formBean = (CapacityGroupForm) form;
        int pid = Integer.parseInt(request.getParameter("id"));
        CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
        CapacityGroupDTO dto = scheduler.getCapacityGroupDTO(pid);
        
        MyAppError error = scheduler.deleteCapacityGroup(pid);
        if(error.getErrorType()>0)
        {
            target = "failure";
            message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + error.getErrorMessage() + "</span>";            
            request.getSession(true).setAttribute(Constants.MESSAGE, message);
               
        } else{

            LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
            Gson json = new GsonBuilder().serializeNulls().create();
            ActivityDTO ac_dto = new ActivityDTO();
            ac_dto.setUserId(login_dto.getClientId());
            ac_dto.setChangedValue(json.toJson(scheduler.getCapacityGroupDTO(pid)));
            ac_dto.setActionName(Constants.DELETE_ACTION);
            ac_dto.setTableName("mvts_capacity_group");
            ac_dto.setPrimaryKey(String.valueOf(dto.getGroup_id()));
            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
            activityTaskScheduler.addActivityDTO(ac_dto);
            message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + error.getErrorMessage() + dto.getGroup_name() + "</span>";

            request.getSession(true).setAttribute(Constants.MESSAGE, message);
        
        }
        //return mapping.findForward("success");        
        
        return mapping.findForward(target);
    }
    
}
