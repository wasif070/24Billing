package com.myapp.struts.clients;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import org.apache.log4j.Logger;

public class EditClientAction extends Action {

    static Logger logger = Logger.getLogger(AddClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        ClientForm formBean = (ClientForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            ClientDTO dto = new ClientDTO();
            ClientTaskSchedular scheduler = new ClientTaskSchedular();
            dto.setId(formBean.getId());
            dto.setClient_id(formBean.getClient_id());
            dto.setClient_password(formBean.getClient_password());
            dto.setClient_name(formBean.getClient_name());
            dto.setClient_email(formBean.getClient_email());
            dto.setIncoming_prefix(formBean.getIncoming_prefix());
            dto.setIncoming_to(formBean.getIncoming_to());
            dto.setOutgoing_prefix(formBean.getOutgoing_prefix());
            dto.setOutgoing_to(formBean.getOutgoing_to());

            dto.setClient_type(formBean.getClient_type());
            dto.setClient_status(formBean.getClient_status());
            dto.setRateplan_id(formBean.getRateplan_id());
            dto.setClient_credit_limit(formBean.getClient_credit_limit());
            dto.setClient_balance(formBean.getClient_balance());
            dto.setPrefix(formBean.getPrefix());
            dto.setClient_call_limit(formBean.getClient_call_limit());
            dto.setClient_level(formBean.getClient_level());
            dto.setIs_icx(formBean.getIs_icx());
            dto.setNumber_of_cct_or_bw(formBean.getNumber_of_cct_or_bw());
            dto.setMother_company_id(formBean.getMother_company_id());

            if (formBean.getClient_level() == 2) {
                dto.setClient_type(0);//if Reseller then client type origination must
            }

            if (login_dto.getOwn_id() > 0) {
                dto.setParent_id((int) login_dto.getOwn_id());
            } else {
                dto.setParent_id(formBean.getParent_id());
            }

            MyAppError error = scheduler.editClientInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                try {
                    Gson json = new GsonBuilder().serializeNulls().create();
                    ActivityDTO ac_dto = new ActivityDTO();
                    ac_dto.setUserId(login_dto.getClientId());
                    ac_dto.setChangedValue(json.toJson(scheduler.getClientDTO(formBean.getId())));
                    ac_dto.setActionName(Constants.EDIT_ACTION);
                    ac_dto.setTableName("clients");
                    ac_dto.setPrimaryKey(dto.getClient_id());
                    ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                    activityTaskScheduler.addActivityDTO(ac_dto);
                } catch (Exception ex) {
                    logger.debug("Exception edit client-->" + ex);
                }

                String list_all = "?list_all=1";
                if (dto.getParent_id() > 0) {
                    list_all = "?list_all=0";
                }
                formBean.setMessage(false, "Client is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath().concat(list_all + "&id=" + dto.getParent_id()), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
