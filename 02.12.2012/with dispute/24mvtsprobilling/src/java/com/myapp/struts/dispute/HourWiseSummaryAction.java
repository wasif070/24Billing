/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.Utils;
import com.myapp.struts.session.Constants;
import java.util.ArrayList;
/**
 *
 * @author reefat
 */
public class HourWiseSummaryAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
      

        ArrayList<DisputeDTO> disputeDTOList = new ArrayList<DisputeDTO>();
        String target = "success";  
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);        
        if (login_dto != null && login_dto.getSuperUser()){
        int pageNo = 1;
        
        boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
        if (status == true) {
            pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
        }
        
        DisputeTaskScheduler dispute_scheduler = new DisputeTaskScheduler();
        DisputeForm disputeform = (DisputeForm) form;
//        System.out.println("Carrier_id_datewise_summary :: " + disputeform.getCarrier_id_datewise_summary());
        
        if (disputeform.getRecordPerPage() > 0) {
            request.getSession(true).setAttribute(Constants.USER_RECORD_PER_PAGE, disputeform.getRecordPerPage());
        }        
//        dispute_scheduler.printDisputeDateSelect(disputeform.getSelectedIDs());
        int carrier_id = disputeform.getCarrier_id_datewise_summary();  
        

        disputeDTOList = dispute_scheduler.getDisputeDTOsHourlySummaryofSelectedDates(carrier_id,disputeform.getSelectedIDs(),login_dto);
//        disputeform.setDisputeDTOs_datewise_details(disputeDTOList);            
        disputeform.setDisputeDTOs_hourwise_summary(disputeDTOList);
   
        
        if(disputeform.getDisputeDTOs_hourwise_summary()!=null && disputeform.getDisputeDTOs_hourwise_summary().size() <= (disputeform.getRecordPerPage() * (pageNo - 1))){
            ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
            return changedActionForward;
        }        
        }
        else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
    
        
}
