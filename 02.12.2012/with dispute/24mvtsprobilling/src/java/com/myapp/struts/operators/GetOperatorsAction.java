/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class GetOperatorsAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        int id = Integer.parseInt(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            OperatorsForm formBean = (OperatorsForm) form;
            OperatorDTO dto = new OperatorDTO();
            OperatorTaskScheduler scheduler = new OperatorTaskScheduler();
            dto = scheduler.getOperatorDTO(id);
            if (dto != null) {
                formBean.setOperator_id(dto.getOp_id());
                formBean.setOperator_name(dto.getOp_name());
                formBean.setOperator_prefix(dto.getOp_prefix());
//                try {
//                    formBean.setParent_id(ClientLoader.getInstance().getClientDTOByID(dto.getClient_id()).getParent_id());
//                } catch (Exception ex) {
//                }
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
