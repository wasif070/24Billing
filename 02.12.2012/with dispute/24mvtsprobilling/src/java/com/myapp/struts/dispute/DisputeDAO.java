/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import com.myapp.struts.session.Constants;
import java.sql.Statement;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
/**
 *
 * @author reefat
 */
public class DisputeDAO {

    private String folder_name = Constants.FOLDER_NAME;
    private int[] int_array;
    private int[] temp_mapped_columns;
    private String sql="";
    private String sql_sub = "";  
    private MappedByteBuffer buffer;
    private String ext = Constants.EXT;     
    
    public String disputedCarrierDataInsertion(String[] mappedColumns) throws Exception{
  
        DBConnection db = null;
        db = databaseconnector.DBConnector.getInstance().makeConnection();
        PreparedStatement ps;      
        String[] csvFileList = getDirectoryContent(folder_name);
        
        File f = null ;
        
        //which fields should have quotation sign
        int_array = new int[mappedColumns.length];
        temp_mapped_columns = new int[mappedColumns.length];
        
        
        int flag= 0;        
        String temp="";
        char c;
        int size = mappedColumns.length;
        for(int mapping_index = 0;mapping_index<size;mapping_index++){
            System.out.println(mappedColumns[mapping_index].charAt(mappedColumns[mapping_index].length()-1));
            temp_mapped_columns[mapping_index] = Integer.valueOf(String.valueOf(mappedColumns[mapping_index].charAt(mappedColumns[mapping_index].length()-1))) - 1;
        }
        
        for(int mapping_index = 0;mapping_index<temp_mapped_columns.length;mapping_index++){
            System.out.println("cc :: "+temp_mapped_columns[mapping_index]);            
        }        

        
        sql = "insert into carrier_cdr (cdr_date,dialed_no,terminated_no,duration,connection_time,origin_ip,term_ip) values(";
//**************************************************************                
        if(csvFileList!=null){
            for(String file:csvFileList){
                String[] temp_array = new String[100];  
                temp="";
                file = folder_name + file;
                f = new File(file);
                long length = f.length();
                buffer = new FileInputStream(f).getChannel().map(FileChannel.MapMode.READ_ONLY, 0, length);                
                int i=0,index=0;
                while (i < f.length()) {
//                        System.out.print((char) buffer.get(i));
                        c = (char) buffer.get(i);
                        if(c!=',' && c!='\n' && ((i+1) != f.length())){
                                temp = temp + String.valueOf(c);
                        }
                        else{  
                            if((i+1) == f.length()){
                                temp = temp + String.valueOf(c);
                            }
                            temp_array[index] = temp;
                            index++;
                            temp = "";                            
                            if(c=='\n' || ((i+1) == f.length())){
                                sql_sub = sql;
                                
                                for(int loop_variable=0;loop_variable<temp_mapped_columns.length;loop_variable++){
                                    sql_sub = sql_sub + "'"+ temp_array[temp_mapped_columns[loop_variable]] +"'";
                                    
                                    if((loop_variable+1)!=temp_mapped_columns.length){
                                        sql_sub = sql_sub + ",";
                                    }                                     
                                }
                                sql_sub = sql_sub + ");";
                                System.out.println("SQL :: " + sql_sub);
                                index = 0;
                                try{
                                    ps = db.connection.prepareStatement(sql_sub, Statement.RETURN_GENERATED_KEYS);
                                    ps.executeUpdate();                                   
                                }
                                catch(Exception e){
                                    System.out.println("error ::: " + e.getMessage());
                                    return "error occured while inserting" + e.getMessage();
                                }
                            }                            
                        }
                        
                        
                        if((i+1) == f.length()){
                            temp_array[index] = temp; 
                            index++;
                            temp = "";   
                                sql_sub = sql;
                                for(int k=0;k<index;k++){
                                    for(flag=0;flag<int_array.length;flag++){
                                        if(k==int_array[flag]){
                                            temp_array[k] = "'"+temp_array[k]+"'";
                                            break;
                                        }
                                    }                                    
                                    sql_sub = sql_sub + temp_array[k];

                                    if((k+1)!=index){
                                        sql_sub = sql_sub + ",";
                                    }                                     
                                }
                                sql_sub = sql_sub + ");";
                                index = 0;
                                
                                                       
                        }
                        i++;
                }  
              
            }
            
 
        }
        
        return "Inserted Successfully!!";
        
    }    
    
    public class GenericExtFilter implements FilenameFilter {

        private String ext;

        public GenericExtFilter(String ext) {
            this.ext = ext;
        }

        @Override
        public boolean accept(File dir, String name) {
            return (name.endsWith(ext));
        }
    }
    
    private String[] getDirectoryContent(String folder) {
        GenericExtFilter filter = new GenericExtFilter(Constants.EXT);

        File dir = new File(folder);

        if (dir.isDirectory() == false) {
            System.out.println("Directory does not exists : " + folder);
            return null;
        }
        String[] list = dir.list(filter);

        if (list.length == 0) {
            System.out.println("No such files ends with : " + ext);
            return null;
        }

        return list;
    }
    
}
