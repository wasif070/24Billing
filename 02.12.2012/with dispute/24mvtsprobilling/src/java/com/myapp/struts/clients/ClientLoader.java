package com.myapp.struts.clients;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.rateplan.RateplanDTO;
import com.myapp.struts.rateplan.RateplanLoader;
import com.myapp.struts.rates.RateLoader;
import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.user.UserDTO;
import com.myapp.struts.user.UserLoader;
import com.myapp.struts.util.AppConstants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import org.apache.log4j.Logger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.text.DecimalFormat;
import parents.ParentLoader;

public class ClientLoader {

    static Logger logger = Logger.getLogger(ClientLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<ClientDTO> clientList = null;
    private HashMap<Long, ClientDTO> clientDTOByID = null;
    private HashMap<String, ClientDTO> clientDTOByPrefix = null;
    static ClientLoader clientLoder = null;
    DecimalFormat df = new DecimalFormat("#00.0000");

    public ClientLoader() {
        forceReload();
    }

    public static ClientLoader getInstance() {
        if (clientLoder == null) {
            createClientLoader();
        }
        return clientLoder;
    }

    private synchronized static void createClientLoader() {
        if (clientLoder == null) {
            clientLoder = new ClientLoader();
        }
    }

    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            clientList = new ArrayList<ClientDTO>();
            clientDTOByID = new HashMap<Long, ClientDTO>();
            clientDTOByPrefix = new HashMap<String, ClientDTO>();
            String sql = "select id,client_id,client_password,client_name,client_email,client_type,is_icx,number_of_cct_or_bw,client_status,rateplan_id,client_credit_limit,client_balance,prefix,client_call_limit,client_level,parent_id,mother_company_id from clients where client_delete=0 order by id DESC";
            ResultSet resultSet = statement.executeQuery(sql);
            ClientDTO dto = new ClientDTO();
            while (resultSet.next()) {
                dto = new ClientDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setClient_id(resultSet.getString("client_id"));
                dto.setClient_password(resultSet.getString("client_password"));
                dto.setClient_name(resultSet.getString("client_name"));
                dto.setClient_email(resultSet.getString("client_email"));
                dto.setPrefix(resultSet.getString("prefix"));
                dto.setClient_type(resultSet.getInt("client_type"));
                dto.setIs_icx(resultSet.getInt("is_icx"));
                dto.setNumber_of_cct_or_bw(resultSet.getInt("number_of_cct_or_bw"));
                dto.setClient_type_name(Constants.CLIENT_TYPE_NAME[dto.getClient_type()]);
                dto.setClient_status(resultSet.getInt("client_status"));
                dto.setClient_status_name(Constants.LIVE_STATUS_STRING[dto.getClient_status()]);
                dto.setRateplan_id(resultSet.getLong("rateplan_id"));
                dto.setAvg_rate(RateLoader.getInstance().getAvarageRate(dto.getRateplan_id()));
                if (dto.getRateplan_id() > 0) {
                    RateplanDTO rdto = RateplanLoader.getInstance().getRateplanDTOByID(dto.getRateplan_id());
                    if (rdto != null) {
                        dto.setRateplan_name(rdto.getRateplan_name());
                    }
                }
                dto.setMother_company_id(resultSet.getLong("mother_company_id"));
                try {
                    dto.setMother_company_name(ParentLoader.getInstance().getParentDTOByID(dto.getMother_company_id()).getParent_name());
                } catch (Exception ex) {
                    dto.setMother_company_name("NF");
                }
                dto.setClient_credit_limit(df.format(resultSet.getDouble("client_credit_limit")));
                dto.setClient_balance(df.format(resultSet.getDouble("client_balance")));
                dto.setClient_call_limit(resultSet.getInt("client_call_limit"));
                dto.setClient_level(resultSet.getInt("client_level"));
                dto.setClient_level_str(dto.getClient_level() == 1 ? "Client" : "Reseller");
                dto.setParent_id(resultSet.getInt("parent_id"));
                dto.setMother_company_id(resultSet.getLong("mother_company_id"));

                dto.setSuperUser(false);
                clientDTOByID.put(dto.getId(), dto);
                clientDTOByPrefix.put(dto.getPrefix(), dto);
                clientList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<ClientDTO> getClientDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<ClientDTO> newList = new ArrayList<ClientDTO>();
        for (ClientDTO dto : clientList) {
            if (dto.getParent_id() == login_dto.getOwn_id()) {
                dto.setParent_name("Self");
                try {
                    ClientDTO parent_dto = clientDTOByID.get((long) dto.getParent_id());
                    dto.setParent_name(parent_dto.getClient_id());
                } catch (Exception pnfExp) {
                    logger.debug("parent not found");
                }
                newList.add(dto);
            }
        }
        return newList;

    }

    public synchronized ArrayList<Long> getChildren(long id) {
        checkForReload();
        ArrayList<Long> newList = new ArrayList<Long>();
        for (ClientDTO dto : clientList) {
            if (dto.getParent_id() == id) {
                newList.add(dto.getId());
            }

        }
        return newList;
    }

    public synchronized String getTopLevelClient() {
        checkForReload();
        String top_leve_client_ids = "-1";
        for (ClientDTO dto : clientList) {
            if (dto.getParent_id() == -1) {
                top_leve_client_ids += "," + dto.getId();
            }
        }
        return top_leve_client_ids;
    }

    public synchronized String getChildrenStr(long parent) {
        checkForReload();
        String children = "-1";
        for (ClientDTO dto : clientList) {
            if (dto.getParent_id() == parent) {
                children += "," + dto.getId();
            }
        }
        return children;
    }

    public synchronized ClientDTO getClientDTOByID(long id) {
        checkForReload();
        return clientDTOByID.get(id);
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByType(int type) {
        checkForReload();
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH) {
                clientListByType.add(dto);
            }
        }
        return clientListByType;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByTypeAndParent(int type, long parent) {
        checkForReload();
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if ((dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH) && dto.getParent_id() == parent) {
                clientListByType.add(dto);
            }
        }
        return clientListByType;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByTypeAndParent(int type, int parent, int level) {
        checkForReload();
        //level 1=Client
        //level 2=Reseller
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getClient_level() == level && dto.getParent_id() == parent && (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH)) {
                clientListByType.add(dto);
            }
        }
        return clientListByType;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByTypeAndParent(int type, int parent, int level, LoginDTO login_dto) {
        checkForReload();
        //level 1=Client
        //level 2=Reseller
        ArrayList<ClientDTO> clientListByType = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (login_dto.getClient_level() < 0) {
                if (dto.getClient_level() == level && (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH)) {
                    clientListByType.add(dto);
                }
            } else {
                if (dto.getClient_level() == level && dto.getParent_id() == parent && (dto.getClient_type() == type || dto.getClient_type() == Constants.BOTH)) {
                    clientListByType.add(dto);
                }
            }
        }
        return clientListByType;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByLevel(int level) {
        checkForReload();
        ArrayList<ClientDTO> clientDTOByLevel = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getClient_level() == level) {
                clientDTOByLevel.add(dto);
            }
        }
        return clientDTOByLevel;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOByLevel(long id, int level, int access_level) {
        checkForReload();
        ArrayList<ClientDTO> clientDTOByLevel = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);

            if (level > 0) {
                if (access_level > 0) {
                    if (dto.getClient_level() == level && ((dto.getParent_id() == id && id > 0) || (dto.getId() == id && level > 1))) {
                        clientDTOByLevel.add(dto);
                    }
                } else {
                    if (dto.getClient_level() == level) {
                        if (id > 0) {
                            if (dto.getParent_id() == id) {
                                clientDTOByLevel.add(dto);
                            }
                        } else {
                            clientDTOByLevel.add(dto);
                        }
                    }
                }
            } else {
                if (access_level > 0) {
                    if (dto.getParent_id() == id && id > 0) {
                        clientDTOByLevel.add(dto);
                    }
                } else {
                    if (id > 0) {
                        if (dto.getParent_id() == id) {
                            clientDTOByLevel.add(dto);
                        }
                    } else {
                        clientDTOByLevel.add(dto);
                    }
                }
            }
        }
        return clientDTOByLevel;
    }

    public synchronized ArrayList<ClientDTO> getClientDTOById(int id) {
        checkForReload();
        ArrayList<ClientDTO> clientListByid = new ArrayList<ClientDTO>();
        for (int inc = 0; inc < clientList.size(); inc++) {
            ClientDTO dto = clientList.get(inc);
            if (dto.getId() == id) {
                clientListByid.add(dto);
            }
        }
        return clientListByid;
    }

    public ArrayList<ClientDTO> getClientDTOsWithSearchParam(ClientDTO udto, LoginDTO login_dto) {
        ArrayList<ClientDTO> newList = new ArrayList<ClientDTO>();
        checkForReload();
        ArrayList<ClientDTO> list = clientList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                ClientDTO dto = (ClientDTO) i.next();
                if ((udto.searchWithClientID && !dto.getClient_id().toLowerCase().startsWith(udto.getClient_id()))
                        || (udto.searchWithStatus && dto.getClient_status() != udto.getClient_status())
                        || (udto.searchWithType && dto.getClient_type() != udto.getClient_type())
                        || (udto.swParentId && dto.getParent_id() != udto.getId())) {
                    continue;
                }
                dto.setParent_name("Self");
                try {
                    ClientDTO parent_dto = clientDTOByID.get((long) dto.getParent_id());
                    dto.setParent_name(parent_dto.getClient_id());
                } catch (Exception pnfExp) {
                    logger.debug("parent not found");
                }
                newList.add(dto);
            }
        }

        if (!udto.swParentId) {
            ArrayList<ClientDTO> finalList = new ArrayList<ClientDTO>();
            for (ClientDTO dto : newList) {
                if (dto.getParent_id() == login_dto.getOwn_id()) {
                    finalList.add(dto);
                }
            }
            return finalList;
        }

        return newList;
    }

    public ArrayList<ClientDTO> getClientDTOsSorted() {
        checkForReload();
        ArrayList<ClientDTO> list = clientList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    ClientDTO dto1 = (ClientDTO) o1;
                    ClientDTO dto2 = (ClientDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public synchronized boolean checkParentBalance(long parent, double balance) {
        if (parent < 0 || Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("OVER_SALES").getSettingValue()) == 1) {
            return true;
        }
        checkForReload();
        ClientDTO parentClient = null;
        try {
            parentClient = ClientLoader.getInstance().getClientDTOByID(parent);
            if (parentClient != null && Double.parseDouble(parentClient.getClient_balance()) >= balance) {
                return true;
            }
        } catch (Exception ex) {
        }
        return false;
    }

    public synchronized boolean checkParentBalance(double[] balances, LoginDTO login_dto) {
        if (login_dto.getOwn_id() < 0 || Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("OVER_SALES").getSettingValue()) == 1) {
            return true;
        }
        checkForReload();
        double tot_balance = 0.00;
        for (int i = 0; i < balances.length; i++) {
            tot_balance += balances[i];
            logger.debug(i + "->total balance-->" + tot_balance);
        }

        try {
            ClientDTO self_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
            logger.debug("total balance-->" + self_dto.getClient_balance());
            if (self_dto != null && Double.parseDouble(self_dto.getClient_balance()) >= tot_balance) {
                return true;
            }
        } catch (Exception ex) {
            logger.debug("parent balance client dto not found exception-->" + ex);
        }

        return false;
    }

    public synchronized boolean checkDuplicatePrefix(String prefix) {
        checkForReload();
        for (ClientDTO client_dto : clientList) {
            if (client_dto.getPrefix().equalsIgnoreCase(prefix)) {
                return true;
            }
        }
        return false;
    }

    public synchronized boolean checkDuplicateClient(String client_id) {
        checkForReload();
        for (ClientDTO client_dto : clientList) {
            if (client_dto.getClient_id().equalsIgnoreCase(client_id)) {
                return true;
            }
        }
        ArrayList<UserDTO> userList = UserLoader.getInstance().getUserDTOList();
        for (UserDTO user_dto : userList) {
            if (user_dto.getUserId().equalsIgnoreCase(client_id)) {
                return true;
            }
        }
        return false;
    }

    public synchronized String getClientsTree(long id) {
        checkForReload();
        ArrayList<ClientDTO> list = new ArrayList<ClientDTO>();
        ClientDTO cdto = null;
        String tree = "";
        try {
            do {
                if (cdto != null) {
                    cdto = clientDTOByID.get((long) cdto.getParent_id());
                } else {
                    cdto = clientDTOByID.get(id);
                }
                if (cdto != null) {
                    list.add(cdto);
                }
            } while (cdto.getParent_id() != -1);
        } catch (Exception e) {
            logger.fatal("Exception in getClientsTree:", e);
        }

        for (int i = list.size() - 1; i >= 0; i--) {
            ClientDTO dto = list.get(i);
            tree += dto.getClient_id() + "->";
        }

        return tree.length() > 0 ? tree.substring(0, tree.length() - 2) : tree;
    }

     public synchronized ClientDTO getClientDTOByPrefix(String prefix) {
        checkForReload();
        return clientDTOByPrefix.get(prefix);

    }
}
