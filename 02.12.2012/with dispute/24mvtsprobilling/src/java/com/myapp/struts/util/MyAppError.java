package com.myapp.struts.util;

public class MyAppError {

    public static int NoError = 0;
    public static int DBError = 1;
    public static int ValidationError = 2;
    public static int OtherError = 3;
    public static int NotUpdated = 4;
    public static int Updated = 5;
    public static int PartialUpdated = 6;
    public String errorMessage;
    public int errorType;

    public MyAppError() {
        errorType = 0;
        errorMessage = "";
    }

    public int getErrorType() {
        return errorType;
    }

    public void setErrorType(int type) {
        errorType = type;
    }

    public void setErrorMessage(String message) {
        this.errorMessage = message;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }
}
