/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.AppConstants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class OperatorLoader {
    
    static Logger logger = Logger.getLogger(OperatorLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    static OperatorLoader operatorLoader = null;
    private ArrayList<OperatorDTO> operatorList = null;
    private HashMap<Long,OperatorDTO> operatorDTOByID = null;
    
    public static OperatorLoader getInstance() {
        if (operatorLoader == null) {
            createOperatorLoader();
        }
        return operatorLoader;
    }    
    
private synchronized static void createOperatorLoader(){
    if(operatorLoader == null){
        operatorLoader = new OperatorLoader();
    }
}    
    
    private void reload() {
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            operatorList = new ArrayList<OperatorDTO>();
            operatorDTOByID = new HashMap<Long, OperatorDTO>();

            String sql = "select id,op_prefix,op_name from operators order by id DESC";
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                OperatorDTO dto = new OperatorDTO();
                dto.setOp_id(resultSet.getLong("id"));
                dto.setOp_prefix(resultSet.getString("op_prefix"));
                dto.setOp_name(resultSet.getString("op_name"));

                operatorDTOByID.put(dto.getOp_id(), dto);
                operatorList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in OperatorLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }
    
    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }   
    
    public synchronized OperatorDTO getoperatorDTOByID(long id) {
        checkForReload();
        return operatorDTOByID.get(id);
    }
    
    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }
    
    public ArrayList<OperatorDTO> getOperatorDTOsWithSearchParam(OperatorDTO udto, LoginDTO login_dto) {
        ArrayList<OperatorDTO> newList = new ArrayList<OperatorDTO>();
        checkForReload();
        if (operatorList != null && operatorList.size() > 0) {
            Iterator i = operatorList.iterator();
            while (i.hasNext()) {
                OperatorDTO dto = (OperatorDTO) i.next();
                if ((udto.searchWithPrefix && !dto.getOp_prefix().toLowerCase().startsWith(udto.getOp_prefix()))) {
                    continue;
                }
                newList.add(dto);
            }
        }

//        ArrayList<OperatorDTO> finalList = new ArrayList<OperatorDTO>();
//        OperatorDTO baseRateplan = null;
//        if (login_dto.getOwn_id() > 0) {
//            ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
//            if (client_dto != null) {
//                baseRateplan = rateplanDTOByID.get(client_dto.getRateplan_id());
//                if (baseRateplan != null) {
//                    finalList.add(baseRateplan);
//                }
//            }
//        }

//        for (RateplanDTO dto : newList) {
//            if (dto.getClient_id() == login_dto.getOwn_id()) {
//                finalList.add(dto);
//            }
//        }
        return newList;
    }  
    
    public synchronized ArrayList<OperatorDTO> getOperatorDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<OperatorDTO> newList = new ArrayList<OperatorDTO>();

//        OperatorDTO baseOperator = null;
//        if (login_dto.getOwn_id() > 0) {
//            ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID(login_dto.getOwn_id());
//            if (client_dto != null) {
//                baseRateplan = rateplanDTOByID.get(client_dto.getRateplan_id());
//                if (baseRateplan != null) {
//                    newList.add(baseRateplan);
//                }
//            }
//        }

        for (OperatorDTO dto : operatorList) {
    //        if (dto.getClient_id() == login_dto.getOwn_id()) {
                newList.add(dto);
   //         }
        }
        return newList;
    }    
    
    public synchronized OperatorDTO getOperatorDTOByID(long id) {
        checkForReload();
        return operatorDTOByID.get(id);
    }    
    
}
