/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.util.MyAppError;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author reefat
 */
public class OperatorDAO {
    
    static Logger logger = Logger.getLogger(OperatorDAO.class.getName());
    
public MyAppError addOperator(OperatorDTO op_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement statement = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select op_prefix from operators where op_prefix=?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, op_dto.getOp_prefix());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Operator.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "insert into operators(op_prefix,op_name) values(?,?)";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, op_dto.getOp_prefix());
            ps.setString(2, op_dto.getOp_name());

            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                error.setErrorMessage(String.valueOf(rs.getLong(1)));
            }

            OperatorLoader.getInstance().forceReload();
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding Operator Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleDelete(long destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String validIds = "-1";
        //String invalidNames = "";
        String validNames = "";
        String msg = "";
        ArrayList<Long> allDestCodes = new ArrayList<Long>();

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            //String destCodeIds = "-1";
            for (long destCode : destcodeIds) {
                //destCodeIds += "," + destCode;
                allDestCodes.add(destCode);
            }

            //sql = "select clients.rateplan_id from clients,mvts_rateplan where clients.rateplan_id=mvts_rateplan.rateplan_id and client_delete = 0 and clients.rateplan_id in(" + destCodeIds + ")";
            //logger.debug("sql" + sql);
            //ps = dbConnection.connection.prepareStatement(sql);
            //resultSet = ps.executeQuery(sql);
//            if (resultSet.next()) {
//                long rate_paln_id = resultSet.getLong("rateplan_id");
//                invalidNames += OperatorLoader.getInstance().getOperatorDTOByID(rate_paln_id).getRateplan_name() + ",";
//                allDestCodes.remove(rate_paln_id);
//            }
            for (long destCode : allDestCodes) {
                validIds += "," + destCode;
                validNames += OperatorLoader.getInstance().getOperatorDTOByID(destCode).getOp_prefix() + ",";
            }
            

            if (validNames.length() > 0) {
                sql = "DELETE FROM OPERATORS " + " WHERE id IN (" + validIds + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    OperatorLoader.getInstance().forceReload();
                }        
                msg += "These Prefixes have been deleted successfully : " + validNames.substring(0, validNames.length() - 1) + "<BR>";
            }
            logger.debug("Message-->" + msg);

            error.setErrorMessage(msg);
        } catch (Exception ex) {
            logger.fatal("Error while deleting the Prefixes!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError editOperatorInformation(OperatorDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "select id from operators where op_prefix = ? and id != ?";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getOp_prefix());
            ps.setLong(2, p_dto.getOp_id());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Operator.");
                resultSet.close();
                return error;
            }
            resultSet.close();
            sql = "update operators set op_name=?,op_prefix=? where id=" + p_dto.getOp_id();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getOp_name());
            ps.setString(2, p_dto.getOp_prefix());            
            if (ps.executeUpdate() > 0) {
                OperatorLoader.getInstance().forceReload();
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Operator: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }  
    
    public MyAppError deleteOperator(OperatorDTO p_dto) {
        MyAppError error = new MyAppError();

        DBConnection dbConnection = null;
        PreparedStatement ps = null;

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            String sql = "delete from operators  where id=" + p_dto.getOp_id();
            ps = dbConnection.connection.prepareStatement(sql);
            
            if (ps.executeUpdate() > 0) {
                OperatorLoader.getInstance().forceReload();
            }
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while deleting Operator: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }    
}
