package com.myapp.struts.gateway;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.dialplan.DialplanLoader;
import com.myapp.struts.rates.RateDTO;
import com.myapp.struts.rates.RateLoader;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

public class GatewayDAO {

    static Logger logger = Logger.getLogger(GatewayDAO.class.getName());

    public GatewayDAO() {
    }

    public MyAppError addGatewayInformation(GatewayDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        PreparedStatement remotePS = null;
        Statement statement = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "select gateway_id from mvts_gateway where gateway_name=? or src_address_list=? or dst_address=? ";
            remotePS = dbConn.connection.prepareStatement(sql);
            remotePS.setString(1, p_dto.getGateway_name());
            remotePS.setString(2, p_dto.getGateway_ip());
            remotePS.setString(3, p_dto.getGateway_ip());
            ResultSet resultSet = remotePS.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Gateway");
                if (resultSet != null) {
                    resultSet.close();
                }
                return error;
            }
            if (resultSet != null) {
                resultSet.close();
            }

            sql = "select id from gateway where (gateway_name=? or gateway_ip=?) and gateway_delete=0";
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getGateway_name());
            ps.setString(2, p_dto.getGateway_ip());
            resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Gateway");
                if (resultSet != null) {
                    resultSet.close();
                }
                return error;
            }
            if (resultSet != null) {
                resultSet.close();
            }

            String prefixes = "";
            if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("POST_PAID").getSettingValue()) == AppConstants.YES) {
                String INT_PREFIX = SettingsLoader.getInstance().getSettingsDTO("INT_PREFIX").getSettingValue();
                ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID((long) p_dto.getClientId());
                ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOListByRatePlanId(client_dto.getRateplan_id());
                HashMap<String, String> map = new HashMap<String, String>();
                for (RateDTO rateDTO : rateList) {
                    if (!rateDTO.getRate_destination_code().startsWith(INT_PREFIX)) {
                        map.put(rateDTO.getRate_destination_code(), rateDTO.getRate_destination_code());
                    }
                }
                for (String prefix : map.keySet()) {
                    prefixes += prefix + "[0-9]*;";
                }
                prefixes = prefixes.endsWith(";") ? prefixes.substring(0, prefixes.length() - 1) : "";
            }

            if (p_dto.getGateway_type() == 0) {
                sql = "insert into mvts_gateway"
                        + "(gateway_name," //1
                        + "description," //2
                        + "equipment_type," //3
                        + "enable," //4
                        + "src_enable," //5
                        + "dst_enable," //6
                        + "reg_type," //7
                        + "protocol," //8
                        + "src_zone," //9
                        + "src_address_list," //10
                        + "dst_port_h323," //11
                        + "dst_port_sip," //12
                        + "src_codec_allow," //13
                        + "dst_proxy_policy," //14
                        + "dst_codecs_sort," //15
                        + "src_dnis_prefix_allow," //16
                        + "src_rbt_enable,"//17
                        + "dst_h323_first_answer_timeout,"//18
                        + "dst_sip_first_answer_timeout,"//19
                        + "dst_connect_msg_timeout,"//20
                        + "src_can_update_media_channel,"//21
                        + "src_faststart,"//22
                        + "src_response_faststart,"//23
                        + "src_tunneling,"//24
                        + "src_start_h245_after,"//25
                        + "dst_faststart,"//26
                        + "dst_tunneling,"//27
                        + "dst_start_h245_after,"//28
                        + "sip_gate_query,"//29
                        + "src_debug,"//30
                        + "dst_debug,"//31
                        + "radius_auth_call_enable," //32
                        + "radius_acct_enable," //33
                        + "stat_enable,"//34
                        + "disallow_dynamic_payload_type,"//35
                        + "src_drop_call_on_alerting_timeout,"//36
                        + "dst_default_protocol,"//37
                        + "src_codec_policy,"//38
                        + "dst_codec_policy,"//39
                        + "src_start_h245_can_be_forced,"//40
                        + "dst_start_h245_can_be_forced,"//41
                        + "dst_add_end_of_pulsing,"//42
                        + "src_h323_dtmf_policy,"//43
                        + "sip_reason_policy,"//44
                        + "radius_force_originate_telephony,"//45
                        + "always_use_ts_conf_id,"//46
                        + "dst_sip_router_timeout) ";//47
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Origination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 0);
                ps.setInt(7, 0);
                ps.setInt(8, p_dto.getProtocol_id());
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, p_dto.getGateway_dst_port_h323());
                ps.setInt(12, p_dto.getGateway_dst_port_sip());
                ps.setInt(13, 1);
                ps.setInt(14, 5);
                ps.setInt(15, 1);
                ps.setString(16, prefixes);
                ps.setInt(17, 0);
                ps.setInt(18, 10000);
                ps.setInt(19, 32000);
                ps.setInt(20, 90);
                ps.setInt(21, 0);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, 0);
                ps.setInt(30, 0);
                ps.setInt(31, 0);
                ps.setInt(32, p_dto.getEnable_radius());
                ps.setInt(33, p_dto.getEnable_radius());
                ps.setInt(34, 1);
                ps.setInt(35, 0);
                ps.setInt(36, 0);
                ps.setInt(37, 1);
                ps.setInt(38, 1);
                ps.setInt(39, 1);
                ps.setInt(40, 1);
                ps.setInt(41, 1);
                ps.setInt(42, 1);
                ps.setInt(43, 0);
                ps.setInt(44, 0);
                ps.setInt(45, 0);
                ps.setInt(46, 0);
                ps.setInt(47, 32000);
                logger.debug("insert query-->" + ps.toString());
                if (ps.executeUpdate() < 1) {
                    error.setErrorType(MyAppError.DBError);
                    error.setErrorMessage("Switch gateway did not added. Please try again.");
                    return error;
                }
            }
            if (p_dto.getGateway_type() == 1) {
                sql = "insert into mvts_gateway"
                        + "(gateway_name,"//1
                        + "description,"//2
                        + "equipment_type,"//3
                        + "enable,"//4
                        + "src_enable,"//5
                        + "dst_enable,"//6
                        + "reg_type,"//7
                        + "protocol,"//8
                        + "dst_zone,"//9
                        + "dst_address,"//10
                        + "dst_port_h323,"//11
                        + "dst_port_sip,"//12
                        + "dst_proxy_policy,"//13
                        + "dst_codec_allow,"//14
                        + "dst_codecs_sort,"//15
                        + "dst_h323_first_answer_timeout,"//16
                        + "dst_sip_first_answer_timeout,"//17
                        + "dst_connect_msg_timeout,"//18
                        + "src_faststart,"//19
                        + "src_response_faststart,"//20
                        + "src_tunneling,"//21
                        + "src_start_h245_after,"//22
                        + "dst_ani_type_of_number,"//23
                        + "dst_ani_numbering_plan,"//24
                        + "dst_dnis_type_of_number,"//25
                        + "dst_dnis_numbering_plan,"//26
                        + "dst_ani_presentation,"//27
                        + "dst_ani_screening,"//28
                        + "dst_faststart,"//29
                        + "dst_tunneling,"//30
                        + "dst_start_h245_after,"//31
                        + "dst_report_orig_dest,"//32
                        + "dst_sip_privacy_method,"//33
                        + "sip_gate_query,"//34
                        + "src_debug,"//35
                        + "dst_debug,"//36
                        + "stat_enable,"//37
                        + "dst_cpc_method,"//38
                        + "disallow_dynamic_payload_type,"//39
                        + "cancel_src_number_translations,"//40
                        + "dst_default_protocol,"//41
                        + "dst_use_display_name,"//42
                        + "src_codec_policy,"//43
                        + "dst_codec_policy,"//44
                        + "src_start_h245_can_be_forced,"//45
                        + "dst_start_h245_can_be_forced,"//46
                        + "dst_add_end_of_pulsing,"//47
                        + "dst_h323_dtmf_policy,"//48
                        + "sip_reason_policy,"//49
                        + "radius_force_originate_telephony,"//50
                        + "dst_sip_router_timeout) ";//51
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Termination");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 0);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, p_dto.getProtocol_id());
                ps.setString(9, "voip");
                ps.setString(10, p_dto.getGateway_ip());
                ps.setInt(11, p_dto.getGateway_dst_port_h323());
                ps.setInt(12, p_dto.getGateway_dst_port_sip());
                ps.setInt(13, 0);
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 10000);
                ps.setInt(17, 32000);
                ps.setInt(18, 90);
                ps.setInt(19, 1);
                ps.setInt(20, 7);
                ps.setInt(21, 1);
                ps.setInt(22, 2);
                ps.setInt(23, -1);
                ps.setInt(24, -1);
                ps.setInt(25, -1);
                ps.setInt(26, -1);
                ps.setInt(27, -2);
                ps.setInt(28, -2);
                ps.setInt(29, 1);
                ps.setInt(30, 1);
                ps.setInt(31, 2);
                ps.setInt(32, 0);
                ps.setInt(33, 1);
                ps.setInt(34, 0);
                ps.setInt(35, 0);
                ps.setInt(36, 0);
                ps.setInt(37, 1);
                ps.setInt(38, 0);
                ps.setInt(39, 0);
                ps.setInt(40, 0);
                ps.setInt(41, 1);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 0);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 32000);
                logger.debug("insert query1-->" + ps.toString());
                if (ps.executeUpdate() < 1) {
                    error.setErrorType(MyAppError.DBError);
                    error.setErrorMessage("Switch gateway did not added. Please try again.");
                    return error;
                }
            }

            if (p_dto.getGateway_type() == 2) {
                sql = "insert into mvts_gateway"
                        + "(gateway_name," //1
                        + "description,"//2
                        + "equipment_type,"//3
                        + "enable,"//4
                        + "src_enable,"//5
                        + "dst_enable,"//6
                        + "reg_type,"//7
                        + "protocol,"//8
                        + "src_zone,"//9
                        + "dst_zone,"//10
                        + "src_address_list,"//11
                        + "dst_address,"//12
                        + "dst_port_h323,"//13
                        + "src_codec_allow,"//14
                        + "dst_codec_allow,"//15
                        + "dst_codecs_sort,"//16
                        + "src_dnis_prefix_allow,"//17
                        + "src_rbt_enable,"//18
                        + "dst_h323_first_answer_timeout,"//19
                        + "dst_sip_first_answer_timeout,"//20
                        + "dst_connect_msg_timeout,"//21
                        + "src_faststart,"//22
                        + "src_response_faststart,"//23
                        + "src_tunneling,"//24
                        + "src_start_h245_after,"//25
                        + "dst_tunneling,"//26
                        + "dst_faststart,"//27
                        + "dst_start_h245_after,"//28
                        + "dst_ani_type_of_number,"//29
                        + "dst_ani_numbering_plan,"//30
                        + "dst_dnis_type_of_number,"//31
                        + "dst_dnis_numbering_plan,"//32
                        + "dst_ani_presentation,"//33
                        + "dst_ani_screening,"//34
                        + "sip_gate_query,"//35
                        + "src_debug,"//36
                        + "dst_debug,"//37
                        + "radius_auth_call_enable,"//38
                        + "radius_acct_enable,"//39
                        + "stat_enable,"//40
                        + "disallow_dynamic_payload_type,"//41
                        + "src_drop_call_on_alerting_timeout,"//42
                        + "dst_default_protocol,"//43
                        + "src_codec_policy,"//44
                        + "dst_codec_policy,"//45
                        + "src_start_h245_can_be_forced,"//46
                        + "dst_start_h245_can_be_forced,"//47
                        + "dst_add_end_of_pulsing,"//48
                        + "src_h323_dtmf_policy,"//49
                        + "dst_h323_dtmf_policy,"//50
                        + "sip_reason_policy,"//51
                        + "radius_force_originate_telephony,"//52
                        + "always_use_ts_conf_id,"//53
                        + "dst_sip_router_timeout,dst_port_sip) ";//54
                sql += "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                ps = dbConn.connection.prepareStatement(sql);
                ps.setString(1, p_dto.getGateway_name());
                ps.setString(2, "Both");
                ps.setInt(3, 1);
                if (p_dto.getGateway_status() == 0) {
                    ps.setInt(4, 1);
                }
                if (p_dto.getGateway_status() == 1) {
                    ps.setInt(4, 0);
                }
                ps.setInt(5, 1);
                ps.setInt(6, 1);
                ps.setInt(7, 0);
                ps.setInt(8, p_dto.getProtocol_id());
                ps.setString(9, "voip");
                ps.setString(10, "voip");
                ps.setString(11, p_dto.getGateway_ip());
                ps.setString(12, p_dto.getGateway_ip());
                ps.setInt(13, p_dto.getGateway_dst_port_h323());
                ps.setInt(14, 1);
                ps.setInt(15, 1);
                ps.setInt(16, 1);
                ps.setInt(17, 90);
                ps.setString(17, prefixes);
                ps.setInt(18, 0);
                ps.setInt(19, 10000);
                ps.setInt(20, 32000);
                ps.setInt(21, 90);
                ps.setInt(22, 1);
                ps.setInt(23, 7);
                ps.setInt(24, 1);
                ps.setInt(25, 2);
                ps.setInt(26, 1);
                ps.setInt(27, 1);
                ps.setInt(28, 2);
                ps.setInt(29, -1);
                ps.setInt(30, -1);
                ps.setInt(31, -1);
                ps.setInt(32, -1);
                ps.setInt(33, -2);
                ps.setInt(34, -2);
                ps.setInt(35, 0);
                ps.setInt(36, 0);
                ps.setInt(37, 0);
                ps.setInt(38, p_dto.getEnable_radius());
                ps.setInt(39, p_dto.getEnable_radius());
                ps.setInt(40, 1);
                ps.setInt(41, 0);
                ps.setInt(42, 0);
                ps.setInt(43, 1);
                ps.setInt(44, 1);
                ps.setInt(45, 1);
                ps.setInt(46, 1);
                ps.setInt(47, 1);
                ps.setInt(48, 1);
                ps.setInt(49, 0);
                ps.setInt(50, 0);
                ps.setInt(51, 0);
                ps.setInt(52, 0);
                ps.setInt(53, 0);
                ps.setInt(54, 32000);
                ps.setInt(55, p_dto.getGateway_dst_port_sip());
                logger.debug("insert query-->" + ps.toString());
                if (ps.executeUpdate() < 1) {
                    error.setErrorType(MyAppError.DBError);
                    error.setErrorMessage("Switch gateway did not added. Please try again.");
                    return error;
                }
            }

            sql = "insert into gateway(gateway_ip,gateway_name,gateway_type,gateway_status,client_id,owner_id) values(?,?,?,?,?,?)";
            ps = dbConnection.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, p_dto.getGateway_ip());
            ps.setString(2, p_dto.getGateway_name());
            ps.setInt(3, p_dto.getGateway_type());
            ps.setInt(4, p_dto.getGateway_status());
            ps.setInt(5, p_dto.getClientId());
            ps.setInt(6, p_dto.getOwner_id());
            if (ps.executeUpdate() < 1) {
                error.setErrorType(MyAppError.DBError);
                error.setErrorMessage("Gateway did not added. Please try again.");
                return error;
            }

            long last_id = 0;
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                last_id = rs.getLong(1);
                error.setErrorMessage(String.valueOf(last_id));
            }

            if (p_dto.getGateway_type() > 0) {
                if (p_dto.getPrefixes() != null && p_dto.getPrefixes().length > 0) {
                    for (int i = 0; i < p_dto.getPrefixes().length; i++) {
                        sql = "insert into gateway_operators(gate_id,op_id) values(" + last_id + "," + p_dto.getPrefixes()[i] + ")";
                        ps = dbConnection.connection.prepareStatement(sql);
                        ps.executeUpdate();
                    }
                }
            }
            GatewayLoader.getInstance().forceReload();
            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while adding gateway: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remotePS != null) {
                    remotePS.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }

            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError editGatewayInformation(GatewayDTO p_dto) {
        if (p_dto.getPrev_gateway_type() != p_dto.getGateway_type()) {
            return changeGatewayType(p_dto);
        }

        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        PreparedStatement remotePS = null;
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "";

            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                sql = "select gateway_id from mvts_gateway where (gateway_name=? or src_address_list=? or dst_address=?) and gateway_id!=" + p_dto.getSwitchGatewayId();
                remotePS = dbConn.connection.prepareStatement(sql);
                remotePS.setString(1, p_dto.getGateway_name());
                remotePS.setString(2, p_dto.getGateway_ip());
                remotePS.setString(3, p_dto.getGateway_ip());
                ResultSet resultSet = remotePS.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Gateway");
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    return error;
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            }

            sql = "select id from gateway where (gateway_name=? or gateway_ip=?) and gateway_delete=0 and id!=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getGateway_name());
            ps.setString(2, p_dto.getGateway_ip());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Gateway");
                if (resultSet != null) {
                    resultSet.close();
                }
                return error;
            }
            if (resultSet != null) {
                resultSet.close();
            }

            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                String prefixes = "";
                if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("POST_PAID").getSettingValue()) == AppConstants.YES) {
                    String INT_PREFIX = SettingsLoader.getInstance().getSettingsDTO("INT_PREFIX").getSettingValue();
                    ClientDTO client_dto = ClientLoader.getInstance().getClientDTOByID((long) p_dto.getClientId());
                    ArrayList<RateDTO> rateList = RateLoader.getInstance().getRateDTOListByRatePlanId(client_dto.getRateplan_id());
                    HashMap<String, String> map = new HashMap<String, String>();
                    for (RateDTO rateDTO : rateList) {
                        if (!rateDTO.getRate_destination_code().startsWith(INT_PREFIX)) {
                            map.put(rateDTO.getRate_destination_code(), rateDTO.getRate_destination_code());
                        }
                    }
                    for (String prefix : map.keySet()) {
                        prefixes += prefix + "[0-9]*;";
                    }
                    prefixes = prefixes.endsWith(";") ? prefixes.substring(0, prefixes.length() - 1) : "";
                }

                if (p_dto.getGateway_type() == 0) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "enable=?,"//3
                            + "protocol=?,"//4
                            + "src_zone=?,"//5
                            + "src_address_list=?,"//6
                            + "dst_port_h323=?,"//7
                            + "dst_port_sip=?,"//8
                            + "src_dnis_prefix_allow=?,"//9    
                            + "radius_auth_call_enable=?," //10
                            + "radius_acct_enable=?  where "; //11
                    sql += " gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Origination");
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(3, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(3, 0);
                    }
                    ps.setInt(4, p_dto.getProtocol_id());
                    ps.setString(5, "voip");
                    ps.setString(6, p_dto.getGateway_ip());
                    ps.setInt(7, p_dto.getGateway_dst_port_h323());
                    ps.setInt(8, p_dto.getGateway_dst_port_sip());
                    ps.setString(9, prefixes);
                    ps.setInt(10, p_dto.getEnable_radius());
                    ps.setInt(11, p_dto.getEnable_radius());
                    logger.debug("update gateway query-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }

                if (p_dto.getGateway_type() == 1) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "enable=?,"//3
                            + "protocol=?,"//4
                            + "dst_zone=?,"//5
                            + "dst_address=?,"//6
                            + "dst_port_h323=?,"//7
                            + "dst_port_sip=? ";//8                       
                    sql += " where gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Termination");
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(3, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(3, 0);
                    }
                    ps.setInt(4, p_dto.getProtocol_id());
                    ps.setString(5, "voip");
                    ps.setString(6, p_dto.getGateway_ip());
                    ps.setInt(7, p_dto.getGateway_dst_port_h323());
                    ps.setInt(8, p_dto.getGateway_dst_port_sip());
                    logger.debug("update gateway query1-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }

                if (p_dto.getGateway_type() == 2) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "enable=?,"//3
                            + "protocol=?,"//4
                            + "src_zone=?,"//5
                            + "dst_zone=?,"//6
                            + "src_address_list=?,"//7
                            + "dst_address=?,"//8
                            + "dst_port_h323=?,"//9
                            + "src_dnis_prefix_allow=?,"//10
                            + "radius_auth_call_enable=?," //11
                            + "radius_acct_enable=?,dst_port_sip=?  where "; //12
                    sql += " gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Both");
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(3, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(3, 0);
                    }
                    ps.setInt(4, p_dto.getProtocol_id());
                    ps.setString(5, "voip");
                    ps.setString(6, "voip");
                    ps.setString(7, p_dto.getGateway_ip());
                    ps.setString(8, p_dto.getGateway_ip());
                    ps.setInt(9, p_dto.getGateway_dst_port_h323());
                    ps.setString(10, prefixes);
                    ps.setInt(11, p_dto.getEnable_radius());
                    ps.setInt(12, p_dto.getEnable_radius());
                    ps.setInt(13, p_dto.getGateway_dst_port_sip());
                    logger.debug("update gateway query-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }
            } else {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "update gateway set gateway_ip=?,gateway_name=?,gateway_type=?,gateway_status=?,client_id=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getGateway_ip());
            ps.setString(2, p_dto.getGateway_name());
            ps.setInt(3, p_dto.getGateway_type());
            ps.setInt(4, p_dto.getGateway_status());
            ps.setInt(5, p_dto.getClientId());
            if (ps.executeUpdate() < 1) {
                error.setErrorType(MyAppError.NotUpdated);
                error.setErrorMessage("Local gateway did not edited. Please try again.");
                return error;
            }

            if (p_dto.getGateway_type() > 0) {
                ArrayList<Long> current_prefixes = new ArrayList<Long>();
                if (p_dto.getPrefixes() != null && p_dto.getPrefixes().length > 0) {
                    for (int i = 0; i < p_dto.getPrefixes().length; i++) {
                        current_prefixes.add((long) p_dto.getPrefixes()[i]);
                    }
                }

                if (p_dto.getPrev_prefixes() != null && p_dto.getPrev_prefixes().size() > 0) {
                    for (int i = 0; i < p_dto.getPrev_prefixes().size(); i++) {
                        if (!current_prefixes.contains(p_dto.getPrev_prefixes().get(i))) {
                            try {
                                sql = "delete from gateway_operators where gate_id=" + p_dto.getId() + " and op_id='" + p_dto.getPrev_prefixes().get(i) + "' ";
                                ps = dbConnection.connection.prepareStatement(sql);
                                ps.executeUpdate();
                            } catch (Exception ex) {
                                logger.debug("delete gateway_operators-->" + ex);
                            }
                        }
                    }
                }

                if (p_dto.getPrefixes() != null && p_dto.getPrefixes().length > 0) {
                    for (int i = 0; i < p_dto.getPrefixes().length; i++) {
                        try {
                            sql = "insert into gateway_operators(gate_id,op_id) values(" + p_dto.getId() + "," + p_dto.getPrefixes()[i] + ")";
                            ps = dbConnection.connection.prepareStatement(sql);
                            ps.executeUpdate();
                        } catch (Exception ex) {
                            logger.debug("insert into gateway_operators-->" + ex);
                        }
                    }
                }
            }

            GatewayLoader.getInstance().forceReload();
            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing gateway: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remotePS != null) {
                    remotePS.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError changeGatewayType(GatewayDTO p_dto) {
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        PreparedStatement remotePS = null;
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql = "";
            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                sql = "select gateway_id from mvts_gateway where (gateway_name=? or src_address_list=? or dst_address=?) and gateway_id!=" + p_dto.getSwitchGatewayId();
                remotePS = dbConn.connection.prepareStatement(sql);
                remotePS.setString(1, p_dto.getGateway_name());
                remotePS.setString(2, p_dto.getGateway_ip());
                remotePS.setString(3, p_dto.getGateway_ip());
                ResultSet resultSet = remotePS.executeQuery();
                if (resultSet.next()) {
                    error.setErrorType(MyAppError.ValidationError);
                    error.setErrorMessage("Duplicate Gateway");
                    if (resultSet != null) {
                        resultSet.close();
                    }
                    return error;
                }
                if (resultSet != null) {
                    resultSet.close();
                }
            }

            sql = "select id from gateway where (gateway_name=? or gateway_ip=?) and gateway_delete=0 and id!=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);
            ps.setString(1, p_dto.getGateway_name());
            ps.setString(2, p_dto.getGateway_ip());
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("Duplicate Gateway");
                if (resultSet != null) {
                    resultSet.close();
                }
                return error;
            }
            if (resultSet != null) {
                resultSet.close();
            }

            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                String prefixes = "";

                if (p_dto.getGateway_type() == 0) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "equipment_type=?,"//3
                            + "enable=?,"//4
                            + "src_enable=?,"//5
                            + "dst_enable=?,"//6
                            + "reg_type=?,"//7
                            + "protocol=?,"//8
                            + "src_zone=?,"//9
                            + "src_address_list=?,"//10
                            + "dst_port_h323=?,"//11
                            + "dst_port_sip=?,"//12
                            + "src_codec_allow=?,"//13
                            + "dst_proxy_policy=?,"//14
                            + "dst_codecs_sort=?,"//15
                            + "src_dnis_prefix_allow=?,"//16
                            + "src_rbt_enable=?,"//17
                            + "dst_h323_first_answer_timeout=?,"//18
                            + "dst_sip_first_answer_timeout=?,"//19
                            + "dst_connect_msg_timeout=?,"//20
                            + "src_can_update_media_channel=?,"//21
                            + "src_faststart=?,"//22
                            + "src_response_faststart=?,"//23
                            + "src_tunneling=?,"//24
                            + "src_start_h245_after=?,"//25
                            + "dst_faststart=?,"//26
                            + "dst_tunneling=?,"//27
                            + "dst_start_h245_after=?,"//28
                            + "sip_gate_query=?,"//29
                            + "src_debug=?,"//30
                            + "dst_debug=?,"//31
                            + "radius_auth_call_enable=?,"//32
                            + "radius_acct_enable=?,"//33
                            + "stat_enable=?,"//34
                            + "disallow_dynamic_payload_type=?,"//35
                            + "src_drop_call_on_alerting_timeout=?,"//36
                            + "dst_default_protocol=?,"//37
                            + "src_codec_policy=?,"//38
                            + "dst_codec_policy=?,"//39
                            + "src_start_h245_can_be_forced=?,"//40
                            + "dst_start_h245_can_be_forced=?,"//41
                            + "dst_add_end_of_pulsing=?,"//42
                            + "src_h323_dtmf_policy=?,"//43
                            + "sip_reason_policy=?,"//44
                            + "radius_force_originate_telephony=?,"//45
                            + "always_use_ts_conf_id=?,"//46
                            + "dst_sip_router_timeout=?,"//47       
                            + "radius_auth_call_enable=?," //48
                            + "radius_acct_enable=?  where "; //49
                    sql += " gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Origination");
                    ps.setInt(3, 1);
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(4, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(4, 0);
                    }
                    ps.setInt(5, 1);
                    ps.setInt(6, 0);
                    ps.setInt(7, 0);
                    ps.setInt(8, p_dto.getProtocol_id());
                    ps.setString(9, "voip");
                    ps.setString(10, p_dto.getGateway_ip());
                    ps.setInt(11, p_dto.getGateway_dst_port_h323());
                    ps.setInt(12, p_dto.getGateway_dst_port_sip());
                    ps.setInt(13, 1);
                    ps.setInt(14, 5);
                    ps.setInt(15, 1);
                    ps.setString(16, prefixes);
                    ps.setInt(17, 0);
                    ps.setInt(18, 10000);
                    ps.setInt(19, 32000);
                    ps.setInt(20, 90);
                    ps.setInt(21, 0);
                    ps.setInt(22, 1);
                    ps.setInt(23, 7);
                    ps.setInt(24, 1);
                    ps.setInt(25, 2);
                    ps.setInt(26, 1);
                    ps.setInt(27, 1);
                    ps.setInt(28, 2);
                    ps.setInt(29, 0);
                    ps.setInt(30, 0);
                    ps.setInt(31, 0);
                    ps.setInt(32, 1);
                    ps.setInt(33, 1);
                    ps.setInt(34, 1);
                    ps.setInt(35, 0);
                    ps.setInt(36, 0);
                    ps.setInt(37, 1);
                    ps.setInt(38, 1);
                    ps.setInt(39, 1);
                    ps.setInt(40, 1);
                    ps.setInt(41, 1);
                    ps.setInt(42, 1);
                    ps.setInt(43, 0);
                    ps.setInt(44, 0);
                    ps.setInt(45, 0);
                    ps.setInt(46, 0);
                    ps.setInt(47, 32000);
                    ps.setInt(48, p_dto.getEnable_radius());
                    ps.setInt(49, p_dto.getEnable_radius());
                    logger.debug("update gateway query-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }

                if (p_dto.getGateway_type() == 1) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "equipment_type=?,"//3
                            + "enable=?,"//4
                            + "src_enable=?,"//5
                            + "dst_enable=?,"//6
                            + "reg_type=?,"//7
                            + "protocol=?,"//8
                            + "dst_zone=?,"//9
                            + "dst_address=?,"//10
                            + "dst_port_h323=?,"//11
                            + "dst_port_sip=?,"//12
                            + "dst_proxy_policy=?,"//13
                            + "dst_codec_allow=?,"//14
                            + "dst_codecs_sort=?,"//15
                            + "dst_h323_first_answer_timeout=?,"//16
                            + "dst_sip_first_answer_timeout=?,"//17
                            + "dst_connect_msg_timeout=?,"//18
                            + "src_faststart=?,"//19
                            + "src_response_faststart=?,"//20
                            + "src_tunneling=?,"//21
                            + "src_start_h245_after=?,"//22
                            + "dst_ani_type_of_number=?,"//23
                            + "dst_ani_numbering_plan=?,"//24
                            + "dst_dnis_type_of_number=?,"//25
                            + "dst_dnis_numbering_plan=?,"//26
                            + "dst_ani_presentation=?,"//27
                            + "dst_ani_screening=?,"//28
                            + "dst_faststart=?,"//29
                            + "dst_tunneling=?,"//30
                            + "dst_start_h245_after=?,"//31
                            + "dst_report_orig_dest=?,"//32
                            + "dst_sip_privacy_method=?,"//33
                            + "sip_gate_query=?,"//34
                            + "src_debug=?,"//35
                            + "dst_debug=?,"//36
                            + "stat_enable=?,"//37
                            + "dst_cpc_method=?,"//38
                            + "disallow_dynamic_payload_type=?,"//39
                            + "cancel_src_number_translations=?,"//40
                            + "dst_default_protocol=?,"//41
                            + "dst_use_display_name=?,"//42
                            + "src_codec_policy=?,"//43
                            + "dst_codec_policy=?,"//44
                            + "src_start_h245_can_be_forced=?,"//45
                            + "dst_start_h245_can_be_forced=?,"//46
                            + "dst_add_end_of_pulsing=?,"//47
                            + "dst_h323_dtmf_policy=?,"//48
                            + "sip_reason_policy=?,"//49
                            + "radius_force_originate_telephony=?,"//50
                            + "dst_sip_router_timeout=? where ";//51                            
                    sql += " gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Termination");
                    ps.setInt(3, 1);
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(4, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(4, 0);
                    }
                    ps.setInt(5, 0);
                    ps.setInt(6, 1);
                    ps.setInt(7, 0);
                    ps.setInt(8, p_dto.getProtocol_id());
                    ps.setString(9, "voip");
                    ps.setString(10, p_dto.getGateway_ip());
                    ps.setInt(11, p_dto.getGateway_dst_port_h323());
                    ps.setInt(12, p_dto.getGateway_dst_port_sip());
                    ps.setInt(13, 0);
                    ps.setInt(14, 1);
                    ps.setInt(15, 1);
                    ps.setInt(16, 10000);
                    ps.setInt(17, 32000);
                    ps.setInt(18, 90);
                    ps.setInt(19, 1);
                    ps.setInt(20, 7);
                    ps.setInt(21, 1);
                    ps.setInt(22, 2);
                    ps.setInt(23, -1);
                    ps.setInt(24, -1);
                    ps.setInt(25, -1);
                    ps.setInt(26, -1);
                    ps.setInt(27, -2);
                    ps.setInt(28, -2);
                    ps.setInt(29, 1);
                    ps.setInt(30, 1);
                    ps.setInt(31, 2);
                    ps.setInt(32, 0);
                    ps.setInt(33, 1);
                    ps.setInt(34, 0);
                    ps.setInt(35, 0);
                    ps.setInt(36, 0);
                    ps.setInt(37, 1);
                    ps.setInt(38, 0);
                    ps.setInt(39, 0);
                    ps.setInt(40, 0);
                    ps.setInt(41, 1);
                    ps.setInt(42, 0);
                    ps.setInt(43, 1);
                    ps.setInt(44, 1);
                    ps.setInt(45, 1);
                    ps.setInt(46, 1);
                    ps.setInt(47, 1);
                    ps.setInt(48, 0);
                    ps.setInt(49, 0);
                    ps.setInt(50, 0);
                    ps.setInt(51, 32000);
                    logger.debug("update gateway query1-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }

                if (p_dto.getGateway_type() == 2) {
                    sql = "update mvts_gateway set "
                            + "gateway_name=?,"//1
                            + "description=?,"//2
                            + "equipment_type=?,"//3
                            + "enable=?,"//4
                            + "src_enable=?,"//5
                            + "dst_enable=?,"//6
                            + "reg_type=?,"//7
                            + "protocol=?,"//8
                            + "src_zone=?,"//9
                            + "dst_zone=?,"//10
                            + "src_address_list=?,"//11
                            + "dst_address=?,"//12
                            + "dst_port_h323=?,"//13
                            + "src_codec_allow=?,"//14
                            + "dst_codec_allow=?,"//15
                            + "dst_codecs_sort=?,"//16
                            + "src_dnis_prefix_allow=?,"//17
                            + "src_rbt_enable=?,"//18
                            + "dst_h323_first_answer_timeout=?,"//19
                            + "dst_sip_first_answer_timeout=?,"//20
                            + "dst_connect_msg_timeout=?,"//21
                            + "src_faststart=?,"//22
                            + "src_response_faststart=?,"//23
                            + "src_tunneling=?,"//24
                            + "src_start_h245_after=?,"//25
                            + "dst_tunneling=?,"//26
                            + "dst_faststart=?,"//27
                            + "dst_start_h245_after=?,"//28
                            + "dst_ani_type_of_number=?,"//29
                            + "dst_ani_numbering_plan=?,"//30
                            + "dst_dnis_type_of_number=?,"//31
                            + "dst_dnis_numbering_plan=?,"//32
                            + "dst_ani_presentation=?,"//33
                            + "dst_ani_screening=?,"//34
                            + "sip_gate_query=?,"//35
                            + "src_debug=?,"//36
                            + "dst_debug=?,"//37
                            + "radius_auth_call_enable=?,"//38
                            + "radius_acct_enable=?,"//39
                            + "stat_enable=?,"//40
                            + "disallow_dynamic_payload_type=?,"//41
                            + "src_drop_call_on_alerting_timeout=?,"//42
                            + "dst_default_protocol=?,"//43
                            + "src_codec_policy=?,"//44
                            + "dst_codec_policy=?,"//45
                            + "src_start_h245_can_be_forced=?,"//46
                            + "dst_start_h245_can_be_forced=?,"//47
                            + "dst_add_end_of_pulsing=?,"//48
                            + "src_h323_dtmf_policy=?,"//49
                            + "dst_h323_dtmf_policy=?,"//50
                            + "sip_reason_policy=?,"//51
                            + "radius_force_originate_telephony=?,"//52
                            + "always_use_ts_conf_id=?,"//53
                            + "dst_sip_router_timeout=?,"//54
                            + "radius_auth_call_enable=?," //55
                            + "radius_acct_enable=?,dst_port_sip=?  where "; //56
                    sql += " gateway_id=" + p_dto.getSwitchGatewayId();
                    ps = dbConn.connection.prepareStatement(sql);
                    ps.setString(1, p_dto.getGateway_name());
                    ps.setString(2, "Both");
                    ps.setInt(3, 1);
                    if (p_dto.getGateway_status() == 0) {
                        ps.setInt(4, 1);
                    }
                    if (p_dto.getGateway_status() == 1) {
                        ps.setInt(4, 0);
                    }
                    ps.setInt(5, 1);
                    ps.setInt(6, 1);
                    ps.setInt(7, 0);
                    ps.setInt(8, p_dto.getProtocol_id());
                    ps.setString(9, "voip");
                    ps.setString(10, "voip");
                    ps.setString(11, p_dto.getGateway_ip());
                    ps.setString(12, p_dto.getGateway_ip());
                    ps.setInt(13, p_dto.getGateway_dst_port_h323());
                    ps.setInt(14, 1);
                    ps.setInt(15, 1);
                    ps.setInt(16, 1);
                    ps.setString(17, prefixes);
                    ps.setInt(18, 0);
                    ps.setInt(19, 10000);
                    ps.setInt(20, 32000);
                    ps.setInt(21, 90);
                    ps.setInt(22, 1);
                    ps.setInt(23, 7);
                    ps.setInt(24, 1);
                    ps.setInt(25, 2);
                    ps.setInt(26, 1);
                    ps.setInt(27, 1);
                    ps.setInt(28, 2);
                    ps.setInt(29, -1);
                    ps.setInt(30, -1);
                    ps.setInt(31, -1);
                    ps.setInt(32, -1);
                    ps.setInt(33, -2);
                    ps.setInt(34, -2);
                    ps.setInt(35, 0);
                    ps.setInt(36, 0);
                    ps.setInt(37, 0);
                    ps.setInt(38, 1);
                    ps.setInt(39, 1);
                    ps.setInt(40, 1);
                    ps.setInt(41, 0);
                    ps.setInt(42, 0);
                    ps.setInt(43, 1);
                    ps.setInt(44, 1);
                    ps.setInt(45, 1);
                    ps.setInt(46, 1);
                    ps.setInt(47, 1);
                    ps.setInt(48, 1);
                    ps.setInt(49, 0);
                    ps.setInt(50, 0);
                    ps.setInt(51, 0);
                    ps.setInt(52, 0);
                    ps.setInt(53, 0);
                    ps.setInt(54, 32000);
                    ps.setInt(55, p_dto.getEnable_radius());
                    ps.setInt(56, p_dto.getEnable_radius());
                    ps.setInt(57, p_dto.getGateway_dst_port_sip());
                    logger.debug("update gateway query-->" + ps.toString());
                    if (ps.executeUpdate() < 1) {
                        error.setErrorType(MyAppError.NotUpdated);
                        error.setErrorMessage("Switch gateway did not edited. Please try again.");
                        return error;
                    }
                }

            } else {
                GatewayLoader.getInstance().forceReload();
            }

            sql = "update gateway set gateway_ip=?,gateway_name=?,gateway_type=?,gateway_status=?,client_id=? where id=" + p_dto.getId();
            ps = dbConnection.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getGateway_ip());
            ps.setString(2, p_dto.getGateway_name());
            ps.setInt(3, p_dto.getGateway_type());
            ps.setInt(4, p_dto.getGateway_status());
            ps.setInt(5, p_dto.getClientId());
            if (ps.executeUpdate() < 1) {
                error.setErrorType(MyAppError.NotUpdated);
                error.setErrorMessage("Gateway did not edited. Please try again.");
                return error;
            }

            GatewayLoader.getInstance().forceReload();
            DialplanLoader.getInstance().forceReload();
        } catch (Exception ex) {
            logger.fatal("Error while editing gateway: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remotePS != null) {
                    remotePS.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError deleteGateway(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                sql = "select gateway_name from gateway where id =" + cid;
                ps = dbConnection.connection.prepareStatement(sql);
                resultSet = ps.executeQuery(sql);
                GatewayDTO dto = new GatewayDTO();
                while (resultSet.next()) {
                    dto.setGateway_name(resultSet.getString("gateway_name"));
                }


                sql = "select gateway_id from mvts_gateway where gateway_name='" + dto.getGateway_name() + "'";
                stmt = dbConn.connection.createStatement();
                resultSet = stmt.executeQuery(sql);
                int gateway_id = -1;
                while (resultSet.next()) {
                    gateway_id = resultSet.getInt("gateway_id");
                }
                sql = "select dialpeer_name,gateway_list from mvts_dialpeer where gateway_list like '%" + gateway_id + ";%'";
                resultSet = stmt.executeQuery(sql);
                String dial_peers = "";
                while (resultSet.next()) {
                    String[] gateway_ids = resultSet.getString("gateway_list").split(";");
                    for (String g_id : gateway_ids) {
                        if (Integer.parseInt(g_id) == gateway_id) {
                            dial_peers += "," + resultSet.getString("dialpeer_name");
                        }
                    }
                }
                if (dial_peers.length() > 0) {
                    error.setErrorType(MyAppError.OtherError);
                    error.setErrorMessage(dial_peers.substring(1) + " dialplan(s) is/are associated with this gateway.<div class='clear'></div>Please remove the gateway from dialplan first.");
                    return error;
                } else {
                    try {
                        dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
                        sql = "delete from mvts_gateway where gateway_name='" + dto.getGateway_name() + "'";
                        ps = dbConn.connection.prepareStatement(sql);
                        if (ps.executeUpdate() < 1) {
                            error.setErrorType(MyAppError.DBError);
                            error.setErrorMessage("Switch Gateway did not delete. Please try again.");
                            return error;
                        } else {
                            sql = "update gateway set gateway_delete = 1 where id =" + cid;
                            ps = dbConnection.connection.prepareStatement(sql);
                            if (ps.executeUpdate() > 0) {
                                GatewayLoader.getInstance().forceReload();
                                DialplanLoader.getInstance().forceReload();
                            }
                            error.setErrorType(MyAppError.NoError);
                            error.setErrorMessage("Gateway has been deleted successfuly.");
                            return error;
                        }
                    } catch (Exception ex) {
                        logger.fatal("Error while deleting gateway in MVTSPro: ", ex);
                        error.setErrorType(MyAppError.DBError);
                        error.setErrorMessage("Gateway did not delete. Please try again.");
                        return error;
                    }
                }
            } else {
                sql = "update gateway set gateway_delete = 1 where id =" + cid;
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    GatewayLoader.getInstance().forceReload();
                }
                error.setErrorType(MyAppError.NoError);
                error.setErrorMessage("Gateway has been deleted successfuly.");
                return error;
            }
        } catch (Exception ex) {
            logger.fatal("Error while deleting gateway: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }

        }
        return error;
    }

    public MyAppError multipleGatewayDelete(long gatewayIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        Statement stmt = null;
        ResultSet resultSet = null;
        remotedbconnector.DBConnection dbConn = null;
        String selectedIdsString = Utils.implodeArray(gatewayIds, ",");
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                sql = "select gateway_name from gateway where id in(" + selectedIdsString + ")";
                stmt = dbConnection.connection.createStatement();
                resultSet = stmt.executeQuery(sql);
                String selectedNamesString = "'-1'";
                while (resultSet.next()) {
                    selectedNamesString += ",'" + resultSet.getString("gateway_name") + "'";
                }

                ArrayList<Integer> gateway_ids = new ArrayList<Integer>();
                sql = "select gateway_id from mvts_gateway where gateway_name in(" + selectedNamesString + ")";
                dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
                stmt = dbConn.connection.createStatement();
                resultSet = stmt.executeQuery(sql);
                while (resultSet.next()) {
                    gateway_ids.add(resultSet.getInt("gateway_id"));
                }
                sql = "select dialpeer_name,gateway_list from mvts_dialpeer where 1 ";
                for (int gateway_id : gateway_ids) {
                    sql += " or gateway_list like '%" + gateway_id + "%'";
                }
                logger.debug("Dial peer name sql-->" + sql);
                resultSet = stmt.executeQuery(sql);
                String dial_peers = "";
                while (resultSet.next()) {
                    String[] ids = resultSet.getString("gateway_list").split(";");
                    for (String g_id : ids) {
                        if (gateway_ids.contains(Integer.parseInt(g_id))) {
                            dial_peers += "," + resultSet.getString("dialpeer_name");
                        }
                    }
                }
                if (dial_peers.length() > 0) {
                    error.setErrorType(MyAppError.OtherError);
                    error.setErrorMessage(dial_peers.substring(1) + " dialplan(s) is/are associated with this gateway.<div class='clear'></div>Please remove the gateway from dialplan first.");
                    return error;
                } else {
                    try {
                        sql = "delete from mvts_gateway where gateway_name in(" + selectedNamesString + ")";
                        ps = dbConn.connection.prepareStatement(sql);
                        if (ps.executeUpdate() < 1) {
                            error.setErrorType(MyAppError.DBError);
                            error.setErrorMessage("MVTSPRO Gateway did not delete. Please try again.");
                            return error;
                        } else {
                            sql = "update gateway set gateway_delete = 1 where id in(" + selectedIdsString + ")";
                            ps = dbConnection.connection.prepareStatement(sql);
                            if (ps.executeUpdate() > 0) {
                                GatewayLoader.getInstance().forceReload();
                                DialplanLoader.getInstance().forceReload();
                            }
                            error.setErrorType(MyAppError.NoError);
                            error.setErrorMessage("MVTSPRO Gateway has been deleted successfuly.");
                        }
                    } catch (Exception ex) {
                        error.setErrorType(MyAppError.DBError);
                        error.setErrorMessage("MVTSPRO Gateway did not delete. Please try again.");
                        return error;
                    }
                }
            } else {
                sql = "update gateway set gateway_delete = 1 where id in(" + selectedIdsString + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    GatewayLoader.getInstance().forceReload();
                }
                error.setErrorType(MyAppError.NoError);
                error.setErrorMessage("MVTSPRO Gateway has been deleted successfuly.");
            }
        } catch (Exception ex) {
            logger.fatal("Error while deleting gateway ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }

    public MyAppError multipleGatewayStatusUpdate(long gatewayIds[], int gateway_status) {
        String sql = "";
        MyAppError error = new MyAppError();
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        remotedbconnector.DBConnection dbConn = null;
        String selectedIdsString = Utils.implodeArray(gatewayIds, ",");
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");

        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();

            if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                sql = "select gateway_name from gateway where id in(" + selectedIdsString + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                resultSet = ps.executeQuery(sql);
                String selectedNamesString = "'-1'";
                while (resultSet.next()) {
                    selectedNamesString += ",'" + resultSet.getString("gateway_name") + "'";
                }

                int sts[] = {1, 0};
                sql = "update mvts_gateway set enable = " + sts[gateway_status] + " where gateway_name in(" + selectedNamesString + ")";
                logger.debug("Gateway status update-->" + sql);
                ps = dbConn.connection.prepareStatement(sql);

                if (ps.executeUpdate() < 1) {
                    error.setErrorType(MyAppError.NotUpdated);
                    error.setErrorMessage("Switch Gateway status did not updated. Please try again.");
                    return error;
                }
            }

            sql = "update gateway set gateway_status = " + gateway_status + " where id in(" + selectedIdsString + ")";
            ps = dbConnection.connection.prepareStatement(sql);
            if (ps.executeUpdate() > 0) {
                GatewayLoader.getInstance().forceReload();
                if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
                    DialplanLoader.getInstance().forceReload();
                }
            } else {
                error.setErrorType(MyAppError.NotUpdated);
                error.setErrorMessage("Gateway status did not updated. Please try again.");
                return error;
            }


        } catch (Exception ex) {
            logger.fatal("Error while editing gateway: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
