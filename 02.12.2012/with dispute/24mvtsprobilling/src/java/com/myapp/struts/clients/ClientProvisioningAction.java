package com.myapp.struts.clients;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.dialplan.DialplanDTO;
import com.myapp.struts.dialplan.DialplanLoader;
import com.myapp.struts.dialplan.DialplanTaskSchedular;
import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.gateway.GatewayTaskSchedular;
import com.myapp.struts.rateplan.RateplanDTO;
import com.myapp.struts.rateplan.RateplanTaskSchedular;
import com.myapp.struts.rates.RateDTO;
import com.myapp.struts.rates.RateTaskSchedular;
import com.myapp.struts.util.Sequencer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;
import org.apache.log4j.Logger;

public class ClientProvisioningAction extends Action {

    static Logger logger = Logger.getLogger(ClientProvisioningAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            ClientForm formBean = (ClientForm) form;
            ClientDTO dto = new ClientDTO();
            MyAppError error = new MyAppError();
            /*-----------------------------------ADD CLIENT ID-------------------------------------------*/
            boolean found = true;
            dto.setClient_id(formBean.getClient_id());
            if (dto.getClient_id().trim().length() == 0) {
                dto.setClient_id(formBean.getClient_name().replaceAll(" ", ""));
                if (dto.getClient_id().trim().length() == 0) {
                    while (found) {
                        DecimalFormat df = new DecimalFormat("#####00000");
                        String client_id = "CL" + df.format(Sequencer.getInstance().getNextId("client_id"));
                        found = ClientLoader.getInstance().checkDuplicateClient(client_id);
                        dto.setClient_id(client_id);
                    }
                }
            }
            /*-----------------------------------ADD PREFIX-------------------------------------------*/
            String prefix = "";
            found = true;
            DecimalFormat df = new DecimalFormat("###000");
            while (found) {
                prefix = df.format(Sequencer.getInstance().getNextId("prefix"));
                found = ClientLoader.getInstance().checkDuplicatePrefix(prefix);
            }
            dto.setPrefix(prefix);
            /*-----------------------------------ADD RATEPLAN-------------------------------------------*/
            RateplanDTO rplan_dto = new RateplanDTO();
            RateplanTaskSchedular rplan_scheduler = new RateplanTaskSchedular();

            rplan_dto.setRateplan_name("RP" + dto.getClient_id());
            rplan_dto.setRateplan_des("");
            rplan_dto.setUser_id(login_dto.getId());
            rplan_dto.setRateplan_status(0);
            rplan_dto.setClient_id((int) login_dto.getOwn_id());
            error = rplan_scheduler.addRateplanInformation(rplan_dto);
            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(rplan_scheduler.getRateplanDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("mvts_rateplan");
                ac_dto.setPrimaryKey("RP" + dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            }
            /*-----------------------------------ADD RATE-------------------------------------------*/
            RateDTO rate_dto = new RateDTO();
            RateTaskSchedular rate_scheduler = new RateTaskSchedular();
            long rateplan_id = Integer.parseInt(error.getErrorMessage());
            rate_dto.setRateplan_id(rateplan_id);
            rate_dto.setRate_destination_code(dto.getPrefix());
            rate_dto.setRate_destination_name("R" + dto.getClient_id());
            rate_dto.setRate_status(0);
            String[] var = new String[1];
            var[0] = "-1";
            rate_dto.setRate_day(var);
            var = new String[1];
            var[0] = "0";
            rate_dto.setRate_fromhour(var);
            var = new String[1];
            var[0] = "0";
            rate_dto.setRate_frommin(var);
            var = new String[1];
            var[0] = "23";
            rate_dto.setRate_tohour(var);
            var = new String[1];
            var[0] = "59";
            rate_dto.setRate_tomin(var);
            var = new String[1];
            var[0] = "0.03";
            rate_dto.setRatePerMin(var);
            var = new String[1];
            var[0] = "1";
            rate_dto.setFirstPulse(var);
            var = new String[1];
            var[0] = "1";
            rate_dto.setNextPulse(var);
            var = new String[1];
            var[0] = "0";
            rate_dto.setGracePeriod(var);
            var = new String[1];
            var[0] = "0";
            rate_dto.setFailedPeriod(var);

            error = rate_scheduler.addRateInformation(rate_dto);
            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(rate_scheduler.getRateDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("mvts_rates");
                ac_dto.setPrimaryKey("R" + dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            }
            /*-----------------------------------ADD CLIENT-------------------------------------------*/
            ClientTaskSchedular scheduler = new ClientTaskSchedular();

            Random rn = new Random();
            int n = 10000000 - 100000 + 1;
            dto.setClient_password("PASS" + (100000 + Math.abs(rn.nextInt()) % n));

            dto.setClient_name(formBean.getClient_name());
            dto.setClient_email("");
            dto.setIncoming_prefix("");
            dto.setIncoming_to("");
            dto.setOutgoing_prefix("");
            dto.setOutgoing_to("");
            dto.setClient_type(0);
            dto.setClient_status(0);
            dto.setRateplan_id(rate_dto.getRateplan_id());
            dto.setClient_credit_limit("0");
            dto.setClient_balance("10");
            dto.setClient_call_limit(10);
            formBean.setPrefix(dto.getPrefix());
            dto.setClient_level(1);//Must Client
            dto.setIs_icx(0);
            dto.setNumber_of_cct_or_bw(0);
            dto.setParent_id(-1);
            dto.setTransaction_by(-1);
            dto.setDest_no_translate(formBean.getDest_no_translate());

            if (ClientLoader.getInstance().checkParentBalance(dto.getParent_id(), Double.parseDouble(dto.getClient_balance()))) {
                error = scheduler.addClientInformation(login_dto.getId(), dto);
            } else {
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Parent has not sufficient balance");
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getClientDTO(Long.parseLong(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("clients");
                ac_dto.setPrimaryKey(dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            }
            /*-----------------------------------ADD GATEWAY-------------------------------------------*/
            GatewayDTO gate_dto = new GatewayDTO();
            GatewayTaskSchedular gate_scheduler = new GatewayTaskSchedular();
            String gateway_ips[] = formBean.getOrigination_ip().split(";");
            int ending = 0;
            int client_id = Integer.parseInt(error.getErrorMessage());
            for (String gateway_ip : gateway_ips) {
                try {
                    gate_dto.setGateway_ip(gateway_ip);
                    gate_dto.setGateway_name("G" + dto.getClient_id() + (ending > 0 ? ending : ""));
                    gate_dto.setGateway_type(0);
                    gate_dto.setGateway_status(0);
                    gate_dto.setClientId(client_id);
                    gate_dto.setOwner_id(-1);
                    gate_dto.setGateway_dst_port_h323(0);
                    gate_dto.setGateway_dst_port_sip(0);
                    gate_dto.setProtocol_id(1);
                    gate_dto.setEnable_radius(0);
                    error = gate_scheduler.addGatewayInformation(gate_dto);
                    ending++;
                } catch (Exception ex) {
                }
            }
            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(gate_scheduler.getGatewayDTO(Long.parseLong(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("gateway");
                ac_dto.setPrimaryKey("G" + dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            }

            /*-----------------------------------ADD DIALPLAN-------------------------------------------*/
            DialplanDTO dial_dto = new DialplanDTO();
            DialplanTaskSchedular dial_scheduler = new DialplanTaskSchedular();
            dial_dto.setDialplan_ani_translate("");
            if (formBean.getDest_no_translate() == 1) {
                dial_dto.setDialplan_dnis_translate("^" + dto.getPrefix() + "88(.*)/35\\1");
            } else {
                dial_dto.setDialplan_dnis_translate("");
            }
            dial_dto.setDialplan_hunt_mode(0);
            dial_dto.setDialplan_priority(0);
            dial_dto.setDialplan_capacity(0);
            dial_dto.setDialplan_enable(1);
            dial_dto.setDialplan_sched_type(0);
            dial_dto.setClient_id(-1);
            dial_dto.setDialplan_sched_tod_on("0000");
            dial_dto.setDialplan_sched_tod_off("0000");

            ArrayList<GatewayDTO> gateway_list = GatewayLoader.getInstance().getOperators();
            for (GatewayDTO gateway_dto : gateway_list) {

                String gateway_ids = "";
                ArrayList<DialplanDTO> gatewayList = DialplanLoader.getInstance().getGatewayList(login_dto);
                int gateways[] = new int[gatewayList.size()];
                for (int i = 0; i < gatewayList.size(); i++) {
                    ArrayList<GatewayDTO> g_list = GatewayLoader.getInstance().getOperators(gateway_dto.getId());
                    for (GatewayDTO g_dto : g_list) {
                        DialplanDTO d_dto = gatewayList.get(i);
                        if (g_dto.getGateway_name().equalsIgnoreCase(d_dto.getGateway_name_single())) {
                            gateways[i] = d_dto.getGateway_id_single();
                            gateway_ids += d_dto.getGateway_id_single() + ";";
                        }
                    }
                }
                if (gateway_ids.length() > 0) {
                    dial_dto.setGateway_id(gateways);
                    dial_dto.setDialplan_gateway_list(gateway_ids);

                    dial_dto.setDialplan_name("D" + dto.getClient_id() + gateway_dto.getOp_name());
                    dial_dto.setDialplan_description("D" + dto.getClient_id() + gateway_dto.getOp_name());
                    dial_dto.setDialplan_dnis_pattern(dto.getPrefix() + gateway_dto.getPrefix() + "[0-9]*;");
                    error = dial_scheduler.addDialplanInformation(dial_dto);
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(dial_scheduler.getDialplanDTO(Integer.parseInt(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("dialpeer");
                ac_dto.setPrimaryKey("D" + dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            }
            /*-----------------------------------------------------------------------------------------------------*/

            if (error.getErrorType() <= 0) {
                formBean.setMessage(false, "Client is added successfully.");
                String print = new String();
                print = "Client ID: " + dto.getClient_id() + "<div class='clear'></div>"
                        + " Client Name: " + dto.getClient_name() + "<div class='clear'></div>"
                        + " Origination Gateway Name: " + gate_dto.getGateway_name() + "<div class='clear'></div>"
                        + " Origination Gateway IP: " + gate_dto.getGateway_ip() + "<div class='clear'></div>"
                        + " Rate Plan Name: " + rplan_dto.getRateplan_name() + "<div class='clear'></div>"
                        + " Rate Destination Name: " + rate_dto.getRate_destination_name() + "<div class='clear'></div>"
                        + " Prefix: " + dto.getPrefix();
                request.setAttribute(mapping.getAttribute(), formBean);
                request.getSession(true).setAttribute("print", print);
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
