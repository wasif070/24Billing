/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.gateway.GatewayDTO;
import com.myapp.struts.gateway.GatewayLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.AppConstants;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.apache.log4j.Logger;
import remotedbconnector.DBConnection;

/**
 *
 * @author reefat
 */
public class CapacityGroupLoader {
    
    static Logger logger = Logger.getLogger(CapacityGroupLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private long loadingTime_Client = 0;
    static CapacityGroupLoader capacityGrpLoader;
    private ArrayList<CapacityGroupDTO> capacityGrpList;
    private ArrayList<CapacityGroupDTO> clientcapacityGrpList;
    private HashMap<Integer, CapacityGroupDTO> capacityGrpDTOByID = null;
    private HashMap<String, CapacityGroupDTO> clientcapacityGrpDTOByID = null;
    PreparedStatement ps = null;
    private ArrayList<CapacityGroupDTO> clientList = null;
    private ArrayList<ClientDTO> clientDTOList = null;
    private ArrayList<GatewayDTO> gatewayDTOList = null;
    
    public static CapacityGroupLoader getInstance() {
        if (capacityGrpLoader == null) {
            createUserLoader();
        }
        return capacityGrpLoader;
    }

    private synchronized static void createUserLoader() {
        if (capacityGrpLoader == null) {
            capacityGrpLoader = new CapacityGroupLoader();
        }
    }  
    
    public ArrayList<CapacityGroupDTO> getCapacityGrpDTOsWithSearchParam(CapacityGroupDTO udto) {
        ArrayList newList = null;
        checkForReload();
        ArrayList<CapacityGroupDTO> list = capacityGrpList;
        if (list != null && list.size() > 0) {
            newList = new ArrayList();
            Iterator i = list.iterator();
            while (i.hasNext()) {
                CapacityGroupDTO dto = (CapacityGroupDTO) i.next();
                if (udto.searchWithUserID && !dto.getGroup_name().toLowerCase().startsWith(udto.getGroup_name().toLowerCase())) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }
    
    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }
    
    private void reload() {
        DBConnection dbConn = null;
        //Statement statement = null;
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            Statement remoteSTMT = null;
            capacityGrpDTOByID = new HashMap<Integer, CapacityGroupDTO>();
            //userDTOByID = new HashMap<Long, CapacityGroupDTO>();
            //clientList = new ArrayList<UserDTO>();
            capacityGrpList = new ArrayList<CapacityGroupDTO>();
            clientList = new ArrayList<CapacityGroupDTO>();
            
            
            
            String sql = "select * from mvts_capacity_group order by group_id ASC";
            ps = dbConn.connection.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                CapacityGroupDTO dto = new CapacityGroupDTO();
                
                dto.setGroup_id(resultSet.getInt("group_id"));
                dto.setGroup_name(resultSet.getString("group_nm"));
                dto.setDescription(resultSet.getString("description"));
                
                if(resultSet.getInt("parent_group_id")!=0){
                    dto.setParent_group_id(String.valueOf(resultSet.getInt("parent_group_id")));
                    CapacityGroupDTO tempDTO = new CapacityGroupDTO();
                    tempDTO =  getCapacityGroupDTOByID(resultSet.getInt("parent_group_id"));
                    dto.setParent_group_name(tempDTO.getGroup_name());                    
                }
                
                
                dto.setCapacity(resultSet.getInt("capacity"));
                capacityGrpDTOByID.put(dto.getGroup_id(), dto);
                //capacityGrpDTOByID.put(dto.getUserId(), dto);
                capacityGrpList.add(dto);
            }
            resultSet.close();

        } catch (Exception e) {
            logger.fatal("Exception in UserLoader:" + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
    } 
    
    public synchronized ArrayList<CapacityGroupDTO> getCapacityGrpDTOList() {
        checkForReload();
        return capacityGrpList;
    } 
     
    public synchronized CapacityGroupDTO getCapacityGroupDTOByID(int id) {
        checkForReload();
        return capacityGrpDTOByID.get(id);
    } 
     
    public synchronized ArrayList<CapacityGroupDTO> getClientList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<CapacityGroupDTO> newList = new ArrayList<CapacityGroupDTO>();
        for (CapacityGroupDTO dto : clientList) {
//            if (dto.getGateway_owner_id() == login_dto.getOwn_id()) {
                newList.add(dto);
//            }
        }
        return newList;
    }
    
    public int checkCapacityGrpParentChildRelation(CapacityGroupDTO udto) {
       
        int parent_capacity = 0,parent_used_capacity = 0,count=0;
        int return_val = 0;
        checkForReload();
        ArrayList<CapacityGroupDTO> list = capacityGrpList;
        if (list != null && list.size() > 0) {
            
            Iterator i = list.iterator();
            while (i.hasNext()) {
                CapacityGroupDTO dto = (CapacityGroupDTO) i.next();
                if (udto.getParent_group_id()!=null && dto.getParent_group_id() != null && dto.getParent_group_id().equals(String.valueOf(udto.getParent_group_id()))){
                    parent_used_capacity = parent_used_capacity + dto.getCapacity();
                }
                
                if (udto.getParent_group_id()!=null && (dto.getGroup_id() == (Integer.parseInt(udto.getParent_group_id())))) {
                    parent_capacity = dto.getCapacity();
                }                
                
                if(udto.getGroup_name().equals(dto.getGroup_name()))
                    count++;
            }
            
            if(udto.getParent_group_id()!=null &&  Integer.parseInt(udto.getParent_group_id()) !=0 && (parent_capacity - parent_used_capacity < udto.getCapacity()) )
                return_val = 1;

            if(count > 0)
                return_val = 2;            
            
        }
        return return_val;
    }
    
    public int checkCapacityGrpParentLimit(CapacityGroupDTO udto) {
       
        int parent_capacity = 0,parent_used_capacity = 0,count=0,child_used_capacity = 0;
        int return_val = 0;
        checkForReload();
        ArrayList<CapacityGroupDTO> list = capacityGrpList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                CapacityGroupDTO dto = (CapacityGroupDTO) i.next();
                if (dto.getParent_group_id()!= null &&  Integer.parseInt(udto.getParent_group_id()) != 0 && Integer.parseInt(dto.getParent_group_id()) != 0 && dto.getParent_group_id().equals(String.valueOf(udto.getParent_group_id()))) {
                    parent_used_capacity = parent_used_capacity + dto.getCapacity();
                }  
                
                if (dto.getParent_group_id()!= null && Integer.parseInt(dto.getParent_group_id()) != 0 && dto.getParent_group_id().equals(String.valueOf(udto.getGroup_id()))) {
                    child_used_capacity = child_used_capacity + dto.getCapacity();
                }                
                
                if (Integer.parseInt(udto.getParent_group_id()) != 0 && dto.getGroup_id() == (Integer.parseInt(udto.getParent_group_id()))) {
                    parent_capacity = dto.getCapacity();
                }
                
                if(udto.getGroup_name().equals(dto.getGroup_name()) && udto.getGroup_id()!=dto.getGroup_id())
                    count++;                
            }
            
//            if(!(Integer.parseInt(udto.getParent_group_id()) == 0 && parent_capacity >= udto.getCapacity() && (parent_capacity-parent_used_capacity) >= udto.getCapacity()))
//                return_val = 1;
            if(Integer.parseInt(udto.getParent_group_id()) == 0 && child_used_capacity<=udto.getCapacity())
                return_val = 0;
            else if(parent_capacity >= udto.getCapacity() && (parent_capacity-parent_used_capacity) >= udto.getCapacity() && child_used_capacity<=udto.getCapacity())
                return_val = 0;            
            else
                return_val = 1;
            
            if(count > 0)
                return_val = 2;             
        }
        return return_val;
    }
    
    public ArrayList<CapacityGroupDTO> getClientCapacityGrpDTOs(LoginDTO l_dto) {
        checkForClientReload(l_dto);
        return clientcapacityGrpList;
    }
    
    private void checkForClientReload(LoginDTO l_dto) {
        long current_time = System.currentTimeMillis();
        if (current_time - loadingTime_Client > LOADING_INTERVAL) {
            loadingTime_Client = current_time;
            ReloadClientCapacityGrpDTOs(l_dto);
        }
    }    
    
    public synchronized void ForceClientReload(LoginDTO l_dto) {
            ReloadClientCapacityGrpDTOs(l_dto);        
    }     
    
    public void ReloadClientCapacityGrpDTOs(LoginDTO l_dto) {
        

        DBConnection dbConn = null;
        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            Statement remoteSTMT = null;
            clientcapacityGrpList = new ArrayList<CapacityGroupDTO>();
            clientcapacityGrpDTOByID = new HashMap<String, CapacityGroupDTO>();
            
            String sql = "select src_address_list,dst_address,src_capacity_group,dst_capacity_group,group_nm " + 
                        " from mvts_gateway Left join mvts_capacity_group " + 
                        " on ((mvts_gateway.src_capacity_group = mvts_capacity_group.group_id) OR (mvts_gateway.dst_capacity_group = mvts_capacity_group.group_id))";
            ps = dbConn.connection.prepareStatement(sql);

            ResultSet resultSet = ps.executeQuery(sql);
            
            clientDTOList = new ArrayList<ClientDTO>();
            gatewayDTOList = new ArrayList<GatewayDTO>();
            clientDTOList = ClientLoader.getInstance().getClientDTOList(l_dto);
            GatewayLoader.getInstance().forceReload();
            gatewayDTOList = GatewayLoader.getInstance().getGatewayDTOList(l_dto);
            
            
            while (resultSet.next()) {
                CapacityGroupDTO dto = new CapacityGroupDTO();
                
                if(resultSet.getString("src_address_list") != null)
                dto.setCg_org_gateway_ip(resultSet.getString("src_address_list"));
                
                if(resultSet.getString("dst_address") != null)
                dto.setCg_term_gateway_ip(resultSet.getString("dst_address"));
                
                if(resultSet.getString("src_capacity_group") != null)
                dto.setCg_org_capacity_group(resultSet.getString("src_capacity_group"));
                
                if(resultSet.getString("dst_capacity_group") != null)
                dto.setCg_term_capacity_group(resultSet.getString("dst_capacity_group"));
                                
                if(resultSet.getString("group_nm") != null)
                dto.setGroup_name(resultSet.getString("group_nm"));
                
                Iterator i = gatewayDTOList.iterator();
                while(i.hasNext()){
                    GatewayDTO gDTO = (GatewayDTO) i.next(); 
                    if(dto.getCg_org_gateway_ip().equals(gDTO.getGateway_ip()) || dto.getCg_term_gateway_ip().equals(gDTO.getGateway_ip())){
                        dto.setCg_gateway_type(gDTO.getGateway_type_name());
                        Iterator in = clientDTOList.iterator();
                        while(in.hasNext()){
                            
                            ClientDTO cDTO = (ClientDTO) in.next();
                            if(cDTO.getId()==gDTO.getClientId()){
                                dto.setCg_client_id(cDTO.getClient_id());
                                String temp = "";
                                
                                if(cDTO.getClient_type()==0)
                                    temp = "Origination";
                                else if(cDTO.getClient_type()==1)
                                    temp = "Termination";
                                else if(cDTO.getClient_type() == 2)
                                    temp = "Both";
                                                
                                dto.setCg_client_type(temp);
                            }
                        }
                        
                    }
                }
                
                clientcapacityGrpList.add(dto);
                if(dto.getCg_gateway_type().equals("Termination"))
                    clientcapacityGrpDTOByID.put(dto.getCg_term_gateway_ip(), dto);
                else
                    clientcapacityGrpDTOByID.put(dto.getCg_org_gateway_ip(), dto);
            }
            resultSet.close();

        } catch (Exception e) {
            logger.fatal("Exception in UserLoader:" + e);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }        
    }
    
    public synchronized CapacityGroupDTO getClientCapacityGrpDTOByID(LoginDTO l_dto,String gw_id) {
        checkForClientReload(l_dto);
        return clientcapacityGrpDTOByID.get(gw_id);
    }
    
    public ArrayList<CapacityGroupDTO> getClientCapacityGrpDTOsWithSearchParam(CapacityGroupDTO udto, LoginDTO login_dto) {
        ArrayList<CapacityGroupDTO> newList = new ArrayList<CapacityGroupDTO>();
        checkForClientReload(login_dto);
        if (clientcapacityGrpList != null && clientcapacityGrpList.size() > 0) {
            Iterator i = clientcapacityGrpList.iterator();
            while (i.hasNext()) {
                CapacityGroupDTO dto = (CapacityGroupDTO) i.next();
                if ((udto.searchWithUserID && !dto.getCg_client_id().toLowerCase().startsWith(udto.getCg_client_id()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }    
    
}
