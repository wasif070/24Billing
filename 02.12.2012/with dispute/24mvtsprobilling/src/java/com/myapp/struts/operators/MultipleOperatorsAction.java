/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class MultipleOperatorsAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            boolean actionSuccessful = false;
            String actionName = Constants.EDIT_ACTION;
            ArrayList<OperatorDTO> list = new ArrayList<OperatorDTO>();

            OperatorsForm formBean = (OperatorsForm) form;
            OperatorDTO dto = new OperatorDTO();
            OperatorTaskScheduler scheduler = new OperatorTaskScheduler();

            MyAppError error = new MyAppError();

            if (formBean.getDeleteBtn() != null && formBean.getDeleteBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    for (long id : formBean.getSelectedIDs()) {
                        OperatorDTO DTO = scheduler.getOperatorDTO((int) id);
                        DTO.setIs_deleted(1);
                        list.add(DTO);
                    }
                    error = scheduler.deleteMultiple(formBean.getSelectedIDs());
                    actionSuccessful = true;
                    actionName = Constants.DELETE_ACTION;                    
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please select a specific Operator!");
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(error.getErrorMessage());
            } else {
                actionSuccessful = true;
                if (actionSuccessful) {
                    if (actionName.equals(Constants.EDIT_ACTION)) {
                        list = new ArrayList<OperatorDTO>();
                        for (long id : formBean.getSelectedIDs()) {
                            list.add(scheduler.getOperatorDTO((int) id));
                        }
                    }
                    for (OperatorDTO DTO : list) {
                        try {
                            Gson json = new GsonBuilder().serializeNulls().create();
                            ActivityDTO ac_dto = new ActivityDTO();
                            ac_dto.setUserId(login_dto.getClientId());
                            ac_dto.setChangedValue(json.toJson(DTO));
                            ac_dto.setActionName(actionName);
                            ac_dto.setTableName("operators");
                            ac_dto.setPrimaryKey(DTO.getOp_prefix());
                            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                            activityTaskScheduler.addActivityDTO(ac_dto);
                        } catch (Exception e) {
                            //logger.debug("Exception during activity log-->" + e);
                        }
                    }
                }
                formBean.setMessage(error.getErrorMessage());
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
                
    }
    
}
