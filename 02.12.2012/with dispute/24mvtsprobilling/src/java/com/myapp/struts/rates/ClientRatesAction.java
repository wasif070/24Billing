/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rates;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Ashraful
 */
public class ClientRatesAction extends Action {

    static Logger logger = Logger.getLogger(ListRateAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        long rateplan_id = 0;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && !login_dto.getSuperUser()) {
            ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(login_dto.getId());
            rateplan_id = cdto.getRateplan_id();
            request.getSession(true).setAttribute("sess_rate_id", rateplan_id);
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            if (request.getParameter("d-49216-p") != null) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            RateTaskSchedular scheduler = new RateTaskSchedular();
            RateForm rateForm = (RateForm) form;

            if (list_all == 0) {
                if (rateForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, rateForm.getRecordPerPage());
                }
                RateDTO dto = new RateDTO();
                if (rateForm.getRate_destination_code() != null) {
                    dto.searchWithPrefixName = true;
                    dto.setRate_destination_code(rateForm.getRate_destination_code());
                }
                rateForm.setRateList(scheduler.getSearchRateDTOs(rateplan_id, login_dto, dto));
            } else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    rateForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                rateForm.setRateList(scheduler.getRateDTOs(rateplan_id, login_dto));
            }

            if (rateForm.getRateList() != null && rateForm.getRateList().size() <= (rateForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                logger.debug("AAAA" + rateForm.getRateList().size());
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}