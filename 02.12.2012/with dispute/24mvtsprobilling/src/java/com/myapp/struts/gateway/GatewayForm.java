package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class GatewayForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private int doValidate;
    private long id;
    private String gateway_name;
    private String gateway_ip;
    private int gateway_status;
    private int gateway_type;
    private int clientId;
    private int gateway_dst_port_h323;
    private int gateway_dst_port_sip;
    private int mvts_id;
    private String mvts_name;
    private String message;
    private long[] selectedIDs;
    private ArrayList gatewayList;
    private String activateBtn;
    private String inactiveBtn;
    private String blockBtn;
    private String deleteBtn;
    private long switchGatewayId;
    private int jgateway_type;
    private int parent_id;
    private int owner_id;
    private String prev_gateway_name;
    private int protocol_id;
    private int enable_radius;
    private int prev_gateway_type;
    private String prefix;
    private int[] prefixes;
    private String src_dnis_prefix_allow;
    private String client_name;

    public GatewayForm() {
        super();
        gateway_type = -1;
        gateway_status = -1;
        owner_id = -1;
        enable_radius = 1;
        if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("POST_PAID").getSettingValue()) == AppConstants.YES) {
            enable_radius = 0;
        } else {
            enable_radius = 1;
        }

//        ArrayList<GatewayDTO> list = GatewayLoader.getInstance().getOperators();
//        prefixes = new int[list.size()];
//        int i = 0;
//        for (GatewayDTO dto : list) {
//            prefixes[i++] = (int) dto.getId();
//        }

    }

    public int getJgateway_type() {
        return jgateway_type;
    }

    public void setJgateway_type(int jgateway_type) {
        this.jgateway_type = jgateway_type;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public ArrayList getGatewayList() {
        return gatewayList;
    }

    public void setGatewayList(ArrayList gatewayList) {
        this.gatewayList = gatewayList;
    }

    public String getGateway_name() {
        return gateway_name;
    }

    public void setGateway_name(String gateway_name) {
        this.gateway_name = gateway_name;
    }

    public int getGateway_status() {
        return gateway_status;
    }

    public void setGateway_status(int gateway_status) {
        this.gateway_status = gateway_status;
    }

    public int getGateway_type() {
        return gateway_type;
    }

    public void setGateway_type(int gateway_type) {
        this.gateway_type = gateway_type;
    }

    public String getGateway_ip() {
        return gateway_ip;
    }

    public void setGateway_ip(String gateway_ip) {
        this.gateway_ip = gateway_ip;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getGateway_dst_port_h323() {
        return gateway_dst_port_h323;
    }

    public void setGateway_dst_port_h323(int gateway_dst_port_h323) {
        this.gateway_dst_port_h323 = gateway_dst_port_h323;
    }

    public int getGateway_dst_port_sip() {
        return gateway_dst_port_sip;
    }

    public void setGateway_dst_port_sip(int gateway_dst_port_sip) {
        this.gateway_dst_port_sip = gateway_dst_port_sip;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public String getMessage() {
        return message;
    }

    public String getActivateBtn() {
        return activateBtn;
    }

    public void setActivateBtn(String activateBtn) {
        this.activateBtn = activateBtn;
    }

    public String getBlockBtn() {
        return blockBtn;
    }

    public void setBlockBtn(String blockBtn) {
        this.blockBtn = blockBtn;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public String getInactiveBtn() {
        return inactiveBtn;
    }

    public void setInactiveBtn(String inactiveBtn) {
        this.inactiveBtn = inactiveBtn;
    }

    public int getMvts_id() {
        return mvts_id;
    }

    public void setMvts_id(int mvts_id) {
        this.mvts_id = mvts_id;
    }

    public String getMvts_name() {
        return mvts_name;
    }

    public void setMvts_name(String mvts_name) {
        this.mvts_name = mvts_name;
    }

    public long getSwitchGatewayId() {
        return switchGatewayId;
    }

    public void setSwitchGatewayId(long switchGatewayId) {
        this.switchGatewayId = switchGatewayId;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getPrev_gateway_name() {
        return prev_gateway_name;
    }

    public void setPrev_gateway_name(String prev_gateway_name) {
        this.prev_gateway_name = prev_gateway_name;
    }

    public int getProtocol_id() {
        return protocol_id;
    }

    public void setProtocol_id(int protocol_id) {
        this.protocol_id = protocol_id;
    }

    public int getEnable_radius() {
        return enable_radius;
    }

    public void setEnable_radius(int enable_radius) {
        this.enable_radius = enable_radius;
    }

    public int getPrev_gateway_type() {
        return prev_gateway_type;
    }

    public void setPrev_gateway_type(int prev_gateway_type) {
        this.prev_gateway_type = prev_gateway_type;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public int[] getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(int[] prefixes) {
        this.prefixes = prefixes;
    }

    public String getSrc_dnis_prefix_allow() {
        return src_dnis_prefix_allow;
    }

    public void setSrc_dnis_prefix_allow(String src_dnis_prefix_allow) {
        this.src_dnis_prefix_allow = src_dnis_prefix_allow;
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }
    
    
    

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
        ActionErrors errors = new ActionErrors();
        if (this.getDoValidate() == Constants.CHECK_VALIDATION) {
            if (getGateway_ip() == null || getGateway_ip().length() < 1) {
                errors.add("gateway_ip", new ActionMessage("errors.gateway_ip.required"));
            } else if (Integer.parseInt(settings.getSettingValue()) == AppConstants.NO && !Utils.isValidIP(getGateway_ip())) {
                errors.add("gateway_ip", new ActionMessage("errors.invalid.gateway_ip"));
            }

            if (getGateway_name() == null || getGateway_name().length() < 1) {
                errors.add("gateway_name", new ActionMessage("errors.gateway_name.required"));
            }

            if (request.getParameter("gateway_type").length() < 1) {
                errors.add("gateway_type", new ActionMessage("errors.gateway_type.required"));
            }

            if (request.getParameter("clientId").length() < 1) {
                errors.add("clientId", new ActionMessage("errors.client_id.required"));
            }
            
            if (getGateway_type() < 0) {
                errors.add("gateway_type", new ActionMessage("errors.gateway_type.required"));
            }
            
            if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("CLIENT_PROVISIONING").getSettingValue()) == AppConstants.YES) {
                if (getGateway_type() > 0 && (getPrefixes() == null || getPrefixes().length == 0)) {
                    errors.add("prefixes", new ActionMessage("errors.required", new String("Operators selection")));
                }
            }
        }
        return errors;
    }
}
