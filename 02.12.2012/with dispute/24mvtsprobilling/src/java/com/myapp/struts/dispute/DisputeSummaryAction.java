/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import java.util.ArrayList;
import com.myapp.struts.util.AppConstants;

/**
 *
 * @author reefat
 */
public class DisputeSummaryAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String target = AppConstants.SUCCESS;        
        
        ArrayList<DisputeDTO> disputeList = new ArrayList<DisputeDTO>();                      
        DisputeForm disputeForm = (DisputeForm) form;   
        DisputeTaskScheduler disputeTaskScheduler = new DisputeTaskScheduler();       
        
        String startTime = disputeForm.getFromYear_summary() + "-" + disputeForm.getFromMonth_summary() + "-" + disputeForm.getFromDay_summary() + " " + "0:0:0";
        String endDate = disputeForm.getToYear_summary() + "-" + disputeForm.getToMonth_summary() + "-" + disputeForm.getToDay_summary() + " " + "23:59:59";
        int carrier_id = disputeForm.getCarrier_id_summary();      
        
        disputeList = disputeTaskScheduler.getDisputeDTOsWithSearchParameterDate("DisputeSummary",carrier_id,startTime, endDate);
        
        request.getSession(true).setAttribute("showDispute", disputeList);
        return mapping.findForward(target);
    }
    
}