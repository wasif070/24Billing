package com.myapp.struts.rates;

/**
 *
 * @author Ashraful
 */
public class RateDTO {

    public boolean searchWithPrefixName = false;
    private long rateplan_id;
    private long id;
    private long rate_id;
    private String rate_destination_code;
    private String rate_destination_name;
    private float rate_per_min;
    private int rate_first_pulse;
    private int rate_next_pulse;
    private int rate_grace_period;
    private int rate_failed_period;
    private int rate_status;
    private String rate_statusname;
    private long rate_date;
    private String rate_created_date;
    private String[] rate_day;
    private String[] rate_fromhour;
    private String[] rate_frommin;
    private String[] rate_tohour;
    private String[] rate_tomin;
    private int rate_sin_day;
    private int rate_sin_fromhour;
    private int rate_sin_frommin;
    private int rate_sin_tohour;
    private int rate_sin_tomin;
    private String[] ids;
    private long startingRow;
    private int limit;
    private String doSearch;
    private int sortItem;
    private int sortOrder;
    private int is_deleted;
    private String[] ratePerMin;
    private String[] firstPulse;
    private String[] nextPulse;
    private String[] gracePeriod;
    private String[] failedPeriod;
    private String formated_time_frame;

    public RateDTO() {
    }

    public String[] getIds() {
        return ids;
    }

    public void setIds(String[] ids) {
        this.ids = ids;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRate_destination_code() {
        return rate_destination_code;
    }

    public void setRate_destination_code(String rate_destination_code) {
        this.rate_destination_code = rate_destination_code;
    }

    public String getRate_destination_name() {
        return rate_destination_name;
    }

    public void setRate_destination_name(String rate_destination_name) {
        this.rate_destination_name = rate_destination_name;
    }

    public int getRate_first_pulse() {
        return rate_first_pulse;
    }

    public void setRate_first_pulse(int rate_first_pulse) {
        this.rate_first_pulse = rate_first_pulse;
    }

    public int getRate_grace_period() {
        return rate_grace_period;
    }

    public void setRate_grace_period(int rate_grace_period) {
        this.rate_grace_period = rate_grace_period;
    }

    public int getRate_failed_period() {
        return rate_failed_period;
    }

    public void setRate_failed_period(int rate_failed_period) {
        this.rate_failed_period = rate_failed_period;
    }

    public long getRate_id() {
        return rate_id;
    }

    public void setRate_id(long rate_id) {
        this.rate_id = rate_id;
    }

    public int getRate_next_pulse() {
        return rate_next_pulse;
    }

    public void setRate_next_pulse(int rate_next_pulse) {
        this.rate_next_pulse = rate_next_pulse;
    }

    public float getRate_per_min() {
        return rate_per_min;
    }

    public void setRate_per_min(float rate_per_min) {
        this.rate_per_min = rate_per_min;
    }

    public int getRate_status() {
        return rate_status;
    }

    public void setRate_status(int rate_status) {
        this.rate_status = rate_status;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRate_statusname() {
        return rate_statusname;
    }

    public void setRate_statusname(String rate_statusname) {
        this.rate_statusname = rate_statusname;
    }

    public long getRate_date() {
        return rate_date;
    }

    public void setRate_date(long rate_date) {
        this.rate_date = rate_date;
    }

    public String getRate_created_date() {
        return rate_created_date;
    }

    public void setRate_created_date(String rate_created_date) {
        this.rate_created_date = rate_created_date;
    }

    public String[] getRate_day() {
        return rate_day;
    }

    public void setRate_day(String[] rate_day) {
        this.rate_day = rate_day;
    }

    public String[] getRate_fromhour() {
        return rate_fromhour;
    }

    public void setRate_fromhour(String[] rate_fromhour) {
        this.rate_fromhour = rate_fromhour;
    }

    public String[] getRate_frommin() {
        return rate_frommin;
    }

    public void setRate_frommin(String[] rate_frommin) {
        this.rate_frommin = rate_frommin;
    }

    public String[] getRate_tohour() {
        return rate_tohour;
    }

    public void setRate_tohour(String[] rate_tohour) {
        this.rate_tohour = rate_tohour;
    }

    public String[] getRate_tomin() {
        return rate_tomin;
    }

    public void setRate_tomin(String[] rate_tomin) {
        this.rate_tomin = rate_tomin;
    }

    public int getRate_sin_day() {
        return rate_sin_day;
    }

    public void setRate_sin_day(int rate_sin_day) {
        this.rate_sin_day = rate_sin_day;
    }

    public int getRate_sin_fromhour() {
        return rate_sin_fromhour;
    }

    public void setRate_sin_fromhour(int rate_sin_fromhour) {
        this.rate_sin_fromhour = rate_sin_fromhour;
    }

    public int getRate_sin_frommin() {
        return rate_sin_frommin;
    }

    public void setRate_sin_frommin(int rate_sin_frommin) {
        this.rate_sin_frommin = rate_sin_frommin;
    }

    public int getRate_sin_tohour() {
        return rate_sin_tohour;
    }

    public void setRate_sin_tohour(int rate_sin_tohour) {
        this.rate_sin_tohour = rate_sin_tohour;
    }

    public int getRate_sin_tomin() {
        return rate_sin_tomin;
    }

    public void setRate_sin_tomin(int rate_sin_tomin) {
        this.rate_sin_tomin = rate_sin_tomin;
    }

    public String getDoSearch() {
        return doSearch;
    }

    public void setDoSearch(String doSearch) {
        this.doSearch = doSearch;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getSortItem() {
        return sortItem;
    }

    public void setSortItem(int sortItem) {
        this.sortItem = sortItem;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public long getStartingRow() {
        return startingRow;
    }

    public void setStartingRow(long startingRow) {
        this.startingRow = startingRow;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public String[] getFailedPeriod() {
        return failedPeriod;
    }

    public void setFailedPeriod(String[] failedPeriod) {
        this.failedPeriod = failedPeriod;
    }

    public String[] getFirstPulse() {
        return firstPulse;
    }

    public void setFirstPulse(String[] firstPulse) {
        this.firstPulse = firstPulse;
    }

    public String[] getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(String[] gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public String[] getNextPulse() {
        return nextPulse;
    }

    public void setNextPulse(String[] nextPulse) {
        this.nextPulse = nextPulse;
    }

    public String[] getRatePerMin() {
        return ratePerMin;
    }

    public void setRatePerMin(String[] ratePerMin) {
        this.ratePerMin = ratePerMin;
    }

    public String getFormated_time_frame() {
        return formated_time_frame;
    }

    public void setFormated_time_frame(String formated_time_frame) {
        this.formated_time_frame = formated_time_frame;
    }
}
