package com.myapp.struts.invoice;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import org.apache.log4j.Logger;

public class ListInvoiceAction extends Action {

    static Logger logger = Logger.getLogger(InvoiceLoader.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }
            InvoiceTaskSchedular scheduler = new InvoiceTaskSchedular();
            InvoiceForm gatewayForm = (InvoiceForm) form;
            if (list_all == 0) {
                if (gatewayForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, gatewayForm.getRecordPerPage());
                }
                InvoiceDto idto = new InvoiceDto();
                if (gatewayForm.getClient() != null && gatewayForm.getClient().trim().length() > 0) {
                    idto.searchWithClientID = true;
                    idto.setClient(gatewayForm.getClient().toLowerCase());
                }
                if (gatewayForm.getId() > 0) {
                    idto.searchWithInvoiceNumber = true;
                    idto.setId(gatewayForm.getId());
                }
                gatewayForm.setInvoicelist(scheduler.getInvoiceDTOsWithSearchParam(idto, login_dto));
            } else {
                logger.debug("list Invoice--->");
                gatewayForm.setInvoicelist(scheduler.getGatewayDTOs(login_dto));
                request.getSession(true).setAttribute(mapping.getAttribute(), gatewayForm);
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
