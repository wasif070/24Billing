/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class ListClientCapacityGroupAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {
            int list_all = 0;
            int pageNo = 1;
            if (request.getParameter("list_all") != null) {
                list_all = Integer.parseInt(request.getParameter("list_all"));
            }
            boolean status = Utils.IntegerValidation(request.getParameter("d-49216-p"));
            if (status == true) {
                pageNo = Integer.parseInt(request.getParameter("d-49216-p"));
            }

            CapacityGroupTaskScheduler scheduler = new CapacityGroupTaskScheduler();
            CapacityGroupForm capacitygrpForm = (CapacityGroupForm) form;


            if (list_all == 0) {
                if (capacitygrpForm.getRecordPerPage() > 0) {
                    request.getSession(true).setAttribute(Constants.SESS_RECORD_PER_PAGE, capacitygrpForm.getRecordPerPage());
                }
                CapacityGroupDTO udto = new CapacityGroupDTO();
                if (capacitygrpForm.getClientId() != null && capacitygrpForm.getClientId().trim().length() > 0) {
                    udto.searchWithUserID = true;
                    udto.setCg_client_id(capacitygrpForm.getClientId().toLowerCase());
                }
                capacitygrpForm.setClientcapacityGrpList(scheduler.getClientCapacityGrpDTOsWithSearchParam(udto, login_dto));
            } 
            else {
                if (request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE) != null) {
                    capacitygrpForm.setRecordPerPage(Integer.parseInt(request.getSession(true).getAttribute(Constants.SESS_RECORD_PER_PAGE).toString()));
                }
                
                capacitygrpForm.setClientcapacityGrpList(scheduler.getClientCapacityGrpDTOs(login_dto));
                
            }
            //capacitygrpForm.setSelectedIDs(null);
            if (capacitygrpForm.getCapacityGrpList() != null && capacitygrpForm.getCapacityGrpList().size() <= (capacitygrpForm.getRecordPerPage() * (pageNo - 1))) {
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath() + "?d-49216-p=1", false);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
