package com.myapp.struts.gateway;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.dialplan.DialplanDTO;
import com.myapp.struts.dialplan.DialplanLoader;
import com.myapp.struts.dialplan.DialplanTaskSchedular;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class AddGatewayAction extends Action {

    static Logger logger = Logger.getLogger(AddGatewayAction.class.getName());

    @Override
    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            GatewayForm formBean = (GatewayForm) form;
            GatewayDTO dto = new GatewayDTO();
            GatewayTaskSchedular scheduler = new GatewayTaskSchedular();

            dto.setGateway_ip(formBean.getGateway_ip());
            dto.setGateway_name(formBean.getGateway_name());
            dto.setGateway_type(formBean.getGateway_type());
            dto.setGateway_status(formBean.getGateway_status());
            dto.setClientId(formBean.getClientId());
            dto.setOwner_id((int) login_dto.getOwn_id());
            dto.setGateway_dst_port_h323(formBean.getGateway_dst_port_h323());
            dto.setGateway_dst_port_sip(formBean.getGateway_dst_port_sip());
            dto.setProtocol_id(formBean.getProtocol_id());
            dto.setEnable_radius(formBean.getEnable_radius());
            dto.setPrefixes(formBean.getPrefixes());
            dto.setSrc_dnis_prefix_allow(formBean.getSrc_dnis_prefix_allow());

            MyAppError error = scheduler.addGatewayInformation(dto);

            if (formBean.getPrefixes() != null && formBean.getPrefixes().length > 0) {
                GatewayDTO gateway_dto = GatewayLoader.getInstance().getGatewayDTOByID(Long.parseLong(error.getErrorMessage()));
                ArrayList<DialplanDTO> d_list = DialplanLoader.getInstance().getDialplanDTOList(login_dto);
                ArrayList<String> added_ops = new ArrayList<String>();
                for (int cur_op_id : formBean.getPrefixes()) {
                    added_ops.add(GatewayLoader.getInstance().getOperator(cur_op_id).getPrefix());
                }

                ArrayList<String> added_dps = new ArrayList<String>();
                for (String prefix : added_ops) {
                    for (DialplanDTO dial_dto : d_list) {
                        if (dial_dto.getDialplan_dnis_pattern_without_modify().contains(prefix + "[")) {
                            added_dps.add(dial_dto.getDialplan_name());
                        }
                    }
                }

                DialplanTaskSchedular d_task_scheduler = new DialplanTaskSchedular();
                d_task_scheduler.updateDialpeer(added_dps, gateway_dto.getSwitchGatewayId(), false);
            }


            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {

                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getGatewayDTO(Long.parseLong(error.getErrorMessage()))));
                ac_dto.setActionName(Constants.ADD_ACTION);
                ac_dto.setTableName("gateway");
                ac_dto.setPrimaryKey(dto.getGateway_name());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "Gateway is added successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
