/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;
import com.myapp.struts.session.Constants;
/**
 *
 * @author reefat
 */
public class CarrierColumnMappingSubmitAction extends Action {
    static Logger logger = Logger.getLogger(CarrierColumnMappingSubmitAction.class.getName());
    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String target = "success";
        int result_msg = 0;
            
        if (request.getParameter("result_msg") != null) {
            result_msg = Integer.parseInt(request.getParameter("list_all"));
        }
        
        DisputeForm disputeform = (DisputeForm) form;
        DisputeTaskScheduler dispute_scheduler = new DisputeTaskScheduler();
        String[] xx = disputeform.getOur_db_columns();        
        String result = dispute_scheduler.insertCarrierDataForDispute(xx);
        
        if(result_msg!=0){
            request.getSession(true).setAttribute(Constants.RESULT_MESSAGE,null);
        }
        else            
        request.getSession(true).setAttribute(Constants.RESULT_MESSAGE, result);        
        
        return (mapping.findForward(target)); 
    }
    
}