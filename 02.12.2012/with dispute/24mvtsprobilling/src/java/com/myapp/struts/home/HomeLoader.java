/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.home;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.Utils;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class HomeLoader {

    static Logger logger = Logger.getLogger(HomeLoader.class.getName());
    private ArrayList<HomeDTO> orgList = null;
    private ArrayList<HomeDTO> termList = null;
    static HomeLoader homeLoader = null;
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();

    public HomeLoader() {
    }

    public static HomeLoader getInstance() {
        if (homeLoader == null) {
            createHomeLoader();
        }
        return homeLoader;
    }

    private synchronized static void createHomeLoader() {
        if (homeLoader == null) {
            homeLoader = new HomeLoader();
        }
    }

    private void reload_org(LoginDTO login_dto) {
    }

    public synchronized ArrayList<HomeDTO> getOrgDTOList(LoginDTO login_dto) {
        long start_time = System.currentTimeMillis();
        String today_00_hour = Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            String children = ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id());
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            orgList = new ArrayList<HomeDTO>();
            HomeDTO dto = new HomeDTO();
            String sql = "select origin_client_id,sum(duration),count(cdr_id) from flamma_cdr where connection_time>='" + today_00_hour + "' and duration>0 and origin_client_id in(" + children + ") group by origin_client_id";
            logger.debug("Org Sql: " + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dto = new HomeDTO();
                dto.setOrg_client_id(resultSet.getInt("origin_client_id"));
                try {
                    ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(dto.getOrg_client_id());
                    dto.setOrg_client_name(clientDTO.getClient_id());
                    dto.setOrg_client_balance(Double.parseDouble(clientDTO.getClient_balance()));
                } catch (Exception ex) {
                }
                dto.setOrg_calls(resultSet.getLong("count(cdr_id)"));
                dto.setOrg_duration(resultSet.getLong("sum(duration)"));
                orgList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in HomeLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }


        ArrayList<HomeDTO> newList = new ArrayList<HomeDTO>();
        ArrayList<Long> ids = new ArrayList<Long>();
        if (login_dto.getOwn_id() < 0) {
            String[] client_ids = ClientLoader.getInstance().getTopLevelClient().split(",");
            for (String id : client_ids) {
                ids.add(Long.parseLong(id));
            }
        }

        for (HomeDTO dto : orgList) {
            if (login_dto.getOwn_id() > 0) {
                ClientDTO c_dto = ClientLoader.getInstance().getClientDTOByID(dto.getOrg_client_id());
                if (c_dto != null && c_dto.getParent_id() == login_dto.getOwn_id()) {
                    newList.add(dto);
                }
            } else {
                if (ids.contains((long) dto.getOrg_client_id())) {
                    newList.add(dto);
                }
            }
        }
        logger.debug("Org Elapsed time-->" + (System.currentTimeMillis() - start_time));
        return newList;
    }

    public synchronized ArrayList<HomeDTO> getTermDTOList(LoginDTO login_dto) {
        long start_time = System.currentTimeMillis();
        String today_00_hour = Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        DBConnection dbConnection = null;
        Statement statement = null;
        try {
            String children = ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id());
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            HomeDTO dto = new HomeDTO();
            String sql = "select term_client_id,sum(duration),count(cdr_id) from flamma_cdr where connection_time>='" + today_00_hour + "' and duration>0 and term_client_id in(" + children + ") and client_level!=-1 group by term_client_id ";
            logger.debug("Term Sql: " + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            termList = new ArrayList<HomeDTO>();
            while (resultSet.next()) {
                dto = new HomeDTO();
                dto.setTerm_client_id(resultSet.getInt("term_client_id"));
                try {
                    ClientDTO clientDTO = ClientLoader.getInstance().getClientDTOByID(dto.getTerm_client_id());
                    dto.setTerm_client_name(clientDTO.getClient_id());
                    dto.setTerm_client_balance(Double.parseDouble(clientDTO.getClient_balance()));
                } catch (Exception ex) {
                }
                dto.setTerm_calls(resultSet.getLong("count(cdr_id)"));
                dto.setTerm_duration(resultSet.getLong("sum(duration)"));
                termList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in HomeLoader:" + e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }


        ArrayList<HomeDTO> newList = new ArrayList<HomeDTO>();
        ArrayList<Long> ids = new ArrayList<Long>();

        if (login_dto.getOwn_id() < 0) {
            String[] client_ids = ClientLoader.getInstance().getTopLevelClient().split(",");
            for (String id : client_ids) {
                ids.add(Long.parseLong(id));
            }
        }

        for (HomeDTO dto : termList) {
            if (login_dto.getOwn_id() > 0) {
                ClientDTO c_dto = ClientLoader.getInstance().getClientDTOByID(dto.getTerm_client_id());
                if (c_dto != null && c_dto.getParent_id() == login_dto.getOwn_id()) {
                    newList.add(dto);
                }
            } else {
                if (ids.contains((long) dto.getTerm_client_id())) {
                    newList.add(dto);
                }
            }
        }
        logger.debug("Elapsed time-->" + (System.currentTimeMillis() - start_time));
        return newList;
    }
}
