/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.login;

import org.apache.log4j.Logger;

public class LoginLoader {

    static Logger logger = Logger.getLogger(LoginLoader.class.getName());
    static LoginLoader loader = null;
    private LoginDTO login_dto = null;

    public LoginLoader() {
    }

    public static LoginLoader getInstance() {
        if (loader == null) {
            createLoader();
        }
        return loader;
    }

    private synchronized static void createLoader() {
        if (loader == null) {
            loader = new LoginLoader();
        }
    }

    public LoginDTO getLoginDTO() {
        return login_dto;
    }

    public void setLoginDTO(LoginDTO p_dto) {
        login_dto = new LoginDTO();
        if (p_dto != null) {
            login_dto.setClientId(p_dto.getClientId());
            login_dto.setClientPassword(p_dto.getClientPassword());
            login_dto.setClientStatus(p_dto.getClientStatus());
            login_dto.setClientType(p_dto.getClientType());
            login_dto.setClient_balance(p_dto.getClient_balance());
            login_dto.setClient_level(p_dto.getClient_level());
            login_dto.setId(p_dto.getId());
            login_dto.setLoginTime(p_dto.getLoginTime());
            login_dto.setOwn_id(p_dto.getOwn_id());
            login_dto.setParent_id(p_dto.getParent_id());
            login_dto.setRole_id(p_dto.getRole_id());
            login_dto.setSuperUser(p_dto.getSuperUser());
            login_dto.setUser(p_dto.isUser());
        }
    }

    public static void main(String args[]) {
        LoginDTO dto = new LoginDTO();
        LoginLoader.getInstance().setLoginDTO(dto);
        System.out.println(LoginLoader.getInstance().getLoginDTO().getClient_level());
    }
}
