/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

/**
 *
 * @author reefat
 */
public class OperatorTaskScheduler {
    public MyAppError addRateplanInformation(OperatorDTO op_dto) {
        OperatorDAO opDAO = new OperatorDAO();
        return opDAO.addOperator(op_dto);
    }    
    
    public OperatorDTO getOperatorDTO(int id) {
        return OperatorLoader.getInstance().getoperatorDTOByID(id);
    } 
    
    public ArrayList<OperatorDTO> getOperatorDTOsWithSearchParam(OperatorDTO udto, LoginDTO l_dto) {
        return OperatorLoader.getInstance().getOperatorDTOsWithSearchParam(udto, l_dto);
    } 
    
    public ArrayList<OperatorDTO> getOperatorDTOs(LoginDTO l_dto) {
        return OperatorLoader.getInstance().getOperatorDTOList(l_dto);
    }
    public OperatorDTO getRateplanDTO(int id) {
        return OperatorLoader.getInstance().getOperatorDTOByID(id);
    }    
    
    public MyAppError deleteMultiple(long destcodeIds[]) {
        OperatorDAO dao = new OperatorDAO();
        return dao.multipleDelete(destcodeIds);
    }  
    
    public MyAppError editOperatorInformation(OperatorDTO p_dto) {
        OperatorDAO opDAO = new OperatorDAO();
        return opDAO.editOperatorInformation(p_dto);
    }    
    
    public MyAppError deleteOperator(OperatorDTO p_dto) {
        OperatorDAO opDAO = new OperatorDAO();
        return opDAO.deleteOperator(p_dto);
    }    
}
