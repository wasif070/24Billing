/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.user;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.clients.ClientDAO;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

/**
 *
 * @author Administrator
 */
public class DeleteUserAction extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        long cid = Long.parseLong(request.getParameter("id"));
        UserTaskSchedular rt = new UserTaskSchedular();
        UserDTO dto=rt.getUserDTO(cid);

        MyAppError error = new MyAppError();
        error = rt.deleteUser((int)cid);
        
        dto.setIs_deleted(1);
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        Gson json = new GsonBuilder().serializeNulls().create();
        ActivityDTO ac_dto = new ActivityDTO();
        ac_dto.setUserId(login_dto.getClientId());
        ac_dto.setChangedValue(json.toJson(rt.getUserDTO(cid)));
        ac_dto.setActionName(Constants.DELETE_ACTION);
        ac_dto.setTableName("user");
        ac_dto.setPrimaryKey(dto.getUserId());
        ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
        activityTaskScheduler.addActivityDTO(ac_dto);
        
        request.getSession(true).setAttribute(Constants.MESSAGE, "User Deleted Successfully!");

        return mapping.findForward("success");
    }
}