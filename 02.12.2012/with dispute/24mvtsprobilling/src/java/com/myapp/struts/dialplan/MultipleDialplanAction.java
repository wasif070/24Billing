package com.myapp.struts.dialplan;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class MultipleDialplanAction extends Action {

    static Logger logger = Logger.getLogger(MultipleDialplanAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientType();
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
        if (login_dto != null && login_dto.getSuperUser() && Integer.parseInt(settings.getSettingValue()) == AppConstants.NO) {
            boolean actionSuccessful = false;
            String actionName = Constants.EDIT_ACTION;
            ArrayList<DialplanDTO> list = new ArrayList<DialplanDTO>();
            DialplanForm formBean = (DialplanForm) form;
            DialplanTaskSchedular scheduler = new DialplanTaskSchedular();
            MyAppError error = new MyAppError();

            if (formBean.getActivateBtn() != null && formBean.getActivateBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.activateMultiple(formBean.getSelectedIDs());
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    } else {
                        actionSuccessful = true;
                        formBean.setMessage(false, "Dialplan(s) is/are activated successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a dialplan!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (formBean.getInactiveBtn() != null && formBean.getInactiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.inactivateMultiple(formBean.getSelectedIDs());
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    } else {
                        actionSuccessful = true;
                        formBean.setMessage(false, "Dialplan(s) is/are inactivated successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a dialplan!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (formBean.getDeleteBtn() != null && formBean.getDeleteBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    for (long id : formBean.getSelectedIDs()) {
                        DialplanDTO DTO = scheduler.getDialplanDTO((int) id);
                        DTO.setIs_deleted(1);
                        list.add(DTO);
                    }
                    error = scheduler.deleteMultiple(formBean.getSelectedIDs());
                    actionSuccessful = true;
                    actionName = Constants.DELETE_ACTION;
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please select at least a dialplan!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            } else {
                actionSuccessful = true;
                if (actionSuccessful) {
                    if (actionName.equals(Constants.EDIT_ACTION)) {
                        list = new ArrayList<DialplanDTO>();
                        for (long id : formBean.getSelectedIDs()) {
                            list.add(scheduler.getDialplanDTO((int) id));
                        }
                    }
                    for (DialplanDTO DTO : list) {
                        try {
                            Gson json = new GsonBuilder().serializeNulls().create();
                            ActivityDTO ac_dto = new ActivityDTO();
                            ac_dto.setUserId(login_dto.getClientId());
                            ac_dto.setChangedValue(json.toJson(DTO));
                            ac_dto.setActionName(actionName);
                            ac_dto.setTableName("dialpeer");
                            ac_dto.setPrimaryKey(DTO.getDialplan_name());
                            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                            activityTaskScheduler.addActivityDTO(ac_dto);
                        } catch (Exception e) {
                            logger.debug("Exception during activity log-->" + e);
                        }
                    }
                }
                formBean.setMessage(false, "Dialplan List is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
