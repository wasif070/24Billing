/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class DeleteClientAction extends Action {

    static Logger logger = Logger.getLogger(ClientDAO.class.getName());

    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        int cid = Integer.parseInt(request.getParameter("id"));
        ClientTaskSchedular rt = new ClientTaskSchedular();
        ClientDTO dto = rt.getClientDTO(cid);


        MyAppError error = new MyAppError();
        ArrayList<Long> children = ClientLoader.getInstance().getChildren(cid);
        if (children.size() > 0) {
            error.setErrorType(MyAppError.OtherError);
            error.setErrorMessage("This client has children. It can't be deleted.");
        } else {
            error = rt.deleteClient(cid);
            try {
                dto.setIs_deleted(1);
                LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(dto));
                ac_dto.setActionName(Constants.DELETE_ACTION);
                ac_dto.setTableName("clients");
                ac_dto.setPrimaryKey(dto.getClient_id());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);
            } catch (Exception ex) {
            }
        }
        ClientForm formBeForm = new ClientForm();
        formBeForm.setMessage(error.getErrorType() > 0 ? true : false, error.getErrorMessage());
        request.getSession(true).setAttribute(Constants.MESSAGE, formBeForm.getMessage());
        return mapping.findForward("success");
    }
}
