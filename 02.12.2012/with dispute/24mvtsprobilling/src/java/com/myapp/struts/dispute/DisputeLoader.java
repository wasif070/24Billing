/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import com.myapp.struts.login.LoginDTO;
import java.sql.Statement;
import databaseconnector.DBConnection;
import com.myapp.struts.util.Utils;
import java.sql.ResultSet;
import java.util.*;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
/**
 *
 * @author reefat
 */
public class DisputeLoader {

    private String fromDate;
    private String toDate;
    private int carrierID;
    
    private long temp;    
    private double v_difference;
    private static DisputeLoader disputLoader = null;
    private ArrayList<DisputeDTO> disputeSummaryList = null;    
    private HashMap<String, DisputeDTO> disputeSummaryHashMap = null;     
    private ArrayList<DisputeDTO> disputeListsemiFinal = null;
    private ArrayList<DisputeDTO> disputeListFinal = null;
    private ArrayList<DisputeDTO> disputeClientList;
    
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();
    
    public static DisputeLoader getInstance() {
        if (disputLoader == null) {
            CreateDisputeLoader();
        }
        return disputLoader;
    }

    private synchronized static void CreateDisputeLoader() {
        disputLoader = new DisputeLoader();
    }

    public synchronized ArrayList<DisputeDTO> getDisputeDTOList(LoginDTO login_dto) {
        reloadDisputeSummary();
        ArrayList<DisputeDTO> newList = new ArrayList<DisputeDTO>();
        for (DisputeDTO dto : disputeSummaryList) {
            newList.add(dto);
        }
        return newList;
    } 
    
    private void reloadDisputeSummary() {
        disputeSummaryList = new ArrayList<DisputeDTO>();
        DBConnection dbConnection = null;
        Statement statement = null;
        
//        databaseconnector.DBConnection dBConnection1 = null;

        ResultSet resultSet = null;
        String sql = "";
        try {
            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();                
                sql = "select count(1) as number_of_cdr,sum(duration) as total_duration from carrier_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "'";
                System.out.println("sql for carrier-->" + sql);
                resultSet = statement.executeQuery(sql);
                System.out.println("sql for carrier-->" + sql);
                while (resultSet.next()) {
                    DisputeDTO dto = new DisputeDTO();
                    System.out.println("dto values :: " + resultSet.getLong("number_of_cdr") + resultSet.getInt("number_of_cdr") + resultSet.getString("number_of_cdr"));
                    dto.setCdr_owner("Carrier");
                    dto.setNo_of_cdr_summary(resultSet.getLong("number_of_cdr"));
                    dto.setTotal_duration_summary(Utils.getTimeHHMMSS((int) resultSet.getLong("total_duration")));
                    disputeSummaryList.add(dto);
                }
            } catch (Exception ex) {
//                logger.debug("Exception in carrier data retrived-->"+ex);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
            try {
                dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
                statement = dbConnection.connection.createStatement();
                String condition_sql="";
                if(carrierID>0){
                    condition_sql = "and origin_client_id = " + carrierID;
                }                
                sql = "select count(cdr_id) as number_of_cdr,sum(duration) as total_duration from flamma_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "' " + condition_sql;
                resultSet = statement.executeQuery(sql);
                System.out.println("sql for our system-->" + sql);
                while (resultSet.next()) {
                    DisputeDTO dto = new DisputeDTO();
                    dto.setCdr_owner("VisionTel");
                    dto.setNo_of_cdr_summary(resultSet.getLong("number_of_cdr"));
                    dto.setTotal_duration_summary(Utils.getTimeHHMMSS(resultSet.getLong("total_duration")));
                    disputeSummaryList.add(dto);
                }
            } catch (Exception ex) {
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception ex) {
        }   
    } 
    
    public synchronized ArrayList<DisputeDTO> getDisputeDTOListWithSearchParameterDate(String type,int carrier_id,String startTDate, String endDate) {
         
        fromDate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(startTDate) - Utils.getTimeLong(timeZone));
        toDate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(endDate) - Utils.getTimeLong(timeZone));
        carrierID = carrier_id;
        if(type.equals("DisputeSummary"))
        reloadDisputeSummary();
        else if(type.equals("DisputeDatewiseSummary"))
        reloadDisputeDatewiseSummary();
 
        
        ArrayList<DisputeDTO> newList = new ArrayList<DisputeDTO>();
        if(disputeSummaryList==null)
            disputeSummaryList = new ArrayList<DisputeDTO>();
        for (DisputeDTO dto : disputeSummaryList) {
            newList.add(dto);
        }
        return newList;

    }   

    private void reloadDisputeDatewiseSummary() {
        
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;        
        disputeSummaryList = new ArrayList<DisputeDTO>();
        disputeSummaryHashMap = new HashMap<String, DisputeDTO>();        
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            
            String sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern,count(1) total_cdr_carrier,sum(duration) call_duration_carrier from carrier_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "' group by date_pattern;";
            resultSet = statement.executeQuery(sql);
            DisputeDTO dto = new DisputeDTO();
            while (resultSet.next()) {
                dto = new DisputeDTO();
                dto.setDate_datewise_summary(resultSet.getDate("date_pattern").toString());
                dto.setTotal_calls_ipvision_datewise_summary("-");
                dto.setTotal_duration_ipvision_datewise_summary("-");
                
                dto.setDate_datewise_summary_carrier(resultSet.getDate("date_pattern").toString());
                dto.setTotal_calls_carrier_datewise_summary(resultSet.getString("total_cdr_carrier"));
                dto.setTotal_duration_carrier_datewise_summary(Utils.getTimeHHMMSS(resultSet.getInt("call_duration_carrier")));
                
                disputeSummaryHashMap.put(dto.getDate_datewise_summary_carrier(), dto);
            }
            resultSet.close();
        } catch (Exception e) {
//            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
             
        }              
        try {
            String condition_sql="";
            if(carrierID>0){
                condition_sql = "and origin_client_id = " + carrierID;
            }
            
            String sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern_flamma,count(1) as number_of_cdr,sum(duration) as total_duration from flamma_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "'" + condition_sql + "  group by date_pattern_flamma;";
            resultSet = statement.executeQuery(sql);
            DisputeDTO dto = new DisputeDTO();
            System.out.println("before removal :: "+disputeSummaryHashMap.size());
            String date_temp = null;
            while (resultSet.next()) {
                date_temp = resultSet.getDate("date_pattern_flamma").toString();
                if(disputeSummaryHashMap.containsKey(date_temp)){
                    dto = disputeSummaryHashMap.get(date_temp);

                    disputeSummaryHashMap.remove(date_temp);
                    dto.setDate_datewise_summary(resultSet.getDate("date_pattern_flamma").toString());
                    dto.setTotal_calls_ipvision_datewise_summary(resultSet.getBigDecimal("number_of_cdr").toString());
                    dto.setTotal_duration_ipvision_datewise_summary(Utils.getTimeHHMMSS((int) resultSet.getLong("total_duration")));                    
                            
                    temp =  Utils.getTimeLong(dto.getTotal_duration_carrier_datewise_summary());
                    v_difference = temp - (double) resultSet.getLong("total_duration");
//                    double v_percentage;
//                    v_percentage = v_difference/temp;
                    dto.setDeviation_datewise_summary(String.valueOf(v_difference*100/temp));
                    disputeSummaryList.add(dto);                    
                }
                else{
                dto = new DisputeDTO();
                dto.setDate_datewise_summary(resultSet.getDate("date_pattern_flamma").toString());
                dto.setTotal_calls_ipvision_datewise_summary(resultSet.getBigDecimal("number_of_cdr").toString());
                dto.setTotal_duration_ipvision_datewise_summary(Utils.getTimeHHMMSS((int) resultSet.getLong("total_duration")));
                dto.setDate_datewise_summary_carrier(resultSet.getDate("date_pattern_flamma").toString());                
                dto.setTotal_calls_carrier_datewise_summary("-");                
                dto.setTotal_duration_carrier_datewise_summary("-");
                
//                disputeSummaryHashMap.put(dto.getDate_datewise_summary_carrier(), dto);
                disputeSummaryList.add(dto);
                }
//                int map_size = disputeSummaryHashMap.size();
                

            }

            Iterator iterator = disputeSummaryHashMap.keySet().iterator();
//                key_list = disputeSummaryHashMap.keySet();
            while(iterator.hasNext()){
                dto = disputeSummaryHashMap.get(iterator.next().toString());
                disputeSummaryList.add(dto);                     
            }            
            resultSet.close();
        } catch (Exception e) {
//            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
        }        
    }

    public synchronized ArrayList<DisputeDTO> getDisputeDTOListOfparticularDates(int carrier_id,String[] dateLists,LoginDTO login_dto) {
        disputeDTOLISTofSelectedDates(carrier_id,dateLists);
        ArrayList<DisputeDTO> newList = new ArrayList<DisputeDTO>();
        for (DisputeDTO dto : disputeListsemiFinal) {
            newList.add(dto);
        }
        return newList;

    } 
    
    
public void disputeDTOLISTofSelectedDates(int carrier_id,String[] dateLists){
    
        disputeListsemiFinal = new ArrayList<DisputeDTO>();
        disputeListFinal = new ArrayList<DisputeDTO>();
        String carrier_condition = "";
        carrierID = carrier_id;
        if(carrierID>0){
            carrier_condition = " origin_client_id = " + carrierID + " and ";
        }        
        String select_sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d %H:%i:%s') connection_time,dialed_no,terminated_no,duration from flamma_cdr where duration>0 and " + carrier_condition +" connection_time >=";
        String sub_sql = "";
        String selectedIdsString = Utils.implodeArrayName(dateLists, ",");
        String[] arr_str = null;
        arr_str = selectedIdsString.split(",");
        
        for(int i=0;i<arr_str.length;i++){
            
            sub_sql = sub_sql + select_sql +"'"+arr_str[i].substring(0, 10) +" " + arr_str[i].substring(11, 13) + ":00:00' and connection_time <=" +"'"+arr_str[i].substring(0, 10) +" " + arr_str[i].substring(11, 13) + ":59:59'";
            
            if((i+1)!=arr_str.length){
                sub_sql = sub_sql + " union ";                
            }
            else
                sub_sql = sub_sql + ";";
        }
        
        System.out.println("selected dates :: \n"+sub_sql);        
        
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;   
        Calendar cdr,cdr1,cdr2;
        cdr = Calendar.getInstance();
        cdr1 = Calendar.getInstance();
        cdr2 = Calendar.getInstance();

        //******************************try 001 (start)********************************
        try {
            try {
        dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
        statement = dbConnection.connection.createStatement();
//                sql = "select cdr_date,cdr_id,duration from flamma_cdr where cdr_date ='2010-1-1 0:0:0' and cdr_date <='2010-1-31 23:59:59' group by cdr_date";
            resultSet = statement.executeQuery(sub_sql);
//                logger.debug("group_by_date : sql for flamma_cdr-->" + sql);
            while (resultSet.next()) {
                    DisputeDTO dto = new DisputeDTO();
                    dto.setCdr_date_details(resultSet.getString("connection_time"));
                    dto.setCaller_number_details(resultSet.getString("dialed_no"));
                    dto.setCalled_number_details(resultSet.getString("terminated_no"));                    
                    dto.setDuration_ipvision_details(Utils.getTimeHHMMSS((int) resultSet.getLong("duration")).toString());
                    disputeListsemiFinal.add(dto);                        
                }
            } catch (Exception ex) {
//                logger.debug("no filter : Exception in carrier data retrived-->"+ex);
            } finally {
        }
            
        } catch (Exception ex) {
        }
        //******************************try 001 (end)********************************
        
        select_sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d %H:%i:%s') connection_time,dialed_no,terminated_no,duration from carrier_cdr where duration>0 and connection_time >=";
        sub_sql = "";
        
        for(int i=0;i<arr_str.length;i++){
            
            sub_sql = sub_sql + select_sql +"'"+arr_str[i].substring(0, 10) +" " + arr_str[i].substring(11, 13) + ":00:00' and connection_time <=" +"'"+arr_str[i].substring(0, 10) +" " + arr_str[i].substring(11, 13) + ":59:59'";
            
            if((i+1)!=arr_str.length){
                sub_sql = sub_sql + " union ";                
            }
            else
                sub_sql = sub_sql + ";";
        }
        
        System.out.println("selected dates 2 :: \n"+sub_sql);        
        
        //******************************try 002 (start)********************************
        try {
            try {
            resultSet = statement.executeQuery(sub_sql);
            int flag = 0;

            while (resultSet.next()) {
                    DisputeDTO dto = new DisputeDTO();
                    DisputeDTO dto_temp = new DisputeDTO();
                    dto_temp.setCdr_date_details(resultSet.getString("connection_time"));
                    dto_temp.setCaller_number_details(resultSet.getString("dialed_no"));
                    dto_temp.setCalled_number_details(resultSet.getString("terminated_no"));
                    flag = 0;
                    if(disputeListsemiFinal.size()>0){
                        for(int ind=0;ind<disputeListsemiFinal.size();ind++){

                            dto = disputeListsemiFinal.get(ind);

                            cdr = convertToCalenderObject(dto);
                            cdr1 = (Calendar)cdr.clone();
                            cdr2 = (Calendar)cdr.clone();    

                            cdr1.add(Calendar.SECOND, -30);
                            cdr2.add(Calendar.SECOND, 30);

                            cdr = convertToCalenderObject(dto_temp);

                            if( cdr1.before(cdr) && cdr2.after(cdr)                                                                
                                 && dto.getCaller_number_details().equals(dto_temp.getCaller_number_details()) 
                                 && dto.getCalled_number_details().equals(dto_temp.getCalled_number_details())){

                                    disputeListsemiFinal.get(ind).setDuration_carrier_details(Utils.getTimeHHMMSS((int) resultSet.getLong("duration")).toString());
                                    flag = 1;
                                    break;
                            }
                            if(ind==(disputeListsemiFinal.size()-1) && flag==0){
                                dto = new DisputeDTO();
                                dto.setCdr_date_details(resultSet.getString("connection_time"));
                                dto.setCaller_number_details(resultSet.getString("dialed_no"));
                                dto.setCalled_number_details(resultSet.getString("terminated_no"));                    
                                dto.setDuration_ipvision_details("-");
                                dto.setDuration_carrier_details(Utils.getTimeHHMMSS((int) resultSet.getLong("duration")).toString());

                                disputeListFinal.add(dto);
                            }

                        }                        
                    }
                    else{
                        dto_temp.setDuration_ipvision_details("-");
                        dto_temp.setDuration_carrier_details(Utils.getTimeHHMMSS((int) resultSet.getLong("duration")).toString());
                        disputeListFinal.add(dto_temp);
                    }

                                 
                }
                
                for(int i=0;i<disputeListsemiFinal.size();i++){
                    if(disputeListsemiFinal.get(i).getDuration_carrier_details() == null){
                        disputeListsemiFinal.get(i).setDuration_carrier_details("-");
                    }                    
                }                  
            
                for(int i=0;i<disputeListFinal.size();i++){
                    disputeListsemiFinal.add(disputeListFinal.get(i));
                }                
            
                
            
            } catch (Exception ex) {
//                logger.debug("no filter : Exception in carrier data retrived-->"+ex);
            } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
        }
            
        } catch (Exception ex) {
        }
        
        //******************************try 002 (end)********************************        
    } 
    


    private Calendar convertToCalenderObject(DisputeDTO dto) {
                        String vv = dto.getCdr_date_details();
                        Integer v_year = Integer.parseInt(vv.substring(0, 4));
                        Integer v_month = Integer.parseInt(vv.substring(5, 7));
                        Integer v_dayOfMonth = Integer.parseInt(vv.substring(8, 10));
                        Integer v_hour = Integer.parseInt(vv.substring(11, 13));
                        Integer v_minute = Integer.parseInt(vv.substring(14,16));
                        Integer v_second = Integer.parseInt(vv.substring(17, 19));
                        Calendar calend_er = new GregorianCalendar(v_year, v_month, v_dayOfMonth, v_hour, v_minute, v_second);
                        return calend_er;
    }
    
    public synchronized ArrayList<DisputeDTO> getDisputeDTOCarrierList() {
        checkForReloadCarrierList();
        
        ArrayList<DisputeDTO> newList = new ArrayList<DisputeDTO>();
        for (DisputeDTO dto : disputeClientList) {
            newList.add(dto);
        }
        return newList;        
    }    
    
    private void checkForReloadCarrierList() {
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;      
        disputeClientList = new ArrayList<DisputeDTO>();
                
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            
            String sql = "select id,client_id from clients;";
//            logger.debug("group by solution"+sql);
            resultSet = statement.executeQuery(sql);
            DisputeDTO dto = new DisputeDTO();
            while (resultSet.next()) {
                dto = new DisputeDTO();
                dto.setID(String.valueOf(resultSet.getInt("id")));
                dto.setClientID(resultSet.getString("client_id"));
                disputeClientList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
//            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }             
        }         
    }    
    
    public synchronized ArrayList<DisputeDTO> getDisputeDTOListHourlySummaryOfparticularDates(int carrier_id,String[] dateLists,LoginDTO login_dto) {
        reloadDisputeHourWiseSummary(carrier_id,dateLists);
        ArrayList<DisputeDTO> newList = new ArrayList<DisputeDTO>();
        for (DisputeDTO dto : disputeSummaryList) {
            newList.add(dto);
        }
        return newList;

    }     

    public void reloadDisputeHourWiseSummary(int carrier_id,String[] dateLists) {
        
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet resultSet = null;        
        disputeSummaryList = new ArrayList<DisputeDTO>();
        disputeSummaryHashMap = new HashMap<String, DisputeDTO>();   
        
//*****************************************************************************

//        String carrier_condition = "";
        carrierID = carrier_id;
//        if(carrierID>0){
//            carrier_condition = " term_client_id = " + carrierID + " and ";
//        }        
        String select_sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern,substring(connection_time,12,2) hour,substring(connection_time,1,13) hashMapKey,count(1) total_cdr_carrier,sum(duration) call_duration_carrier from carrier_cdr where duration>0 and DATE_FORMAT(connection_time,'%Y-%m-%d') =";
        String sub_sql = "";
        String selectedIdsString = Utils.implodeArrayName(dateLists, ",");
        String[] arr_str = null;
        arr_str = selectedIdsString.split(",");
        
        for(int i=0;i<arr_str.length;i++){
            
            sub_sql = sub_sql + select_sql +"'"+ arr_str[i]+"'" + " group by date_pattern,substring(connection_time,1,13)";
            
            if((i+1)!=arr_str.length){
                sub_sql = sub_sql + " union ";                
            }
            else
                sub_sql = sub_sql + ";";
        }
        
        System.out.println("selected dates :: \n"+sub_sql);        
                
//*****************************************************************************        
        
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            
            String sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern,substring(connection_time,12,2) hour,substring(connection_time,1,13) hashMapKey,count(1) total_cdr_carrier,sum(duration) call_duration_carrier from carrier_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "' group by date_pattern,substring(connection_time,1,13);";
//            resultSet = statement.executeQuery(sql);
            resultSet = statement.executeQuery(sub_sql);            
            DisputeDTO dto = new DisputeDTO();
            while (resultSet.next()) {
                dto = new DisputeDTO();
                dto.setDate_datewise_summary(resultSet.getDate("date_pattern").toString() + " "+resultSet.getString("hour")+":00:00 - " + resultSet.getString("hour")+":59:59");
//                dto.setHour_datewise_summary(resultSet.getString("hour"));
                dto.setTotal_calls_ipvision_datewise_summary("-");
                dto.setTotal_duration_ipvision_datewise_summary("-");
                
                dto.setDate_datewise_summary_carrier(resultSet.getDate("date_pattern").toString() + " "+resultSet.getString("hour")+":00:00 - " + resultSet.getString("hour")+":59:59");
                dto.setTotal_calls_carrier_datewise_summary(resultSet.getString("total_cdr_carrier"));
                dto.setTotal_duration_carrier_datewise_summary(Utils.getTimeHHMMSS(resultSet.getInt("call_duration_carrier")));
                
                disputeSummaryHashMap.put(resultSet.getString("hashMapKey"), dto);
            }
            resultSet.close();
        } catch (Exception e) {
//            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
             
        }              
        try {
            String condition_sql="";
            if(carrierID>0){
                condition_sql = "and origin_client_id = " + carrierID;
            }
            
//*******************************************************************************
        
        select_sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern_flamma,substring(connection_time,12,2) hour,count(1) as number_of_cdr,substring(connection_time,1,13) hashMapKey,sum(duration) as total_duration from flamma_cdr where duration>0 and DATE_FORMAT(connection_time,'%Y-%m-%d') =";
        sub_sql = "";
        
        for(int i=0;i<arr_str.length;i++){
            
            sub_sql = sub_sql + select_sql +"'"+ arr_str[i]+"'" + " group by date_pattern_flamma,substring(connection_time,1,13)";
            
            if((i+1)!=arr_str.length){
                sub_sql = sub_sql + " union ";                
            }
            else
                sub_sql = sub_sql + ";";
        }
        
        System.out.println("selected dates 2 :: \n"+sub_sql);              
//*******************************************************************************            
            
            String sql = "select DATE_FORMAT(connection_time,'%Y-%m-%d') as date_pattern_flamma,substring(connection_time,12,2) hour,count(1) as number_of_cdr,substring(connection_time,1,13) hashMapKey,sum(duration) as total_duration from flamma_cdr where duration>0 and connection_time >='" + fromDate + "' and connection_time<='" + toDate + "'" + condition_sql + "  group by date_pattern_flamma,substring(connection_time,1,13);";
//            resultSet = statement.executeQuery(sql);
            resultSet = statement.executeQuery(sub_sql);
            DisputeDTO dto = new DisputeDTO();
            System.out.println("before removal :: "+disputeSummaryHashMap.size());
            String date_temp = null;
            while (resultSet.next()) {
                date_temp = resultSet.getString("hashMapKey");
                if(disputeSummaryHashMap.containsKey(date_temp)){
                    dto = disputeSummaryHashMap.get(date_temp);

                    disputeSummaryHashMap.remove(date_temp);
                    dto.setDate_datewise_summary(resultSet.getDate("date_pattern_flamma").toString() + " "+resultSet.getString("hour")+":00:00 - " + resultSet.getString("hour")+":59:59");
                    dto.setTotal_calls_ipvision_datewise_summary(resultSet.getBigDecimal("number_of_cdr").toString());
                    dto.setTotal_duration_ipvision_datewise_summary(Utils.getTimeHHMMSS((int) resultSet.getLong("total_duration")));                    
                                          
                    temp =  Utils.getTimeLong(dto.getTotal_duration_carrier_datewise_summary());
                    v_difference = temp - (double) resultSet.getLong("total_duration");
                    dto.setDeviation_datewise_summary(String.valueOf(v_difference*100/temp));
                    disputeSummaryList.add(dto);                    
                }
                else{
                dto = new DisputeDTO();
                dto.setDate_datewise_summary(resultSet.getDate("date_pattern_flamma").toString() + " "+resultSet.getString("hour")+":00:00 - " + resultSet.getString("hour")+":59:59");
//                dto.setHour_datewise_summary(resultSet.getString("hour"));  
                dto.setTotal_calls_ipvision_datewise_summary(resultSet.getBigDecimal("number_of_cdr").toString());
                dto.setTotal_duration_ipvision_datewise_summary(Utils.getTimeHHMMSS((int) resultSet.getLong("total_duration")));
                dto.setDate_datewise_summary_carrier(resultSet.getDate("date_pattern_flamma").toString());                
                dto.setTotal_calls_carrier_datewise_summary("-");                
                dto.setTotal_duration_carrier_datewise_summary("-");
                
                disputeSummaryList.add(dto);
                }
//                int map_size = disputeSummaryHashMap.size();
                

            }

            Iterator iterator = disputeSummaryHashMap.keySet().iterator();
//                key_list = disputeSummaryHashMap.keySet();
            while(iterator.hasNext()){
                dto = disputeSummaryHashMap.get(iterator.next().toString());
                disputeSummaryList.add(dto);                     
            }            
            resultSet.close();
        } catch (Exception e) {
//            logger.fatal("Exception in ClientLoader:" + e);
        } finally {
                try {
                    if (statement != null) {
                        statement.close();
                    }
                } catch (Exception e) {
                }
                try {
                    if (dbConnection.connection != null) {
                        databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                    }
                } catch (Exception e) {
                }
        }        
    }    

    
}