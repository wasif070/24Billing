/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.invoice;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import java.io.FileInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class DownloadinvoiceAction extends Action {

    static Logger logger = Logger.getLogger(DownloadinvoiceAction.class.getName());

    public ActionForward execute(
            ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        logger.debug("DownloadinvoiceAction started");
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        if (login_dto != null) {

            InvoiceDto dto = new InvoiceDto();
            InvoiceTaskSchedular schedular = new InvoiceTaskSchedular();
            ServletContext context = request.getSession(true).getServletContext();
            String realContextPath = context.getRealPath("");
            File realContextPathFile = new File(realContextPath);
            
            
            String filename = schedular.GenerateInvoiceXLS(id, realContextPathFile);
            String fileDisplayName = filename.substring(filename.lastIndexOf(System.getProperty("file.separator")) + 1);          
            
            File f = new File(filename);
            response.setContentType("application/vnd.ms-excel");
            response.setHeader("Content-Disposition", "inline; filename=" + fileDisplayName);
            response.setHeader("Pragma", "public");
            response.setHeader("Cache-Control", "no-store");
            response.addHeader("Cache-Control", "max-age=0");
            FileInputStream fin = null;
            try {
                fin = new FileInputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            int size = 1024;
            try {
                response.setContentLength(fin.available());
                byte[] buffer = new byte[size];
                ServletOutputStream os = null;

                os = response.getOutputStream();
                int length = 0;
                while ((length = fin.read(buffer)) != -1) {
                    os.write(buffer, 0, length);

                }
                fin.close();
                os.flush();
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.debug("filename" + filename);
        }
        return null;
    }
}