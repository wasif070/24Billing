/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.log4j.Logger;
import databaseconnector.DBConnection;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author reefat
 */
public class CapacityGroupDAO {
    
    static Logger logger = Logger.getLogger(CapacityGroupDAO.class.getName());
    private static ArrayList<Integer> deletedCapacityGroupList = null;
    private static ArrayList<CapacityGroupDTO> deletedCapacityGroupDTO = null;
    
    public MyAppError addCapacityGroup(CapacityGroupDTO p_dto) {
        MyAppError error = new MyAppError();
        String optional_cond1,optional_cond2;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;
        //Statement statement = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql;
            
            if(p_dto.getParent_group_id()!=null){
                optional_cond1 = "parent_group_id,";
                optional_cond2 = "?,";
            } else{
                optional_cond1 = "";
                optional_cond2 = "";            
            }

            sql = "insert into mvts_capacity_group(group_nm,description,"+optional_cond1+"capacity) values(?,?,"+optional_cond2+"?)";
            ps = dbConn.connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, p_dto.getGroup_name());
            ps.setString(2, p_dto.getDescription());

            if(p_dto.getParent_group_id()!=null){
            ps.setInt(3, Integer.parseInt(p_dto.getParent_group_id()));
            ps.setInt(4, p_dto.getCapacity());
            } else{
            ps.setInt(3, p_dto.getCapacity());          
            }            
            //ps.execute
            ps.executeUpdate();
            ResultSet rs = ps.getGeneratedKeys();
            if (rs != null && rs.next()) {
                error.setErrorMessage(String.valueOf(rs.getLong(1)));
            }
            CapacityGroupLoader.getInstance().forceReload();            


            
        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while adding user: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
//            try {
//                if (statement != null) {
//                    statement.close();
//                }
//            } catch (Exception e) {
//            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    } 
    
    public MyAppError mapCapacityGroup(LoginDTO login_dto,CapacityGroupDTO p_dto) {
        MyAppError error = new MyAppError();
        String[] multipleNameStrings = new String[1000];
        //DBConnection databaseConnection = null;
        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbconn = null;
        PreparedStatement ps = null;
        String sql;
        ResultSet resultSet = null;
        //Statement statement = null;
        try{
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            int[] ids = p_dto.getMappedClientId();
            
            String output = "";

            if (ids.length > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(ids[0]);

                for (int i = 1; i < ids.length; i++) {
                    sb.append(",");
                    sb.append(ids[i]);
                }

                output = sb.toString();
            }   
            
            sql = "select gateway_ip from gateway where client_id in (" + output + ")";
            CapacityGroupDTO c_dto;
            
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            int i = 0;
            while(resultSet.next()){
                c_dto= new CapacityGroupDTO();
                c_dto.setGatewayIP(resultSet.getString("gateway_ip"));
                multipleNameStrings[i] = "'" + resultSet.getString("gateway_ip") + "'";
                i++;
            }
            String selectedNamesString = "";
            //= Utils.implodeArrayName(multipleNameStrings, ",");
            
            if (i > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append(multipleNameStrings[0]);

                for (int j = 1; j < i; j++) {
                    sb.append(",");
                    sb.append(multipleNameStrings[j]);
                }

                selectedNamesString = sb.toString();
            }            
            
            String optional_column1 = "";
            String optional_column2 = "";
            switch(p_dto.getGatewayType()){
                case 0:
                        optional_column1 = " src_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " src_address_list in(" + selectedNamesString + ")";
                        break;
                case 1:
                        optional_column1 = " dst_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " dst_address in(" + selectedNamesString + ")";
                        break;
                case 2:
                        optional_column1 = " src_capacity_group = " + p_dto.getGroup_id() + " , dst_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " src_address_list in(" + selectedNamesString + ") and dst_address in(" + selectedNamesString + ")";
                        //optional_column4 = "dst_address";
                        break;
            }
                
                    
            dbconn = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "update mvts_gateway set " + optional_column1 + "  where " + optional_column2;
            logger.debug("SQL :::::: select src_address_list,dst_address,src_capacity_group,dst_capacity_group from mvts_gateway where  " + " src_address_list in(" + selectedNamesString + ") or dst_address in(" + selectedNamesString + ")");
            ps = dbconn.connection.prepareStatement(sql);
            
       //     System.out.println("Update SQL :: - >" + sql);
            
            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway Capacity_Group update failed.");
            }else{
                CapacityGroupLoader.getInstance().ForceClientReload(login_dto);
            }            
            
        }
        catch(Exception e){
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error while assigning Capacity Group.");
            logger.fatal("Error while assigning Capacity Group: ", e);        
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }

            try {
                if (dbconn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbconn);
                }
                if(dbConnection.connection != null){
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError editCapacityGroup(CapacityGroupDTO p_dto) {
        MyAppError error = new MyAppError();

        //DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        PreparedStatement ps = null;

        try {
            dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
            String sql;
            String optional_cond;
                optional_cond = "parent_group_id=?,";
          
            sql = "update mvts_capacity_group set group_nm=?,description=?," + optional_cond + "capacity=? where group_id=" + p_dto.getGroup_id();
            ps = dbConn.connection.prepareStatement(sql);

            ps.setString(1, p_dto.getGroup_name());
            ps.setString(2, p_dto.getDescription());
            
            if(Integer.parseInt(p_dto.getParent_group_id())!=0){
                ps.setInt(3, Integer.parseInt(p_dto.getParent_group_id()));
                ps.setInt(4, p_dto.getCapacity());
            } else{
//                ps.setInt(3, null);
                ps.setNull(3, java.sql.Types.INTEGER);
                ps.setInt(4, p_dto.getCapacity());          
            }            
            

            
            if (ps.executeUpdate() > 0) {
                CapacityGroupLoader.getInstance().forceReload();
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Database Error.");
            logger.fatal("Error while editing Rate Plan: ", ex);
        } finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError delCapacityGroup(int cid) {
        String sql = "";
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
            dbConnection = remotedbconnector.DBConnector.getInstance().makeConnection();
            sql = "select count(1) as count from mvts_capacity_group,mvts_gateway where parent_group_id =" + cid + " or src_capacity_group = " + cid + " or dst_capacity_group="+cid;
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            int v_count=0;
            
            if(resultSet.next())
                v_count = resultSet.getInt("count");
            
            if(v_count==0){
                sql = "delete from mvts_capacity_group where group_id =" + cid;
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    CapacityGroupLoader.getInstance().forceReload();
                    error.setErrorMessage("Successfully deleted : CapacityGroup :: ");
                }            
            }
            else{
                error.setErrorType(MyAppError.OtherError);
                error.setErrorMessage("Child found; Parent can't be deleted");
            }

        } catch (Exception ex) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error while deleting capacity group : " + ex.getMessage());
            logger.fatal("Error while deleting capacity group: ", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public MyAppError multipleCapacityGroupDelete(int destcodeIds[]) {
        String sql = "";
        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        String validIds = "-1";
        String invalidNames = "";
        String validNames = "";
        String msg = "";
        ArrayList<Integer> allDestCodes = new ArrayList<Integer>();

        try {
            dbConnection = remotedbconnector.DBConnector.getInstance().makeConnection();
            String str_destCodeIds = "-1";
            for (int destCode : destcodeIds) {
                str_destCodeIds += "," + destCode;
                allDestCodes.add(destCode);
            }
            
            //sql = "select clients.rateplan_id from clients,mvts_rateplan where clients.rateplan_id=mvts_rateplan.rateplan_id and client_delete = 0 and clients.rateplan_id in(" + destCodeIds + ")";
            sql = "select DISTINCT(parent_group_id) parent_group_id from mvts_capacity_group where parent_group_id in(" + str_destCodeIds + ")";
            logger.debug("sql" + sql);
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                int capacity_group_id = resultSet.getInt("parent_group_id");
                invalidNames += CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(capacity_group_id).getGroup_name() + ",";
                Integer x = new Integer(capacity_group_id);
                //capacity_group_id;
                allDestCodes.remove(x);
            }
            
            
            str_destCodeIds = "-1";
            Iterator i = allDestCodes.iterator();
            while(i.hasNext()){
                str_destCodeIds += "," + i.next();
            }           
            
            sql = "select distinct(cg) from (select src_capacity_group cg from mvts_gateway union select dst_capacity_group cg from mvts_gateway)  x where cg is not null and cg in(" + str_destCodeIds + ")";
            logger.debug("sql" + sql);
            ps = dbConnection.connection.prepareStatement(sql);
            resultSet = ps.executeQuery(sql);
            while (resultSet.next()) {
                int capacity_group_id = resultSet.getInt("cg");
                invalidNames += CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(capacity_group_id).getGroup_name() + ",";
                Integer x = new Integer(capacity_group_id);
                //capacity_group_id;
                allDestCodes.remove(x);
            }            
            deletedCapacityGroupList = allDestCodes;
//            String str = "";
//            Iterator iter = deletedCapacityGroupList.iterator();
////            deletedCapacityGroupDTO.add(CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(Integer.parseInt(String.valueOf(iter.next()))));
//            while(iter.hasNext()){
//                str = ""+iter.next();
//                int f = Integer.parseInt(str);
//                deletedCapacityGroupDTO.add(CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(Integer.parseInt(String.valueOf(iter.next()))));
//            }
            deletedCapacityGroupDTO = new ArrayList<CapacityGroupDTO>();
            for (int index : deletedCapacityGroupList) {
                CapacityGroupDTO cDTO = new CapacityGroupDTO();
                cDTO = CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(index);
                deletedCapacityGroupDTO.add(cDTO);
            }
            
            for (int destCode : allDestCodes) {
                validIds += "," + destCode;
                validNames += CapacityGroupLoader.getInstance().getCapacityGroupDTOByID(destCode).getGroup_name() + ",";
            }
            resultSet.close();

            if (validNames.length() > 0) {
                sql = "delete from mvts_capacity_group where group_id in (" + validIds + ")";
                ps = dbConnection.connection.prepareStatement(sql);
                if (ps.executeUpdate() > 0) {
                    CapacityGroupLoader.getInstance().forceReload();
                }
                //sql = "update mvts_rates set rate_delete = 1,rate_delete_time=UNIX_TIMESTAMP() where rateplan_id in (" + validIds + ")";
                //ps = dbConnection.connection.prepareStatement(sql);
                //ps.executeUpdate();

                msg += "These Capacity Groups have been deleted successfully : " + validNames.substring(0, validNames.length() - 1) + "<BR>";
            }

            if (invalidNames.length() > 0) {
                error.setErrorType(MyAppError.OtherError);
                msg += "These Capacity Group, can not be deleted : " + invalidNames.substring(0, invalidNames.length() - 1);
            }
            logger.debug("Message-->" + msg);

            error.setErrorMessage(msg);
        } catch (Exception ex) {
            logger.fatal("Error while deleting the Capacity Group!", ex);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
    
    public static ArrayList<CapacityGroupDTO> getDeletedDTOss(){        
        return deletedCapacityGroupDTO;
    }
    
    public MyAppError editClientCapacityInformation(LoginDTO login_dto,CapacityGroupDTO p_dto) {

        MyAppError error = new MyAppError();
        remotedbconnector.DBConnection dbconn = null;
        PreparedStatement ps = null;
        String sql;
        try{                   
            String optional_column1 = "";
            String optional_column2 = "";
            switch(p_dto.getGatewayType()){
                case 0:
                        optional_column1 = " src_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " src_address_list = ?";
                        break;
                case 1:
                        optional_column1 = " dst_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " dst_address = ?";
                        break;
                case 2:
                        optional_column1 = " src_capacity_group = " + p_dto.getGroup_id() + " , dst_capacity_group = " + p_dto.getGroup_id();
                        optional_column2 = " src_address_list = ? and dst_address = ?";
                        //optional_column4 = "dst_address";
                        break;
            }
            
            dbconn = remotedbconnector.DBConnector.getInstance().makeConnection();           
            
            sql = "update mvts_gateway set " + optional_column1 + "  where " + optional_column2;
            logger.debug("SQL :::::: select src_address_list,dst_address,src_capacity_group,dst_capacity_group from mvts_gateway where  " + " src_address_list = or dst_address = ");
            ps = dbconn.connection.prepareStatement(sql);
            
            switch(p_dto.getGatewayType()){
                case 0:
                        ps.setString(1, p_dto.getGatewayIP());
                        break;
                case 1:
                        ps.setString(1, p_dto.getGatewayIP());
                        break;
                case 2:
                        ps.setString(1, p_dto.getGatewayIP());
                        ps.setString(2, p_dto.getGatewayIP());
                        break;
            }     
            if (ps.executeUpdate() < 1) {
                error.setErrorMessage("MVTSPRO Gateway Client_Capacity_Group update failed.");
            }else{
                CapacityGroupLoader.getInstance().ForceClientReload(login_dto);
            }            
            
        }
        catch(Exception e){
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error while updating Capacity Group Information.");
            logger.fatal("Error while updating Capacity Group Info: ", e);        
        }finally {
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }

            try {
                if (dbconn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbconn);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }    
    
}
