/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.operators;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author reefat
 */
public class DeleteOperatorsAction extends Action {

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO); 
        MyAppError error = new MyAppError();
                
        if (login_dto != null && login_dto.getSuperUser()) {    
        int oper_id = Integer.parseInt(request.getParameter("id"));
        OperatorTaskScheduler scheduler = new OperatorTaskScheduler();
        OperatorDTO opDTO = scheduler.getOperatorDTO(oper_id);
        

        error = scheduler.deleteOperator(opDTO);
        
        OperatorsForm formBean = new OperatorsForm();

        //return mapping.findForward("success");
        if(error.getErrorType()>0){
            target = "failure";
            request.getSession(true).setAttribute(Constants.MESSAGE, "Deleted UnSuccessful");
        }
        else{
            formBean.setMessage(error.getErrorMessage());
            request.getSession(true).setAttribute(Constants.MESSAGE, "Deleted Successfully");
            
                try{
                        Gson json = new GsonBuilder().serializeNulls().create();
                        ActivityDTO ac_dto = new ActivityDTO();
                        ac_dto.setUserId(login_dto.getClientId());
                        ac_dto.setChangedValue(json.toJson(scheduler.getOperatorDTO((int)(long)opDTO.getOp_id())));
                        ac_dto.setActionName(Constants.DELETE_ACTION);
                        ac_dto.setTableName("operators");
                        ac_dto.setPrimaryKey(opDTO.getOp_prefix());
                        ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                        activityTaskScheduler.addActivityDTO(ac_dto);    
                }
                catch(Exception e){

                }            
            }
                      
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }        
        
        return mapping.findForward(target);
    }
    
}
