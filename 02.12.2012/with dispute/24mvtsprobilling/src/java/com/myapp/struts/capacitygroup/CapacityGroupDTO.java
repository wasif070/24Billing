/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.capacitygroup;

/**
 *
 * @author reefat
 */
public class CapacityGroupDTO {

    private int group_id;
    private String group_name;
    private String description;
    private String parent_group_id;
    private String parent_group_name;
    private int capacity;
    public boolean searchWithUserID = false;
    private int[] mappedClientId;
    private String gatewayIP;
    private int gatewayType;
    
    private String cg_client_id;    
    private String cg_client_type;    
    private String cg_org_gateway_ip;
    private String cg_term_gateway_ip;
    private String cg_org_capacity_group;
    private String cg_term_capacity_group;
    private String cg_gateway_type;

    public String getParent_group_name() {
        return parent_group_name;
    }

    public void setParent_group_name(String parent_group_name) {
        this.parent_group_name = parent_group_name;
    }

    
    public String getCg_client_id() {
        if(cg_client_id==null)
            cg_client_id="";
        return cg_client_id;
    }

    public void setCg_client_id(String cg_client_id) {
        this.cg_client_id = cg_client_id;
    }

    public String getCg_client_type() {
        return cg_client_type;
    }

    public void setCg_client_type(String cg_client_type) {
        this.cg_client_type = cg_client_type;
    }

    public String getCg_org_capacity_group() {
        if(cg_org_capacity_group==null)
            cg_org_capacity_group = "";         
        return cg_org_capacity_group;
    }

    public void setCg_org_capacity_group(String cg_org_capacity_group) {
        this.cg_org_capacity_group = cg_org_capacity_group;
    }

    public String getCg_org_gateway_ip() {
        if(cg_org_gateway_ip == null)
            cg_org_gateway_ip = "";
        return cg_org_gateway_ip;
    }

    public void setCg_org_gateway_ip(String cg_org_gateway_ip) {
        this.cg_org_gateway_ip = cg_org_gateway_ip;
    }

    public String getCg_term_capacity_group() {
        if(cg_term_capacity_group==null)
            cg_term_capacity_group = "";
        return cg_term_capacity_group;
    }

    public void setCg_term_capacity_group(String cg_term_capacity_group) {
        this.cg_term_capacity_group = cg_term_capacity_group;
    }

    public String getCg_term_gateway_ip() {
        if(cg_term_gateway_ip==null)
            cg_term_gateway_ip = "";        
        return cg_term_gateway_ip;
    }

    public void setCg_term_gateway_ip(String cg_term_gateway_ip) {
        this.cg_term_gateway_ip = cg_term_gateway_ip;
    }

    public String getCg_gateway_type() {
        return cg_gateway_type;
    }

    public void setCg_gateway_type(String cg_gateway_type) {
        this.cg_gateway_type = cg_gateway_type;
    }
    
    public int getGatewayType() {
        return gatewayType;
    }

    public void setGatewayType(int gatewayType) {
        this.gatewayType = gatewayType;
    }

    public String getGatewayIP() {
        return gatewayIP;
    }

    public void setGatewayIP(String gatewayIP) {
        this.gatewayIP = gatewayIP;
    }

    public int[] getMappedClientId() {
        return mappedClientId;
    }

    public void setMappedClientId(int[] mappedClientId) {
        this.mappedClientId = mappedClientId;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getGroup_id() {
        return group_id;
    }

    public void setGroup_id(int group_id) {
        this.group_id = group_id;
    }

    public String getGroup_name() {
        return group_name;
    }

    public void setGroup_name(String group_name) {
        this.group_name = group_name;
    }

    public String getParent_group_id() {
        return parent_group_id;
    }

    public void setParent_group_id(String parent_group_id) {
        this.parent_group_id = parent_group_id;
    }
}
