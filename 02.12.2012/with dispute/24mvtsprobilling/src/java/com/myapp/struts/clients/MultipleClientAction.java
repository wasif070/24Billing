/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.clients;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import com.myapp.struts.util.Utils;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class MultipleClientAction extends Action {

    static Logger logger = Logger.getLogger(AddClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {

            ClientForm formBean = (ClientForm) form;
            ClientDTO dto = new ClientDTO();
            ClientTaskSchedular scheduler = new ClientTaskSchedular();

            boolean actionSuccessful = false;
            String actionName = Constants.EDIT_ACTION;
            ArrayList<ClientDTO> clientList = new ArrayList<ClientDTO>();


            MyAppError error = new MyAppError();

            if (formBean.getActivateBtn() != null && formBean.getActivateBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.activateMultiple(formBean.getSelectedIDs());
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    } else {
                        actionSuccessful = true;
                        formBean.setMessage(false, "Client(s) is/are activated successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }

                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (formBean.getInactiveBtn() != null && formBean.getInactiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.inactivateMultiple(formBean.getSelectedIDs());
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    } else {
                        actionSuccessful = true;
                        formBean.setMessage(false, "Client(s) is/are Inactivated successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                    formBean.setMessage(true, error.getErrorMessage());
                }
            }

            if (formBean.getBlockBtn() != null && formBean.getBlockBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.blockMultiple(formBean.getSelectedIDs());
                    if (error.getErrorType() > 0) {
                        target = "failure";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    } else {
                        actionSuccessful = true;
                        formBean.setMessage(false, "Client(s) is/are blocked successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getDeleteBtn() != null && formBean.getDeleteBtn().length() > 0) {
                boolean children_found = false;
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    for (long clientId : formBean.getSelectedIDs()) {
                        ArrayList<Long> children = ClientLoader.getInstance().getChildren(clientId);
                        if (children.isEmpty()) {
                            ClientDTO clDTO = scheduler.getClientDTO(clientId);
                            clDTO.setIs_deleted(1);
                            clientList.add(clDTO);
                        } else {
                            children_found = true;
                        }
                    }

                    long[] filteredClientIds = new long[clientList.size()];
                    for (int i = 0; i < clientList.size(); i++) {
                        filteredClientIds[i] = clientList.get(i).getId();
                    }

                    error = scheduler.deleteMultiple(filteredClientIds);
                    actionSuccessful = true;
                    actionName = Constants.DELETE_ACTION;
                    String error_message = "";
                    if (children_found) {
                        error_message = "Some clients has not been deleted for having children.<BR>";
                    }
                    if (error.getErrorType() == MyAppError.PartialUpdated) {
                        error_message += error.getErrorMessage();
                        formBean.setMessage(false, error_message);
                    } else if (error.getErrorType() == MyAppError.Updated) {
                        formBean.setMessage(false, "Client(s) is/are deleted successfully.");
                    } else {
                        error_message += error.getErrorMessage();
                        formBean.setMessage(true, error_message);
                    }
                    error.setErrorType(MyAppError.NoError);

                    request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getRechargeBtn() != null && formBean.getRechargeBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];

                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (Utils.isDouble(temp)) {
                                if (Double.parseDouble(temp) > 0) {
                                    valid_clients[i] = clients[i];
                                    amount[i] = Double.parseDouble(temp);
                                    description[i] = des;
                                } else {
                                    target = "failure_recharge";
                                    formBean.setMessage(true, error.getErrorMessage());
                                    ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                                    return changedActionForward;
                                }

                            } else {
                                target = "failure_recharge";
                                formBean.setMessage(true, error.getErrorMessage());
                                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                                return changedActionForward;
                            }
                        }
                    }

                    boolean checkBalance = ClientLoader.getInstance().checkParentBalance(amount, login_dto);
                    if (checkBalance) {
                        error = scheduler.rechargeMultiple(login_dto.getId(), valid_clients, amount, description, login_dto);
                    } else {
                        error.setErrorType(MyAppError.OtherError);
                        error.setErrorMessage("Recharge action failed. Parent has not enought balance.");
                    }

                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), false);
                        return changedActionForward;
                    } else {
                        actionSuccessful = true;
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance recharged successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    logger.debug("Client ids length <0");
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getReturnBtn() != null && formBean.getReturnBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];
                    int inc = 0;
                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (temp != null) {
                                valid_clients[inc] = clients[i];
                                amount[inc] = Double.parseDouble(temp);
                                description[inc] = des;
                                inc++;
                            }
                        }
                    }
                    error = scheduler.returnMultiple(login_dto.getId(), valid_clients, amount, description, login_dto);
                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                        ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                        return changedActionForward;
                    } else {
                        actionSuccessful = true;
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance Returned successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (formBean.getReceiveBtn() != null && formBean.getReceiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    long[] clients = formBean.getSelectedIDs();
                    long[] valid_clients = new long[clients.length];
                    double[] amount = new double[clients.length];
                    String[] description = new String[clients.length];
                    if (clients.length > 0) {
                        for (int i = 0; i < clients.length; i++) {
                            String temp = (String) request.getParameter("amount_" + clients[i]);
                            String des = (String) request.getParameter("des_" + clients[i]);
                            if (temp != null) {
                                valid_clients[i] = clients[i];
                                amount[i] = Double.parseDouble(temp);
                                description[i] = des;
                            }
                        }
                    }
                    error = scheduler.receiveMultiple(login_dto.getId(), valid_clients, amount, description, login_dto);
                    if (error.getErrorType() > 0) {
                        target = "failure_recharge";
                        formBean.setMessage(true, error.getErrorMessage());
                    } else {
                        actionSuccessful = true;
                        target = "success_recharge";
                        formBean.setMessage(false, "Client's Balance recieved successfully.");
                        request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                    }
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least a client!");
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                actionSuccessful = true;
                if (actionSuccessful) {
                    if (actionName.equals(Constants.EDIT_ACTION)) {
                        clientList = new ArrayList<ClientDTO>();
                        for (long cl_id : formBean.getSelectedIDs()) {
                            clientList.add(scheduler.getClientDTO(cl_id));
                        }
                    }
                    for (ClientDTO clientDTO : clientList) {
                        try {
                            Gson json = new GsonBuilder().serializeNulls().create();
                            ActivityDTO ac_dto = new ActivityDTO();
                            ac_dto.setUserId(login_dto.getClientId());
                            ac_dto.setChangedValue(json.toJson(clientDTO));
                            ac_dto.setActionName(actionName);
                            ac_dto.setTableName("clients");
                            ac_dto.setPrimaryKey(clientDTO.getClient_id());
                            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                            activityTaskScheduler.addActivityDTO(ac_dto);
                        } catch (Exception e) {
                            logger.debug("Exception during activity log-->" + e);
                        }
                    }
                }
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
