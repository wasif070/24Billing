/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import java.util.ArrayList;
import com.myapp.struts.login.LoginDTO;

/**
 *
 * @author reefat
 */
public class DisputeTaskScheduler {

    public DisputeTaskScheduler() {
    }    
    
    public ArrayList<DisputeDTO> getDisputeDTOs(LoginDTO l_dto) {
        return DisputeLoader.getInstance().getDisputeDTOList(l_dto);
    }    
    
    public ArrayList<DisputeDTO> getDisputeDTOsWithSearchParameterDate(String type,int carrier_id,String startTDate, String endDate) {
        return DisputeLoader.getInstance().getDisputeDTOListWithSearchParameterDate(type,carrier_id,startTDate,endDate);
    }    
    
    public ArrayList<DisputeDTO> getDisputeDTOsofSelectedDates(int carrier_id,String[] selected_date_list,LoginDTO l_dto) {
        return DisputeLoader.getInstance().getDisputeDTOListOfparticularDates(carrier_id,selected_date_list,l_dto);
    }     
    
    public ArrayList<DisputeDTO> getDisputeDTOsHourlySummaryofSelectedDates(int carrier_id,String[] selected_date_list,LoginDTO l_dto) {
        return DisputeLoader.getInstance().getDisputeDTOListHourlySummaryOfparticularDates(carrier_id,selected_date_list,l_dto);
    }     
    
    public String insertCarrierDataForDispute(String[] mappedList) throws Exception{
        DisputeDAO dispute_Dao = new DisputeDAO();
        return dispute_Dao.disputedCarrierDataInsertion(mappedList);
    }    
}