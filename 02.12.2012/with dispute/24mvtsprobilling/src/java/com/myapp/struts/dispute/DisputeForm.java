/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.dispute;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author reefat
 */
public class DisputeForm extends org.apache.struts.action.ActionForm {
    
    private String name;
    private int number;
    
//********************(dispute summary)**********************************
    
    private int carrier_id_summary;
    private int fromYear_summary;
    private int fromMonth_summary;
    private int fromDay_summary;
    private int toYear_summary;
    private int toMonth_summary;
    private int toDay_summary;    
    private String HourlyBtn;
    private String DetailsBtn;
//*****************(dispute date wise summary)***************************

    private int carrier_id_datewise_summary;    
    private int recordPerPage;    
    private int fromYear_dateWise;
    private int fromMonth_dateWise;
    private int fromDay_dateWise;
    private int toYear_dateWise;
    private int toMonth_dateWise;
    private int toDay_dateWise;    
    private ArrayList<DisputeDTO> disputeDTOs_datewise_summary;
    private String[] selectedIDs;       
//***********************************************************************    
    
    private String n_o_c;
    private String[] our_db_columns;
//    private String hour_datewise_summary;
    private ArrayList<DisputeDTO> disputeDTOs_hourwise_summary;
    
    
    public DisputeForm(){
        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        fromDay_dateWise = day;
        fromMonth_dateWise = month;
        fromYear_dateWise = year;
        toDay_dateWise = day;
        toMonth_dateWise = month;
        toYear_dateWise = year;
        
        fromDay_summary = day;
        fromMonth_summary = month;
        fromYear_summary = year;
        toDay_summary = day;
        toMonth_summary = month;
        toYear_summary = year;        
    }

    public ArrayList<DisputeDTO> getDisputeDTOs_hourwise_summary() {
        return disputeDTOs_hourwise_summary;
    }

    public void setDisputeDTOs_hourwise_summary(ArrayList<DisputeDTO> disputeDTOs_hourwise_summary) {
        this.disputeDTOs_hourwise_summary = disputeDTOs_hourwise_summary;
    }

//    public String getHour_datewise_summary() {
//        return hour_datewise_summary;
//    }
//
//    public void setHour_datewise_summary(String hour_datewise_summary) {
//        this.hour_datewise_summary = hour_datewise_summary;
//    }

    public String getDetailsBtn() {
        return DetailsBtn;
    }

    public void setDetailsBtn(String DetailsBtn) {
        this.DetailsBtn = DetailsBtn;
    }

    public String getHourlyBtn() {
        return HourlyBtn;
    }

    public void setHourlyBtn(String HourlyBtn) {
        this.HourlyBtn = HourlyBtn;
    }

    public int getCarrier_id_datewise_summary() {
        return carrier_id_datewise_summary;
    }

    public void setCarrier_id_datewise_summary(int carrier_id_datewise_summary) {
        this.carrier_id_datewise_summary = carrier_id_datewise_summary;
    }

    public int getCarrier_id_summary() {
        return carrier_id_summary;
    }

    public void setCarrier_id_summary(int carrier_id_summary) {
        this.carrier_id_summary = carrier_id_summary;
    }
    
    public String[] getOur_db_columns() {
        return our_db_columns;
    }

    public void setOur_db_columns(String[] our_db_columns) {
        this.our_db_columns = our_db_columns;
    }    

    public String getN_o_c() {
        return n_o_c;
    }

    public void setN_o_c(String n_o_c) {
        this.n_o_c = n_o_c;
    }
    
    private ArrayList<DisputeDTO> disputeDTOs_datewise_details;

    public ArrayList<DisputeDTO> getDisputeDTOs_datewise_details() {
        return disputeDTOs_datewise_details;
    }

    public void setDisputeDTOs_datewise_details(ArrayList<DisputeDTO> disputeDTOs_datewise_details) {
        this.disputeDTOs_datewise_details = disputeDTOs_datewise_details;
    }
    
    
    
    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public String[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(String[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public ArrayList<DisputeDTO> getDisputeDTOs_datewise_summary() {
        return disputeDTOs_datewise_summary;
    }

    public void setDisputeDTOs_datewise_summary(ArrayList<DisputeDTO> disputeDTOs_datewise_summary) {
        this.disputeDTOs_datewise_summary = disputeDTOs_datewise_summary;
    }

    public int getFromDay_dateWise() {
        return fromDay_dateWise;
    }

    public void setFromDay_dateWise(int fromDay_dateWise) {
        this.fromDay_dateWise = fromDay_dateWise;
    }

    public int getFromMonth_dateWise() {
        return fromMonth_dateWise;
    }

    public void setFromMonth_dateWise(int fromMonth_dateWise) {
        this.fromMonth_dateWise = fromMonth_dateWise;
    }

    public int getFromYear_dateWise() {
        return fromYear_dateWise;
    }

    public void setFromYear_dateWise(int fromYear_dateWise) {
        this.fromYear_dateWise = fromYear_dateWise;
    }

    public int getToDay_dateWise() {
        return toDay_dateWise;
    }

    public void setToDay_dateWise(int toDay_dateWise) {
        this.toDay_dateWise = toDay_dateWise;
    }

    public int getToMonth_dateWise() {
        return toMonth_dateWise;
    }

    public void setToMonth_dateWise(int toMonth_dateWise) {
        this.toMonth_dateWise = toMonth_dateWise;
    }

    public int getToYear_dateWise() {
        return toYear_dateWise;
    }

    public void setToYear_dateWise(int toYear_dateWise) {
        this.toYear_dateWise = toYear_dateWise;
    }


    public int getFromDay_summary() {
        return fromDay_summary;
    }

    public void setFromDay_summary(int fromDay_summary) {
        this.fromDay_summary = fromDay_summary;
    }

    public int getFromMonth_summary() {
        return fromMonth_summary;
    }

    public void setFromMonth_summary(int fromMonth_summary) {
        this.fromMonth_summary = fromMonth_summary;
    }

    public int getFromYear_summary() {
        return fromYear_summary;
    }

    public void setFromYear_summary(int fromYear_summary) {
        this.fromYear_summary = fromYear_summary;
    }

    public int getToDay_summary() {
        return toDay_summary;
    }

    public void setToDay_summary(int toDay_summary) {
        this.toDay_summary = toDay_summary;
    }

    public int getToMonth_summary() {
        return toMonth_summary;
    }

    public void setToMonth_summary(int toMonth_summary) {
        this.toMonth_summary = toMonth_summary;
    }

    public int getToYear_summary() {
        return toYear_summary;
    }

    public void setToYear_summary(int toYear_summary) {
        this.toYear_summary = toYear_summary;
    }

    
    /**
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * @param string
     */
    public void setName(String string) {
        name = string;
    }

    /**
     * @return
     */
    public int getNumber() {
        return number;
    }

    /**
     * @param i
     */
    public void setNumber(int i) {
        number = i;
    }


 
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getName() == null || getName().length() < 1) {
            errors.add("name", new ActionMessage("error.name.required"));
            // TODO: add 'error.name.required' key to your resources
        }
        return errors;
    }
}