package com.myapp.struts.gateway;

import com.myapp.struts.session.Constants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class GatewayLoader {

    static Logger logger = Logger.getLogger(GatewayLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<GatewayDTO> gatewayList = null;
    private HashMap<String, GatewayDTO> switchGatewayMap = null;
    private HashMap<Long, GatewayDTO> gatewayDTOByID = null;
    private HashMap<String, String> gatewayDTOByIP = null;
    static GatewayLoader gatewayLoader = null;

    public GatewayLoader() {
        forceReload();
    }

    public static GatewayLoader getInstance() {
        if (gatewayLoader == null) {
            createGatewayLoader();
        }
        return gatewayLoader;
    }

    private synchronized static void createGatewayLoader() {
        if (gatewayLoader == null) {
            gatewayLoader = new GatewayLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        remotedbconnector.DBConnection dbConn = null;
        Statement statement = null;
        Statement remoteStatement = null;
        String sql = "";
        SettingsDTO settings = SettingsLoader.getInstance().getSettingsDTO("HARD_SWITCH");
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            gatewayList = new ArrayList<GatewayDTO>();
            gatewayDTOByID = new HashMap<Long, GatewayDTO>();
            switchGatewayMap = new HashMap<String, GatewayDTO>();
            gatewayDTOByIP = new HashMap<String, String>();

            if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("IS_SWITCH_IP_ONLINE").getSettingValue()) == AppConstants.YES) {
                sql = "select src_dnis_prefix_allow,gateway_id,gateway_name,dst_port_h323,dst_port_sip,protocol,radius_auth_call_enable from mvts_gateway";
                dbConn = remotedbconnector.DBConnector.getInstance().makeConnection();
                remoteStatement = dbConn.connection.createStatement();
                ResultSet rs = remoteStatement.executeQuery(sql);
                while (rs.next()) {
                    GatewayDTO sGatewatDTO = new GatewayDTO();
                    sGatewatDTO.setGateway_dst_port_h323(rs.getInt("dst_port_h323"));
                    sGatewatDTO.setGateway_dst_port_sip(rs.getInt("dst_port_sip"));
                    sGatewatDTO.setSwitchGatewayId(rs.getLong("gateway_id"));
                    sGatewatDTO.setProtocol_id(rs.getInt("protocol"));
                    sGatewatDTO.setEnable_radius(rs.getInt("radius_auth_call_enable"));
                    sGatewatDTO.setSrc_dnis_prefix_allow(rs.getString("src_dnis_prefix_allow"));
                    switchGatewayMap.put(rs.getString("gateway_name"), sGatewatDTO);
                }
            }

            sql = "select id,gateway_ip,gateway_name,gateway_type,gateway_status,client_id,owner_id from gateway where gateway_delete=0 order by id DESC";
            logger.debug("SQL-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                GatewayDTO dto = new GatewayDTO();
                dto.setId(resultSet.getLong("id"));
                dto.setGateway_ip(resultSet.getString("gateway_ip"));
                dto.setGateway_name(resultSet.getString("gateway_name"));
                dto.setOwner_id(resultSet.getInt("owner_id"));
                try {
                    if (Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("IS_SWITCH_IP_ONLINE").getSettingValue()) == AppConstants.YES) {
                        dto.setSwitchGatewayId(switchGatewayMap.get(dto.getGateway_name()).getSwitchGatewayId());
                    } else {
                        dto.setSwitchGatewayId(-1);
                    }
                } catch (Exception ex) {
                }
                dto.setGateway_type(resultSet.getInt("gateway_type"));
                try {
                    dto.setGateway_type_name(Constants.CLIENT_TYPE_NAME[dto.getGateway_type()]);
                } catch (Exception ex) {
                }
                dto.setClientId(resultSet.getInt("client_id"));
                if (dto.getClientId() > 0) {
                    ClientDTO cdto = ClientLoader.getInstance().getClientDTOByID(resultSet.getLong("client_id"));
                    if (cdto != null) {
                        if (cdto.getClient_id() != null && cdto.getClient_id().length() > 0) {
                            dto.setClient_name(cdto.getClient_id());
                            dto.setClient_balance(Double.parseDouble(cdto.getClient_balance()));

                        }
                    }

                }
                if (dto.getGateway_name().length() > 0 && Integer.parseInt(SettingsLoader.getInstance().getSettingsDTO("IS_SWITCH_IP_ONLINE").getSettingValue()) == AppConstants.YES) {
                    GatewayDTO gdto = switchGatewayMap.get(dto.getGateway_name());
                    if (gdto != null) {
                        dto.setGateway_dst_port_h323(gdto.getGateway_dst_port_h323());
                        dto.setGateway_dst_port_sip(gdto.getGateway_dst_port_sip());
                        dto.setProtocol_id(gdto.getProtocol_id());
                        dto.setEnable_radius(gdto.getEnable_radius());

                        String dnis_pattern = "";
                        try {
                            String temps[] = gdto.getSrc_dnis_prefix_allow().split(";");
                            for (String str : temps) {
                                int index = str.indexOf("[");
                                if (index > -1) {
                                    dnis_pattern += str.substring(0, index) + ";";
                                }
                            }
                            if (dnis_pattern.endsWith(";")) {
                                dnis_pattern = dnis_pattern.substring(0, dnis_pattern.length() - 1);
                            }
                        } catch (Exception ex) {
                        }

                        dto.setSrc_dnis_prefix_allow(dnis_pattern);
                    }
                } else {
                    dto.setGateway_dst_port_h323(0);
                    dto.setGateway_dst_port_sip(0);
                    dto.setProtocol_id(-1);
                    dto.setEnable_radius(0);
                    dto.setSrc_dnis_prefix_allow("");
                }

                dto.setGateway_status(resultSet.getInt("gateway_status"));
                dto.setGateway_status_name(Constants.GATEWAY_STATUS_STRING[dto.getGateway_status()]);
                dto.setSuperUser(false);
                gatewayDTOByID.put(dto.getId(), dto);
                gatewayList.add(dto);
                gatewayDTOByIP.put(dto.getGateway_ip(), dto.getGateway_name());

            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (remoteStatement != null) {
                    remoteStatement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
            try {
                if (dbConn.connection != null) {
                    remotedbconnector.DBConnector.getInstance().freeConnection(dbConn);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<GatewayDTO> newList = new ArrayList<GatewayDTO>();
        for (GatewayDTO dto : gatewayList) {
            if (login_dto.getClient_level() < 0) {
                newList.add(dto);
            } else {
                if (dto.getOwner_id() == login_dto.getOwn_id()) {
                    newList.add(dto);
                }
            }
        }
        return newList;
    }

    public synchronized GatewayDTO getGatewayDTOByID(long id) {
        checkForReload();
        return gatewayDTOByID.get(id);
    }

    public synchronized HashMap<String, String> getGatewayNames() {
        checkForReload();
        return gatewayDTOByIP;
    }

    public synchronized ArrayList<GatewayDTO> getGatewayDTOByType(int type) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByType = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getGateway_type() == type) {
                gatewayListByType.add(dto);
            }
        }
        return gatewayListByType;
    }

    public synchronized ArrayList<GatewayDTO> getClientsGatewayList(long client_id) {
        checkForReload();
        ArrayList<GatewayDTO> gatewayListByClient = new ArrayList<GatewayDTO>();
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (dto.getClientId() == client_id) {
                gatewayListByClient.add(dto);
            }
        }
        return gatewayListByClient;
    }

    public synchronized ClientDTO getClientDTOByIp(String ip_address) {
        checkForReload();
        ClientDTO clientDto = new ClientDTO();
        if (ip_address == null) {
            return null;
        }
        for (int inc = 0; inc < gatewayList.size(); inc++) {
            GatewayDTO dto = gatewayList.get(inc);
            if (ip_address.startsWith(dto.getGateway_ip())) {
                clientDto = ClientLoader.getInstance().getClientDTOByID(dto.getClientId());
            }
        }
        return clientDto;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsWithSearchParam(GatewayDTO udto, LoginDTO login_dto) {
        ArrayList<GatewayDTO> newList = new ArrayList<GatewayDTO>();
        checkForReload();
        ArrayList<GatewayDTO> list = gatewayList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                GatewayDTO dto = (GatewayDTO) i.next();
                if ((udto.searchWithClientID && !dto.getClient_name().toLowerCase().startsWith(udto.getClient_name()))
                        || (udto.searchWithGatewayIP && !dto.getGateway_ip().toLowerCase().startsWith(udto.getGateway_ip())
                        || (udto.searchWithStatus && dto.getGateway_status() != udto.getGateway_status())
                        || (udto.searchWithType && dto.getGateway_type() != udto.getGateway_type()))) {
                    continue;
                }
                newList.add(dto);
            }
        }

        ArrayList<GatewayDTO> finalList = new ArrayList<GatewayDTO>();
        for (GatewayDTO dto : newList) {
            if (login_dto.getClient_level() < 0) {
                finalList.add(dto);
            } else {
                if (dto.getOwner_id() == login_dto.getOwn_id()) {
                    finalList.add(dto);
                }
            }
        }
        return finalList;
    }

    public ArrayList<GatewayDTO> getGatewayDTOsSorted() {
        checkForReload();
        ArrayList<GatewayDTO> list = gatewayList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    GatewayDTO dto1 = (GatewayDTO) o1;
                    GatewayDTO dto2 = (GatewayDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }

    public ArrayList<GatewayDTO> getOperators() {
        DBConnection dbConnection = null;
        Statement statement = null;
        String sql = "";
        ArrayList<GatewayDTO> op_list = new ArrayList<GatewayDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "select id,op_prefix,op_name from operators";
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                GatewayDTO gate_dto = new GatewayDTO();
                gate_dto.setId(rs.getLong("id"));
                gate_dto.setPrefix(rs.getString("op_prefix"));
                gate_dto.setOp_name(rs.getString("op_name"));
                op_list.add(gate_dto);
            }

        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return op_list;
    }

    public GatewayDTO getOperator(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String sql = "";
        GatewayDTO gate_dto = new GatewayDTO();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "select id,op_prefix,op_name from operators where id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                gate_dto.setId(rs.getLong("id"));
                gate_dto.setPrefix(rs.getString("op_prefix"));
                gate_dto.setOp_name(rs.getString("op_name"));
            }

        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return gate_dto;
    }

    public ArrayList<Long> getOperatorsByGatewayID(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String sql = "";
        ArrayList<Long> op_list = new ArrayList<Long>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "select op_id from gateway_operators where gate_id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                op_list.add(rs.getLong("op_id"));
            }

        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return op_list;
    }

    public ArrayList<GatewayDTO> getOperators(long id) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String sql = "";
        ArrayList<GatewayDTO> op_list = new ArrayList<GatewayDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            sql = "select operators.id,op_prefix,op_name,gateway_name from gateway,gateway_operators,operators where gateway.id=gateway_operators.gate_id and operators.id=gateway_operators.op_id and (gateway_type=1 or gateway_type=2) and gateway_delete=0 and operators.id=" + id;
            ResultSet rs = statement.executeQuery(sql);
            while (rs.next()) {
                GatewayDTO gate_dto = new GatewayDTO();
                gate_dto.setId(rs.getLong("operators.id"));
                gate_dto.setPrefix(rs.getString("op_prefix"));
                gate_dto.setOp_name(rs.getString("op_name"));
                gate_dto.setGateway_name(rs.getString("gateway_name"));
                op_list.add(gate_dto);
            }

        } catch (Exception e) {
            logger.fatal("Exception in GatewayLoader:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return op_list;
    }
}
