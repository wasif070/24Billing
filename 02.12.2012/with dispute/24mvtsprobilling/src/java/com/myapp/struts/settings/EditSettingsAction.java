package com.myapp.struts.settings;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.logging.Logger;

public class EditSettingsAction extends Action {

    static Logger logger = Logger.getLogger(EditSettingsAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        SettingsForm formBean = (SettingsForm) form;
        if (login_dto != null && login_dto.getSuperUser()) {
            SettingsDTO dto = new SettingsDTO();
            SettingsTaskSchedular scheduler = new SettingsTaskSchedular();
            String sign = "";
            NumberFormat formatter = new DecimalFormat("00");
            dto.setId(formBean.getId());
            if (formBean.getId() == 1) {
                if (formBean.getSign() == 1) {
                    sign = "+";
                }
                if (formBean.getSign() == 2) {
                    sign = "-";
                }
                dto.setSettingValue(sign + String.valueOf(formatter.format(formBean.getHour())) + ":" + String.valueOf(formatter.format(formBean.getMin())) + ":" + String.valueOf(formatter.format(formBean.getSec())));
            } else {
                dto.setSettingValue(String.valueOf(formBean.getSettingValue()));
            }

            MyAppError error = scheduler.editSettingsInformation(dto);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {
                SettingsDTO settingsDTO=scheduler.getSettingsDTOById(formBean.getId());
                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(settingsDTO));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("settings");
                ac_dto.setPrimaryKey(settingsDTO.getSettingName());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "Settings is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }

        return (mapping.findForward(target));
    }
}
