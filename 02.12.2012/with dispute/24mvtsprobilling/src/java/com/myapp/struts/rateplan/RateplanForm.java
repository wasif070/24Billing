/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.rateplan;

import com.myapp.struts.session.Constants;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 *
 * @author Ashraful
 */
public class RateplanForm extends org.apache.struts.action.ActionForm {

    private long rateplan_id;
    private String rateplan_name;
    private String rateplan_des;
    private int rateplan_firstpulse;
    private int rateplan_pulse;
    private int rateplan_status;
    int action;
    private long[] selectedIDs;
    private String deleteBtn;
    private int pageNo;
    private int recordPerPage;
    private String message;
    private int doValidate;
    private ArrayList rateplanList;
    private int client_id;
    private int parent_id;

    public RateplanForm() {
        client_id = -1;
    }

    public String getRateplan_des() {
        return rateplan_des;
    }

    public void setRateplan_des(String rateplan_des) {
        this.rateplan_des = rateplan_des;
    }

    public long getRateplan_id() {
        return rateplan_id;
    }

    public void setRateplan_id(long rateplan_id) {
        this.rateplan_id = rateplan_id;
    }

    public String getRateplan_name() {
        return rateplan_name;
    }

    public void setRateplan_name(String rateplan_name) {
        this.rateplan_name = rateplan_name;
    }

    public int getRateplan_status() {
        return rateplan_status;
    }

    public void setRateplan_status(int rateplan_status) {
        this.rateplan_status = rateplan_status;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public String getDeleteBtn() {
        return deleteBtn;
    }

    public void setDeleteBtn(String deleteBtn) {
        this.deleteBtn = deleteBtn;
    }

    public long[] getSelectedIDs() {
        return selectedIDs;
    }

    public void setSelectedIDs(long[] selectedIDs) {
        this.selectedIDs = selectedIDs;
    }

    public int getDoValidate() {
        return doValidate;
    }

    public void setDoValidate(int doValidate) {
        this.doValidate = doValidate;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        this.message = message;
    }

    public ArrayList getRateplanList() {
        return rateplanList;
    }

    public void setRateplanList(ArrayList rateplanList) {
        this.rateplanList = rateplanList;
    }

    public int getRateplan_firstpulse() {
        return rateplan_firstpulse;
    }

    public void setRateplan_firstpulse(int rateplan_firstpulse) {
        this.rateplan_firstpulse = rateplan_firstpulse;
    }

    public int getRateplan_pulse() {
        return rateplan_pulse;
    }

    public void setRateplan_pulse(int rateplan_pulse) {
        this.rateplan_pulse = rateplan_pulse;
    }

    public int getClient_id() {
        return client_id;
    }

    public void setClient_id(int client_id) {
        this.client_id = client_id;
    }

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getAction() == Constants.ADD || getAction() == Constants.UPDATE) {

            if (getRateplan_name() != null && getRateplan_name().trim().length() < 1) {
                errors.add("rateplan_name", new ActionMessage("errors.rateplan_name.required"));
            }

        }


        return errors;
    }
}
