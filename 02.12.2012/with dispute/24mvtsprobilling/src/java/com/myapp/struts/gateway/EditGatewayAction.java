package com.myapp.struts.gateway;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.dialplan.DialplanDTO;
import com.myapp.struts.dialplan.DialplanLoader;
import com.myapp.struts.dialplan.DialplanTaskSchedular;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import org.apache.log4j.Logger;

public class EditGatewayAction extends Action {

    static Logger logger = Logger.getLogger(EditGatewayAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        GatewayForm formBean = (GatewayForm) form;

        if (login_dto != null && login_dto.getSuperUser()) {
            ArrayList<Long> prev_op_ids = GatewayLoader.getInstance().getOperatorsByGatewayID(formBean.getId());
            ArrayList<Integer> current_op_ids = new ArrayList<Integer>();
            GatewayDTO dto = new GatewayDTO();
            GatewayTaskSchedular scheduler = new GatewayTaskSchedular();
            dto.setId(formBean.getId());
            dto.setGateway_ip(formBean.getGateway_ip());
            dto.setGateway_name(formBean.getGateway_name());
            dto.setPrev_gateway_name(formBean.getPrev_gateway_name());
            dto.setGateway_type(formBean.getGateway_type());
            dto.setGateway_status(formBean.getGateway_status());
            dto.setClientId(formBean.getClientId());
            dto.setOwner_id((int) login_dto.getOwn_id());
            dto.setGateway_dst_port_h323(formBean.getGateway_dst_port_h323());
            dto.setGateway_dst_port_sip(formBean.getGateway_dst_port_sip());
            dto.setSwitchGatewayId(formBean.getSwitchGatewayId());
            dto.setProtocol_id(formBean.getProtocol_id());
            dto.setEnable_radius(formBean.getEnable_radius());
            dto.setPrev_gateway_type(formBean.getPrev_gateway_type());
            dto.setSrc_dnis_prefix_allow(formBean.getSrc_dnis_prefix_allow());

            if (request.getParameterValues("prefixes") != null && request.getParameterValues("prefixes").length > 0) {
                String parameters[] = request.getParameterValues("prefixes");
                int[] opids = new int[parameters.length];
                for (int i = 0; i < parameters.length; i++) {
                    opids[i] = Integer.parseInt(parameters[i]);
                    current_op_ids.add(opids[i]);
                }
                dto.setPrefixes(opids);
            }
            dto.setPrev_prefixes(prev_op_ids);


            MyAppError error = scheduler.editGatewayInformation(dto);


            ArrayList<DialplanDTO> d_list = DialplanLoader.getInstance().getDialplanDTOList(login_dto);

            logger.debug("op_ids size-->" + prev_op_ids.size());

            ArrayList<String> added_ops = new ArrayList<String>();
            for (long cur_op_id : current_op_ids) {
                if (!prev_op_ids.contains(cur_op_id)) {
                    logger.debug("added prefix-->" + GatewayLoader.getInstance().getOperator(cur_op_id).getPrefix());
                    added_ops.add(GatewayLoader.getInstance().getOperator(cur_op_id).getPrefix());
                }
            }

            ArrayList<String> added_dps = new ArrayList<String>();
            for (String prefix : added_ops) {
                for (DialplanDTO dial_dto : d_list) {
                    if (dial_dto.getDialplan_dnis_pattern_without_modify().contains(prefix + "[")) {
                        logger.debug("added dialplan-->" + dial_dto.getDialplan_name());
                        added_dps.add(dial_dto.getDialplan_name());
                    }
                }
            }


            ArrayList<String> removed_ops = new ArrayList<String>();
            for (long prev_op_id : prev_op_ids) {
                if (!current_op_ids.contains((int) prev_op_id)) {
                    logger.debug("removed prefix-->" + GatewayLoader.getInstance().getOperator(prev_op_id).getPrefix());
                    removed_ops.add(GatewayLoader.getInstance().getOperator(prev_op_id).getPrefix());
                }
            }

            ArrayList<String> removed_dps = new ArrayList<String>();
            for (String prefix : removed_ops) {
                for (DialplanDTO dial_dto : d_list) {
                    boolean matched = false;
                    for (int g_id : dial_dto.getDialplan_gateway_list_array()) {
                        if (g_id == formBean.getSwitchGatewayId()) {
                            matched = true;
                        }
                    }
                    if (matched && dial_dto.getDialplan_dnis_pattern_without_modify().contains(prefix + "[")) {
                        logger.debug("removed dialplan-->" + dial_dto.getDialplan_name());
                        removed_dps.add(dial_dto.getDialplan_name());
                    }
                }
            }

            DialplanTaskSchedular d_task_scheduler = new DialplanTaskSchedular();
            d_task_scheduler.updateDialpeer(removed_dps, dto.getSwitchGatewayId(), true);
            d_task_scheduler.updateDialpeer(added_dps, dto.getSwitchGatewayId(), false);

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
            } else {

                Gson json = new GsonBuilder().serializeNulls().create();
                ActivityDTO ac_dto = new ActivityDTO();
                ac_dto.setUserId(login_dto.getClientId());
                ac_dto.setChangedValue(json.toJson(scheduler.getGatewayDTO(formBean.getId())));
                ac_dto.setActionName(Constants.EDIT_ACTION);
                ac_dto.setTableName("gateway");
                ac_dto.setPrimaryKey(dto.getGateway_name());
                ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                activityTaskScheduler.addActivityDTO(ac_dto);

                formBean.setMessage(false, "Gateway is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
