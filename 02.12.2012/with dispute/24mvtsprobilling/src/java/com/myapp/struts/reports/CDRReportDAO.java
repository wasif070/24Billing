/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myapp.struts.reports;

import com.myapp.struts.clients.ClientDTO;
import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.settings.SettingsDTO;
import com.myapp.struts.util.Utils;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.AppConstants;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;

/**
 *
 * @author Ashraful
 */
public class CDRReportDAO {

    static Logger logger = Logger.getLogger(CDRReportDAO.class.getName());
    SettingsDTO settingsDTO = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE");
    String timeZone = settingsDTO.getSettingValue();
    String curdate = Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis())) - Utils.getTimeLong(timeZone));
    String now = Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis() - Utils.getTimeLong(timeZone));

    public CDRReportDAO() {
    }

    public HashMap<String, Object> getDTOList(CDRReportDTO sdto, int start, int end, LoginDTO login_dto) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = " duration>0 ";

        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = " duration <=0";
            }

            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
            } else {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            }

            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and (origin_destination like '" + sdto.getOrigin_destination() + "%' or origin_prefix like '" + sdto.getOrigin_destination() + "%') ";
            }

            if (sdto.getOrigin_caller() != null && sdto.getOrigin_caller().length() > 0) {
                if (sdto.getOrigin_caller().equalsIgnoreCase("NULL")) {
                    condition += " and origin_caller is NULL ";
                } else {
                    condition += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                }
            }

            if (sdto.getTerminated_no() != null && sdto.getTerminated_no().length() > 0) {
                condition += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
            }

            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
            }

            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip = '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and (term_destination like '" + sdto.getTerm_destination() + "%' or term_prefix like '" + sdto.getTerm_destination() + "%') ";
            }

            if (sdto.getFromDate() != null) {
                condition += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time >= '" + curdate + "' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
            }
        } else {
            condition += " and disconnect_time >= '" + curdate + "' and disconnect_time <= '" + now + "' ";
            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
            } else {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            }
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String limit_sql = "limit " + start + "," + end;
            //String sql = "select * from flamma_cdr where" + condition + " order by cdr_id desc " + limit_sql;
            String sql = "select * from flamma_cdr ,auth_error where flamma_cdr.conf_id=auth_error.conf_id and " + condition + " order by cdr_id desc " + limit_sql;
            logger.debug("Details sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                dto.setAuth_error_cause(resultSet.getString("error_cause"));
                //dto.setAuth_error_cause("");
                mvtsCdrList.add(dto);
            }

            resultSet = statement.executeQuery(sql.replace(limit_sql, "").replace("*", "count(cdr_id) as total_cdrs"));
            map.put("CDR_COUNT", 0);
            while (resultSet.next()) {
                map.put("CDR_COUNT", resultSet.getLong("total_cdrs"));
            }
            logger.debug("map.get(CDR_COUNT)-->" + map.get("CDR_COUNT"));
            sql = "select SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount) as org_bill,SUM(term_bill_amount) as term_bill from flamma_cdr where " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("sum_duration"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("org_bill"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("term_bill"));
                mvtsCdrList.add(dto);
            }
            map.put("CDR_LIST", mvtsCdrList);
            logger.debug("mvtsCdrList details-->" + mvtsCdrList.size());

            try {
                resultSet.close();
            } catch (SQLException sqlEx) {
            }
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOListWithSearch:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }

        return map;
    }

    public HashMap<String, Object> getSummary(CDRReportDTO sdto, int start, int end, LoginDTO login_dto) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        String conditionForTotalCall = "";
        String conditionForParentCost = "";
        condition = " duration >0 ";
        conditionForParentCost = " duration >0 ";
        HashMap<String, Long> hash_key = new HashMap<String, Long>();
        HashMap<String, Double> hash_key_parent = new HashMap<String, Double>();
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = "duration <= 0";
                conditionForTotalCall = "duration > 0";
            }

            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
                conditionForTotalCall += " and origin_client_id=" + sdto.getOrigin_client_id();
                conditionForParentCost += " and origin_client_id=" + sdto.getOrigin_client_id();
            }

            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
                conditionForTotalCall += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
                conditionForParentCost += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
                conditionForTotalCall += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
                conditionForParentCost += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }

            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
                conditionForTotalCall += " and term_client_id=" + sdto.getTerm_client_id();
                conditionForParentCost += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip = '" + sdto.getTerm_ip() + "'";
                conditionForTotalCall += " and term_ip = '" + sdto.getTerm_ip() + "'";
                conditionForParentCost += " and term_ip = '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
                conditionForTotalCall += " and term_destination like '" + sdto.getTerm_destination() + "%'";
                conditionForParentCost += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }

            if (sdto.getOrigin_caller() != null && sdto.getOrigin_caller().length() > 0) {
                if (sdto.getOrigin_caller().equalsIgnoreCase("NULL")) {
                    condition += " and origin_caller is NULL ";
                    conditionForTotalCall += " and origin_caller is NULL ";
                    conditionForParentCost += " and origin_caller is NULL ";
                } else {
                    condition += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                    conditionForTotalCall += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                    conditionForParentCost += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                }
            }

            if (sdto.getTerminated_no() != null && sdto.getTerminated_no().length() > 0) {
                condition += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
                conditionForTotalCall += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
                conditionForParentCost += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
            }

            if (sdto.getFromDate() != null) {
                condition += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForTotalCall += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForParentCost += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time >= '" + curdate + "' ";
                conditionForTotalCall += " and disconnect_time >= '" + curdate + "' ";
                conditionForParentCost += " and disconnect_time >= '" + curdate + "' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForTotalCall += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForParentCost += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
                conditionForTotalCall += " and disconnect_time <= '" + now + "' ";
                conditionForParentCost += " and disconnect_time <= '" + now + "' ";
            }
            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForTotalCall += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            } else {
                if (sdto.getOrigin_client_id() <= 0) {
                    condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                    conditionForTotalCall += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                    conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                } else {
                    condition += " and origin_client_id in(" + sdto.getOrigin_client_id() + ") ";
                    conditionForTotalCall += " and origin_client_id in(" + sdto.getOrigin_client_id() + ") ";
                    conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                }
            }
        } else {
            condition = "duration >0 and disconnect_time >= '" + curdate + "' and disconnect_time <= '" + now + "' ";
            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            } else {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            }
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (sdto.getCallType() == 2) {
                sql = "select count(cdr_id) as no_of_call,concat(origin_client_id,term_client_id,origin_ip,term_ip) as hash_key from flamma_cdr where " + conditionForTotalCall + " group by origin_client_id,term_client_id,origin_ip,term_ip order by cdr_id desc limit " + start + "," + end;
                logger.debug("To find out total call sql-->" + sql);
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    hash_key.put(resultSet.getString("hash_key"), resultSet.getLong("no_of_call"));
                }
            }

            if (sdto.getCallType() != 2 && login_dto.getOwn_id() > 0) {
                sql = "select concat(child_id,term_client_id,origin_ip,term_ip) as hash_key_parent, SUM(origin_bill_amount) as origin_bill from flamma_cdr where " + conditionForParentCost + " group by child_id,term_client_id,origin_ip,term_ip order by cdr_id desc limit " + start + "," + end;
                logger.debug("To find out parent bill sql-->" + sql);
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    hash_key_parent.put(resultSet.getString("hash_key_parent"), resultSet.getDouble("origin_bill"));
                }
            }

            sql = "select count(cdr_id) as no_of_call,origin_client_id,origin_client_name,term_client_name,origin_ip,term_ip, SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount) as origin_bill,SUM(term_bill_amount) as term_bill,concat(origin_client_id,term_client_id,origin_ip,term_ip) as hash_key from flamma_cdr where " + condition + " group by origin_client_id,term_client_id,origin_ip,term_ip order by cdr_id desc limit " + start + "," + end;
            logger.debug("Summary sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                if (sdto.getCallType() == 2) {
                    dto.setTotalFailure(resultSet.getLong("no_of_call"));
                    try {
                        dto.setTotalCall(((long) hash_key.get(resultSet.getString("hash_key"))) + dto.getTotalFailure());
                    } catch (Exception e) {
                        dto.setTotalCall(dto.getTotalFailure());
                    }
                }
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill"));
                if (sdto.getCallType() != 2 && login_dto.getOwn_id() > 0) {
                    dto.setParentBillAmount(hash_key_parent.get(resultSet.getString("hash_key")));
                    dto.setProfit(dto.getOrigin_bill_amount() - dto.getParentBillAmount());
                }
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill"));
                dto.setDuration(resultSet.getInt("sum_duration"));
                mvtsCdrList.add(dto);
            }


            String limit_sql = "limit " + start + "," + end;
            resultSet = statement.executeQuery(sql.replace(limit_sql, "").replace("count(cdr_id) as no_of_call,origin_client_name,term_client_name,origin_ip,term_ip, SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount) as origin_bill,SUM(term_bill_amount) as term_bill,concat(origin_client_id,term_client_id,origin_ip,term_ip) as hash_key", "count(cdr_id) as total_cdrs"));
            map.put("CDR_COUNT", 0);
            while (resultSet.next()) {
                map.put("CDR_COUNT", (Integer.parseInt(map.get("CDR_COUNT").toString()) + 1));
            }


            sql = "select SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount),SUM(term_bill_amount) from flamma_cdr where " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("sum_duration"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("SUM(origin_bill_amount)"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("SUM(term_bill_amount)"));
                mvtsCdrList.add(dto);
            }

            map.put("CDR_LIST", mvtsCdrList);

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getSummaryMvtsCdrDTOListWithSearch:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    public HashMap<String, Object> getSummaryByMotherClient(CDRReportDTO sdto, int start, int end, LoginDTO login_dto) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        String conditionForTotalCall = "";
        String conditionForParentCost = "";
        condition = " duration >0 ";
        conditionForParentCost = " duration >0 ";
        HashMap<String, Long> hash_key = new HashMap<String, Long>();
        HashMap<String, Double> hash_key_parent = new HashMap<String, Double>();
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition = "duration <= 0";
                conditionForTotalCall = "duration > 0";
            }

            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
                conditionForTotalCall += " and origin_client_id=" + sdto.getOrigin_client_id();
                conditionForParentCost += " and origin_client_id=" + sdto.getOrigin_client_id();
            }

            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
                conditionForTotalCall += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
                conditionForParentCost += " and origin_ip = '" + sdto.getOrigin_ip() + "'";
            }

            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
                conditionForTotalCall += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
                conditionForParentCost += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }

            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
                conditionForTotalCall += " and term_client_id=" + sdto.getTerm_client_id();
                conditionForParentCost += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip = '" + sdto.getTerm_ip() + "'";
                conditionForTotalCall += " and term_ip = '" + sdto.getTerm_ip() + "'";
                conditionForParentCost += " and term_ip = '" + sdto.getTerm_ip() + "'";
            }

            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
                conditionForTotalCall += " and term_destination like '" + sdto.getTerm_destination() + "%'";
                conditionForParentCost += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }

            if (sdto.getOrigin_caller() != null && sdto.getOrigin_caller().length() > 0) {
                if (sdto.getOrigin_caller().equalsIgnoreCase("NULL")) {
                    condition += " and origin_caller is NULL ";
                    conditionForTotalCall += " and origin_caller is NULL ";
                    conditionForParentCost += " and origin_caller is NULL ";
                } else {
                    condition += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                    conditionForTotalCall += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                    conditionForParentCost += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                }
            }

            if (sdto.getTerminated_no() != null && sdto.getTerminated_no().length() > 0) {
                condition += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
                conditionForTotalCall += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
                conditionForParentCost += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
            }

            if (sdto.getFromDate() != null) {
                condition += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForTotalCall += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForParentCost += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time >= '" + curdate + "' ";
                conditionForTotalCall += " and disconnect_time >= '" + curdate + "' ";
                conditionForParentCost += " and disconnect_time >= '" + curdate + "' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForTotalCall += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
                conditionForParentCost += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
                conditionForTotalCall += " and disconnect_time <= '" + now + "' ";
                conditionForParentCost += " and disconnect_time <= '" + now + "' ";
            }
            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForTotalCall += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            } else {
                if (sdto.getOrigin_client_id() <= 0) {
                    condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                    conditionForTotalCall += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                    conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                } else {
                    condition += " and origin_client_id in(" + sdto.getOrigin_client_id() + ") ";
                    conditionForTotalCall += " and origin_client_id in(" + sdto.getOrigin_client_id() + ") ";
                    conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                }
            }
        } else {
            condition = "duration >0 and disconnect_time >= '" + curdate + "' and disconnect_time <= '" + now + "' ";
            if (login_dto.getOwn_id() < 0) {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            } else {
                condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                conditionForParentCost += " and child_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
            }
        }
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            if (sdto.getCallType() == 2) {
                sql = "select count(cdr_id) as no_of_call,origin_client_id as hash_key from flamma_cdr where " + conditionForTotalCall + " group by origin_client_id order by cdr_id desc ";
                logger.debug("To find out total call sql-->" + sql);
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    hash_key.put(resultSet.getString("hash_key"), resultSet.getLong("no_of_call"));
                }
            }

            if (sdto.getCallType() != 2 && login_dto.getOwn_id() > 0) {
                sql = "select child_id as hash_key_parent, SUM(origin_bill_amount) as origin_bill from flamma_cdr where " + conditionForParentCost + " group by child_id order by cdr_id desc ";
                logger.debug("To find out parent bill sql-->" + sql);
                ResultSet resultSet = statement.executeQuery(sql);
                while (resultSet.next()) {
                    hash_key_parent.put(resultSet.getString("hash_key_parent"), resultSet.getDouble("origin_bill"));
                }
            }

            sql = "select count(cdr_id) as no_of_call,origin_client_id,origin_client_name,term_client_name,origin_ip,term_ip, SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount) as origin_bill,SUM(term_bill_amount) as term_bill,origin_client_id as hash_key from flamma_cdr where " + condition + " group by origin_client_id order by cdr_id desc ";
            logger.debug("Summary sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            HashMap<Long, CDRReportDTO> groupByMap = new HashMap<Long, CDRReportDTO>();
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                if (sdto.getCallType() == 2) {
                    dto.setTotalFailure(resultSet.getLong("no_of_call"));
                    try {
                        dto.setTotalCall(((long) hash_key.get(resultSet.getString("hash_key"))) + dto.getTotalFailure());
                    } catch (Exception e) {
                        dto.setTotalCall(dto.getTotalFailure());
                    }
                }

                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill"));
                if (sdto.getCallType() != 2 && login_dto.getOwn_id() > 0) {
                    dto.setParentBillAmount(hash_key_parent.get(resultSet.getString("hash_key")));
                    dto.setProfit(dto.getOrigin_bill_amount() - dto.getParentBillAmount());
                }

                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill"));
                dto.setDuration(resultSet.getInt("sum_duration"));

                ClientDTO cl_dto = ClientLoader.getInstance().getClientDTOByID(dto.getOrigin_client_id());
                if (cl_dto != null && cl_dto.getMother_company_id() > 0) {
                    if (groupByMap.containsKey(cl_dto.getMother_company_id())) {
                        CDRReportDTO tempDTO = groupByMap.get(cl_dto.getMother_company_id());
                        tempDTO.setTotalFailure(tempDTO.getTotalFailure() + dto.getTotalFailure());
                        tempDTO.setTotalCall(tempDTO.getTotalCall() + dto.getTotalCall());
                        tempDTO.setDuration(tempDTO.getDuration() + dto.getDuration());
                        tempDTO.setOrigin_bill_amount(tempDTO.getOrigin_bill_amount() + dto.getOrigin_bill_amount());
                        tempDTO.setTerm_bill_amount(tempDTO.getTerm_bill_amount() + dto.getTerm_bill_amount());
                        tempDTO.setTotal_client(tempDTO.getTotal_client() + 1);
                        tempDTO.setOrigin_client_name(cl_dto.getMother_company_name());
                        dto.setOrigin_ip("N/A");
                        dto.setTerm_ip("N/A");
                        dto.setTerm_client_name("N/A");
                        groupByMap.put(cl_dto.getMother_company_id(), tempDTO);
                    } else {
                        dto.setOrigin_ip("N/A");
                        dto.setTerm_ip("N/A");
                        dto.setTerm_client_name("N/A");
                        dto.setOrigin_client_name(cl_dto.getMother_company_name());
                        groupByMap.put(cl_dto.getMother_company_id(), dto);
                    }
                } else {
                    mvtsCdrList.add(dto);
                }
            }

            for (long key : groupByMap.keySet()) {
                CDRReportDTO q_dto = groupByMap.get(key);
                mvtsCdrList.add(q_dto);
            }
            map.put("CDR_COUNT", mvtsCdrList.size());

            sql = "select SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount),SUM(term_bill_amount) from flamma_cdr where " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("sum_duration"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("SUM(origin_bill_amount)"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("SUM(term_bill_amount)"));
                mvtsCdrList.add(dto);
            }

            map.put("CDR_LIST", mvtsCdrList);

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getSummaryMvtsCdrDTOListWithSearch:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    public HashMap<String, Object> getDTOListByClient(CDRReportDTO sdto, long client_id, int start, int end) {
        HashMap<String, Object> map = new HashMap<String, Object>();
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "(origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        if (sdto != null) {
            if (sdto.getCallType() == 2) {
                condition += " and duration <= 0";
            } else {
                condition += " and duration > 0";
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }
            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }
            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }
            if (sdto.getFromDate() != null) {
                condition += " and disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time >= '" + curdate + "' ";
            }
            if (sdto.getToDate() != null) {
                condition += " and disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
            }
        } else {
            condition = " duration >0 and disconnect_time >= '" + curdate + "' and disconnect_time <= '" + now + "' ";
            condition += " and (origin_client_id=" + client_id + " or term_client_id=" + client_id + ")";
        }

        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String limit_sql = "limit " + start + "," + end;
            //String sql = "select * from flamma_cdr where client_level=1 and " + condition + " order by cdr_id desc " + limit_sql;
            String sql = "select * from flamma_cdr ,auth_error where flamma_cdr.conf_id=auth_error.conf_id and client_level=1 and " + condition + " order by cdr_id desc " + limit_sql;
            logger.debug("client cdrs sql-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setCdr_id(resultSet.getLong("cdr_id"));
                dto.setDialed_no(resultSet.getString("dialed_no"));
                dto.setOrigin_caller(resultSet.getString("origin_caller"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_rate_id(resultSet.getLong("origin_rate_id"));
                dto.setOrigin_rate_des(resultSet.getString("origin_rate_des"));
                dto.setOrigin_rate(resultSet.getDouble("origin_rate"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerminated_no(resultSet.getString("terminated_no"));
                dto.setTerm_caller(resultSet.getString("term_caller"));
                dto.setTerm_client_id(resultSet.getLong("term_client_id"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_rate_id(resultSet.getLong("term_rate_id"));
                dto.setTerm_rate_des(resultSet.getString("term_rate_des"));
                dto.setTerm_rate(resultSet.getDouble("term_rate"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setConnection_time(resultSet.getString("connection_time"));
                dto.setDuration(resultSet.getInt("duration"));
                dto.setPdd(resultSet.getDouble("pdd"));
                dto.setDisconnect_cause(resultSet.getString("disconnect_desc"));
                dto.setAuth_error_cause(resultSet.getString("error_cause"));
                //dto.setAuth_error_cause("");
                mvtsCdrList.add(dto);
            }

            map.put("CDR_COUNT", 0);
            resultSet = statement.executeQuery(sql.replace(limit_sql, "").replace("*", "count(cdr_id) as total_cdrs"));
            while (resultSet.next()) {
                map.put("CDR_COUNT", resultSet.getLong("total_cdrs"));
            }


            sql = "select SUM(FLOOR((duration+999)/1000)) as sum_duration,SUM(origin_bill_amount),SUM(term_bill_amount) from flamma_cdr where client_level = 1 and " + condition;
            logger.debug("sum sql" + sql);
            resultSet = statement.executeQuery(sql);
            if (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setTotal_duration(resultSet.getLong("sum_duration"));
                dto.setTotal_origin_bill_amount(resultSet.getDouble("SUM(origin_bill_amount)"));
                dto.setTotal_term_bill_amount(resultSet.getDouble("SUM(term_bill_amount)"));
                mvtsCdrList.add(dto);
            }

            map.put("CDR_LIST", mvtsCdrList);
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getMvtsCdrDTOByClient:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return map;
    }

    public ArrayList<CDRReportDTO> getMvtsCdrDTOList(CDRReportDTO sdto, LoginDTO login_dto, boolean is_client) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = "1";
        String datePattern = "";
        String groupBy = "";
        String hashKey = "";
        String time_adjustment = timeZone.contains("+") ? timeZone.substring(1) : timeZone;

        if (sdto != null) {
            if (sdto.getFromDate() != null) {
                condition = " disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition = " disconnect_time >= '" + curdate + "' ";
            }

            if (!is_client) {
                if (login_dto.getOwn_id() < 0) {
                    condition += " and origin_client_id in(" + ClientLoader.getInstance().getTopLevelClient() + ") ";
                } else {
                    condition += " and origin_client_id in(" + ClientLoader.getInstance().getChildrenStr(login_dto.getOwn_id()) + ") ";
                }
            } else {
                condition += " and (origin_client_id =" + login_dto.getId() + " or term_client_id=" + login_dto.getId() + ") ";
            }

            if (sdto.getToDate() != null) {
                condition += " and disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
            }
            if (sdto.getOrigin_client_id() > 0) {
                condition += " and origin_client_id=" + sdto.getOrigin_client_id();
            }
            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip() + "'";
            }
            if (sdto.getOrigin_destination() != null && sdto.getOrigin_destination().length() > 0) {
                condition += " and origin_destination like '" + sdto.getOrigin_destination() + "%'";
            }
            if (sdto.getTerm_client_id() > 0) {
                condition += " and term_client_id=" + sdto.getTerm_client_id();
            }
            if (sdto.getTerm_ip() != null && sdto.getTerm_ip().length() > 0) {
                condition += " and term_ip like '" + sdto.getTerm_ip() + "'";
            }
            if (sdto.getTerm_destination() != null && sdto.getTerm_destination().length() > 0) {
                condition += " and term_destination like '" + sdto.getTerm_destination() + "%'";
            }

            if (sdto.getOrigin_caller() != null && sdto.getOrigin_caller().length() > 0) {
                if (sdto.getOrigin_caller().equalsIgnoreCase("NULL")) {
                    condition += " and origin_caller is NULL ";
                } else {
                    condition += " and origin_caller like '" + sdto.getOrigin_caller() + "%' ";
                }
            }

            if (sdto.getTerminated_no() != null && sdto.getTerminated_no().length() > 0) {
                condition += " and terminated_no like '" + sdto.getTerminated_no() + "%' ";
            }

            if (sdto.getSummaryType() == 1) {
                datePattern = " date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m-%d %h %p') AS disconn_time ";
                hashKey += "date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m-%d %h %p')";
            } else if (sdto.getSummaryType() == 0 || sdto.getSummaryType() == 2) {
                datePattern = " date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m-%d') as disconn_time ";
                hashKey += "date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m-%d')";
            } else if (sdto.getSummaryType() == 3) {
                datePattern = " date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m') as disconn_time ";
                hashKey += "date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y-%m')";
            } else if (sdto.getSummaryType() == 4) {
                datePattern = " date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y') as disconn_time ";
                hashKey += "date_format(ADDTIME(disconnect_time,'" + time_adjustment + "'),'%Y')";
            }

            groupBy = " GROUP BY disconn_time ";
            hashKey += " as hash_key ";

            if (sdto.getSummaryBy() != null && sdto.getSummaryBy().length > 0) {
                hashKey = " concat(" + hashKey.replace("as hash_key", "");
                for (int j = 0; j < sdto.getSummaryBy().length; j++) {
                    switch (sdto.getSummaryBy()[j]) {
                        case 0:
                            groupBy += ",origin_ip";
                            hashKey += ",origin_ip";
                            break;
                        case 1:
                            groupBy += ",origin_destination";
                            hashKey += ",origin_destination";
                            break;
                        case 2:
                            groupBy += ",term_ip";
                            hashKey += ",term_ip";
                            break;
                        case 3:
                            groupBy += ",term_destination";
                            hashKey += ",term_destination";
                            break;
                    }
                }
                hashKey += ") as hash_key ";
            }
        }

        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            HashMap<String, Long> dropcallList = new HashMap<String, Long>();
            HashMap<String, Long> successCallList = new HashMap<String, Long>();
            HashMap<String, Double> pddList = new HashMap<String, Double>();
            String sql = "select count(distinct(conf_id)) as no_of_drop_call, " + hashKey + ", " + datePattern + "  from flamma_cdr where  duration<1 and " + condition + " " + groupBy;
            logger.debug("quality cdr list drop calls-->" + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                dropcallList.put(resultSet.getString("hash_key"), resultSet.getLong("no_of_drop_call"));
            }

            sql = "select count(distinct(conf_id)) as no_of_success_call, " + hashKey + ", " + datePattern + "  from flamma_cdr where  duration>0 and " + condition + " " + groupBy;
            logger.debug("quality cdr list success calls-->" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                successCallList.put(resultSet.getString("hash_key"), resultSet.getLong("no_of_success_call"));
            }

            sql = "select avg(pdd) as avg_pdd, " + hashKey + ", " + datePattern + "  from flamma_cdr where  pdd>0 and " + condition + " " + groupBy;
            logger.debug("quality cdr pdd-->" + sql);
            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                pddList.put(resultSet.getString("hash_key"), resultSet.getDouble("avg_pdd"));
            }

            sql = "select count(distinct(conf_id)) as no_of_call, " + hashKey + " ,origin_client_id,origin_client_name,origin_ip,origin_prefix,origin_destination,sum(origin_bill_amount) as origin_bill_amount,term_client_name,term_ip,term_prefix,term_destination,sum(term_bill_amount) as term_bill_amount,sum(FLOOR((duration+999)/1000)) as call_duration,sum(pdd) as avg_pdd,avg(duration) as avg_acd, " + datePattern + " from flamma_cdr where " + condition + " " + groupBy + " order by cdr_id desc";
            logger.debug("quality cdr list all calls-->" + sql);

            resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                String disconn_time = resultSet.getString("disconn_time");
                dto.setStartDate(disconn_time);
                dto.setHash_key(resultSet.getString("hash_key"));
                dto.setOrigin_client_id(resultSet.getLong("origin_client_id"));
                try {
                    dto.setTotalFailure((Long) dropcallList.get(dto.getHash_key()));
                } catch (Exception ex) {
                    dto.setTotalFailure(0);
                }

                try {
                    dto.setTotalSuccess((Long) successCallList.get(dto.getHash_key()));
                } catch (Exception ex) {
                    dto.setTotalSuccess(0);
                }

                try {
                    dto.setPdd(pddList.get(dto.getHash_key()));
                } catch (Exception ex) {
                    dto.setPdd(0);
                }

                dto.setOrigin_client_name(resultSet.getString("origin_client_name"));
                dto.setOrigin_ip(resultSet.getString("origin_ip"));
                dto.setOrigin_prefix(resultSet.getString("origin_prefix"));
                dto.setOrigin_destination(resultSet.getString("origin_destination"));
                dto.setOrigin_bill_amount(resultSet.getDouble("origin_bill_amount"));
                dto.setTerm_client_name(resultSet.getString("term_client_name"));
                dto.setTerm_ip(resultSet.getString("term_ip"));
                dto.setTerm_prefix(resultSet.getString("term_prefix"));
                dto.setTerm_destination(resultSet.getString("term_destination"));
                dto.setTerm_bill_amount(resultSet.getDouble("term_bill_amount"));
                dto.setDuration(resultSet.getLong("call_duration"));
                dto.setAcd(resultSet.getDouble("avg_acd"));
                dto.setHash_key(resultSet.getString("hash_key"));
                mvtsCdrList.add(dto);
            }
            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in quality report:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return mvtsCdrList;
    }

    public ArrayList<CDRQualityDTO> getCdrQualityDTOs(CDRReportDTO sdto, LoginDTO login_dto, boolean is_client) {
        ArrayList<CDRQualityDTO> qualityCdrList = new ArrayList<CDRQualityDTO>();
        ArrayList<CDRReportDTO> mvtsCdrList = new ArrayList<CDRReportDTO>();
        mvtsCdrList = getMvtsCdrDTOList(sdto, login_dto, is_client);

        if (mvtsCdrList != null && mvtsCdrList.size() > 0) {
            for (int inc = 0; inc < mvtsCdrList.size(); inc++) {
                CDRReportDTO mcdr = new CDRReportDTO();
                mcdr = mvtsCdrList.get(inc);
                CDRQualityDTO temp = new CDRQualityDTO();

                temp.setStart_time(mcdr.getStartDate());
                temp.setOrigin_client_name(mcdr.getOrigin_client_name());
                temp.setOrigin_ip(mcdr.getOrigin_ip());
                temp.setOrigin_destination(mcdr.getOrigin_destination());
                temp.setOrigin_bill_amount(mcdr.getOrigin_bill_amount());
                temp.setTerm_client_name(mcdr.getTerm_client_name());
                temp.setTerm_ip(mcdr.getTerm_ip());
                temp.setTerm_destination(mcdr.getTerm_destination());
                temp.setTerm_bill_amount(mcdr.getTerm_bill_amount());
                temp.setDuration(mcdr.getDuration());
                temp.setTotal_fail(0);
                temp.setTotal_success(0);
                double pdd = mcdr.getPdd();
                temp.setAvg_pdd(pdd);
                temp.setAcd(mcdr.getAcd());
                temp.setTotal_success((int) mcdr.getTotalSuccess());
                temp.setTotal_fail((int) mcdr.getTotalFailure());
                qualityCdrList.add(temp);
            }
        }

        return qualityCdrList;
    }

    public StringBuffer makeCdrQualityExportStrings(ArrayList<CDRQualityDTO> qualityCdrList, String summary_by, boolean is_client) {
        StringBuffer csvString = new StringBuffer();
        NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());
        NumberFormat pddformatter = new DecimalFormat("#00.00");
        CDRQualityDTO dto = new CDRQualityDTO();
         if (qualityCdrList.size() > 0) {
            for (int inc = 0; inc < qualityCdrList.size(); inc++) {
                dto = qualityCdrList.get(inc);
                csvString.append(dto.getStart_time()).append(",");

                if (summary_by.contains("0")) {
                    csvString.append(dto.getOrigin_client_name()).append(",");
                    csvString.append(dto.getOrigin_ip()).append(",");
                }
                if (summary_by.toString().contains("1")) {
                    csvString.append(dto.getOrigin_destination()).append(",");
                }
                csvString.append(amount_formatter.format(dto.getOrigin_bill_amount())).append(",");
                if (summary_by.contains("2")) {
                    csvString.append(dto.getTerm_client_name()).append(",");
                    csvString.append(dto.getTerm_ip()).append(",");
                }
                if (summary_by.contains("3")) {
                    csvString.append(dto.getTerm_destination()).append(",");
                }
                if (!is_client) {
                    csvString.append(amount_formatter.format(dto.getTerm_bill_amount())).append(",");
                }
                csvString.append(Utils.getMMSS(dto.getDuration())).append(",");
                csvString.append(dto.getTotal_success()).append("+").append(dto.getTotal_fail()).append("=").append(dto.getTotal_success()).append(dto.getTotal_fail()).append(",");
                csvString.append(pddformatter.format(((double) dto.getTotal_success() / (dto.getTotal_success() + dto.getTotal_fail())) * 100)).append(",");
                if (dto.getTotal_success() > 0) {
                    csvString.append(Utils.getMMSS((long) (dto.getDuration() / dto.getTotal_success()))).append(",");
                } else {
                    csvString.append("00:00" + ",");
                }
                if (dto.getTotal_success() > 0) {
                    csvString.append(pddformatter.format((double) (dto.getAvg_pdd()))).append('\n');
                } else {
                    csvString.append("00:00" + '\n');
                }
            }
         }
        return csvString;
    }

    public ArrayList<CDRReportDTO> getErrorReportDTOs(CDRReportDTO sdto, LoginDTO login_dto) {
        DBConnection dbConnection = null;
        Statement statement = null;
        String condition = "";
        condition = " duration <= 0";

        if (sdto != null) {
            if (sdto.getFromDate() != null) {
                condition += " and  disconnect_time >='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getFromDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time >= '" + curdate + "' ";
            }

            if (sdto.getToDate() != null) {
                condition += " and  disconnect_time <='" + Utils.ToDateDDMMYYYYhhmmss(Utils.getDateLong(sdto.getToDate()) - Utils.getTimeLong(timeZone)) + "'";
            } else {
                condition += " and disconnect_time <= '" + now + "' ";
            }

            if (sdto.getOrigin_ip() != null && sdto.getOrigin_ip().trim().length() > 0) {
                condition += " and origin_ip like '" + sdto.getOrigin_ip().trim() + "%' ";
            }
            if (sdto.getTerminated_no() != null && sdto.getTerminated_no().trim().length() > 0) {
                condition += " and terminated_no like '" + sdto.getTerminated_no().trim() + "%' ";
            }


        }
        ArrayList<CDRReportDTO> errorList = new ArrayList<CDRReportDTO>();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            String sql = "";
            String count_sql = "";
            if (sdto.getErrorType() == AppConstants.SWITCH_ERROR) {
                sql = "select count(disconnect_desc) as error_count,disconnect_desc as error from flamma_cdr,auth_error where auth_error.conf_id=flamma_cdr.conf_id and " + condition + " group by disconnect_desc";
                count_sql = "select count(cdr_id) as total_errors from flamma_cdr,auth_error where flamma_cdr.conf_id=auth_error.conf_id and " + condition;
            } else {
                sql = "select count(error_cause) as error_count,error_cause as error from auth_error,flamma_cdr where auth_error.conf_id=flamma_cdr.conf_id and error_cause!='' and " + condition + " group by error_cause";
                count_sql = "select count(cdr_id) as total_errors from flamma_cdr,auth_error where flamma_cdr.conf_id=auth_error.conf_id and error_cause!='' and " + condition;
            }
            logger.debug("errors sql = " + sql);
            ResultSet resultSet = statement.executeQuery(sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setErrorCount(resultSet.getLong("error_count"));
                dto.setErrorName(resultSet.getString("error"));
                errorList.add(dto);
            }


            logger.debug("total_errors sql " + count_sql);
            resultSet = statement.executeQuery(count_sql);
            while (resultSet.next()) {
                CDRReportDTO dto = new CDRReportDTO();
                dto.setErrorSum(resultSet.getLong("total_errors"));
                errorList.add(dto);
            }

            resultSet.close();
        } catch (Exception e) {
            logger.fatal("Exception in getErrorReportDTOs:", e);
        } finally {
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return errorList;
    }

    public StringBuffer makeCdrExportStrings(ArrayList<CDRReportDTO> cdrList, CDRReportDTO cdto, LoginDTO login_dto, boolean is_client) {

        StringBuffer csvString = new StringBuffer();
        NumberFormat amount_formatter = new DecimalFormat(Utils.getNumberFormat());
        CDRReportDTO dto = new CDRReportDTO();
        if (cdrList.size() > 0) {
            for (int inc = 0; inc < cdrList.size() - 1; inc++) {
                dto = cdrList.get(inc);
                if (!is_client) {
                    if (login_dto != null && login_dto.getClientType() != 0) {
                        csvString.append(dto.getDialed_no()).append(",");
                    } else if (login_dto == null) {
                        csvString.append(dto.getDialed_no()).append(",");
                    }
                    csvString.append(dto.getOrigin_caller()).append(",");
                } else {
                    csvString.append(dto.getOrigin_caller()).append(",");
                    csvString.append(dto.getDialed_no()).append(",");
                }
                if (!is_client) {
                    csvString.append(dto.getOrigin_client_name()).append(",");
                }
                csvString.append(dto.getOrigin_ip()).append(",");
                csvString.append(dto.getOrigin_prefix()).append(",");
                csvString.append(dto.getOrigin_destination()).append(",");
                csvString.append(dto.getOrigin_rate_des()).append(",");
                csvString.append(amount_formatter.format(dto.getOrigin_bill_amount())).append(",");
                if (!is_client) {
                    if (login_dto != null && login_dto.getClientType() != 0) {
                        csvString.append(dto.getTerminated_no()).append(",");
                    } else if (login_dto == null) {
                        csvString.append(dto.getTerminated_no()).append(",");
                    }
                    csvString.append(dto.getTerm_caller()).append(",");
                    csvString.append(dto.getTerm_client_name()).append(",");
                    if (login_dto != null && login_dto.getClient_level() != 2) {
                        csvString.append(dto.getTerm_ip()).append(",");
                    } else if (login_dto == null) {
                        csvString.append(dto.getTerm_ip()).append(",");
                    }
                    csvString.append(dto.getTerm_prefix()).append(",");
                    csvString.append(dto.getTerm_destination()).append(",");
                    csvString.append(dto.getTerm_rate_des()).append(",");
                    csvString.append(amount_formatter.format(dto.getTerm_bill_amount())).append(",");
                }
                csvString.append(Utils.getMMSS(dto.getDuration())).append(",");
                csvString.append(Utils.ToDateDDMMYYYYhhmmss(Utils.getTimeLong(timeZone) + Utils.getDateLong(dto.getConnection_time()))).append(",");
                csvString.append(dto.getPdd()).append(",");
                csvString.append(dto.getDisconnect_cause());
                if (cdto.getCallType() == 2) {
                    csvString.append(",").append(dto.getAuth_error_cause()).append('\n');
                } else {
                    csvString.append('\n');
                }
            }
        }

        return csvString;
    }
}
