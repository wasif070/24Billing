package ans;

import com.myapp.struts.clients.ClientLoader;
import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetAnsAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {

            AnsForm formBean = (AnsForm) form;
            AnsDTO dto = new AnsDTO();
            AnsTaskSchedular scheduler = new AnsTaskSchedular();
            dto = scheduler.getAnsDTO(id);

            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setName(dto.getName());
                formBean.setPrimary_prefix(dto.getPrimary_prefix());
                formBean.setOther_prefixes(dto.getOther_prefixes());
                formBean.setCompany_name(dto.getCompany_name());
                formBean.setStatus(dto.getStatus());
            } else {
                target = "failure";
            }
            request.getSession(true).setAttribute(mapping.getAttribute(), formBean);

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
