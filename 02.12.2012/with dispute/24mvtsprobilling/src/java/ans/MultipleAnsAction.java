/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ans;

import activitylog.ActivityDTO;
import activitylog.ActivityTaskScheduler;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myapp.struts.clients.AddClientAction;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author Administrator
 */
public class MultipleAnsAction extends Action {

    static Logger logger = Logger.getLogger(AddClientAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        login_dto.getClientStatus();
        if (login_dto != null && login_dto.getSuperUser()) {

            boolean actionSuccessful = false;
            String actionName = Constants.EDIT_ACTION;
            ArrayList<AnsDTO> list = new ArrayList<AnsDTO>();


            AnsForm formBean = (AnsForm) form;

            AnsTaskSchedular scheduler = new AnsTaskSchedular();

            MyAppError error = new MyAppError();

            if (formBean.getActivateBtn() != null && formBean.getActivateBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.activateMultiple(formBean.getSelectedIDs());
                    actionSuccessful = true;
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least an ans!");
                }
            }

            if (formBean.getInactiveBtn() != null && formBean.getInactiveBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    error = scheduler.inactivateMultiple(formBean.getSelectedIDs());
                    actionSuccessful = true;
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least an ans!");
                }
            }

            if (formBean.getDeleteBtn() != null && formBean.getDeleteBtn().length() > 0) {
                if (formBean.getSelectedIDs() != null && formBean.getSelectedIDs().length > 0) {
                    for (long id : formBean.getSelectedIDs()) {
                        AnsDTO DTO = scheduler.getAnsDTO(id);
                        DTO.setDeleted(1);
                        list.add(DTO);
                    }
                    error = scheduler.deleteMultiple(formBean.getSelectedIDs());
                    actionSuccessful = true;
                    actionName = Constants.DELETE_ACTION;
                } else {
                    error.setErrorType(1);
                    error.setErrorMessage("Please Select at least an ans!");
                }
            }

            if (error.getErrorType() > 0) {
                target = "failure";
                formBean.setMessage(true, error.getErrorMessage());
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
            } else {
                actionSuccessful = true;

                if (actionSuccessful) {
                    if (actionName.equals(Constants.EDIT_ACTION)) {
                        list = new ArrayList<AnsDTO>();
                        for (long id : formBean.getSelectedIDs()) {
                            list.add(scheduler.getAnsDTO(id));
                        }
                    }
                    for (AnsDTO DTO : list) {
                        try {
                            Gson json = new GsonBuilder().serializeNulls().create();
                            ActivityDTO ac_dto = new ActivityDTO();
                            ac_dto.setUserId(login_dto.getClientId());
                            ac_dto.setChangedValue(json.toJson(DTO));
                            ac_dto.setActionName(actionName);
                            ac_dto.setTableName("ans_clients");
                            ac_dto.setPrimaryKey(DTO.getPrimary_prefix());
                            ActivityTaskScheduler activityTaskScheduler = new ActivityTaskScheduler();
                            activityTaskScheduler.addActivityDTO(ac_dto);
                        } catch (Exception e) {
                            logger.debug("Exception during activity log-->" + e);
                        }
                    }
                }
                formBean.setMessage(false, "ANS List is updated successfully.");
                request.getSession(true).setAttribute(Constants.MESSAGE, formBean.getMessage());
                ActionForward changedActionForward = new ActionForward(mapping.findForward(target).getPath(), true);
                return changedActionForward;
            }
        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
