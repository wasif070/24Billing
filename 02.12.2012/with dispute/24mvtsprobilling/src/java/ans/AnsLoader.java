package ans;

import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import org.apache.log4j.Logger;
import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.AppConstants;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;

public class AnsLoader {

    static Logger logger = Logger.getLogger(AnsLoader.class.getName());
    private static long LOADING_INTERVAL = AppConstants.LOADING_INTERVAL;
    private long loadingTime = 0;
    private ArrayList<AnsDTO> ansList = null;
    private HashMap<Long, AnsDTO> ansDTOByID = null;
    private HashMap<String, String> ansByPrefix = null;
    static AnsLoader ansLoader = null;

    public AnsLoader() {
        forceReload();
    }

    public static AnsLoader getInstance() {
        if (ansLoader == null) {
            createAnsLoader();
        }
        return ansLoader;
    }

    private synchronized static void createAnsLoader() {
        if (ansLoader == null) {
            ansLoader = new AnsLoader();
        }
    }

    private void reload() {

        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            ansList = new ArrayList<AnsDTO>();
            ansDTOByID = new HashMap<Long, AnsDTO>();
            ansByPrefix = new HashMap<String, String>();

            String sql = "select * from ans_clients where deleted=0";
            logger.debug("ans loader-->" + sql);
            statement = dbConnection.connection.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                AnsDTO dto = new AnsDTO();
                dto.setId(rs.getLong("id"));
                dto.setName(rs.getString("name"));
                dto.setCompany_name(rs.getString("company_name"));
                dto.setPrimary_prefix(rs.getString("primary_prefix"));
                dto.setOther_prefixes(rs.getString("other_prefixes"));
                dto.setStatus(rs.getInt("status"));
                dto.setDeleted(rs.getInt("deleted"));
                ansList.add(dto);
                ansDTOByID.put(dto.getId(), dto);
                ansByPrefix.put(dto.getPrimary_prefix(), dto.getName());
            }

            logger.debug("ans loader list-->" + ansList.size());

        } catch (Exception e) {
            logger.fatal("Exception in AnsLoader:", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
    }

    private void checkForReload() {
        long currentTime = System.currentTimeMillis();
        if (currentTime - loadingTime > LOADING_INTERVAL) {
            loadingTime = currentTime;
            reload();
        }
    }

    public synchronized void forceReload() {
        loadingTime = System.currentTimeMillis();
        reload();
    }

    public synchronized ArrayList<AnsDTO> getAnsDTOList(LoginDTO login_dto) {
        checkForReload();
        ArrayList<AnsDTO> newList = new ArrayList<AnsDTO>();
        for (AnsDTO dto : ansList) {
            if (login_dto.getClient_level() < 0) {
                newList.add(dto);
            }
        }
        return newList;
    }

    public synchronized AnsDTO getAnsDTOByID(long id) {
        checkForReload();
        return ansDTOByID.get(id);
    }

    public synchronized String getAnsNameByPrefix(String prefix) {
        checkForReload();
        return ansByPrefix.get(prefix);
    }

    public ArrayList<AnsDTO> getAnsDTOsWithSearchParam(AnsDTO udto, LoginDTO login_dto) {
        ArrayList<AnsDTO> newList = new ArrayList<AnsDTO>();
        checkForReload();
        ArrayList<AnsDTO> list = ansList;
        if (list != null && list.size() > 0) {
            Iterator i = list.iterator();
            while (i.hasNext()) {
                AnsDTO dto = (AnsDTO) i.next();
                if ((udto.sw_company_name && !dto.getCompany_name().toLowerCase().startsWith(udto.getCompany_name()))
                        || (udto.sw_name && !dto.getName().toLowerCase().startsWith(udto.getName()))
                        || (udto.sw_primary_prefix && !dto.getPrimary_prefix().toLowerCase().startsWith(udto.getPrimary_prefix()))
                        || (udto.sw_other_prefix && !dto.getOther_prefixes().toLowerCase().startsWith(udto.getOther_prefixes()))) {
                    continue;
                }
                newList.add(dto);
            }
        }
        return newList;
    }

    public ArrayList<AnsDTO> getAnsDTOsSorted() {
        checkForReload();
        ArrayList<AnsDTO> list = ansList;
        if (list != null && list.size() > 0) {
            Collections.sort(list, new Comparator() {

                public int compare(Object o1, Object o2) {
                    int val = 0;
                    AnsDTO dto1 = (AnsDTO) o1;
                    AnsDTO dto2 = (AnsDTO) o2;
                    if (dto1.getId() < dto2.getId()) {
                        val = 1;
                    }
                    return val;
                }
            });
        }
        return list;
    }
}
