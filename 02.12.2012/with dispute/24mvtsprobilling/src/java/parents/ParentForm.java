package parents;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class ParentForm extends org.apache.struts.action.ActionForm {

    private int pageNo;
    private int recordPerPage;
    private long id;
    private String parent_name;
    private String parent_desc;
    private int status;
    private String statusStr;
    private int is_deleted;
    private ArrayList<ParentDTO> parentList;
    private String message;
    

    public ParentForm() {
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public ArrayList<ParentDTO> getParentList() {
        return parentList;
    }

    public void setParentList(ArrayList<ParentDTO> parentList) {
        this.parentList = parentList;
    }

    public String getParent_desc() {
        return parent_desc;
    }

    public void setParent_desc(String parent_desc) {
        this.parent_desc = parent_desc;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public int getRecordPerPage() {
        return recordPerPage;
    }

    public void setRecordPerPage(int recordPerPage) {
        this.recordPerPage = recordPerPage;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<span style='color:red; font-size: 12px;font-weight:bold'>" + message + "</span>";
        } else {
            this.message = "<span style='color:blue; font-size: 12px;font-weight:bold'>" + message + "</span>";
        }
    }

    /**
     * This is the action called from the Struts framework.
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    @Override
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if (getParent_name() == null || getParent_name().length() < 1) {
            errors.add("parent_name", new ActionMessage("errors.required", "Parent Name"));
        }
        return errors;
    }
}
