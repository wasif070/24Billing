package parents;

import java.util.ArrayList;

public class ParentDTO {

    private long id;
    private String parent_name;
    private String parent_desc;
    private int status;
    private String statusStr;
    private int is_deleted;
    private ArrayList<ParentDTO> parentList;
    public boolean sw_name;

    public ParentDTO() {
        status = 1;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIs_deleted() {
        return is_deleted;
    }

    public void setIs_deleted(int is_deleted) {
        this.is_deleted = is_deleted;
    }

    public ArrayList<ParentDTO> getParentList() {
        return parentList;
    }

    public void setParentList(ArrayList<ParentDTO> parentList) {
        this.parentList = parentList;
    }

    public String getParent_desc() {
        return parent_desc;
    }

    public void setParent_desc(String parent_desc) {
        this.parent_desc = parent_desc;
    }

    public String getParent_name() {
        return parent_name;
    }

    public void setParent_name(String parent_name) {
        this.parent_name = parent_name;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getStatusStr() {
        return statusStr;
    }

    public void setStatusStr(String statusStr) {
        this.statusStr = statusStr;
    }
}
