package parents;

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.util.MyAppError;
import java.util.ArrayList;

public class ParentTaskSchedular {

    public ParentTaskSchedular() {
    }

    public MyAppError addParent(ParentDTO p_dto) {
        ParentDAO userDAO = new ParentDAO();
        return userDAO.addParent(p_dto);
    }

    public MyAppError editParent(ParentDTO p_dto) {
        ParentDAO userDAO = new ParentDAO();
        return userDAO.editParent(p_dto);
    }

    public ParentDTO getParentDTO(long id) {
        return ParentLoader.getInstance().getParentDTOByID(id);
    }

    public ArrayList<ParentDTO> getParentDTOsSorted(LoginDTO l_dto) {
        return ParentLoader.getInstance().getParentDTOsSorted();
    }

    public ArrayList<ParentDTO> getParentDTOs(LoginDTO l_dto) {
        return ParentLoader.getInstance().getParentDTOList();
    }

    public ArrayList<ParentDTO> getParentDTOsWithSearchParam(ParentDTO udto, LoginDTO l_dto) {
        return ParentLoader.getInstance().getParentDTOsWithSearchParam(udto);
    }

    public MyAppError deleteParent(int cid) {
        ParentDAO dao = new ParentDAO();
        return dao.deleteParent(cid);
    }
}
