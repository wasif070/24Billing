package parents;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class GetAction extends Action {

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        long id = Long.parseLong(request.getParameter("id"));
        if (login_dto != null && login_dto.getSuperUser()) {
            ParentForm formBean = (ParentForm) form;
            ParentDTO dto = new ParentDTO();
            ParentTaskSchedular scheduler = new ParentTaskSchedular();
            dto = scheduler.getParentDTO(id);
            if (dto != null) {
                formBean.setId(dto.getId());
                formBean.setParent_name(dto.getParent_name());
                formBean.setParent_desc(dto.getParent_desc());
                formBean.setStatus(dto.getStatus());
                formBean.setStatusStr(dto.getStatusStr());
            } else {
                target = "failure";
            }
            if (mapping.getScope().equals("request")) {
                request.setAttribute(mapping.getAttribute(), formBean);
                request.getSession(true).setAttribute("id", dto.getId());
            } else {
                request.getSession(true).setAttribute(mapping.getAttribute(), formBean);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
