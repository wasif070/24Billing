package aggregatedreports;

import com.myapp.struts.login.LoginDTO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.Utils;
import org.apache.log4j.Logger;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;

public class MonthlyReportAction extends Action {

    static Logger logger = Logger.getLogger(MonthlyReportAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        String target = "success";
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null && login_dto.getSuperUser()) {

            AggregateReportTaskSchedular scheduler = new AggregateReportTaskSchedular();
            AggregateReportForm aggregateForm = (AggregateReportForm) form;

            AggregateReportDTO udto = new AggregateReportDTO();

            if (aggregateForm.getSearch_btn() != null) {
                if (aggregateForm.getOrigin_client_id() > 0) {
                    udto.swClient = true;
                    udto.setOrigin_client_id(aggregateForm.getOrigin_client_id());
                }
                if (aggregateForm.getFrom_year() > 0) {
                    udto.swDateTime = true;
                    udto.setFrom_date(aggregateForm.getFrom_year() + "-" + aggregateForm.getFrom_month() + "-" + aggregateForm.getFrom_day() + " " + aggregateForm.getFrom_hour() + ":" + aggregateForm.getFrom_min() + ":" + aggregateForm.getFrom_sec());
                    udto.setTo_date(aggregateForm.getTo_year() + "-" + aggregateForm.getTo_month() + "-" + aggregateForm.getTo_day() + " " + aggregateForm.getTo_hour() + ":" + aggregateForm.getTo_min() + ":" + aggregateForm.getTo_sec());
                }
            }

            aggregateForm.setList(scheduler.getDTOs(udto));
            aggregateForm.setSearch_btn(null);

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
