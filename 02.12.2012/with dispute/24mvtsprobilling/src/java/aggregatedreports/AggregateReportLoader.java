package aggregatedreports;

import com.myapp.struts.clients.ClientLoader;
import databaseconnector.DBConnection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import org.apache.log4j.Logger;
import com.myapp.struts.settings.SettingsLoader;
import com.myapp.struts.util.MyAppError;
import com.myapp.struts.util.Utils;
import java.sql.PreparedStatement;

public class AggregateReportLoader {

    static Logger logger = Logger.getLogger(AggregateReportLoader.class.getName());
    private ArrayList<AggregateReportDTO> list = null;
    String timeZone = SettingsLoader.getInstance().getSettingsDTO("TIME_ZONE").getSettingValue();

    public AggregateReportLoader() {
    }

    public ArrayList<AggregateReportDTO> getDTOs(AggregateReportDTO p_dto) {
        String today_00_hour = Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        String now = Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        DBConnection dbConnection = null;
        Statement statement = null;
        ResultSet rs = null;
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();
            statement = dbConnection.connection.createStatement();
            list = new ArrayList<AggregateReportDTO>();

            String sql = "select origin_client_id,total_duration,disconn_time from day_wise_report_view where 1 ";
            if (p_dto.swClient) {
                sql += " and origin_client_id=" + p_dto.getOrigin_client_id();
            }
            if (p_dto.swDateTime) {
                sql += " and disconnect_time>='" + p_dto.getFrom_date() + "' and disconnect_time<'" + p_dto.getTo_date() + "'";
            } else {
                sql += " and disconnect_time>='" + today_00_hour + "' and disconnect_time<'" + now + "'";
            }

            logger.debug("report loader sql-->" + sql);
            statement = dbConnection.connection.createStatement();
            rs = statement.executeQuery(sql);
            while (rs.next()) {
                AggregateReportDTO dto = new AggregateReportDTO();
                dto.setOrigin_client_id(rs.getLong("origin_client_id"));
                try {
                    dto.setClient_id_str(ClientLoader.getInstance().getClientDTOByID(dto.getOrigin_client_id()).getClient_id());
                } catch (Exception ex) {
                    dto.setClient_id_str("NF");
                }
                dto.setTotal_duration(Utils.getMMSS(rs.getLong("total_duration")));
                dto.setTotal_duration_sec(rs.getString("total_duration"));
                dto.setDisconnect_time_str(rs.getString("disconn_time"));
                list.add(dto);
            }

        } catch (Exception e) {
            logger.fatal("Exception in Aggregate Report Loader:", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (statement != null) {
                    statement.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return list;
    }

    public MyAppError deleteReport(AggregateReportDTO p_dto) {
        String today_00_hour = Utils.ToDateDDMMYYYY0h0m0s(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        String now = Utils.ToDateDDMMYYYYhhmmss(System.currentTimeMillis() - Utils.getTimeLong(timeZone));
        long count = 0;
        DBConnection dbConnection = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        MyAppError error = new MyAppError();
        try {
            dbConnection = databaseconnector.DBConnector.getInstance().makeConnection();

            String sql = "delete from flamma_cdr where ";

            if (p_dto.getCallType() == 2) {
                sql += " duration <=0";
            } else {
                sql += " duration >0";
            }
            if (p_dto.swClient) {
                sql += " and origin_client_id=" + p_dto.getOrigin_client_id();
            }
            if (p_dto.swTermClient) {
                sql += " and term_client_id=" + p_dto.getTerm_client_id();
            }
            if (p_dto.swDateTime) {
                sql += " and disconnect_time>='" + p_dto.getFrom_date() + "' and disconnect_time<'" + p_dto.getTo_date() + "'";
            } else {
                sql += " and disconnect_time>='" + today_00_hour + "' and disconnect_time<'" + now + "'";
            }

            ps = dbConnection.connection.prepareStatement(sql.replace("delete", "select count(cdr_id) as total_cdrs"));
            rs = ps.executeQuery();
            while (rs.next()) {
                count = rs.getLong("total_cdrs");
            }
            rs.close();
            logger.debug("count total_cdrs-->" + count);

            if (count > 0) {
                ps = dbConnection.connection.prepareStatement(sql);
                logger.debug("delete report sql-->" + sql);
                if (ps.executeUpdate() > 0) {
                    error.setErrorType(MyAppError.NoError);
                    error.setErrorMessage(count + " CDRs are successfully deleted.");
                    return error;
                }
            } else {
                error.setErrorType(MyAppError.ValidationError);
                error.setErrorMessage("No CDR found");
                return error;
            }
        } catch (Exception e) {
            error.setErrorType(MyAppError.DBError);
            error.setErrorMessage("Error in Deleting CDR");
            logger.fatal("Error in Deleting CDR: ", e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
            }
            try {
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception e) {
            }
            try {
                if (dbConnection.connection != null) {
                    databaseconnector.DBConnector.getInstance().freeConnection(dbConnection);
                }
            } catch (Exception e) {
            }
        }
        return error;
    }
}
