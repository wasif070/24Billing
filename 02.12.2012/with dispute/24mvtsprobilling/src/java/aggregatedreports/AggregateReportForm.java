package aggregatedreports;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class AggregateReportForm extends org.apache.struts.action.ActionForm {

    private long id;
    private long origin_client_id;
    private long term_client_id;
    private String disconnect_time;
    private String disconnect_time_str;
    private String total_duration;
    private String search_btn;
    private ArrayList<AggregateReportDTO> list;
    private int from_day;
    private int from_month;
    private int from_year;
    private int from_hour;
    private int from_min;
    private int from_sec;
    private int to_day;
    private int to_month;
    private int to_year;
    private int to_hour;
    private int to_min;
    private int to_sec;
    private int callType;
    private String message;

    public AggregateReportForm() {
        Calendar cal = new GregorianCalendar();
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        int day = cal.get(Calendar.DAY_OF_MONTH);
        from_day = day;
        from_month = month;
        from_year = year;

        to_day = day;
        to_month = month;
        to_year = year;
        to_hour = 23;
        to_min = 59;
        to_sec = 59;
        callType = 2;
    }

    public String getDisconnect_time() {
        return disconnect_time;
    }

    public void setDisconnect_time(String disconnect_time) {
        this.disconnect_time = disconnect_time;
    }

    public String getDisconnect_time_str() {
        return disconnect_time_str;
    }

    public void setDisconnect_time_str(String disconnect_time_str) {
        this.disconnect_time_str = disconnect_time_str;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ArrayList<AggregateReportDTO> getList() {
        return list;
    }

    public void setList(ArrayList<AggregateReportDTO> list) {
        this.list = list;
    }

    public long getOrigin_client_id() {
        return origin_client_id;
    }

    public void setOrigin_client_id(long origin_client_id) {
        this.origin_client_id = origin_client_id;
    }

    public String getTotal_duration() {
        return total_duration;
    }

    public void setTotal_duration(String total_duration) {
        this.total_duration = total_duration;
    }

    public String getSearch_btn() {
        return search_btn;
    }

    public void setSearch_btn(String search_btn) {
        this.search_btn = search_btn;
    }

    public int getFrom_day() {
        return from_day;
    }

    public void setFrom_day(int from_day) {
        this.from_day = from_day;
    }

    public int getFrom_hour() {
        return from_hour;
    }

    public void setFrom_hour(int from_hour) {
        this.from_hour = from_hour;
    }

    public int getFrom_min() {
        return from_min;
    }

    public void setFrom_min(int from_min) {
        this.from_min = from_min;
    }

    public int getFrom_month() {
        return from_month;
    }

    public void setFrom_month(int from_month) {
        this.from_month = from_month;
    }

    public int getFrom_sec() {
        return from_sec;
    }

    public void setFrom_sec(int from_sec) {
        this.from_sec = from_sec;
    }

    public int getFrom_year() {
        return from_year;
    }

    public void setFrom_year(int from_year) {
        this.from_year = from_year;
    }

    public int getTo_day() {
        return to_day;
    }

    public void setTo_day(int to_day) {
        this.to_day = to_day;
    }

    public int getTo_hour() {
        return to_hour;
    }

    public void setTo_hour(int to_hour) {
        this.to_hour = to_hour;
    }

    public int getTo_min() {
        return to_min;
    }

    public void setTo_min(int to_min) {
        this.to_min = to_min;
    }

    public int getTo_month() {
        return to_month;
    }

    public void setTo_month(int to_month) {
        this.to_month = to_month;
    }

    public int getTo_sec() {
        return to_sec;
    }

    public void setTo_sec(int to_sec) {
        this.to_sec = to_sec;
    }

    public int getTo_year() {
        return to_year;
    }

    public void setTo_year(int to_year) {
        this.to_year = to_year;
    }

    public int getCallType() {
        return callType;
    }

    public void setCallType(int callType) {
        this.callType = callType;
    }

    public long getTerm_client_id() {
        return term_client_id;
    }

    public void setTerm_client_id(long term_client_id) {
        this.term_client_id = term_client_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(boolean error, String message) {
        if (error) {
            this.message = "<div class='error'>" + message + "</div>";
        } else {
            this.message = "<div class='success'>" + message + "</div>";
        }
    }
}
