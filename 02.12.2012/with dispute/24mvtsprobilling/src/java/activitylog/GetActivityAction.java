/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import com.myapp.struts.login.LoginDTO;
import com.myapp.struts.session.Constants;
import com.myapp.struts.util.AppConstants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForward;


import org.apache.log4j.Logger;

public class GetActivityAction extends Action {

    static Logger logger = Logger.getLogger(GetActivityAction.class.getName());

    public ActionForward execute(ActionMapping mapping,
            ActionForm form,
            HttpServletRequest request,
            HttpServletResponse response) {
        logger.debug("GetActivityAction class started");

        String target = AppConstants.SUCCESS;
        LoginDTO login_dto = (LoginDTO) request.getSession(true).getAttribute(Constants.LOGIN_DTO);
        if (login_dto != null) {
            ActivityTaskScheduler scheduler = new ActivityTaskScheduler();
            ActivityForm actForm = (ActivityForm) form;

            /*--------------------For geting searching parameter-------------------*/
            ActivityDTO actDTO = new ActivityDTO();

            if (request.getParameter("cid") != null) {
                actDTO.setId(Long.parseLong(request.getParameter("cid")));
            }
            if (request.getParameter("pkey") != null) {
                actDTO.setPrimaryKey(request.getParameter("pkey").trim());
                actDTO.queryWithPKey = true;
            }
            if (request.getParameter("tname") != null) {
                actDTO.setTableName(request.getParameter("tname").trim());
            }

            ActivityDTO activityDTO = scheduler.getActivityDTO(actDTO);

            if (actDTO != null) {
                request.getSession(true).setAttribute(AppConstants.ACTIVITY_DTO, activityDTO);
                request.getSession(true).setAttribute(mapping.getAttribute(), actForm);
            }

        } else {
            request.getSession(true).removeAttribute(Constants.LOGIN_DTO);
            request.getSession(true).setAttribute(Constants.LOGIN_ACCESS_DENIED, "yes");
            target = "index";
        }
        return (mapping.findForward(target));
    }
}
