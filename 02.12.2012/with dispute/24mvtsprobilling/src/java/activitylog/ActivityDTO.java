/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ActivityDTO {

    private long id;
    private String userId;
    private long logTime;
    private String changeTime;
    private String previousValue;
    private String changedValue;
    private String tableName;
    private int isdeleted;
    private int status;
    private String actionName;
    private String primaryKey;
    private long fromTime;
    private long toTime;
    public boolean swTableName = false;
    public boolean swActionName = false;
    public boolean swTimeDuration = false;
    public boolean swUserId = false;
    public boolean queryWithPKey = false;
    public boolean swPKey = false;
    ArrayList<ComparisonDTO> comparisonList;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(String changeTime) {
        this.changeTime = changeTime;
    }

    public String getChangedValue() {
        return changedValue;
    }

    public void setChangedValue(String changedValue) {
        this.changedValue = changedValue;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getIsdeleted() {
        return isdeleted;
    }

    public void setIsdeleted(int isdeleted) {
        this.isdeleted = isdeleted;
    }

    public long getLogTime() {
        return logTime;
    }

    public void setLogTime(long logTime) {
        this.logTime = logTime;
    }

    public String getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(String previousValue) {
        this.previousValue = previousValue;
    }

    public String getPrimaryKey() {
        return primaryKey;
    }

    public void setPrimaryKey(String primaryKey) {
        this.primaryKey = primaryKey;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getFromTime() {
        return fromTime;
    }

    public void setFromTime(long fromTime) {
        this.fromTime = fromTime;
    }

    public long getToTime() {
        return toTime;
    }

    public void setToTime(long toTime) {
        this.toTime = toTime;
    }

    public ArrayList<ComparisonDTO> getComparisonList() {
        return comparisonList;
    }

    public void setComparisonList(ArrayList<ComparisonDTO> comparisonList) {
        this.comparisonList = comparisonList;
    }
}
