/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package activitylog;

import com.myapp.struts.util.AppError;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class ActivityTaskScheduler {

    public ArrayList<ActivityDTO> getSearchedActivityDTOs(ActivityDTO p_dto) {
        return ActivityLoader.getInstance().getActivityDTOsWithSearchParam(p_dto);
    }

    public ActivityDTO getActivityDTO(ActivityDTO p_dto) {
        return ActivityLoader.getInstance().getActivityDTO(p_dto);
    }

    public AppError addActivityDTO(ActivityDTO activity_dto) {
        return ActivityDBFunction.addActivityLog(activity_dto);
    }
}
