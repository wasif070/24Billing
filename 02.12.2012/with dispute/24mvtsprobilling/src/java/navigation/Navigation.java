/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package navigation;

import java.util.ArrayList;



/**
 *
 * @author Rajib
 */
public class Navigation {

    public Navigation() {
    }
    public static String getNavigationStr(ArrayList<String> navMap, String baseURL ){
    String link = "<div class='nav_menu'>";
        if (navMap != null && navMap.size() > 0) {
            link += "<a href=\"" + baseURL + "home/home.jsp\">Home  &raquo;</a> "; // user/listUser.do?list_all=1
            for (String str : navMap) {
                String parts[] = str.split(";", 2);
                String href = parts[0];
                String linkText = parts[1];
                if (!href.equalsIgnoreCase("")) {
                    link += "<a href=\"" + baseURL + href + "\">" + linkText + " &raquo;</a> ";
                } else {
                    link += "<span class=''>" + linkText + "</span>";
                }
            }
        }
        link += "</div>";
        return link;
    }
}
